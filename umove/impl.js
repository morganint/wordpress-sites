//$(document).ready(function(){

//===form submit===
jQuery(document).ready(function($){
	
	$('body').on('click', '.sord_sbmt_but', function(){
		$('#sord_form').submit();
	});
	$('body').on('click', '.sord_sbmt_but_1', function(){
		$('#sord_form_1').submit();
	});
	
	$('body').on('click', '#SubmTransport_1', function(){
		$('#FormTransport_1').submit();
	});
	
	$('body').on('click', '#SubmTransport_2', function(){
		$('#FormTransport_2').submit();
	});
	
	$('body').on('submit', '.FormTransportAction', function(e){
		e.preventDefault();
		$('#'+$(this).attr('id')+' .MssageTransportBox').html('');
		
		var fID = $(this).attr('id');
        
        var err = '';
		
        function checkFields(fields) {
            for (var i = 0; i<fields.length; i++) {
                var input = $('#'+fID+' [name=' + fields[i] + ']')
                var value = input.val();
            
                if( !value || input.is(':invalid')){
                    err += input.attr('data-err') + '<br>';
                }
            }
            return err;
        }
        
        var fieldsToCheck = ['st_from', 'st_to', 'st_day', 'st_size', 'st_f1_eml', 'st_f1_fullname', 'st_f1_phone']
        checkFields(fieldsToCheck)
        
        
//		if($('#'+fID+' input[name=st_from]').val() == ''){
//			err = $('#'+fID+' input[name=st_from]').attr('data-err');
//		}
//		
//		if($('#'+fID+' input[name=st_to]').val() == ''){
//			err = $('#'+fID+' input[name=st_to]').attr('data-err');
//		}
//		
//		if($('#'+fID+' select[name=st_day]').val() == 0){
//			err = $('#'+fID+' select[name=st_day]').attr('data-err');
//		}
//		
//		if($('#'+fID+' select[name=st_size]').val() == 0){
//			err = $('#'+fID+' select[name=st_size]').attr('data-err');
//		}
//		
//		if($('#'+fID+' input[name=st_f1_eml]').val() == ''){
//			err = $('#'+fID+' input[name=st_f1_eml]').attr('data-err');
//		}
//
//		if($('#'+fID+' input[name=st_f1_eml]').is(':invalid')){
//			err = $('#'+fID+' input[name=st_f1_eml]').attr('data-err');
//		}
//		
//		if($('#'+fID+' input[name=st_f1_fullname]').val() == ''){
//			err = $('#'+fID+' input[name=st_f1_fullname]').attr('data-err');
//		}
//		
//		if($('#'+fID+' input[name=st_f1_phone]').val() == ''){
//			err = $('#'+fID+' input[name=st_f1_phone]').attr('data-err');
//		}
		
		if(err != ''){
			$('#'+$(this).attr('id')+' .MssageTransportBox').html(err);	
		}else{
			$('#'+$(this).attr('id')+' .MssageTransportBox').html('Loading...');

			if( $('select[name=st_day]').val() === "Tomorrow") {
				$("#ex2").modal({
					escapeClose: false,
					clickClose: false,
					showClose: false
				});
				return;
			};
			
			$.ajax({
				url:  WPajaxURL,
				type: 'POST',
				data: {
					action:         'notifyAdminNewOrd',
					st_from:        $('#'+fID+' input[name=st_from]').val(),
					st_to:          $('#'+fID+' input[name=st_to]').val(),
					st_day:         $('#'+fID+' select[name=st_day]').val(),
					st_size:        $('#'+fID+' select[name=st_size]').val(),
					st_f1_eml:      $('#'+fID+' input[name=st_f1_eml]').val(),
					st_f1_fullname: $('#'+fID+' input[name=st_f1_fullname]').val(),
					st_f1_phone: 	$('#'+fID+' input[name=st_f1_phone]').val(),
				},
				success: function(data){

					if(data.length == 0){
						//alert($('#'+fID).attr('data-thanks'));
						$('#ex1text').html($('#'+fID).attr('data-thanks'));
						$("#ex1").modal({
							escapeClose: false,
							clickClose: false,
							showClose: false
						});
						
					}else{
						alert(data);
					}
				}
			});
			$('#'+$(this).attr('id')+' .MssageTransportBox').html('');
		}
		
	});
	
});