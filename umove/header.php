<!DOCTYPE html>
<html <?php language_attributes() ?>>
	<head>
		<meta name="viewport" content="width=device-width, initial-scale=1.0, shrink-to-fit=no, user-scalable=no" />
		<meta http-equiv="X-UA-Compatible" content="ie=edge" />
		<meta charset="<?php bloginfo('charset') ?>">
		<?php wp_head() ?>
	</head>
	<body <?php body_class() ?>>
		
		<?php if(get_field('sopt_preloader_yn', 'option')){ ?>
			
			<script>document.querySelector("body").classList.add("js");</script>
			<div class="preloader">
				<div id="fountainG">
				<div id="fountainG_1" class="fountainG"></div>
				<div id="fountainG_2" class="fountainG"></div>
				<div id="fountainG_3" class="fountainG"></div>
				<div id="fountainG_4" class="fountainG"></div>
				<div id="fountainG_5" class="fountainG"></div>
				<div id="fountainG_6" class="fountainG"></div>
				<div id="fountainG_7" class="fountainG"></div>
				<div id="fountainG_8" class="fountainG"></div>
     		</div>
			</div>
		<?php } ?>
		
		<header class="main-header">
			<div class="container">
				<div class="main-header__logo">
					<a href="<?php echo home_url() ?>">
						<img src="<?php echo get_template_directory_uri() ?>/_slicing/data/img/logo.png" alt="<?php bloginfo('name') ?>" />
					</a>
				</div>
				<nav class="main-nav">
					<div class="second-menu">
						<?php if( has_nav_menu('HeaderMenu')){
							wp_nav_menu( array(
									'container'      => false,
									'theme_location' => 'HeaderMenu',
									'menu_id'        => 'HeaderMenu_ID_0',
									'menu_class'     => 'second-menu__list',
									'items_wrap'     => '<ul id="%1$s" class="%2$s">%3$s</ul>',
								)
							);
						} ?>
					</div>

					<div class="menu">
						<!-- <div class="first-menu">
							<?php if( has_nav_menu('HeaderMenu')){
								wp_nav_menu( array(
										'container'      => false,
										'theme_location' => 'HeaderMenuSecond',
										'menu_id'        => 'HeaderMenu_ID_1',
										'menu_class'     => 'first-menu__list',
										'items_wrap'     => '<ul id="%1$s" class="%2$s">%3$s</ul>',
									)
								);
							} ?>
						</div> -->
						<form action="<?php the_field('opt_orderinfo_page_url', 'option') ?>" method="POST" id="sord_form" class="search">
							<input type="text" name="sord_number" placeholder="Reference Number" class="search__input" />
							<input type="submit" name="sord_sbmt"  name="sord_number" value="" class="search__submit" disabled/>
						</form>
					</div>
					
					
					
					
					
					<div class="menu-icon">
						<span></span>
						<span></span>
						<span></span>
						<span></span>
					</div>
				</nav>
			</div>
		</header>