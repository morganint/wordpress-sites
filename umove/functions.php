<?php

// Disable actions in core
include( get_template_directory() . '/inc/wp_disables.php' );

// Theme functions
include( get_template_directory() . '/inc/functions_theme.php' );

// Default settings
include( get_template_directory() . '/inc/default.php' );

// Custom Post Types
include( get_template_directory() . '/inc/cpt.php' );

// Custom Menu Walker
include( get_template_directory() . '/inc/classes.php' );

// Theme thumbnails
include( get_template_directory() . '/inc/thumbnails.php' );

// Theme menus
include( get_template_directory() . '/inc/menus.php' );

// Theme css & js
include( get_template_directory() . '/inc/scripts.php' );

// Theme AJAX
include( get_template_directory() . '/inc/ajax.php' );

// Theme Customizer
include( get_template_directory() . '/inc/functions_customizer.php' );

// Admin functions
//include( get_template_directory() . '/inc/functions_admin.php' );

// PayPal functions
//include( get_template_directory() . '/inc/functions_paypal.php' );

// woocommerce functions
//include( get_template_directory() . '/inc/functions_woo.php' );

// fix image orientation after uploading
//include( get_template_directory() . '/inc/fix-image-orientation.php' );

require_once(ABSPATH . 'wp-admin/includes/screen.php');

//Payment method Payhub integration
//include (get_template_directory() . '/inc/function_payment_method.php');
