<?php get_header(); ?>

<?php while(have_posts()){
	the_post(); ?>
	<section class="container features">
		<div class="row">
			<div class="col">
				<h1><?php the_title() ?></h1>
				<?php the_content() ?>
				
				<?php if(comments_open()){
					comments_template();
				} ?>
			</div>
		</div>
	</section>
<?php } ?>

<script>
	//===add view for post===
	//var addViewPost = <?php the_id() ?>;
	<?php //update_post_meta(get_the_id(), 'sp_view', (int)get_post_meta(get_the_id(), 'sp_view', true)+1); ?>
	//alert(<?php echo (int)get_post_meta(get_the_id(), 'sp_view', true) ?>);
</script>

<?php get_footer(); ?>