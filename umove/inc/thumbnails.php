<?php
// Theme thumbnails
add_theme_support('post-thumbnails');

add_image_size('thumbnail_1920x970',     1920, 970,  true); // home block 1
add_image_size('thumbnail_1920x970_m',   800,  600,  true); // home block 1

add_image_size('thumbnail_400x250',      400,  250,  true); // blog

add_image_size('thumbnail_752x678',      752,  678,  true); // home block 2

add_image_size('thumbnail_501x841',      501,  841,  true); // home block 2

add_image_size('thumbnail_563x526',      563,  526,  true); // step 1 block 2 slider
add_image_size('thumbnail_97x114',       97,   114,  true); // step 1 block 2 slider
