<?php

//===Theme css IN HEAD===
function theme_styles()  { 
	wp_enqueue_style('jquery-modal',        get_template_directory_uri().'/_slicing/data/css/jquery.modal.min.css',                                array());
	wp_enqueue_style('slice_vendors',       get_template_directory_uri().'/_slicing/data/css/vendors.css',                                         array());
	wp_enqueue_style('slice_styles',        get_template_directory_uri().'/_slicing/data/css/styles.css',                                          array());
	wp_enqueue_style('nice-select',         get_template_directory_uri().'/_slicing/data/css/nice-select.min.css',                                 array());
	wp_enqueue_style('jquery-ui',           get_template_directory_uri().'/_slicing/data/css/jquery-ui/jquery-ui.css',                             array());
	wp_enqueue_style('timepicker',          get_template_directory_uri().'/_slicing/data/css/jquery.timepicker.min.css',                           array());
	wp_enqueue_style('theme',               get_stylesheet_uri(),                                                                                  array());
}
	add_action('wp_enqueue_scripts', 'theme_styles');
	
// Theme css & js
function base_scripts_styles() {
	$in_footer = true;
	wp_deregister_script('jquery');
	wp_enqueue_script('jquery',             get_template_directory_uri().'/_slicing/data/js/jquery_3.3.1.js',                                     array(), '', $in_footer);
}
add_action('wp_enqueue_scripts', 'base_scripts_styles');

//===Theme css & js IN FOOTER===
function prefix_add_footer_styles(){
	$in_footer = true;
	wp_enqueue_script('jquery-modal', get_template_directory_uri().'/_slicing/data/js/jquery.modal.min.js', array('jquery'), '', $in_footer);
	wp_enqueue_script('slice_vendors', get_template_directory_uri().'/_slicing/data/js/slick.min.js', array('jquery'), '', $in_footer);
	wp_enqueue_script('creditCardValidator', get_template_directory_uri().'/_slicing/data/js/jquery.creditCardValidator.js', array('jquery'), '', $in_footer);
	wp_enqueue_script('inputmask', get_template_directory_uri().'/_slicing/data/js/jquery.inputmask.min.js', array('jquery'), '', $in_footer);
	wp_enqueue_script('timepicker', get_template_directory_uri().'/_slicing/data/js/jquery.timepicker.min.js', array('jquery'), '', $in_footer);
	wp_enqueue_script('nice-select', get_template_directory_uri().'/_slicing/data/js/jquery.nice-select.min.js',                             array('jquery'), '', $in_footer);
	wp_enqueue_script('jquery-ui', get_template_directory_uri().'/_slicing/data/js/jquery-ui.js', array('jquery'), '', $in_footer);
	wp_enqueue_script('slice_vendors', get_template_directory_uri().'/_slicing/data/js/vendors.js', array('jquery'), '', $in_footer);
	wp_enqueue_script('slice_bundle', get_template_directory_uri().'/_slicing/data/js/bundle.js', array('jquery'), '', $in_footer);
	wp_enqueue_script('google_maps', 'https://maps.googleapis.com/maps/api/js?key=AIzaSyDOB4hNSqQ3vD-eRgusimgH-Rx3FlJejnc&libraries=places', array('jquery'), '', $in_footer);
	wp_enqueue_script('implement', get_template_directory_uri().'/impl.js', array('jquery'), '', $in_footer);
	wp_enqueue_script('comment-reply');
};
add_action('get_footer', 'prefix_add_footer_styles');

function wpse71503_init() {
    if (!is_admin()) {
        wp_deregister_style('thickbox');
        wp_deregister_script('thickbox');
    }
}
add_action('init', 'wpse71503_init');

//=====GOOGLE SPEED TEST=====
function remove_cssjs_ver( $src ){
	if( strpos( $src, '?ver=' ) )
		$src = remove_query_arg( 'ver', $src );
	return $src;
}
add_filter( 'style_loader_src',  'remove_cssjs_ver', 10, 2 );
add_filter( 'script_loader_src', 'remove_cssjs_ver', 10, 2 );
//=====//GOOGLE SPEED TEST=====

//===HTML VALIDATION===
add_filter('style_loader_tag', 'clean_style_tag');
function clean_style_tag($src) {
	$src = str_replace("type='text/css'", '', $src);
	$src = str_replace('type="text/css"', '', $src);
    return $src;
}
add_filter('script_loader_tag', 'clean_script_tag');
function clean_script_tag($src) {
	$src = str_replace("type='text/javascript'", '', $src);
	$src = str_replace('type="text/javascript"', '', $src);
    return $src;
}
//===//HTML VALIDATION===

//===ACF QTANSLATE admin styles===
add_action('admin_head', 'my_custom_fonts');
function my_custom_fonts() {
	echo'<style>
		.multi-language-field {
			margin-top: -20px;
		}
	</style>';
}
