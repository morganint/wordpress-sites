<?php

//===Post Type Reservation===
function codex_shiporder_init(){
	//===post_type===
	$labels = array(
		'name'               => __('Quote List',     'umove'),
		'singular_name'      => __('Quotes',         'umove'),
		'menu_name'          => __('Quote List',  'umove'),
		'name_admin_bar'     => __('Orders',         'umove'),
        'edit_item'          => __('Edit Quote'),
	);
	$args = array(
		'labels'             => $labels,
		'public'             => true,
		'publicly_queryable' => false,
        
		'show_ui'            => true,
		'show_in_menu'       => true,
		'query_var'          => true,
		'rewrite'            => array('slug' => 'shiporder'),
		'capability_type'    => 'post',
		'has_archive'        => true,
		'hierarchical'       => false,
		'menu_position'      => null,
		'supports'           => array('title'),
    
        'capabilities'       => array( 'create_posts' => false ),       

        'map_meta_cap'       => true,
        
	);
    
	register_post_type('shiporder', $args );
}
add_action('init', 'codex_shiporder_init');



//#ac-pro-version .padding-box { display: none; }

//div#direct-feedback { display: none; }

//div#plugin-support {display: none;}
    