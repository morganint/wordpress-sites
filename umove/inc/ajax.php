<?php
//===send email to USER from admin board===
add_action('wp_ajax_cscpfu',        'cscpfu');
add_action('wp_ajax_nopriv_cscpfu', 'cscpfu');
function cscpfu(){
	if(isset($_POST['pidcsp'])){
		$post_id = $_POST['pidcsp'];
		
		$boby = get_field('themltpl_body_delivery',  'option', false, false);
		//$boby = str_replace('[emltpl_refnumb]',      encode($post_id), $boby);
        $boby = str_replace('[emltpl_refnumb]',      get_post_meta($post_id, 'st_referal_number',          true), $boby);
        
		$boby = str_replace('[emltpl_st_from]',      get_post_meta($post_id, 'st_from',          true), $boby);
		$boby = str_replace('[emltpl_st_to]',        get_post_meta($post_id, 'st_to',            true), $boby);
		$boby = str_replace('[st_f1_fullname]',      get_post_meta($post_id, 'st_f1_fullname',   true), $boby);
		$boby = str_replace('[stp1p_b4_cart_list]',  get_post_meta($post_id, 'st_cartype',       true), $boby);
		$boby = str_replace('[st_delivery_type]',    get_post_meta($post_id, 'st_delivery_days', true), $boby);
		$boby = str_replace('[st_delivery]',         get_post_meta($post_id, 'st_day',           true), $boby);
		$boby = str_replace('[st_summ]',             get_post_meta($post_id, 'st_summ',          true), $boby);
		$boby = str_replace('[usr_url_front_urder]', get_post_meta($post_id, 'client_url',       true), $boby);
        
	
		$to       = get_post_meta($post_id, 'st_usr_email', true); 
		$subject  = __('Your Move Quote', 'umove');
		$headers  = 'From: '.get_option('admin_email')."\r\n";
		$headers .= "MIME-Version: 1.0\r\n";
		$headers .= "Content-Type: text/html; charset=ISO-8859-1\r\n";
		$emlr = mail($to, $subject, $boby, $headers);
		
		
		$firstname = get_post_meta($post_id, 'st_f1_fullname', true);
		$amount = get_post_meta($post_id, 'st_summ', true);
		$dealset = get_post_meta($post_id, 'hubsport_quote_id', true);
        $refNum = get_post_meta($post_id, 'st_referal_number', true);
        
		$arr3 = array(
            'properties' => array(
                array(
					//'value' => $firstname, 
					'value' => $firstname . ' - ' . $refNum, 
					'name' => 'dealname'
				),
            
				array(
					'value' => 1020595, //contractsent
					'name' => 'dealstage'
				),
				array(
					'value' => '39551299', 
					'name' => 'hubspot_owner_id'
				),
								
				array(
					'value' => $amount, 
					'name' => 'amount'
				),
				array(
					'value' => 'newbusiness', 
					'name' => 'dealtype' 
				),
            )
        );
        $post_json = json_encode($arr3);
       // $endpoint = 'https://api.hubapi.com/deals/v1/deal?hapikey=c1c64878-af5c-40bb-b65e-3a18def1a565';
        $endpoint = 'https://api.hubapi.com/deals/v1/deal/'.$dealset.'?hapikey=c1c64878-af5c-40bb-b65e-3a18def1a565&dealstage=1020595';
		             //https://api.hubapi.com/deals/v1/deal/1081693383?hapikey=c1c64878-af5c-40bb-b65e-3a18def1a565
        
        $ch = @curl_init();
        @curl_setopt($ch, CURLOPT_POST, true);
        @curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
        @curl_setopt($ch, CURLOPT_CUSTOMREQUEST, "PUT");
        @curl_setopt($ch, CURLOPT_POSTFIELDS, $post_json);
        @curl_setopt($ch, CURLOPT_URL, $endpoint);
        @curl_setopt($ch, CURLOPT_HTTPHEADER, array('Content-Type: application/json'));
        @curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
        $response = @curl_exec($ch);
        $status_code = @curl_getinfo($ch, CURLINFO_HTTP_CODE);
        $curl_errors = curl_error($ch);
        @curl_close($ch);
        //echo "\ncurl Errors: " . $curl_errors;
        //echo "\nStatus code: " . $status_code;
        //echo "\nResponse: " . $response;
        
        /********************/
		
		
		
		echo __('Quote Submitted to email:', 'umove').$to.';<br/> You price:'.get_post_meta($post_id, 'st_summ', true);
	}else{
		_e('Error request!', 'umove');
	}
	die();
}

add_action('wp_ajax_notifyAdminNewOrd',        'notifyAdminNewOrd');
add_action('wp_ajax_nopriv_notifyAdminNewOrd', 'notifyAdminNewOrd');
function notifyAdminNewOrd(){
	if( isset($_POST['st_from']) && isset($_POST['st_f1_phone']) && isset($_POST['st_to']) && isset($_POST['st_day']) && isset($_POST['st_size']) && isset($_POST['st_f1_eml']) && isset($_POST['st_f1_fullname']) ){
		$post_id = wp_insert_post(array(
			'post_title'    => 'temp',
			'post_content'  => '',
			'post_status'   => 'publish',
			'post_type'     => 'shiporder',
		));
		$post_idx = $post_id;
		$sttl = encode($post_idx);
		wp_update_post(array('ID'=>$post_id, 'post_title'=>$post_idx));
		
		$au = get_home_url().'/wp-admin/post.php?post='.$post_id.'&action=edit';
		$uu = get_field('opt_tsp_spep_1_page', 'option').'?post_id='.$sttl;
		
		update_post_meta($post_id, 'st_wp_post_id',     	$post_idx);
		update_post_meta($post_id, 'st_from',           	htmlspecialchars($_POST['st_from']));
		update_post_meta($post_id, 'st_to',             	htmlspecialchars($_POST['st_to']));
		update_post_meta($post_id, 'st_f1_fullname',    	htmlspecialchars($_POST['st_f1_fullname']));
		update_post_meta($post_id, 'st_f1_phone',    		htmlspecialchars($_POST['st_f1_phone']));
		update_post_meta($post_id, 'st_day',            	htmlspecialchars($_POST['st_day']));
		update_post_meta($post_id, 'st_size',           	htmlspecialchars($_POST['st_size']));
		update_post_meta($post_id, 'st_usr_email',      	htmlspecialchars($_POST['st_f1_eml']));
		update_post_meta($post_id, 'admin_url',         	$au);
		update_post_meta($post_id, 'client_url',        	$uu);
		update_post_meta($post_id, 'st_referal_number', 	$sttl);
		update_post_meta($post_id, 'ord_payed_yn', 	        'no');
		update_post_meta($post_id, 'st_summ', 	            '0.01');
        
		//$to      = get_option('admin_email');
		$to      = "Quotes@yourmovetoday.com, Glenn@yourmovetoday.com, Josh@yourmovetoday.com, steve@umovecompany.com, ben@yourmovetoday.com";
		//$to      = "wp.dev.morgan@gmail.com";
		$subject = __('New Reference Number from Your Move Quote client', 'umove');
		$message = __('Your Move Order Number', 'umove').' №'.$post_idx.'. '.chr(10).chr(13).'admin-URL: '.get_home_url().'/wp-admin/post.php?post='.$post_id.'&action=edit '.chr(10).chr(13).'user-URL: '.get_field('opt_tsp_spep_1_page', 'option').'?post_id='.$sttl;
		$headers = 'From: '.get_option('admin_email')."\r\n";
		mail($to, $subject, $message, $headers);
		
		$email = htmlspecialchars($_POST['st_f1_eml']);
        $firstname = htmlspecialchars($_POST['st_f1_fullname']);
        $lastname = '';
        $phone = htmlspecialchars($_POST['st_f1_phone']);
        $hs_lead_status = 'NEW';
        $lifecyclestage = 'opportunity';
    
        $arr = array(
            'properties' => array(
                array(
                    'property' => 'email',
                    'value' => $email
                ),
                array(
                    'property' => 'firstname',
                    'value' => $firstname
                ),
                array(
                    'property' => 'hs_lead_status',
                    'value' => $hs_lead_status
                ),
                array(
                    'property' => 'lifecyclestage',
                    'value' => $lifecyclestage
                ),
                array(
                    'property' => 'phone',
                    'value' => $phone
                )
            )
        );
        $post_json = json_encode($arr);
        $endpoint = 'https://api.hubapi.com/contacts/v1/contact?hapikey=c1c64878-af5c-40bb-b65e-3a18def1a565';
        $ch = @curl_init();
        @curl_setopt($ch, CURLOPT_POST, true);
        @curl_setopt($ch, CURLOPT_POSTFIELDS, $post_json);
        @curl_setopt($ch, CURLOPT_URL, $endpoint);
        @curl_setopt($ch, CURLOPT_HTTPHEADER, array('Content-Type: application/json'));
        @curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
        $response = @curl_exec($ch);
        $status_code = @curl_getinfo($ch, CURLINFO_HTTP_CODE);
        $curl_errors = curl_error($ch);
        @curl_close($ch);
	
        
        /******deals*********/
        
        //$dealname = htmlspecialchars($_POST['st_f1_fullname']);
        //dealstage = '6524461';
		//dealowner = 'newbusiness';
        //amount = '777';

        //$lastname = 'Morgan';
        //$phone = '777-77-77';
        //$hs_lead_status = 'NEW';
        //$lifecyclestage = 'opportunity';
        //$spl = explode(' ', $string);
        
        $arr2 = array(
            'properties' => array(
                array(
					'value' => $firstname . ' - ' . $sttl, 
					'name' => 'dealname'
				),
            
				array(
					'value' => 1013097, 
					'name' => 'dealstage'
				),
				array(
					'value' => '39551299', 
					'name' => 'hubspot_owner_id'
				),
								
				/*array(
					'value' => '777', 
					'name' => 'amount'
				),*/
				array(
					'value' => 'newbusiness', 
					'name' => 'dealtype' 
				),
            )
        );
        $post_json = json_encode($arr2);
        $endpoint = 'https://api.hubapi.com/deals/v1/deal?hapikey=c1c64878-af5c-40bb-b65e-3a18def1a565';
		
        $ch = @curl_init();
        @curl_setopt($ch, CURLOPT_POST, true);
        @curl_setopt($ch, CURLOPT_POSTFIELDS, $post_json);
        @curl_setopt($ch, CURLOPT_URL, $endpoint);
        @curl_setopt($ch, CURLOPT_HTTPHEADER, array('Content-Type: application/json'));
        @curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
        $response = @curl_exec($ch);
        $status_code = @curl_getinfo($ch, CURLINFO_HTTP_CODE);
        $curl_errors = curl_error($ch);
        @curl_close($ch);
        //echo "\ncurl Errors: " . $curl_errors;
        //echo "\nStatus code: " . $status_code;
        //echo "\nResponse: " . $response;
        //value to db
        $json = json_decode($response, true);
        $dealId = $json['dealId'];
        //echo $dealId;
        //https://api.hubapi.com/deals/v1/deal/1081693383?hapikey=c1c64878-af5c-40bb-b65e-3a18def1a565
        
		update_post_meta($post_id, 'hubsport_quote_id', htmlspecialchars($dealId));

	}else{
		echo __('Error request!', 'umove');
	}
	die();
}