<?php
/**
 * Plugin Name: Fix Image Orientation
 * Plugin URI:  http://www.n7studios.co.uk
 * Description: Checks for an EXIF orientation flag, and rotates the image so it displays with the correct orientation in WordPress. Also retains the EXIF and IPTC metadata on the image.
 * Author:      Tim Carr
 * Author URI:  http://www.n7studios.co.uk
 * Version:     1.0.0
 */

/**
* Orientation flags we're looking for:
* 8: We need to rotate the image 90 degrees counter-clockwise
* 3: We need to rotate the image 180 degrees
* 6: We need to rotate the image 90 degrees clockwise (270 degrees counter-clockwise)
*/
function fix_image_orientation( $file ) {

	if ( ! file_exists( $file['file'] ) ) {
		return $file;
	}

	$exif_data = wp_read_image_metadata( $file['file'] );
	if ( ! $exif_data ) {
		return $file;
	}

	if ( ! isset( $exif_data['orientation'] ) ) {
		return $file;
	}

	$required_orientations = array( 8, 3, 6 );
	if ( ! in_array( $exif_data['orientation'], $required_orientations ) ) {
		return $file;
	}

	$image = wp_get_image_editor( $file['file'] );
	if ( is_wp_error( $image ) ) {
		return $file;
	} 

	$source_size = getimagesize( $file['file'], $image_info );

	switch ( $exif_data['orientation'] ) {

		case 8:
			$image->rotate( 90 );
			break;

		case 3:
			$image->rotate( 180 );
			break;

		case 6:
			$image->rotate( 270 );
			break;

	}

	$image->save( $file['file'] );


	$result = transfer_iptc_exif_to_image( $image_info, $file['file'], $exif_data['orientation'] );
	if ( ! $result ) {
		return $file;
	}
	
	return $file;

}
add_filter( 'wp_handle_upload', 'fix_image_orientation' );

function transfer_iptc_exif_to_image( $image_info, $destination_image, $original_orientation ) {

    if ( ! file_exists( $destination_image ) ) {
    	return false;
    }

    $exif_data = ( ( is_array( $image_info ) && key_exists( 'APP1', $image_info ) ) ? $image_info['APP1'] : null );
    if ( $exif_data ) {
    	$exif_data = str_replace( chr( dechex( $original_orientation ) ) , chr( 0x1 ), $exif_data );

        $exif_length = strlen( $exif_data ) + 2;
        if ( $exif_length > 0xFFFF ) {
        	return false;
        }

        $exif_data = chr(0xFF) . chr(0xE1) . chr( ( $exif_length >> 8 ) & 0xFF) . chr( $exif_length & 0xFF ) . $exif_data;
    }

    $iptc_data = ( ( is_array( $image_info ) && key_exists( 'APP13', $image_info ) ) ? $image_info['APP13'] : null );
    if ( $iptc_data ) {
        $iptc_length = strlen( $iptc_data ) + 2;
        if ( $iptc_length > 0xFFFF ) {
        	return false;
        }

        $iptc_data = chr(0xFF) . chr(0xED) . chr( ( $iptc_length >> 8) & 0xFF) . chr( $iptc_length & 0xFF ) . $iptc_data;
    }    

    $destination_image_contents = file_get_contents( $destination_image );
    if ( ! $destination_image_contents ) {
    	return false;
    }
    if ( strlen( $destination_image_contents ) == 0 ) {
    	return false;
    }

    $destination_image_contents = substr( $destination_image_contents, 2 );
    $portion_to_add = chr(0xFF) . chr(0xD8); // Variable accumulates new & original IPTC application segments
    $exif_added = ! $exif_data;
    $iptc_added = ! $iptc_data;

    while ( ( substr( $destination_image_contents, 0, 2 ) & 0xFFF0 ) === 0xFFE0 ) {
        $segment_length = ( substr( $destination_image_contents, 2, 2 ) & 0xFFFF );
        $iptc_segment_number = ( substr( $destination_image_contents, 1, 1 ) & 0x0F );   // Last 4 bits of second byte is IPTC segment #
        if ( $segment_length <= 2 ) {
        	return false;
        }
        
        $thisexistingsegment = substr( $destination_image_contents, 0, $segment_length + 2 );
        if ( ( 1 <= $iptc_segment_number) && ( ! $exif_added ) ) {
            $portion_to_add .= $exif_data;
            $exif_added = true;
            if ( 1 === $iptc_segment_number ) {
                $thisexistingsegment = '';
            }
        }

        if ( ( 13 <= $iptc_segment_number ) && ( ! $iptc_added ) ) {
            $portion_to_add .= $iptc_data;
            $iptc_added = true;
            if ( 13 === $iptc_segment_number ) {
                $thisexistingsegment = '';
            }
        }

        $portion_to_add .= $thisexistingsegment;
        $destination_image_contents = substr( $destination_image_contents, $segment_length + 2 );
    }

    if ( ! $exif_added ) {
        $portion_to_add .= $exif_data;
    }
    if ( ! $iptc_added ) {
        $portion_to_add .= $iptc_data;
    }

    $output_file = fopen( $destination_image, 'w' );
    if ( $output_file ) {
    	return fwrite( $output_file, $portion_to_add . $destination_image_contents ); 
    }

    return false;
    
}