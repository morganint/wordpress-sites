<?php
	//===Disable Emoji===
	remove_action('wp_head',             'wp_generator');
	remove_action('wp_head',             'print_emoji_detection_script', 7);
	remove_action('admin_print_scripts', 'print_emoji_detection_script');
	remove_action('wp_print_styles',     'print_emoji_styles');
	remove_action('admin_print_styles',  'print_emoji_styles');
	remove_filter('the_content_feed',    'wp_staticize_emoji');
	remove_filter('comment_text_rss',    'wp_staticize_emoji');
	remove_filter('wp_mail',             'wp_staticize_emoji_for_email');
	
	//===Disable embeds on init===
	add_action('init', function(){
		remove_action('wp_head',          'wp_oembed_add_discovery_links');
		remove_action('wp_head',          'wp_oembed_add_host_js');
	}, PHP_INT_MAX - 1);
	
	//===disable NEW EDITOR for posts===
	add_filter('use_block_editor_for_post', '__return_false', 10);
	//===disable NEW EDITOR for post types===
	add_filter('use_block_editor_for_post_type', '__return_false', 10);
	
	//===remove defoult editor from pages===
	add_action('admin_init', 'hide_editor');
	function hide_editor(){
		if(isset($_POST['post_ID'])){$post_id = $_POST['post_ID'];}
		if(isset($_GET['post'])){$post_id = $_GET['post'];}
		if(!isset($post_id))return;
		$tpn = get_page_template_slug($post_id);
		if( $tpn == 'template_page/home.php' ||
			$tpn == 'template_page/transport_step_1.php' ||
			$tpn == 'template_page/transport_step_2.php' ||
			$tpn == 'template_page/transport_step_31.php' ||
			$tpn == 'template_page/transport_step_4.php'
		){
			remove_post_type_support('page', 'editor');
			remove_post_type_support('page', 'thumbnail');
		}
	}
	
	
	
	// Remove fields from profile page
	if ( ! function_exists( 'cor_remove_personal_options' ) ) {
		function cor_remove_personal_options( $subject ) {
			$subject = preg_replace('#<h2>'.__("Personal Options").'</h2>#s', '', $subject, 1); // Remove the "Personal Options" title
			$subject = preg_replace('#<tr class="user-rich-editing-wrap(.*?)</tr>#s', '', $subject, 1); // Remove the "Visual Editor" field
			$subject = preg_replace('#<tr class="user-comment-shortcuts-wrap(.*?)</tr>#s', '', $subject, 1); // Remove the "Keyboard Shortcuts" field
			//$subject = preg_replace('#<tr class="show-admin-bar(.*?)</tr>#s', '', $subject, 1); // Remove the "Toolbar" field
			$subject = preg_replace('#<h2>'.__("Name").'</h2>#s', '', $subject, 1); // Remove the "Name" title
			$subject = preg_replace('#<tr class="user-display-name-wrap(.*?)</tr>#s', '', $subject, 1); // Remove the "Display name publicly as" field
			$subject = preg_replace('#<h2>'.__("Contact Info").'</h2>#s', '', $subject, 1); // Remove the "Contact Info" title
			$subject = preg_replace('#<tr class="user-url-wrap(.*?)</tr>#s', '', $subject, 1); // Remove the "Website" field
			$subject = preg_replace('#<h2>'.__("About Yourself").'</h2>#s', '', $subject, 1); // Remove the "About Yourself" title
			$subject = preg_replace('#<tr class="user-description-wrap(.*?)</tr>#s', '', $subject, 1); // Remove the "Biographical Info" field
			//$subject = preg_replace('#<tr class="user-profile-picture(.*?)</tr>#s', '', $subject, 1); // Remove the "Profile Picture" field
			$subject = preg_replace('#<h2>'.__("Account Management").'</h2>#s', '', $subject, 1); // Remove the "Personal Options" title
			return $subject;
		}

		function cor_profile_subject_start() {
			ob_start( 'cor_remove_personal_options' );
		}

		function cor_profile_subject_end() {
			ob_end_flush();
		}
	}
	add_action( 'admin_head', 'cor_profile_subject_start' );
	add_action( 'admin_footer', 'cor_profile_subject_end' );
	
	
	/* remoove color sheme in admin */
	add_action( 'admin_init', 'my_limit_admin_color_options', 1 );
	function my_limit_admin_color_options(){
		global $_wp_admin_css_colors;
		$fresh_color_data = $_wp_admin_css_colors['fresh'];
		$_wp_admin_css_colors = array( 'fresh' => $fresh_color_data );
	}
	
	
	//===ACF QTANSLATE admin styles===
	add_action('admin_head', 'wpusr_custom_fonts');
	function wpusr_custom_fonts(){
		echo'<style>
			.form-table th, .form-table td {
				padding: 7px 10px 2px 0;
			}
		</style>';
	}
	
	
?>