<?php



//===ADMIN BOARD SEARCH QUOTES BY SECRET NUMBER===
function extend_cpt_admin_searchxx( $query ) {
  // Make sure we're in the admin area and that this is our custom post type
  if ( !is_admin() || $query->query['post_type'] != 'shiporder' ){return;}

  // Put all the meta fields you want to search for here
  $custom_fields = array("st_referal_number");
  // The string submitted via the search form
  $searchterm = $query->query_vars['s'];

  // Set to empty, otherwise no results will be returned.
  // The one downside is that the displayed search text is empty at the top of the page.
  $query->query_vars['s'] = '';

  if ($searchterm != ""){
    // Add additional meta_query parameter to the WP_Query object.
    // Reference: https://codex.wordpress.org/Class_Reference/WP_Query#Custom_Field_Parameters
    $meta_query = array();
    foreach($custom_fields as $cf) {
      array_push($meta_query, array(
        'key'      => $cf,
        'value'    => $searchterm,
        'compare'  => 'LIKE'
      ));
    }
    // Use an 'OR' comparison for each additional custom meta field.
    if (count($meta_query) > 1){
      $meta_query['relation'] = 'OR';
    }
    // Set the meta_query parameter
    $query->set('meta_query', $meta_query);


    // To allow the search to also return "OR" results on the post_title
    $query->set('_meta_or_title', $searchterm);
  }
}
add_action('pre_get_posts', 'extend_cpt_admin_searchxx');


/**
 * If you wanted to have two sets of metaboxes.
 */
function add_events_metaboxes_1() {
	add_meta_box(
		'wpt_events_location',
		'Send Quotation',
		'wpt_events_location',
		'shiporder',
		'side',
		'default'
	);
} 
add_action('add_meta_boxes', 'add_events_metaboxes_1');

/**
 * Output the HTML for the metabox.
 */
function wpt_events_location(){
	global $post;
	wp_nonce_field( basename( __FILE__ ), 'event_fields' );
	$location = get_post_meta($post->ID, 'location', true);
	echo'<span id="admin_sent_deliv_propous" class="button button-large" data-pidcsp="'.$post->ID.'">Send quote to client</span>
		<div id="admin_sent_deliv_propous_msg"></div>';
}

add_action('admin_head', 'propouse_action_js');
function propouse_action_js(){
  echo '<style>
			#admin_sent_deliv_propous{cursor: pointer; border:1px solid #000000; width:100%; text-align: center;}
	
			#admin_sent_deliv_propous {
				background: #0085ba;
				border-color: #0073aa #006799 #006799;
				box-shadow: 0 1px 0 #006799;
				color: #fff;
				text-decoration: none;
				text-shadow: 0 -1px 1px #006799, 1px 0 1px #006799, 0 1px 1px #006799, -1px 0 1px #006799;		
			}
			
		</style>
		<script>
			function csready(){
				function admin_sent_deliv_propous_set(){
					var pidcsp = jQuery("#admin_sent_deliv_propous").attr("data-pidcsp");
					jQuery.ajax({
						type:     "POST",
						cache:    false,
						url:      ajaxurl,
						data:     "action=cscpfu&pidcsp="+pidcsp,
						success:function(data){
							jQuery("#admin_sent_deliv_propous_msg").html(data);
						}
					});
				}
				
				//event.preventDefault();
				//event.stopPropagation();
				
				var acsobject = document.getElementById("admin_sent_deliv_propous");
				acsobject.addEventListener("click", admin_sent_deliv_propous_set, false);
			}
			
			document.addEventListener("DOMContentLoaded", csready);
		</script>';
}


function encode(){
	$res = substr(time(), 99);
	$res .= rand(1,7).rand(1,7).rand(1,7).rand(1,7).rand(1,7).rand(1,7);
    return $res;
}


function decode($key){
    $args = array(
		'post_type'         => 'shiporder',
        'meta_query'        => array(
            array(
                'key'       => 'st_referal_number',
                'value'     => $key,
            ),
        ),
        'posts_per_page'    => '1',
    );
    $posts = get_posts($args);
    if(!$posts || is_wp_error($posts)){
		return false;
	}else{
		return $posts[0]->ID;
	}
}


//===get fist X words from post===
function get_excerpt_v1(){
	$excerpt = get_the_content();
	$excerpt = preg_replace(" ([.*?])",'',$excerpt);
	$excerpt = strip_shortcodes($excerpt);
	$excerpt = strip_tags($excerpt);
	$excerpt = substr($excerpt, 0, 100);
	$excerpt = substr($excerpt, 0, strripos($excerpt, " "));
	$excerpt = trim(preg_replace( '/\s+/', ' ', $excerpt));
	return $excerpt;
}


function custom_excerpt_length( $length ) {
	return 30;
}
add_filter( 'excerpt_length', 'custom_excerpt_length', 999 );

//===add filter for excerpt (qtranslatex)===
add_filter('get_the_excerpt', 'filter_function_name_2980', 10, 2);
function filter_function_name_2980($post_excerpt, $post){
	$post_excerpt = apply_filters('the_excerpt', $post_excerpt);
	return $post_excerpt;
}


//==BOOTSTRAPE 4 Page Navigation===
function page_navicx($before = '', $after = ''){
    global $wpdb, $wp_query;
    $request = $wp_query->request;
    $posts_per_page = intval(get_query_var('posts_per_page'));
    $paged = intval(get_query_var('paged'));
    $numposts = $wp_query->found_posts;
    $max_page = $wp_query->max_num_pages;
    if ( $numposts <= $posts_per_page ) { return; }
    if(empty($paged) || $paged == 0) {
        $paged = 1;
    }
    $pages_to_show = 7;
    $pages_to_show_minus_1 = $pages_to_show-1;
    $half_page_start = floor($pages_to_show_minus_1/2);
    $half_page_end = ceil($pages_to_show_minus_1/2);
    $start_page = $paged - $half_page_start;
    if($start_page <= 0) {
        $start_page = 1;
    }
    $end_page = $paged + $half_page_end;
    if(($end_page - $start_page) != $pages_to_show_minus_1) {
        $end_page = $start_page + $pages_to_show_minus_1;
    }
    if($end_page > $max_page) {
        $start_page = $max_page - $pages_to_show_minus_1;
        $end_page = $max_page;
    }
    if($start_page <= 0) {
        $start_page = 1;
    }
	
    echo $before.'<ul class="pagination list-unstyled">';
    // if($paged > 1){
        // echo '<li class="page-item prev">
				// <a class="page-link" href="'.get_pagenum_link().'" title="First"><span></span><span></span><span></span></a>
			  // </li>';
    // }
	
    if(get_previous_posts_link()){
		echo '<li class="page-item">
				<a class="page-link btn btn-slick btn-prev" href="'.get_previous_posts_page_link().'"><span></span><span></span><span></span></a>
			  </li>'; 
	}else{
		echo '<li class="page-item disabled">
				<a class="page-link btn btn-slick btn-prev" href="#"><span></span><span></span><span></span></a>
			  </li>';
	}
     
    for($i = $start_page; $i <= $end_page; $i++){
        if($i == $paged){
            echo '<li class="page-item active">
					<a class="page-link" href="#">'.$i.'</a>
				  </li>';
        }else{
            echo '<li class="page-item">
					<a class="page-link" href="'.get_pagenum_link($i).'">'.$i.'</a>
				  </li>';
        }
    }
	
    if(get_next_posts_link()){
		echo '<li class="page-item">
				<a class="page-link btn btn-slick btn-next" href="'.get_next_posts_page_link().'"><span></span><span></span><span></span></a>
			  </li>';
	}else{
		echo '<li class="page-item disabled">
				<a class="page-link btn btn-slick btn-next" href="#"><span></span><span></span><span></span></a>
			  </li>';
	}
	
    // if($end_page < $max_page) {
        // $last_page_text = '<span></span><span></span><span></span>';
        // echo '<li class="page-item next"><a class="page-link" href="'.get_pagenum_link($max_page).'" title="Last">'.$last_page_text.'</a></li>';
    // }
    echo '</ul>'.$after;
}


function cc_mime_types($mimes) {
  $mimes['svg'] = 'image/svg+xml';
  return $mimes;
}
add_filter('upload_mimes', 'cc_mime_types');



function remove_menus(){
  remove_menu_page( 'edit.php?post_type=acf-field-group' );
}
add_action( 'admin_menu', 'remove_menus' );
