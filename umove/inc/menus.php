<?php
// Theme menus

register_nav_menus( array(
	'HeaderMenu'       => __( 'Header menu', 'umove' ),
	'HeaderMenuSecond' => __( 'Header menu second', 'umove' ),
	'footer1'   => __( 'Footer 1', 'umove' ),
	'footer2'   => __( 'Footer 2', 'umove' ),
	'footer3'   => __( 'Footer 3', 'umove' ),
) );