<?php



//===RESET PASSWORD CUSTOM EMAIL===
// function codecanal_reset_password_message($message, $key){
	// if(strpos($_POST['user_login'], '@')){
		// $user_data = get_user_by('email', trim($_POST['user_login']));
	// }else{
		// $login = trim($_POST['user_login']);
		// $user_data = get_user_by('login', $login);
	// }
	// $user_login = $user_data->user_login;
	// $msg = 'Кто-то запросил сброс пароля для учётной записи на сайте ПЭОЖД АВАНТАЖ:'."\r\n\r\n";
	// //$msg .= network_site_url() . "\r\n\r\n";
	// //$msg .= sprintf(__('Username: %s', 'avantag'), $user_login) . "\r\n\r\n";
	// $msg .= 'Если произошла ошибка, просто проигнорируйте это письмо, и ничего не произойдёт.' . "\r\n\r\n";
	// $msg .= 'Чтобы сбросить пароль, перейдите по следующей ссылке: ';
	// $msg .= '<a href="'.network_site_url("wp-login.php?action=rp&key=$key&login=".rawurlencode($user_login), 'login').'">перейти</a>';
	// return $msg;
// }
// add_filter('retrieve_password_message', codecanal_reset_password_message, null, 2);



//===CHANGE EMAIL FROM===
// function wpb_sender_email( $original_email_address ) {
    // return get_option('admin_email');
// }
// function wpb_sender_name( $original_email_from ) {
    // return 'ПЭОЖД АВАНТАЖ ';
// }
// add_filter( 'wp_mail_from', 'wpb_sender_email' );
// add_filter( 'wp_mail_from_name', 'wpb_sender_name' );



// auth user after register
function usrauthAfterRegister(){
	$creds = array(
			'user_login'    => $_POST['usrmail'],
			'user_password' => $_POST['usrpswdin'],
			'remember'      => true
		);
	$user = wp_signon( $creds, false );
	if(!is_wp_error($user)){
		wp_redirect( get_field('private_myaccount_page', 'option') );
		exit;
	}
}
if( isset($_POST['usrmail']) && isset($_POST['usrpswdin']) ){
	usrauthAfterRegister();
}
// auth user after register


// auth user simple
function usrauthSimple(){
	$creds = array(
			'user_login'    => $_POST['authusrmail'],
			'user_password' => $_POST['authusrpswdin'],
			'remember'      => true
		);
	$user = wp_signon( $creds, false );
	if(!is_wp_error($user)){
		$GLOBALS['private_auth_page']='';
		wp_redirect( get_field('private_myaccount_page', 'option') );
		exit;
	}else{
		$GLOBALS['private_auth_page']='Error auth.';
		wp_redirect( get_field('private_auth_page', 'option') );
		exit;
	}
}
if( isset($_POST['authusrmail']) && isset($_POST['authusrpswdin']) ){
	usrauthSimple();
}
//===


// logout user
if( isset($_GET['logout']) ){
	wp_logout();
	wp_redirect(home_url());
	exit;
}
//===


// auth form for function usrauthSimple()
function getSiteAuthForm(){
	$res='<div id="accountInformer" class="siteerror">'.$GLOBALS['private_auth_page'].'</div>
			<form action="'.get_field('private_auth_page', 'option').'" method="post">
				<div class="wrap-input">
					<input type="mail" placeholder="E-Mail" name="authusrmail" />
				</div>	
				<div class="wrap-input">
					<input type="password" placeholder="" name="authusrpswdin" />
				</div>	
				<input type="submit" class="btn-anchor" value="Connexion">
			</form>';
	return $res;
}
//===


// register form for pages templates
function getSiteRegisterForm(){
	$usrID        = get_current_user_id();
	$user_info    = wp_get_current_user();
	$regsex       = get_user_meta($usrID, 'regsex',       true);
	$first_name   = get_user_meta($usrID, 'first_name',   true);
	$last_name    = get_user_meta($usrID, 'last_name',    true);
	$regaddr      = get_user_meta($usrID, 'regaddr',      true);
	$regpostcode  = get_user_meta($usrID, 'regpostcode',  true);
	
	$res='  <label for="regemail">Email:</label>
			<input id="regemail"   type="email"  name="regemail"   '.(is_user_logged_in() ?'readonly value="'.$user_info->data->user_email.'"' :'').'>
			
			<input id="regsex_1" type="radio" name="regsex" value="m"  '.($regsex=='m' || $regsex=='' ?'checked' :'').'>
			<label for="regsex_1">M</label>
			<input id="regsex_2" type="radio" name="regsex" value="ml" '.($regsex=='ml' ?'checked' :'').'>
			<label for="regsex_2">Male</label>	
			
			<label for="first_name">First name:</label>
			<input id="first_name" type="text" name="first_name" value="'.$first_name.'">
			<span class="icon-checkmark"></span>

			<label for="last_name">Last name:</label>
			<input id="last_name" type="text"  name="last_name" value="'.$last_name.'">
			<span class="icon-checkmark"></span>


			<label for="regaddr">Address:</label>
			<input id="regaddr" type="text" name="regaddr" value="'.$regaddr.'">

			<label for="regpostcode">Post:</label>
			<input id="regpostcode" type="text" name="regpostcode" value="'.$regpostcode.'">

			<label for="regpass">Password :</label>
			<input id="regpass" type="password" name="regpass">

			<label for="regpass_c">Confirmation :</label>
			<input id="regpass_c" type="password" name="regpass_c">';
	return $res;
}
//===


