<?php get_header() ?>

<div class="container">
	<div class="row">
		<div class="col">
			<h3><?php the_archive_title() ?></h3>
		</div>
	</div>
</div>
<section class="container">
	<div class="row">
		<?php if ( have_posts() ){ ?>
			<?php while(have_posts()){
				the_post(); ?>
				<?php get_template_part('template_part/list_item') ?>
			<?php } ?>
			<?php get_template_part('template_part/pager') ?>
		<?php }else{ ?>
			<?php get_template_part('template_part/not_found') ?>
		<?php } ?>
	</div>
</section>

<?php get_footer() ?>