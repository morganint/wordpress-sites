<section class="location">
	<div class="container">
		<div class="location__title">
			<h2><?php the_field('hom_b5_title') ?></h2>
			<?php the_field('hom_b5_stitle') ?>
		</div>
        
		<div class="location__search">
			<form class="search search--reference" action="<?php the_field('opt_orderinfo_page_url', 'option') ?>" method="post" id="sord_form">
				<div class="inputs">
                <input type="submit" name="sord_sbmt"   class="search__submit" value="" disabled/>
				<input type="text"   name="sord_number" placeholder="Search / Reference Number " class="search__input" />
                </div>
                <div class="sorted_btn">
                <button class="btn sord_sbmt_but" disabled><?php _e('Find', 'umove') ?></button>
                </div>    
			</form>
			<!--<button class="btn sord_sbmt_but"><?php _e('Find', 'umove') ?></button>-->
		</div>
	</div>
</section>