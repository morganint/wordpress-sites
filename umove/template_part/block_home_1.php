<?php
if(!wp_is_mobile()){
	$img = wp_get_attachment_image_src(get_field('hom_b1_img'), 'thumbnail_1920x970');
}else{
	$img = wp_get_attachment_image_src(get_field('hom_b1_img'), 'thumbnail_1920x970_m');
} ?>
<section class="hero" style="background-image: url('<?php echo(isset($img[0]) ?$img[0] :get_template_directory_uri().'/_slicing/data/img/header.jpg') ?>');">
	<div class="wrapper"></div>
	<div class="container">
		<div class="hero__content">
			<div class="hero__text-box">
				<h1><?php the_field('hom_b1_title') ?></h1>
				<?php the_field('hom_b1_stitle') ?>
				<a class="btn btn-mobile-form" id="SubmTransport_2" href="#uID">
					<?php the_field('hom_b1_f1_subm') ?>
				</a>
			</div>
			<div class="quote-form">
				<span class="quote-form-close">
					<svg xmlns="http://www.w3.org/2000/svg" width="24" height="24" viewBox="0 0 24 24"><g fill="#FFF" fill-rule="evenodd"><path d="M2.1.686L23.315 21.9 21.9 23.315.686 2.1z"/><path d="M.686 21.9L21.9.685 23.315 2.1 2.1 23.314z"/></g></svg>
				</span>
				<form method="post" action="<?php the_field('opt_tsp_spep_1_page', 'option') ?>" id="FormTransport_1" class="FormTransportAction" data-thanks="<?php echo htmlspecialchars(get_field('hom_b1_f1_thanks', 'option')) ?>">
					<h3 class="quote-form__title"><?php the_field('hom_b1_f1_title', 'option') ?></h3>
					<div class="quote-form__list">
						<div class="quote-form__items">
							<div class="quote-form__item">
								<input data-city-select type="text" name="st_from" data-err="<?php _e('Plese enter from!', 'umove') ?>" placeholder="<?php echo htmlspecialchars(get_field('hom_b1_f1_from', 'option')) ?>" />
							</div>
							<div class="quote-form__item">
								<input data-city-select type="text" name="st_to"   data-err="<?php _e('Plese enter to!', 'umove') ?>" placeholder="<?php echo htmlspecialchars(get_field('hom_b1_f1_to', 'option')) ?>" />
							</div>
						</div>
						<div class="quote-form__items">
							<div class="quote-form__item">
								<select class="select-time" name="st_day" data-err="<?php _e('Plese select day!', 'umove') ?>"><?php
									$i=0;
									while(have_rows('hom_b1_f1_when_list', 'option')){
										the_row();
										$txt = htmlspecialchars(get_sub_field('hom_b1_f1_when_list_title', 'option')); ?>
										<option <?php echo($i++==0 ?'data-display="'.$txt.'" value=""' :'value="'.$txt.'"') ?>><?php echo $txt ?></option><?php
									} ?>
								</select>
							</div>
							<div class="quote-form__item">
								<select class="select-size" name="st_size" data-err="<?php _e('Plese select size!', 'umove') ?>"><?php
									$i=0;
									while(have_rows('hom_b1_f1_size_list', 'option')){
										the_row();
										$txt = htmlspecialchars(get_sub_field('hom_b1_f1_size_list_title', 'option')); ?>
										<option <?php echo($i++==0 ?'data-display="'.$txt.'" value=""' :'value="'.$txt.'"') ?>><?php echo $txt ?></option><?php
									} ?>
								</select>
							</div>
						</div>
						<div class="quote-form__items">
							<div class="quote-form__item">
								<input type="text" name="st_f1_fullname" data-err="<?php _e('Plese enter full name!', 'umove') ?>" placeholder="<?php echo htmlspecialchars(get_field('st_f1_fullname', 'option')) ?>" required />
							</div>
							<div class="quote-form__item">
								<input type="email" name="st_f1_eml" pattern="[a-z0-9._%+-]+@[a-z0-9.-]+\.[a-z]{2,}$" data-err="<?php _e('Plese enter a valid email!', 'umove') ?>" placeholder="<?php echo htmlspecialchars(get_field('hom_b1_f1_eml', 'option')) ?>" required />
							</div>
							<div class="quote-form__item">
								<input type="text" name="st_f1_phone" data-err="<?php _e('Plese enter phone number!', 'umove') ?>" placeholder="<?php echo htmlspecialchars(get_field('st_f1_phone_number', 'option')) ?>" required />
							</div>
						</div>
					</div>
					<div class="MssageTransportBox"></div>
					<a class="btn" id="SubmTransport_1" href="#uID">
						<?php the_field('hom_b1_f1_subm') ?>
					</a>
				</form>
			</div>
		</div>
	</div>
</section>