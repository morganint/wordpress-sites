<section class="features">
	<div class="container">
		<h2><?php the_field('hom_b2_title') ?></h2>
		<div class="features__box ">
			<div class="features__list accordion"><?php
				while(have_rows('hom_b2_list')){
					the_row(); ?>
					<h4 class="trigger"><?php the_sub_field('hom_b2_list_title') ?></h4>
					<div class="inner">
						<?php the_sub_field('hom_b2_list_content') ?>
					</div><?php 
				} ?>
			</div>
			<div class="features__image"><?php
				$img = wp_get_attachment_image_src(get_field('hom_b2_img'), 'thumbnail_752x678');
				$image_alt = get_post_meta(get_field('hom_b2_img'), '_wp_attachment_image_alt', true); ?>
				<img src="<?php echo(isset($img[0]) ?$img[0] :get_template_directory_uri().'/_slicing/data/img/bitmap-2.png') ?>" alt="<?php echo($image_alt ?htmlspecialchars($image_alt) :htmlspecialchars(get_the_title())) ?>">
			</div>
		</div>
	</div>
</section>