<section class="quote-form quote-form--dark">
	<div class="container">
		<form method="post" action="<?php the_field('opt_tsp_spep_1_page', 'option') ?>" id="FormTransport_2" class="FormTransportAction" data-thanks="<?php echo htmlspecialchars(get_field('hom_b1_f1_thanks', 'option')) ?>">
			<h2 class="quote-form__title"><?php the_field('hom_b1_f1_title', 'option') ?></h2>
			<div class="quote-form__list">
			<div class="quote-form__items">
					<div class="quote-form__item">
						<input type="text" name="st_f1_fullname" data-align='center' data-err="<?php _e('Plese enter full name!', 'umove') ?>" placeholder="<?php echo htmlspecialchars(get_field('st_f1_fullname', 'option')) ?>" required />
					</div>
				</div>
				<div class="quote-form__items">
					<div class="quote-form__item">
						<input type="email" name="st_f1_eml" pattern="[a-z0-9._%+-]+@[a-z0-9.-]+\.[a-z]{2,}$" data-err="<?php _e('Plese enter a valid email!', 'umove') ?>" placeholder="<?php echo htmlspecialchars(get_field('hom_b1_f1_eml', 'option')) ?>" required />
					</div>
					<div class="quote-form__item">
						<input type="text" name="st_f1_phone" data-err="<?php _e('Plese enter phone number!', 'umove') ?>" placeholder="<?php echo htmlspecialchars(get_field('st_f1_phone_number', 'option')) ?>" required />
					</div>
				</div>
				<div class="quote-form__items">
					<div class="quote-form__item">
						<input data-city-select type="text" name="st_from" data-err="<?php _e('Plese enter from!', 'umove') ?>" placeholder="<?php the_field('hom_b1_f1_from', 'option') ?>" />
					</div>
					<div class="quote-form__item">
						<input data-city-select type="text" name="st_to"   data-err="<?php _e('Plese enter to!', 'umove') ?>"   placeholder="<?php the_field('hom_b1_f1_to', 'option') ?>" />
					</div>
				</div>
				<div class="quote-form__items">
					<div class="quote-form__item">
						<select class="select-time" name="st_day" data-err="<?php _e('Plese select day!', 'umove') ?>"><?php
							$i=0;
							while(have_rows('hom_b1_f1_when_list', 'option')){
								the_row();
								$txt = htmlspecialchars(get_sub_field('hom_b1_f1_when_list_title', 'option')); ?>
								<option <?php echo($i++==0 ?'data-display="'.$txt.'" value=""' :'value="'.$txt.'"') ?>><?php echo $txt ?></option><?php
							} ?>
						</select>
					</div>
					<div class="quote-form__item">
						<select class="select-size" name="st_size" data-err="<?php _e('Plese select size!', 'umove') ?>"><?php
							$i=0;
							while(have_rows('hom_b1_f1_size_list', 'option')){
								the_row();
								$txt = htmlspecialchars(get_sub_field('hom_b1_f1_size_list_title', 'option')); ?>
								<option <?php echo($i++==0 ?'data-display="'.$txt.'" value=""' :'value="'.$txt.'"') ?>><?php echo $txt ?></option><?php
							} ?>
						</select>
					</div>
				</div>				
			</div>
			<div class="MssageTransportBox"></div>
			<a class="btn" id="SubmTransport_2" class="SubmTransportAction" href="#uID1">
				<?php the_field('hom_b1_f1_subm',2) ?>
			</a>
		</form>
	</div>
</section>