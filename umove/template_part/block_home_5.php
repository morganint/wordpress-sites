<section class="stages">
	<div class="container">
		<h2><?php the_field('hom_b4_title') ?></h2>
		<div class="stages__box">
			<div class="stages__list">
				<div class="stages__items accordion"><?php
					$i=0;
					while(have_rows('hom_b4_list')){
						the_row(); ?>
						<h4 class="trigger"><span><?php echo(++$i<10 ?'0'.$i :$i) ?></span> <?php the_sub_field('hom_b4_list_title') ?></h4>
						<div class="inner">
							<?php the_sub_field('hom_b4_list_content') ?>
						</div><?php 
					} ?>
				</div>
			</div>
			<div class="stages__image"><?php
				$img = wp_get_attachment_image_src(get_field('hom_b4_img'), 'thumbnail_501x841');
				$image_alt = get_post_meta(get_field('hom_b4_img'), '_wp_attachment_image_alt', true); ?>
				<img src="<?php echo(isset($img[0]) ?$img[0] :get_template_directory_uri().'/_slicing/data/img/delivery-courier.png') ?>" alt="<?php echo($image_alt ?htmlspecialchars($image_alt) :htmlspecialchars(get_the_title())) ?>" width="501px" height="841px">
			</div>
		</div>
	</div>
</section>