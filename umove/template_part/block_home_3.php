<section class="advantages">
	<div class="container">
		<h2><?php the_field('hom_b3_title') ?></h2>
		<?php the_field('hom_b3_stitle') ?>
		<div class="advantages__list"><?php
			$i=0;
			while(have_rows('hom_b3_list')){
				the_row(); ?>
				<?php if(!wp_is_mobile()){
					$img = wp_get_attachment_image_src(get_post_thumbnail_id(), 'thumbnail_1920x1080');
				}else{
					$img = wp_get_attachment_image_src(get_post_thumbnail_id(), 'thumbnail_800x600');
				}
				$alt = htmlspecialchars(get_post_meta(get_post_thumbnail_id(), '_wp_attachment_image_alt', true)); ?>
				<?php if($i++ % 2){ ?>
					<div class="advantages__item">
						<div class="advantages__text-box">
							<h3><?php the_sub_field('hom_b3_list_title'); ?></h3>
							<?php the_sub_field('hom_b3_list_stitle'); ?>
						</div>
						<div class="advantages__image"><?php
							$img = wp_get_attachment_image_src(get_sub_field('hom_b3_list_img'), 'thumbnail_800x600');
							$alt = htmlspecialchars(get_post_meta(get_sub_field('hom_b3_list_img'), '_wp_attachment_image_alt', true)); ?>
							<img src="<?php echo(isset($img[0]) ?$img[0] :get_template_directory_uri().'/_slicing/data/img/bitmap.png') ?>" alt="<?php echo($alt ?$alt :htmlspecialchars(get_the_title())) ?>">
						</div>
					</div>
				<?php }else{ ?>
					<div class="advantages__item">
						<div class="advantages__image"><?php
							$img = wp_get_attachment_image_src(get_sub_field('hom_b3_list_img'), 'thumbnail_800x600');
							$alt = htmlspecialchars(get_post_meta(get_sub_field('hom_b3_list_img'), '_wp_attachment_image_alt', true)); ?>
							<img src="<?php echo(isset($img[0]) ?$img[0] :get_template_directory_uri().'/_slicing/data/img/bitmap.png') ?>" alt="<?php echo($alt ?$alt :htmlspecialchars(get_the_title())) ?>">
						</div>
						<div class="advantages__text-box">
							<h3><?php the_sub_field('hom_b3_list_title'); ?></h3>
							<?php the_sub_field('hom_b3_list_stitle'); ?>
						</div>
					</div><?php
				}
			} ?>
		</div>
	</div>
</section>