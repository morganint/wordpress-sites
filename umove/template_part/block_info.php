<section class="service service--fill">
	<div class="consultant">
		<h3><?php the_field('stp1p_b5_title', 'option') ?></h3>
		<div class="consultant__contact">
			<?php the_field('stp1p_b5_content_1', 'option') ?>
			<div class="consultant__logo">
				<?php the_field('stp1p_b5_content_2', 'option') ?>
			</div>
		</div>
		<div class="consultant__text">
			<?php the_field('stp1p_b5_content_3', 'option') ?>
		</div>
	</div>
</section>