<div <?php post_class('col-12 col-xl-6') ?> id="post-<?php the_ID() ?>">
	<div class="my-blog__block">
		<div class="csm100">
			<a href="<?php the_permalink() ?>"><?php
				$img = wp_get_attachment_image_src(get_post_thumbnail_id(), 'thumbnail_400x250');
				$image_alt = get_post_meta(get_post_thumbnail_id(), '_wp_attachment_image_alt', true); ?>
				<?php if(isset($img[0])){ ?>
					<img src="<?php echo(isset($img[0]) ?$img[0] :'') ?>" alt="<?php echo($image_alt ?htmlspecialchars($image_alt) :htmlspecialchars(get_the_title())) ?>" class="img-responsive">
				<?php }else{ ?>
					<img src="<?php echo get_template_directory_uri() ?>/img/noimage.png" alt="no image use" class="img-responsive">
				<?php } ?>
			</a>
		</div>
		<div class="csm100">
			<a href="<?php the_permalink() ?>">
				<h3><?php the_title() ?></h3>
			</a>
			<?php the_excerpt() ?>
			<p class="postTime">
				<?php echo __('Date', 'umove').':&nbsp;'.get_the_time('d/M/Y').'.&nbsp;&nbsp;&nbsp;'.__('Author', 'umove').':&nbsp;'.get_the_author_meta('first_name').'&nbsp;'.get_the_author_meta('last_name').'.' ?>
			</p>
		</div>
	</div>
</div>