(function(){function r(e,n,t){function o(i,f){if(!n[i]){if(!e[i]){var c="function"==typeof require&&require;if(!f&&c)return c(i,!0);if(u)return u(i,!0);var a=new Error("Cannot find module '"+i+"'");throw a.code="MODULE_NOT_FOUND",a}var p=n[i]={exports:{}};e[i][0].call(p.exports,function(r){var n=e[i][1][r];return o(n||r)},p,p.exports,r,e,n,t)}return n[i].exports}for(var u="function"==typeof require&&require,i=0;i<t.length;i++)o(t[i]);return o}return r})()({1:[function(require,module,exports){
"use strict";

$(document).ready(function () {
  $(".accordion>.inner").not(":first-of-type").hide();
  $(".accordion").find(".trigger:first").addClass("active");
  $(".accordion>.trigger").on("click", function () {
    var $this = $(this);
    var findAccordion = $this.closest(".accordion");

    if (!$this.hasClass("active")) {
      findAccordion.find(">.inner").stop().slideUp(200);
      findAccordion.find(">.trigger").removeClass("active");
      $this.addClass("active").next().stop().slideDown(200);
    }
  });
  
  if ($(window).width() < 992) {
    $(".advices__item").find(".content").not(":first").hide();
  }

  ;
  $(".advices__item > h4").on("click", function () {
    if ($(this).hasClass("active")) {
      $(this).removeClass("active");
      $(this).siblings(".content").slideUp(200);
    } else {
      $(".advices__item > h4").removeClass("active");
      $(this).addClass("active"); // $(".content").slideUp(200);

      $(this).siblings(".content").slideDown(200);
    }
  });
});

},{}],2:[function(require,module,exports){
"use strict";

$(document).ready(function () {
  if ($(window).width() < 768) {
    $('.reserve__item-title').click(function () {
      $(this).closest('.reserve__item').find('.reserve__item-in').slideToggle();
      $(this).closest('.reserve__item').toggleClass('active');
    });
    $('.btn-mobile-reserve').click(function (e) {
      e.preventDefault();
      $('.btn-mobile-reserve').toggleClass('active');
      $('.reserve-right').slideToggle();
    });
  }

  $('.reserve-right .tracks-form__title').click(function () {
    $(this).closest('.tracks-form__item').find('.tracks-form-in').slideToggle();
    $(this).closest('.tracks-form__item').toggleClass('active');
  });



  if($('.reserve-right').length) {

    var aTop = $('.reserve-right').offset().top;
    var aService = $('.service').offset().top;
    var aService = $('.service').offset().top;
    var aLeft = $('.reserve-right').offset().left;
    var aWidth = $('.reserve-right').outerWidth();

    if ($(window).width() > 1024) {
      if(($(this).scrollTop() + 80) >= aTop){
        var aLeft = $('.reserve-right').offset().left;
        $('.reserve-right').css({"left": ""+ aLeft +"px", "right": "auto",  "width": ""+ aWidth +"", "position": "fixed", "top": "50px", "bottom": "auto"});
   
        if(($(this).scrollTop() + $(window).height()) >= aService) {
          var aLeft = $('.reserve-right').offset().left;
          $('.reserve-right').css({"left": "auto", "right": "0", "width": ""+ aWidth +"", "position": "absolute", "top": "auto",  "bottom": "0"});
        
        }
      } else {
        // $('.reserve-right').removeClass('fixed');
        $('.reserve-right').css({"left": ""+ aLeft +"px", "width": ""+ aWidth +"", "position": "static", "top": "50px"});
      }
    }

    $(window).scroll(function(){
      if ($(window).width() > 1024) {
        if(($(this).scrollTop() + 80) >= aTop){
          var aLeft = $('.reserve-right').offset().left;
          $('.reserve-right').css({"left": ""+ aLeft +"px", "right": "auto",  "width": ""+ aWidth +"", "position": "fixed", "top": "50px", "bottom": "auto"});
     
          if(($(this).scrollTop() + $(window).height()) >= aService) {
            var aLeft = $('.reserve-right').offset().left;
            $('.reserve-right').css({"left": "auto", "right": "0", "width": ""+ aWidth +"", "position": "absolute", "top": "auto",  "bottom": "0"});
          
          }
        } else {
          // $('.reserve-right').removeClass('fixed');
          $('.reserve-right').css({"left": ""+ aLeft +"px", "width": ""+ aWidth +"", "position": "static", "top": "50px"});
        }
      }
    
     
    });
  }



});

},{}],3:[function(require,module,exports){
"use strict";

(function () {
  var options = {
    types: ['(cities)'],
    componentRestrictions: {
      country: "us"
    }
  };
  $('[data-city-select]').each(function () {
    new google.maps.places.Autocomplete(this, options);
  });
})();

},{}],4:[function(require,module,exports){
"use strict";

(function ($) {
  $(function () {
    if ($('.quote1 .quote-trans__car').length) {
      $('.quote1 .quote-trans__car').slick({
        slidesToShow: 1,
        slidesToScroll: 1,
        arrows: false,
        fade: true,
        asNavFor: '.quote1 .quote-trans__car-nav',
        responsive: [{
          breakpoint: 767,
          settings: {
            dots: true
          }
        }]
      });
      $('.quote1 .quote-trans__car-nav').slick({
        slidesToShow: 4,
        slidesToScroll: 1,
        asNavFor: '.quote1 .quote-trans__car',
        dots: false,
        centerMode: true,
        focusOnSelect: true
      });
    }
  });
})(jQuery);

},{}],5:[function(require,module,exports){
"use strict";

(function () {
  // $(".popup-with-form").magnificPopup({
  //   type: "inline",
  //   preloader: false,
  // });
  $('.btn-mobile-form').click(function (e) {
    e.preventDefault();
    $('.hero .quote-form').addClass('active');
    $('body').addClass('open-form');
  });
  $('.quote-form-close').click(function (e) {
    e.preventDefault();
    $('.hero .quote-form').removeClass('active');
    $('body').removeClass('open-form');
  });
})();

},{}],6:[function(require,module,exports){
"use strict";

$(document).ready(function () {
  if ($('#txtSpotDate').length) {
    $('#txtSpotDate').datepicker({
      minDate: 0
    });
  }

  $('.reserve-delivery__date .reserve-change').click(function (e) {
    e.preventDefault();
    $('#txtSpotDate').datepicker('show');
  }); //contact information



  if($('#txtEmailAddress').length) {
    var emailAddress = $('#txtEmailAddress').val();

    if (emailAddress.length) {
      $('#summary-cart-contact-info-region').text(emailAddress);
      $('#summary-cart-contact-info-region').removeClass('hidden');
    } else {
      $('#summary-cart-contact-info-region').addClass('hidden');
    }
  }
  $('#txtEmailAddress').on('change paste keyup', function () {
    var emailAddress = $(this).val();

    if (emailAddress.length) {
      $('#summary-cart-contact-info-region').text(emailAddress);
      $('#summary-cart-contact-info-region').removeClass('hidden');
    } else {
      $('#summary-cart-contact-info-region').addClass('hidden');
    }
  }); //moving from
  if($('#txtPhone').length) {
    var emailAddress = $('#txtPhone').val();

    if (emailAddress.length) {
      $('#summary-cart-contact-info-region3').text(emailAddress);
      $('#summary-cart-contact-info-region3').removeClass('hidden');
    } else {
      $('#summary-cart-contact-info-region3').addClass('hidden');
    }
  }
  $('#txtPhone').on('change paste keyup', function () {
    var emailAddress = $(this).val();

    if (emailAddress.length) {
      $('#summary-cart-contact-info-region3').text(emailAddress);
      $('#summary-cart-contact-info-region3').removeClass('hidden');
    } else {
      $('#summary-cart-contact-info-region3').addClass('hidden');
    }
  }); //moving from

  if($('#txtFullName').length) {
    var emailAddress = $('#txtFullName').val();

    if (emailAddress.length) {
      $('#summary-cart-contact-info-region2').text(emailAddress);
      $('#summary-cart-contact-info-region2').removeClass('hidden');
    } else {
      $('#summary-cart-contact-info-region2').addClass('hidden');
    }
  }
  $('#txtFullName').on('change paste keyup', function () {
    var emailAddress = $(this).val();

    if (emailAddress.length) {
      $('#summary-cart-contact-info-region2').text(emailAddress);
      $('#summary-cart-contact-info-region2').removeClass('hidden');
    } else {
      $('#summary-cart-contact-info-region2').addClass('hidden');
    }
  }); //moving from


  if($('#txtOriginCity').length) {
    var text = $('#txtOriginCity').val();
    var info = $('#summary-cart-moving-from-region .mov-1');
    if (text.length) {
      $(info).text(text + ',  ');
      $(info).removeClass('hidden');
    } else {
      $(info).addClass('hidden');
    }
  }
  if($('#selOriginState').length) {
    var text = $('#selOriginState').val();
    var info = $('#summary-cart-moving-from-region .mov-2');
    if (text.length > 1) {
      $(info).text(text + ',  ');
      $(info).removeClass('hidden');
    } else {
      $(info).addClass('hidden');
    }
  }
  if($('#txtOriginZip').length) {
    var text = $('#txtOriginZip').val();
    var info = $('#summary-cart-moving-from-region .mov-3');
    if (text.length) {
      $(info).text(text + ',  ');
      $(info).removeClass('hidden');
    } else {
      $(info).addClass('hidden');
    }
  }
  if($('#txtDestinationCity').length) {
    var text = $('#txtDestinationCity').val();
    var info = $('#summary-cart-moving-to-region .mov-1');
    if (text.length) {
      $(info).text(text + ',  ');
      $(info).removeClass('hidden');
    } else {
      $(info).addClass('hidden');
    }
  }
  if($('#selDestinationState').length) {
    var text = $('#selDestinationState').val();
    var info = $('#summary-cart-moving-to-region .mov-2');
    if (text.length > 1) {
      $(info).text(text + ',  ');
      $(info).removeClass('hidden');
    } else {
      $(info).addClass('hidden');
    }
  }
  if($('#txtDestinationZip').length) {
    var text = $('#txtDestinationZip').val();
    var info = $('#summary-cart-moving-to-region .mov-3');
    if (text.length) {
      $(info).text(text + ',  ');
      $(info).removeClass('hidden');
    } else {
      $(info).addClass('hidden');
    }
  }
  if($('#txtOriginAddress').length) {
    var text = $('#txtOriginZip').val();
    var info = $('#summary-cart-moving-from-region .mov-0');
    if (text.length) {
      $(info).text(text + ',  ');
      $(info).removeClass('hidden');
    } else {
      $(info).addClass('hidden');
    }
  }
  if($('#txtDestinationAddress').length) {
    var text = $('#txtDestinationAddress').val();
    var info = $('#summary-cart-moving-to-region .mov-0');
    if (text.length) {
      $(info).text(text + ',  ');
      $(info).removeClass('hidden');
    } else {
      $(info).addClass('hidden');
    }
  }











  $('#txtOriginCity').on('change paste keyup', function () {
    var originCity = $(this).val();
    var info = $('#summary-cart-moving-from-region .mov-1');

    if (originCity.length) {
      $(info).text(originCity + ',  ');
      $(info).removeClass('hidden');
    } else {
      $(info).addClass('hidden');
    }
  });
  $('#txtOriginAddress').on('change paste keyup', function () {
    var originCity = $(this).val();
    var info = $('#summary-cart-moving-from-region .mov-0');

    if (originCity.length) {
      $(info).text(originCity + ',  ');
      $(info).removeClass('hidden');
    } else {
      $(info).addClass('hidden');
    }
  });


  $('#selOriginState').on('change', function () {
    var originState = $(this).val();
    var info = $('#summary-cart-moving-from-region .mov-2');

    if (originState != 0) {
      $(info).text(originState + '  ');
      $(info).removeClass('hidden');
    } else {
      $(info).addClass('hidden');
    }
  });


  $('#txtOriginZip').on('change paste keyup', function () {
    var originZip = $(this).val();
    var info = $('#summary-cart-moving-from-region .mov-3');

    if (originZip.length) {
      $(info).text(originZip);
      $(info).removeClass('hidden');
    } else {
      $(info).addClass('hidden');
    }
  }); //moving to

  $('#txtDestinationAddress').on('change paste keyup', function () {
    var originCity = $(this).val();
    var info = $('#summary-cart-moving-to-region .mov-0');

    if (originCity.length) {
      $(info).text(originCity + ',  ');
      $(info).removeClass('hidden');
    } else {
      $(info).addClass('hidden');
    }
  });
  $('#txtDestinationCity').on('change paste keyup', function () {
    var originCity = $(this).val();
    var info = $('#summary-cart-moving-to-region .mov-1');

    if (originCity.length) {
      $(info).text(originCity + ',  ');
      $(info).removeClass('hidden');
    } else {
      $(info).addClass('hidden');
    }
  });
  $('#selDestinationState').on('change', function () {
    var originState = $(this).val();
    var info = $('#summary-cart-moving-to-region .mov-2');

    if (originState != 0) {
      $(info).text(originState + '  ');
      $(info).removeClass('hidden');
    } else {
      $(info).addClass('hidden');
    }
  });
  $('#txtDestinationZip').on('change paste keyup', function () {
    var originZip = $(this).val();
    var info = $('#summary-cart-moving-to-region .mov-3');

    if (originZip.length) {
      $(info).text(originZip);
      $(info).removeClass('hidden');
    } else {
      $(info).addClass('hidden');
    }
  }); //date

  $('#txtSpotDate').on('change paste keyup', function () {
    var origin = $(this).val();
    var info = $('#summary-cart-spot-date-line-item');

    if (origin.length) {
      $(info).text(origin);
      $(info).removeClass('hidden');
    } else {
      $(info).addClass('hidden');
    }
  }); //payment

  $('#txtCreditCardName').on('change paste keyup', function () {
    var originCity = $(this).val();
    var info = $('#summary-payment-name');

    if (originCity.length) {
      $(info).text(originCity);
      $(info).removeClass('hidden');
    } else {
      $(info).addClass('hidden');
    }
  });
  $('#selCreditCardExpirationMonth, #selCreditCardExpirationYear').on('change', function () {
    var originM = $('#selCreditCardExpirationMonth').val();
    var originY = $("#selCreditCardExpirationYear").val();
    var info = $('#summary-expires-date');

    if (originM != 0 && originY != 0) {
      $(info).text('Expires ' + originM + ' / ' + originY);
      $(info).removeClass('hidden');
    } else {
      $(info).addClass('hidden');
    }
  });

  // function additionalOptions() {
  //   var origin = $('#selStorage').val();
  //   var origin1 = $('#selAdditionalLiability').val();
  //   var origin2 = $('#selWeightTickets').val();

  //   if (origin != 0 || origin1 != 0 || origin2 != 0) {
  //     $('#monthly-storage').closest('.tracks-form__element').find('input').attr('checked', 'checked');
  //   } else {
  //     $('#monthly-storage').closest('.tracks-form__element').find('input').removeAttr('checked');
  //   }
  // } 
  function additionalOptions() {
    $('#edditional-options').prop('checked', true);
  }

  //more options

  $('#edditional-options').on('change', function (){
    var total = $('#total-price').val();
    var origin = $('#monthly-storage span').text().substring(1);
    var origin1 = $('#liability-coverage span').text().substring(1);
    var origin2 = $('#weight-tickets span').text().substring(1);

    if ($('#edditional-options:not(:checked)')){
      var newTotal = (total - origin - origin1 - origin2).toFixed(2);
      $('#monthly-storage span').text('$');
      $('#liability-coverage span').text('$');
      $('#weight-tickets span').text('$')
      $('#monthly-storage').addClass('hidden');
      $('#liability-coverage').addClass('hidden');
      $('#weight-tickets').addClass('hidden');
      $('#total-price').val(newTotal);
      priceLength();
    } 
})

  $('#selStorage').on('change', function () {
    var origin = +$(this).val();
    var info = $('#monthly-storage span');
    var total = +$('#total-price').val();
    var oldPrice = $('#monthly-storage span').text().substring(1);
    var oldPriceLength = oldPrice.length;

    if (origin != 0) {
      $(info).text('$' + origin);
      $('#monthly-storage').removeClass('hidden');

      if (oldPriceLength > 0) {
        origin = origin - oldPrice;
      }
      var newTotal = (total + origin).toFixed(2);
      $('#total-price').val(newTotal);
      priceLength();
    } else {
      $('#monthly-storage').addClass('hidden');

      if (oldPriceLength > 0) {
        $(info).text('$');
        var newTotal = (total - oldPrice).toFixed(2);
        $('#total-price').val(newTotal);
        priceLength();
      }    
    }
    $('#edditional-options').is(':checked')
    additionalOptions();
  });

  $('#selAdditionalLiability').on('change', function () {
    var origin = +$(this).val();
    var info = $('#liability-coverage span');
    var total = +$('#total-price').val();
    var oldPrice = $('#liability-coverage span').text().substring(1);
    var oldPriceLength = oldPrice.length;

    if (origin != 0) {
      $(info).text('$' + origin);
      $('#liability-coverage').removeClass('hidden');

      if (oldPriceLength > 0) {
        origin = origin - oldPrice;
      }
      var newTotal = (total + origin).toFixed(2);
      $('#total-price').val(newTotal);
      priceLength();
    } else {
      $('#liability-coverage').addClass('hidden');

      if (oldPriceLength > 0) {
        $(info).text('$');
        var newTotal = (total - oldPrice).toFixed(2);
        $('#total-price').val(newTotal);
        priceLength();
      }
    }

    additionalOptions();
  });
  $('#selWeightTickets').on('change', function () {
    var origin = +$(this).val();
    var info = $('#weight-tickets span');
    var total = +$('#total-price').val();
    var oldPrice = $('#weight-tickets span').text().substring(1);
    var oldPriceLength = oldPrice.length;

    if (origin != 0) {
      $(info).text('$' + origin);
      $('#weight-tickets').removeClass('hidden');

      if (oldPriceLength > 0) {
        origin = origin - oldPrice;
      }
      var newTotal = (total + origin).toFixed(2);
      $('#total-price').val(newTotal);
      priceLength();
    } else {
      $('#weight-tickets').addClass('hidden');

      if (oldPriceLength > 0) {
        $(info).text('$');
        var newTotal = (total - oldPrice).toFixed(2);
        $('#total-price').val(newTotal);
        priceLength();
      }
    }

    additionalOptions();
  });



  $('.reserve-transit input[type="radio"]').on('change', function () {
    var origin = +$('.reserve-transit input:checked').val();
    var transitDay = +$('.reserve-transit input:checked').attr('data-date');
    var estimTransit = $('#summary-cart-transit-estimate');
    var guarantTransit = $('#summary-cart-transit-guaranteed');
    var guarantDay = $('#guaranteed-day');
    var info = $('#guaranteed-price');
    var total = +$('#total-price').val();
    var oldPrice = +$('#guaranteed-price').text().slice(1);
    var oldPriceLength = oldPrice.length;
    var transportation = $('#total-price').data('val')
    var transportationNew = (parseFloat(transportation) + origin).toFixed(2)
     $('#total-p').text(transportationNew);

    if (origin != 0) {
      $(info).text('$' + origin);
      $(estimTransit).hide();
      $(guarantTransit).css('display', 'flex');

      if (oldPrice > 0) {
        origin = origin - oldPrice;
      }

      var newTotal = (total + origin).toFixed(2);
      // newTotal = String(newTotal);
      // var totalModify = newTotal.replace(/(\d)(?=(\d{3})+(\D|$))/g, '$1,');
      $('#total-price').val(newTotal);
      priceLength();
      $(guarantDay).text(transitDay);
    } else {
      $(estimTransit).show();
      $(guarantTransit).hide();

      if (oldPrice > 0) {
        $(info).text('$');
        var newTotal = (total - oldPrice).toFixed(2);
        // newTotal = String(newTotal);
        // var totalModify = newTotal.replace(/(\d)(?=(\d{3})+(\D|$))/g, '$1,');
        $('#total-price').val(newTotal);
        priceLength();
      }
    }
  });

  
  // $('#servLoading0').click(function(e){
  //   e.preventDefault();
  // });

  if( $('.step-1 #st_cartype0').length) {
    var total = +$('#st_cartype0').data('val');
    // var total = + $('#servLoading0').data('val');
    // $('#total-price').val(total);
    var newTotal = total;
    if ($('#servLoading0').is(':checked')) {
      var origin = +$('#servLoading0').data('val').toFixed(2);
      var newTotal = (newTotal + origin).toFixed(2);
    }
    if ($('#servLoading1').is(':checked')) {
      var origin = +$('#servLoading1').data('val');
      var newTotal = (newTotal + origin).toFixed(2);
    }
    if ($('#servLoading2').is(':checked')) {
      var origin2 = +$('#servLoading2').data('val');
      var newTotal = (newTotal + origin2).toFixed(2);
    }
    $('#total-price').data('val', newTotal);
    $('#total-price').val(newTotal);
    priceLength();
  }

  $('#servLoading0').on('change', function () {
    var origin = +$(this).data('val');
    var total = +$('#total-price').val();

    if ($('#servLoading0').is(':checked')) {
      var newTotal = (total + origin).toFixed(2);
      // newTotal = String(newTotal);
      // var totalModify = newTotal.replace(/(\d)(?=(\d{3})+(\D|$))/g, '$1,');
      $('#total-price').data('val', newTotal);
      $('#total-price').val(newTotal);
      priceLength()
    } else {
      var newTotal = (total - origin).toFixed(2);
      // newTotal = String(newTotal);
      // var totalModify = newTotal.replace(/(\d)(?=(\d{3})+(\D|$))/g, '$1,');
      $('#total-price').data('val', newTotal);
      $('#total-price').val(newTotal);
      priceLength()
    }
  });

  $('#servLoading1').on('change', function () {
    var origin = +$(this).data('val');
    var total = +$('#total-price').val();

    if ($('#servLoading1').is(':checked')) {
      var newTotal = (total + origin).toFixed(2);
      // newTotal = String(newTotal);
      // var totalModify = newTotal.replace(/(\d)(?=(\d{3})+(\D|$))/g, '$1,');
      $('#total-price').data('val', newTotal);
      $('#total-price').val(newTotal);
      priceLength()
    } else {
      var newTotal = (total - origin).toFixed(2);
      // newTotal = String(newTotal);
      // var totalModify = newTotal.replace(/(\d)(?=(\d{3})+(\D|$))/g, '$1,');
      $('#total-price').data('val', newTotal);
      $('#total-price').val(newTotal);
      priceLength()
    }
  });
  $('#servLoading2').on('change', function () {
    var origin = +$(this).data('val');
    var total = +$('#total-price').val();

    if ($('#servLoading2').is(':checked')) {
      var newTotal = (total + origin).toFixed(2);
      // newTotal = String(newTotal);
      // var totalModify = newTotal.replace(/(\d)(?=(\d{3})+(\D|$))/g, '$1,');
      $('#total-price').data('val', newTotal);
      $('#total-price').val(newTotal);
      priceLength()
    } else {
      var newTotal = (total - origin).toFixed(2);
      // newTotal = String(newTotal);
      // var totalModify = newTotal.replace(/(\d)(?=(\d{3})+(\D|$))/g, '$1,');
      $('#total-price').data('val', newTotal);
      $('#total-price').val(newTotal);
      priceLength()
    }
  });
  // $('#servUploading').on('change', function () {
  //   var origin = +$(this).val();
  //   var total = +$('#total-price').text().replace(',', '');

  //   if ($('#servUploading').is(':checked')) {
  //     var newTotal = total + origin;
  //     newTotal = String(newTotal);
  //     var totalModify = newTotal.replace(/(\d)(?=(\d{3})+(\D|$))/g, '$1,');
  //     $('#total-price').text(totalModify);
  //   } else {
  //     var newTotal = total - origin;
  //     newTotal = String(newTotal);
  //     var totalModify = newTotal.replace(/(\d)(?=(\d{3})+(\D|$))/g, '$1,');
  //     $('#total-price').text(totalModify);
  //   }
  // });
});


$('#st_cartype1, #st_cartype0').on('change', function () {
  var total = +$('#total-price').val();
  var firstCount = +$('#st_cartype0').data('val')
  var origin = +$('#st_cartype1').data('val');

  if ($('#st_cartype1').is(':checked')) {
    $('.quote-1').hide(); 
    $('.quote-2').show();
  var newTotal = (total + origin - firstCount).toFixed(2);
  $('#total-price').data('val', newTotal);
  $('#total-price').val(newTotal);
  priceLength()
  
 
  } else {
    $('.quote-2').hide();
    $('.quote-1').show();
    var newTotal = (total - origin + firstCount).toFixed(2);
    $('#total-price').data('val', newTotal);
    $('#total-price').val(newTotal);
    priceLength()
  }
});

if($('#total-price').length) {
  $('#rdoExpeditedOptions-1').val(0);
  $('#total-p').text($('#total-price').data('val'));

  $('#transportation').on('click', function(e){
   console.log($('#total-price').data('val'))
  })
  setTimeout(function(){
    var width = +$('#total-price').val().length + 1;
    $('#total-price').css('width', ''+ width * 13 +'px');
  }, 200);
}

var priceLength = function(){
  var width = +$('#total-price').val().length + 1;
  $('#total-price').css('width', ''+ width * 13 +'px');
}

$('#total-price').on('change', function () { 
  setTimeout(function(){
    var width = +$('#total-price').val().length + 1;
    $('#total-price').css('width', ''+ width * 13 +'px');
  }, 200);
});


if($('.reserve-transit').length) {
  $('#rdoExpeditedOptions-1').attr('checked','checked');
}





},{}],7:[function(require,module,exports){
"use strict";

(function () {
  var googleMapEle = document.getElementById("google-map");
  if (!googleMapEle) return; //   const hendersonBounds = {
  //     north: 36.159,
  //     south: 35.96,
  //     west: -115.38, 
  //     east: -114.66
  //   };

  function initMap() {
    var map = new google.maps.Map(googleMapEle, {
      draggable: false,
      zoom: 5,
      center: {
        lat: 35.83,
        lng: -98.58
      },
      disableDefaultUI: true,
      //   restriction: {
      //     latLngBounds: hendersonBounds
      //   },
      styles: []
    });
    var marker = new google.maps.Marker({
      position: {
        lat: 39.83,
        lng: -98.58
      },
      map: map,
      icon: '../img/map-icon.svg'
    });
  }

  initMap();
})();

},{}],8:[function(require,module,exports){
"use strict";

$(document).ready(function () {
  // Don't touch
  //objectFitImages();
  //svg4everybody();
  // End don't touch

  require('./target-blank.js');

  require('./menu.js');

  require('./menu-scroll.js');

  require('./select.js');

  require('./accordion.js');

  require('./form-popup.js');

  require('./google-map.js');

  require('./carousel.js');

  require('./accordion2.js');

  require('./form.js');

  require('./autocomplete.js');
}); // remove preloader

$(window).on('load', function () {
  $('.preloader').fadeOut();
});

},{"./accordion.js":1,"./accordion2.js":2,"./autocomplete.js":3,"./carousel.js":4,"./form-popup.js":5,"./form.js":6,"./google-map.js":7,"./menu-scroll.js":9,"./menu.js":10,"./select.js":11,"./target-blank.js":12}],9:[function(require,module,exports){
"use strict";

var header = $(".main-header"),
    scrollPrev = 0;
    $(window).scroll(function () {
      var scrolled = $(window).scrollTop();
    
      if (scrolled > 100 && scrolled > scrollPrev) {
        header.addClass("off");
        header.addClass("out");
      } else {
        header.removeClass("off");
      }
    
      scrollPrev = scrolled;
    
      if (scrollPrev < 10) {
        header.removeClass("out");
      }
    });

},{}],10:[function(require,module,exports){
"use strict";

(function ($) {
  $(function () {
    $(".menu-icon").on("click", function () {
      if ($("header nav").hasClass("main-nav--open")) {
        $('.main-header').removeClass("out");
        $(this).closest(".main-nav").toggleClass("main-nav--open");
      } else {
        $(this).closest(".main-nav").toggleClass("main-nav--open");
      }
    });
  });
})(jQuery);

},{}],11:[function(require,module,exports){
"use strict";

(function () {
  $('select').niceSelect({});
  $('b[role="presentation"]').hide();
})();

},{}],12:[function(require,module,exports){
"use strict";

// add target blank to external links
var siteUrl = window.location.hostname;
$('a[href*="//"]:not([href*="' + siteUrl + '"])').attr({
  target: '_blank',
  rel: 'noopener noreferrer'
});

},{}]},{},[8])

//# sourceMappingURL=bundle.js.map
