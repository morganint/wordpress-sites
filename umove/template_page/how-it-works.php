<?php 
/*Template Name: How it Works*/
get_header();?>
  <main>
    <?php $image = get_field('hero_image'); ?>  
    <section class="hero" style="background-image: url(<?php echo $image; ?>);">
      <div class="wrapper"></div>
      <div class="container">
        <div class="hero__content">
          <div class="hero__text-box ">
            <h1>
              <?php the_field('hero_title');?>
            </h1>
            <p>
              <?php the_field('hero_content', false, false);?>
            </p>
          </div>
          <?php get_template_part('template_part/quote') ?>
        </div>
      </div>
    </section>

    <section class="media">
    			<video class="video" playsinline controls  poster="https://yourmovetoday.com/wp-content/themes/umove/_slicing/data/media/poster.png">
      				<source src="https://yourmovetoday.com/wp-content/themes/umove/_slicing/data/media/video.mp4" type="video/mp4" />
  				</video>
		</section>

    <section class="work">
      <div class="container">
        <div class="work__title">
          <h2><?php the_field('how_it_works');?></h2>
        </div>

        <div class="work__box">
          <div class="work__text-box">
            <h3><?php the_field('text_box');?></h3>
            <?php if( have_rows('work_items') ): ?>
            <?php while( have_rows('work_items') ): the_row(); 
            // vars
            $item = get_sub_field('item', false, false);
            ?>  
            <p>
              <?php echo $item; ?>
            </p>
            <?php endwhile; ?>
        <?php endif; ?>  
          </div>
          <div class="work__img-box">
            <img src="<?php the_field('image_box'); ?>" alt="courier near the door" />
          </div>
        </div>

        <div class="work__box">
          
          <div class="work__img-box work__img-box--left">
            <img src="<?php the_field('image_box_2'); ?>" alt="courier near the door" />
          </div>
    
        <div class="work__text-box work__text-box--right">
            <h3>
              <?php the_field('text_title');?>
            </h3>
            
            <?php if( have_rows('content_items') ): ?>
            <?php while( have_rows('content_items') ): the_row(); 
            // vars
            $items = get_sub_field('items', false, false);
            ?>    
              
            <p>
              <?php echo $items;?>
            </p>
              
            <?php endwhile; ?>
        <?php endif; ?>    
            
              
          </div>
        </div>

        <div class="work__box">
          <div class="work__text-box work__text-box--last">
            <h3>
              <?php the_field('title_');?>
            </h3>
            
                <?php if( have_rows('text_block') ): ?>
                <?php while( have_rows('text_block') ): the_row(); 
                    $item = get_sub_field('item', false, false);
                ?>    
              <p>
              <?php echo $item; ?>
            </p>
            <?php endwhile; ?>
            <?php endif; ?> 
          </div>
        </div>
          
        <div class="work__img-box work__img-box--last">
          <img src="<?php the_field('image_box_bottom'); ?>" alt="courier near the door" />
        </div>
          
      </div>
    </section>

    <?php get_template_part('template_part/block_home_4') ?>

    
    
    <section class="location">
      <div class="container">
        <div class="location__title">
          <h2><?php echo get_field('hom_b5_title',2); ?></h2>
          <?php echo get_field('hom_b5_stitle',2); ?>
        </div>

        <div class="location__search">
		  <form class="search search--reference" action="<?php the_field('opt_orderinfo_page_url', 'option') ?>" method="post" id="sord_form">
            <div class="inputs">
              <input type="submit" name="sord_sbmt" class="search__submit" value="" disabled/>
              <input type="text" name="sord_number" placeholder="Search / Reference Number" class="search__input" />
            </div>
            <div class="sorted_btn">
              <button class="btn sord_sbmt_but" disabled><?php _e('Find', 'umove') ?></button>
            </div> 			
          </form>
		    </div>
      </div>
    </section>  
      
      
    <section class="map" id="map">
	<div class="container">
		<?php the_field('hom_b6_svg', 2) ?>
	</div>
</section>
  </main>

  <?php get_footer();?>