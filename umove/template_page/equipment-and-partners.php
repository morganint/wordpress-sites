<?php 
/*Template Name: Equipment and Partners*/
get_header();?>
  <main>
    <section class="hero" style="background-image: url(<?php the_field('hero_image'); ?>);">
      <div class="wrapper"></div>
      <div class="container">
        <div class="hero__content">
          <div class="hero__text-box">
            <h1>
              <?php the_field('title'); ?>
            </h1>
            <?php the_field('content'); ?>

            <a class="btn btn-mobile-form" href="">Show my quote</a>
          </div>
          <?php get_template_part('template_part/quote') ?>
        </div>
      </div>
    </section>

    <section class="equipment-and-partners">
      <div class="container">
        <h2>Equipment and partners</h2>
        <div class="equipment-and-partners__box">
          <ul class="equipment-and-partners__list">
           <?php if( have_rows('equipment') ): ?> 
            
              <?php while( have_rows('equipment') ): the_row(); 
                $image = get_sub_field('image');
                $title = get_sub_field('title');
                $content = get_sub_field('content');
                ?> 
            <li class="equipment-and-partners__item">
              <h3>
                <span>
                  <img src="<?php echo $image;?>" alt="icon" /></span>
                <?php echo $title;?>
              </h3>
              <?php echo $content;?>
            </li>
               <?php endwhile; ?>
            <?php endif; ?>
          </ul>
          <div class="equipment-and-partners__image">
            <img src="<?php the_field('image'); ?>" width="590px" height="607px" alt="courier" />
          </div>
        </div>
      </div>
    </section>

    <section class="location">
      <div class="container">
        <div class="location__title">
          <h2><?php the_field('location'); ?></h2>
          <?php the_field('location_content'); ?>
        </div>

        <div class="location__search">
		    	<form class="search search--reference" action="<?php the_field('opt_orderinfo_page_url', 'option') ?>" method="post" id="sord_form">
            <div class="inputs">
              <input type="submit" name="sord_sbmt"   class="search__submit" value="" disabled/>
              <input type="text"   name="sord_number" placeholder="Search / Reference Number " class="search__input" />
            </div>
            <div class="sorted_btn">
              <button class="btn sord_sbmt_but" disabled><?php _e('Find', 'umove') ?></button>
            </div> 			
          </form>
		    </div>

      </div>
    </section>

    <section class="map" id="map">
	    <div class="container">
	    	<?php the_field('hom_b6_svg', 2) ?>
	    </div>
    </section>
  </main>

 <?php get_footer();?>