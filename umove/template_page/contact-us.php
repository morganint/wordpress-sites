<?php 
/*Template Name: Contact Us*/
get_header();?>

  <main>
    <section class="hero contact" style="background-image: url(<?php the_field('hero_image'); ?>);">
      <div class="wrapper"></div>
      <div class="container">
        <div class="contact-in">
          <h1 class="contact__title">
            <?php the_title();?>
          </h1>
          <div class="contact__inner">
            <div class="contact__left">
                <p><?php the_field('title');?></p>
              <div class="contact-nav">
                <div class="contact__item">
                  <div class="contact__item-image">
                    <svg xmlns="http://www.w3.org/2000/svg" width="38" height="58" viewBox="0 0 38 58">
                      <path fill="#FFF" fill-rule="evenodd" d="M19 2.333c9.372 0 16.97 7.69 16.97 17.174 0 6.323-5.674 18.376-17.02 36.16C7.67 37.883 2.03 25.83 2.03 19.507 2.03 10.022 9.628 2.333 19 2.333zM19 13a5.333 5.333 0 1 0 0 10.667A5.333 5.333 0 0 0 19 13z" opacity=".33" />
                    </svg>
                  </div>
                  <div class="contact__item-text">
                  <a href="<?php the_field('google_map_url')?>" target='_blank'><?php the_field('address');?></a>
                  </div>
                </div>
                <div class="contact__item">
                  <div class="contact__item-image">
                    <svg xmlns="http://www.w3.org/2000/svg" width="38" height="49" viewBox="0 0 38 49">
                      <path fill="#FFF" fill-rule="nonzero" d="M29.708 2.062l-20.416-.02a4.095 4.095 0 0 0-4.084 4.083v36.75a4.095 4.095 0 0 0 4.084 4.083h20.416a4.095 4.095 0 0 0 4.084-4.083V6.125c0-2.246-1.838-4.063-4.084-4.063zm0 36.73H9.292V10.208h20.416v28.584z" opacity=".33" />
                    </svg>
                  </div>
                  <div class="contact__item-text">
                    <a href="tel:<?php the_field('address_copy');?>"><?php the_field('address_copy');?></a>
                  </div>
                </div>
                <div class="contact__item">
                  <div class="contact__item-image">
                    <svg xmlns="http://www.w3.org/2000/svg" width="38" height="40" viewBox="0 0 38 40">
                      <path fill="#FFF" fill-rule="nonzero" d="M31.167 7.833H7.833a2.913 2.913 0 0 0-2.902 2.917l-.014 17.5a2.925 2.925 0 0 0 2.916 2.917h23.334a2.925 2.925 0 0 0 2.916-2.917v-17.5a2.925 2.925 0 0 0-2.916-2.917zm0 5.834L19.5 20.958 7.833 13.667V10.75L19.5 18.042l11.667-7.292v2.917z" opacity=".33" />
                    </svg>
                  </div>
                  <div class="contact__item-text">
                    <a href="mailto:<?php the_field('email');?>"><?php the_field('email');?></a>
                  </div>
                </div>
              </div>
              <div class="contact-social">
                <a href="https://www.facebook.com/yourmovetoday1/" target="_blank">
                  <svg xmlns="http://www.w3.org/2000/svg" width="32" height="32" viewBox="0 0 32 32">
                    <path fill="#FFF" fill-rule="evenodd" d="M15.683 31V16.86h5.308l.794-5.511h-6.102V7.83c0-1.595.496-2.683 3.054-2.683L22 5.146V.217C21.436.15 19.499 0 17.245 0c-4.704 0-7.924 2.568-7.924 7.285v4.064H4v5.51h5.32V31h6.363z" opacity=".3" />
                  </svg>
                </a>
                <a href="https://twitter.com/yourmovetoday1" target="_blank">
                  <svg xmlns="http://www.w3.org/2000/svg" width="32" height="32" viewBox="0 0 32 32">
                    <path fill="#FFF" fill-rule="evenodd" d="M30.173 3.459a12.15 12.15 0 0 1-3.912 1.519A6.09 6.09 0 0 0 21.77 3c-3.396 0-6.152 2.806-6.152 6.268 0 .491.053.968.158 1.427-5.115-.262-9.65-2.754-12.687-6.551a6.347 6.347 0 0 0-.833 3.155 6.294 6.294 0 0 0 2.739 5.216 6.074 6.074 0 0 1-2.79-.781v.076c0 3.038 2.122 5.572 4.94 6.146a5.905 5.905 0 0 1-1.622.22c-.397 0-.784-.037-1.159-.11.784 2.489 3.055 4.301 5.75 4.35A12.207 12.207 0 0 1 2.467 25.1c-.496 0-.987-.029-1.468-.086a17.218 17.218 0 0 0 9.435 2.814c11.322 0 17.512-9.55 17.512-17.833 0-.272-.005-.544-.016-.812A12.575 12.575 0 0 0 31 5.94a12.12 12.12 0 0 1-3.534.987 6.262 6.262 0 0 0 2.707-3.467" opacity=".3" />
                  </svg>
                </a>
                <a href="https://www.instagram.com/yourmovetoday/" target="_blank">
                  <svg xmlns="http://www.w3.org/2000/svg" width="32" height="32" viewBox="0 0 32 32">
                    <path fill="#FFF" fill-rule="evenodd" d="M27.434 2H4.566C2.596 2 1 3.537 1 5.435V27.53c0 1.897 1.596 3.436 3.566 3.436h22.868c1.97 0 3.566-1.539 3.566-3.436V5.435C31 3.537 29.404 2 27.434 2zm-4.871 4.525c0-.499.42-.904.937-.904h2.812c.518 0 .938.405.938.904v2.717c0 .499-.42.904-.938.904H23.5c-.517 0-.937-.405-.937-.904V6.525zm-6.528 4.434c3.186 0 5.77 2.489 5.77 5.557 0 3.07-2.584 5.558-5.77 5.558-3.186 0-5.769-2.488-5.769-5.558 0-3.068 2.583-5.557 5.769-5.557zm12.153 16.386c0 .499-.42.905-.938.905H4.75c-.518 0-.938-.406-.938-.905V13.767h3.75c-.488.679-.653 1.945-.653 2.75 0 4.846 4.094 8.79 9.126 8.79 5.032 0 9.127-3.944 9.127-8.79 0-.805-.119-2.052-.724-2.75h3.75v13.578z" opacity=".3" />
                  </svg>
                </a>
                <a href="https://www.linkedin.com/company/your-move-today" target="_blank">
                  <svg enable-background="new 0 0 128 128" id="Social_Icons" version="1.1" width="32" height="32" viewBox="0 0 128 128" xml:space="preserve" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink"><g id="_x34__stroke"><g id="LinkedIn"><rect clip-rule="evenodd" fill="none" fill-rule="evenodd" height="128" width="128"/><path clip-rule="evenodd" d="M3.229,43.133h24.948v79.049H3.229V43.133z     M15.067,33.246h-0.181C5.855,33.246,0,27.203,0,19.543C0,11.734,6.028,5.818,15.24,5.818c9.204,0,14.864,5.901,15.044,13.703    C30.284,27.18,24.444,33.246,15.067,33.246z M128,122.182H99.71V81.275c0-10.707-4.425-18.017-14.156-18.017    c-7.443,0-11.582,4.933-13.509,9.686c-0.722,1.706-0.61,4.083-0.61,6.46v42.777H43.409c0,0,0.361-72.462,0-79.049h28.026v12.406    c1.656-5.424,10.611-13.166,24.903-13.166c17.731,0,31.661,11.37,31.661,35.855V122.182z" fill="#fff" fill-rule="evenodd" opacity=".3" id="LinkedIn_1_"/></g></g></svg>
                </a>
              </div>
            </div>
            <div class="contact-form ">
              <h3 class="contact-form__title">
                Get In Touch
              </h3>
			  <?php echo do_shortcode('[contact-form-7 id="5823" title="Home contact"]');?>
            </div>
          </div>

        </div>
      </div>
    </section>
  </main>

 <?php get_footer();?>