<?php
/*
Template Name: Order info
*/
get_header();
	
	while(have_posts()){
		the_post();
		$pthid = get_post_thumbnail_id();
		$stp3p_iimg_title = get_field('stp3p_iimg_title');
		$stp3p_info_iphone = get_field('stp3p_info_iphone');
		$stp3p_info_iphone2 = get_field('stp3p_info_iphone2');
		$stp3p_info_phone = get_field('stp3p_info_phone');
	}
	
	if(isset($_POST['sord_number']) && (int)$_POST['sord_number'] > 0){
		$post_id = (int)$_POST['sord_number'];
		query_posts('post_type=shiporder&p='.$post_id);
		if(have_posts()){
			while(have_posts()){
			the_post(); ?>
			<section class="hero hero--empty"></section>
			<section class="quote quote-confirmation">
				<div class="container">
					<div class="track-info__title">
						<h1><?php _e('Confirmation', 'umove') ?></h1>
					</div>
					<div class="tracks tracks-desk">
						<div class="tracks__inner">
							<div class="tracks__text">
								<h3 class="tracks__title"><?php _e('Reference Number', 'umove') ?>: #<?php echo $post_id ?></h3>
								<?php the_content() ?>
							</div>
						</div>
					</div>
					<div class="quote__inner">
						<div class="quote__left">
							<div class="quote-trans">
								<div class="quote-trans__top">
									<div class="quote-trans__item">
										<svg xmlns="http://www.w3.org/2000/svg" width="27" height="42" viewBox="0 0 27 42"><g fill="none" fill-rule="evenodd"><path fill="#FFF" d="M13.46 42C22.487 28.33 27 19.063 27 14.202 27 6.911 20.956 1 13.5 1S0 6.91 0 14.202C0 19.063 4.487 28.33 13.46 42z"/><circle cx="13.5" cy="13.5" r="4.5" fill="#002035"/></g></svg>
										<div class="quote-trans__item-inner">
											<h4 class="quote-trans__item-title"><?php echo get_post_meta($post_id, 'st_from', true) ?></h4>
											<span class="quote-trans__item-subtitle"><?php _e('Start', 'umove') ?></span>
										</div>
									</div>
									<div class="quote-trans__item">
										<div class="quote-trans__item-inner">
											<h4 class="quote-trans__item-title"><?php echo get_post_meta($post_id, 'st_to', true) ?></h4>
											<span class="quote-trans__item-subtitle"><?php _e('Finish', 'umove') ?></span>
										</div>
										<svg xmlns="http://www.w3.org/2000/svg" width="27" height="42" viewBox="0 0 27 42"><g fill="none" fill-rule="evenodd"><path fill="#FFF" d="M13.46 42C22.487 28.33 27 19.063 27 14.202 27 6.911 20.956 1 13.5 1S0 6.91 0 14.202C0 19.063 4.487 28.33 13.46 42z"/><circle cx="13.5" cy="13.5" r="4.5" fill="#002035"/></g></svg>
									</div>
								</div>
								<div class="quote-trans__line">
									<span></span>
									<span></span>
								</div>
								<p class="quote-trans__car-text"><?php echo $stp3p_iimg_title ?></p>
								<div class="quote-trans__car"><?php
									$img = wp_get_attachment_image_src($pthid, 'thumbnail_563x526');
									$image_alt = get_post_meta($pthid, '_wp_attachment_image_alt', true); ?>
									<img src="<?php echo(isset($img[0]) ?$img[0] :get_template_directory_uri().'/img/noimage.png') ?>" alt="<?php echo($image_alt ?htmlspecialchars($image_alt) :htmlspecialchars(get_the_title())) ?>">
								</div>
							</div>
							<div class="tracks-call">
								<h3 class="tracks__title"><?php _e('You could save up to', 'umove') ?> $<?php echo get_post_meta($post_id, 'st_summ', true) ?> <?php _e('on your move', 'umove') ?></h3>
								<div class="tracks-call__text">
									<?php echo $stp3p_info_iphone ?>
								</div>
								<?php echo $stp3p_info_iphone2 ?>
							</div>
						</div>
						<div class="tracks tracks-mobile">
							<div class="tracks__inner">
								<div class="tracks__text">
									<h3 class="tracks__title"><?php _e('Reference Number', 'umove') ?>: #<?php echo $post_id ?></h3>
								</div>
							</div>
						</div>
						<div class=" tracks-form">
							<div class="tracks-form__item">
								<h2 class="tracks-form__title"><?php _e('Summary', 'umove') ?></h2>
								<p><?php _e('Choose between a Trailer or ReloCubes for your move', 'umove') ?></p>
								<div class="tracks-summary">
									<div class="tracks-summary__item">
										<h4 class="tracks-summary__item-title"><?php _e('Way', 'umove') ?></h4>
										<p><?php echo get_post_meta($post_id, 'st_from', true) ?> to <?php echo get_post_meta($post_id, 'st_to', true) ?></p>
									</div>
									<div class="tracks-summary__item">
										<h4 class="tracks-summary__item-title"><?php _e('Transit Time', 'umove') ?></h4>
										<p><?php echo $_POST['st_day'] ?></p>
									</div>
									<div class="tracks-summary__item">
										<h4 class="tracks-summary__item-title"><?php _e('Reference Number', 'umove') ?></h4>
										<p>#<?php echo $post_id ?></p>
									</div>
								</div>
							</div>
							<div class="tracks-form__total">
								<h3 class="tracks-form__total-title">
									<?php _e('Total', 'umove') ?>:
									<span class="tracks-form__total-price">$<?php echo get_post_meta($post_id, 'st_summ', true) ?></span>
								</h3>
								<p class="tracks-form__total-info"><?php _e('Driver, Taxes & Fuel included', 'umove') ?></p>
							</div>
							<div class="tracks-bottom">
								<div class="tracks-form__call">
									<?php echo $stp3p_info_phone ?>
								</div>
							</div>
						</div>
						<div class="confirmation-img">
							<svg xmlns="http://www.w3.org/2000/svg" width="501" height="841" viewBox="0 0 501 841"><g fill="none" fill-rule="evenodd"><path fill="#ECBBA1" fill-rule="nonzero" d="M40.702 69h3.164l-.048 33.42.048 36.134-.071.378 1.204 75.997L136 232.418 121.175 288s-88.451-11.683-108.941-29.998c-6.54-5.853-7.153-23.342-8.593-33.16-5.028-34.34 21.175-83.432 21.175-83.432s-11.709-19.471.472-44.678C34.919 76.977 40.702 69 40.702 69z"/><path d="M261.11 227.2c-.06-.14-.16-.28-.22-.4.04.14.14.26.22.4z"/><path fill="#1F80BF" fill-rule="nonzero" d="M106 287.256L109.376 224l85.062 4.377c24.455 47.16 39.835 72.434 40.562 73.822-28.323 8.33-129-14.943-129-14.943zM199.666 499c-14.969 89.328-3.712 240.433-1.951 306.291.142 4.788 4.926 8.209 11.994 10.45 5.188-7.077 13.35-11.677 22.631-11.677 9.377 0 17.635 4.718 22.846 11.936 8.377-2.24 14.731-5.78 16.064-10.685 15.73-57.626.619-258.101 23.226-257.747.167 0 .334.118.524.141v-31.277c-50.594-.118-82.555-5.26-95.334-17.432z"/><path fill="#7AC35E" fill-rule="nonzero" d="M256.298 815.796c-5.204-7.11-13.425-11.796-22.81-11.796-9.266 0-17.416 4.546-22.596 11.54C207.233 820.387 205 826.425 205 833h57c0-6.457-2.186-12.355-5.702-17.204z"/><path fill="#1F80BF" fill-rule="nonzero" d="M264.236 208c-3.825-.097-5.75 14.45-5.75 14.45.48 1.488 1.155 2.903 1.949 4.295.072.146.192.317.264.488a28.396 28.396 0 002.791 3.856c.217.22.505.464.722.683 3.296 3.54 7.579 6.517 12.608 8.64 1.443.61 3.007 1.123 4.571 1.562.746.22 1.468.464 2.286.66 1.082.268 2.261.463 3.416.658 2.599.39 5.366.684 8.421.708v-18.012c-24.613-.537-22.207-17.817-31.278-17.988zm-.536 23.65c.22.24.4.44.6.7v-.14c-.16-.18-.4-.38-.6-.56z"/><path fill="#1F80BF" fill-rule="nonzero" d="M264 231a29.514 29.514 0 01-3-4c.776 1.215 1.759 2.532 3 4zm-4.81-8s.46 1.682 1.62 4a21.94 21.94 0 01-1.62-4z"/><path fill="#7AC35E" fill-rule="nonzero" d="M286.741 243.09c-1.176-.187-2.258-.423-3.34-.634-.777-.188-1.483-.424-2.236-.635-1.506-.447-3.059-.942-4.47-1.506-4.942-2.07-9.106-4.94-12.33-8.328v.164c-.259-.305-.47-.54-.706-.823-1.13-1.34-2.047-2.588-2.73-3.717-.07-.165-.187-.306-.258-.47-1.341-2.4-1.906-4.141-1.906-4.141-39.058 6.423-64.446 6.446-64.446 6.446v1.859c0 1.576-.164 61.166-.282 114.31-.07 53.898-.07 114.124.282 139.86.094 5.53 2.259 10.21 6.4 14.187C213.354 511.8 244.954 516.929 295 517V243.867c-2.87-.047-5.576-.33-8.259-.776z"/><path fill="#ECBBA1" fill-rule="nonzero" d="M230.67 103.76c0 .08.02.16.02.24.22.06.42.16.64.24l-.66-.48z"/><path fill="#783826" fill-rule="nonzero" d="M237.39 107.722l.36.278L249 77a953.769 953.769 0 00-19.495 2.386c-.144 2.155-.505 4.101-.505 6.395 0 5.584.457 11.283 1.25 16.983l.793.533c2.164.95 4.327 2.34 6.346 4.425z"/><path fill="#ECBBA1" fill-rule="nonzero" d="M247.778 77.372l-11.15 31.55-.358-.282c-2.001-2.122-4.146-3.537-6.29-4.504-.262-.094-.5-.189-.762-.283-2.407-.873-4.741-1.226-6.719-.212-5.456 2.759-6.028 12.45-1.287 21.576 4.194 8.088 11.032 12.686 16.369 11.884 6.337 16.978 15.486 32.164 26.422 42.587v28.957c8.982.165 6.624 16.86 30.997 17.355V74c-14.51.024-33.046 1.769-47.222 3.372z"/><path fill="#1F80BF" fill-rule="nonzero" d="M289.855 9.024c-.28 0-.514-.024-.818-.024-36.786.802-59.423 26.889-61.037 68a102.2 102.2 0 002.245-3.797c2.456-4.152 5.683-9.553 9.518-14.2 3.484-4.269 7.39-7.877 11.693-9.316 10.477-3.444 32.46 7.288 43.544 7.24V9.355c-1.777-.095-3.274-.33-5.145-.33z"/><path fill="#113154" fill-rule="nonzero" d="M227.99 80.96v.08l.02-.06c0-.02-.02-.02-.02-.02zm1.97-7.96c-.7 1.516-1.36 2.882-1.92 4 .52-1.019 1.24-2.186 1.88-3.28.02-.248.02-.472.04-.72zm21.654-23.322c-4.285 1.432-8.174 5.022-11.644 9.27-3.82 4.647-7.033 9.998-9.478 14.129-.023.21-.023.446-.047.657-.745 1.056-1.583 2.135-2.189 3.098-.07 1.056-.233 2.018-.256 3.074a.403.403 0 01.14-.14.824.824 0 00-.117.187c0 .024.07.024.07.047.117 0 .769-.117 1.886-.258 3.26-.446 10.224-1.408 18.887-2.417 13.857-1.596 31.975-3.333 46.134-3.357V56.86c-11.062.07-32.953-10.608-43.386-7.182z"/><path fill="#124C87" fill-rule="nonzero" d="M228.04 81.01s-.02 0-.02-.02l-.06.02h.08z"/><path fill="#124C87" fill-rule="nonzero" d="M227.97 81.03l.06-.02c0-.02-.06-.02-.06-.04v.06z"/><path fill="#113154" fill-rule="nonzero" d="M228 80.99zm.06-.07c-.02.02-.1.06-.12.12v.02s.02 0 .02.02c0-.02.08-.14.1-.16z"/><path fill="#124C87" fill-rule="nonzero" d="M325.35 805.394c.782 4.973 6.591 8.461 14.51 10.606 5.194-7.141 13.374-11.831 22.692-11.831 8.892 0 16.716 4.266 21.933 10.865 6.496-2.192 11.191-5.374 12.187-9.64 9.769-40.632 8.37-205.447-5.193-308.394-11.713 13.882-43.865 19.75-95.53 19.75H295v31.252c32.555 2.852 23.995 216.736 30.35 257.392z"/><path fill="#5CA044" fill-rule="nonzero" d="M384.463 814.747C379.233 808.219 371.39 804 362.476 804c-9.341 0-17.542 4.64-22.747 11.703C336.163 820.55 334 826.496 334 833h57c0-6.947-2.52-13.264-6.537-18.253z"/><path fill="#124C87" fill-rule="nonzero" d="M327.119 208c-9.054-.121-5.925 18.025-31.678 18.025-.358 0-.597-.098-.955-.098v18.024c.215 0 .358.049.597.049.908 0 1.768-.097 2.652-.17 1.48-.05 2.795-.22 4.156-.366 4.062-.609 7.884-1.632 11.396-3.118 5.733-2.41 10.463-5.967 13.832-10.18 1.935-2.388 3.464-4.994 4.395-7.795 0 .024-.669-14.346-4.395-14.37z"/><path fill="#5CA044" fill-rule="nonzero" d="M398.77 346.96c-.5-52.853-1.641-98.721-1.736-102.696 0-.165-.024-.353-.024-.353S371.412 229.469 331.874 223c-.928 2.705-2.45 5.198-4.377 7.527-3.379 4.07-8.065 7.503-13.775 9.832-3.473 1.435-7.303 2.423-11.347 3.01-1.356.19-2.76.26-4.14.354-.808.023-1.45.188-2.283.188-.143 0-.215-.024-.357-.024-.143 0-.31.024-.476.024H295V517h.952c51.86 0 84.096-5.857 95.848-19.711 2.926-3.458 4.924-7.292 5.21-11.808 2.117-32.672 2.236-88.678 1.76-138.52z"/><path fill="#522517" fill-rule="nonzero" d="M353.816 109l.072-.07c2.158-2.388 4.507-3.877 6.807-4.846C361.502 98.13 362 92.15 362 86.312c0-2.576-.26-4.869-.474-7.28-3.155-.354-10.01-1.158-18.526-2.032l10.816 32z"/><path fill="#D39885" fill-rule="nonzero" d="M367.539 103.622c-2.083-1.06-4.615-.66-7.147.377-2.296.967-4.615 2.451-6.793 4.831l-.07.071-10.77-31.861C328.37 75.602 309.272 74 295.143 74H295v151.906c.355 0 .592.094.947.094 25.513 0 22.412-17.533 31.382-17.439v-30.258c10.271-10.37 18.886-25.027 24.921-41.288 5.325 1.108 12.33-3.558 16.567-11.806 4.71-9.144 4.118-18.806-1.278-21.587z"/><path fill="#ECBBA1" fill-rule="nonzero" d="M360 104.99v.02z"/><path fill="#124C87" fill-rule="nonzero" d="M295 9v47.492h.093c9.901-.094 29.33-9.678 38.928-7.229 6.71 1.695 12.836 7.912 17.938 14.269 3.075 3.838 5.778 7.7 7.968 10.69.745 1.036 1.444 2.001 2.073 2.778-1.374-41.723-19.01-66.305-67-68z"/><path fill="#113154" fill-rule="nonzero" d="M361.137 74.458c.56.864 1.26 1.805 1.76 2.542-.54-.839-1.14-1.881-1.78-3-.02.153-.02.305.02.458z"/><path fill="#091D2C" fill-rule="nonzero" d="M362.858 77.03c-.59-.68-1.415-1.572-2.076-2.346-.023-.14-.023-.281-.023-.422-2.218-3.002-4.955-6.826-8.07-10.649-5.167-6.357-11.373-12.525-18.168-14.214-9.72-2.44-29.399 7.107-39.427 7.2H295v17.1h.142c14.086 0 33.15 1.595 47.472 3.026 8.447.844 15.266 1.665 18.428 2.017C362.316 78.906 363 79 363 79c-.024-.704-.118-1.29-.142-1.97z"/><g fill-rule="nonzero"><path fill="#522517" d="M489.88 450.478c6.58 4.17 8.554 12.912 4.394 19.486l-49.256 78.39c-4.16 6.597-12.878 8.576-19.458 4.406l-109.698-69.295c-6.58-4.194-8.577-12.936-4.441-19.533l49.279-78.343c4.183-6.621 12.878-8.577 19.435-4.454l109.745 69.343z"/><path fill="#FFF" d="M324.69 472.909l49.812-79.265 106.674 67.39-49.811 79.264z"/><path fill="#F7AD53" d="M367.397 408.726c3.314 2.097 4.301 6.456 2.21 9.755l-24.699 39.301c-2.068 3.275-6.439 4.312-9.729 2.215l-17.836-11.263c-3.29-2.097-4.301-6.48-2.233-9.778l24.698-39.301c2.092-3.322 6.463-4.312 9.73-2.215l17.86 11.286z"/></g><path fill="#ECBBA1" fill-rule="nonzero" d="M414.37 360.9l9.708 15.476-35.817 108.568-16.047 4.216s-11.145 2.992-20.76 1.46c-4.736-.776-5.75 4.217-2.78 6.903 7.988 7.278 16.4 5.088 20.218 5.865 2.733.518-14.94 16.277-8.907 21.341 2.662 2.262 5.608-2.709 8.27-.282 1.32 1.225-1.625 5.064 1.768 7.797 3.417 2.708 5.325-1.296 7.069-.354 1.932 1.013.07 4.617 3.652 6.902 3.841 2.45 7.282-1.625 8.295-1.39 3.04.73-.354 4.806 5.514 5.583 5.42.707 18.403-23.461 22.975-35.098 2.003-5.111-.73-13.403-.73-13.403L475 382.384V364.41L457.845 334l-43.475 26.9z"/><path fill="#124C87" fill-rule="nonzero" d="M417.105 367L461 334.505s-35.289-83.01-64-90.505c.093 3.947 1.213 49.764 1.703 102.582L417.105 367z"/><g fill-rule="nonzero"><path fill="#F7AD53" d="M42.611 55.01h169.542v181.193H42.611z"/><path fill="#BC7329" d="M215.693 239.742H39.094V51.472h176.6v188.27zm-169.542-7.077h162.462V58.549H46.151v174.116z"/><path d="M95.758 173.69h63.224v9.436H95.758zm27.848-44.136l-13.924-21.09-13.924 21.09h9.77v36.422h8.308v-36.422zm35.376 0l-13.947-21.09-13.924 21.09h9.77v36.422h8.307v-36.422z" fill="#BC7329"/></g></g></svg>
						</div>
					</div>
				</div>
			</section><?php
			}
		}else{ ?>
			<section class="service service--fill">
			<div class="error-message">
				<a href="<?php echo home_url() ?>">
					<p><?php _e('Order not found! Please click to go step 1.', 'umove') ?></p>
				</a>
			</div>
		</section><?php
		}
	}else{ ?>
		<section class="service service--fill">
			<div class="error-message">
				<a href="<?php echo home_url() ?>">
					<p><?php _e('Error! Please click to go step 1.', 'umove') ?></p>
				</a>
			</div>
		</section><?php
	} ?>
	
	<?php get_template_part('template_part/block_info') ?>
	
<?php get_footer(); ?>