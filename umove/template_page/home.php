<?php
/*
Template Name: Home
*/
get_header(); ?>

<?php while(have_posts()){
	the_post(); ?>
	<main>
		<?php get_template_part('template_part/block_home_1') ?>

		<section class="media">
    			<video class="video" playsinline controls  poster="https://yourmovetoday.com/wp-content/themes/umove/_slicing/data/media/poster.png">
      				<source src="https://yourmovetoday.com/wp-content/themes/umove/_slicing/data/media/video.mp4" type="video/mp4" />
  				</video>
		</section>
		
		<?php get_template_part('template_part/block_home_2') ?>

		<?php get_template_part('template_part/block_home_3') ?>
		
		<?php get_template_part('template_part/block_home_4') ?>
		
		<?php //get_template_part('template_part/block_home_5') ?>
		
		<?php get_template_part('template_part/block_home_6') ?>
		
		<?php get_template_part('template_part/block_home_7') ?>
	</main>
<?php } ?>
	
<?php get_footer() ?>