<?php
/*
Template Name: Transportation step 3 Thanks
*/
get_header();
	
	if(
		isset($_GET['post_id']) 
		
	){
        isset($_POST['st_from']) 
		&& isset($_POST['st_to']) 
		&& isset($_POST['st_day']) 
		&& isset($_POST['st_size'])
		//&& isset($_POST['st_f1_phone'])
		
		&& isset($_POST['st_cartype']) 
		&& isset($_POST['st_addsrvlist']) 
		&& isset($_POST['st_summ']) 
		&& isset($_POST['st_usr_email'])
		
		&& isset($_POST['st_usr_email']) 
		&& isset($_POST['st_usr_phone']) 
		&& isset($_POST['st_usr_phone1']) 
		&& isset($_POST['st_usr_contact_addr_addr']) 
		&& isset($_POST['st_usr_contact_addr_appartment']) 
		&& isset($_POST['st_usr_contact_addr_city']) 
            
        && isset($_POST['st_usr_contact_addr_zip'])    
        && isset($_POST['user_destination_address_zip'])
		
		//&& isset($_POST['st_delidery_date']) 
		//&& isset($_POST['st_delidery_time']) 
		&& isset($_POST['st_pickup_date']) 
		&& isset($_POST['st_pickup_time']) 
		&& isset($_POST['st_arrival_date']) 
		
		&& isset($_POST['loaded_on']) 
		&& isset($_POST['st_transit_days']) 
		
		// && isset($_POST['moroptb_mo_1'])
		// && isset($_POST['moroptb_mo_2'])
		// && isset($_POST['moroptb_mo_3'])
		// && isset($_POST['moroptb_mo_4'])
		
		//&& isset($_POST['st_delivery_days']) 
		&& isset($_POST['st_referal_number']) 
		&& isset($_POST['st_usr_name_card']) 
		&& isset($_POST['st_visa_card_number'])
		&& isset($_POST['st_visa_card_exp_month']) 
		&& isset($_POST['st_visa_card_exp_year']) 
		&& isset($_POST['st_visa_card_secr_code'])
		&& isset($_POST['st_f1_fullname']);
        
		$_POST['st_moove_messaging_yn']   = (isset($_POST['st_moove_messaging_yn'])   ?'yes' :'no');
		$_POST['st_mssg_rate_info_1']     = (isset($_POST['st_mssg_rate_info_1'])     ?'yes' :'no');
		$_POST['st_mssg_rate_info_2']     = (isset($_POST['st_mssg_rate_info_2'])     ?'yes' :'no');
		$_POST['st_mssg_car_ship_info_1'] = (isset($_POST['st_mssg_car_ship_info_1']) ?'yes' :'no');
		$_POST['st_mssg_car_ship_info_2'] = (isset($_POST['st_mssg_car_ship_info_2']) ?'yes' :'no');
		
		$codePID = $_GET['post_id'];
		$post_id = decode($_GET['post_id']);
		
		update_post_meta($post_id, 'ord_payed_yn', 'no');
		update_post_meta($post_id, 'st_from', htmlspecialchars($_POST['st_from']));
		update_post_meta($post_id, 'st_to', htmlspecialchars($_POST['st_to']));
		update_post_meta($post_id, 'st_day', htmlspecialchars($_POST['st_day']));
		update_post_meta($post_id, 'st_size', htmlspecialchars($_POST['st_size']));
		update_post_meta($post_id, 'st_cartype', htmlspecialchars($_POST['st_cartype']));
		update_post_meta($post_id, 'st_addsrvlist', htmlspecialchars(implode(',', $_POST['st_addsrvlist'])));
		update_post_meta($post_id, 'st_summ', htmlspecialchars($_POST['st_summ']));
		update_post_meta($post_id, 'st_usr_email', htmlspecialchars($_POST['st_usr_email']));
		update_post_meta($post_id, 'st_f1_phone', htmlspecialchars($_POST['st_f1_phone']));
		update_post_meta($post_id, 'st_usr_phone1', htmlspecialchars($_POST['st_usr_phone1']));
		update_post_meta($post_id, 'st_usr_contact_addr_addr', htmlspecialchars($_POST['st_usr_contact_addr_addr']));
		update_post_meta($post_id, 'st_usr_contact_addr_appartment', htmlspecialchars($_POST['st_usr_contact_addr_appartment']));
		update_post_meta($post_id, 'st_usr_contact_addr_city', htmlspecialchars($_POST['st_usr_contact_addr_city']));
		update_post_meta($post_id, 'st_usr_contact_addr_country', htmlspecialchars($_POST['st_usr_contact_addr_country']));
		
        update_post_meta($post_id, 'st_usr_contact_addr_zip', htmlspecialchars($_POST['st_usr_contact_addr_zip']));
        
		update_post_meta($post_id, 'st_usr_destination_addr_addr', htmlspecialchars($_POST['st_usr_destination_addr_addr']));
		update_post_meta($post_id, 'st_usr_destination_addr_appartment', htmlspecialchars($_POST['st_usr_destination_addr_appartment']));
		update_post_meta($post_id, 'st_usr_destination_addr_city', htmlspecialchars($_POST['st_usr_destination_addr_city']));
		update_post_meta($post_id, 'st_usr_destination_addr_country', htmlspecialchars($_POST['st_usr_destination_addr_country']));
		
        update_post_meta($post_id, 'user_destination_address_zip', htmlspecialchars($_POST['user_destination_address_zip']));
		
        //update_post_meta($post_id, 'st_delidery_date', htmlspecialchars($_POST['st_delidery_date']));
		//update_post_meta($post_id, 'st_delidery_time', htmlspecialchars($_POST['st_delidery_time']));
		update_post_meta($post_id, 'st_pickup_date', htmlspecialchars($_POST['st_pickup_date']));
		update_post_meta($post_id, 'st_pickup_time', htmlspecialchars($_POST['st_pickup_time']));
		update_post_meta($post_id, 'st_arrival_date', htmlspecialchars($_POST['st_arrival_date']));
		//update_post_meta($post_id, 'st_delivery_days', htmlspecialchars($_POST['st_delivery_days']));
		
		update_post_meta($post_id, 'st_usr_name_card', htmlspecialchars($_POST['st_usr_name_card']));
		update_post_meta($post_id, 'st_visa_card_number', htmlspecialchars($_POST['st_visa_card_number']));
		update_post_meta($post_id, 'st_visa_card_exp_month', htmlspecialchars($_POST['st_visa_card_exp_month']));
		update_post_meta($post_id, 'st_visa_card_exp_year', htmlspecialchars($_POST['st_visa_card_exp_year']));
		update_post_meta($post_id, 'st_visa_card_secr_code', htmlspecialchars($_POST['st_visa_card_secr_code']));
		update_post_meta($post_id, 'st_moove_messaging_yn', htmlspecialchars($_POST['st_moove_messaging_yn']));
		update_post_meta($post_id, 'st_mssg_rate_info_1', htmlspecialchars($_POST['st_mssg_rate_info_1']));
		update_post_meta($post_id, 'st_mssg_rate_info_2', htmlspecialchars($_POST['st_mssg_rate_info_2']));
		update_post_meta($post_id, 'st_mssg_car_ship_info_1', htmlspecialchars($_POST['st_mssg_car_ship_info_1']));
		update_post_meta($post_id, 'st_mssg_car_ship_info_2', htmlspecialchars($_POST['st_mssg_car_ship_info_2']));
		
		
		update_post_meta($post_id, 'loaded_on', htmlspecialchars($_POST['loaded_on']));
	
		require_once(dirname(dirname(__FILE__)).'/inc/class-gatewey-old/Gateway.php');

		$Gateway = new Gateway();

		$TransactionType = "sale";
		$PaymentType     = "creditcard";

		$GatewayUserName = get_field('sopfi_secr_username', 'option');
		$GatewayPassword = get_field('sopfi_secr_userpass', 'option');

		$CCNumber  = $_POST['st_visa_card_number'];
		$CCExpDate = $_POST['st_visa_card_exp_month'].$_POST['st_visa_card_exp_year']; // "1124"
		$CVV       = $_POST['st_visa_card_secr_code'];
		
		$Amount           =  $_POST['st_summ'];
		$OrderDescription = get_field('sopfi_note', 'option');
		$FirstName        = $_POST['st_usr_name_card'];
		$LastName         = $_POST['st_usr_name_card'];
		$EMail            = $_POST['st_usr_email'];
        
        $SAFE_Action      = $post_id;
		
        $SAFE_ID          = $codePID;
        
        $MagData = "";

		$results = $Gateway->process($TransactionType, $GatewayUserName, $GatewayPassword, "", "", $PaymentType, $SAFE_Action, $SAFE_ID, $CCNumber, $CCExpDate, $CVV, "", "", "", "", "", "", $Amount, "", "",
						$OrderDescription, "", "", $FirstName, $LastName, "", "", "", "", "", "", "", "", "", $EMail, "", "", "", "", "", "", "", "", "", "", "", "", $MagData);
		
		//||1=1
		if(isset($results['ProcessTransactionResult']['STATUS_CODE']) && (int)$results['ProcessTransactionResult']['STATUS_CODE']==1){
			//set WP post order as paid
			update_post_meta($post_id, 'ord_payed_yn', 'yes');
			update_post_meta($post_id, 'ord_pay_trans_id', $results['ProcessTransactionResult']['TRANS_ID']);
			
			
			$boby = get_field('themltpl_body', 'option');
			$boby = str_replace('[emltpl_refnumb]', $SAFE_ID, $boby);
			$boby = str_replace('[emltpl_st_from]', htmlspecialchars($_POST['st_from']), $boby);
			$boby = str_replace('[emltpl_st_to]', htmlspecialchars($_POST['st_to']), $boby);
			$boby = str_replace('[st_f1_fullname]', htmlspecialchars($_POST['st_f1_fullname']), $boby);
			$boby = str_replace('[stp1p_b4_cart_list]', htmlspecialchars($_POST['st_cartype']), $boby);
			//$boby = str_replace('[st_delivery_type]', htmlspecialchars($_POST['st_delivery_days']), $boby);
			//$boby = str_replace('[st_delivery]', htmlspecialchars($_POST['xxx']), $boby);
			//$boby = str_replace('[st_delidery_time]', htmlspecialchars($_POST['st_delidery_time']), $boby); // need add to mail template && out in adminpost
			$boby = str_replace('[st_pickup_date]', htmlspecialchars($_POST['st_pickup_date']), $boby); // need add to mail template && out in adminpost
			$boby = str_replace('[st_pickup_time]', htmlspecialchars($_POST['st_pickup_time']), $boby); // need add to mail template && out in adminpost
			$boby = str_replace('[tpl_st_summ]', htmlspecialchars($_POST['st_summ']), $boby);
			$boby = str_replace('[tpl_tr_days]', get_post_meta ( $post_id, 'st_transit_days', true ), $boby);
			
			// $loaded = get_post_meta($post_id, 'st_pickup_date', true);
			// $lDate = date("m/d/Y", strtotime($loaded));
			// $boby = str_replace('[emltpl_loaded_on]', $lDate, $boby);
			$boby = str_replace('[emltpl_loaded_on]', get_post_meta($post_id, 'st_pickup_date', true), $boby);
			$boby = str_replace('[emltpl_arrival_date]', get_post_meta($post_id, 'st_arrival_date', true), $boby);
			
            $event_date = get_post_meta($post_id, 'estimating_date', true);
			//print_r(get_post_meta($post_id, 'estimating_date', true));
            $preparr = explode(':', $event_date);
            $hour = $preparr[0];
            $min = $preparr[1];
			$boby = str_replace('[emltpl_estimating_date]', $event_date, $boby);
            $boby = str_replace('[emltpl_estimating_time]', get_post_meta($post_id, 'estimating_time', true), $boby);
            $boby = str_replace('[emltpl_now]', get_post_meta($post_id, 'now', true), $boby);
			
			$to      = $_POST['st_usr_email'];
			$subject = __('Your Move Quote client', 'umove');
			$headers  = 'From: '.get_option('admin_email')."\r\n";
			$headers .= "MIME-Version: 1.0\r\n";
			$headers .= "Content-Type: text/html; charset=ISO-8859-1\r\n";
			mail($to, $subject, $boby, $headers); ?>
			
            <!--start deals update-->
			<?php 
			
			$firstname = get_post_meta($post_id, 'st_f1_fullname', true);
			$amount = get_post_meta($post_id, 'st_summ', true);
			$dealset = get_post_meta($post_id, 'hubsport_quote_id', true);
			$refNum = get_post_meta($post_id, 'st_referal_number', true);
			
			$arr3 = array(
            'properties' => array(
                array(
					//'value' => $firstname, 
					'value' => $firstname . ' - ' . $amount . ' - ' . $refNum, 
					'name' => 'dealname'
				),
            
				array(
					'value' => 'contractsent', //
					'name' => 'dealstage'
				),
				array(
					'value' => '39551299', 
					'name' => 'hubspot_owner_id'
				),
								
				array(
					'value' => $amount, 
					'name' => 'amount'
				),
				array(
					'value' => 'newbusiness', 
					'name' => 'dealtype' 
				),
            )
        );
        $post_json = json_encode($arr3);
       // $endpoint = 'https://api.hubapi.com/deals/v1/deal?hapikey=c1c64878-af5c-40bb-b65e-3a18def1a565';
        $endpoint = 'https://api.hubapi.com/deals/v1/deal/'.$dealset.'?hapikey=c1c64878-af5c-40bb-b65e-3a18def1a565&dealstage=1020595';
		             //https://api.hubapi.com/deals/v1/deal/1081693383?hapikey=c1c64878-af5c-40bb-b65e-3a18def1a565
        
        $ch = @curl_init();
        @curl_setopt($ch, CURLOPT_POST, true);
        @curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
        @curl_setopt($ch, CURLOPT_CUSTOMREQUEST, "PUT");
        @curl_setopt($ch, CURLOPT_POSTFIELDS, $post_json);
        @curl_setopt($ch, CURLOPT_URL, $endpoint);
        @curl_setopt($ch, CURLOPT_HTTPHEADER, array('Content-Type: application/json'));
        @curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
        $response = @curl_exec($ch);
        $status_code = @curl_getinfo($ch, CURLINFO_HTTP_CODE);
        $curl_errors = curl_error($ch);
        @curl_close($ch);
        //echo "\ncurl Errors: " . $curl_errors;
        //echo "\nStatus code: " . $status_code;
        //echo "\nResponse: " . $response;
        
        /********************/
		?>
            <!--end deals update -->


			<section class="hero hero--empty"></section>
			<section class="quote quote-confirmation">
				<div class="container">
					<div class="track-info__title">
						<h1><?php _e('Confirmation', 'umove') ?></h1>
					</div>
					<div class="tracks tracks-desk">
						<div class="tracks__inner">
						<h3 class="tracks__title"><?php _e('Reference Number', 'umove') ?>: #<?php echo $codePID ?></h3>
						</div>
					</div>
					<div class="quote__inner">
						<div class="quote__left">
							<div class="quote-trans">
								<div class="quote-trans__top">
									<div class="quote-trans__item quote-trans__item--left">
										<svg xmlns="http://www.w3.org/2000/svg" width="27" height="42" viewBox="0 0 27 42"><g fill="none" fill-rule="evenodd"><path fill="#FFF" d="M13.46 42C22.487 28.33 27 19.063 27 14.202 27 6.911 20.956 1 13.5 1S0 6.91 0 14.202C0 19.063 4.487 28.33 13.46 42z"/><circle cx="13.5" cy="13.5" r="4.5" fill="#002035"/></g></svg>
										<div class="quote-trans__item-inner">
											<h4 class="quote-trans__item-title"><?php echo get_post_meta($post_id, 'st_from', true) ?></h4>
											<span class="quote-trans__item-subtitle"><?php _e('Start', 'umove') ?></span>
										</div>
									</div>
									<div class="quote-trans__item quote-trans__item--right">
										<div class="quote-trans__item-inner">
											<h4 class="quote-trans__item-title"><?php echo get_post_meta($post_id, 'st_to', true) ?></h4>
											<span class="quote-trans__item-subtitle"><?php _e('Finish', 'umove') ?></span>
										</div>
										<svg xmlns="http://www.w3.org/2000/svg" width="27" height="42" viewBox="0 0 27 42"><g fill="none" fill-rule="evenodd"><path fill="#FFF" d="M13.46 42C22.487 28.33 27 19.063 27 14.202 27 6.911 20.956 1 13.5 1S0 6.91 0 14.202C0 19.063 4.487 28.33 13.46 42z"/><circle cx="13.5" cy="13.5" r="4.5" fill="#002035"/></g></svg>
									</div>
								</div>
								<div class="quote-trans__line">
									<span></span>
									<span></span>
								</div>
								<div class="quote-trans__car"><?php
									$img = wp_get_attachment_image_src(get_field('stp3p_mcimg'), 'thumbnail_563x526');
									$image_alt = get_post_meta(get_field('stp3p_mcimg'), '_wp_attachment_image_alt', true); ?>
									<img src="<?php echo(isset($img[0]) ?$img[0] :'') ?>" alt="<?php echo($image_alt ?htmlspecialchars($image_alt) :htmlspecialchars(get_the_title())) ?>">
								</div>
							</div>
						</div>
						<div class="tracks tracks-mobile">
							<div class="tracks__inner">
								<div class="tracks__text">
									<h3 class="tracks__title"><?php _e('Reference Number', 'umove') ?>: #<?php echo $codePID ?></h3>
								</div>
							</div>
						</div>
						<form class=" tracks-form">
							<div class="tracks-form__item">
								<h2 class="tracks-form__title"><?php _e('Summary', 'umove') ?></h2>
								<div class="tracks-summary">
									<div class="tracks-summary__item">
										<h4 class="tracks-summary__item-title"><?php _e('pick up location & delivery destination', 'umove') ?></h4>
										<p><?php echo get_post_meta($post_id, 'st_from', true) ?> to <?php echo get_post_meta($post_id, 'st_to', true) ?></p>
									</div>
									<div class="tracks-summary__item">
										<h4 class="tracks-summary__item-title"><?php _e('pick up & delivery date', 'umove') ?></h4>
										<p><?php echo get_post_meta($post_id, 'st_pickup_date', true) ?><?php //echo $_POST['st_pickup_date'] ?></p>
									</div>
									<div class="tracks-summary__item">
										<h4 class="tracks-summary__item-title"><?php _e('Reference Number', 'umove') ?></h4>
										<p>#<?php echo $codePID ?></p>
									</div>
								</div>
							</div>
							<div class="tracks-form__total">
								<h3 class="tracks-form__total-title">
									<?php _e('Total', 'umove') ?>:
									<span class="tracks-form__total-price">$<?php echo get_post_meta($post_id, 'st_summ', true) ?></span>
								</h3>
								<p class="tracks-form__total-info"><?php _e('Driver, Taxes & Fuel included', 'umove') ?></p>
							</div>
							<div class="tracks-bottom">
								<!--<button type="submit" class="btn tracks-btn"><?php _e('Tracking My Quote', 'umove') ?></button>-->
								<a href="<?php the_permalink(6323);?>" target="_blank" class="btn-o "><?php _e('Contact Us', 'umove') ?></a>
								<div class="tracks-form__call">
									<?php the_field('stp3p_info_phone') ?>
								</div>
							</div>
						</form>
					</div>
					
				</div>
			</section>
			
		<?php }else{ ?>
			<section class="service service--fill">
				<div class="error-message">
					<p><br/><br/><br/><?php
						if(isset($results['ProcessTransactionResult']['STATUS_MSG'])){
							echo __('Error creating transaction', 'umove').':'.$results['ProcessTransactionResult']['STATUS_MSG'];
						}else{
							_e('Errors №39, 63 !', 'umove');
						} ?>
					</p>
				</div>
			</section>
        <?php
		}
	}else{ ?>
		<section class="service service--fill">
			<div class="error-message">
				<a href="<?php echo home_url() ?>">
					<p><?php _e('Error! Please click to go Home page.', 'umove') ?></p>
				</a>
			</div>
		</section><?php
	} ?>
	
	<?php get_template_part('template_part/block_info') ?>
	
<?php get_footer(); ?>