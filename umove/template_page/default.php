<?php 
/*Template Name: Inner pages*/
get_header();?>

<main>   
	<section class="hero hero--empty"></section>

    <section class="text-page">
	<div class="container container-small">
    	<div class="text-page__title">
     	 	<h1><?php the_title()?></h1>
		</div>
		<div>
			<?php 
        		if ( have_posts() ) : 
                    while ( have_posts() ) : the_post(); ?>					    	
					<?php the_content() ?>                            
                <?php 
			endwhile; endif; ?> 
		</div>
    	<!-- <div class="container">
		    <h2 class="inner-page-title"><?php /* the_title()?></h2>
		     <div class="stages__boxxxxx">
			    <div class="stages__list"> 
                   <?php 
                        if ( have_posts() ) : 
                            while ( have_posts() ) : the_post(); ?>					    	
						    	<?php the_content() ?>                            
                    <?php 
				  	 endwhile; endif; */?>  
			     </div>			  
			</div> 
		</div> -->
	</section>    
</main>

<?php get_footer();?>