<?php 
/*Template Name: Coverage*/
get_header();?>

  <section class="hero hero--empty"></section>

  <section class="coverage-area">
    <div class="container">
      <div class="coverage-area__title">
        <h1><?php the_title();?></h1>
      </div>

      <div class="area-row">
        <div class="area-row__title">
          <h2><?php the_field('subtitle')?></h2>
        </div>

        <div class="area-row__box">
          <div class="area-row__text">
            <?php the_field('content')?>
            <div class="area-row__text">
            <h3 class="area-row__title"><?php the_field('services_area_title')?></h3>
            <?php the_field('service_area_text')?>
          </div>
          </div>
          <div class="area-row__img">
            <img src="<?php the_field('image_block')?>" alt="delivery container" />
          </div>
        </div>
      </div>

      
    </div>
  </section>

  <section class="map" id="map">
    <div class="area-row__text city-search">        
      <h3 class="area-row__title"><?php the_field('hom_b5_title',2); ?></h3>
         <?php the_field('hom_b5_stitle',2); ?>
      <!--<div class="area-row__search">
        <form class="search search--reference">
          <input type="submit" name="" value="" class="search__submit" />
          <input type="text" name="" placeholder="Search" class="search__input" />
        </form>
        <button class="btn">Find</button>
      </div>-->
    </div>

    <div class="location__search">
                <form class="search search--reference" action="<?php the_field('opt_orderinfo_page_url', 'option') ?>" method="post" id="sord_form">
                    <div class="inputs">
                        <input type="submit" name="sord_sbmt"   class="search__submit" value="" disabled/>
                        <input type="text"   name="sord_number" placeholder="Search / Reference Number " class="search__input" />
                    </div>
                    <div class="sorted_btn">
                        <button class="btn sord_sbmt_but" disabled><?php _e('Find', 'umove') ?></button>
                    </div> 			
                </form>
            </div>  
      
    <div class="container city-search">
      <img src="<?php echo get_template_directory_uri();?>/_slicing/data/img/map.png" alt="" />
    </div>
    
  </section>
  
    <!--<section class="location">
          <div class="container">
            <div class="location__title">
              <h2><?php the_field('subtitle'); ?></h2>
              <?php the_field('subcontent'); ?>
            </div>

            <div class="location__search">
                <form class="search search--reference" action="<?php the_field('opt_orderinfo_page_url', 'option') ?>" method="post" id="sord_form">
                    <div class="inputs">
                        <input type="submit" name="sord_sbmt"   class="search__submit" value="" disabled/>
                        <input type="text"   name="sord_number" placeholder="Search / Reference Number " class="search__input" />
                    </div>
                    <div class="sorted_btn">
                        <button class="btn sord_sbmt_but" disabled><?php _e('Find', 'umove') ?></button>
                    </div> 			
                </form>
            </div>
          </div>
        </section>-->



    </div>
  </section>
 <?php get_footer();?>