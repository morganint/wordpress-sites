<?php 
/*Template Name: Storage*/
get_header();?>

  <section class="hero hero--empty"></section>

  <section class="storage">
    <div class="container container-small">
      <div class="storage__title">
        <h1>Trailer Storage</h1>
      </div>

      <div class="storage__row">
        <div class="storage__box">
          <ul class="storage__box-list">
            <li class="storage__box-item">
              <h3>Adding a Storage Trailer to Your Long-Distance Move</h3>
              <p>
                Some people may need a little extra time during their move.
                Whether you need a month to close on a new house or several months
                to find a place to live, U-Move can help. Pair our easy,
                affordable moving service with storage in the trailer.
              </p>
            </li>
            <li class="storage__box-item">
              <h3>Benefits of Using Trailer Storage</h3>
              <p>
                U-Move storage saves you the hassle and hard work of moving in and
                out of a traditional storage unit. Instead of loading and
                unloading many times, you can store right in the trailer for as
                long as you need.
              </p>

              <p>
                Here’s how it works: You load your belongings inside the
                equipment. We’ll transport and store it at one of our secure
                service centers at origin, at destination, or while in transit.
                The items will stay inside the equipment until you’re ready for
                them. When you’re ready, just give us a call, and we’ll deliver
                them!
              </p>
              <p>
                Unsure of what you need? Our moving consultants can help you
                figure out the best option based on the specifics of your move.
              </p>
            </li>
          </ul>
        </div>
        <div class="storage__image">
          <img src="<?php echo get_template_directory_uri();?>/_slicing/data/img/bitmap-2.png" alt="bitmap" />
        </div>
      </div>
    </div>

    <div class="storage__FAQ">
      <div class="container container-small">
        <div class="storage__FAQ-title">
          <h2>FAQ</h2>
        </div>
        <div class="storage__FAQ-text-box">
          <h3>FAQ about U-Move Storage Trailers</h3>
          <p>
            Do you have questions about storing in a trailer as part of your move?
            We’re here to help. Here’s a list of some of the most frequently asked
            questions:
          </p>
        </div>

        <div class="FAQ">
          <ul class="FAQ__list">
            <li class="FAQ__item">
              <h4>How does pricing work?</h4>
              <p>
                Storage trailer pricing is based on the number of trailers you
                store. Storage starts once your shipment arrives at the service
                center where it’s stored, and it’s billed every 30 days from
                there. Note that storage pricing is not pro-rated.
              </p>
            </li>
            <li class="FAQ__item">
              <h4>How do I cancel my storage reservation?</h4>
              <p>
                If you added storage to your move, but end up not needing it, we
                make it easy to remove from your reservation. Simply call
                XXX-XXX-XXXX, or email moving@umove.com before the trailer goes
                into storage to let us know.
              </p>
            </li>
          </ul>
          <ul class="FAQ__list">
            <li class="FAQ__item">
              <h4>What can I store in the trailer?</h4>
              <p>
                While most things like clothing, furniture and appliances are safe
                for transit, some things can’t be moved or stored in a trailer.
                Before your move, be sure to check out the Do Not Ship list, so
                you can dispose of these items or pack them separately from your
                U-Move shipment.
              </p>
            </li>
            <li class="FAQ__item">
              <h4>Will it be climate-controlled?</h4>
              <p>
                U-Move trailers are not climate controlled, so you may consider
                bringing any heat-sensitive items with you rather than packing
                into the trailer.
              </p>
            </li>
          </ul>
          <ul class="FAQ__list">
            <li class="FAQ__item">
              <h4>Will my things be co-mingled with someone else’s?</h4>
              <p>
                You’ll be pleased to know that your belongings are never
                co-mingled or transferred.
              </p>
            </li>

            <li class="FAQ__item">
              <h4>
                What’s the liability coverage while my shipment is in storage?
              </h4>
              <p>
                U-Move offers basic liability coverage while the shipment is in
                transit and while it’s being stored
              </p>
            </li>
          </ul>
          <ul class="FAQ__list">
            <li class="FAQ__item">
              <h4>
                Can I put my own lock on the trailer while it is in storage?
              </h4>
              <p>
                U-Move customers may use their own lock on the shipment divider
                wall (bulkhead) inside the trailer, but the trailer doors will be
                secured with our lock while it’s traveling and in storage.
              </p>
            </li>
            <li class="FAQ__item">
              <h4>Do I have access to a trailer while it is in storage?</h4>
              <p>
                Once your shipment is in storage, you will not have access to it
                until it’s delivered to your final destination. We recommend
                taking any important documents, valuables or other items you’ll
                need right away with you
              </p>
            </li>
          </ul>
          <ul class="FAQ__list">
            <li class="FAQ__item">
              <h4>What do I do when I’m ready for delivery?</h4>
              <p>
                It’s easy! Just call your local service center to schedule a
                delivery appointment.
              </p>
              <p>
                For pricing and additional information, call XXX-XXX-XXXX, and ask
                about trailer storage. Want to know more about how U-Move trailer
                moves work?
              </p>
            </li>
          </ul>
          <div class="FAQ-decor">
            <span>?</span>
          </div>
        </div>
      </div>
    </div>

    <div class="container container-small">
      <div class="storage__row storage__row--end">
        <div class="storage__row-title">
          <h2>Moving Container Storage</h2>
        </div>
        <div class="storage__box storage__box--end">
          <ul class="storage__box-list">
            <li class="storage__box-item">
              <h3>Options for Moving Container Storage</h3>
              <p>
                Our container storage make it easy and affordable to move and
                store your belongings. Simply place your items into a moving
                container, and we’ll store them until you’re set for delivery at
                your new home.
              </p>
              <p>
                There’s no need to unload everything into a temporary storage
                facility, only to load it up again and drive it to your
                destination. Both short- and long-term storage is available at one
                of our secure service centers. A moving consultant can help you
                decide whether you need storage at origin, in transit or at
                destination.
              </p>
            </li>
            <li class="storage__box-item">
              <h3>Options for Moving Container Storage</h3>
              <ul>
                <li>
                  <p>We deliver the containers to your home</p>
                </li>
                <li>
                  <p>You have up to three business days to load</p>
                </li>
                <li>
                  <p>
                    We pick up the containers and store them at a secure service
                    center
                  </p>
                </li>
                <li>
                  <p>
                    When you’re ready for delivery, we bring the containers to
                    your new home and allow up to three business days to unload
                  </p>
                </li>
                <li>
                  <p>We pick up the empty containers</p>
                </li>
              </ul>
            </li>
          </ul>
          <div class="storage__banner">
            <div class="banner">
              <div class="banner__text-box">
                <h3>Moving consultant <br>available 24/7</h3>
                <div class="banner__tel">
                  <svg xmlns="http://www.w3.org/2000/svg" width="37" height="38" viewBox="0 0 37 38">
                    <path fill="#0074C1" fill-rule="nonzero" d="M35.956 26.978l-.208-.379c-1.471-2.668-9.526-4.035-9.6-4.035l-.178-.008-.803.112c-1.255.29-2.303 1.167-3.752 2.512-1.872-1.093-3.893-2.795-5.104-4.036-1.241-1.27-2.965-3.545-3.894-5.321 1.635-1.606 2.78-2.876 2.853-4.586 0-.074-1.025-8.182-3.633-9.788l-.379-.23C10.545.795 9.215 0 7.595 0a4.94 4.94 0 0 0-1.263.17 5.52 5.52 0 0 0-1.486.677C3.635 1.68 1.428 3.441.35 5.767-.652 8.057.024 19.732 8.776 28.881 17.068 37.56 26.698 38 28.548 38c.476 0 1.122-.022 1.664-.119l.208-.007.498-.186c2.393-.981 4.228-3.1 5.083-4.251 1.998-2.587.468-5.478-.045-6.459z" />
                  </svg>
                  <span>+1-541-754-3010</span>
                </div>
                <p>
                  If you need loading or unloading help, ask your U-Move moving
                  consultant for a recommendation.
                </p>
                <p>Online reservation discount ends October 31, 2019</p>
              </div>
              <div class="banner__image">
                <img src="<?php echo get_template_directory_uri();?>/_slicing/data/img/hero-checklist1.jpg" alt="" />
              </div>
            </div>
          </div>
        </div>
      </div>
    </div>
  </section>
  <?php get_footer();?>