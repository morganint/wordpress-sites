<?php
/*
Template Name: Order Tracking
*/
get_header();
	
	while(have_posts()){
		the_post();
		$pthid = get_post_thumbnail_id();
		$stp3p_iimg_title   = get_field('stp3p_iimg_title');
		$stp3p_info_iphone  = get_field('stp3p_info_iphone');
		$stp3p_info_iphone2 = get_field('stp3p_info_iphone2');
		$stp3p_info_phone   = get_field('stp3p_info_phone');
	}

        

	if( isset($_POST['sord_number']) && $_POST['sord_number'] && get_post_meta(decode($_POST['sord_number']), 'st_wp_post_id', true) ){
		$post_id = decode($_POST['sord_number']);
		
		if(get_post_meta($post_id, 'ord_payed_yn', true) == 'yes'){
			
			query_posts('post_type=shiporder&p='.$post_id);
			if(have_posts()){
				while(have_posts()){
				the_post(); ?>
				<section class="hero hero--empty"></section>
				<section class="track-info">
					<div class="container-small">
					  <div class="track-info__text-box">
						<div class="track-info__title">
						  <h1>Tracking Information</h1>
						</div>

						<div class="track-info__text">
						  <h3><?php _e('Reference Number', 'umove') ?>: #<?php echo $_POST['sord_number'] ?></span></h3>

						  <div class="track-info__list">
							<ul class="track-info__list-items">
								<li class="track-info__list-item">
									<?php echo get_post_meta($post_id, 'st_from', true) ?> to <?php echo get_post_meta($post_id, 'st_to', true) ?>
								</li>
								<?php if (get_field('st_pickup_date', $post_id)){ ?>    
									<li class="track-info__list-item">
										Loading on: <span><?php echo get_field('st_pickup_date', $post_id) ?></span> 
									</li>
								<?php }?>    
								<li class="track-info__list-item">
									<?php echo __('Name', 'umove').': '.get_post_meta($post_id, 'st_f1_fullname', true) ?>
								</li>
								<li class="track-info__list-item">
									<?php echo __('Phone', 'umove').': '.get_post_meta($post_id, 'st_f1_phone', true) ?>
								</li>
								<li class="track-info__list-item">
									<?php echo __('Email', 'umove').': '.get_post_meta($post_id, 'st_usr_email', true) ?>
								</li>
							</ul>
							<div class="track-info__image">
							  <img src="<?php echo get_template_directory_uri() ?>/_slicing/data/img/01-illustration.svg" alt="bitmap" />
							</div>
						  </div>

						  <?php
								$codePID = $_GET['post_id'];
								$post_id = decode($_GET['post_id']);
								$variable = get_field('ord_payed_yn', $_GET['post_id']);
								if($variable == '')
								{?>
								  <a href="<?php echo home_url();?>/transportation-step-1/?post_id=<?php the_field('st_referal_number', $codePID)?>" class="btn">Reserve Online</a>
						   <?php }?>
						</div>
					  </div>
					</div>
				</section>

					  <section class="track-move">
						<div class="container-small">
						  <div class="track-move__location">
							<div class="track-move__location-point">
							  <p class="track-move__location-title"><?php echo get_field('st_from', $post_id) ?></p>
							  <p class="track-move__location-date">
								Pickup Date: <span><?php echo get_field('st_pickup_date', $post_id) ?></span>
							  </p>
							</div>

							<div class="track-move__location-point">
							  <p class="track-move__location-title">Your Shipment</p>
							  <p class="track-move__location-now">
								Now in <?php echo get_field('now', $post_id) ?> 
							  </p>
							</div>

							<div class="track-move__location-point">
							  <p class="track-move__location-title"><?php echo get_field('st_to', $post_id) ?></p>
							  
								<p class="track-move__location-date">
								Arrival date:
								<span>
									<?php echo get_field('st_arrival_date', $post_id) ?>
								</span>
							  </p>
								
								<p class="track-move__location-date">
								Estimated time:
								<span>
									 
									
									<?php 
									$preptime = get_field('estimating_time', $post_id);
									$preparr = explode(':', $preptime);
									$hour = $preparr[0];
									$min = $preparr[1];
									echo '<time content="PT'.$hour.'H'.$min.'M" itemprop="prepTime">'.$preptime.'</time>';
									?>
									
								</span>
							  </p>
							</div>
						  </div>
						  <div class="track-move__line">
							<div class="track-move__arrow"></div>
						  </div>
						</div>
					  </section>

					  <section class="track-options">
						<div class="container-small">
						  <div class="track-options__list">
							<ul class="track-options__item">
							  <li class="track-options__item-image">
								<img src="<?php echo get_template_directory_uri();?>/_slicing/data/img/service.svg" alt="service" />
							  </li>
							  <li class="track-options__item-title">
								<h3>Service Options</h3>
							  </li>
							  <li class="track-options__item-text">
								<p>Dedicated Service</p>
								<!--<p>26ft box truck with lift-gate</p>-->
							  </li>
							  <!--<li class="track-options__item-comment">
								<span>Includes taxes and fuel</span>
							  </li>-->
							</ul>
						  </div>
						  <div class="track-options__list">
							<ul class="track-options__item">
							  <li class="track-options__item-image">
								<img src="<?php echo get_template_directory_uri();?>/_slicing/data/img/trailer-img.png" alt="Trailer" />
							  </li>
							  <li class="track-options__item-title">
								<h3>EQUIPMENT</h3>
							  </li>
							  <li class="track-options__item-text">
								<p>26ft box truck with lift-gate</p>
								<p>Includes taxes & fuel</p>
								<!--<p>Adjust Price Per Ft: $151</p>-->
							  </li>
							</ul>
						  </div>
						  <div class="track-options__list">
							<ul class="track-options__item">
							  <li class="track-options__item-image">
								<img src="<?php echo get_template_directory_uri();?>/_slicing/data/img/door.svg" alt="Door-to-Door" />
							  </li>
							  <li class="track-options__item-title">
								<h3>Door-to-Door</h3>
							  </li>
							  <li class="track-options__item-text">
								<p>Summary price</p>
							  </li>
							  <li class="track-options__item-price">$<?php //echo get_post_meta($post_id, 'st_summ', true) ?>
								<?php echo get_field('st_summ', $post_id) ?>
								</li>
							</ul>
						  </div>
						</div>
					  </section>
						<style> .track-banner__title {margin-bottom: 0px!important;}</style>
						<section class="track-banner">
							<div class="container-small">
							  <div class="track-banner__title">
								<h3>Door-to-Door Delivery</h3>
								<p>Let us come to you! Here’s how it works:</p>
							  </div>
							  <div class="track-banner__text-box">
								<p>
								  Load your belongings at your <?php echo get_field('st_from', $post_id) ?> address.
								</p>
								<p>
								  Your Move takes care of all the transportation including fuel, taxes and
								  driver fees.
								</p>
								<p>Unload your belongings at your <?php echo get_field('st_to', $post_id) ?> address.</p>
							  </div>
							</div>
						</section>

						<?php
						get_template_part('template_part/block_info2');
						get_template_part('template_part/block_tracking_section');
					} 
				}
		}else{ ?>
			<!--<section class="track-info">
				<div class="container-small">
					<div class="track-info__title">
					  <h1><?php //_e('Number not paid', 'umove') ?></h1>
					</div>
				</div>
			</section>-->
			
			<section class="service service--fill">
				<div class="error-message">
					<p>To check tracking information data, you must complete payment. Please &nbsp;&nbsp;<a href="<?php echo home_url();?>/transportation-step-1/?post_id=<?php echo $_POST['sord_number'] ?>">click here</a> to be redirected to the payment page or <a href="https://yourmovetoday.com/contact/">contact our team here.</a>.
				</div>
				
				<div class="location__search">
				<form class="search search--reference" action="<?php the_field('opt_orderinfo_page_url', 'option') ?>" method="post" id="sord_form">
					<div class="inputs">
					<input type="submit" name="sord_sbmt"   class="search__submit" value="" disabled/>
					<input type="text"   name="sord_number" placeholder="Search / Reference Number " class="search__input" />
					</div>
					<div class="sorted_btn">
						<button class="btn sord_sbmt_but" disabled><?php _e('Find', 'umove') ?></button>
					</div>
				</form>
			</div>
			</section>
			
			
		<?php }
	}else{ ?>
		<section class="service service--fill">
			<div class="error-message">
    	        <p>Reference number not recognized. Please try again or
            	<a href="https://yourmovetoday.com/contact/">contact our team here</a>.</p>
			</div>
			
			<div class="location__search">
				<form class="search search--reference" action="<?php the_field('opt_orderinfo_page_url', 'option') ?>" method="post" id="sord_form">
					<div class="inputs">
					<input type="submit" name="sord_sbmt"   class="search__submit" value="" disabled/>
					<input type="text"   name="sord_number" placeholder="Search / Reference Number " class="search__input" />
					</div>
					<div class="sorted_btn">
						<button class="btn sord_sbmt_but" disabled><?php _e('Find', 'umove') ?></button>
					</div>
				</form>
			</div>
		</section><?php
	} ?>
	
	<?php get_footer(); ?>