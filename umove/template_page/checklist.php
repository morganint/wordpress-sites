<?php 
/*Template Name: Checklist*/
get_header();?>
  <main>
    <section class="hero" style="background-image: url(<?php the_field('hero_image'); ?>);">
      <div class="wrapper"></div>
      <div class="container">
        <div class="hero__content">
          <div class="hero__text-box hero__text-box--checklist">
            <h1>
              <?php the_field('title'); ?>
            </h1>
            <?php the_field('content'); ?>

            <a class="btn btn-mobile-form" href="">Show my quote</a>
          </div>
          <?php get_template_part('template_part/quote') ?>
        </div>
      </div>
    </section>

    <section class="stages">
    	<div class="container">
		    <h2>Here’s a five step checklist to get ready for your smooth as silk move:</h2>
		    <div class="stages__box">
			    <div class="stages__list">
				    <div id="accordion" class="stages__items accordion-ui"><?php
					    $i=0;
					    while(have_rows('steps')){
						    the_row(); ?>
						    <h4 class="trigger"><span><?php echo(++$i<10 ?'0'.$i :$i) ?></span> <?php the_sub_field('title') ?></h4>
					    	<div class="inner">
						    	<?php the_sub_field('content') ?>
					  	</div><?php 
				  	} ?>
				  </div>
			  </div>
			  <div class="stages__image"><?php
				  $img = wp_get_attachment_image_src(get_field('checklist_desktop'), 'thumbnail_501x841');
				  $image_alt = get_post_meta(get_field('checklist_desktop'), '_wp_attachment_image_alt', true); ?>
				  <img src="<?php the_field('checklist_desktop'); ?>" alt="<?php echo($image_alt ?htmlspecialchars($image_alt) :htmlspecialchars(get_the_title())) ?>" width="838px" height="918px">
			</div>
		</div>
	</div>
</section>
        <?php get_template_part('template_part/block_home_4') ?>
        <section class="location">
          <div class="container">
            <div class="location__title">
              <h2><?php the_field('subtitle'); ?></h2>
              <?php the_field('subcontent'); ?>
            </div>

            <div class="location__search">
                <form class="search search--reference" action="<?php the_field('opt_orderinfo_page_url', 'option') ?>" method="post" id="sord_form">
                    <div class="inputs">
                        <input type="submit" name="sord_sbmt"   class="search__submit" value="" disabled/>
                        <input type="text"   name="sord_number" placeholder="Search / Reference Number " class="search__input" />
                    </div>
                    <div class="sorted_btn">
                        <button class="btn sord_sbmt_but" disabled><?php _e('Find', 'umove') ?></button>
                    </div> 			
                </form>
            </div>
          </div>
        </section>

    <section class="map" id="map">
	<div class="container">
		<?php the_field('hom_b6_svg', 2) ?>
	</div>
</section>
	
  </main>

  <?php get_footer();?>