<?php
/*
Template Name: Transportation step 1
*/
get_header(); ?>

<?php while(have_posts()){
	the_post(); ?>
	
<?php } ?>
	
	<?php 
	if( isset($_GET['post_id']) && $_GET['post_id'] && get_post_meta(decode($_GET['post_id']), 'st_wp_post_id', true) ){
		$codePID                        = $_GET['post_id'];
		$_GET['post_id']                = decode($_GET['post_id']);
		$_POST['st_from']      			= get_post_meta($_GET['post_id'], 'st_from', true);
		$_POST['st_to']        			= get_post_meta($_GET['post_id'], 'st_to', true);
		$_POST['st_day']       			= get_post_meta($_GET['post_id'], 'st_day', true);
		$_POST['st_size']      			= get_post_meta($_GET['post_id'], 'st_size', true);
		$_POST['st_usr_email'] 			= get_post_meta($_GET['post_id'], 'st_usr_email', true);
		$_POST['st_f1_phone'] 			= get_post_meta($_GET['post_id'], 'st_f1_phone', true); ?>
		<section class="hero hero--empty"></section>
		
		<section class="quote quote1">
			<div class="container">
				<div class="track-info__title">
					<h1><?php the_field('spagetr_stitle') ?></h1>
				</div>
				<div class="tracks">
					<div class="tracks__inner">
						<div class="tracks__text">
							<h3 class="tracks__title"><p><span style="text-transform: capitalize;"><?php echo get_post_meta($_GET['post_id'], 'st_f1_fullname', true); ?></span>, thank you for requesting a quote from Your Move.<br>We appreciate the opportunity to earn your business! </p></h3>
							<!--<h3 class="tracks__title"><?php //the_field('stp1p_stitle') ?></h3>-->
						</div>
						<div class="tracks__image">
							<?php the_field('stp1p_rimg') ?>
						</div>
					</div>
				</div>
				<div class="quote__inner">
					<div class="quote__left">
						<div class="quote-trans">
							
                            <div class="quote-trans__top">
								<div class="quote-trans__item quote-trans__item--left">
									<svg xmlns="http://www.w3.org/2000/svg" width="27" height="42" viewBox="0 0 27 42"><g fill="none" fill-rule="evenodd"><path fill="#FFF" d="M13.46 42C22.487 28.33 27 19.063 27 14.202 27 6.911 20.956 1 13.5 1S0 6.91 0 14.202C0 19.063 4.487 28.33 13.46 42z"/><circle cx="13.5" cy="13.5" r="4.5" fill="#002035"/></g></svg>
									<div class="quote-trans__item-inner">
										<h4 class="quote-trans__item-title"><?php echo $_POST['st_from'] ?></h4>
										<span class="quote-trans__item-subtitle"><?php _e('Start', 'umove') ?></span>
									</div>
								</div>
								<div class="quote-trans__item quote-trans__item--right">
									<div class="quote-trans__item-inner">
										<h4 class="quote-trans__item-title"><?php echo $_POST['st_to'] ?></h4>
										<span class="quote-trans__item-subtitle"><?php _e('Finish', 'umove') ?></span>
									</div>
									<svg xmlns="http://www.w3.org/2000/svg" width="27" height="42" viewBox="0 0 27 42"><g fill="none" fill-rule="evenodd"><path fill="#FFF" d="M13.46 42C22.487 28.33 27 19.063 27 14.202 27 6.911 20.956 1 13.5 1S0 6.91 0 14.202C0 19.063 4.487 28.33 13.46 42z"/><circle cx="13.5" cy="13.5" r="4.5" fill="#002035"/></g></svg>
								</div>
							</div>
                            
							<div class="quote-trans__line">
								<span></span>
								<span></span>
							</div>
							<div class="quote-1">
								<div class="quote-trans__car"><?php
									while(have_rows('stp1p_car_list')){
										the_row();
										$img = wp_get_attachment_image_src(get_sub_field('stp1p_car_list_img'), 'thumbnail_563x526');
										$image_alt = get_post_meta(get_sub_field('stp1p_car_list_img'), '_wp_attachment_image_alt', true); ?>
										<?php if($img){?>
										<img src="<?php echo $img[0]; ?>" alt="<?php echo($image_alt ?htmlspecialchars($image_alt) :htmlspecialchars(get_the_title())) ?>">
										<?php }?>
									<?php } ?>
								</div>
								<div class="quote-trans__car-nav" style="display:none;"><?php
									while(have_rows('stp1p_car_list')){
										the_row();
										$img = wp_get_attachment_image_src(get_sub_field('stp1p_car_list_img'), 'thumbnail_97x114');
										$image_alt = get_post_meta(get_sub_field('stp1p_car_list_img'), '_wp_attachment_image_alt', true); ?>
										<?php if($img){?>
										<img src="<?php echo $img[0]; ?>" alt="<?php echo($image_alt ?htmlspecialchars($image_alt) :htmlspecialchars(get_the_title())) ?>">
										<?php }?>
									<?php }
									 ?>
								</div> 
							</div>
							<div class="quote-2 video">
							<iframe width="560" height="315" src="https://www.youtube.com/embed/s6uwOCJW9fs" frameborder="0" allow="accelerometer; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>
							</div>
						</div>
						<div class="tracks-call">
							<h3 class="tracks__title"><?php the_field('stp1p_b3_title') ?></h3>
							<div class="tracks-call__text">
								<?php the_field('stp1p_b3_content') ?>
							</div>
							<ul class="tracks-call__list"><?php
								while(have_rows('stp1p_b3_list')){
									the_row(); ?>
									<li><?php the_sub_field('stp1p_b3_list_title') ?></li>
									<?php 
								} ?>
							</ul>
						</div>
					</div>
					
					<form class="tracks-form step-1" method="post" action="<?php the_field('opt_tsp_spep_2_page', 'option') ?>?post_id=<?php echo $codePID ?>">
						<div class="tracks-form__item">
							<h2 class="tracks-form__title"><?php the_field('stp1p_b4_title') ?></h2><?php
							the_field('stp1p_b4_stitle');
							$i = 0;
							while(have_rows('stp1p_b4_cart_list', $_GET['post_id'])){
								the_row();
								$txt = htmlspecialchars(get_sub_field('stp1p_b4_cart_list_title')); 
                                $price_car = get_sub_field('stp1p_b4_cart_list_price'); ?>
								<label class="label-radio">
									<input type="radio" name="st_cartype" class="st_cartype" id="st_cartype<?php echo $i ?>" value="<?php echo $txt ?>" <?php echo($i++ == 0 ?'checked' :'') ?> data-val="<?php echo $price_car ?>" <?php echo($price_car == $mincarprice ?'checked' :'') ?>>
									<span class="label-radio__inner"><?php echo $txt ?></span>
								</label><?php
							} ?>
						</div>
						<div class="tracks-form__total">
							<h3 class="tracks-form__total-title">
								<?php _e('Total', 'umove') ?>:
								<span class="tracks-form__total-price">$<?php the_field('st_summ', $_GET['post_id']) ?>
									<input type="text" id="total-price" name="st_summ" value="<?php the_field('st_summ', $_GET['post_id']) ?>" data-val="<?php the_field('st_summ', $_GET['post_id']) ?>" readonly>
								</span>
							</h3>
							<p class="tracks-form__total-info">
								<?php the_field('stp1p_b4_subprice') ?>
							</p>
						</div>
						<div class="tracks-bottom">
							<div class="tracks-mail">
								<input type="hidden" name="st_usr_email" value="<?php echo $_POST['st_usr_email'] ?>" placeholder="Email Address" required="required" readonly>
							</div>
                             <?php
							if(get_field('ord_payed_yn', $_GET['post_id']) != 'yes'){ ?>
                                    <button type="submit" class="btn tracks-btn">
                                        <?php the_field('stp1p_b4_submit_title') ?>
                                    </button>
                                <?php } ?>
                            <div class="tracks-form__call">
								<?php the_field('stp1p_b4_submit_phone') ?>
							</div>
							
							<br/><br/>
							<p><?php //echo __('Name', 'umove').': '.get_post_meta($_GET['post_id'], 'st_f1_fullname', true) ?></p>
							<p><?php //echo __('Phone', 'umove').': '.get_post_meta($_GET['post_id'], 'st_f1_phone', true) ?></p>
							<p><?php //echo __('Email', 'umove').': '.get_post_meta($_GET['post_id'], 'st_usr_email', true) ?></p>
							
						</div>
						<input type="hidden" name="st_from"       value="<?php echo $_POST['st_from'] ?>">
						<input type="hidden" name="st_to"         value="<?php echo $_POST['st_to'] ?>">
						<input type="hidden" name="st_day"        value="<?php echo $_POST['st_day'] ?>">
						<input type="hidden" name="st_size"       value="<?php echo $_POST['st_size'] ?>">
						<input type="hidden" name="st_f1_phone"   value="<?php echo $_POST['st_f1_phone'] ?>">
					</form>
				</div>
			</div>
		</section>
		
		<?php get_template_part('template_part/block_info') ?>
		
	<?php }else{ ?>
		<section class="service service--fill">
		<div class="error-message">
    	        <p>Reference number not recognized. Please try again or
            	<a href="https://yourmovetoday.com/contact/">contact our team here</a>.</p>
			</div>
			
			<div class="location__search">
			<form class="search search--reference" action="<?php the_field('opt_orderinfo_page_url', 'option') ?>" method="post" id="sord_form">
				    <div class="inputs">
                        <input type="submit" name="sord_sbmt"   class="search__submit" value="" disabled/>
				        <input type="text"   name="sord_number" placeholder="Search / Reference Number " class="search__input" />
                    </div>
                    <div class="sorted_btn">
                        <button class="btn sord_sbmt_but" disabled><?php _e('Find', 'umove') ?></button>
                    </div>    
			</form>
			</div>
		</section>
	<?php } ?>
	
<?php get_footer() ?>