<?php
/*
Template Name: Transportation step 2
*/
get_header(); ?>

<?php while(have_posts()){
	the_post(); ?>
	
<?php } ?>
	
	<?php //===check previous steps===
	print_r($_POST);
	if(
		isset($_GET['post_id'])&& isset($_POST['st_from'])&& isset($_POST['st_to']) && isset($_POST['st_day']) && isset($_POST['st_size']) && isset($_POST['st_f1_phone']) && //===from home form=== 
		isset($_POST['st_summ']) && isset($_POST['st_usr_email']) 
		//===from step 1===
	){
		if(!isset($_POST['st_cartype'])){
			$_POST['st_cartype'] = '';
		}
		
		$codePID = $_GET['post_id'];
		$post_id = decode($_GET['post_id']); ?>
		
		<section class="hero hero--empty"></section>
		<section class="quote reserve">
			<div class="container">
				<div class="track-info__title">
					<h1><?php the_field('spagetr_stitle') ?></h1>
				</div>
				<form class="quote__inner" method="post" action="<?php the_field('opt_tsp_spep_3_page', 'option') ?>?post_id=<?php echo $codePID ?>">
					<div class="quote__left">
						<div class="reserve__item reserve-contact">
							<h3 class="reserve__item-title">
								<?php the_field('stp2p_b1_title') ?>
								<span class="reserve__item-icon"></span>
							</h3>
							<div class="reserve__item-in">
								<div class="reserve__item-content">
									<h4 class="reserve__item-subtitle"><?php _e('Contact Information', 'umove') ?></h4>
									<div class="reserve__item-form">
										<input type="text"  name="st_f1_fullname" placeholder="Full Name" id="txtFullName" value="<?php echo get_post_meta($post_id, 'st_f1_fullname', true) ?>" readonly>
										<input type="email" name="st_usr_email"   placeholder="Email Address" id="txtEmailAddress" required="required" value="<?php echo $_POST['st_usr_email'] ?>" readonly>
										<input type="tel"   name="st_f1_phone"    placeholder="Phone Number"  id="txtPhone"        value="<?php echo get_post_meta($post_id, 'st_f1_phone', true) ?>"    readonly>
										<input type="tel"   name="st_usr_phone1"  placeholder="Alternate Phone Number (optional)">
									</div>
									<!--<label class="label-checkbox">
										<input type="checkbox" name="st_moove_messaging_yn" checked>
										<span class="label-checkbox__inner"><?php //_e('Send me text message updates about my move.', 'umove') ?></span>
									</label>-->
									<h4 class="reserve__item-subtitle"><?php _e('Contact Information', 'umove') ?></h4>
									
									
                                    <div class="reserve__item-form">
										<input type="text" name="st_usr_contact_addr_addr"       placeholder="Address"  id="txtOriginAddress">
										<input type="text" name="st_usr_contact_addr_appartment" placeholder="Apartment/Storage Unit Name">
										<input type="text" name="st_usr_contact_addr_city"       placeholder="City"     id="txtOriginCity"  value="<?php echo $_POST['st_from'] ?>" readonly>
										<input type="text" name="st_usr_contact_addr_zip"        placeholder="Zip"      id="txtOriginZip">
									</div>
                                    
									<h4 class="reserve__item-subtitle"><?php _e('Destination Address', 'umove') ?></h4>
									
									
                                    <div class="reserve__item-form">
										<input type="text" name="st_usr_destination_addr_addr" placeholder="Address" id="txtDestinationAddress">
										<input type="text" name="st_usr_destination_addr_appartment" placeholder="Apartment/Storage Unit Name">
										<input type="text" name="st_usr_destination_addr_city" placeholder="City" id="txtDestinationCity" value="<?php echo $_POST['st_to'] ?>" readonly> 	
										
										<input type="text" placeholder="Zip" name="user_destination_address_zip" id="txtDestinationZip">
										
										
									</div>
                                    
								</div>
							</div>
						</div>
						
						
						
						
						<div class="reserve__item reserve-delivery">
							<h3 class="reserve__item-title">
								<?php the_field('stp2p_b3_title') ?>
								<span class="reserve__item-icon"></span>
							</h3>
							<div class="reserve__item-in">
								<div class="reserve-details__inner">
									<!-- <div class="reserve-details__item">
										<h4 class="reserve__item-subtitle"><?php /*_e('Delivery Date & Time', 'umove') ?></h4>
										<div class="reserve-delivery__date">
											<input type="text" id="txtSpotDate" name="st_delidery_date" placeholder="DD/MM/YYYY">
											<input type="text" id="deliveryTime" name="st_delidery_time" placeholder="hh:mm">
											// <a href="#uID" class="reserve-change"><?php _e('Change', 'umove') ?></a> 
										</div>
										<p><?php the_field('st_delidery_datetime_descr', $post_id, false, false) ?> <?php echo $_POST['st_to'] ?></p>
										//<p><?php //the_field('stp2p_b3_date_title') ?><?php //echo $_POST['st_to']*/ ?></p>
									</div> -->
									<div class="reserve-details__item reserve-details__item--single">
										<h4 class="reserve__item-subtitle"><?php _e('Pickup Date & Time ', 'umove') ?></h4>
										<div class="reserve-delivery__date">
											<input type="text" id="pickUpDate" name="st_pickup_date" placeholder="MM/DD/YYYY">
											<input type="text" id="pickUpTime" name="st_pickup_time" placeholder="hh:mm">
										</div>
										<?php the_field('st_pickup_datetime_descr', $post_id) ?>
										<!--<p><?php //the_field('stp2p_b3_date_title') ?><?php //echo $_POST['st_to'] ?></p>-->
									</div>
									
								</div>
								<div class="reserve-delivery__info">
											<h4 class="reserve__item-subtitle"><?php the_field('stp2p_b3_info_title') ?></h4>
											<?php the_field('stp2p_b3_info_content') ?>
										</div>
								<div class="reserve-delivery__attention">
									<h4 class="reserve__item-subtitle"><?php the_field('stp2p_b3_varning_title') ?></h4>
									<?php the_field('stp2p_b3_varning_content') ?>
								</div>
							</div>
						</div>
						
						<div class="reserve__item reserve-transit">
							<h3 class="reserve__item-title">
								<?php the_field('stp2p_b4_title') ?>
								<span class="reserve__item-icon"></span>
							</h3>
							<div class="reserve__item-in">
								<div class="reserve-details__inner">
									
									<!--<div class="reserve-details__item">
										<h4 class="reserve__item-subtitle"><?php //the_field('stp2p_b4_list_stitle') ?></h4>
										<label class="label-radio">
											<input type="radio" name="st_delivery_days" data-date="0" value="0" id="rdoExpeditedOptions-0" checked="checked">
											<div class="label-radio__inner">
												<h5><?php //_e('Standard', 'umove') ?> - $0</h5>
												<?php //_e('Standard', 'umove') ?>
											</div>
										</label>
											
										<?php /*if(get_field('stp2p_b4_list', $post_id)):?>
										<h4 class="reserve__item-subtitle"><?php echo 'Guaranteed Transit' ?></h4>
										<?php endif;?>
										<?php
										$i = 0;
										$j = 7;
										if(have_rows('stp2p_b4_list', $post_id)){?>
											<?php while(have_rows('stp2p_b4_list', $post_id)){
												the_row(); ?>
												<label class="label-radio">
													<input type="radio" name="st_delivery_days" data-date="<?php echo $j-- ?>" value="<?php the_sub_field('stp2p_b4_list_price') //the_sub_field('stp2p_b4_list_title')?>" id="rdoExpeditedOptions-<?php echo ++$i ?>">
													<div class="label-radio__inner">
														<h5><?php the_sub_field('stp2p_b4_list_title') ?> - $<?php the_sub_field('stp2p_b4_list_price') ?></h5>
														<?php the_sub_field('stp2p_b4_list_content') ?>
													</div>
												</label><?php
											}
										}//else{ ?>
											<!--<label class="label-radio">
												<input type="radio" name="st_delivery_days" data-date="0" value="0" id="rdoExpeditedOptions-0" checked="checked">
												<div class="label-radio__inner">
													<h5><?php //_e('Standard', 'umove') ?> - $0</h5>
													<?php //_e('Standard', 'umove') ?>
												</div>
											</label>--><?php
										} */?>
									</div>-->
									
									<div class="reserve-delivery__info">
										<h4 class="reserve__item-subtitle"><?php the_field('stp2p_b4_title_1') ?></h4>
										<p>
											<?php the_field('stp2p_b4_content_1') ?>
											<!-- <span><?php //echo ''.$_POST['st_from'].' to '.$_POST['st_to'].' and should arrive in ' ?></span> -->
                                            <?php $arrival = get_field('st_transit_days', $post_id);?>
											<span><?php echo ''.$_POST['st_from'].' to '.$_POST['st_to'].' and should arrive in '.$arrival.' days from loading date' ?></span>
											<span class="summary-cart-spot-date-line-item"></span>
										</p>
									</div>
									<!-- <div class="reserve-delivery__info">
										<h4 class="reserve__item-subtitle"><?php the_field('stp2p_b4_title_2') ?></h4>
										<?php //the_field('stp2p_b4_content_2') ?>
										<p>Know the exact day your shipment will arrive in <?php echo $_POST['st_to'].'.' ?> We'll refund up to 100% of your transportation charges if your shipment doesn't arrive on your guaranteed delivery date By knowing your exact delivery date, you can schedule your unloading help in advance</p>
									</div> -->
								</div>
							</div>
						</div>
						
						<div class="reserve__item reserve-more">
							<h3 class="reserve__item-title">
								<?php the_field('stp2p_b5_title') ?>
								<span class="reserve__item-icon"></span>
							</h3>
							<div class="reserve__item-in">
								<div class="reserve-details__inner">
									
									<div class="reserve-details__item"><?php
										if(have_rows('stp2p_b5_list1', $post_id)){ ?>
											<h4 class="reserve__item-subtitle"><?php the_field('stp2p_b5_list1_btitle') ?></h4>
											<p><?php the_field('stp2p_b5_list1_bstitle') ?></p>
											<select id="selStorage" ><?php
												while(have_rows('stp2p_b5_list1', $post_id)){
													the_row(); ?>
													<option value="<?php the_sub_field('stp2p_b5_list1_price') ?>"><?php the_sub_field('stp2p_b5_list1_title') ?></option>
												<?php } ?>
											</select><?php
										}
										
										if(have_rows('stp2p_b5_list2', $post_id)){ ?>
											<h4 class="reserve__item-subtitle"><?php the_field('stp2p_b5_list2_btitle') ?></h4>
											<p><?php the_field('stp2p_b5_list2_bstitle') ?></p>
											<select id="selAdditionalLiability"><?php
												while(have_rows('stp2p_b5_list2', $post_id)){
													the_row(); ?>
													<option value="<?php the_sub_field('stp2p_b5_list2_price') ?>"><?php the_sub_field('stp2p_b5_list2_title') ?></option>
												<?php } ?>
											</select><?php
										} ?>
									</div>
									
									<!--<?php /*if(have_rows('stp2p_b5_list3', $post_id)){ ?>
										<div class="reserve-details__item">
											<h4 class="reserve__item-subtitle"><?php the_field('stp2p_b5_list3_btitle') ?></h4>
											<p><?php the_field('stp2p_b5_list3_bstitle') ?></p>
											<select id="selWeightTickets"><?php
												while(have_rows('stp2p_b5_list3', $post_id)){
													the_row(); ?>
													<option value="<?php the_sub_field('stp2p_b5_list3_price') ?>"><?php the_sub_field('stp2p_b5_list3_title') ?></option>
												<?php } ?>
											</select>
										</div>
									<?php }*/ ?>-->
								</div>
								
								<div class="reserve-details__inner">
									<div class="reserve-details__item">
										<h4 class="reserve__item-subtitle"><?php the_field('stp2p_b5_i1_title') ?></h4>
										<label class="label-checkbox">
											<input type="checkbox" name="st_mssg_rate_info_1" checked>
											<span class="label-checkbox__inner"><?php the_field('stp2p_b5_i1_contenr') ?></span>
										</label>
									</div>
									<div class="reserve-details__item">
										<h4 class="reserve__item-subtitle"><?php the_field('stp2p_b5_i2_title') ?></h4>
										<label class="label-checkbox">
											<input type="checkbox" name="st_mssg_car_ship_info_1">
											<span class="label-checkbox__inner"><?php the_field('stp2p_b5_i2_contenr') ?></span>
										</label>
									</div>
								</div>

								<div class="reserve-details__inner">
									<div class="reserve-details__item">
										<h4 class="reserve__item-subtitle">Packing Materials</h4>
										<label class="label-checkbox">
											<input type="checkbox" name="st_mssg_rate_info_2">
											<span class="label-checkbox__inner">Please send me more materials on moving box kits, packing materials and furniture pads</span>
										</label>
									</div>
									<div class="reserve-details__item">
										<h4 class="reserve__item-subtitle">Storage Options</h4>
										<label class="label-checkbox">
											<input type="checkbox" name="st_mssg_car_ship_info_2">
											<span class="label-checkbox__inner">I would like more information on storage options.</span>
										</label>
									</div>
								</div>

								
							</div>
						</div>
						<div class="reserve__item reserve-payment">
							<h3 class="reserve__item-title">
								<?php the_field('stp2p_b6_title') ?>
								<span class="reserve__item-icon"></span>
							</h3>
							
                            
                            <div class="reserve__item-in">
								<h4 class="reserve__item-subtitle"><?php _e('Card Information', 'umove') ?></h4>
								<div class="reserve-payment__inner">
									<input type="text" placeholder="Full Name" id="txtCreditCardName" name="st_usr_name_card">
									<div class="reserve-payment__number">
										<input type="text" placeholder="Card Number" name="st_visa_card_number">
											<!-- Oksana -->
										<span class="reserve-payment__number-valid">&#10003;</span>
										<div class="reserve-payment__image"><img src="<?php echo get_template_directory_uri() ?>/_slicing/data/img/icon-card.png"></div>
									</div>
									<div class="reserve-payment__code">
										<select class="select-month" id="selCreditCardExpirationMonth" name="st_visa_card_exp_month">
											<option value="0" data-display="Exp. Month">---</option>
											<option value="01">01 - Jan</option>
											<option value="02">02 - Feb</option>
											<option value="03">03 - Mar</option>
											<option value="04">04 - Apr</option>
											<option value="05">05 - May</option>
											<option value="06">06 - Jun</option>
											<option value="07">07 - Jul</option>
											<option value="08">08 - Aug</option>
											<option value="09">09 - Sep</option>
											<option value="10">10 - Oct</option>
											<option value="11">11 - Nov</option>
											<option value="12">12 - Dec</option>
										</select>
										<select class="select-year" id="selCreditCardExpirationYear" name="st_visa_card_exp_year">
											<option value="0" data-display="Exp. Year">---</option>
											<option value="20">2020</option>
											<option value="21">2021</option>
											<option value="22">2022</option>
											<option value="23">2023</option>
											<option value="24">2024</option>
											<option value="25">2025</option>
											<option value="26">2026</option>
											<option value="26">2027</option>
											<option value="26">2028</option>
											<option value="26">2029</option>
											<option value="26">2030</option>
											<option value="26">2031</option>
											<option value="26">2032</option>
											<option value="26">2033</option>
											<option value="26">2034</option>
											<option value="26">2035</option>
											<option value="26">2036</option>
											<option value="26">2037</option>
											<option value="26">2038</option>
											<option value="26">2039</option>
											<option value="26">2040</option>
										</select>
										<input type="number" name="st_visa_card_secr_code" placeholder="Security Code">
									</div>
								</div>
								<!-- <div class="reserve-payment__reference-wrap">
									<h4 class="reserve__item-subtitle"><?php _e('Refferal Number', 'umove') ?></h4>
									<div class="reserve-payment__reference">
										<input type="text" name="st_referal_number" placeholder="Refferal Number (optional)">
										<button class="reserve-payment__apply btn-o"><?php _e('Apply', 'umove') ?></button>
									</div>
								</div>
								<div class="reserve-delivery__info">
									<h4 class="reserve__item-subtitle"><?php the_field('stp2p_b6_pay_title') ?></h4>
									<?php the_field('stp2p_b6_pay_content') ?>
								</div> -->
							</div>
                            
						</div>
					</div>
					<a href="#uID" class="btn btn-mobile-reserve">
						<span class="btn-s"><?php _e('Show summary information', 'umove') ?></span>
						<span class="btn-h"><?php _e('Hide summary information', 'umove') ?></span>
					</a>
					<div class="tracks-form reserve-right">
						<div class="tracks-form__item tracks-form__item-contact active">
							<h2 class="tracks-form__title">
								<?php _e('Contact Information', 'umove') ?>
								<span class="reserve__item-icon"></span>
							</h2>
							<div class="tracks-form-in">
								<div class="tracks-form__info">
									<div id="summary-cart-contact-info-region"></div>
									<div id="summary-cart-contact-info-region2"></div>
									<div id="summary-cart-contact-info-region3"></div>
								</div>
							</div>
						</div>
						<div class="tracks-form__item tracks-form__item-from">
							<h2 class="tracks-form__title">
								<?php _e('Moving from', 'umove') ?>
								<span class="reserve__item-icon"></span>
							</h2>
							<div class="tracks-form-in">
								<div class="tracks-form__info">
									<div id="summary-cart-moving-from-region">
										<div class="mov-0 hidden"></div>
										<span class="mov-1 hidden"></span>
										<span class="mov-2 hidden"></span>
										<span class="mov-3 hidden"></span>
									</div>
								</div>
							</div>
						</div>
						<div class="tracks-form__item">
							<h2 class="tracks-form__title">
								<?php _e('Moving To', 'umove') ?>
								<span class="reserve__item-icon"></span>
							</h2>
							<div class="tracks-form-in">
								<div class="tracks-form__info">
									<div id="summary-cart-moving-to-region">
										<div class="mov-0 hidden"></div>
										<span class="mov-1 hidden"></span>
										<span class="mov-2 hidden"></span>
										<span class="mov-3 hidden"></span>
									</div>
								</div>
							</div>
						</div>
						<div class="tracks-form__item">
							<h2 class="tracks-form__title">
								<?php _e('Transportation', 'umove') ?>
								<span class="reserve__item-icon"></span>
							</h2>
							<div class="tracks-form-in">
							<div class="tracks-form__element">
								<!-- <label class="label-checkbox">
									<input type="checkbox" id='transportation' checked>
									<span class="label-checkbox__inner">
										<?php _e('Transportation', 'umove') ?>
										<span class="label-checkbox__price ">
											$<span id="total-p"></span>
										</span>
									</span>
								</label> -->
								<div class="tracks-form__info">
									<div><?php echo $_POST['st_cartype'] ?></div>
									<div><?php
										//foreach($_POST['st_addsrvlist'] as $item){
											//echo $item.'<br/>';
										//} ?>
									</div>
									<div>Pick up date: <span id="pick-up-date"></span></div>
									<div>Pick up time: <span id="pick-up-time"></span></div>
									<div>Estimated transit: <span id="transit-days"><?php the_field('st_transit_days', $post_id) ?> days</span></div>

									<!-- <div>Delivery date: <span class="summary-cart-spot-date-line-item"></span></div>
									<div>Delivery time: <span id="delivery-time"></span></div> -->
									<!-- <div>
										<span id="summary-cart-transit-estimate"><?php// _e('Estimated Transit Time:', 'umove') ?> Standard</span>
										<span id="summary-cart-transit-guaranteed" class="hidden">
											<div>Guaranteed Transit: <span id="guaranteed-day"></span> Business days</div>
											<span id="guaranteed-price">$</span>
										</span>
									</div> -->
								</div>
							</div>
							<!-- <div class="tracks-form__element">
								
								<div class="tracks-form__info summary-options">
									<div id="monthly-storage" class="hidden">Monthly Storage<span>$</span></div>
									<div id="liability-coverage" class="hidden">Liability Coverage<span>$</span></div>
									<div id="weight-tickets" class="hidden">Weight Tickets<span>$</span></div>
								</div>
							</div> -->
							</div>
						</div>
						<div class="tracks-form__item">
							<h2 class="tracks-form__title">
								<?php _e('Payment Information', 'umove') ?>
								<span class="reserve__item-icon"></span>
							</h2>
							
                            <div class="tracks-form-in">
								<div class="tracks-form__info">
									<div id="summary-payment-name" class="hidden"></div>
										<!-- Oksana -->
									<div id="summary-payment-number" class="hidden"></div>
									<div id="summary-expires-date" class="hidden"></div>
									<div id="summary-security-code" class="hidden"></div>
								</div>
							</div>
                            
						</div>
						<div class="tracks-form__total">
							<h3 class="tracks-form__total-title">
								<?php _e('Total:', 'umove') ?>
								<span class="tracks-form__total-price">
									$<?php the_field('st_summ', $post_id) ?>
									<input type="text" id="total-price" name="st_summ" value="<?php the_field('st_summ', $post_id) ?>" data-val="<?php the_field('st_summ', $post_id) ?>" readonly>
									<!--$<input type="text" id="total-price" name="st_summ" value="<?php //echo $_POST['st_summ'] ?>" data-val="<?php //echo $_POST['st_summ'] ?>" readonly>-->
								</span>
							</h3>
							<p class="tracks-form__total-info">Your price includes taxes, fuel & lift-gate</p>
						</div>
						
                        <?php
						if(get_field('ord_payed_yn', $post_id) != 'yes'){ ?>
                        
                        <div class="agreement">
						<label class="label-checkbox">
							<input type="checkbox" id="agreementCheckbox" name="" >
								<p class="label-checkbox__inner">I agree with <a href="<?php the_permalink(6629);?>" target="_blank">Terms and conditions</a></p>
							</label>
						</div>
						<div class="tracks-bottom">
							<button type="submit" disabled="disabled" id="submit-form" class="btn tracks-btn">Confirm Reservation</button>
						</div>
						<p class="tiny-text">Only click Confirm Reservation button once</p>
                        
                       <?php } ?>
					</div>
					
					<!--FROM home-->
					<input type="hidden" name="st_from" value="<?php echo $_POST['st_from'] ?>">
					<input type="hidden" name="st_to"   value="<?php echo $_POST['st_to']   ?>">
					<input type="hidden" name="st_day"  value="<?php echo $_POST['st_day']  ?>">
					<input type="hidden" name="st_size" value="<?php echo $_POST['st_size'] ?>">
					<!--<input type="hidden" name="st_f1_phone" value="<?php //echo $_POST['st_f1_phone'] ?>">-->
					
					<!--FROM STEP 1-->
					<?php
					 //foreach($_POST['st_addsrvlist'] as $item){
						//echo'<input type="hidden" name="st_addsrvlist[]" value="'.$item.'">';
					//} ?>
					<input type="hidden" name="st_cartype" value="<?php echo $_POST['st_cartype'] ?>">
				</form>
				<div class="reserve-img">
					<img src="<?php echo get_template_directory_uri() ?>/_slicing/data/img/bitmap-2.png">
				</div>
			</div>
		</section>
		
		<?php get_template_part('template_part/block_info') ?>
		
	<?php }else{ ?>
		<section class="service service--fill">
			<div class="error-message">
				<a href="<?php echo home_url() ?>">
					<p><?php _e('Error! Please click to go Home page.', 'umove') ?></p>
				</a>
			</div>
		</section>
	<?php } ?>
	
<?php get_footer() ?>