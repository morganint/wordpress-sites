<?php 
/*Template Name: About*/
get_header();?>
  <main>
    <section class="hero" style="background-image: url(<?php the_field('hero_image'); ?>);">
      <div class="wrapper"></div>
      <div class="container">
        <div class="hero__content">
          <div class="hero__text-box hero__text-box--checklist">
            <h1>
             <?php the_field('main_title'); ?>
            </h1>
            <?php the_field('main_content'); ?>

            <a class="btn btn-mobile-form" href="">Show my quote</a>
          </div>
          <?php get_template_part('template_part/quote') ?>
        </div>
      </div>
    </section>

    <section class="about">
      <div class="container">
        <div class="about__main-block">
          <div class="about__text-block">
            <div class="about__title">
              <h2> <?php the_field('secondary_title'); ?></h2>
            </div>
            <div class="about__text">
               <?php the_field('secondary_content'); ?>

              <div class="about__image-block">
                <img src="<?php the_field('image'); ?>" width="497px" height="442px" alt="about-us" />
              </div>

            
                
              <div class="about__links">
                <?php if(get_field('contact_us')){?>      
                <a href="<?php the_field('contact_us'); ?>" class="btn">Get in touch Now</a>
                <?php }?>  
              </div>
            </div>
          </div>
        </div>
        <div class="about__benefits-block">
          <div class="benefits">
            <ul class="benefits__list">
            <?php if( have_rows('live') ): ?>  
                <?php $counter = 1;?>
                <?php while( have_rows('live') ): the_row(); 
                $title = get_sub_field('title');
                $content = get_sub_field('content');
                ?>    
            
            <li class="benefits__item">
                <h3><?php echo $title;?></h3>
                    <?php echo $content;?>
              </li>
              <?php endwhile; ?>
            <?php endif; ?>   
              
            </ul>
          </div>
        </div>
      </div>
      <div class="about__img-below">
        <img src="<?php the_field('image_'); ?>" alt="u-move container" />
      </div>
    </section>
	  
    <?php get_template_part('template_part/block_home_4') ?>

    <section class="location">
      <div class="container">
        <div class="location__title">
          <h2><?php the_field('location_title'); ?></h2>
          <?php the_field('location_content'); ?>
        </div>

        <div class="location__search">
		    	<form class="search search--reference" action="<?php the_field('opt_orderinfo_page_url', 'option') ?>" method="post" id="sord_form">
            <div class="inputs">
              <input type="submit" name="sord_sbmt"   class="search__submit" value="" disabled/>
              <input type="text"   name="sord_number" placeholder="Search / Reference Number " class="search__input" />
            </div>
            <div class="sorted_btn">
              <button class="btn sord_sbmt_but" disabled><?php _e('Find', 'umove') ?></button>
            </div> 			
          </form>
		    </div>

      </div>
    </section>
    <section class="map" id="map">
	<div class="container">
		<?php the_field('hom_b6_svg', 2) ?>
	</div>
</section>
  </main>

 <?php get_footer();?>