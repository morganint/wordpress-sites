<?php get_header() ?>

<section class="container">
	<div class="row">
		<?php if ( have_posts() ){ ?>
			<?php while(have_posts()){
				the_post(); ?>
				<?php get_template_part('template_part/list_item') ?>
			<?php } ?>
			<?php get_template_part('template_part/pager') ?>
		<?php }else{ ?>
			<?php get_template_part('template_part/not_found') ?>
		<?php } ?>
	</div>
</section>

<?php get_footer() ?>