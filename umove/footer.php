		<footer class="main-footer">
			<div class="container">
				<div class="main-footer__nav">
					<div class="main-footer__list"><?php
						wp_nav_menu([
						   'theme_location' => 'footer1',
						   'items_wrap'     => '<ul class="main-footer__items"><li class="main-footer__item">%3$s</li></ul>',
						   'container' => false
						] ); ?>  
						<div class="main-footer__copyright">
							<a href="<?php echo home_url() ?>" class="main-footer__logo">
								<img src="<?php echo get_template_directory_uri() ?>/_slicing/data/img/logo.png" width="103px" height="40px" alt="<?php bloginfo('name') ?>" />
							</a>
							<p class="main-footer__copyright-text">
								<?php the_field('sopt_copy', 'option') ?> <?php echo date('Y');?>
							</p>
						</div>
					</div>
					<div class="main-footer__list">
						 <?php wp_nav_menu( [
							   'theme_location' => 'footer2',
							   'items_wrap'     => '<ul class="main-footer__items"><li class="main-footer__item">%3$s</li></ul>',
							   'container' => false
							] ); ?>
					</div>
					<div class="main-footer__list">
						<?php wp_nav_menu( [
							   'theme_location' => 'footer3',
							   'items_wrap'     => '<ul class="main-footer__items"><li class="main-footer__item">%3$s</li></ul>',
							   'container' => false
							] ); ?>
					</div>
					<div class="main-footer__list">
						<?php wp_nav_menu( [
						   'theme_location' => 'HeaderMenuSecond',
						   'items_wrap'     => '<ul class="main-footer__items"><li class="main-footer__item">%3$s</li></ul>',
						   'container' => false
						] ); ?>
						<div class="social">
							<p class="social__title">Social Networks</p>
							<ul class="social__list">
								<li class="social__item">
									<a class="social__link social__link--fb" href="https://www.facebook.com/yourmovetoday1/"></a>
								</li>
								<li class="social__item">
									<a class="social__link social__link--tw" href="https://twitter.com/yourmovetoday1"></a>
								</li>
								<li class="social__item">
									<a class="social__link social__link--in" href="https://www.instagram.com/yourmovetoday/"></a>
								</li>
								<li class="social__item">
									<a class="social__link social__link--linkedin" href="https://www.linkedin.com/company/your-move-today"></a>
								</li>
							</ul>
              				  <a href="https://yourmovetoday.com/contact/" class="btn btn--white">Get in touch</a>
						</div>
					</div>
				</div>
			</div>
		</footer>
		
		<div id="ex1" class="modal">
			<div id="ex1body">
				<div class="ex1img" >
					<img src="https://yourmovetoday.com/wp-content/uploads/2019/11/bitmap.png">
				</div>
				<div class='ex1text'>
					<h3 id='ex1title'>Thank you for submitting your details to Your Move!</h3>
					<p id='ex1text'></p>
					<a href="#uID" rel="modal:close" class="close-modal"><?php _e('Close', 'umove') ?></a>
				</div>	
			</div>
		</div>
		
		<div id="ex2" class="modal">
			<div id="ex2body">
				<div class="ex1img" >
					<img src="https://yourmovetoday.com/wp-content/uploads/2019/11/bitmap.png">
				</div>
				<div class='ex1text'>
					<p id='ex2text'>Next or same day bookings need to be confirmed by our transportation department, please <a href="<?php the_permalink(6323);?>"> contact us</a> to ensure availability</p>
					<a href="#uID" rel="modal:close" class="close-modal"><?php _e('Close', 'umove') ?></a>
				</div>	
			</div>
		</div>

		<script>
			var WPThemeURL = '<?php echo get_template_directory_uri(); ?>/';
			var WPajaxURL = '<?php echo admin_url('admin-ajax.php'); ?>';
		</script>
		<?php wp_footer() ?>
	
	</body>
</html>