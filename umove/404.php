<?php get_header() ?>
	
	<section class="container features">
		<div class="row">
			<div class="col">
				<h1>404</h1>
				<?php get_template_part('template_part/not_found') ?>
			</div>
		</div>
	</section>

<?php get_footer() ?>