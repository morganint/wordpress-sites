<?php get_header() ?>

<?php while(have_posts()){
	the_post(); ?>
	<section class="container features">
		<div class="row">
			<div class="col">
				<h1><?php the_title() ?></h1>
				<?php the_content() ?>
			</div>
		</div>
	</section>
<?php } ?>

<?php get_footer() ?>