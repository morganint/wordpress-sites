<?php
/**
 * The theme header
 *
 * @package ecolinewindows
 */
?>
  <!DOCTYPE html>
<html <?php language_attributes(); ?>>
  <head>

    <!-- SSL verification by Convergine - do not remove -->
    <meta name="_globalsign-domain-verification" content="ZNHOFZQNEO7z2SGp0SBDpCTrzFIb6n2WBsxdvSoKho"/>
    <meta name="detectify-verification" content="3026b74175d8c11bf5c744eca0b13dd7"/>
    <!-- SSL verification by Convergine - do not remove end -->
    <meta charset="<?php bloginfo('charset'); ?>">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <link rel="shortcut icon" href="<?php echo get_stylesheet_directory_uri(); ?>/images/128x128.png" type="image/x-icon">
    <link rel="icon" href="<?php echo get_stylesheet_directory_uri(); ?>/images/128x128.png" type="image/x-icon">
    <link rel="apple-touch-icon" href="<?php echo get_stylesheet_directory_uri(); ?>/images/128x128.png" type="image/x-icon">
    <link rel="apple-touch-icon-precomposed" href="<?php echo get_stylesheet_directory_uri(); ?>/images/128x128.png" type="image/x-icon">

    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">

    <?php wp_head(); ?>

    <!-- pinterest snippet -->
    <meta name="p:domain_verify" content="6bbfa28d478f4efcb3f473a43a3e9674"/>
  </head>

<body <?php body_class(); ?> id="<?php (is_front_page()) ? print('page-home') : print('page-internal'); ?>">
<!--[if lt IE 8]>
<div class="alert alert-danger ancient-browser-alert">You are using an <strong>outdated</strong> browser. Please <a
        href="http://browsehappy.com/" rel="nofollow" target="_blank">upgrade your browser</a>.
</div>
<![endif]-->
  <div id="fixed-header">
    <div class="container">
      <div class="navbar navbar-expand-lg navbar-toggleable-lg row no-gutters">
        <div class="col-12 col-md-auto">
          <a id="fixed-header-logo" href="<?php echo esc_url(home_url('/')); ?>"><img
                    src="<?php echo get_stylesheet_directory_uri(); ?>/images/128x128.png" width="42" height="42"
                    alt="<?php bloginfo('name'); ?>"/></a>
          <ul class="navbar-nav" id="fixed-header-nav"></ul>
        </div>
        <div class="col">
          <ul class="navbar-nav d-none d-xl-block" id="fixed-header-nav-action"></ul>
        </div>
      </div>
    </div>
  </div>
  <header id="header">
    <div class="container">
      <div class="navbar navbar-expand-lg navbar-toggleable-lg" id="navbar-collapse-container">
        <a class="navbar-brand" href="<?php echo esc_url(home_url('/')); ?>"><img class=""
                                                                                  src="<?php echo get_stylesheet_directory_uri(); ?>/images/logo.png"
                                                                                  alt="<?php bloginfo('name'); ?>"></a>
        <button class="navbar-toggler d-lg-none" id="btn-menu-cta" type="button" data-toggle="collapse"
                data-target="#contactNavContainer" aria-controls="contactNavContainer" aria-expanded="false"
                aria-label="Contact menu">
          <i class="fa fa-phone" aria-hidden="true"></i><span class="btn-label"> Contact us</span>
        </button>
        <button class="navbar-toggler d-lg-none" id="btn-menu-trigger" type="button" data-toggle="collapse"
                data-target="#corporateNavContainer" aria-controls="corporateNavContainer" aria-expanded="false"
                aria-label="Main menu">
          <i class="navbar-toggler-icon fa fa-lg fa-bars text-primary" aria-hidden="true"></i>
        </button>
        <nav class="collapse navbar-collapse"
             id="corporateNavContainer"
             itemscope itemtype="http://schema.org/SiteNavigationElement" role="navigation">

          <?php
          wp_nav_menu(array(
            'theme_location'  => 'corporate',
            'depth'           => 2,
            'container'       => false,
            'container_class' => false,
            'container_id'    => false,
            'menu_class'      => 'nav navbar-nav mr-auto',
            'items_wrap'      => '<ul id="%1$s" class="%2$s" itemprop="about" itemscope="" itemtype="http://schema.org/ItemList">%3$s</ul>',
            'fallback_cb'     => 'WP_Bootstrap_Navwalker::fallback',
            'walker'          => new WP_Bootstrap_Navwalker(),
          ));
          ?>

        </nav>

        <div class="collapse navbar-collapse" id="contactNavContainer"></div>
        <div class="navbar-text d-none d-lg-flex">
          <ul class="navbar-nav" id="nav-action">
            <li class="nav-item d-none d-xl-flex"><a class="nav-link link phone-link contact-phone-link" href="tel: +18666446418"><i
                        class="fa fa-mobile" aria-hidden="true"></i> +1 866 644 6418</a></li>
            <li class="nav-item dropdown ">
              <a class="nav-link link btn btn-primary" href="/contact-us/" id="ctaMenuLink" data-toggle="dropdown"
                 aria-haspopup="true" aria-expanded="false">Contact Us</a>
              <nav class="dropdown-menu"
                   aria-labelledby="ctaMenuLink"
                   itemscope itemtype="http://schema.org/SiteNavigationElement" role="navigation">
                <?php wp_nav_menu(array(
                  'theme_location' => 'action',
                  'container' => false,
                  'menu_class' => 'list-unstyled',
                  'items_wrap'      => '<ul id="%1$s" class="%2$s" itemprop="about" itemscope="" itemtype="http://schema.org/ItemList">%3$s</ul>',
                )); ?>
              </nav>
            </li>
          </ul>
        </div>
      </div>

    </div>
    <div class="d-none d-lg-block" id="productNavWrapper">
      <div class="container">
        <nav class="navbar navbar-expand-lg navbar-inverse bg-primary navbar-toggleable-lg"
             id="productNavContainer"
             itemscope itemtype="http://schema.org/SiteNavigationElement" role="navigation">
          <?php
          wp_nav_menu(array(
            'theme_location' => 'primary',
            'depth' => 3,
            'container' => 'div',
            'container_class' => 'collapse navbar-collapse',
            'container_id' => 'bs-example-navbar-collapse-1',
            'menu_class' => 'nav navbar-nav',
            'items_wrap'      => '<ul id="%1$s" class="%2$s" itemprop="about" itemscope="" itemtype="http://schema.org/ItemList">%3$s</ul>',
            'fallback_cb' => 'WP_Bootstrap_Navwalker::fallback',
            'walker' => new WP_Bootstrap_Navwalker(),
          ));
          ?>
        </nav>
      </div>
    </div>
  </header>
<main class="h-entry" id="main-wrapper">
<?php if (!get_field('hide_header')): ?>
  <?php if (is_front_page()) { ?>
    <?php include get_template_directory() . '/inc/banner-home.php'; ?>
  <?php } elseif (is_home() or is_search()) { ?>
    <?php include get_template_directory() . '/inc/banner-blog-search.php'; ?>
  <?php } elseif (is_category()) { ?>
    <?php include get_template_directory() . '/inc/banner-category.php'; ?>
  <?php } elseif (is_post_type_archive('product')) { ?>
    <?php include get_template_directory() . '/inc/banner-archive-product.php'; ?>
  <?php } elseif (is_post_type_archive()) { ?>
    <?php include get_template_directory() . '/inc/banner-archive.php'; ?>
  <?php } elseif (is_tax()) { ?>
    <?php include get_template_directory() . '/inc/banner-taxonomy.php'; ?>
  <?php } elseif (get_post_type(get_the_ID()) == 'location') { ?>
    <?php include get_template_directory() . '/inc/banner-location.php'; ?>
  <?php } else { ?>
    <?php include get_template_directory() . '/inc/banner-default.php'; ?>
  <?php } ?>
<?php endif; ?>