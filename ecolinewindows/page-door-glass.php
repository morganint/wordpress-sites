<?php
/**
 * Template for displaying pages
 * Template Name: Door Glass Styles
 * @package ecolinewindows
 */
?> 
<?php get_header(); ?>

<section class="margin-top-4rem margin-bottom-4rem">
	<div class="container margin-bottom-4rem">
		<header class="row">
			<div class="col-sm-4"><h2><?php the_title(); ?></h2></div>
			<div class="col-sm-8"><?php the_content(); ?></div>
		</header>
	</div>
	<div class="container-fluid">
	<?php $glassstyles = new WP_Query( array( 'post_type' => 'glass-styles', 'orderby' => 'title', 'order' => 'ASC', 'posts_per_page' => 100 ) ) ;?>
	<?php if ( $glassstyles->have_posts() ) { ?>
		<?php while ( $glassstyles->have_posts() ) { ?>
			<?php $counter++; ?>
			<?php if ((($counter - 1) % 4 == 0) or ($counter == 1)) {echo '<div class="row justify-content-center">';} ?>
				<div class="col-sm-6 col-md-4 col-lg-3">
				<?php $glassstyles->the_post(); ?>
					<div class="card card-glass mb-5">
						<?php if ( has_post_thumbnail() ) { ?>
							<?php $featured_img_url = get_the_post_thumbnail_url(get_the_ID(),'full'); ?>
							<a class="main-image-link" href="<?php echo $featured_img_url; ?>" data-fancybox="glassgallery<?php echo get_the_ID(); ?>" title="<?php the_title(); ?> Decorative Door Glass"><?php the_post_thumbnail( 'medium', array( 'class'  => 'img-fluid main-image' ) ); ?></a>
						<?php }	?>
						<!-- hiding privacy level indicator
						<div class="privacylevel">
								<?php $privacy_level = get_post_meta($post->ID, 'privacy_level', true); ?>
								<?php $privacy_level_opacity = ($privacy_level-2)/10; ?>
								<div class="privacylevel-indicator" style="background: rgba(255,255,255,<?php echo $privacy_level_opacity; ?>)"><?php echo $privacy_level; ?>/10</div>
								<span>Privacy Level</span> 
						</div> -->
						<?php $camingfinishes = get_field('caming_finishes'); ?>
						<?php if( $camingfinishes ): ?>
						<div class="camingfinishes">
							<div class="d-flex flex-row justify-content-center">
								<?php foreach( $camingfinishes as $camingfinish ): ?>
									<div class="px-2 text-center">
										<a title="<?php the_title(); ?> door with <?php echo $camingfinish; ?> caming" data-fancybox="glassgallery<?php echo get_the_ID(); ?>" href="/wp-content/uploads/<?php echo $camingfinish; ?>.jpg"><img src="/wp-content/uploads/<?php echo $camingfinish; ?>.jpg" alt="<?php echo $camingfinish; ?>" /></a>
										<div class="clearfix"></div>
										<span><?php echo $camingfinish; ?></span>
									</div>
								<?php endforeach; ?>
							</div>
						</div>
						<?php endif; ?>
						
						<div class="d-flex justify-content-between flex-title">
							<div class="p-4"><h2><?php the_title(); ?></h2></div>
							<div class="p-4 d-none d-xs-inline-block">
								<?php $image = get_field('additional_preview_image_1'); ?>
								<?php if( !empty($image) ): ?>
									<?php $url = $image['url']; ?>
									<?php $title = $image['title']; ?>
									<?php $size = 'square-thumbnail'; ?>
									<?php $thumb = $image['sizes'][ $size ]; ?>
									<?php $width = $image['sizes'][ $size . '-width' ]; ?>
									<?php $height = $image['sizes'][ $size . '-height' ]; ?>
									<a class="link-preview" data-fancybox="glassgallery<?php echo get_the_ID(); ?>" href="<?php echo $url; ?>" title="<?php the_title(); ?> Decorative Door Glass">
										<img src="<?php echo $thumb; ?>" alt="<?php the_title(); ?> Decorative Door Glass" width="<?php echo $width; ?>" height="<?php echo $height; ?>" />
									</a>
								<?php endif; ?>
								<?php $image = get_field('additional_preview_image_2'); ?>
								<?php if( !empty($image) ): ?>
									<?php $url = $image['url']; ?>
									<?php $title = $image['title']; ?>
									<?php $size = 'square-thumbnail'; ?>
									<?php $thumb = $image['sizes'][ $size ]; ?>
									<?php $width = $image['sizes'][ $size . '-width' ]; ?>
									<?php $height = $image['sizes'][ $size . '-height' ]; ?>
									<a class="link-preview" data-fancybox="glassgallery<?php echo get_the_ID(); ?>" href="<?php echo $url; ?>" title="<?php the_title(); ?> Decorative Door Glass">
										<img src="<?php echo $thumb; ?>" alt="<?php the_title(); ?> Decorative Door Glass" width="<?php echo $width; ?>" height="<?php echo $height; ?>" />
									</a>
								<?php endif; ?>
								<?php $image = get_field('additional_preview_image_3'); ?>
								<?php if( !empty($image) ): ?>
									<?php $url = $image['url']; ?>
									<?php $title = $image['title']; ?>
									<?php $size = 'square-thumbnail'; ?>
									<?php $thumb = $image['sizes'][ $size ]; ?>
									<?php $width = $image['sizes'][ $size . '-width' ]; ?>
									<?php $height = $image['sizes'][ $size . '-height' ]; ?>
									<a class="link-preview" data-fancybox="glassgallery<?php echo get_the_ID(); ?>" href="<?php echo $url; ?>" title="<?php the_title(); ?> Decorative Door Glass">
										<img src="<?php echo $thumb; ?>" alt="<?php the_title(); ?> Decorative Door Glass" width="<?php echo $width; ?>" height="<?php echo $height; ?>" />
									</a>
								<?php endif; ?>
							</div>
						</div>
						<div class="card-body">							
							<?php $field = get_field_object('sizes'); ?>
							<?php $sizes = $field['value']; ?>
							<?php if( $sizes ): ?>
							<div class="d-flex flex-row justify-content-center flex-wrap">
								<?php foreach( $sizes as $size ): ?>
									<div class="p-0 mb-1 text-center">
										<a title="<?php echo $field['choices'][ $size ]; ?> glass size on <?php the_title(); ?> door" data-fancybox="glassgallery<?php echo get_the_ID(); ?>" href="/wp-content/uploads/<?php echo $size; ?>.png"><img src="/wp-content/uploads/<?php echo $size; ?>.png" style="height: 60px" alt="<?php echo $field['choices'][ $size ]; ?>" /></a>
										<div class="clearfix"></div>
										<span><?php echo $field['choices'][ $size ]; ?></span>
									</div>
								<?php endforeach; ?>
							</div>
							<?php endif; ?>	
						</div>						
					</div>						
				</div>
			<?php if (($counter % 4 == 0) or ($counter == $number)) {echo '</div>';} ?>
		<?php } ?>
	<?php wp_reset_postdata(); ?>
	<?php }  ?>
	</div>	
</section>

<?php $currentID = get_the_ID(); ?>
<?php $otherfeatures = new WP_Query( array( 'post_type' => 'doorfeatures', 'posts_per_page' => '6', 'post__not_in' => array($currentID) ) ) ;?>

<?php if ( $otherfeatures->have_posts() ) { ?>
<div class="sidebar sidebar-overlayed sidebar-overlayed-green d-none d-md-inline-block mb-5"  style="background-image: url(/wp-content/uploads/Decorative-Glass.jpg);"><div class="eco-overlay overlay-green"></div>
	<aside class="container-fluid margin-top-2rem margin-bottom-2rem">
		<header class="d-none"><h2 class="text-center text-white margin-bottom-30px">Other Door Features & Options</h2></header>
		<div class="row justify-content-center">
			<div class="col-10 justify-content-center">
				<div class="row">
					<?php while ( $otherfeatures->have_posts() ) { ?>
					<?php $otherfeatures->the_post(); ?>
					<div class="col-6 col-sm-4 col-lg text-center">				
						<a href="<?php echo get_permalink(); ?>" class="text-white margin-bottom-30px"><div style="background-image: url(<?php the_post_thumbnail_url(); ?>); background-size: cover; background-repeat: no-repeat; background-position: center center; width: 120px; height: 120px; display: inline-block;" class="rounded-circle"></div></a>
						<h5 class="text-secondary"><strong><a class="text-white" href="<?php echo get_permalink(); ?>"><?php the_title(); ?></a></strong></h5>				
					</div>
					<?php } ?>
				</div>
			</div>
		</div>
	</aside>
</div>
<?php wp_reset_postdata(); ?>
<?php }  ?>

<?php $related_query = new WP_Query( array( 'category_name' => $categories[0]->name . ',Customer Guide', 'posts_per_page' => '6' ) ) ;?>

<?php if ( $related_query->have_posts() ) { ?>
	<aside class="container">
		<h3 class="text-center margin-bottom-2rem">More recommendations and facts <span class="clearfix d-none d-lg-block"></span>about <?php the_title(); ?> from <a href="https://www.ecolinewindows.ca/blog/">our blog</h3>
		<div class="row margin-bottom-4rem">
			<?php while ( $related_query->have_posts() ) { ?>
			<?php $related_query->the_post(); ?>
			<div class="col-sm-4">
				<article class="row margin-bottom-2rem">
					<div class="col-5 col-sm-12 col-lg-4">
						<a href="<?php echo get_permalink(); ?>"><img class="img-fluid rounded" src="<?php the_post_thumbnail_url(); ?>" alt="<?php the_title(); ?>"></a>
					</div>
					<div class="col-7 col-sm-12 col-lg-8">
						<h5 class="mt-0"><a href="<?php echo get_permalink(); ?>" class="text-grey"><?php the_title(); ?></a></h5>
					</div>
				</article>
			</div>
			<?php } ?>
		</div>
	</aside>
	<?php wp_reset_postdata(); ?>
<?php }  ?>

<?php get_sidebar( 'contacts' ); ?>
<?php get_footer(); ?> 