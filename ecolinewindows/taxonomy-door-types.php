<?php 
/**
 * Displaying door types archive
 * 
 * @package ecolinewindows
 */

get_header(); 

$tax = get_queried_object();
$tax_background = get_field('background', 'door-types_' . $tax->term_id);
$tax_features = get_field('features', 'door-types_' . $tax->term_id); 


?> 

<section class="margin-top-3rem">

	<header class="page-header margin-bottom-1rem">
		<?php the_archive_title( '<h1 class="page-title text-center p-name">', '</h1>' ); ?>
		<p class="text-center abovelined"><span>&nbsp;</span></p>
	</header>
	<div class="category-wrapper entry-content" style="background-image: url(<?php echo $tax_background; ?>);">
		<section class="container-fluid">
			<div class="row">
				<div class="col-sm-2 col-md-4 col-lg-8" style="background-image: url(<?php echo $tax_background; ?>); background-size: cover; background-position: center center; background-repeat: no-repeat;"></div>
				<div class="col-sm-10 col-md-8 col-lg-4 bg-white-semi-transparent"> 
					<?php if($tax->description) { ?>
						<div class="category-description category-container"><p class="lead"><?php echo $tax->description; ?></p></div>
					<?php } else { ?>						
						<div class="category-features category-container"><p class="lead"><?php echo $tax_features; ?></p></div>
					<?php } ?>
														
				</div>
			</div>
		</section>
	</div>

	<div class="bg-grey-light padding-top-3rem padding-bottom-3rem">
		
		<?php global $post; ?>
		<?php $collection = get_posts(array('nopaging' => true,	'post_type' => 'doors',	'taxonomy' => $tax->taxonomy, 'term' => $tax->slug)); ?>
		<?php $collection_count = count($collection); ?>	
		<?php $show_glass = false; ?>	
			
		<?php foreach($collection as $post) : ?>				
			<?php setup_postdata($post); ?>
				<?php $featured_img_url = get_the_post_thumbnail_url($post->ID ,'full');  ?>
				<article class="container bg-white margin-bottom-3rem" id="<?php echo $post->post_name; ?>">
					<header class="row justify-content-center margin-bottom-3rem">
						<div class="col-sm-6">
							<div  class="margin-top-1rem margin-bottom-1rem">
								<a data-fancybox="glassgallery<?php echo $post->ID; ?>" href="<?php echo $featured_img_url; ?>"><?php the_post_thumbnail( 'medium', ['class' => 'img-fluid float-right'] ); ?></a>
								<?php $glass_styles = get_field('glass_styles_collection'); ?>
								<?php if( $glass_styles ): ?>
									<?php $show_glass = true; ?>	
									<div class="d-flex flex-row flex-wrap justify-content-center justify-content-lg-start category-glass-previews collection-glass-previews">
									<?php foreach( $glass_styles as $glass_style): ?>
										<?php setup_postdata($glass_style); ?>
										<?php $featured_img_url = get_the_post_thumbnail_url($glass_style->ID ,'full');  ?>
										<div class="pl-3 pb-1 text-center">
											<a data-fancybox="glassgallery<?php echo $post->ID; ?>" href="<?php echo $featured_img_url; ?>" title="<?php echo $glass_style->post_title; ?> <?php echo $post->name; ?>">
												<?php echo get_the_post_thumbnail( $glass_style->ID, 'medium', array( 'class' => '' ) ); ?>
											</a>
											<div class="clearfix category-glass-preview-label"><?php echo $glass_style->post_title; ?></div>
										</div>
									<?php endforeach; ?>
									</div>
								<?php endif; ?>	
							</div>
						</div>
						<div class="col-sm-6">
							<h2 class="category-title margin-top-2rem"><?php the_title(); ?></h2>
							<p class="margin-bottom-2rem"><?php echo $post->post_content; ?></p>
							<div class="d-flex flex-row flex-wrap justify-content-start margin-bottom-2rem">
							<?php for ($preview_count = 1; $preview_count <= 25; $preview_count++) { ?>
								<?php $preview = get_field('preview_'.$preview_count); ?>
								<?php if ($preview) { ?>
									<?php if ($preview['width'] > $preview['height']) { ?>
										<?php $height = '70' ?>
									<?php } else { ?>
										<?php $height = '140' ?>
									<?php } ?>
									<? /*<pre><?php print_r($preview); ?></pre>*/ ?>
									<div class="p-2 pb-1 text-center">
										<a data-fancybox="stylesgallery<?php echo $post->ID; ?>" href="<?php echo $preview['url']; ?>" title="<?php echo $preview['title']; ?>">
											<img src="<?php echo $preview['sizes']['medium']; ?>" alt="<?php echo $preview['title']; ?>" height="<?php echo $height; ?> <?php echo $preview['caption']; ?>" />
										</a>
										<div class="clearfix category-glass-preview-label">
											<?php echo $preview['title']; ?>
											<?php if ($preview['caption']) { ?>
												<br /><i class="fa fa-arrows-v" aria-hidden="true"></i> &nbsp;<?php echo $preview['caption']; ?>
											<?php } ?>
										</div>
									</div>
								<?php } ?>
							<?php } ?>
							</div>
						</div>
					</header>
				</article>
		<?php endforeach;?> 
		<?php wp_reset_postdata(); ?>
		
	</div>
	<?php echo $features; ?>
</section>

<?php $otherfeatures = new WP_Query( array( 'post_type' => 'doorfeatures', 'posts_per_page' => '6' ) ) ;?>
<?php if ( $otherfeatures->have_posts() && $show_glass == true ) { ?>
<div class="bg-grey-light padding-bottom-3rem d-none d-md-block">
	<aside class="container-fluid margin-bottom-4rem">
		<header class="d-none"><h2 class="text-center text-white margin-bottom-30px">Door Features & Options</h2></header>
		<div class="row justify-content-center">
			<div class="col-10">
				<div class="row justify-content-center">
					<?php while ( $otherfeatures->have_posts() ) { ?>
					<?php $otherfeatures->the_post(); ?>
					<div class="col-6 col-sm-4 col-lg text-center">				
						<div class="margin-bottom-1rem"><a href="<?php echo get_permalink(); ?>"><div style="background-image: url(<?php the_post_thumbnail_url(); ?>); background-size: cover; background-repeat: no-repeat; background-position: center center; width: 120px; height: 120px; display: inline-block;" class="rounded-circle"></div></a></div>
						<h4 class="text-1rem font-weight-bold text-secondary"><a href="<?php echo get_permalink(); ?>"><?php the_title(); ?></a></h4>				
					</div>
					<?php } ?>
				</div>
			</div>
		</div>
	</aside>
</div>
<?php wp_reset_postdata(); ?>
<?php }  ?>

<?php $glassoptions = new WP_Query( array( 'post_type' => 'glass-styles', 'posts_per_page' => '12' ) ) ;?>
<?php if ( $glassoptions->have_posts() && $show_glass == true ) { ?>
	<aside style="position: relative;">	
		<div class="padding-top-3rem" style="position: absolute; width: 100%; height: 100%; background-color: rgba(0,0,0,0.6); z-index: 2;">
			<header class="text-center margin-top-3rem">
				<h2 class="text-white text-3rem margin-bottom-1rem">Decorative Door Glass</h2>
				<p><a class="btn btn-white" href="/decorative-door-glass/"><i class="fa fa-th-list"></i> View all options</a></p>
			</header>
		</div>
		<div class="container-fluid">
			<div class="row justify-content-center">
				<?php while ( $glassoptions->have_posts() ) { ?>
					<?php $glassoptions->the_post(); ?>
					<div class="col-2" style="background-image: url(<?php the_post_thumbnail_url('thumbnail'); ?>); background-size: cover; background-repeat: no-repeat; background-position: center center; height: 160px;"></div>
				<?php } ?>
			</div>
		</div>
	</aside>
<?php wp_reset_postdata(); ?>
<?php }  ?>

<?php get_sidebar( 'contacts' ); ?>
<?php get_footer(); ?>  