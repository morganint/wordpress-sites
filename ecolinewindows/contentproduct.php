<?php if(is_single()){ ?>

	<article class="container content-overlapper"> 
		<div id="post-<?php the_ID(); ?>" class="row row-fixedbar">
			<div class="col-sm-8">
				<div class="entry-content margin-bottom-4rem" id="blog-body">
					<?php the_content(ecolinewindowsMoreLinkText()); ?> 
					<div class="clearfix"></div>
				</div>
			</div>
			<div class="col-sm-4">
				<aside id="sidebar-fixed" role="complementary">
					<?php dynamic_sidebar( 'blog' ); ?>
				</aside>
			</div>
		</div>
	</article>

<?php } else { ?>

	<article id="post-<?php the_ID(); ?>" class="col-md-6 margin-bottom-1rem h-entry">
		<header class="entry-header">
			<a href="<?php the_permalink(); ?>" rel="bookmark"><?php the_post_thumbnail('large', array( 'class' => 'img-fluid rounded' )); ?></a>
			<div class="row justify-content-center">
				<div class="col-10 bg-white rounded margin-top--3rem">
					<h3 class="entry-title text-center margin-top-1rem margin-bottom-2rem p-name"><a href="<?php the_permalink(); ?>" rel="bookmark"><strong><?php the_title(); ?></strong></a></h3>
					<div class="p-summary text-justify"><?php the_excerpt(); ?></div>
					<a href="<?php the_permalink(); ?>" class="btn btn-secondary btn-block" rel="bookmark">Continue reading ...</a>
					<?php if ('post' == get_post_type()) { ?> 
					<div class="entry-meta hidden">
						<?php ecolinewindowsPostOn(); ?> 
					</div><!-- .entry-meta -->
					<?php } //endif; ?> 					
				</div>
			</div>
		</header><!-- .entry-header -->
	</article><!-- #post-## -->

<?php } ?>