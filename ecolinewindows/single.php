<?php
/**
 * Template for displaying single post (read full post page).
 * 
 * @package ecolinewindows
 */

get_header(); ?> 


						<?php 
						while (have_posts()) {
							the_post();

							get_template_part('content', get_post_format());

							echo "\n\n";
							
							ecolinewindowsPagination();

						} //endwhile;
						?> 
	
<?php get_sidebar( 'contacts' ); ?>
<?php get_footer(); ?> 
