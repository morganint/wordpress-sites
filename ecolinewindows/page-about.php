<?php
/**
 * Template for displaying pages
 * Template Name: About (Corporate)
 * @package ecolinewindows
 */
?> 
<?php get_header(); ?>
<?php dynamic_sidebar( 'widget-banner-flip-home' ); ?>
<div class="container">

			<section>
				<?php while (have_posts()) { ?>
					<?php the_post(); ?>
					<?php the_content(); ?>
				<?php } ?> 
			</section>

</div>
<?php dynamic_sidebar( 'widget-product-line' ); ?>
<?php dynamic_sidebar( 'widget-shadowed' ); ?>
<?php get_sidebar( 'videos-featured' ); ?>
<?php dynamic_sidebar( 'widget-product-line-doors' ); ?>
<?php dynamic_sidebar( 'widget-green-parallax' ); ?>
<?php get_sidebar( 'facebook-comments' ); ?>
<?php get_sidebar( 'contacts' ); ?>
<?php get_footer(); ?> 