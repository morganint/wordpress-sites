<?php
/**
 * Template for displaying pages
 * Template Name: Home page
 * @package ecolinewindows
 */

get_header();

?>
<?php dynamic_sidebar( 'widget-banner-flip-home' ); ?>

<?php include(locate_template('block-text-image.php', false, false)); ?>

<?php dynamic_sidebar( 'widget-product-line' ); ?>
<?php dynamic_sidebar( 'widget-product-features' ); ?>
<?php dynamic_sidebar( 'widget-cta' ); ?>
<?php dynamic_sidebar( 'widget-product-appearance' ); ?>
<?php dynamic_sidebar( 'widget-product-options' ); ?>
<?php dynamic_sidebar( 'widget-cta' ); ?>
<?php dynamic_sidebar( 'widget-financing' ); ?>
<?php dynamic_sidebar( 'widget-product-warranty' ); ?>
<?php dynamic_sidebar( 'widget-product-line-doors' ); ?>
<?php dynamic_sidebar( 'widget-green-parallax' ); ?>
<?php // echo do_shortcode( '[sales_map province=all city=all]' ); ?>
<?php get_sidebar( 'facebook-comments' ); ?>
<?php get_sidebar( 'contacts' ); ?>

<?php get_footer(); ?>