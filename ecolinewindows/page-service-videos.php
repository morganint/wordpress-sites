<?php
/**
 * Template for displaying pages
 * Template Name: Service Videos
 * @package ecolinewindows
 */
?> 
<?php get_header(); ?>
<!-- Nav tabs -->

<div class="container">	
	<ul class="nav nav-tabs nav-justified" id="nav-gallery" role="tablist">
	  <li class="nav-item">
		<a class="nav-link active" data-toggle="tab" href="#view-styles" role="tab">Styles & Options</a>
	  </li>
	  <li class="nav-item">
		<a class="nav-link" data-toggle="tab" href="#view-guide" role="tab">Customer Guide</a>
	  </li>
	  <li class="nav-item">
		<a class="nav-link" data-toggle="tab" href="#view-kb" role="tab">Knowledge Center</a>
	  </li>
	  <li class="nav-item">
		<a class="nav-link" data-toggle="tab" href="#view-maintenance" role="tab">Maintenance</a>
	  </li>
	</ul>

	<div class="clearfix"></div>
	<div class="tab-content margin-top-3rem margin-bottom-4rem">
		
		<div class="tab-pane active" id="view-styles" role="tabpanel">
			<?php $args = array( 'post_type' => 'ecovideo', 'category_name' => 'Styles' ); ?>
			<?php $loop = new WP_Query( $args ); ?>
			<?php $number = $loop->post_count; ?>
			<?php $counter = 0; ?>
			<h2 class="text-center margin-bottom-2rem">Window Styles</h2>
				<div class="row">				
					<?php while ( $loop->have_posts() ) : $loop->the_post(); ?>		
						<?php $youtube_video_id = get_field('youtube_video_id'); ?>						
						<?php $video_duration = get_field('video_duration'); ?>						
						<?php $next_steps = get_field_object('next_steps'); ?>
						<?php $caption = ''; ?>
								
							<?php if($next_steps){ ?>							
								<?php $caption = '<div class=&quot;btn-group pull-right&quot;>' ?>
								<?php  foreach($next_steps['value'] as $next_step) { ?>
									<?php $caption .= '<a href=&quot;'.$next_step.'&quot; class=&quot;btn btn-secondary btn-sm&quot;>'.$next_steps['choices'][ $next_step ].'</a>'; ?> 
								<?php } ?>
								<?php $caption .= '</div>' ?>
							<?php } ?>
							<div class="col-xs-12 col-md-4">
								<div class="gallery">
									<div class="gallery-image">
										<img src="https://img.youtube.com/vi/<?php echo $youtube_video_id; ?>/maxresdefault.jpg" class="img-fluid img-thumbnail rounded" height="1280" width="720">						  
										<a data-fancybox data-caption="<?php the_title(); ?> <?php echo $caption; ?>" href="https://www.youtube.com/watch?v=<?php echo $youtube_video_id; ?>&amp;autoplay=1&amp;rel=0&amp;controls=0&amp;showinfo=0"><div class="gallery-text"><i class="fa fa-youtube-play" aria-hidden="true"></i></div></a>
										<span class="gallery-time"><?php echo $video_duration; ?></span>
									</div> 
									<h3><a data-fancybox data-caption="<?php the_title(); ?> <?php echo $caption; ?>" href="https://www.youtube.com/watch?v=<?php echo $youtube_video_id; ?>&amp;autoplay=1&amp;rel=0&amp;controls=0&amp;showinfo=0"><?php the_title(); ?></a></h3>
								</div>	
							</div>
					<?php endwhile; ?>
				</div>
			<?php wp_reset_postdata(); ?>
		</div>
		
		<div class="tab-pane" id="view-guide" role="tabpanel">
			<?php $args = array( 'post_type' => 'ecovideo', 'category_name' => 'Customer Guide' ); ?>
			<?php $loop = new WP_Query( $args ); ?>
			<?php $number = $loop->post_count; ?>
			<?php $counter = 0; ?>
			<h2 class="text-center margin-bottom-2rem">Customer Guide</h2>
				<div class="row">				
					<?php while ( $loop->have_posts() ) : $loop->the_post(); ?>		
						<?php $youtube_video_id = get_field('youtube_video_id'); ?>						
						<?php $video_duration = get_field('video_duration'); ?>						
						<?php $next_steps = get_field_object('next_steps'); ?>
						<?php $caption = ''; ?>
								
							<?php if($next_steps){ ?>							
								<?php $caption = '<div class=&quot;btn-group pull-right&quot;>' ?>
								<?php  foreach($next_steps['value'] as $next_step) { ?>
									<?php $caption .= '<a href=&quot;'.$next_step.'&quot; class=&quot;btn btn-secondary btn-sm&quot;>'.$next_steps['choices'][ $next_step ].'</a>'; ?> 
								<?php } ?>
								<?php $caption .= '</div>' ?>
							<?php } ?>
							<div class="col-xs-12 col-md-4">
								<div class="gallery">
									<div class="gallery-image">
										<img src="https://img.youtube.com/vi/<?php echo $youtube_video_id; ?>/maxresdefault.jpg" class="img-fluid img-thumbnail rounded" height="1280" width="720">						  
										<a data-fancybox data-caption="<?php the_title(); ?> <?php echo $caption; ?>" href="https://www.youtube.com/watch?v=<?php echo $youtube_video_id; ?>&amp;autoplay=1&amp;rel=0&amp;controls=0&amp;showinfo=0"><div class="gallery-text"><i class="fa fa-youtube-play" aria-hidden="true"></i></div></a>
										<span class="gallery-time"><?php echo $video_duration; ?></span>
									</div> 
									<h3><a data-fancybox data-caption="<?php the_title(); ?> <?php echo $caption; ?>" href="https://www.youtube.com/watch?v=<?php echo $youtube_video_id; ?>&amp;autoplay=1&amp;rel=0&amp;controls=0&amp;showinfo=0"><?php the_title(); ?></a></h3>
								</div>	
							</div>
					<?php endwhile; ?>
				</div>
			<?php wp_reset_postdata(); ?>
		</div>
		
		<div class="tab-pane" id="view-kb" role="tabpanel">
			<?php $args = array( 'post_type' => 'ecovideo', 'category_name' => 'Windows 101' ); ?>
			<?php $loop = new WP_Query( $args ); ?>
			<?php $number = $loop->post_count; ?>
			<?php $counter = 0; ?>
			<h2 class="text-center margin-bottom-2rem">Knowledge Center</h2>
				<div class="row">				
					<?php while ( $loop->have_posts() ) : $loop->the_post(); ?>		
						<?php $youtube_video_id = get_field('youtube_video_id'); ?>						
						<?php $video_duration = get_field('video_duration'); ?>						
						<?php $next_steps = get_field_object('next_steps'); ?>
						<?php $caption = ''; ?>
								
							<?php if($next_steps){ ?>							
								<?php $caption = '<div class=&quot;btn-group pull-right&quot;>' ?>
								<?php  foreach($next_steps['value'] as $next_step) { ?>
									<?php $caption .= '<a href=&quot;'.$next_step.'&quot; class=&quot;btn btn-secondary btn-sm&quot;>'.$next_steps['choices'][ $next_step ].'</a>'; ?> 
								<?php } ?>
								<?php $caption .= '</div>' ?>
							<?php } ?>
							<div class="col-xs-12 col-md-4">
								<div class="gallery">
									<div class="gallery-image">
										<img src="https://img.youtube.com/vi/<?php echo $youtube_video_id; ?>/maxresdefault.jpg" class="img-fluid img-thumbnail rounded" height="1280" width="720">						  
										<a data-fancybox data-caption="<?php the_title(); ?> <?php echo $caption; ?>" href="https://www.youtube.com/watch?v=<?php echo $youtube_video_id; ?>&amp;autoplay=1&amp;rel=0&amp;controls=0&amp;showinfo=0"><div class="gallery-text"><i class="fa fa-youtube-play" aria-hidden="true"></i></div></a>
										<span class="gallery-time"><?php echo $video_duration; ?></span>
									</div> 
									<h3><a data-fancybox data-caption="<?php the_title(); ?> <?php echo $caption; ?>" href="https://www.youtube.com/watch?v=<?php echo $youtube_video_id; ?>&amp;autoplay=1&amp;rel=0&amp;controls=0&amp;showinfo=0"><?php the_title(); ?></a></h3>
								</div>	
							</div>
					<?php endwhile; ?>
				</div>
			<?php wp_reset_postdata(); ?>
		</div>
		
		<div class="tab-pane" id="view-maintenance" role="tabpanel">
			<?php $args = array( 'post_type' => 'ecovideo', 'category_name' => 'maintenance' ); ?>
			<?php $loop = new WP_Query( $args ); ?>
			<?php $number = $loop->post_count; ?>
			<?php $counter = 0; ?>
			<h2 class="text-center margin-bottom-2rem">Window Maintenance</h2>
				<div class="row">				
					<?php while ( $loop->have_posts() ) : $loop->the_post(); ?>		
						<?php $youtube_video_id = get_field('youtube_video_id'); ?>						
						<?php $video_duration = get_field('video_duration'); ?>						
						<?php $next_steps = get_field_object('next_steps'); ?>
						<?php $caption = ''; ?>
								
							<?php if($next_steps){ ?>							
								<?php $caption = '<div class=&quot;btn-group pull-right&quot;>' ?>
								<?php  foreach($next_steps['value'] as $next_step) { ?>
									<?php $caption .= '<a href=&quot;'.$next_step.'&quot; class=&quot;btn btn-secondary btn-sm&quot;>'.$next_steps['choices'][ $next_step ].'</a>'; ?> 
								<?php } ?>
								<?php $caption .= '</div>' ?>
							<?php } ?>
							<div class="col-xs-12 col-md-4">
								<div class="gallery">
									<div class="gallery-image">
										<img src="https://img.youtube.com/vi/<?php echo $youtube_video_id; ?>/maxresdefault.jpg" class="img-fluid img-thumbnail rounded" height="1280" width="720">						  
										<a data-fancybox data-caption="<?php the_title(); ?> <?php echo $caption; ?>" href="https://www.youtube.com/watch?v=<?php echo $youtube_video_id; ?>&amp;autoplay=1&amp;rel=0&amp;controls=0&amp;showinfo=0"><div class="gallery-text"><i class="fa fa-youtube-play" aria-hidden="true"></i></div></a>
										<span class="gallery-time"><?php echo $video_duration; ?></span>
									</div> 
									<h3><a data-fancybox data-caption="<?php the_title(); ?> <?php echo $caption; ?>" href="https://www.youtube.com/watch?v=<?php echo $youtube_video_id; ?>&amp;autoplay=1&amp;rel=0&amp;controls=0&amp;showinfo=0"><?php the_title(); ?></a></h3>
								</div>	
							</div>
					<?php endwhile; ?>
				</div>
			<?php wp_reset_postdata(); ?>
		</div>
	</div>
</div>

<?php get_sidebar( 'contacts' ); ?>
<?php get_footer(); ?> 