<?php
/**
 * Template for displaying product (window styles) page.
 * 
 * @package ecolinewindows
 */
?>
<?php get_header(); ?> 
<?php $categories = get_the_category(); ?> 

<div class="container content-overlapper margin-bottom-4rem" style="z-index: 999">
	<section>
		<?php if (have_posts()) : while (have_posts()) : the_post(); ?>
			<?php the_content(); ?>
		<?php endwhile; endif; ?>
	</section>
</div>

	<aside class="container margin-bottom-4rem">
			<?php $args = array( 'post_type' => 'ecovideo', 'cat' => $categories[0]->cat_ID . ',179', 'posts_per_page' => '3', 'orderby' => 'ID', 'order'  => 'DESC' ); ?>
			<?php $loop = new WP_Query( $args ); ?>
			<?php $number = $loop->post_count; ?>
			<?php $counter = 0; ?>
			<h2 class="text-center margin-bottom-2rem">Buyers Guide</h2>
				<div class="row">				
					<?php while ( $loop->have_posts() ) : $loop->the_post(); ?>		
						<?php $youtube_video_id = get_field('youtube_video_id'); ?>						
						<?php $video_duration = get_field('video_duration'); ?>						
						<?php $next_steps = get_field_object('next_steps'); ?>
						<?php $caption = ''; ?>
								
							<?php if($next_steps){ ?>							
								<?php $caption = '<div class=&quot;btn-group pull-right&quot;>' ?>
								<?php  foreach($next_steps['value'] as $next_step) { ?>
									<?php $caption .= '<a href=&quot;'.$next_step.'&quot; class=&quot;btn btn-secondary btn-sm&quot;>'.$next_steps['choices'][ $next_step ].'</a>'; ?> 
								<?php } ?>
								<?php $caption .= '</div>' ?>
							<?php } ?>
							<div class="col-xs-12 col-md-4">
								<div class="gallery">
									<div class="gallery-image">
										<img src="https://img.youtube.com/vi/<?php echo $youtube_video_id; ?>/maxresdefault.jpg" class="img-fluid img-thumbnail rounded" height="1280" width="720">						  
										<a data-fancybox data-caption="<?php the_title(); ?> <?php echo $caption; ?>" href="https://www.youtube.com/watch?v=<?php echo $youtube_video_id; ?>&amp;autoplay=1&amp;rel=0&amp;controls=0&amp;showinfo=0"><div class="gallery-text"><i class="fa fa-youtube-play" aria-hidden="true"></i></div></a>
										<span class="gallery-time"><?php echo $video_duration; ?></span>
									</div> 
									<h3><a data-fancybox data-caption="<?php the_title(); ?> <?php echo $caption; ?>" href="https://www.youtube.com/watch?v=<?php echo $youtube_video_id; ?>&amp;autoplay=1&amp;rel=0&amp;controls=0&amp;showinfo=0"><?php the_title(); ?></a></h3>
								</div>	
							</div>
					<?php endwhile; ?>
				</div>
				<?php wp_reset_postdata(); ?>
			<p class="text-center margin-top-2rem">View more videos and photos in our <strong><a href="/gallery">gallery</a></strong></p>
	</aside>
	
	
<?php $currentID = get_the_ID(); ?>
<?php $otherfeatures = new WP_Query( array( 'post_type' => 'features', 'posts_per_page' => '6', 'post__not_in' => array($currentID) ) ) ;?>

<?php if ( $otherfeatures->have_posts() ) { ?>
<div class="sidebar sidebar-overlayed sidebar-overlayed-green margin-bottom-4rem padding-bottom-2rem d-none d-md-inline-block"  style="background-image: url(/wp-content/uploads/images/custom-replacement-window.jpg);">
	<aside class="container-fluid">
		<h3 class="text-center text-white margin-bottom-30px">Other Features & Options</h3>
		<div class="row justify-content-center">
			<div class="col-10">
				<div class="row justify-content-center">
					<?php while ( $otherfeatures->have_posts() ) { ?>
					<?php $otherfeatures->the_post(); ?>
					<div class="col-6 col-sm-4 col-lg-2 text-center">				
						<a href="<?php echo get_permalink(); ?>" class="text-white margin-bottom-30px"><span class="fa-stack text-3rem"><i class="fa fa-circle fa-stack-2x"></i><i class="fa fa-check fa-stack-1x text-green"></i></span></a>					
						<h5 class="text-secondary"><strong><a class="text-white" href="<?php echo get_permalink(); ?>"><?php the_title(); ?></a></strong></h5>				
					</div>
					<?php } ?>
				</div>
			</div>
		</div>
		<div class="row justify-content-center"><div class="col-2"><hr style="border-top-color: #fff;" /></div></div>
		<p class="text-center"><a href="/window-features-options/" class="btn btn-white">View all</a></p>
	</aside>
</div>
<?php wp_reset_postdata(); ?>
<?php }  ?>

<?php $related_query = new WP_Query( array( 'category_name' => $categories[0]->name . ',Customer Guide', 'posts_per_page' => '6' ) ) ;?>

<?php if ( $related_query->have_posts() ) { ?>
	<aside class="container">
		<h3 class="text-center margin-bottom-2rem">More recommendations and facts <span class="clearfix d-none d-lg-block"></span>about <?php the_title(); ?> from <a href="https://www.ecolinewindows.ca/blog/">our blog</h3>
		<div class="row margin-bottom-4rem">
			<?php while ( $related_query->have_posts() ) { ?>
			<?php $related_query->the_post(); ?>
			<div class="col-sm-4">
				<article class="row margin-bottom-2rem">
					<div class="col-5 col-sm-12 col-lg-4">
						<a href="<?php echo get_permalink(); ?>"><img class="img-fluid rounded" src="<?php the_post_thumbnail_url(); ?>" alt="<?php the_title(); ?>"></a>
					</div>
					<div class="col-7 col-sm-12 col-lg-8">
						<h5 class="mt-0"><a href="<?php echo get_permalink(); ?>" class="text-grey"><?php the_title(); ?></a></h5>
					</div>
				</article>
			</div>
			<?php } ?>
		</div>
	</aside>
	<?php wp_reset_postdata(); ?>
<?php }  ?>

<?php get_sidebar( 'contacts' ); ?>
<?php get_footer(); ?> 
