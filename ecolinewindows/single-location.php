<?php
/**
 * Template for displaying single post (read full post page).
 *
 * @package ecolinewindows
 */

get_header();

$city = get_field('city');
$province = get_field('provice');

$description_windows = get_field('description_windows');
$description_doors = get_field('description_doors');
$description_reviews = get_field('description_reviews');
$content_below_widgets = get_field('content_below_widgets');
$read_next = get_field('read_next');

$related_query = new WP_Query(array(
  'tag' => $city,
  'posts_per_page' => '5'
));
$locations = get_pages(array(
  'child_of' => $post->ID,
  'post_type' => 'location',
  'meta_key' => 'location_page_type',
  'meta_value' => 'location',
  'sort_column' => 'meta_value',
  'sort_order' => 'desc'
));
$locations_blogs = get_pages(array(
  'child_of' => $post->ID,
  'sort_column' => 'post_date',
  'post_type' => 'location',
  'sort_order' => 'desc',
  'meta_key' => 'location_page_type',
  'meta_value' => 'blog',
  'posts_per_page' => '5'
));


?>


<?php //dynamic_sidebar( 'widget-banner-flip-home' ); ?>

<?php echo do_shortcode('[show-top-rated]'); ?>

<?php include(locate_template('block-text-image.php', false, false)); ?>

<section style="border-bottom: #f4f4f4 1px solid;">
  <div class="container">
    <div class="row">
      <div class="d-none d-lg-block col-lg-4 col-xl-5 align-self-end text-center">
        <img src="/wp-content/uploads/window-installer.jpg" class="img-fluid" width="380" height="415"
             alt="Ecoline windows installer"/>
      </div>
      <div class="col-lg-8 col-xl-7 align-self-center text-center text-md-left">
        <div class="mx-5 mx-lg-auto">
          <?php if (have_posts()) : while (have_posts()) : the_post(); ?>
            <?php the_content(); ?>
          <?php endwhile; endif; ?>
        </div>
      </div>
    </div>
  </div>
</section>

<?php if ($locations) { ?>
  <aside class="bg-grey-light padding-top-4rem padding-bottom-4rem" role="complimentary">
    <div class="container">
      <h2 class="text-center text-1rem font-weight-normal">Working directly with homeowners in <?php echo $city; ?>
        and
        all surrounding areas, including:</h2>
      <div class="row justify-content-center">
        <?php foreach ($locations as $location) { ?>
          <div class="col-6 col-sm-4 col-lg-2 py-1 text-20px font-weight-bold text-center">
            <a class="font-secondary" href="<?php the_permalink() ?><?php echo $location->post_name; ?>/">
              <span class="fa-stack fa-lg"><i class="fa fa-circle fa-inverse fa-stack-2x"></i><i
                        class="fa fa-map-marker fa-stack-1x"></i></span>
              <span class="w-100 d-block"></span>
              <?php echo get_post_meta($location->ID, 'city', true); ?>
            </a>
          </div>
        <?php } ?>
        <?php $content_below_widgets = get_field('content_below_widgets'); ?>
      </div>
    </div>
  </aside>
  <?php echo do_shortcode("[sales_map province='$province' city='$city' only_city=1]"); ?>
<? } ?>

<?php if ($city) { ?>
  <aside class="container margin-top-4rem">
    <?php if ($description_reviews) { ?>
      <div class="container">
        <?php echo $description_reviews; ?>
      </div>
    <?php } ?>
    <?php if ($locations_blogs) { ?>
      <?php echo do_shortcode('[RICH_REVIEWS_SHOW category="' . $city . '" num="all"]'); ?>
      <header class="text-center text-20px font-weight-bold margin-top-2rem margin-bottom-1rem"><?php echo do_shortcode('[RICH_REVIEWS_SNIPPET category="' . $city . '"]'); ?></header>
    <?php } ?>
    <div class="d-flex justify-content-center flex-wrap">
      <?php if (get_field('facebook_url')) { ?>
        <div class="px-2 py-1"><a class="text-24px" href="<?php echo get_field('facebook_url'); ?>"><span
                    class="fa-stack"><i class="fa fa-square text-facebook fa-stack-2x"></i><i
                      class="fa fa-facebook fa-stack-1x fa-inverse"></i></span></a></div>
      <?php } ?>
      <?php if (get_field('bbb_url')) { ?>
        <div class="px-2 py-1"><a href="<?php echo get_field('bbb_url'); ?>"><img
                    src="/wp-content/uploads/images/bbb.png" alt="Ecoline Windows reviews on BBB" width="120"
                    height="60"/></a></div>
      <?php } ?>
      <?php if (get_field('homestars_url')) { ?>
        <div class="px-2 py-1"><a href="<?php echo get_field('homestars_url'); ?>"><img
                    src="/wp-content/uploads/homestars.png" alt="Ecoline Windows reviews on Homestars" width="103"
                    height="60"/></a></div>
      <?php } ?>
    </div>
  </aside>
<?php } ?>

<?php if ($description_windows) { ?>
  <div class="container padding-top-4rem margin-bottom--30px">
    <?php echo $description_windows; ?>
  </div>
<?php } ?>

<?php dynamic_sidebar('widget-product-line'); ?>

<?php if (is_single(5337)) {
  /**************** CALGARY PAGE **************/ ?>
  <section class="container">
    <div class="row justify-content-center mt-2 mb-5 py-lg-3">
      <div class="col-sm-10 text-center">
        <h2><strong>Only Top-Performing Window Covering Products</strong></h2>
        <div class="row justify-content-center mt-4">
          <div class="col-4 border-underline">&nbsp;</div>
        </div>
        <p>While the installation is one of the key element to a happy home, we top the ultimate experience with great
          quality products. We proudly offer a variety of shutters, shades and blinds to please each homeowner's
          unique
          style, needs and preferences.</p>
      </div>
    </div>
  </section>
  <section class="container-fluid">
    <div class="row align-items-stretch">
      <div class="col-lg-6 p-5 bg-cover bg-cover-position-x-right bg-cover-position-y-center"
           style="background-image: url(https://www.ecolinewindows.ca/wp-content/uploads/shutters-overlapped-green.jpg)">
        <div class="row justify-content-end align-items-center m-lg-5">
          <div class="col-lg-12 col-xl-8 pt-lg-5 pr-lg-5 align-self-center text-white">
            <header><h2 class="text-3rem"><strong><a href="https://www.ecolinewindows.ca/blinds-shades-shutters/"
                                                     class="text-white">Shutters</a></strong></h2></header>
            <p>Finely crafted wood shutters are made with premium hardwood to provide a rich finish while
              state-of-the-art technology builds in superior structural integrity and durability. Maxxmar shutters are
              also available in leather and wood-look finishes to complement the room's decor. Exclusive specialty
              shapes and arches include operable louvers and operable panels and are available in both vinyl and wood
              profiles. Ideal for living rooms, bedrooms and entryways.</p>
            <p><a href="https://www.ecolinewindows.ca/blinds-shades-shutters/" class="btn btn-light">Learn more</a>
            </p>
          </div>
        </div>
      </div>
      <div class="col-lg-6 p-lg-5 text-center text-lg-left">
        <div class="my-5 p-lg-5 overlapped-left bg-white">
          <div class="row justify-content-center justify-content-lg-start">
            <div class="col-sm-4 col-lg-6 col-xl-4">
              <a href="https://www.ecolinewindows.ca/blinds-shades-shutters/"><img
                        src="https://www.ecolinewindows.ca/wp-content/uploads/Blinds-Horizontal-1-480x270.jpg" alt=""
                        width="480" height="270" class="img-fluid mb-3"/></a>
              <h3><strong><a href="https://www.ecolinewindows.ca/blinds-shades-shutters/">Blinds</a></strong></h3>
              <p>Available in 2" and 3.5" textured PVC wood and faux wood, textured or print finishes. </p>
              <p><a href="https://www.ecolinewindows.ca/blinds-shades-shutters/" class="btn btn-primary">Learn
                  more</a>
              </p>
            </div>
            <div class="col-sm-4 col-lg-6 col-xl-4">
              <a href="https://www.ecolinewindows.ca/blinds-shades-shutters/"><img
                        src="https://www.ecolinewindows.ca/wp-content/uploads/Cellular-shade-1-480x270.jpg" alt=""
                        width="480" height="270" class="img-fluid mb-3"/></a>
              <h3><strong><a href="https://www.ecolinewindows.ca/blinds-shades-shutters/">Shades</a></strong></h3>
              <p>Four unique shade styles are available: cellular, pleated, roller, roman. </p>
              <p><a href="https://www.ecolinewindows.ca/blinds-shades-shutters/" class="btn btn-primary">Learn
                  more</a>
              </p>
            </div>
          </div>
        </div>
      </div>
    </div>
  </section>
  <section class="container my-5 py-lg-5">
    <div class="row align-items-center  justify-content-end">
      <div class="col-sm-4 col-lg-2">
        <img src="https://www.ecolinewindows.ca/wp-content/uploads/DecoraSafe.jpg" alt="DecoraSafe" width="360"
             height="360" class="img-fluid rounded-lg wp-image-12655"/>
      </div>
      <div class="col-sm-8">
        <h3>Your Child Safety With DecoraSafe</h3>
        <p>Maxxmar takes child safety seriously. Fitted with the DecoraSafe design, all blinds are made with no
          accessible cords or chains. A free-hanging wand is easily operated with a crank or retractable chain pusher
          tab. Once released, the chain automatically retracts out of the reach of children.</p>
      </div>
    </div>
  </section>
<?php } ?>

<?php if ($description_doors) { ?>
  <div class="container padding-top-2rem mb-5">
    <?php echo $description_doors; ?>
  </div>
<?php } ?>

<?php dynamic_sidebar('widget-product-line-doors'); ?>

<?php dynamic_sidebar('widget-green-parallax'); ?>

<?php if ($content_below_widgets) { ?>
  <div class="container pt-5 mt-5">
    <?php echo $content_below_widgets; ?>
  </div>
<?php } ?>

<div class="container margin-top-4rem">
  <div class="row justify-content-center padding-top-2rem">

    <?php if ($read_next) { ?>
      <aside class="col-lg-8 padding-bottom-3rem">
        <?php echo $read_next; ?>
      </aside>
    <? } else { ?>
      <?php if ($locations_blogs) { ?>
        <aside class="col-sm-6 col-lg-4 padding-bottom-3rem">
          <h2 class="text-1rem font-weight-bold margin-bottom-2rem">Latest updates from this location:</h2>
          <?php foreach ($locations_blogs as $blog) { ?>
            <article class="row margin-bottom-2rem text-sm-left">
              <div class="col-5 col-sm-4">
                <a href="<?php the_permalink() ?><?php echo $blog->post_name; ?>/"
                   class="margin-bottom-1rem"><?php echo get_the_post_thumbnail($blog->ID, 'medium', array('class' => 'img-fluid rounded')); ?></a>
              </div>
              <div class="col-7 col-sm-8">
                <h3 class="mt-0  text-1rem font-weight-normal"><a
                          href="<?php the_permalink() ?><?php echo $blog->post_name; ?>/"
                          class="text-grey"><?php echo $blog->post_title; ?></a></h3>
              </div>
            </article>
          <?php } ?>
        </aside>
      <?php } ?>
      <?php if ($related_query->have_posts()) { ?>
        <aside class="col-sm-6 col-lg-4 padding-bottom-3rem">
          <h2 class="text-1rem font-weight-bold margin-bottom-2rem">More recommendations from our <a
                    href="https://www.ecolinewindows.ca/blog/">blog</a>:</h2>
          <?php while ($related_query->have_posts()) { ?>
            <?php $related_query->the_post(); ?>
            <article class="row margin-bottom-2rem">
              <div class="col-5 col-sm-4">
                <a href="<?php echo get_permalink(); ?>"><img class="img-fluid rounded"
                                                              src="<?php the_post_thumbnail_url(); ?>"
                                                              alt="<?php the_title(); ?>"></a>
              </div>
              <div class="col-7 col-sm-8">
                <h3 class="mt-0 text-1rem font-weight-normal"><a href="<?php echo get_permalink(); ?>"
                                                                 class="text-grey"><?php the_title(); ?></a></h3>
              </div>
            </article>
          <?php } ?>
        </aside>
        <?php wp_reset_postdata(); ?>
      <?php } ?>
    <?php } ?>
    <aside class="col-lg-4 align-self-end" role="complimentary">
      <div class="card bg-green rounded-top" id="location-form-container">
        <div class="card-body rounded-top">
          <h3 class="card-title text-white text-center text-bold margin-bottom-2rem"><strong>Get a Free Online
              Quote</strong></h3>
          <?php echo do_shortcode('[contact-form-7 id="2007"]'); ?>
          <?php if (get_field('phone')) { ?>
            <div class="text-italics text-center text-white text-muted">- or -</div>
            <h3 class="card-title text-white text-center margin-no"><strong>Contact Us</strong></h3>
            <p class="text-center text-white text-32px call-to-us">
              <strong>
                <a href="tel:<?php the_field('phone'); ?>" class="text-warning contact-phone-link">
                  <i class="fa fa-mobile" aria-hidden="true"></i>
                </a>&nbsp;
                <a href="tel:<?php the_field('phone', $post->ID); ?>"
                   class="text-warning contact-phone-link"><?php the_field('phone', $post->ID); ?></a>
              </strong>
            </p>
          <?php } ?>
          <?php if (get_field('display_local_address')) { ?>
            <p class="text-center text-white h-adr"><span
                      class="p-street-address"><?php the_field('street_address'); ?></span><br/> <span
                      class="p-locality"><?php the_field('city'); ?></span>, <?php the_field('province'); ?> <span
                      class="p-country-name">Canada</span> <span
                      class="p-postal-code"><?php the_field('postal'); ?></span></p>
          <?php } ?>
        </div>
      </div>
    </aside>
  </div>
</div>

<div class="d-none">
  <?php get_sidebar('contacts'); ?>
</div>


<?php

$short_description = get_field('short_description');
$place_id = get_field('place_id'); //ChIJdY-pEfxvcVMRh_M2bSPSxXM
$api = 'AIzaSyDRzb2H9H-p8_LGkZ0w5PfzCLt33Vq8AWM';

if (!empty($place_id)) {
  $url = 'https://maps.googleapis.com/maps/api/place/details/json?place_id=' . $place_id . '&key=' . $api;

  $ch = curl_init();
  $timeout = 0;

  curl_setopt($ch, CURLOPT_URL, $url);
  curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, 0);
  curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
  curl_setopt($ch, CURLOPT_HEADER, 0);
  curl_setopt($ch, CURLOPT_CONNECTTIMEOUT, $timeout);

  $data = curl_exec($ch);

  $response = json_decode($data, true);

  ?>

  <script type="application/ld+json">
    {
      "@context": "https://schema.org/",
      "@type": "Product",
      "name": "<?php the_title(); ?>",
      "description": "<?php echo $short_description ?>",
      "brand": {
        "name": "Ecoline Windows"
      },
      "aggregateRating": {
        "@type": "AggregateRating",
        "ratingValue" : "<?php echo $response['result']['rating'] ?>",
        "reviewCount": "<?php echo $response['result']['user_ratings_total'] ?>"
      },
      "review": [
      <?php
    $id = 1;
    $count = count($response['result']['reviews']);
    foreach ($response['result']['reviews'] as $review) { ?>
        {
          "@type": "Review",
          "author": "<?php echo $review['author_name'] ?>",
          "datePublished": "<?php echo $review['relative_time_description'] ?>",
          "reviewBody": "<?php echo $review['text'] ?>",
          "reviewRating": {
            "@type": "Rating",
            "bestRating": "5",
            "ratingValue": "<?php echo $review['rating'] ?>",
            "worstRating": "1"
          }
        }
        <?php
      if($id < $count) echo ',';
      $id++;
    } ?>
      ]
    }

  </script>

<?php } ?>

<?php get_footer(); ?>