<?php 
/**
 * Displaying archive page (category, tag, archives post, author's post)
 * 
 * @package ecolinewindows
 */

get_header(); ?> 

<section class="container margin-top-4rem">
	<?php if (have_posts()) { ?> 
	<?php $number = $loop->post_count; ?>
		<?php while (have_posts()) { ?>
			<?php $counter++; ?>
			<?php if ((($counter - 1) % 2 == 0) or ($counter == 1)) {echo '<div class="row">';} ?>
				<?php the_post(); ?> 
				<?php get_template_part('content', get_post_format()); ?>
			<?php if (($counter % 2 == 0) or ($counter == $number)) {echo '</div>';} ?>
		<?php }	?> 
		<div class="clearfix margin-top-3rem"></div>
		<?php ecolinewindowsPagination(); ?> 
	<?php } else { ?> 
		<?php get_template_part('no-results', 'archive'); ?> 
	<?php } ?> 
</section>

<?php get_sidebar( 'contacts' ); ?>
<?php get_footer(); ?>  