<?php
/**
 * Template for displaying full screen pages
 * Template Name: Full Screen
 * @package ecolinewindows
 */
?> 
<?php get_header(); ?>
<?php dynamic_sidebar( 'widget-banner-flip-home' ); ?>
	<section class="margin-top-4rem">
		<?php while (have_posts()) { ?>
			<?php the_post(); ?>
			<?php the_content(); ?>
		<?php } ?> 
	</section>
	
<?php dynamic_sidebar( 'widget-shadowed' ); ?>
<?php get_sidebar( 'contacts' ); ?>
<?php get_footer(); ?> 