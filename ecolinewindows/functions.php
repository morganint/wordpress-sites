<?php
/**
 * EcolineWindows theme
 *
 * @package ecolinewindows
 */

if ( ! defined( 'ABSPATH' ) ) {
	exit; // Exit if accessed directly.
}


function ecolineBootstrapEditorCSS() {
    add_editor_style( get_template_directory_uri() . '/js/bootstrap/css/bootstrap.min.css' );
	 add_editor_style(get_template_directory_uri() . '/fonts/font-awesome/css/font-awesome.min.css');
    add_editor_style( get_template_directory_uri() . '/style.css' );
}
add_action( 'admin_init', 'ecolineBootstrapEditorCSS' );

if (!function_exists('ecolinewindowsSetup')) {
    /**
     * Setup theme and register support wp features.
     */
    function ecolinewindowsSetup() {

        // add theme support title-tag
        add_theme_support('title-tag');

        // enable support for post thumbnail or feature image on posts and pages
        add_theme_support('post-thumbnails');

        // allow the use of html5 markup
        // @link https://codex.wordpress.org/Theme_Markup
        add_theme_support('html5', array('caption', 'comment-form', 'comment-list', 'gallery', 'search-form'));

        // add support menu
        register_nav_menus(array(
            'primary' => __('Primary Menu', 'ecolinewindows'),
            'corporate' => __('Corporate Menu', 'ecolinewindows'),
            'action' => __('Action Menu', 'ecolinewindows'),
            'about' => __('About Menu', 'ecolinewindows'),
            'windows' => __('Windows Menu', 'ecolinewindows'),
            'doors' => __('Doors Menu', 'ecolinewindows'),
            'pricing' => __('Pricing Menu', 'ecolinewindows'),
            'faq' => __('Windows 101 Menu', 'ecolinewindows'),
            'services' => __('Services Menu', 'ecolinewindows'),
            'legal' => __('Legal Menu', 'ecolinewindows'),
        ));

    }// ecolinewindowsSetup
}
add_action('after_setup_theme', 'ecolinewindowsSetup');

function eco_archive_title( $title ) {
    if ( is_category() ) {
        $title = single_cat_title( '', false );
    } elseif ( is_tag() ) {
        $title = single_tag_title( '', false );
    } elseif ( is_author() ) {
        $title = '<span class="vcard">' . get_the_author() . '</span>';
    } elseif ( is_post_type_archive() ) {
        $title = post_type_archive_title( '', false );
    } elseif ( is_tax() ) {
        $title = single_term_title( '', false );
    }

    return $title;
}

add_filter( 'get_the_archive_title', 'eco_archive_title' );

function products_post_type() {
	$labels = array(
		'name'                => _x( 'Window Types', 'Post Type General Name', 'text_domain' ),
		'singular_name'       => _x( 'Window Type', 'Post Type Singular Name', 'text_domain' ),
		'menu_name'           => __( 'Window Types', 'text_domain' ),
		'parent_item_colon'   => __( 'Parent Window Type:', 'text_domain' ),
		'all_items'           => __( 'All Window Types', 'text_domain' ),
		'view_item'           => __( 'View Window Types', 'text_domain' ),
		'add_new_item'        => __( 'Add New Window Type', 'text_domain' ),
		'add_new'             => __( 'New Window Type', 'text_domain' ),
		'edit_item'           => __( 'Edit Window Type', 'text_domain' ),
		'update_item'         => __( 'Update Window Type', 'text_domain' ),
		'search_items'        => __( 'Search Window Types', 'text_domain' ),
		'not_found'           => __( 'No window types found', 'text_domain' ),
		'not_found_in_trash'  => __( 'No window Types found in Trash', 'text_domain' ),
	);
	$args = array(
		'label'               => __( 'Window Type', 'text_domain' ),
		'description'         => __( 'Window type information pages', 'text_domain' ),
		'labels'              => $labels,
		'supports'            => array( 'title', 'editor', 'excerpt', 'author', 'thumbnail', 'comments', 'custom-fields', 'page-attributes' ),
		'taxonomies'          => array( 'category', 'post_tag' ),
		'hierarchical'        => true,
		'public'              => true,
		'show_ui'             => true,
		'show_in_menu'        => true,
		'show_in_nav_menus'   => true,
		'show_in_admin_bar'   => true,
		'menu_position'       => 20,
		'can_export'          => true,
		'has_archive'         => true,
		'exclude_from_search' => false,
		'publicly_queryable'  => true,
		'rewrite'   		  => array( 'slug' => 'window-styles'),
		'menu_icon' 		  => 'dashicons-tablet',
		'capability_type'     => 'page',
	);
	register_post_type( 'product', $args );
}
add_action( 'init', 'products_post_type');

function features_post_type() {
	$labels = array(
		'name'                => _x( 'Window Features', 'Post Feature General Name', 'text_domain' ),
		'singular_name'       => _x( 'Window Feature', 'Post Feature Singular Name', 'text_domain' ),
		'menu_name'           => __( 'Window Features', 'text_domain' ),
		'parent_item_colon'   => __( 'Parent Window Feature:', 'text_domain' ),
		'all_items'           => __( 'All Window Features', 'text_domain' ),
		'view_item'           => __( 'View Window Features', 'text_domain' ),
		'add_new_item'        => __( 'Add New Window Feature', 'text_domain' ),
		'add_new'             => __( 'New Window Feature', 'text_domain' ),
		'edit_item'           => __( 'Edit Window Feature', 'text_domain' ),
		'update_item'         => __( 'Update Window Feature', 'text_domain' ),
		'search_items'        => __( 'Search Window Features', 'text_domain' ),
		'not_found'           => __( 'No window Features found', 'text_domain' ),
		'not_found_in_trash'  => __( 'No window Features found in Trash', 'text_domain' ),
	);
	$args = array(
		'label'               => __( 'Window Feature', 'text_domain' ),
		'description'         => __( 'Window Feature information pages', 'text_domain' ),
		'labels'              => $labels,
		'supports'            => array( 'title', 'editor', 'excerpt', 'author', 'thumbnail', 'comments', 'custom-fields', 'post-formats', 'page-attributes' ),
		'taxonomies'          => array( 'category', 'post_tag' ),
		'hierarchical'        => true,
		'public'              => true,
		'show_ui'             => true,
		'show_in_menu'        => true,
		'show_in_nav_menus'   => true,
		'show_in_admin_bar'   => true,
		'menu_position'       => 20,
		'can_export'          => true,
		'has_archive'         => true,
		'exclude_from_search' => false,
		'publicly_queryable'  => true,
		'rewrite'   		  => array( 'slug' => 'features-and-options'),
		'menu_icon' 		  => 'dashicons-tablet',
		'capability_type'     => 'page',
	);
	register_post_type( 'features', $args );
}
add_action( 'init', 'features_post_type');

function doors_post_type() {
	$labels = array(
		'name'                => _x( 'Doors Collection', 'Post Type General Name', 'text_domain' ),
		'singular_name'       => _x( 'Door', 'Post Type Singular Name', 'text_domain' ),
		'menu_name'           => __( 'Doors Collection', 'text_domain' ),
		'parent_item_colon'   => __( 'Parent Door:', 'text_domain' ),
		'all_items'           => __( 'All Doors', 'text_domain' ),
		'view_item'           => __( 'View Door', 'text_domain' ),
		'add_new_item'        => __( 'Add New Door', 'text_domain' ),
		'add_new'             => __( 'New Door', 'text_domain' ),
		'edit_item'           => __( 'Edit Door', 'text_domain' ),
		'update_item'         => __( 'Update Door', 'text_domain' ),
		'search_items'        => __( 'Search Doors', 'text_domain' ),
		'not_found'           => __( 'No doors found', 'text_domain' ),
		'not_found_in_trash'  => __( 'No doors found in trash', 'text_domain' ),
	);
	$args = array(
		'label'               => __( 'Door Type', 'text_domain' ),
		'description'         => __( 'Door type information pages', 'text_domain' ),
		'labels'              => $labels,
		'supports'            => array( 'title', 'editor', 'excerpt', 'author', 'thumbnail', 'comments', 'custom-fields', 'post-formats', 'page-attributes' ),
		'taxonomies'          => array( 'post_tag', 'page-attributes' ),
		'hierarchical'        => true,
		'public'              => true,
		'show_ui'             => true,
		'show_in_menu'        => true,
		'show_in_nav_menus'   => true,
		'show_in_admin_bar'   => true,
		'menu_position'       => 20,
		'can_export'          => true,
		'has_archive'         => true,
		'exclude_from_search' => false,
		'publicly_queryable'  => true,
		'rewrite'   		  => array( 'slug' => 'doors'),
		'menu_icon' 		  => 'dashicons-tablet',
		'capability_type'     => 'page',
	);
	register_post_type( 'doors', $args );
}
add_action( 'init', 'doors_post_type');


function create_door_type_taxonomies() {

	$labels = array(
		'name'              => _x( 'Door Types', 'taxonomy general name', 'textdomain' ),
		'singular_name'     => _x( 'Door Type', 'taxonomy singular name', 'textdomain' ),
		'search_items'      => __( 'Search Door Types', 'textdomain' ),
		'all_items'         => __( 'All Door Types', 'textdomain' ),
		'edit_item'         => __( 'Edit Door Type', 'textdomain' ),
		'update_item'       => __( 'Update Door Type', 'textdomain' ),
		'add_new_item'      => __( 'Add New Door Type', 'textdomain' ),
		'new_item_name'     => __( 'New Door Type Name', 'textdomain' ),
		'menu_name'         => __( 'Types', 'textdomain' ),
	);

	$args = array(
		'hierarchical'      => true,
		'labels'            => $labels,
		'show_ui'           => true,
		'show_admin_column' => true,
		'query_var'         => true,
		'rewrite'           => array( 'slug' => 'door-types', 'with_front' => false ),
	);

	register_taxonomy( 'door-types', array( 'doors' ), $args );
	flush_rewrite_rules();

}
add_action( 'init', 'create_door_type_taxonomies' );


function doorfeatures_post_type() {
	$labels = array(
		'name'                => _x( 'Door Features', 'Post Feature General Name', 'text_domain' ),
		'singular_name'       => _x( 'Door Feature', 'Post Feature Singular Name', 'text_domain' ),
		'menu_name'           => __( 'Door Features', 'text_domain' ),
		'parent_item_colon'   => __( 'Parent Door Feature:', 'text_domain' ),
		'all_items'           => __( 'All Door Features', 'text_domain' ),
		'view_item'           => __( 'View Door Features', 'text_domain' ),
		'add_new_item'        => __( 'Add New Door Feature', 'text_domain' ),
		'add_new'             => __( 'New Door Feature', 'text_domain' ),
		'edit_item'           => __( 'Edit Door Feature', 'text_domain' ),
		'update_item'         => __( 'Update Door Feature', 'text_domain' ),
		'search_items'        => __( 'Search Door Features', 'text_domain' ),
		'not_found'           => __( 'No door Features found', 'text_domain' ),
		'not_found_in_trash'  => __( 'No door Features found in Trash', 'text_domain' ),
	);
	$args = array(
		'label'               => __( 'Door Feature', 'text_domain' ),
		'description'         => __( 'Door feature information pages', 'text_domain' ),
		'labels'              => $labels,
		'supports'            => array( 'title', 'editor', 'excerpt', 'author', 'thumbnail', 'comments', 'custom-fields', 'page-attributes' ),
		'taxonomies'          => array( 'door-types' ),
		'hierarchical'        => true,
		'public'              => true,
		'show_ui'             => true,
		'show_in_menu'        => true,
		'show_in_nav_menus'   => true,
		'show_in_admin_bar'   => true,
		'menu_position'       => 20,
		'can_export'          => true,
		'has_archive'         => true,
		'exclude_from_search' => false,
		'publicly_queryable'  => true,
		'rewrite'   		  => array( 'slug' => 'door-features'),
		'menu_icon' 		  => 'dashicons-tablet',
		'capability_type'     => 'page',
	);
	register_post_type( 'doorfeatures', $args );
}
add_action( 'init', 'doorfeatures_post_type');

function glassstyles_post_type() {
	$labels = array(
		'name'                => _x( 'Glass Styles', 'Post Feature General Name', 'text_domain' ),
		'singular_name'       => _x( 'Glass Style', 'Post Feature Singular Name', 'text_domain' ),
		'menu_name'           => __( 'Glass Styles', 'text_domain' ),
		'parent_item_colon'   => __( 'Parent Glass Style:', 'text_domain' ),
		'all_items'           => __( 'All Glass Styles', 'text_domain' ),
		'view_item'           => __( 'View Glass Styles', 'text_domain' ),
		'add_new_item'        => __( 'Add New Glass Style', 'text_domain' ),
		'add_new'             => __( 'New Glass Style', 'text_domain' ),
		'edit_item'           => __( 'Edit Glass Style', 'text_domain' ),
		'update_item'         => __( 'Update Glass Style', 'text_domain' ),
		'search_items'        => __( 'Search Glass Styles', 'text_domain' ),
		'not_found'           => __( 'No door Features found', 'text_domain' ),
		'not_found_in_trash'  => __( 'No door Features found in Trash', 'text_domain' ),
	);
	$args = array(
		'label'               => __( 'Glass Style', 'text_domain' ),
		'description'         => __( 'Door feature information pages', 'text_domain' ),
		'labels'              => $labels,
		'supports'            => array( 'title', 'editor', 'excerpt', 'thumbnail' ),
		'taxonomies'          => array( 'category', 'post_tag' ),
		'hierarchical'        => true,
		'public'              => true,
		'show_ui'             => true,
		'show_in_menu'        => true,
		'show_in_nav_menus'   => true,
		'show_in_admin_bar'   => true,
		'menu_position'       => 20,
		'can_export'          => true,
		'has_archive'         => true,
		'exclude_from_search' => false,
		'publicly_queryable'  => true,
		'menu_icon' 		  => 'dashicons-tablet',
		'capability_type'     => 'page',
	);
	register_post_type( 'glass-styles', $args );
}
add_action( 'init', 'glassstyles_post_type');


function location_post_type() {
	$labels = array(
		'name'                => _x( 'Office Locations', 'Post Type General Name', 'text_domain' ),
		'singular_name'       => _x( 'Location', 'Post Type Singular Name', 'text_domain' ),
		'menu_name'           => __( 'Locations', 'text_domain' ),
		'parent_item_colon'   => __( 'Parent Location:', 'text_domain' ),
		'all_items'           => __( 'All Locations', 'text_domain' ),
		'view_item'           => __( 'View Locations', 'text_domain' ),
		'add_new_item'        => __( 'Add Location', 'text_domain' ),
		'add_new'             => __( 'New Location', 'text_domain' ),
		'edit_item'           => __( 'Edit Locations', 'text_domain' ),
		'update_item'         => __( 'Update Locations', 'text_domain' ),
		'search_items'        => __( 'Search Locations', 'text_domain' ),
		'not_found'           => __( 'No locations found', 'text_domain' ),
		'not_found_in_trash'  => __( 'No locations found in Trash', 'text_domain' ),
	);
	$args = array(
		'label'               => __( 'Location', 'text_domain' ),
		'description'         => __( 'Location information pages', 'text_domain' ),
		'labels'              => $labels,
		'supports'            => array( 'title', 'editor', 'excerpt', 'thumbnail', 'custom-fields', 'page-attributes' ),
		'hierarchical'        => true,
		'public'              => true,
		'show_ui'             => true,
		'show_in_menu'        => true,
		'show_in_nav_menus'   => true,
		'show_in_admin_bar'   => true,
		'menu_position'       => 20,
		'can_export'          => true,
		'has_archive'         => true,
		'exclude_from_search' => false,
		'publicly_queryable'  => true,
		'rewrite'   		  => array( 'slug' => 'location'),
		'menu_icon' 		  => 'dashicons-location',
		'capability_type'     => 'page',
	);
	register_post_type( 'location', $args );
}
add_action( 'init', 'location_post_type');


function ecovideo_post_type() {
	$labels = array(
		'name'                => _x( 'Video Gallery', 'Post Type General Name', 'text_domain' ),
		'singular_name'       => _x( 'Video', 'Post Type Singular Name', 'text_domain' ),
		'menu_name'           => __( 'Video Gallery', 'text_domain' ),
		'parent_item_colon'   => __( 'Parent Video:', 'text_domain' ),
		'all_items'           => __( 'All Videos', 'text_domain' ),
		'view_item'           => __( 'View Videos', 'text_domain' ),
		'add_new_item'        => __( 'Add New Video', 'text_domain' ),
		'add_new'             => __( 'New Video', 'text_domain' ),
		'edit_item'           => __( 'Edit Video', 'text_domain' ),
		'update_item'         => __( 'Update Video', 'text_domain' ),
		'search_items'        => __( 'Search Video', 'text_domain' ),
		'not_found'           => __( 'No video found', 'text_domain' ),
		'not_found_in_trash'  => __( 'No video found in Trash', 'text_domain' ),
	);
	$args = array(
		'label'               => __( 'Video', 'text_domain' ),
		'description'         => __( 'Video information pages', 'text_domain' ),
		'labels'              => $labels,
		'taxonomies'          => array('category', 'post_tag'),
		'menu_position'       => 20,
		'public' 				=> true,
		'has_archive' 			=> true,
		'exclude_from_search' 	=> true,
		'publicly_queryable' 	=> false,
		'menu_icon' 			=> 'dashicons-video-alt3',
		'supports' 				=> array( 'title', 'excerpt'),
	);
	register_post_type( 'ecovideo', $args );
}
add_action( 'init', 'ecovideo_post_type');



function ecophoto_post_type() {
	$labels = array(
		'name'                => _x( 'Photo Gallery', 'Post Type General Name', 'text_domain' ),
		'singular_name'       => _x( 'Photo', 'Post Type Singular Name', 'text_domain' ),
		'menu_name'           => __( 'Photo Gallery', 'text_domain' ),
		'parent_item_colon'   => __( 'Parent Photo:', 'text_domain' ),
		'all_items'           => __( 'All Photos', 'text_domain' ),
		'view_item'           => __( 'View Photos', 'text_domain' ),
		'add_new_item'        => __( 'Add New Photo', 'text_domain' ),
		'add_new'             => __( 'New Photo', 'text_domain' ),
		'edit_item'           => __( 'Edit Photo', 'text_domain' ),
		'update_item'         => __( 'Update Photo', 'text_domain' ),
		'search_items'        => __( 'Search Photo', 'text_domain' ),
		'not_found'           => __( 'No photos found', 'text_domain' ),
		'not_found_in_trash'  => __( 'No photos found in Trash', 'text_domain' ),
	);
	$args = array(
		'label'               => __( 'Photo', 'text_domain' ),
		'description'         => __( 'Photo information pages', 'text_domain' ),
		'labels'              => $labels,
		'taxonomies'          => array('category', 'post_tag'),
		'menu_position'       => 20,
		'public' 				=> true,
		'has_archive' 			=> true,
		'exclude_from_search' 	=> true,
		'publicly_queryable' 	=> false,
		'menu_icon' 			=> 'dashicons-format-image',
		'supports' 				=> array( 'title', 'thumbnail', 'excerpt', 'custom-fields'),
	);
	register_post_type( 'ecophoto', $args );
}
add_action( 'init', 'ecophoto_post_type');

// add post-formats to post_type 'ecophoto'
add_action('init', 'eco_post_formats_to_ecophoto', 11);
function eco_post_formats_to_ecophoto(){
    add_post_type_support( 'ecophoto', 'post-formats' );
    register_taxonomy_for_object_type( 'post_format', 'ecophoto' );
}

// add custom fields to post_type 'ecophoto'
require get_template_directory() . '/inc/ecophoto/ecophoto_fields.php';


function facebookcomment_post_type() {
	$labels = array(
		'name'                => _x( 'Facebook Comments', 'Post Type General Name', 'text_domain' ),
		'singular_name'       => _x( 'Facebook Comment', 'Post Type Singular Name', 'text_domain' ),
		'menu_name'           => __( 'Facebook Comments', 'text_domain' ),
		'parent_item_colon'   => __( 'Parent Comment:', 'text_domain' ),
		'all_items'           => __( 'All Comments', 'text_domain' ),
		'view_item'           => __( 'View Comments', 'text_domain' ),
		'add_new_item'        => __( 'Add New Comment', 'text_domain' ),
		'add_new'             => __( 'New Comment', 'text_domain' ),
		'edit_item'           => __( 'Edit Comment', 'text_domain' ),
		'update_item'         => __( 'Update Comment', 'text_domain' ),
		'search_items'        => __( 'Search Comments', 'text_domain' ),
		'not_found'           => __( 'No Comments found', 'text_domain' ),
		'not_found_in_trash'  => __( 'No Comments found in Trash', 'text_domain' ),
	);
	$args = array(
		'label'               => __( 'Comment', 'text_domain' ),
		'description'         => __( 'Comment information pages', 'text_domain' ),
		'labels'              => $labels,
		'public' 				=> true,
		'has_archive' 			=> true,
		'exclude_from_search' 	=> true,
		'publicly_queryable' 	=> false,
		'menu_icon' 			=> 'dashicons-format-quote',
		'supports' 				=> array( 'title', 'thumbnail', 'excerpt'),
		'taxonomies'          	=> array('category', 'post_tag' )
	);
	register_post_type( 'eco_fb_comment', $args );
}
add_action( 'init', 'facebookcomment_post_type');


// Thumbnail size for Facebook comments (100x100px)
add_action( 'after_setup_theme', 'facebookcomment_image' );
function facebookcomment_image() {
    add_image_size( 'square-thumbnail', 100, 100 );
}


if (!function_exists('ecolinewindowsWidgetsInit')) {
    /**
     * Register widget areas
     */
    function ecolinewindowsWidgetsInit() {
        register_sidebar(array(
            'name' => __('Homepage Banner Test', 'ecolinewindows'),
            'id' => 'widget-banner-home',
            'before_widget' => '<div id="header-banner-text-home">',
            'after_widget' => '</div>',
            'before_title' => '',
            'after_title' => '',
        ));

		register_sidebar(array(
            'name' => __('Banner Flip', 'ecolinewindows'),
            'id' => 'widget-banner-flip-home',
            'before_widget' => '<aside class="sidebar" id="sidebar-flip"><div class="container"><div class="row justify-content-center"><div class="col-md-9 col-lg-6 text-center">',
            'after_widget' => '</div></div></div></aside>',
            'before_title' => '<h3>',
            'after_title' => '</h3>',
        ));

		register_sidebar(array(
            'name' => __('Window Line', 'ecolinewindows'),
            'id' => 'widget-product-line',
            'before_widget' => '<section class="container mt-5" id="sidebar-windows-lineup">',
            'after_widget' => '</section>',
            'before_title' => '<header class="d-none"><h2>',
            'after_title' => '</h2></header>',
		));
		
		register_sidebar(array(
            'name' => __('Window Features', 'ecolinewindows'),
            'id' => 'widget-product-features',
            'before_widget' => '<section id="efficiency-section">',
            'after_widget' => '</section>',
            'before_title' => '<header class="container text-center pt-3"><h2><strong>',
            'after_title' => '</strong></h2><div class="row justify-content-center pt-2"><div class="col-3 border-underline">&nbsp;</div></div></header>',
		));
		
		register_sidebar(array(
            'name' => __('Window Appearance', 'ecolinewindows'),
            'id' => 'widget-product-appearance',
            'before_widget' => '<section class="container-fluid mb-5"><div class="row align-items-stretch"><div class="col-lg-4 bg-custom-windows"></div><div class="col-lg-8 col-xl-6 p-lg-5 text-center text-lg-left">',
            'after_widget' => '</div></div></section>',
            'before_title' => '<header><h2>',
            'after_title' => '</h2></header>',
		));

		register_sidebar(array(
            'name' => __('Window Options', 'ecolinewindows'),
            'id' => 'widget-product-options',
            'before_widget' => '',
            'after_widget' => '',
            'before_title' => '',
            'after_title' => '',
		));

		register_sidebar(array(
            'name' => __('Window Warranty', 'ecolinewindows'),
            'id' => 'widget-product-warranty',
            'before_widget' => '<section class="my-2 mt-5 pt-5 pb-5" id="warranty-section">',
            'after_widget' => '</section>',
            'before_title' => '<header class="container text-center"><h2 class="">Covered by <strong>',
            'after_title' => '</strong></h2><div class="row justify-content-center pt-2"><div class="col-3 border-underline">&nbsp;</div></div></header>',
		));

		register_sidebar(array(
            'name' => __('Doors Product Line', 'ecolinewindows'),
            'id' => 'widget-product-line-doors',
            'before_widget' => '<div class="bg-light">',
            'after_widget' => '</div>',
            'before_title' => '',
            'after_title' => '',
		));

		register_sidebar(array(
            'name' => __('Financing', 'ecolinewindows'),
            'id' => 'widget-financing',
            'before_widget' => '<section class="container-fluid mb-lg-5 bg-light"><div class="row justify-content-end align-items-stretch"><div class="col-lg-3 p-lg-5 my-lg-5"><div class="bg-white p-3 p-lg-5 text-center text-lg-left overlapped-right shadowed">',
            'after_widget' => '</div></div><div class="col-lg-6 bg-financing"></div></div></section>',
            'before_title' => '<span class="fa-stack fa-lg"><i class="fa fa-circle fa-stack-2x text-primary"></i><i class="fa fa-usd fa-stack-1x fa-inverse"></i></span><header><h2>',
            'after_title' => '</h2></header>',
		));
		
		register_sidebar(array(
            'name' => __('Content CTA', 'ecolinewindows'),
            'id' => 'widget-cta',
            'before_widget' => '<section><div class="container">',
            'after_widget' => '</div></section>',
            'before_title' => '',
            'after_title' => '',
		));

		register_sidebar(array(
            'name' => __('Shadowed Below Content', 'ecolinewindows'),
            'id' => 'widget-shadowed',
            'before_widget' => '<section class="sidebar sidebar-shadowed">',
            'after_widget' => '</section>',
            'before_title' => '<header><h2 class="text-center">',
            'after_title' => '</h2></header>',
        ));

		register_sidebar(array(
            'name' => __('Pricing Calculator', 'ecolinewindows'),
            'id' => 'widget-pricing',
            'before_widget' => '<aside class="padding-top-4rem padding-bottom-4rem text-center bg-grey-light px-4">',
            'after_widget' => '</aside>',
            'before_title' => '<header class="d-none">Window Replacement Price Calculator<h2>',
            'after_title' => '</h2></header>',
        ));

		register_sidebar(array(
            'name' => __('Green Parallax', 'ecolinewindows'),
            'id' => 'widget-green-parallax',
			'class' => '',
            'before_widget' => '<section class="sidebar sidebar-overlayed sidebar-overlayed-green d-none d-md-inline-block " style="background-image: url(/wp-content/uploads/images/custom-replacement-window.jpg)"><div class="container text-center my-5">',
            'after_widget' => '</div></section>',
            'before_title' => '<header class="text-center"><div class="row justify-content-md-center"><div class="col-4 col-sm-2"><img class="img-fluid " src="/wp-content/uploads/images/energystar.png" width="81" height="83" alt=""></div><div class="col-4 col-sm-2"><img class="img-fluid " src="/wp-content/uploads/images/nafs-08.png" width="100" height="83" alt=""></div></div><h2 class="margin-top-30px margin-bottom-30px d-block">',
            'after_title' => '</h2></header>',
        ));

		register_sidebar(array(
            'name' => __('Footer Locations', 'ecolinewindows'),
            'id' => 'locations',
            'before_widget' => '<div class="col-sm-4 col-md-3 contact-city">',
            'after_widget' => '</div>',
            'before_title' => '<h3>',
            'after_title' => '</h3>',
        ));

		register_sidebar(array(
            'name' => __('Footer CTA', 'ecolinewindows'),
            'id' => 'footer-cta',
            'before_widget' => '',
            'after_widget' => '',
            'before_title' => '',
            'after_title' => '',
        ));

		register_sidebar(array(
            'name' => __('Footer Social Networks', 'ecolinewindows'),
            'id' => 'footer-smm',
            'before_widget' => '',
            'after_widget' => '',
            'before_title' => '',
            'after_title' => '',
        ));

		register_sidebar(array(
            'name' => __('Hidden (System)', 'ecolinewindows'),
            'id' => 'system',
            'before_widget' => '',
            'after_widget' => '',
            'before_title' => '',
            'after_title' => '',
        ));
		register_sidebar(array(
            'name' => __('Blog Sidebar', 'ecolinewindows'),
            'id' => 'blog',
            'before_widget' => '',
            'after_widget' => '',
            'before_title' => '',
            'after_title' => '',
        ));

    }// ecolinewindowsWidgetsInit
}
add_action('widgets_init', 'ecolinewindowsWidgetsInit');


if (!function_exists('ecolinewindowsEnqueueScripts')) {
    /**
     * Enqueue scripts & styles
     */
    function ecolinewindowsEnqueueScripts()
    {
		global $wp_scripts;

		///CSS
		wp_enqueue_style('bootstrap-style', get_template_directory_uri() . '/js/bootstrap/css/bootstrap.min.css');
		wp_enqueue_style('animate', get_template_directory_uri() . '/js/animate.css/animate.min.css');
		wp_enqueue_style('font-awesome', get_template_directory_uri() . '/js/font-awesome/css/font-awesome.min.css');
		wp_enqueue_style('fancybox', get_template_directory_uri() . '/js/fancybox/dist/jquery.fancybox.min.css');
		wp_enqueue_style('slick-carousel', get_template_directory_uri() . '/js/slider/slick.min.css');
		wp_enqueue_style('ecolinewindows-style', get_template_directory_uri().'/style.css');




		// JS
		wp_enqueue_script('bootstrap', get_template_directory_uri() . '/js/bootstrap/js/bootstrap.bundle.min.js', array(), false, true);
		wp_enqueue_script('dropdown', get_template_directory_uri() . '/js/dropdown/js/script.min.js', array(), false, true);
		wp_enqueue_script('fancybox', get_template_directory_uri() . '/js/fancybox/dist/jquery.fancybox.min.js', array(), false, true);
		wp_enqueue_script('sticky', get_template_directory_uri() . '/js/sticky/jquery.sticky-kit.min.js', array(), false, true);

        wp_enqueue_script('temp-script', get_template_directory_uri() . '/js/temp-script.js');//temp-script
		wp_enqueue_script('slick-carousel', get_template_directory_uri() . '/js/slider/slick.min.js', array(), false, true);
		//wp_enqueue_script('slick-carousel-settings', get_template_directory_uri() . '/js/slider/slider.js', array(), false, true);

		wp_enqueue_script('main-script', get_template_directory_uri() . '/js/script.js', array(), true, true);
		//wp_enqueue_script('main-script', get_template_directory_uri() . '/js/script.js', array(), rand(999, 9999), true);

		if ( is_post_type_archive( 'product' )){
			wp_enqueue_script('isotope', get_template_directory_uri() . '/js/isotope/isotope.pkgd.min.js', array(), false, false);
			wp_enqueue_script('configurator', get_template_directory_uri() . '/js/configurator.js', array(), false, false);

		}

		if ( is_page( '3009' )){
			wp_enqueue_script('calx', get_template_directory_uri() . '/js/calx/jquery-calx-2.2.7.min.js', array(), false, true);
			//wp_enqueue_script('calculator', get_template_directory_uri() . '/js/calculator.min.js', array('bootstrap'), rand(1000,9999), true);
			
			wp_enqueue_script('calculator', get_template_directory_uri() . '/js/calculator.js', array('bootstrap'), rand(1000,9999), true);

		}

		if(is_page('14100')){
			wp_enqueue_script('calx_', get_template_directory_uri() . '/js/calx/jquery-calx-2.2.7.min.js', array(), false, true);
			wp_enqueue_script('calculator_', get_template_directory_uri() . '/js/calx/calculator.js', array('bootstrap'), rand(1000,9999), true);

		}

      wp_localize_script('jquery', 'jivosite_var', array(
        'widget_id' => get_option('jivosite_widget_id')
      ));

    }
}
add_action('wp_enqueue_scripts', 'ecolinewindowsEnqueueScripts');

function remove_unused_cssjs(){

	wp_dequeue_style( 'wp-block-library' );
	wp_dequeue_style( 'font-awesome-css' );
}
add_action( 'wp_enqueue_scripts', 'remove_unused_cssjs', 100 );

/**
 * admin page displaying help.
 */
if (is_admin()) {
    require get_template_directory() . '/inc/ecolinewindowsAdminHelp.php';
    $bbsc_adminhelp = new ecolinewindowsAdminHelp();
    add_action('admin_menu', array($bbsc_adminhelp, 'themeHelpMenu'));
    unset($bbsc_adminhelp);
}


/**
 * Custom template tags for this theme.
 */
require get_template_directory() . '/inc/template-tags.php';


/**
 * Custom functions that act independently of the theme templates.
 */
require get_template_directory() . '/inc/extras.php';


/**
 * Custom dropdown menu and navbar in walker class
 */
require get_template_directory() . '/inc/ecolinewindowsMyWalkerNavMenu.php';


/**
 * --------------------------------------------------------------
 * Theme widget & widget hooks
 * --------------------------------------------------------------
 */
require get_template_directory() . '/inc/widgets/ecolinewindowsSearchWidget.php';
require get_template_directory() . '/inc/template-widgets-hook.php';

function get_utm_referrer() {
    return $_COOKIE['_referrer'];
}
add_shortcode('get_utm_referrer', 'get_utm_referrer');


function get_payment_form() {
	include_once "payment_processor/processor.php";
	//return $content;
}
add_shortcode('payment_form', 'get_payment_form');

function get_payment_terminal() {
    include_once "payment_terminal/index.php";
}
add_shortcode('payment_terminal', 'get_payment_terminal');

function get_payment_terminal_new() {
    include_once "payment_terminal_new/index.php";
}
add_shortcode('payment_terminal_new', 'get_payment_terminal_new');

require 'ecoline_sales_map/custom_functions.php';

function show_top_rated(){
    global $post;
    ?>
    <aside class="sidebar" id="sidebar-flip">
        <div class="container">
            <div class="row justify-content-center">
                <div class="col-md-9 col-lg-6 text-center">
                    <h3>Top-rated by</h3>
                    <div class="textwidget">
                      <?php
                      $jcc = ($post->post_name === 'ottawa') ? ' justify-content-center' : ' justify-content-around';
                      //$flex_fill = ($post->post_name === 'ottawa') ? 'flex-fill-bs' : 'flex-fill-bs-auto';
                      $flex_fill = 'flex-fill-bs';
                      ?>

                        <div class="d-flex <?php echo $jcc ?>" id="sidebar-flip-logos">
                            <?php if ($post->post_name != 'ottawa'): ?>
                                <div class="text-center px-1 px-sm-0 <?php echo $flex_fill ?>">
                                    <img class="img-fluid" src="/wp-content/uploads/bbb-a-png-1.png" width="126" height="60"
                                     alt="BBB Rated Windows Replacement Company"/>
                                </div>
                            <?php endif; ?>
                          <?php /* ?>
                            <div class="text-center px-1 px-sm-0 <?php echo $flex_fill ?>">
                                <img class="img-fluid" src="/wp-content/uploads/googleplus.png" width="105" height="60"
                                     alt="Top Rated Windows Replacement Company"/>
                            </div>
                          <?php */ ?>


                          <?php if ($post->post_name === 'ottawa'): ?>
                            <div class="text-center px-1 px-sm-0 <?php echo $flex_fill ?>">
                              <img class="img-fluid" src="/wp-content/uploads/bbb-a-png-1.png" width="126" height="60"
                                   alt="BBB Rated Windows Replacement Company"/>
                            </div>
                          <?php endif; ?>

                            <div class="text-center px-1 px-sm-0 <?php echo $flex_fill ?>">
                                <img class="img-fluid" src="/wp-content/uploads/image.png" width="52" height="60"
                                     alt="TrustedPros Best of  Replacement Windows Companies"/>
                            </div>
                            <div class="text-center px-1 px-sm-0 <?php echo $flex_fill ?>">
                                <img class="img-fluid" src="/wp-content/uploads/HS-BOA-2020-Logo-91x100.png" width="55" height="60"
                                     alt="HomeStars Best Rated Replacement Windows Company"/>
                            </div>
                        </div>
                        <a href="/covid-19/" style="display:block;padding:15px 0;background-color:#0060c7;color:#fff;width:100%;margin-top:5px;border:1px solid #4fa984;">COVID-19</a>
                    </div>
                </div>
            </div>
        </div>
    </aside>
    <?php
}

add_shortcode('show-top-rated', 'show_top_rated');

remove_action('wp_footer', 'jivositeAppend', 100000);



 

/*
add_filter( 'the_content', 'filter_ftrailingslashit' );
function filter_ftrailingslashit( $content ) {

  $urls = array();

  preg_match_all("/<[Aa][\s]{1}[^>]*[Hh][Rr][Ee][Ff][^=]*=[ '\"\s]*([^ \"'>\s#]+)[^>]*>/", $content, $matches);
  $matches_urls = $matches[1];

  foreach ($matches_urls as $matches_url) {
    if(!preg_match( '/^[a-z0-9-]+?\.php|jpg|jpeg|gif|png|html|cfm/i', $matches_url )) {
      $urls[] = $matches_url;
    }
  }

  foreach ($urls as $url) {
    //$content = preg_replace('"' . (string)$url . '"', trailingslashit($url), $content);
  }


  return $content;
}*/



add_action('acf/init', 'theme_acf_init');
function theme_acf_init()
{
  if (function_exists('acf_add_options_page')) {
    $option_page = acf_add_options_page(array(
      'page_title' => __('Theme general settings', 'my_text_domain'),
      'menu_title' => __('Theme settings', 'my_text_domain'),
      'menu_slug' => 'theme-general-settings',
      'position' => 1.1
    ));
  }
}
 

function wpplus_remove_jquery_migrate( $scripts ) { 
    if ( ! is_admin() && isset( $scripts->registered['jquery'] ) ) { 
        $script = $scripts->registered['jquery']; 
        if ( $script->deps ) { 
            $script->deps = array_diff( $script->deps, array( 'jquery-migrate' ) ); 
        } 
    } 
} 
add_action( 'wp_default_scripts', 'wpplus_remove_jquery_migrate' );