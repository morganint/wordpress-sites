<?php 
/**
 * Displaying archive page (Door Features)
 * 
 * @package ecolinewindows
 */

get_header(); ?> 

<section class="container-fluid margin-top-4rem">
	<header class="page-header margin-bottom-1rem">
		<h1 class="text-center">Door Features & Options</h1>
		<p class="text-center abovelined"><span>&nbsp;</span></p>
	</header>
	<?php if (have_posts()) { ?> 
	<?php $number = $wp_the_query->post_count; ?>
		<?php while (have_posts()) { ?>
			<?php $counter++; ?>
			<?php if ((($counter - 1) % 4 == 0) or ($counter == 1)) {echo '<div class="row">';} ?>
				<?php the_post(); ?> 
					<div class="col-sm-6 col-md-4 col-lg-3 margin-bottom-2rem">
						<div class="card card-glass mb-5">
							<a href="<?php echo get_permalink(); ?>" class="main-image-link rounded" style="background-image: url(<?php the_post_thumbnail_url('thumbnail'); ?>); background-size: cover; background-repeat: no-repeat; background-position: center center; height: 320px;"></a>
							<a href="<?php echo get_permalink(); ?>" class="d-flex flex-title rounded">
								<h2 class="p-4 text-20px" style="height: 86px"><?php the_title(); ?></h2>
							</a>
						</div>
					</div>
			<?php if (($counter % 4 == 0) && ($counter != $number)) {echo '</div>'; ?>
			<?php } elseif ($counter == $number) { ?>
					<div class="col-sm-6 col-md-4 col-lg-3 margin-bottom-2rem">
						<div class="card card-glass mb-5">
							<a href="/decorative-door-glass/" class="main-image-link rounded" style="background-image: url(/wp-content/uploads/Kimberley.jpg); background-size: cover; background-repeat: no-repeat; background-position: center center; height: 320px;"></a>
							<a href="/decorative-door-glass/" class="d-flex flex-title rounded">
								<h2 class="p-4 text-20px" style="height: 86px">Decorative Door Glass</h2>
							</a>
						</div>
					</div>
				</div>
			<?php } ?>
		<?php }	?> 
	<?php } ?> 
</section>

<?php 
	$term = get_queried_object();
	$taxonomy = array( "name" => 'door-types' , "slug" => 'door-types');
	$custom_post_type = "doors";
	if ( have_posts() ):
		the_post();
	endif;
?>

<?php $categories = get_terms($taxonomy['name'], 'orderby=id'); ?>

<div class="padding-top-rem padding-bottom-3rem d-none d-md-block">
	<aside class="container-fluid margin-bottom-4rem">
		<header><h2 class="text-center margin-bottom-2rem">Door Style Collections</h2></header>
		<div class="row justify-content-center">
			<div class="col-8">
				<div class="row justify-content-center">
					<?php foreach( $categories as $category ) { ?>
						<?php $background = get_field('background', 'door-types_' . $category->term_id); ?>
						<div class="col-2 text-center">				
							<div class="margin-bottom-1rem"><a href="/<?php echo $category->taxonomy; ?>/<?php echo $category->slug; ?>/"><div style="background-image: url(<?php echo $background; ?>);" class="rounded-circle rounded-door d-inline-block"></div></a></div>
							<h4 class="text-1rem font-weight-bold text-secondary"><a href="/<?php echo $category->taxonomy; ?>/<?php echo $category->slug; ?>/"><?php echo $category->name; ?></a></h4>				
						</div>
					<?php } ?>
				</div>
			</div>
		</div>
	</aside>
</div>


<?php get_footer(); ?>  