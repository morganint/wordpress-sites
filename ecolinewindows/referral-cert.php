<?php
/**
 * Template for displaying pages
 * Template Name: Referral Cert
 * @package ecolinewindows
 */
?> 
<?php get_header(); ?>

	<section class="container margin-top-4rem">
		<?php while (have_posts()) { ?>
			<?php the_post();
			ob_start();
?>
            [jsoncontentimporter url="http://crm.projero.pro/?status=<?php echo str_replace('/','',str_replace('referral-cert','',$_SERVER['REQUEST_URI']));?>"]
            <h2>Referral by {customername}</h2>
            <img style="width: 80%;" src="https://apps.ecolinewindows.ca/?referral={uniquehash}" />
            [/jsoncontentimporter]
            <br/>
            Share Your Referral Certificate:
            <div class="addthis_toolbox addthis_default_style addthis_32x32_style"></div>


<?php

$contents = ob_get_contents();
ob_end_clean();

// see the contents now
echo do_shortcode($contents);
the_content();

?>

<?php
		 } ?> 
	</section>
	<script src="https://s7.addthis.com/js/300/addthis_widget.js#async=1" type="text/javascript"></script>
<script type="text/javascript">
    // Call this function once the rest of the document is loaded
    function loadAddThis() {
        addthis.init()
    }
</script>
<?php dynamic_sidebar( 'widget-shadowed' ); ?>
<?php get_sidebar( 'contacts' ); ?>
<?php get_footer(); ?> 