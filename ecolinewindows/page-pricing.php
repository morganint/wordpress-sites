<?php
/**
 * Template for displaying pages
 * Template Name: Pricing
 * @package ecolinewindows
 */
?>
<?php get_header(); ?>
<div class="container-fluid container-fluid-margined margin-top-4rem margin-bottom-4rem" id="pricing-page">
	<section class="container-spacer padding-bottom-4rem">
		<?php while (have_posts()) { ?>
			<?php the_post(); ?>
			<?php the_content(); ?>
		<?php } ?>
		<div class="row justify-content-center matrix-row d-none">
			<div class="col-lg-9">
				<form id="minbase">
					<p class="label-step active-step label-step-1 text-center text-md-left"><strong>STEP 1</strong> Select window type</p>
					<?php echo do_shortcode( ' [table id=8 /] ' ); ?>
					<div class="d-none">
						<span class="row-2" data-cell="Z1" data-formula="SUM(A1:F1)" data-format="$0,0"></span>
						<span class="row-3" data-cell="Z2" data-formula="SUM(A2:F2)" data-format="$0,0"></span>
						<span class="row-4" data-cell="Z3" data-formula="SUM(A3:F3)" data-format="$0,0"></span>
						<span class="row-5" data-cell="Z4" data-formula="SUM(A4:F4)" data-format="$0,0"></span>
						<span class="row-6" data-cell="Z5" data-formula="SUM(A5:F5)" data-format="$0,0"></span>
						<span class="row-7" data-cell="Z6" data-formula="SUM(A6:F6)" data-format="$0,0"></span>
						<span class="row-8" data-cell="Z7" data-formula="SUM(A7:F7)" data-format="$0,0"></span>
						<span class="row-9" data-cell="Z8" data-formula="SUM(A8:F8)" data-format="$0,0"></span>
						<span class="row-10" data-cell="Z9" data-formula="SUM(A9:F9)" data-format="$0,0"></span>
					</div>
				</form>
				<form id="minframe">
				<p class="label-step label-step-2 text-center text-md-left"><strong>STEP 2</strong> Select installation type <button type="button" class="button-hint" data-container="body" data-toggle="popover" data-placement="top" data-content="In a retrofit installation, new units are inserted into EXISTING frames. This installation is less labour intensive and less invasive on the structure of your house. In a full-frame installation, all window hardware including casings, jamb extensions, and brickmoulds get replaced. The existing frame in your windows is torn out down to the brick on either side. <a href='https://www.ecolinewindows.ca/full-frame-vs-retrofit-best-window-installation/' target='_blank'>https://www.ecolinewindows.ca/full-frame-vs-retrofit-best-window-installation/</a>"><i class="fa fa-question-circle" aria-hidden="true"></i></button></p>
					<?php echo do_shortcode( ' [table id=11 /] ' ); ?>
					<div class="d-none">
						<span class="row-2" data-cell="Z1" data-formula="SUM(A1:F1)" data-format="$0,0"></span>
						<span class="row-3" data-cell="Z2" data-formula="SUM(A2:F2)" data-format="$0,0"></span>
						<span class="row-4" data-cell="Z3" data-formula="SUM(A3:F3)" data-format="$0,0"></span>
						<span class="row-5" data-cell="Z4" data-formula="SUM(A4:F4)" data-format="$0,0"></span>
						<span class="row-6" data-cell="Z5" data-formula="SUM(A5:F5)" data-format="$0,0"></span>
						<span class="row-7" data-cell="Z6" data-formula="SUM(A6:F6)" data-format="$0,0"></span>
						<span class="row-8" data-cell="Z7" data-formula="SUM(A7:F7)" data-format="$0,0"></span>
						<span class="row-9" data-cell="Z8" data-formula="SUM(A8:F8)" data-format="$0,0"></span>
						<span class="row-10" data-cell="Z9" data-formula="SUM(A9:F9)" data-format="$0,0"></span>
						<dl class="hint">
						  <dt>Retrofit installation</dt>
						  <dd>In a retrofit installation, new windows are inserted into existing frames. This is ideal in preserving the look of a window and is a good option if the frame or structural components are not damaged or aged.</dd>
						  <dt>Full frame installation</dt>
						  <dd>In a full frame installation, the window is replaced with new brickmoulds and casing. This kind of installation is also the best way to ensure the longevity and proper efficiency of new windows.</dd>
						</dl>
					</div>
				</form>
				<form id="minext">
					<p class="label-step label-step-2-1 text-center text-md-left d-none"><strong>STEP 2.1</strong> Select exterior options <button type="button" class="button-hint" data-container="body" data-toggle="popover" data-placement="top"><i class="fa fa-question-circle" aria-hidden="true"></i></button></p>
					<?php echo do_shortcode( ' [table id=18 /] ' ); ?>
					<div class="d-none">
						<span class="row-2" data-cell="Z1" data-formula="SUM(A1:F1)" data-format="$0,0"></span>
						<span class="row-3" data-cell="Z2" data-formula="SUM(A2:F2)" data-format="$0,0"></span>
						<span class="row-4" data-cell="Z3" data-formula="SUM(A3:F3)" data-format="$0,0"></span>
						<span class="row-5" data-cell="Z4" data-formula="SUM(A4:F4)" data-format="$0,0"></span>
						<span class="row-6" data-cell="Z5" data-formula="SUM(A5:F5)" data-format="$0,0"></span>
						<span class="row-7" data-cell="Z6" data-formula="SUM(A6:F6)" data-format="$0,0"></span>
						<span class="row-8" data-cell="Z7" data-formula="SUM(A7:F7)" data-format="$0,0"></span>
						<span class="row-9" data-cell="Z8" data-formula="SUM(A8:F8)" data-format="$0,0"></span>
						<span class="row-10" data-cell="Z9" data-formula="SUM(A9:F9)" data-format="$0,0"></span>
						<dl class="hint">
						  <dt>Aluminum capping</dt>
						  <dd>Aluminum capping or cladding is the way most windows were finished in the past. There is still a wooden brickmould that fastens the window to the wall. The actual exterior side of the wooden brickmould gets covered with bent aluminum capping on the top.</dd>
						  <dt>Vinyl brickmoulds</dt>
						  <dd>There are several reasons why vinyl brickmoulds are a better choice compared to aluminum capping. For starters, it is all about materials. Unlike combination of wood and aluminum, vinyl is much better at withstanding contact with moisture and the elements.</dd>
						</dl>
					</div>
				</form>
				<form id="minfeature">
					<p class="label-step label-step-3 text-center text-md-left"><strong>STEP 3</strong> Select features & options <button type="button" class="button-hint" data-container="body" data-toggle="popover" data-placement="top" data-content="Choose the features and options you'd like to add to your new windows. <a href='https://www.ecolinewindows.ca/window-features-options/ ' target='_blank'>https://www.ecolinewindows.ca/window-features-options/</a>"><i class="fa fa-question-circle" aria-hidden="true"></i></button></p>
					<?php echo do_shortcode( ' [table id=10 /] ' ); ?>
					<div class="d-none">
						<span class="row-2" data-cell="Z1" data-formula="SUM(A1:I1)" data-format="$0,0"></span>
						<span class="row-3" data-cell="Z2" data-formula="SUM(A2:I2)" data-format="$0,0"></span>
						<span class="row-4" data-cell="Z3" data-formula="SUM(A3:I3)" data-format="$0,0"></span>
						<span class="row-5" data-cell="Z4" data-formula="SUM(A4:I4)" data-format="$0,0"></span>
						<span class="row-6" data-cell="Z5" data-formula="SUM(A5:I5)" data-format="$0,0"></span>
						<span class="row-7" data-cell="Z6" data-formula="SUM(A6:I6)" data-format="$0,0"></span>
						<span class="row-8" data-cell="Z7" data-formula="SUM(A7:I7)" data-format="$0,0"></span>
						<span class="row-9" data-cell="Z8" data-formula="SUM(A8:I8)" data-format="$0,0"></span>
						<span class="row-10" data-cell="Z9" data-formula="SUM(A9:I9)" data-format="$0,0"></span>
						<dl class="hint">
						  <dt>Privacy Glass</dt>
						  <dd>Our glass is available in a multitude of designs and each can be tinted to gray or bronze to complement the interior, or ordered frosted to add privacy where desired.</dd>
						  <dt>Custom Solar Bond™ Colours</dt>
						  <dd>There are thirteen standard window colors for you to choose from. We can also custom colour match your Ecoline windows, so they co-ordinate with any existing decor finishes.</dd>
						  <dt>Interior Grills</dt>
						  <dd>Window grills are the decorative bars that are placed in between the panes of your window to give it a unique look, or match the aesthetic with other windows.</dd>
						  <dt>Triple-pane Windows</dt>
						  <dd>An additional pane of glass in the window creates a better insulating barrier. Compared to double-pane units, triple-pane windows have allow less heat to escape, have less condensation, and are more soundproof.</dd>
						</dl>

					</div>
				</form>
				<form id="minloe">
					<p class="label-step label-step-4 text-center text-md-left"><strong>STEP 4</strong> Select number of LoE coats <button type="button" class="button-hint" data-container="body" data-toggle="popover" data-placement="top" data-content="Low-emissive glass coatings help retain more interior heat than ordinary glass in cold winter months (Energy-gain) while filtering our harmful UV rays and reducing solar heat gain (Sun shield) in warm summer months. Multiple combinations of LoE glass coatings are available to achieve various levels of performance. <a href='https://youtu.be/VVhotSzF9bw' target='_blank'>https://youtu.be/VVhotSzF9bw</a>"><i class="fa fa-question-circle" aria-hidden="true"></i></button></p>
					<?php echo do_shortcode( ' [table id=20 /] ' ); ?>
					<div class="d-none">
						<span class="row-2" data-cell="Z1" data-formula="SUM(A1:I1)" data-format="$0,0"></span>
						<span class="row-3" data-cell="Z2" data-formula="SUM(A2:I2)" data-format="$0,0"></span>
						<span class="row-4" data-cell="Z3" data-formula="SUM(A3:I3)" data-format="$0,0"></span>
						<span class="row-5" data-cell="Z4" data-formula="SUM(A4:I4)" data-format="$0,0"></span>
						<span class="row-6" data-cell="Z5" data-formula="SUM(A5:I5)" data-format="$0,0"></span>
						<span class="row-7" data-cell="Z6" data-formula="SUM(A6:I6)" data-format="$0,0"></span>
						<span class="row-8" data-cell="Z7" data-formula="SUM(A7:I7)" data-format="$0,0"></span>
						<span class="row-9" data-cell="Z8" data-formula="SUM(A8:I8)" data-format="$0,0"></span>
						<span class="row-10" data-cell="Z9" data-formula="SUM(A9:I9)" data-format="$0,0"></span>
						<dl class="hint">
						  <dt>LoE Coated Glass</dt>
						  <dd>Low-E is short for "low-emissivity". These coatings are great for several purposes: they keep the warmth inside your home during the winter, they reduce harmful effects of UV radiation, and provide a level of shading to windows in sunny rooms.</dd>
						  <dt>1 coat</dt>
						  <dd>1 coat, the standard low-emissivity coating helps keep the warmth inside your home and reduces harmful effects of UV.</dd>
						  <dt>2 coats</dt>
						  <dd>2 coats, a more advanced coating for even better insulation and reduced heat loss. Great for rooms all around the house.</dd>
						  <dt>3 coats</dt>
						  <dd>3 coats has the same insulating values as 2 coats but with added protection from the sunshine.</dd>
						</dl>
					</div>
				</form>

				<div class="d-none">
					<form id="maxbase">
						<h3>Base Price</h3>
						<?php echo do_shortcode( ' [table id=13 /] ' ); ?>
						<span class="row-2" data-cell="Z1" data-formula="SUM(A1:F1)" data-format="$0,0"></span>
						<span class="row-3" data-cell="Z2" data-formula="SUM(A2:F2)" data-format="$0,0"></span>
						<span class="row-4" data-cell="Z3" data-formula="SUM(A3:F3)" data-format="$0,0"></span>
						<span class="row-5" data-cell="Z4" data-formula="SUM(A4:F4)" data-format="$0,0"></span>
						<span class="row-6" data-cell="Z5" data-formula="SUM(A5:F5)" data-format="$0,0"></span>
						<span class="row-7" data-cell="Z6" data-formula="SUM(A6:F6)" data-format="$0,0"></span>
						<span class="row-8" data-cell="Z7" data-formula="SUM(A7:F7)" data-format="$0,0"></span>
						<span class="row-9" data-cell="Z8" data-formula="SUM(A8:F8)" data-format="$0,0"></span>
						<span class="row-10" data-cell="Z9" data-formula="SUM(A9:F9)" data-format="$0,0"></span>
					</form>
					<form id="maxframe">
						<h3>By Frame</h3>
						<?php echo do_shortcode( ' [table id=17 /] ' ); ?>
						<span class="row-2" data-cell="Z1" data-formula="SUM(A1:F1)" data-format="$0,0"></span>
						<span class="row-3" data-cell="Z2" data-formula="SUM(A2:F2)" data-format="$0,0"></span>
						<span class="row-4" data-cell="Z3" data-formula="SUM(A3:F3)" data-format="$0,0"></span>
						<span class="row-5" data-cell="Z4" data-formula="SUM(A4:F4)" data-format="$0,0"></span>
						<span class="row-6" data-cell="Z5" data-formula="SUM(A5:F5)" data-format="$0,0"></span>
						<span class="row-7" data-cell="Z6" data-formula="SUM(A6:F6)" data-format="$0,0"></span>
						<span class="row-8" data-cell="Z7" data-formula="SUM(A7:F7)" data-format="$0,0"></span>
						<span class="row-9" data-cell="Z8" data-formula="SUM(A8:F8)" data-format="$0,0"></span>
						<span class="row-10" data-cell="Z9" data-formula="SUM(A9:F9)" data-format="$0,0"></span>
					</form>
					<form id="maxext">
						<h3>Exterior options</h3>
						<?php echo do_shortcode( ' [table id=19 /] ' ); ?>
						<span class="row-2" data-cell="Z1" data-formula="SUM(A1:F1)" data-format="$0,0"></span>
						<span class="row-3" data-cell="Z2" data-formula="SUM(A2:F2)" data-format="$0,0"></span>
						<span class="row-4" data-cell="Z3" data-formula="SUM(A3:F3)" data-format="$0,0"></span>
						<span class="row-5" data-cell="Z4" data-formula="SUM(A4:F4)" data-format="$0,0"></span>
						<span class="row-6" data-cell="Z5" data-formula="SUM(A5:F5)" data-format="$0,0"></span>
						<span class="row-7" data-cell="Z6" data-formula="SUM(A6:F6)" data-format="$0,0"></span>
						<span class="row-8" data-cell="Z7" data-formula="SUM(A7:F7)" data-format="$0,0"></span>
						<span class="row-9" data-cell="Z8" data-formula="SUM(A8:F8)" data-format="$0,0"></span>
						<span class="row-10" data-cell="Z9" data-formula="SUM(A9:F9)" data-format="$0,0"></span>
					</form>
					<form id="maxfeature">
						<h3>By Features</h3>
						<?php echo do_shortcode( ' [table id=15 /] ' ); ?>
						<span class="row-2" data-cell="Z1" data-formula="SUM(A1:F1)" data-format="$0,0"></span>
						<span class="row-3" data-cell="Z2" data-formula="SUM(A2:F2)" data-format="$0,0"></span>
						<span class="row-4" data-cell="Z3" data-formula="SUM(A3:F3)" data-format="$0,0"></span>
						<span class="row-5" data-cell="Z4" data-formula="SUM(A4:F4)" data-format="$0,0"></span>
						<span class="row-6" data-cell="Z5" data-formula="SUM(A5:F5)" data-format="$0,0"></span>
						<span class="row-7" data-cell="Z6" data-formula="SUM(A6:F6)" data-format="$0,0"></span>
						<span class="row-8" data-cell="Z7" data-formula="SUM(A7:F7)" data-format="$0,0"></span>
						<span class="row-9" data-cell="Z8" data-formula="SUM(A8:F8)" data-format="$0,0"></span>
						<span class="row-10" data-cell="Z9" data-formula="SUM(A9:F9)" data-format="$0,0"></span>
					</form>
					<form id="maxloe">
						<h3>By LoE</h3>
						<?php echo do_shortcode( ' [table id=21 /] ' ); ?>
						<span class="row-2" data-cell="Z1" data-formula="SUM(A1:F1)" data-format="$0,0"></span>
						<span class="row-3" data-cell="Z2" data-formula="SUM(A2:F2)" data-format="$0,0"></span>
						<span class="row-4" data-cell="Z3" data-formula="SUM(A3:F3)" data-format="$0,0"></span>
						<span class="row-5" data-cell="Z4" data-formula="SUM(A4:F4)" data-format="$0,0"></span>
						<span class="row-6" data-cell="Z5" data-formula="SUM(A5:F5)" data-format="$0,0"></span>
						<span class="row-7" data-cell="Z6" data-formula="SUM(A6:F6)" data-format="$0,0"></span>
						<span class="row-8" data-cell="Z7" data-formula="SUM(A7:F7)" data-format="$0,0"></span>
						<span class="row-9" data-cell="Z8" data-formula="SUM(A8:F8)" data-format="$0,0"></span>
						<span class="row-10" data-cell="Z9" data-formula="SUM(A9:F9)" data-format="$0,0"></span>
					</form>
				</div>
			</div>
			<div class="col-lg-3 col-results tall d-none">

					<div class="total-wrapper text-center text-md-left sticky">
						<form id="calc">
							<p class="total">Estimated replacement cost:<br />
							<strong>$<span class="row-2" data-cell="A1" data-formula="#minbase!Z1+#minfeature!Z1+#minframe!Z1+#minext!Z1+#minloe!Z1" data-format="$0,0"></span><span class="row-3" data-cell="A2" data-formula="#minbase!Z2+#minfeature!Z2+#minframe!Z2+#minext!Z2+#minloe!Z2" data-format="$0,0"></span><span class="row-4" data-cell="A3" data-formula="#minbase!Z3+#minfeature!Z3+#minframe!Z3+#minext!Z3+#minloe!Z3" data-format="$0,0"></span><span class="row-5" data-cell="A4" data-formula="#minbase!Z4+#minfeature!Z4+#minframe!Z4+#minext!Z4+#minloe!Z4" data-format="$0,0"></span><span class="row-6" data-cell="A5" data-formula="#minbase!Z5+#minfeature!Z5+#minframe!Z5+#minext!Z5+#minloe!Z5" data-format="$0,0"></span><span class="row-7" data-cell="A6" data-formula="#minbase!Z6+#minfeature!Z6+#minframe!Z6+#minext!Z6+#minloe!Z6" data-format="$0,0"></span><span class="row-8" data-cell="A7" data-formula="#minbase!Z7+#minfeature!Z7+#minframe!Z7+#minext!Z7+#minloe!Z7" data-format="$0,0"></span><span class="row-9" data-cell="A8" data-formula="#minbase!Z8+#minfeature!Z8+#minframe!Z8+#minext!Z8+#minloe!Z8" data-format="$0,0"></span><span class="row-10" data-cell="A9" data-formula="#minbase!Z9+#minfeature!Z9+#minframe!Z9+#minext!Z1+#minloe!Z9" data-format="$0,0"></span> - $<span class="row-2" data-cell="B1" data-formula="#maxbase!Z1+#maxfeature!Z1+#maxframe!Z1+#maxext!Z1+#maxloe!Z1" data-format="$0,0"></span><span class="row-3" data-cell="B2" data-formula="#maxbase!Z2+#maxfeature!Z2+#maxframe!Z2+#maxext!Z2+#maxloe!Z2" data-format="$0,0"></span><span class="row-4" data-cell="B3" data-formula="#maxbase!Z3+#maxfeature!Z3+#maxframe!Z3+#maxext!Z3+#maxloe!Z3" data-format="$0,0"></span><span class="row-5" data-cell="B4" data-formula="#maxbase!Z4+#maxfeature!Z4+#maxframe!Z4+#maxext!Z4+#maxloe!Z4" data-format="$0,0"></span><span class="row-6" data-cell="B5" data-formula="#maxbase!Z5+#maxfeature!Z5+#maxframe!Z5+#maxext!Z5+#maxloe!Z5" data-format="$0,0"></span><span class="row-7" data-cell="B6" data-formula="#maxbase!Z6+#maxfeature!Z6+#maxframe!Z6+#maxext!Z6+#maxloe!Z6" data-format="$0,0"></span><span class="row-8" data-cell="B7" data-formula="#maxbase!Z7+#maxfeature!Z7+#maxframe!Z7+#maxext!Z7+#maxloe!Z7" data-format="$0,0"></span><span class="row-9" data-cell="B8" data-formula="#maxbase!Z8+#maxfeature!Z8+#maxframe!Z8+#maxext!Z8+#maxloe!Z8" data-format="$0,0"></span><span class="row-10" data-cell="B9" data-formula="#maxbase!Z9+#maxfeature!Z9+#maxframe!Z9+#maxext!Z9+#maxloe!Z9" data-format="$0,0"></span></strong>
							</p>
							<p class="text-center"><button type="button" class="btn btn-default btn-sm d-none d-lg-block" id="reset" /><i class="fa fa-refresh" aria-hidden="true"></i> Reset & Start Over</button></p>
							<p class="text-center"><a class="btn btn-primary btn-lg btn-block d-none" href="/quote/"><i class="fa fa-file-text"></i> Get a detailed quote</a></p>
						</form>
					</div>
			</div>
		</div>

	</section>
</div>
<div class="clearfix"></div>
<aside class="container margin-top-4rem margin-bottom-4rem">
	<?php $args = array( 'post_type' => 'ecovideo', 'cat' => '202', 'posts_per_page' => '3', 'orderby' => 'ID', 'order'  => 'DESC' ); ?>
	<?php $loop = new WP_Query( $args ); ?>
	<?php $number = $loop->post_count; ?>
	<?php $counter = 0; ?>
	<h2 class="text-center">Cost Understanding Guide</h2>
	<p class="text-center margin-bottom-2rem">Get more facts and recommendations from our <strong><a href="/videos/">educatonal videos</a></strong></p>
	<div class="row">
		<?php while ( $loop->have_posts() ) : $loop->the_post(); ?>
			<?php $youtube_video_id = get_field('youtube_video_id'); ?>
			<?php $video_duration = get_field('video_duration'); ?>
			<?php $next_steps = get_field_object('next_steps'); ?>
			<?php $caption = ''; ?>

			<?php if($next_steps){ ?>
				<?php $caption = '<div class=&quot;btn-group pull-right&quot;>' ?>
				<?php  foreach($next_steps['value'] as $next_step) { ?>
					<?php $caption .= '<a href=&quot;'.$next_step.'&quot; class=&quot;btn btn-secondary btn-sm&quot;>'.$next_steps['choices'][ $next_step ].'</a>'; ?>
				<?php } ?>
				<?php $caption .= '</div>' ?>
			<?php } ?>
			<div class="col-xs-12 col-md-4">
				<div class="gallery">
					<div class="gallery-image">
						<img src="https://img.youtube.com/vi/<?php echo $youtube_video_id; ?>/maxresdefault.jpg" class="img-fluid img-thumbnail rounded" height="1280" width="720">
						<a data-fancybox data-caption="<?php the_title(); ?> <?php echo $caption; ?>" href="https://www.youtube.com/watch?v=<?php echo $youtube_video_id; ?>&amp;autoplay=1&amp;rel=0&amp;controls=0&amp;showinfo=0"><div class="gallery-text"><i class="fa fa-youtube-play" aria-hidden="true"></i></div></a>
						<span class="gallery-time"><?php echo $video_duration; ?></span>
					</div>
					<h3><a data-fancybox data-caption="<?php the_title(); ?> <?php echo $caption; ?>" href="https://www.youtube.com/watch?v=<?php echo $youtube_video_id; ?>&amp;autoplay=1&amp;rel=0&amp;controls=0&amp;showinfo=0"><?php the_title(); ?></a></h3>
					</div>
			</div>
		<?php endwhile; ?>
	</div>
	<?php wp_reset_postdata(); ?>
</aside>

<?php $related_query = new WP_Query( array( 'cat' => '202', 'posts_per_page' => '6' ) ) ;?>

<?php if ( $related_query->have_posts() ) { ?>
	<aside class="container margin-bottom-4rem">
		<h3 class="text-center margin-bottom-2rem">More recommendations and facts replacement windows cost from <a href="https://www.ecolinewindows.ca/blog/">our blog</h3>
		<div class="row margin-bottom-4rem">
			<?php while ( $related_query->have_posts() ) { ?>
			<?php $related_query->the_post(); ?>
			<div class="col-sm-4">
				<article class="row margin-bottom-2rem">
					<div class="col-5 col-sm-4">
						<a href="<?php echo get_permalink(); ?>"><img class="img-fluid rounded" src="<?php the_post_thumbnail_url(); ?>" alt="<?php the_title(); ?>"></a>
					</div>
					<div class="col-7 col-sm-8">
						<h5 class="mt-0"><a href="<?php echo get_permalink(); ?>" class="text-grey"><?php the_title(); ?></a></h5>
					</div>
				</article>
			</div>
			<?php } ?>
		</div>
	</aside>
	<?php wp_reset_postdata(); ?>
<?php }  ?>

<?php get_sidebar( 'contacts' ); ?>

<?php get_footer(); ?>

