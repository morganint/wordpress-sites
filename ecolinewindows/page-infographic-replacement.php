<?php
/**
 * Template for displaying pages
 * Template Name: Infographic - Replacement
 * @package ecolinewindows
 */
?> 
<?php get_header(); ?>
<div class="blue-holder">
    <div class="w101-faq d-none d-lg-block">
    		<h1 class="hidden-title">WINDOWS 101 HOW TO PURCHASE NEW WINDOWS AN INFOGRAPHIC</h1>
			<div class="w101-text txt1">
				<h2>THE DECISION</h2>
				<p>so, you’ve decided to replace your old windows!</p>
			</div>
    		<div class="w101-text txt2">
				<h2>RESEARCH</h2>
				<p>you look for window companies and set up consultations. Remember, it’s not about how many quotes you get, but educating yourself on the product.</p>
			</div>
    		<div class="w101-text txt3">
				<h2>QUOTE & CONSULTATION</h2>
				<p>a project consultant visits you at your house and answers any questions you have. At this point you should get an accurate quote.</p>
			</div>
    		<div class="w101-text txt4">
				<h2>THE CONTRACT</h2>
				<p>remember to discuss deadlines, deliverables, and any other questions you can come up with. This is also typically when a company will collect the downpayment.</p>
			</div>
    		<div class="w101-text txt5">
				<h2>THE MEASUREMENT</h2>
				<p>your contract is submitted for processing. Within a day an installer is assigned to your project, and a precise measurement is scheduled for your window replacement.</p>
			</div>
    		<div class="w101-text txt6">
				<h2>THE ORDER</h2>
				<p>the measurement is submitted to the order desk, where it is verified and processed for manufacturing.</p>
			</div>
    		<div class="w101-text txt7">
				<h2>MANUFACTURING</h2>
				<p>The factory receives the order form and begins manufacturing your window order.</p>
			</div>
    		<div class="w101-text txt8">
				<h2>SHIPPING</h2>
				<p>after your windows are manufactured they are shipped to a local warehouse facility.</p>
			</div>
    		<div class="w101-text txt9">
				<h2>SCHEDULING</h2>
				<p>as soon as the windows arrive in the warehouse an installation time is scheduled at a convenient date for you.</p>
			</div>
    		<div class="w101-text txt10">
				<h2>INSTALLATION</h2>
				<p>the installer delivers and replaces your windows. Our installers will address any installation concerns you have and always clean up after themselves.</p>
			</div>
    		<div class="w101-text txt11">
				<h2>POST INSTALLATION REVIEW</h2>
				<p>At the end of the installation your installer will go over window operation, ensure you are satisfied with the work, and collect the remaining balance.</p>
			</div>
    		<div class="w101-text txt12">
				<h2>FOLLOW UP</h2>
				<p>within a week of your installation someone from the head office will follow up to make sure all your questions have been answered and receive any feedback on how the company performed</p>
			</div>
    		<div class="w101-text txt13">
				<h2>WARRANTY PACKAGE</h2>
				<p>within a few weeks after the installation you will receive your warranty package that outlines all the terms and conditions of your warranty.</p>
			</div>
    		<div class="w101-text txt14">
				<h2>SERVICE</h2>
				<p>if you require any additional service, or if you think something has gone wrong with your windows, a customer service technician is always available to address any needs or question you have.</p>
			</div>
    </div>
    <img class="img-fluid d-block d-xl-none margin-top-2rem mh-auto" alt="" src="/wp-content/uploads/windows-replacement-info.jpg" />
</div>
<?php get_sidebar( 'contacts' ); ?>
<?php get_footer(); ?> 