<?php
/**
 * Template for displaying pages
 * Template Name: Service
 * @package ecolinewindows
 */
?> 
<?php get_header(); ?>

	<section class="container margin-top-4rem">
		<?php while (have_posts()) { ?>
			<?php the_post(); ?>
			<?php the_content(); ?>
		<?php } ?> 
	</section>
	
	<aside class="sidebar-shadowed margin-top-4rem">
		<div class="container">
			<?php $args = array( 'post_type' => 'ecovideo', 'cat' => '50,43,44', 'posts_per_page' => '6' ); ?>
			<?php $loop = new WP_Query( $args ); ?>
			<?php $number = $loop->post_count; ?>
			<?php $counter = 0; ?>
			<header><h2 class="text-center">Educational Videos</h2></header>
				<div class="row">				
					<?php while ( $loop->have_posts() ) : $loop->the_post(); ?>		
						<?php $youtube_video_id = get_field('youtube_video_id'); ?>						
						<?php $video_duration = get_field('video_duration'); ?>						
						<?php $next_steps = get_field_object('next_steps'); ?>
						<?php $caption = ''; ?>
								
							<?php if($next_steps){ ?>							
								<?php $caption = '<div class=&quot;btn-group pull-right&quot;>' ?>
								<?php  foreach($next_steps['value'] as $next_step) { ?>
									<?php $caption .= '<a href=&quot;'.$next_step.'&quot; class=&quot;btn btn-secondary btn-sm&quot;>'.$next_steps['choices'][ $next_step ].'</a>'; ?> 
								<?php } ?>
								<?php $caption .= '</div>' ?>
							<?php } ?>
							<div class="col-xs-12 col-md-4">
								<div class="gallery">
									<div class="gallery-image">
										<img src="https://img.youtube.com/vi/<?php echo $youtube_video_id; ?>/maxresdefault.jpg" class="img-fluid img-thumbnail rounded" height="1280" width="720">						  
										<a data-fancybox data-caption="<?php the_title(); ?> <?php echo $caption; ?>" href="https://www.youtube.com/watch?v=<?php echo $youtube_video_id; ?>&amp;autoplay=1&amp;rel=0&amp;controls=0&amp;showinfo=0"><div class="gallery-text"><i class="fa fa-youtube-play" aria-hidden="true"></i></div></a>
										<span class="gallery-time"><?php echo $video_duration; ?></span>
									</div> 
									<h3><a data-fancybox data-caption="<?php the_title(); ?> <?php echo $caption; ?>" href="https://www.youtube.com/watch?v=<?php echo $youtube_video_id; ?>&amp;autoplay=1&amp;rel=0&amp;controls=0&amp;showinfo=0"><?php the_title(); ?></a></h3>
								</div>	
							</div>
					<?php endwhile; ?>
				</div>
			<?php wp_reset_postdata(); ?>
			<hr />
			<p class="text-center"><a class="btn btn-primary" href="/videos/">View more</a></p>
			</div>
	</aside>
<?php dynamic_sidebar( 'widget-shadowed' ); ?>
<?php get_sidebar( 'contacts' ); ?>
<?php get_footer(); ?> 