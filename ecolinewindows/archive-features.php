<?php 
/**
 * Displaying archive page (Features)
 * 
 * @package ecolinewindows
 */

get_header(); ?> 

<section class="container margin-top-4rem margin-bottom-4rem">
	<?php $features_post = get_post( 9952 ); ?> 
	<?php $title = $features_post->post_title; ?>
	<?php $content = $features_post->post_content; ?>
	<h1 class="text-center"><?php echo $title; ?></h1>
	<div class="table-responsive">
		<?php echo $content; ?>
	</div>
</section>
<?php dynamic_sidebar( 'widget-pricing' ); ?>

<?php get_footer(); ?>  