window.dataLayer = window.dataLayer || [];
function gtag(){dataLayer.push(arguments);}
gtag('js', new Date());

gtag('config', 'UA-5287622-6');

//gtag('config', 'AW-1055319350');

//gtag('event', 'conversion', {'send_to': 'AW-1055319350/CVHVCKKpzsIBELbKm_cD'});

document.addEventListener( 'wpcf7mailsent', function( event ) {
  location = 'https://www.ecolinewindows.ca/thank-you/';
}, false );