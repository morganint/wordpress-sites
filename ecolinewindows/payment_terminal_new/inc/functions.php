<?php

$dev = false;

function do_saas_call( $target, $post_data ) {
    $dev = true;
	$response = array( "res" => false, "msg" => "", "data" => array() );
	$server   = str_replace('www',($dev?'dev3':'apps'),$_SERVER['SERVER_NAME']);
	$apiURL = "https://{$server}/saas/api/{$target}";

    //cv_dump($apiURL);
    //cv_dump($post_data);

	$post_data['token'] = "TMP-token";

    //print_r($apiURL);

	$ch = curl_init();
	curl_setopt( $ch, CURLOPT_URL, $apiURL );
	curl_setopt( $ch, CURLOPT_HEADER, true );
	curl_setopt( $ch, CURLOPT_SSL_VERIFYPEER, 0 );
	curl_setopt( $ch, CURLOPT_SSL_VERIFYHOST, 0 );
	curl_setopt( $ch, CURLOPT_RETURNTRANSFER, true );
	curl_setopt( $ch, CURLOPT_POST, 1 );
	curl_setopt( $ch, CURLOPT_POSTFIELDS, http_build_query( $post_data ) );
    //curl_setopt( $ch, CURLOPT_HTTPHEADER, $headers );
    curl_setopt($ch, CURLOPT_REFERER, "http://{$_SERVER['SERVER_NAME']}{$_SERVER['REQUEST_URI']}");

	$data = curl_exec( $ch );

	if ( curl_errno( $ch ) ) {
		$response['msg'] = "Connection error. Please try again (" . curl_errno( $ch ) . " ".curl_error( $ch ).")";
	} else {
		$headers  = substr( $data, 0, curl_getinfo( $ch, CURLINFO_HEADER_SIZE ) );
		$body     = substr( $data, curl_getinfo( $ch, CURLINFO_HEADER_SIZE ) );
		$httpcode = curl_getinfo( $ch, CURLINFO_HTTP_CODE );

		$response = json_decode( $body ) ? json_decode( $body, 1 ) : $body;
		//cv_dump($httpcode);
		//cv_dump($body);

		if (!empty($response['status']) && $response['status'] ) {
			$response['res']  = true;
			$response['msg']  = $response['message'];
			$response['data'] = $response;
		} else {
			$response['msg'] = $response['message'];
		}

	}
	curl_close( $ch );

	return $response;
}

function cv_dump($var){
	print "<pre>".print_r($var)."</pre>";
}