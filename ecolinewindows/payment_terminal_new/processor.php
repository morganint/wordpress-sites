<?php

if(1==1) {
    // post to saas
    $url = 'https://portal.projero.pro/api/payments-new/empyrean';
    $ch = curl_init();
    curl_setopt($ch,CURLOPT_URL,$url);
    curl_setopt($ch,CURLOPT_POST,1);
    curl_setopt($ch,CURLOPT_POSTFIELDS,$_POST);
    curl_setopt($ch,CURLOPT_RETURNTRANSFER,1);
    curl_setopt($ch,CURLOPT_REFERER,"https://{$_SERVER['SERVER_NAME']}{$_SERVER['REQUEST_URI']}");
    $response = curl_exec($ch);
    curl_close($ch);

    echo $response;
} else {
    // post directly to Empyrean
    $vars = $_POST;

    $testMode = false;
    if($testMode) {
        $username = "demo";
        $password = "password";
        $processor_id = false;
    } else {
        $username = $vars['uID'];
        $password = "d64af8a9b787ab84";
        $processor_id = $vars['pID'];
    }

    $query = "";
    $query .= "username=".urlencode($username)."&";
    $query .= "password=".urlencode($password)."&";
    $query .= "ccnumber=".urlencode($vars['cardNumber'])."&";
    $query .= "ccexp=".urlencode($vars['cardExpiry'])."&";
    $query .= "amount=".urlencode(number_format($vars['amount'], 2, ".", ""))."&";
    $query .= "cvv=".urlencode($vars['cardCVV'])."&";
    $query .= "orderid=".urlencode($vars['orderNumber'])."&";
    $query .= "orderdescription=".urlencode($vars['description'])."&";
    if($processor_id) {
        $query .= "processor_id=".urlencode($processor_id)."&";
    }
    $query .= "type=sale";

    // Responses:
    // response_code = 460 | Service Not Allowed

    $resultPay = new stdClass();

    $url = 'https://empyrean.transactiongateway.com/api/transact.php';
    $ch = curl_init();
    curl_setopt($ch,CURLOPT_URL,$url);
    curl_setopt($ch,CURLOPT_POST,1);
    curl_setopt($ch,CURLOPT_POSTFIELDS,$query);
    curl_setopt($ch,CURLOPT_RETURNTRANSFER,1);
    $response = curl_exec($ch);
    // response from Empyrean is a string like:
    // response=1&responsetext=SUCCESS&authcode=123456&transactionid=4913014745&avsresponse=&cvvresponse=N&orderid=&type=sale&response_code=100
    curl_close($ch);

    $responseArr = array();
    foreach(explode('&', $response) as $chunk) {
        $param = explode("=", $chunk);
        $responseArr[$param[0]] = $param[1];
    }
    $xml = (object)$responseArr;

    if($xml) {
        $resultPay->xml = $xml;
        $resultPay->response = $response;
        if($xml->response == 1) {
            // Payment successful
            $data->data = $xml;
            $data->status = 1;
            $data->message = "";
            echo json_encode($data);
        } else {
            $data->data = $xml;
            $data->status = 2;
            $data->message = $xml->responsetext;
            echo json_encode($data);
        }
    }
}