// SETTINGS
var empyreanResultPaneName = 'empyreanResults';
var url = '/wp-content/themes/ecolinewindows/payment_terminal_new/processor.php';
var language = 'en';
var formId = 'empyreanForm';

function empyreanProcess(){
    empyreanSetLanguage();
    if(empyreanRequiredFields() == true){
        if(empyreanCheckBrowserSupport() == false){
            document.getElementById(empyreanResultPaneName).innerHTML = empyreanTranslate('YOUR BROWSER IS NOT SUPPORTED');
        }else{
            empyreanToggleButton();

            // REQUIRED FIELDS
            var token = null;
            var test = false;

            // OPTIONAL - CARD INFORMATION
            var cardNumber = null;
            var cardExpiry = null;
            var cardCVV = null;
            var cardType = null;
            var cardToken = null;	// THIS IS ONLY USED TO UPDATE CARD INFORMATION

            // OPTIONAL - ORDER
            var amount = '0.00';

            // SAAS FIELDS
            var oid = null;
            var aid = null;
            var description = null;
            var company_customersID = null;
            var companiesID = null;
            var dqID = null;
            var pID = null;
            var uID = null;
            var dq_down_paymentsID = null;
            var generate_contract = null;
            var uploadType = null;

            // SET
            var dataPOST = '';

            // CHECK FOR TEST MODE
            if(document.getElementById('test') != null){ test = document.getElementById('test').value; }

            // GET VALUES - REQUIRED
            if(document.getElementById('token') != null){ token = document.getElementById('token').value; }

            // GET VALUES - CARD INFORMATION
            if(document.getElementById('cardNumber') != null){ cardNumber = document.getElementById('cardNumber').value; }
            if(document.getElementById('cardCVV') != null){ cardCVV = document.getElementById('cardCVV').value; }
            if(document.getElementById('cardType') != null){ cardType = document.getElementById('cardType').value; }
            if(document.getElementById('cardToken') != null){ cardToken = document.getElementById('cardToken').value; }
            if(document.getElementById('cardExpiry') != null) {
                cardExpiry = document.getElementById('cardExpiry').value;
            } else if(document.getElementById('cardExpiryYear') != null && document.getElementById('cardExpiryMonth') != null) {
                // GET MONTH AND YEAR FIELDS
                var month = document.getElementById('cardExpiryMonth').value;
                var year = document.getElementById('cardExpiryYear').value;

                // IF YEAR IS 4 CHARACTERS LONG REMOVE THE FIRST 2 CHARACTERS
                if(year.length == 4){ year = year.substring(2,4); }

                // APPEND MONTH AND YEAR TO EXPIRY DATE
                cardExpiry = month+year;
            }

            // GET VALUES - ORDER
            if(document.getElementById('amount') != null){ amount = document.getElementById('amount').value; }

            // GET VALUES - SAAS FIELDS
            if(document.getElementById('oid') != null){ oid = document.getElementById('oid').value; }
            if(document.getElementById('aid') != null){ aid = document.getElementById('aid').value; }
            if(document.getElementById('description') != null){ description = document.getElementById('description').value; }
            if(document.getElementById('company_customersID') != null){ company_customersID = document.getElementById('company_customersID').value; }
            if(document.getElementById('companiesID') != null){ companiesID = document.getElementById('companiesID').value; }
            if(document.getElementById('dqID') != null){ dqID = document.getElementById('dqID').value; }
            if(document.getElementById('pID') != null){ pID = document.getElementById('pID').value; }
            if(document.getElementById('uID') != null){ uID = document.getElementById('uID').value; }
            if(document.getElementById('dq_down_paymentsID') != null){ dq_down_paymentsID = document.getElementById('dq_down_paymentsID').value; }
            if(document.getElementById('generate_contract') != null){ generate_contract = document.getElementById('generate_contract').value; }
            if(document.getElementById('uploadType') != null){ uploadType = document.getElementById('uploadType').value; }

            // SET ERROR DEFAULT
            var errors = '';

            // CHECK FIELDS
            if(empyreanValidateCardNumber(cardNumber) == false) {
                errors = errors+"("+empyreanTranslate('INVALID CREDIT CARD NUMBER')+") ";
                document.getElementById('cardType').value = 'NA';
            } else {
                // Detect card type
                cardType = detect_card_type(cardNumber);
                if (typeof cardType === undefined) {
                    cardType = "NA";
                }
                document.getElementById('cardType').value = cardType;
            }
            if(empyreanValidateCardExpiry(cardExpiry) == false){ errors = errors+"("+empyreanTranslate('INVALID CREDIT CARD EXPIRY')+") "; }
            if(empyreanValidateCardCVV(cardCVV) == false){ errors = errors+"("+empyreanTranslate('INVALID CREDIT CARD CVV')+") "; }

            // CHECK FOR SSL
            if(empyreanCheckSSL() == false){
                // CHECK ENVIRONEMNT MODE
                if(test == false){
                    // MISSING SSL
                    errors = errors+"("+empyreanTranslate('BROWSER SSL CERTIFICATE MISSING')+") ";
                }
            }

            // CHECK FOR ERRORS
            if(errors == ''){
                document.getElementById(empyreanResultPaneName).innerHTML = empyreanTranslate('CONNECTING')+'...';
                var req = new XMLHttpRequest();
                var dataPOST = '';

                // ADD
                if(test == 1){ dataPOST += '&test=1'; }
                if(cardNumber != null){ dataPOST += '&cardNumber='+cardNumber; }
                if(cardExpiry != null){ dataPOST += '&cardExpiry='+cardExpiry; }
                if(cardCVV != null){ dataPOST += '&cardCVV='+cardCVV; }
                if(cardType != null){ dataPOST += '&cardType='+cardType; }
                if(cardToken != null){ dataPOST += '&cardToken='+cardToken; }
                if(amount != null){ dataPOST += '&amount='+amount; }

                // ADD SAAS FIELDS
                if(oid != null){ dataPOST += '&oid='+oid; }
                if(aid != null){ dataPOST += '&aid='+aid; }
                if(description != null){ dataPOST += '&description='+description; }
                if(company_customersID != null){ dataPOST += '&company_customersID='+company_customersID; }
                if(companiesID != null){ dataPOST += '&companiesID='+companiesID; }
                if(dqID != null){ dataPOST += '&dqID='+dqID; }
                if(pID != null){ dataPOST += '&pID='+pID; }
                if(uID != null){ dataPOST += '&uID='+uID; }
                if(dq_down_paymentsID != null){ dataPOST += '&dq_down_paymentsID='+dq_down_paymentsID; }
                if(generate_contract != null){ dataPOST += '&generate_contract='+generate_contract; }
                if(uploadType != null){ dataPOST += '&uploadType='+uploadType; }

                // CREATE XML REQUEST
                var newRequest = new XMLHttpRequest();

                // OPEN REQUEST
                newRequest.open('POST',url,true);

                // SEND INFO WITH REQUEST
                newRequest.setRequestHeader("Content-type","application/x-www-form-urlencoded");

                // CHANGE STATE
                newRequest.onreadystatechange = function(){
                    // CHECK READY STATE
                    if(this.readyState == 4){
                        // CHECK GOOD STATUS
                        if(this.status == 200){
                            console.log('newRequest',newRequest);
                            // GET JSON
                            var responseJSON = JSON.parse(newRequest.response);
                            console.log('responseJSON',responseJSON);
                            if(responseJSON.status == 1) {
                                // CLEAN SENSITIVE DATA
                                empyreanCleanCardData();

                                //add hidden response inputs
                                document.getElementById(empyreanResultPaneName).innerHTML = '<input type="hidden" name="response" id="response" value="'+responseJSON.xml.response+'">';

                                // CHECK
                                if(document['empyreanForm'] != null){
                                    // SUBMIT FORM
                                    document['empyreanForm'].submit();
                                }
                            } else {
                                document.getElementById(empyreanResultPaneName).innerHTML = responseJSON.message;
                                empyreanToggleButton();
                            }
                        }else{
                            // ENABLE BUTTON
                            empyreanToggleButton();
                            // SHOW ERROR
                            document.getElementById(empyreanResultPaneName).innerHTML = empyreanTranslate('ERROR')+'(STATUS:'+this.status+'): '+empyreanTranslate('COMMUNICATION ERROR');
                        }
                    }
                }
                console.log(dataPOST);
                // SEND NULL
                newRequest.send(dataPOST);
            }else{
                // ENABLE BUTTON
                empyreanToggleButton();
                // SHOW ERROR
                document.getElementById(empyreanResultPaneName).innerHTML = empyreanTranslate('ERROR')+': '+errors;
                console.log(this);
            }
        }
    }
}

function empyreanValidateCardNumber(value){
    // ACCEPT ONLY DIGITS, DASHES OR SPACES
    if(/[^0-9-\s]+/.test(value)){
        return false;
    }
    // REMOVE SPACES AND DASHES
    value = value.replace(/\D/g,"");
    // CHECK CARD LEN (AMEX MUST START WITH 3)
    if(!((value.length == 15 && value.charAt(0) == 3) || value.length == 16 || (value.length == 14 && value.charAt(0) == 3))){
        return false;
    }
    // LUHN ALGORITHM
    var nCheck = 0;
    var nDigit = 0;
    var bEven = false;
    // LOOP
    for(var n = value.length - 1; n >= 0; n--){
        // SET VALUES
        var cDigit = value.charAt(n);
        var nDigit = parseInt(cDigit, 10);
        // CHECK FOR EVEN
        if(bEven){
            // CHECK FOR SOMETHING
            if((nDigit *= 2) > 9){
                // SUBSRACT 9
                nDigit -= 9;
            }
        }
        // INCREASE
        nCheck += nDigit;
        bEven = !bEven;
    }
    // RETURN
    return (nCheck % 10) == 0;
}

function empyreanValidateCardExpiry(value){
    if(value.length == 4){
        if(value.match(/^[0-9]+$/) != null){
            return true;
        }else{
            return false;
        }
    }else{
        return false;
    }
}

function empyreanValidateCardCVV(value){
    if(value.length == 3 || value.length == 4){
        if(value.match(/^[0-9]+$/) != null){
            return true;
        }else{
            return false;
        }
    }else{
        return false;
    }
}

function empyreanToggleButton(){
    if(document.getElementById('buttonProcess') == null){ return; }
    if(document.getElementById('buttonProcess').disabled == true){
        document.getElementById('buttonProcess').value = empyreanTranslate('Pay');
        document.getElementById('buttonProcess').disabled = false;
    }else{
        document.getElementById('buttonProcess').value = empyreanTranslate('Processing')+'...';
        document.getElementById('buttonProcess').disabled = true;
    }
}

function empyreanRequiredFields(){
    var errors = '';
    if(document.getElementById(empyreanResultPaneName) == null){ errors = errors+'('+empyreanResultPaneName+')'; }
    if(document.getElementById('token') == null){ errors = errors+'(token)'; }
    if(document.getElementById('empyreanForm') == null){
        errors = errors+'(empyreanForm)';
    }
    if(errors == ''){
        return true;
    }else{
        window.alert('empyrean REQUIRED FIELDS MISSING: '+errors);
        return false
    }
}

function empyreanCheckBrowserSupport(){
    if('withCredentials' in new XMLHttpRequest()){
        return true;
    }else{
        return false;
    }
}

function empyreanCheckSSL(){
    if(document.location.protocol === 'https:'){
        return true;
    }else{
        return false;
    }
}

function empyreanCleanCardData(){
    if(document.getElementById('cardNumber') != null){ document.getElementById('cardNumber').value = ''; }
    if(document.getElementById('cardExpiry') != null){ document.getElementById('cardExpiry').value = ''; }
    if(document.getElementById('cardExpiryMonth') != null){ document.getElementById('cardExpiryMonth').value = ''; }
    if(document.getElementById('cardExpiryYear') != null){ document.getElementById('cardExpiryYear').value = ''; }
    if(document.getElementById('cardCVV') != null){ document.getElementById('cardCVV').value = ''; }
}

function empyreanSetLanguage(){
    empyreanUpdateFormId();
    var formObject = document.getElementById(formId);
    if(formObject != null){
        var languageObject = formObject.elements['language'];
        if(languageObject != null){
            if(typeof languageObject.value == 'string'){
                var languageValue = languageObject.value;
                languageValue = languageValue.toLowerCase();
                languageValue = languageValue.trim();
                if(languageValue == 'fr'){ language = languageValue; }
                else if(languageValue == 'sp'){ language = languageValue; }
                else if(languageValue == 'en'){ language = languageValue; }
            }
        }
    }
}

function empyreanTranslate(message){
    var messageResult = message;
    var messageLowerCase = message.toLowerCase();
    var messages = [];
    messages['YOUR BROWSER IS NOT SUPPORTED'.toLowerCase()] = {
        'fr':'VOTRE NAVIGATEUR N\'EST PAS COMPATIBLE',
        'sp':'SU NAVEGADOR NO ES COMPATIBLE'
    };
    messages['CONNECTING'.toLowerCase()] = {
        'fr':'CONNEXION',
        'sp':'CONECTANDO'
    };
    messages['ERROR'.toLowerCase()] = {
        'fr':'ERREUR',
        'sp':'ERROR'
    };
    messages['COMMUNICATION ERROR'.toLowerCase()] = {
        'fr':'ERREUR: PROBLГ€ME DE COMMUNICATION',
        'sp':'ERROR DE COMUNICACION'
    };
    messages['INVALID empyrean.JS TOKEN'.toLowerCase()] = {
        'fr':'empyrean.JS JETON NON VALIDE',
        'sp':'TOKEN empyreanJS INVALIDO'
    };
    messages['INVALID CUSTOMER CODE'.toLowerCase()] = {
        'fr':'CODE DU CLIENT NON VALIDE',
        'sp':'CODIGO DE CLIENTE INVALIDO'
    };
    messages['INVALID CREDIT CARD NUMBER'.toLowerCase()] = {
        'fr':'NUMГ‰RO DE CARTE DE CRГ‰DIT NON VALIDE',
        'sp':'NUMERO DE TARJETA DE CREDITO INVALIDO'
    };
    messages['INVALID CREDIT CARD EXPIRY'.toLowerCase()] = {
        'fr':'DATE DвЂ™EXPIRATION NON VALIDE',
        'sp':'FECHA DE EXPIRACION INVALIDA'
    };
    messages['INVALID CREDIT CARD CVV'.toLowerCase()] = {
        'fr':'VVC NON VALIDE',
        'sp':'NUMERO CVV DE TARJETA DE CREDITO INVALIDO'
    };
    messages['BROWSER SSL CERTIFICATE MISSING'.toLowerCase()] = {
        'fr':'CERTIFICAT DE NAVIGUATEUR SSL MANQUANT',
        'sp':'FALTA CERTIFICADO SSL DE NAVEGADOR'
    };
    messages['Processing, Please Wait'.toLowerCase()] = {
        'fr':'En traitement, Veuillez patienter',
        'sp':'Procesando, Favor de Esperar'
    };
    messages['Pay'.toLowerCase()] = {
        'fr':'Traiter la Transaction',
        'sp':'Procesar Transaccion'
    };
    if(typeof messages[messageLowerCase] != 'undefined'){
        if(typeof messages[messageLowerCase][language] != 'undefined'){
            if(messages[messageLowerCase][language].length > 0){
                message = messages[messageLowerCase][language];
            }
        }
    }
    return message;
}

function empyreanUpdateFormId(){
    if(document.getElementById('token') != null){
        if(document.getElementById('token').form != null && document.getElementById('token').form.tagName == 'FORM'){
            if(typeof document.getElementById('token').form.id == 'string' && document.getElementById('token').form.id.length > 0){
                formId = document.getElementById('token').form.id;
            }
        }
    }
}

function detect_card_type(number) {
    var re = {
        electron: /^(4026|417500|4405|4508|4844|4913|4917)\d+$/,
        maestro: /^(5018|5020|5038|5612|5893|6304|6759|6761|6762|6763|0604|6390)\d+$/,
        dankort: /^(5019)\d+$/,
        interpayment: /^(636)\d+$/,
        unionpay: /^(62|88)\d+$/,
        visa: /^4[0-9]{12}(?:[0-9]{3})?$/,
        mastercard: /^5[1-5][0-9]{14}$/,
        amex: /^3[47][0-9]{13}$/,
        diners: /^3(?:0[0-5]|[68][0-9])[0-9]{11}$/,
        discover: /^6(?:011|5[0-9]{2})[0-9]{12}$/,
        jcb: /^(?:2131|1800|35\d{3})\d{11}$/
    }
    for(var key in re) {
        if(re[key].test(number)) {
            return key
        }
    }
}