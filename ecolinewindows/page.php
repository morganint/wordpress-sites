<?php
/**
 * Template for displaying pages
 * Template Name: Default
 * @package ecolinewindows
 */
?> 
<?php get_header(); ?>

	<section class="container margin-top-4rem">
		<?php while (have_posts()) { ?>
			<?php the_post(); ?>
			<?php the_content(); ?>
		<?php } ?> 
	</section>
	
<?php dynamic_sidebar( 'widget-shadowed' ); ?>
<?php get_sidebar( 'contacts' ); ?>
<?php get_footer(); ?> 