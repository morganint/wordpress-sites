<?php
$hide_block_text_image = get_field('hide_block_text_image', 'option');
$content_text_image = get_field('content_text_image', 'option');
$img_text_image = get_field('img_text_image', 'option');


if(empty($hide_block_text_image)) {
?>
<div class="ew-block-text-image py-5">
  <div class="container">
    <div class="row">
      <div class="col-lg-6 d-flex justify-content-center align-items-center flex-column text-center order-2 order-lg-1">
        <h4 class="ew-block-text-image__title">
          <?php echo $content_text_image['title_text_image'] ?>
        </h4>
        <p class="ew-block-text-image__text">
          <?php echo $content_text_image['text_text_image'] ?>
        </p>
        <a class="ew-block-text-image__link btn" href="/contact-us/">
          Book a Virtual Window and Door Quote
        </a>
      </div>
      <div class="col-lg-6 d-flex justify-content-center align-items-center order-1 order-lg-2 pb-lg-0 pb-4">
        <img class="rounded mw-100" src="<?php echo esc_url($img_text_image['sizes']['medium_large']) ?>"
             alt="">
      </div>
    </div>
  </div>
</div>
<?php } ?>