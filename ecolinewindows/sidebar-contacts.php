		<section class="sidebar sidebar-contact text-center text-sm-left" id="contacts">
            <div class="container-fluid">
                <div class="row justify-content-center">
                    <div class="col-sm-6 col-lg-7 col-xl-2 py-5 py-xl-0 col-cta">
						<?php dynamic_sidebar( 'footer-cta' ); ?>					
					</div>
                    <div class="col-lg-10 col-xl-8 py-5 py-xl-0 mb-xl-5 row-locations-wrapper">
						<div class="row row-locations">
							<?php dynamic_sidebar( 'locations' ); ?>						
						</div>
					</div>
				</div>
			</div>
		</section>