<?php 
/**
 * Displaying archive page (category, tag, archives post, author's post)
 * 
 * @package ecolinewindows
 */

get_header(); ?> 

<section class="container margin-top-4rem margin-bottom-4rem">
	<?php $query = new WP_Query( array( 'post_type' => 'location', 'post_parent' => 0 ) ); ?> 
	<?php if ( $query->have_posts() ) : ?>
	<?php $number = $query->post_count; ?>
			<?php while ( $query->have_posts() ) : $query->the_post(); ?> 
				<?php $counter++; ?>
				<?php if ((($counter - 1) % 2 == 0) or ($counter == 1)) {echo '<div class="row">';} ?>
					<article class="col-sm-6 mb-5">
						<h3><a href="<?php the_permalink(); ?>"><?php the_title(); ?></a></h3>
						<p class="text-24px mb-1 pb-0"><strong><a href="tel:<?php the_field('phone'); ?>" class="contact-phone-link"><?php the_field('phone'); ?></a></strong></p>
						<p><?php the_field('street_address'); ?><br /> <?php the_field('city'); ?>, <?php the_field('provice'); ?> Canada <?php the_field('postal'); ?></p>
						<p><a href="<?php the_permalink(); ?>" class="btn btn-primary mb-3">Location Details</a><br />
						<a class="btn btn-link btn-sm" href="/request-a-call-back/"><i class="fa fa-phone"></i>Request a Call Back</a><br />
						<a class="btn btn-link btn-sm" href="/quote/"><i class="fa fa-file-text"></i>Get a Free Quote</a><br />
						<a class="btn btn-link btn-sm" href="/contact-us/"><i class="fa fa-envelope"></i>Send Us an Email</a></p>
					</article>
				<?php if (($counter % 2 == 0) or ($counter == $number)) {echo '</div>';} ?>
			<?php endwhile; wp_reset_postdata(); ?>
		<!-- show pagination here -->
		<?php else : ?>
			<!-- show 404 error here -->
		<?php endif; ?> 
</section>


<?php get_footer(); ?>  