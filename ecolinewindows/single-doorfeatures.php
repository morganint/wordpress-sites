<?php
/**
 * Template for displaying door features page.
 * 
 * @package ecolinewindows
 */
?>
<?php get_header(); ?> 
<?php $categories = get_the_category(); ?> 

<div class="container content-overlapper margin-bottom-4rem" style="z-index: 999">
	<section>
		<?php if (have_posts()) : while (have_posts()) : the_post(); ?>
			<?php the_content(); ?>
		<?php endwhile; endif; ?>
	</section>
</div>
	
<?php $currentID = get_the_ID(); ?>
<?php $otherfeatures = new WP_Query( array( 'post_type' => 'doorfeatures', 'posts_per_page' => '6', 'post__not_in' => array($currentID) ) ) ;?>

<?php if ( $otherfeatures->have_posts() ) { ?>
<div class="sidebar sidebar-overlayed sidebar-overlayed-green d-none d-md-inline-block mb-5"  style="background-image: url(/wp-content/uploads/Decorative-Glass.jpg);">
	<aside class="container-fluid margin-top-4rem margin-bottom-4rem">
		<header class="d-none"><h2 class="text-center text-white margin-bottom-30px">Other Door Features & Options</h2></header>
		<div class="row justify-content-center">
			<div class="col-10">
				<div class="row justify-content-center">
					<?php while ( $otherfeatures->have_posts() ) { ?>
					<?php $otherfeatures->the_post(); ?>
					<div class="col-6 col-sm-4 col-lg text-center">				
						<a href="<?php echo get_permalink(); ?>" class="text-white margin-bottom-30px"><div style="background-image: url(<?php the_post_thumbnail_url(); ?>); background-size: cover; background-repeat: no-repeat; background-position: center center; width: 120px; height: 120px; display: inline-block;" class="rounded-circle"></div></a>
						<h5 class="text-secondary"><strong><a class="text-white" href="<?php echo get_permalink(); ?>"><?php the_title(); ?></a></strong></h5>				
					</div>
					<?php } ?>
				</div>
			</div>
		</div>
	</aside>
</div>
<?php wp_reset_postdata(); ?>
<?php }  ?>

<?php $related_query = new WP_Query( array( 'category_name' => $categories[0]->name . ',Customer Guide', 'posts_per_page' => '6' ) ) ;?>

<?php if ( $related_query->have_posts() ) { ?>
	<aside class="container">
		<h3 class="text-center margin-bottom-2rem">More recommendations and facts <span class="clearfix d-none d-lg-block"></span>about <?php the_title(); ?> from <a href="https://www.ecolinewindows.ca/blog/">our blog</h3>
		<div class="row margin-bottom-4rem">
			<?php while ( $related_query->have_posts() ) { ?>
			<?php $related_query->the_post(); ?>
			<div class="col-sm-4">
				<article class="row margin-bottom-2rem">
					<div class="col-5 col-sm-12 col-lg-4">
						<a href="<?php echo get_permalink(); ?>"><img class="img-fluid rounded" src="<?php the_post_thumbnail_url(); ?>" alt="<?php the_title(); ?>"></a>
					</div>
					<div class="col-7 col-sm-12 col-lg-8">
						<h5 class="mt-0"><a href="<?php echo get_permalink(); ?>" class="text-grey"><?php the_title(); ?></a></h5>
					</div>
				</article>
			</div>
			<?php } ?>
		</div>
	</aside>
	<?php wp_reset_postdata(); ?>
<?php }  ?>

<?php get_sidebar( 'contacts' ); ?>
<?php get_footer(); ?> 
