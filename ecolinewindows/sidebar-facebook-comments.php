<?php $args = array( 'post_type' => 'eco_fb_comment', 'posts_per_page' => 9 ); ?>
<?php $loop = new WP_Query( $args ); ?>
<?php $number = $loop->post_count; ?>
<?php $counter = 0; ?>

	<section class="sidebar sidebar-fb-comments d-none d-sm-block" id="fbcomments-wrapper">
		<div class="container">
			<div id="commentCarousel" class="carousel slide" data-ride="carousel">
				<header><h2 class="text-center margin-top-60px margin-bottom-30px text-24px">Recent Comments from Our Customers</h2></header>
				<div class="carousel-inner row no-gutters" role="listbox">				
					
					<?php while ( $loop->have_posts() ) : $loop->the_post(); ?>		
						<?php $status = ''; ?>
						<?php $counter++; ?>
						
						<?php $fb_comment_author_url = get_post_custom_values( 'fb_comment_author_url' );?>
						<?php foreach ( $fb_comment_author_url as $key => $value ) {?>
						<?php $author_url = $value; ?>
						<?php }	?>
						
						<?php $fb_comment_date = get_post_custom_values( 'fb_comment_date' );?>
						<?php foreach ( $fb_comment_date as $key => $value ) {?>
						<?php $d1 = strtotime($value); ?>
						<?php }	?>

						<?php $d2 = strtotime(date("Y-m-d")); ?>
						<?php $min_date = min($d1, $d2); ?>
						<?php $max_date = max($d1, $d2); ?>
						<?php $months_count = 0; ?>
						<?php while (($min_date = strtotime("+1 MONTH", $min_date)) <= $max_date) {	?>
							<?php $months_count++; ?>
						<?php }	?>
						
						<?php if($months_count > 1) { ?>
							<?php $months_count = $months_count.' months'; ?>
						<?php } else { ?>
							<?php $months_count = $months_count.' month'; ?>
						<?php } ?>
						
						
						<?php if ($counter == 1) { $status = 'active';} ?>
						<?php if ((($counter - 1) % 3 == 0) or ($counter == 1)) {echo '<div class="carousel-item '.$status.'"><div class="carousel-caption d-none d-md-block"><div class="row">';} ?>
							<div class="col-12 col-md-4">							
								<div class="comment-facebook">	
									<div class="clearfix comment-intro d-none d-md-block">
										Commented on <a href="https://www.facebook.com/ecolinewindows/" target="_blank">Ecoline Windows</a>'s public post 
										<a href="https://www.facebook.com/ecolinewindows/" target="_blank" class="pull-right"><i class="fa fa-facebook-official" aria-hidden="true"></i></a>
									</div>
									<div class="media">								
										<a href="<?php echo $author_url?>" rel="nofollow" target="_blank"><img class="d-flex mr-3 " src="<?php the_post_thumbnail_url( 'square-thumbnail' ); ?>" width="100" height="100" alt="<?php the_title(); ?>"></a>								
										<div class="media-body">
											<a href="<?php echo $author_url?>" rel="nofollow" target="_blank"><?php the_title(); ?></a><?php the_excerpt(); ?>
											<div class="comment-meta"><span class="likes"><i class="fa fa-thumbs-o-up" aria-hidden="true"></i> <?php echo(rand(1,2)); ?></span> about <?php echo $months_count; ?>  ago</div>
										</div>
									</div>					
								</div>		
							</div>		
						<?php if (($counter % 3 == 0) or ($counter == $number)) {echo '</div></div></div>';} ?>

					<?php endwhile; ?>
				</div>
			  <a class="carousel-control-prev" href="#commentCarousel" role="button" data-slide="prev">
				<i class="fa fa-angle-left" aria-hidden="true"></i>
				<span class="sr-only">Previous</span>
			  </a>
			  <a class="carousel-control-next" href="#commentCarousel" role="button" data-slide="next">
				<i class="fa fa-angle-right" aria-hidden="true"></i>
				<span class="sr-only">Next</span>
			  </a>
			</div>
			
		</div>
	</section>
	
<?php wp_reset_postdata(); ?>