<section id="map-container" class="d-none d-lg-flex"><div id="map" style="width: 100%; height: 480px"></div></section>
<?php
wp_enqueue_script('ecoline_sales_map_data',get_template_directory_uri().'/ecoline_sales_map/data.php?province=all&city='.$city,[],null,true);
wp_enqueue_script('ecoline_sales_map_google_maps','https://maps.googleapis.com/maps/api/js?key=AIzaSyCAwuhAr7Wnx8zl770XISEdwtdJ2YonbDw',[],null,true);
wp_enqueue_script('ecoline_sales_map_add_markers',get_template_directory_uri().'/ecoline_sales_map/add_markers.php?zoom='.$zoom.'&center='.$center,['ecoline_sales_map_data','ecoline_sales_map_google_maps'],null,true);
?>
