<?php
include_once "../../../../wp-load.php";
$data=array();

$province = $_GET['province'];
$city = $_GET['city'];
$show_child = $_GET['show_child'];

$sql = "SELECT sid, lat, lng FROM addresses WHERE 1";

$sql .= $province=="all"?"":" AND  province='".addslashes($province)."'";
$sql .= $city=="all"?"":" AND city='".addslashes($city)."'";
//print $sql;

$res = $wpdb->get_results($sql,ARRAY_A);
if($wpdb->num_rows){
	foreach ($res as $row){
		if(empty($row['lat']) ||empty($row['lng']))
			continue;

		$data[]=array(
			'lat'=>$row['lat'],
			'lng'=>$row['lng'],
			//'address'=>$row['address'],
			'sid'=>$row['sid']
		);
	}
}

if($show_child){
	$args = array(
		"post_type"=>'location',
		'posts_per_page' => -1,
		'meta_key'=>'city',
		'meta_value'=>$city
	);
	$my_query = new WP_Query( $args );
	if($my_query->found_posts){
		foreach ($my_query->get_posts() as $post){
			$ID = $post->ID;break;
		}
		$locations = get_pages( array( 'child_of' => $ID, 'post_type' => 'location', 'meta_key' => 'location_page_type', 'meta_value' => 'location',  'sort_column' => 'meta_value', 'sort_order' => 'desc' ) );
		foreach ( $locations as $p ) {
			$city = get_field('field_5ab72343ed1da',$p->ID);
			//print_r($city."<br>");

			$sql = "SELECT sid, lat, lng FROM addresses WHERE city='".addslashes($city)."'";

//print $sql;

			$res = $wpdb->get_results($sql,ARRAY_A);
			if($wpdb->num_rows){
				foreach ($res as $row){
					if(empty($row['lat']) ||empty($row['lng']))
						continue;

					$data[]=array(
						'lat'=>$row['lat'],
						'lng'=>$row['lng'],
						//'address'=>$row['address'],
						'sid'=>$row['sid']
					);
				}
			}
		}

	}

}

echo "var addresses = ".json_encode($data);