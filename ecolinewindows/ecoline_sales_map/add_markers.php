<?php
include_once "../../../../wp-load.php";

$script = "function initialize() {

        var map = new google.maps.Map(document.getElementById('map'), {
            ".(!empty($_GET['zoom'])?'zoom:'.$_GET['zoom'].',':'')."
            maxZoom:14,
            mapTypeId: google.maps.MapTypeId.ROADMAP
			".(!empty($_GET['center'])?",center:{ {$_GET['center']} }":"")."
        });
        var latlngbounds = new google.maps.LatLngBounds();
        var markers = [];
        for (var i = 0; i < addresses.length; i++) {

            latlngbounds.extend(new google.maps.LatLng(addresses[i]['lat'], addresses[i]['lng']));
            var latLng = new google.maps.LatLng(addresses[i].lat,
                addresses[i].lng);
            var marker = new google.maps.Marker({
                position: latLng,
                title: 'address: '+addresses[i].address,
                icon:'".get_template_directory_uri()."/ecoline_sales_map/images/m1.png',
                label:{text: '1', color: \"white\"}
            });
            var mData = \"Order #\"+addresses[i].sid+\" Address:\"+addresses[i].address;
            (function (marker, mData) {
                marker.addListener('click', function () {
                    console.log(mData);
                });
            })(marker, mData);

            markers.push(marker);

        }
        markers.push(marker);

		".(!empty($center)?"":" map.fitBounds(latlngbounds);")."
        var options = {
            imagePath: '".get_template_directory_uri()."/ecoline_sales_map/images/m',
            styles: [{
                url: '".get_template_directory_uri()."/ecoline_sales_map/images/m1.png',
                height: 52,
                width: 52,
                anchor: [0, 0],
                textColor: '#ffffff',
                textSize: 10
            }, {
                url: '".get_template_directory_uri()."/ecoline_sales_map/images/m2.png',
                height: 56,
                width: 56,
                anchor: [0, 0],
                textColor: '#ffffff',
                textSize: 11
            }, {
                url: '".get_template_directory_uri()."/ecoline_sales_map/images/m3.png',
                height: 66,
                width: 66,
                anchor: [0, 0],
                textColor: '#ffffff',
                textSize: 13
            }, {
                url: '".get_template_directory_uri()."/ecoline_sales_map/images/m4.png',
                height: 78,
                width: 78,
                anchor: [0, 0],
                textColor: '#ffffff',
                textSize: 16
            }, {
                url: '".get_template_directory_uri()."/ecoline_sales_map/images/m5.png',
                height: 90,
                width: 90,
                anchor: [0, 0],
                textColor: '#ffffff',
                textSize: 28
            }]
        };

        var markerCluster = new MarkerClusterer(map, markers, options);
    }

    google.maps.event.addDomListener(window, 'load', initialize);";

$script = preg_replace('/<!--(?!s*(?:[if [^]]+]|!|>))(?:(?!-->).)*-->/s', '', $script);
$script = str_replace(array("\r\n", "\r", "\n", "\t"), '', $script);
while ( stristr($script, '  '))
    $script = str_replace('  ', ' ', $script);

echo $script;