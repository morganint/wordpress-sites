<?php

add_shortcode( "sales_map", "sales_map_processing" );

function sales_map_processing( $args ) {
	global $wpdb;
	ob_start();
	//print_r($args);
	$province = !empty( $args['province'] ) ? $args['province'] : 'all';
	$city     = !empty( $args['city'] ) ? $args['city'] : 'all';
	$zoom     = !empty( $args['zoom'] ) ? $args['zoom'] : 9;

	$only_city = !empty( $args['only_city'] ) ? $args['only_city'] : 0;

	if($only_city){
		$center = $city == 'all' ? "" : getCoordinates( $city );

		//print_r($city);
		include "map_city.php";
	}else {
		if ( $province == 'all' && $city == 'all' ) {
			include "check_ip.php";

			$sql = "SELECT * FROM addresses WHERE 1";

			$sql .= $province == "all" ? "" : " AND  province='" . addslashes( $province ) . "'";
			$sql .= $city == "all" ? "" : " AND city='" . addslashes( $city ) . "'";
			//print $sql;
			//print "$province $city<br>";
			$res = $wpdb->get_results( $sql, ARRAY_A );
			if ( $wpdb->num_rows == 0 ) {
				$sql  = "SELECT * FROM addresses WHERE 1";
				$city = "all";
				$sql  .= $province == "all" ? "" : " AND  province='" . addslashes( $province ) . "'";
				//print $sql;
				$res = $wpdb->get_results( $sql, ARRAY_A );
				if ( $wpdb->num_rows == 0 ) {
					return;
				}
			}


		}

		$center = $city == 'all' ? "" : getCoordinates( $city );

		//print_r($city);
		include "map.php";
	}
	$content = ob_get_contents();
	ob_clean();

	return $content;
}

function getCoordinates( $city ) {
	global $wpdb;
	$lat = $lng = '';
	$sql = "SELECT * FROM cities_coordinates WHERE city='{$city}'";
	$res = $wpdb->get_row( $sql, ARRAY_A );
	if ( $res!==null ) {
		$lat = $res['lat'];
		$lng = $res['lng'];
	}else{
		$address = urlencode(  $city.", Canada" ); // replace all the white space with "+" sign to match with google search pattern

		$url = "http://maps.google.com/maps/api/geocode/json??key=AIzaSyCAwuhAr7Wnx8zl770XISEdwtdJ2YonbDw&sensor=false&address=$address";

		$response = file_get_contents( $url );

		$json = json_decode( $response, true ); //generate array object from the response from the web
		$lat = $json['results'][0]['geometry']['location']['lat'];
		$lng = $json['results'][0]['geometry']['location']['lng'];
		if(!empty($lat) && !empty($lng)){
			$sql = "INSERT INTO cities_coordinates SET city='$city',lng='{$lng}',lat='{$lat}'";
			$res = $wpdb->query( $sql );
		}
	}



	return !empty($lat) && !empty($lng)? "lat:$lat, lng:$lng" :"";

}

if ( ! function_exists( 'sales_map_scripts' ) ) {
	/**
	 * Enqueue scripts & styles
	 */
	function sales_map_scripts() {
		global $wp_scripts;
		wp_enqueue_script( 'markerclusterer', get_template_directory_uri() . '/ecoline_sales_map/markerclusterer.js', array(), false, true );
		//wp_enqueue_script('google-maps', 'https://maps.googleapis.com/maps/api/js', array(), false, true);

	}// sales_map_scripts
}
add_action( 'wp_enqueue_scripts', 'sales_map_scripts' );
