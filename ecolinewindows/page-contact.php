<?php
/**
 * Template for displaying pages
 * Template Name: Contact
 * @package ecolinewindows
 */
?> 
<?php get_header(); ?>

	<section class="container">
		<div class="row justify-content-center margin-bottom-4rem">
			<div class="col-sm-7 content-overlapper">		
				<?php while (have_posts()) { ?>
					<?php the_post(); ?>
					<?php the_content(); ?>
				<?php } ?> 			
				<div class="sidebar-contact" id="contacts">
					<div class="row row-locations">
						<?php dynamic_sidebar( 'locations' ); ?>
					</div>
				</div>
			</div>
		</div>
	</section>

<?php get_footer(); ?> 