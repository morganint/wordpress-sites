<?php
/**
 * The template for displaying search results.
 * 
 * @package ecolinewindows
 */

get_header(); ?> 

<section class="container  content-overlapper">
						<?php if (have_posts()) { ?> 
						<header class="page-header">
							<h2 class="page-title text-center"><?php printf(__('Search Results for %s', 'ecolinewindows'), '<strong>' . get_search_query() . '</strong>'); ?></h2>
						</header><!-- .page-header -->
						<hr />
						<?php 
						// start the loop
						while (have_posts()) {
							$counter++; 
							if ((($counter - 1) % 2 == 0) or ($counter == 1)) {echo '<div class="row">';} 
							the_post();
							
							/* Include the Post-Format-specific template for the content.
							* If you want to override this in a child theme, then include a file
							* called content-___.php (where ___ is the Post Format name) and that will be used instead.
							*/
							get_template_part('content', 'search');
							if (($counter % 2 == 0) or ($counter == $number)) {echo '</div>';}
						}// end while
						
						ecolinewindowsPagination();
						?> 
						<?php } else { ?> 
						<?php get_template_part('no-results', 'search'); ?>
						<?php } // endif; ?> 
</section>

<?php get_sidebar( 'contacts' ); ?>
<?php get_footer(); ?>  
