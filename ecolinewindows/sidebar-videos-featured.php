<?php $args = array( 'post_type' => 'ecovideo', 'posts_per_page' => 3, 'category_name' => 'homepage' ); ?>
<?php $loop = new WP_Query( $args ); ?>
<?php $number = $loop->post_count; ?>
<?php $counter = 0; ?>

	<section class="sidebar sidebar-shadowed">
		<header><h2 class="text-center">Worry-Free Windows Replacement & Ownership</h2></header>
		<div class="container">
			<div class="row">	
					
					<?php while ( $loop->have_posts() ) : $loop->the_post(); ?>		

						
						<?php $youtube_video_id = get_field('youtube_video_id'); ?>						
						<?php $video_duration = get_field('video_duration'); ?>						
						<?php $next_steps = get_field_object('next_steps'); ?>
						<?php $caption = ''; ?>
						
						<?php if($next_steps){ ?>
							
							<?php $caption = '<div class=&quot;btn-group pull-right&quot;>' ?>
							<?php  foreach($next_steps['value'] as $next_step) { ?>
								<?php $caption .= '<a href=&quot;'.$next_step.'&quot; class=&quot;btn btn-secondary btn-sm&quot;>'.$next_steps['choices'][ $next_step ].'</a>'; ?> 
							<?php } ?>
							<?php $caption .= '</div>' ?>
						<?php } ?>

						<div class="col-xs-12 col-md-4">
							<div class="gallery">
								<div class="gallery-image">
									<img src="https://img.youtube.com/vi/<?php echo $youtube_video_id; ?>/mqdefault.jpg" class="img-fluid img-thumbnail rounded" height="1280" width="720">						  
									<a data-fancybox data-caption="<?php the_title(); ?> <?php echo $caption; ?>" href="https://www.youtube.com/watch?v=<?php echo $youtube_video_id; ?>&amp;autoplay=1&amp;rel=0&amp;controls=0&amp;showinfo=0"><div class="gallery-text"><i class="fa fa-youtube-play" aria-hidden="true"></i></div></a>
									<span class="gallery-time"><?php echo $video_duration; ?></span>
								</div> 
								<h3><a data-fancybox data-caption="<?php the_title(); ?> <?php echo $caption; ?>" href="https://www.youtube.com/watch?v=<?php echo $youtube_video_id; ?>&amp;autoplay=1&amp;rel=0&amp;controls=0&amp;showinfo=0"><?php the_title(); ?></a></h3>	
								

							</div>
							
						</div>		

					<?php endwhile; ?>
			</div>
		</div>
	</section>
	
<?php wp_reset_postdata(); ?>