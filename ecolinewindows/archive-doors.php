<?php 
/**
 * Displaying archive page (Doors)
 * 
 * @package ecolinewindows
 */

get_header(); ?> 

<section class="margin-top-3rem">

	<header class="page-header margin-bottom-3rem">
		<?php the_archive_title( '<h1 class="page-title text-center p-name">', '</h1>' ); ?>
		<p class="text-center abovelined"><span>Your Guide For Choosing The Right Doors</span></p>
	</header>

	<?php 
		$term = get_queried_object();
		$taxonomy = array( "name" => 'door-types' , "slug" => 'door-types');
		$custom_post_type = "doors";

		if ( have_posts() ):
			the_post();
		endif;
	?>
	<?php /* <pre><?php print_r($term); ?></pre> */?>

<?php $categories = get_terms($taxonomy['name'], 'orderby=id'); ?>
<?php $i = 0; ?>
<?php foreach( $categories as $category ) { ?>

	<?php if (0 == $i % 2) { ?>
		<?php $col1_class = 'order-1'; ?>
		<?php $col2_class = 'order-2'; ?>
	<?php } else { ?>
		<?php $col1_class = 'order-2'; ?>
		<?php $col2_class = 'order-1'; ?>
	<?php } ?>
	<?php $i++; ?>
	
	<?php $glass_styles = get_field('related_glass_styles', 'door-types_' . $category->term_id); ?>
	<?php $background = get_field('background', 'door-types_' . $category->term_id); ?>
	<?php $features = get_field('features', 'door-types_' . $category->term_id); ?>
	
	<div class="category-wrapper entry-content" style="background-image: url(<?php echo $background; ?>);">
		<section class="container-fluid">
			<div class="row">
				<div class="col-md-2 col-lg-4 col-xl-6 <?php echo $col1_class; ?>" style="background-image: url(<?php echo $background; ?>); background-size: cover; background-position: center center; background-repeat: no-repeat;"></div>
				<div class="col-md-10 col-lg-8 col-xl-6 <?php echo $col2_class; ?> category-container bg-white-semi-transparent"> 
					<div class="row">
						<div class="col-md-7 col-lg-8">   
							<h2 class="category-title text-center text-md-left"><a href="/<?php echo $category->taxonomy; ?>/<?php echo $category->slug; ?>/"><?php echo $category->name; ?></a></h2>					
							<?php if($features) { ?>
								<div class="category-features"><?php echo $features; ?></div>
							<?php } else { ?>
								<div class="category-description"><?php echo $category->description; ?></div>
							<?php } ?>
						</div>
						<div class="col-md-5 col-lg-4"> 
								<?php if( $glass_styles ): ?>
									<div class="d-flex flex-row flex-wrap justify-content-center justify-content-lg-start category-glass-previews mb-3">
									<?php foreach( $glass_styles as $glass_style): ?>
										<?php setup_postdata($glass_style); ?>
										<?php $featured_img_url = get_the_post_thumbnail_url($glass_style->ID ,'full');  ?>
										<div class="p-2 pb-1 text-center">
											<a data-fancybox="glassgallery<?php echo $category->slug; ?>" href="<?php echo $featured_img_url; ?>" title="<?php echo $glass_style->post_title; ?> <?php echo $category->name; ?>">
												<?php echo get_the_post_thumbnail( $glass_style->ID, 'medium', array( 'class' => '' ) ); ?>
											</a>
											<div class="clearfix category-glass-preview-label"><?php echo $glass_style->post_title; ?></div>
										</div>
									<?php endforeach; ?>
									</div>
									<?php wp_reset_postdata(); ?>
								<?php endif; ?>				
							
							<?php
								global $post; // Access the global $post object.

								// Setup query to return each custom post within this taxonomy category
								$collection = get_posts(array(
									'nopaging' => true,
									'post_type' => $custom_post_type,
									'taxonomy' => $category->taxonomy,
									'term' => $category->slug,
								));
								$collection_count = count($collection)
							?>

							<div class="list-group series-links">
								<?php foreach($collection as $post) : ?>
									<?php setup_postdata($post); ?>
									<a class="list-group-item" href="/<?php echo $category->taxonomy; ?>/<?php echo $category->slug; ?>/<?php echo '#'.$post->post_name; ?>"><i class="fa fa-angle-right" aria-hidden="true"></i> &nbsp;<?php the_title(); ?></a>
								<?php endforeach;?> 
								<?php wp_reset_postdata(); ?>
								<?php if($collection_count > 1): ?><a class="list-group-item list-group-item-last" href="/<?php echo $category->taxonomy; ?>/<?php echo $category->slug; ?>/"><i class="fa fa-angle-right" aria-hidden="true"></i> &nbsp;View All</a><?php endif; ?>
							</div>
						</div>
					</div>
				</div>
			</div>
		</section>
	</div>	
<?php } ?>
</section>

<?php $currentID = get_the_ID(); ?>
<?php $otherfeatures = new WP_Query( array( 'post_type' => 'doorfeatures', 'posts_per_page' => '6', 'post__not_in' => array($currentID) ) ) ;?>

<?php if ( $otherfeatures->have_posts() ) { ?>
<div class="bg-grey-light padding-top-4rem padding-bottom-3rem d-none d-md-block">
	<aside class="container-fluid margin-top-4rem margin-bottom-4rem">
		<header class="d-none"><h2 class="text-center text-white margin-bottom-30px">Door Features & Options</h2></header>
		<div class="row justify-content-center">
			<div class="col-10">
				<div class="row justify-content-center">
					<?php while ( $otherfeatures->have_posts() ) { ?>
					<?php $otherfeatures->the_post(); ?>
					<div class="col-6 col-sm-4 col-lg text-center">				
						<div class="margin-bottom-1rem"><a href="<?php echo get_permalink(); ?>"><div style="background-image: url(<?php the_post_thumbnail_url(); ?>); background-size: cover; background-repeat: no-repeat; background-position: center center; width: 120px; height: 120px; display: inline-block;" class="rounded-circle"></div></a></div>
						<h4 class="text-1rem font-weight-bold text-secondary"><a href="<?php echo get_permalink(); ?>"><?php the_title(); ?></a></h4>				
					</div>
					<?php } ?>
				</div>
			</div>
		</div>
	</aside>
</div>
<?php wp_reset_postdata(); ?>
<?php }  ?>

<?php $glassoptions = new WP_Query( array( 'post_type' => 'glass-styles', 'posts_per_page' => '12' ) ) ;?>
<?php if ( $glassoptions->have_posts() ) { ?>
	<aside style="position: relative;">	
		<div class="padding-top-3rem" style="position: absolute; width: 100%; height: 100%; background-color: rgba(0,0,0,0.6); z-index: 2;">
			<header class="text-center margin-top-3rem">
				<h2 class="text-white text-3rem margin-bottom-1rem">Decorative Door Glass</h2>
				<p><a class="btn btn-white" href="/decorative-door-glass/"><i class="fa fa-th-list"></i> View all options</a></p>
			</header>
		</div>
		<div class="container-fluid">
			<div class="row justify-content-center">
				<?php while ( $glassoptions->have_posts() ) { ?>
					<?php $glassoptions->the_post(); ?>
					<div class="col-2" style="background-image: url(<?php the_post_thumbnail_url('thumbnail'); ?>); background-size: cover; background-repeat: no-repeat; background-position: center center; height: 160px;"></div>
				<?php } ?>
			</div>
		</div>
	</aside>
<?php wp_reset_postdata(); ?>
<?php }  ?>

<?php get_sidebar( 'contacts' ); ?>
<?php get_footer(); ?>  