<?php 
/**
 * Displaying archive page (Product)
 * 
 * @package ecolinewindows
 */

get_header(); ?> 

<aside class="container margin-bottom-3rem margin-top-2rem bg-primary rounded px-4 pb-3">

	<form name="filters" id="filters-form" class="filters">
		<div class="row justify-content-center">
			<div class="col-6 col-lg-2">
				<div class="form-group">
					<label class="text-white text-uppercase filter-label" for="filter-room">By room:</label>
					<select class="form-control" name="filter-room" data-filter-group="room">
						<option value="">Any</option>
						<option value=".livingroom">Living room</option>
						<option value=".diningroom">Dining room</option>
						<option value=".bedroom">Bedroom</option>
						<option value=".bathroom">Bathroom</option>
						<option value=".basement">Basement</option>
						<option value=".kitchen">Kitchen</option>
					</select>
				</div>
			</div>
			<div class="col-6 col-lg-2">
				<div class="form-group">
					<label class="text-white text-uppercase filter-label" for="filter-priority">By priority:</label>
					<select class="form-control" name="filter-priority" data-filter-group="priority">
						<option value="">Any</option>
						<option value=".energy">Most Efficient</option>
						<option value=".cheapest">Cheapest</option>
					</select>
				</div>
			</div>
			<div class="col-6 col-lg-2">
				<div class="form-group">
					<label class="text-white text-uppercase filter-label" for="filter-type">By type:</label>
					<select class="form-control" name="filter-type" data-filter-group="type">
						<option value="">Any</option>
						<option value=".crank">Crank</option>
						<option value=".sliding">Sliding</option>
					</select>
				</div>
			</div>
			<div class="col-6 col-lg-2">
				<div class="form-group">
					<label class="text-white text-uppercase filter-label" for="filter-security">By security:</label>
					<select class="form-control" name="filter-security" data-filter-group="security">
						<option value="">Any</option>
						<option value=".security">Highest</option>
					</select>
				</div>
			</div>
			<div class="col-6 col-lg-2">	
				<div class="form-group">
					<label class="text-white text-uppercase filter-label" for="filter-egress">By installation:</label>
					<select class="form-control" name="filter-egress" data-filter-group="egress">
						<option value="">Any</option>
						<option value=".egress">Egress</option>
					</select>
				</div>
			</div>
			
			<div class="col-6 col-lg-2">
				<div class="form-group">
					<label class="text-white text-uppercase filter-label" for="filter-size">By size:</label>
					<select class="form-control" name="filter-size" data-filter-group="size">
						<option value="">Any</option>
						<option value=".size">Biggest</option>
					</select>
				</div>
			</div>
		</div>
	</div>

</aside>

<div class="container"><div id="message-empty" class="alert alert-warning text-center">No matches found. Try changing criteria. <button class="btn btn-warning btn-sm clearfix" id="isotope-reset">Reset</button></div></div>

<section class="container margin-bottom-4rem"> 
	<div class="row justify-content-center text-center" id="grid">
		<?php
		// we add this, to show all posts in our 
		// Glossary sorted alphabetically
		$args = array( 'posts_per_page' => -1, 'orderby'=> 'title', 'order' => 'ASC', 'post_type' => 'product' );
		$glossaryposts = get_posts( $args ); 
		// here comes The Loop!
		foreach( $glossaryposts as $post ) :  ?>
			
			<?php setup_postdata($post); ?>

			<?php
				$roomfield = get_field_object('room');
				$rooms = $roomfield['value'];
				
				$featurefield = get_field_object('features');
				$features = $featurefield['value'];
				
				$typefield = get_field_object('window_type');
				$types = $typefield['value'];
				
				$priorityfield = get_field_object('priority');
				$priorities = $priorityfield['value'];
				
				$postID = get_the_ID();
			?>



				<?php if( $rooms ): ?>
				<?php foreach( $rooms as $room ): ?>
					<?php ${'roomsclasses'.$postID} .= ' ' . $room; ?>
				<?php endforeach; ?>
				<?php endif; ?>

				<?php if( $features ): ?>
				<?php foreach( $features as $feature ): ?>
					<?php ${'featureclasses'.$postID} .= ' ' . $feature; ?>
				<?php endforeach; ?>
				<?php endif; ?>
				
				<?php if( $types ): ?>
				<?php foreach( $types as $type ): ?>
					<?php ${'typeclasses'.$postID} .= ' ' . $type; ?>
				<?php endforeach; ?>
				<?php endif; ?>
				
				<?php if( $priorities ): ?>
				<?php foreach( $priorities as $priority ): ?>
					<?php ${'priorityclasses'.$postID} .= ' ' . $priority; ?>
				<?php endforeach; ?>
				<?php endif; ?>

	

			<article id="post-<?php echo $postID; ?>" class="col-6 col-sm-3 h-entry window-item text-center <?php echo ${'roomsclasses'.$postID}; ?> <?php echo ${'featureclasses'.$postID}; ?> <?php echo ${'typeclasses'.$postID}; ?> <?php echo ${'priorityclasses'.$postID}; ?>">
				<?php $image = get_field('preview'); ?>
				<?php if( !empty($image) ): ?>
					<a href="<?php the_permalink(); ?>" rel="bookmark"><img src="<?php echo $image['url']; ?>" class="w-50 mb-3" alt="<?php echo $image['alt']; ?>" /></a>
				<?php endif; ?>
						
				<h2><a class="text-grey mb-3" href="<?php the_permalink(); ?>"><?php the_title(); ?></a></h2>
				
			</article><!-- #post-## -->
			
			<?php wp_reset_postdata(); ?>

		<?php endforeach; ?>
		
	</div>
</section>

<div class="w-100 d-block"></div>

<?php dynamic_sidebar( 'widget-pricing' ); ?>

<?php get_sidebar( 'contacts' ); ?>
<?php get_footer(); ?>  