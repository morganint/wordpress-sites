<?php if(is_single()){ ?>

	<article class="container content-overlapper"> 
		<div id="post-<?php the_ID(); ?>" class="row row-fixedbar">
			<div class="col-sm-8">
				<div class="entry-content margin-bottom-4rem" id="blog-body">
					<?php the_content(ecolinewindowsMoreLinkText()); ?> 
					<div class="clearfix"></div>
				</div>
			</div>
			<div class="col-sm-4">
				<aside id="sidebar-fixed" role="complementary">
					<?php dynamic_sidebar( 'blog' ); ?>
					
					<?php if( get_field('banner_show') ): ?>
					
						<?php 
							$banner_headline = get_field('banner_headline');
							$banner_text = get_field('banner_text');
							$banner_url = get_field('banner_url');
							$banner_button_text = get_field('banner_button_text');

							// image
							$banner_image = get_field('banner_image');
							$banner_image_url = $banner_image['url'];
							$banner_image_title = $banner_image['title'];
							$banner_image_alt = $banner_image['alt'];
							$banner_image_caption = $banner_image['caption'];

							// thumbnail
							$banner_image_size = 'thumbnail';
							$banner_image_thumb = $banner_image['sizes'][ $banner_image_size ];
							$banner_image_width = $banner_image['sizes'][ $banner_image_size . '-width' ];
							$banner_image_height = $banner_image['sizes'][ $banner_image_size . '-height' ];
						?>
						
						<div role="banner" class="sidebar-banner">	
							
							<img src="<?php echo $banner_image; ?>" alt="<?php echo $banner_image_alt; ?>" width="<?php echo $banner_image_width; ?>" height="<?php echo $banner_image_height; ?>" class="img-fluid" />
							
							<div class="sidebar-banner-overlay"></div>
								
							<div class="sidebar-banner-content">
								<h3 class="text-1rem sidebar-banner-header text-uppercase"><strong><?php echo $banner_headline; ?></strong></h3>
								<p class="sidebar-banner-tagline"><?php echo $banner_text; ?></p>
								<a class="btn btn-primary btn-sm sidebar-banner-btn" href="<?php echo $banner_url; ?>"><?php echo $banner_button_text; ?></a>
							</div>
							
						</div>
						
					<?php endif; ?>
		
				</aside>
			</div>
		</div>
	</article>

<?php } else { ?>

	<article id="post-<?php the_ID(); ?>" class="col-md-6 margin-bottom-1rem h-entry">
		<header class="entry-header">
			<a href="<?php the_permalink(); ?>" rel="bookmark"><?php the_post_thumbnail('large', array( 'class' => 'img-fluid rounded' )); ?></a>
			<div class="row justify-content-center">
				<div class="col-10 bg-white rounded margin-top--3rem">
					<h3 class="entry-title text-center margin-top-1rem margin-bottom-2rem p-name"><a href="<?php the_permalink(); ?>" rel="bookmark"><strong><?php the_title(); ?></strong></a></h3>
					<div class="p-summary text-justify"><?php the_excerpt(); ?></div>
					<a href="<?php the_permalink(); ?>" class="btn btn-secondary btn-block" rel="bookmark">Continue reading ...</a>
					<?php if ('post' == get_post_type()) { ?> 
					<div class="entry-meta hidden">
						<?php ecolinewindowsPostOn(); ?> 
					</div><!-- .entry-meta -->
					<?php } //endif; ?> 					
				</div>
			</div>

		</header><!-- .entry-header -->
	</article><!-- #post-## -->

<?php } ?>




