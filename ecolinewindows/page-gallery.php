<?php
/**
 * Template for displaying pages
 * Template Name: Gallery
 * @package ecolinewindows
 */
?>
<?php get_header(); ?>
<!-- Nav tabs -->

<div class="container content-overlapper">

			<div class="row mb-5">
				<?php
					$args = array( 'post_type' => 'ecophoto' );
					$loop = new WP_Query( $args );
					$item = 1;
				?>
				<?php while ( $loop->have_posts() ) : $loop->the_post(); ?>
					<?php
						$itemgal = 'images'.$item;
						$title = get_the_title();
						if ( has_post_thumbnail()) {
							$large_image_url = wp_get_attachment_image_src( get_post_thumbnail_id(), 'full');
							$thumb_image_url = wp_get_attachment_image_src( get_post_thumbnail_id(), 'large');
						}?>

							<div class="col-sm-6 col-lg-3 mb-3">
								<?php $next_steps = get_field_object('window_types'); ?>
								<?php $caption = ''; ?>

								<?php if($next_steps['value']) { ?>
									<?php $caption = '<div class=&quot;pull-right hidden-md-down&quot;> Featuring&nbsp;' ?>
									<?php $sel_count = count($next_steps['value']); ?>
									<?php $count = 1; ?>
									<?php foreach($next_steps['value'] as $next_step) { ?>
										<?php $caption .= '<a href=&quot;'.$next_step.'&quot; class=&quot;&quot;>'.$next_steps['choices'][ $next_step ].'</a>'; ?>
										<?php if($count != $sel_count){ $caption .= ',&nbsp;';}?>
										<?php $count++; ?>
									<?php } ?>
									<?php $caption .= '&nbsp;windows</div>' ?>
								<?php } ?>

								<?php $slideinfo = ecophoto_fields_values("sdesc,simage"); ?>

								<a class="card-link" data-fancybox="<?php echo $itemgal ?>" data-caption="<?php the_title(); ?> <?php echo $caption; ?>" href="<?php echo $large_image_url[0]; ?>">
									<div class="img-container mb-3">
										<img src="<?php echo $thumb_image_url[0]; ?>" class="img-fluid img-thumbnail">
										<span class="btn btn-light btn-sm btn-img-count"><i class="fa fa-picture-o" aria-hidden="true"> </i><span class="badge badge-light"><?php echo count($slideinfo); ?></span></span>
										<div class="img-gallery-overlay">
											<div class="img-gallery-overlay-centerpoint">
											   <i class="fa fa-search text-white text-4rem" aria-hidden="true"></i>
											</div>
										 </div>
									</div>
									<?php the_excerpt(); ?>	
								</a>
								<div class="hidden" style="height: 0; overflow: hidden;">
									<?php foreach ($slideinfo as $values) { ?>
										<?php $slide_image = wp_get_attachment_image_src($values["simage"], "full"); ?>
										<?php $slide_thumb = wp_get_attachment_image_src($values["simage"], "thumbnail"); ?>
										<?php $slide_desc = (string) $values["sdesc"]; ?>
										<a data-fancybox="<?php echo $itemgal ?>" data-caption="<?php echo $slide_desc; ?> <?php echo $caption; ?>" href="<?php echo $slide_image[0]; ?>"><img src="<?php echo $slide_thumb[0]; ?>"></a>
									<?php }	?>
								</div>
							</div>
							<?php $item = $item+1 ?>
					<?php endwhile; // end of the loop. ?>
			</div>
			<?php wp_reset_postdata(); ?>
</div>

<?php get_sidebar( 'contacts' ); ?>
<?php get_footer(); ?>