<?php
/**
 * Template for displaying pages
 * Template Name: Infographic - Service
 * @package ecolinewindows
 */
?> 
<?php get_header(); ?>
<div class="wservice-holder">
    <div class="window-service d-none d-lg-block">
		<h1 class="hidden-title">WINDOW SERVICING IN A NUTSHELL</h1>
		<div class="wserv-text wst1">
			<h2>You’ve discovered a problem</h2>
			<p>Even the best work or product can sometimes have issues. Thankfully your windows are covered by hardware and labour warranties!</p>
		</div>
    	<div class="wserv-text wst2">
			<h2>Call the office to report it</h2>
			<strong>Our toll-free number is</strong>
			<span>1 866 644 6418</span>
		</div>
    	<div class="wserv-text wst3">
			<h2>A service representative will make a report of the issues you’re having</h2>
			<p>We make sure to take note of all calls, even if the problem is resolved in conversation. This way, even if you have issues down the road, we can always see what you contacted us about.</p>
		</div>
    	<div class="wserv-text wst4">
			<h2>What is the problem?</h2>
		</div>
    	<div class="wserv-text wst5">
			<h2>Condensation:</h2>
			<p>Foggy windows? Moisture build-up on glass? It is common for new windows to experience even greater condensation than your old ones – the difference in temperature is greater than before. Over time the windows will adjust and you should see a decrease in condensation.</p>
		</div>
    	<div class="wserv-text wst6">
			<h2>Installation:</h2>
			<p>These problems have to do directly with how your windows are installed. The window doesn’t look level? Excessive or too little caulking? Leaking around the frame? All these issues are a result of installation problems.</p>
		</div>
    	<div class="wserv-text wst7">
			<h2>Product defect: </h2>
			<p>Cracked or scratched glass? Ripped screens? Broken hardware? These problems are usually a production defect.</p>
		</div>
    	<div class="wserv-text wst8">
			<h2>Document the problem</h2>
			<p>Take a few pictures of the windows in question. Keep in mind where in the house the window is located. What part of the window is the problem in? 
The more you can tell a service representative about the issue, the better they can respond and come up with a solution.</p>
		</div>
    	<div class="wserv-text wst9">
			<h2>Send us documentation</h2>
			<p>Send us any and all information about the problems you are experiencing to our e-mail at <a href="mailto:service@ecolinewindows.ca">service@ecolinewindows.ca</a>. All the information you submit is attached to your service file to better address the issue.</p>
		</div>
    	<div class="wserv-text wst10">
			<h2>We find a solution</h2>
			<p>We make sure to involve the installers, supervisors, and  production line as necessary, to find the most suitable solution for you. It’s important to us to not only fix the problem, but understand how it happened to prevent this in the future.</p>
		</div>
    	<div class="wserv-text wst11">
			<h2>Follow Up</h2>
			<p>We get back to you within three business days to let you know how we will fix the issue, and what the next steps in the process are.</p>
		</div>
    	<div class="wserv-text wst12">
			<h2>Condensation:</h2>
			<p>The best way to deal with condensation is to lower the humidity in your home. Our service representatives send out information on understanding condensation, and ways control and prevent it.</p>
		</div>
    	<div class="wserv-text wst13">
			<h2>Installation:</h2>
			<p>In case of an installation issue we contact the supervisor that performed your completion inspection. We examine pictures from your completion and ones you submitted to find the best solution.</p>
		</div>
    	<div class="wserv-text wst14">
			<h2>Product defect: </h2>
			<p>We contact the production line to order replacements for defective parts or windows.</p>
		</div>
    	<div class="wserv-text wst15">
			<h2>We check it out.</h2>
		</div>
    	<div class="wserv-text wst16">
			<h2>Condensation:</h2>
			<p>If condensation is extreme or persistent we send a supervisor to measure humidity levels in the house and determine what can be done to help 
further reduce it.</p>
		</div>
    	<div class="wserv-text wst17">
			<h2>Installation:</h2>
			<p>We determine a solution and send an installer to perform the necessary work.</p>
		</div>
    	<div class="wserv-text wst18">
			<h2>Product defect:</h2>
			<p>We receive the right replacement parts and perform repair work on your windows.</p>
		</div>
    	<div class="wserv-text wst19">
			<h2>Service is scheduled</h2>
			<p>As soon as all the parts are in, and work orders are prepared, our service department representatives contact you to schedule a service appointment at a convenient time.</p>
		</div>
    	<div class="wserv-text wst20">
			<h2>Service is performed</h2>
			<p>We come to you, and make it right!</p>
		</div>
    	<div class="wserv-text wst21">
			<h2>Satisfaction guarantee</h2>
			<p>Every service is accompanied by a completion certificate to ensure the job was done properly and issue has been resolved. </p>
			<p>If any problems persist, or you encounter new issues with your windows, just give us a call at </p>
			<span>1 866 644 6418.</span>
		</div>
    </div>
    <img class="img-fluid d-block d-xl-none margin-top-2rem mh-auto" alt="" src="/wp-content/uploads/images/windows-servicing-mobile3.jpg" />
</div>
<?php get_sidebar( 'contacts' ); ?>
<?php get_footer(); ?> 