<?php
/**
 * Header - Default
 * 
 * @package ecolinewindows
 */
?>
<header class="margin-top-3rem">
	<h1 class="text-center p-name">Find your perfect window</h1>
	<p class="text-center p-summary">Use room and feature filters below to find a window that best suits your needs.</p>
</header>