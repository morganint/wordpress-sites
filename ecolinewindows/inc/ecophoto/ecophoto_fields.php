<?php

class ecophoto_fields {
	public $ns_key;
	public $registered_field_types;

	public function __construct() {
		define( "ECOPHOTO_FIELDS_VERSION", "1.0" );
		define( "ECOPHOTO_FIELDS_URL", get_template_directory_uri() . "/inc/ecophoto/");

		$ns_key = wp_cache_get( 'ecophoto_fields_namespace_key', 'ecophoto_fields' );
		if ( $ns_key === false ) {
			wp_cache_set( 'ecophoto_fields_namespace_key', 1, 'ecophoto_fields' );
		}
		$this->ns_key = wp_cache_get( 'ecophoto_fields_namespace_key', 'ecophoto_fields' );

		require( dirname(__FILE__) . "/functions.php" );

		add_action( 'admin_head', array($this, 'admin_head') );
		add_action( 'admin_enqueue_scripts', array($this, 'admin_enqueue_scripts') );

		add_action( 'dbx_post_sidebar', array($this, 'post_dbx_post_sidebar') );
		add_action( 'save_post', array($this, 'save_postdata') );
		add_action( 'save_post', array($this, 'clear_caches') );
		add_action( 'edit_attachment', array($this, 'save_postdata') );

		add_action( 'wp_ajax_ecophoto_fields_metabox_fieldgroup_add', array($this, 'metabox_fieldgroup_add') );
	}

	function admin_enqueue_scripts($hook) {
		$is_on_ecophoto_fields_page = FALSE;
		$page_type = "";

		$current_screen = get_current_screen();
		if ($current_screen->base == "post" && in_array($current_screen->post_type, $this->get_post_connector_attached_types())) {
			$is_on_ecophoto_fields_page = TRUE;
			$page_type = "post";
		} elseif ($current_screen->base === "media-upload") {
			$is_on_ecophoto_fields_page = TRUE;
			$page_type = "media-upload";
		} elseif ($current_screen->id === "settings_page_ecophoto-fields-options") {
			$is_on_ecophoto_fields_page = TRUE;
			$page_type = "settings";
		}

		if ( ! $is_on_ecophoto_fields_page ) return;

		if ("settings" === $page_type) {
			wp_enqueue_style('ecophoto-fields-styles', ECOPHOTO_FIELDS_URL.'css/styles.css', false, ECOPHOTO_FIELDS_VERSION);

		} else {
			wp_enqueue_script("thickbox");
			wp_enqueue_style("thickbox");

			wp_enqueue_style('ecophoto-fields-styles-post', ECOPHOTO_FIELDS_URL.'css/styles-edit-post.css', false, ECOPHOTO_FIELDS_VERSION);

			wp_enqueue_media(); // edit-form-advanced passes this also: array( 'post' => $post_ID

		}
		wp_enqueue_script("jquery-ui-core");
		wp_enqueue_script("jquery-ui-sortable");
		wp_enqueue_script("jquery-ui-dialog");
		wp_enqueue_script("jquery-effects-highlight");
		wp_register_script('ecophoto-fields-scripts', ECOPHOTO_FIELDS_URL.'js/scripts.js', false, ECOPHOTO_FIELDS_VERSION);
		wp_localize_script('ecophoto-fields-scripts', 'sfstrings', array(
			'page_type' => $page_type,
			'txtDelete' => __('Delete', 'ecophoto-fields'),
			'confirmDelete' => __('Delete this field?', 'ecophoto-fields'),
			'confirmDeleteGroup' => __('Delete this group?', 'ecophoto-fields'),
			'confirmDeleteConnector' => __('Delete this post connector?', 'ecophoto-fields'),
			'confirmDeleteRadio' => __('Delete radio button?', 'ecophoto-fields'),
			'confirmDeleteDropdown' => __('Delete dropdown value?', 'ecophoto-fields'),
			'adding' => __('Adding...', 'ecophoto-fields'),
			'add' => __('Add', 'ecophoto-fields'),
			'confirmRemoveGroupConnector' => __('Remove field group from post connector?', 'ecophoto-fields'),
			'confirmRemoveGroup' => __('Remove this field group?', 'ecophoto-fields'),
			'context' => __('Context', 'ecophoto-fields'),
			'normal' => __('normal'),
			'advanced' => __('advanced'),
			'side' => __('side'),
			'low' => __('low'),
			'high' => __('high'),
		));
		wp_enqueue_script('ecophoto-fields-scripts');
		wp_enqueue_style('wp-jquery-ui-dialog');
		do_action("ecophoto_fields_enqueue_scripts", $this);

	}

	function metabox_fieldgroup_add() {

		global $sf;

		$ecophoto_fields_new_fields_count = (int) $_POST["ecophoto_fields_new_fields_count"];
		$post_id = (int) $_POST["post_id"];
		$field_group_id = (int) $_POST["field_group_id"];

		$num_in_set = "new{$ecophoto_fields_new_fields_count}";
		$this->meta_box_output_one_field_group($field_group_id, $num_in_set, $post_id, true);

		exit;
	}

	public function get_post_connector_attached_types() {
		return array('ecophoto');
	}

	public function admin_head() {
		$current_screen = get_current_screen();
		$is_on_ecophoto_fields_page = FALSE;
		if ($current_screen->base == "post" && in_array($current_screen->post_type, $this->get_post_connector_attached_types())) {
			$is_on_ecophoto_fields_page = TRUE;
			$page_type = "post";
		}

		if (!$is_on_ecophoto_fields_page) return;
		global $post, $ef;
		do_action("ecophoto_fields_admin_head", $this);

		if ($post) {

			$post_type = $post->post_type;
			$arr_post_types = $this->get_post_connector_attached_types();
			if (in_array($post_type, $arr_post_types)) {
				$connector_to_use = $this->get_selected_connector_for_post($post);
				$post_connectors = $this->get_post_connectors();
				if (isset($post_connectors[$connector_to_use])) {

					$field_groups = $this->get_field_groups();
					$selected_post_connector = $post_connectors[$connector_to_use];

					$hide_editor = (bool) isset($selected_post_connector["hide_editor"]) && $selected_post_connector["hide_editor"];
					if ($hide_editor) {
						?><style type="text/css">#postdivrich, #postdiv { display: none; }</style><?php
					}
					$selected_post_connector_field_groups = $selected_post_connector["field_groups"];

					foreach ($selected_post_connector_field_groups as $one_post_connector_field_group) {
						if ($one_post_connector_field_group["deleted"]) {
							continue;
						}
						if (isset($field_groups[$one_post_connector_field_group["id"]])) {

							$field_group_to_add = $field_groups[$one_post_connector_field_group["id"]];

							$meta_box_id = "ecophoto_fields_connector_" . $field_group_to_add["id"];
							$meta_box_title = $this->get_string("Field group name, " . $field_group_to_add["slug"], $field_group_to_add["name"] );
							$meta_box_context = $one_post_connector_field_group["context"];
							$meta_box_priority = $one_post_connector_field_group["priority"];
							$meta_box_callback = create_function ("", "global \$ef; \$ef->meta_box_output({$one_post_connector_field_group["id"]}, $post->ID); ");

							add_meta_box( $meta_box_id, $meta_box_title, $meta_box_callback, $post_type, $meta_box_context, $meta_box_priority );

						}

					}
				}

			}
		}

	} // end function admin head

	function post_dbx_post_sidebar() {
		?>
		<input type="hidden" name="ecophoto_fields_nonce" id="ecophoto_fields_nonce" value="<?php echo wp_create_nonce( basename(__FILE__) ); ?>" />
		<?php
	}

	function save_postdata($post_id = null, $post = null) {
		if (!isset($_POST['ecophoto_fields_nonce']) || !wp_verify_nonce( $_POST['ecophoto_fields_nonce'], basename(__FILE__) )) {
			return $post_id;
		}
		if ( defined('DOING_AUTOSAVE') && DOING_AUTOSAVE ) { return $post_id; }
		if ( isset($_POST["ecophoto_fields_selected_connector"]) ) {
			$ecophoto_fields_selected_connector = (isset($_POST["ecophoto_fields_selected_connector"])) ? $_POST["ecophoto_fields_selected_connector"] : null;
			update_post_meta($post_id, "_simple_fields_selected_connector", $ecophoto_fields_selected_connector);
		}

		$post_id = (int) $post_id;
		$fieldgroups = (isset($_POST["ecophoto_fields_fieldgroups"])) ? $_POST["ecophoto_fields_fieldgroups"] : null;
		$field_groups_option = $this->get_field_groups();

		if ( !$table = _get_meta_table("post") ) { return false; }

		global $wpdb;
		if ($post_id && is_array($fieldgroups)) {
			$post_meta = get_post_custom($post_id);
			$arr_meta_keys_to_keep = array(
				"_simple_fields_been_saved",
				"_simple_fields_selected_connector"
			);
			foreach ($post_meta as $meta_key => $meta_val) {

				if ( strpos($meta_key, "_simple_fields_") === 0 ) {
					if ( ! in_array($meta_key, $arr_meta_keys_to_keep ) ) {
						delete_post_meta($post_id, $meta_key);
					}
				}

			}
			$fieldgroups_fixed = $fieldgroups;
			foreach ($fieldgroups as $one_field_group_id => $one_field_group_fields) {

				foreach ($one_field_group_fields as $posted_id => $posted_vals) {
					if ($posted_id == "added") {
						continue;
					}
					$fieldgroups_fixed[$one_field_group_id][$posted_id] = array();
					foreach ($one_field_group_fields["added"] as $added_id => $added_val) {
						$fieldgroups_fixed[$one_field_group_id][$posted_id][$added_id] = @$fieldgroups[$one_field_group_id][$posted_id][$added_id];
					}
				}

			}
			$fieldgroups = $fieldgroups_fixed;
			update_post_meta($post_id, "_simple_fields_been_saved", "1");
			foreach ($fieldgroups as $one_field_group_id => $one_field_group_fields) {
				$arr_fieldgroup_info = $this->get_field_group( $one_field_group_id );

				foreach ($one_field_group_fields as $one_field_id => $one_field_values) {
					$field_info = isset($field_groups_option[$one_field_group_id]["fields"][$one_field_id]) ? $field_groups_option[$one_field_group_id]["fields"][$one_field_id] : NULL;
					$field_type = $field_info["type"]; // @todo: this should be a function

					$do_wpautop = false;
					if ($field_type == "textarea" && isset($field_info["type_textarea_options"]["use_html_editor"]) && $field_info["type_textarea_options"]["use_html_editor"] == 1) {
						$do_wpautop = true;
					}
					$do_wpautop = apply_filters("ecophoto_fields_save_postdata_do_wpautop", $do_wpautop, $post_id);
					$num_in_set = 0;

					foreach ($one_field_values as $one_field_value) {
$arr_field_info = array();
$one_field_slug = "";
if ("added" === $one_field_id) {
	$one_field_slug = "added";
} else {
	foreach ($arr_fieldgroup_info["fields"] as $one_field_in_fieldgroup) {
		if ( intval( $one_field_in_fieldgroup["id"] ) === intval( $one_field_id ) ) {
			$arr_field_info = $one_field_in_fieldgroup;
			break;
		}
	}
	$one_field_slug = $arr_field_info["slug"];
}

						$custom_field_key = $this->get_meta_key( $one_field_group_id, $one_field_id, $num_in_set, $arr_fieldgroup_info["slug"], $one_field_slug );

						$custom_field_value = $one_field_value;




						if (array_key_exists($field_type, $this->registered_field_types)) {
							$custom_field_value = $this->registered_field_types[$field_type]->edit_save($custom_field_value);


						} else {
							if ($do_wpautop) {
								$custom_field_value = wpautop($custom_field_value);
							}

						}
						update_post_meta($post_id, $custom_field_key, $custom_field_value);
						$num_in_set++;

					}

				}

			}
		} else if (empty($fieldgroups)) {
			$wpdb->query("DELETE FROM $table WHERE post_id = $post_id AND meta_key LIKE '_simple_fields_fieldGroupID_%'");
		}

	} // save postdata

	function clear_caches() {

		do_action("ecophoto_fields_clear_caches");

		$prev_key = $this->ns_key;
		$this->ns_key = wp_cache_incr( 'ecophoto_fields_namespace_key', 1, 'ecophoto_fields' );
		global $wp_object_cache;
		if (isset($wp_object_cache->cache["ecophoto_fields"])) {
			$cache = $wp_object_cache->cache;
			$cache['ecophoto_fields'] = array();
			$wp_object_cache->cache = $cache;

		}

		if ($this->ns_key === FALSE) {
			$this->ns_key = $prev_key + 1;
			wp_cache_set( 'ecophoto_fields_namespace_key', $this->ns_key, 'ecophoto_fields' );
		}

	} // clear caches

	function meta_box_output($post_connector_field_id, $post_id) {

		global $ef;

		$field_groups = $this->get_field_groups( false );
		$current_field_group = $field_groups[$post_connector_field_id];
		$num_added_field_groups = 0;
		$meta_key_num_added = $this->get_meta_key_num_added( $current_field_group["id"], $current_field_group["slug"] );
		while (get_post_meta($post_id, "{$meta_key_num_added}{$num_added_field_groups}", true)) {
			$num_added_field_groups++;
		}

		$num_added_field_groups_css = "";
		if ($num_added_field_groups > 0) $num_added_field_groups_css = "ecophoto-fields-meta-box-field-group-wrapper-has-fields-added";

		$field_group_slug_css = "";
		if (isset($current_field_group["slug"]) && !empty($current_field_group["slug"])) {
			$field_group_slug_css = "ecophoto-fields-meta-box-field-group-wrapper-slug-" . $current_field_group["slug"];
		}

	 	$field_group_wrapper_css = "";

		$default_gui_view = isset( $current_field_group["gui_view"] ) ? $current_field_group["gui_view"] : "list";
		if ("table" === $default_gui_view) {
			$field_group_wrapper_css .= " ecophoto-fields-meta-box-field-group-wrapper-view-table ";
		}
		echo "<div class='ecophoto-fields-meta-box-field-group-wrapper $num_added_field_groups_css $field_group_slug_css $field_group_wrapper_css'>";
		echo "<input type='hidden' name='ecophoto-fields-meta-box-field-group-id' value='$post_connector_field_id' />";
		if ( ! empty($current_field_group["description"]) ) {
			printf("<p class='%s'>%s</p>", "ecophoto-fields-meta-box-field-group-description", esc_html( $this->get_string("Field group description, " . $current_field_group["slug"], $current_field_group["description"]) ) );
		}

		if ($current_field_group["repeatable"]) {
			if ("table" === $default_gui_view) {
				echo "<div class='ef-cf ecophoto-fields-metabox-field-group-view-table-headline-wrap'>";
				foreach ( $current_field_group["fields"] as $field_id => $field_arr ) {
					printf('<div class="ecophoto-fields-metabox-field-group-view-table-headline ecophoto-fields-metabox-field-group-view-table-headline-count-%1$d">', $current_field_group["fields_count"]);
					printf('<div class="ecophoto-fields-field-group-view-table-headline-name">%1$s</div>', $this->get_string( "Field name, " . $field_arr["slug"], $field_arr["name"] ) );
					printf('<div class="ecophoto-fields-field-group-view-table-headline-description">%1$s</div>', $this->get_string("Field description, " . $field_arr["slug"], $field_arr["description"] ) );
					printf('</div>');
				}
				echo "</div>";
			}
			$ul_add_css = "";
			echo "
				<div class='ecophoto-fields-metabox-field-add ecophoto-fields-metabox-field-add-top'>
					<a href='#'>+ ".__('Add', 'ecophoto-fields')."</a>
				</div>
			";


			if ("table" === $default_gui_view) {
				$ul_add_css .= " ecophoto-fields-metabox-field-group-fields-view-table";
			}
			$ul_add_css .= " ecophoto-fields-metabox-field-group-fields-count-" . $current_field_group["fields_count"];
			echo "<ul class='ef-cf ecophoto-fields-metabox-field-group-fields ecophoto-fields-metabox-field-group-fields-repeatable $ul_add_css'>";
			$use_defaults = null;
			for ($num_in_set=0; $num_in_set<$num_added_field_groups; $num_in_set++) {
				$this->meta_box_output_one_field_group($post_connector_field_id, $num_in_set, $post_id, $use_defaults);
			}
			echo "</ul>";
			echo "
				<div class='ecophoto-fields-metabox-field-add ecophoto-fields-metabox-field-add-bottom'>
					<a href='#'>+ ".__('Add', 'ecophoto-fields')."</a>
				</div>
			";


		} else {
			$been_saved = (bool) get_post_meta($post_id, "_simple_fields_been_saved", true);
			if ($been_saved) { $use_defaults = false; } else { $use_defaults = true; }

			echo "<ul>";
			$this->meta_box_output_one_field_group($post_connector_field_id, 0, $post_id, $use_defaults);
			echo "</ul>";

		}

		echo "</div>";

	} // end

	function get_meta_key_num_added( $field_group_id = null, $field_group_slug = null ) {

		if ( ! isset( $field_group_id ) || ! is_numeric( $field_group_id ) ) return false;

		$custom_field_key = $this->get_meta_key( $field_group_id, "added", 0, $field_group_slug, "added" );
		$custom_field_key = rtrim($custom_field_key, "0");

		return $custom_field_key;
	}

	function get_meta_key($field_group_id = NULL, $field_id = NULL, $num_in_set = 0, $field_group_slug = "", $field_slug = "") {

		if ( ! isset($field_group_id) || ! isset($field_group_id) || ! is_numeric($field_group_id) || ! isset($field_id) || ! isset($num_in_set) ) return FALSE;
		$custom_field_key_template = '_simple_fields_fieldGroupID_%1$d_fieldID_%2$s_numInSet_%3$s';

		$custom_field_key_template = apply_filters("ecophoto_fields_get_meta_key_template", $custom_field_key_template);

		$custom_field_key = sprintf(
			$custom_field_key_template,
			$field_group_id, // 1
			$field_id, // 2
			$num_in_set, // 3
			$field_group_slug, // 4
			$field_slug // 5
		);
		$custom_field_key = apply_filters("ecophoto_fields_get_meta_key", $custom_field_key);

		return $custom_field_key;

	}

	function meta_box_output_one_field_group($field_group_id, $num_in_set, $post_id, $use_defaults) {

		$post = get_post($post_id);

		$field_groups = $this->get_field_groups();
		$current_field_group = $field_groups[$field_group_id];
		$repeatable = (bool) $current_field_group["repeatable"];
		$field_group_css = "ecophoto-fields-fieldgroup-$field_group_id";


		?><li class="ef-cf ecophoto-fields-metabox-field-group <?php echo $field_group_css ?>">
			<?php // must use this "added"-thingie do be able to track added field group that has no added values (like unchecked checkboxes, that we can't detect ?>
			<input type="hidden" name="ecophoto_fields_fieldgroups[<?php echo $field_group_id ?>][added][<?php echo $num_in_set ?>]" value="1" />
			<div class="ecophoto-fields-metabox-field-group-handle"></div>
			<?php
			if ($repeatable) {
				?><div class="hidden ecophoto-fields-metabox-field-group-delete"><a href="#" title="<?php _e('Remove field group', 'ecophoto-fields') ?>"></a></div><?php
			}
			foreach ($current_field_group["fields"] as $field) {

				if ($field["deleted"]) { continue; }

				$field_id = $field["id"];
				$field_unique_id = "ecophoto_fields_fieldgroups_{$field_group_id}_{$field_id}_{$num_in_set}";
				$field_name = "ecophoto_fields_fieldgroups[$field_group_id][$field_id][$num_in_set]";
				$field_class = "ecophoto-fields-fieldgroups-field-{$field_group_id}-{$field_id} ";
				$field_class .= "ecophoto-fields-fieldgroups-field-type-" . $field["type"];
				if (isset($field["slug"]) && !empty($field["slug"])) {
					$field_class .= " ecophoto-fields-fieldgroups-field-slug-" . $field["slug"];
				}
				$custom_field_key = $this->get_meta_key($field_group_id, $field_id, $num_in_set, $current_field_group["slug"], $field["slug"]);



				$saved_value = get_post_meta($post_id, $custom_field_key, true);
				$field_maybe_translated_name = $this->get_string( "Field name, " . $field["slug"], $field["name"] );
				$description = "";
				if ( ! empty( $field["description"] ) ) {
					$description = sprintf("<div class='ecophoto-fields-metabox-field-description'>%s</div>", esc_html( $this->get_string("Field description, " . $field["slug"], $field["description"] ) ) );
				}
				$field_type_options = ! isset( $field["options"] ) || ! isset( $field["options"][$field["type"]] ) ? array() : (array) $field["options"][ $field["type"] ];
				$placeholder = empty( $field_type_options["placeholder"] ) ? "" : esc_attr( $field_type_options["placeholder"] );

				?>

				<div
					class="ecophoto-fields-metabox-field ef-cf <?php echo $field_class ?>"
					data-fieldgroup_id=<?php echo $field_group_id ?>
					data-field_id="<?php echo $field_id ?>"
					data-num_in_set=<?php echo $num_in_set ?>
					>
					<?php

					if ("file" == $field["type"]) {

						$current_post_id = !empty( $_GET['post_id'] ) ? (int) $_GET['post_id'] : 0;
						$attachment_id = (int) $saved_value;
						$image_html = "";
						$image_name = "";
						$view_file_url = "";
						$class = "";
						if ($attachment_id) {
							$class .= " ecophoto-fields-metabox-field-file-is-selected ";
							$image_post = get_post($attachment_id);
							if ($image_post === NULL) {
							} else {
								$image_thumbnail = wp_get_attachment_image_src( $attachment_id, 'thumbnail', true );
								$image_thumbnail = $image_thumbnail[0];
								$image_html = "<img src='$image_thumbnail' alt='' />";
								$image_name = esc_html($image_post->post_title);
							}
							$view_file_url = wp_get_attachment_url($attachment_id);
						}
						if ($description) {
						}
						echo "<div class='ecophoto-fields-metabox-field-file $class'>";

							echo "<div class='ecophoto-fields-metabox-field-first'>";
							echo "<label>{$field_maybe_translated_name}</label>";
							echo $description;
							echo "</div>";

							echo "<div class='ecophoto-fields-metabox-field-second'>";

							echo "<div class='ecophoto-fields-metabox-field-file-col1'>";
								echo "<div class='ecophoto-fields-metabox-field-file-selected-image'>$image_html</div>";
							echo "</div>";

							echo "\n<div class='ecophoto-fields-metabox-field-file-col2'>";

								echo "\n<input type='hidden' class='text ecophoto-fields-metabox-field-file-fileID' name='$field_name' id='$field_unique_id' value='$attachment_id' />";
								echo "\n<div class='ecophoto-fields-metabox-field-file-selected-image-name'>$image_name</div>";
								$field_unique_id_esc = rawurlencode($field_unique_id);
								$file_url = get_bloginfo('wpurl') . "/wp-admin/media-upload.php?ecophoto_fields_dummy=1&ecophoto_fields_action=select_file&ecophoto_fields_file_field_unique_id=$field_unique_id_esc&post_id=$current_post_id&TB_iframe=true";

								echo "\n<a class='ecophoto-fields-metabox-field-file-select' href='$file_url'>".__('Select', 'ecophoto-fields')."</a> ";
								echo "\n<a href='#' class='ecophoto-fields-metabox-field-file-clear'>".__('Clear', 'ecophoto-fields')."</a> ";
								echo "\n<a class='ecophoto-fields-metabox-field-file-view' target='_blank' href='$view_file_url'>".__('View', 'ecophoto-fields')."</a> ";
								$href_edit = ($attachment_id) ? admin_url("post.php?post={$attachment_id}&action=edit") : "#";
								echo "\n<a href='{$href_edit}' target='_blank' class='ecophoto-fields-metabox-field-file-edit'>".__('Edit', 'ecophoto-fields') . "</a>";

							echo "\n</div>";


							echo "\n</div>"; // second

						echo "\n</div>";

					} elseif ("text" == $field["type"]) {

						$text_value_esc = esc_html($saved_value);

						$type_attr = isset( $field_type_options["subtype"] ) ? $field_type_options["subtype"] : "text";
						$extra_attributes = isset( $field_type_options["attributes"] ) ? $field_type_options["attributes"] : "";

						echo "<div class='ecophoto-fields-metabox-field-first'>";
						echo "<label for='$field_unique_id'> " . $field_maybe_translated_name . "</label>";
						echo $description;
						echo "</div>";

						echo "<div class='ecophoto-fields-metabox-field-second'>";
						echo "<input type='$type_attr' class='text' name='$field_name' id='$field_unique_id' value='$text_value_esc' placeholder='$placeholder' $extra_attributes />";
						echo "</div>";

					} else {
						if (isset($this->registered_field_types[$field["type"]])) {

							$custom_field_type = $this->registered_field_types[$field["type"]];
							$custom_field_type->set_options_base_id($field_unique_id);
							$custom_field_type->set_options_base_name($field_name);
							$custom_field_type_options = isset($field["options"][$field["type"]]) ? $field["options"][$field["type"]] : array();
							echo "<div class='ecophoto-fields-metabox-field-first'>";
							echo "<label>" . $field_maybe_translated_name . "</label>";
							echo $description;
							echo "</div>";

							echo "<div class='ecophoto-fields-metabox-field-second'>";
							if ($use_defaults) $custom_field_type_options["use_defaults"] = $use_defaults;
							$custom_field_type_saved_value = $saved_value;
							$custom_field_type_saved_value = (array) $custom_field_type_saved_value;
							echo $custom_field_type->edit_output($custom_field_type_saved_value, $custom_field_type_options);

							echo "</div>";

						}

					} // field types
					?>
					<div class="ecophoto-fields-metabox-field-custom-field-key hidden">
						<strong><?php _e('Meta key:', 'ecophoto-fields') ?></strong>
						<?php echo $custom_field_key ?>
						<?php if (isset($field["slug"])) { ?>
							<br><strong><?php _e('Slug:', 'ecophoto-fields') ?></strong>
							<?php echo $field["slug"] ?>
						<?php } ?>
					</div>
				</div><!-- // end ecophoto-fields-metabox-field -->
				<?php
			} // foreach

			?>
		</li>
		<?php
	} // end function print

	function get_post_connectors_for_post_type($post_type) {

		global $ef;

		$arr_post_connectors = $this->get_post_connectors();
		$arr_found_connectors = array();

		foreach ($arr_post_connectors as $one_connector) {
			if ($one_connector && in_array($post_type, $one_connector["post_types"])) {
				$arr_found_connectors[] = $one_connector;
			}
		}

		$arr_found_connectors = apply_filters( "ecophoto_fields_get_post_connectors_for_post_type", $arr_found_connectors, $post_type);
		return $arr_found_connectors;

	}

	public function get_selected_connector_for_post( $post ) {
		$connectors = array(
			'ecophoto' => '1'
		);

		return !empty($post) && isset($post->post_type) && isset($connectors[$post->post_type]) ? $connectors[$post->post_type] : 1;
	}

	function get_default_connector_for_post_type($post_type) {

		$post_type_defaults = $this->get_post_type_defaults();
		$selected_post_type_default = (isset($post_type_defaults[$post_type]) ? $post_type_defaults[$post_type] : "__none__");
		$selected_post_type_default = apply_filters( "ecophoto_fields_get_default_connector_for_post_type", $selected_post_type_default, $post_type );

		return $selected_post_type_default;

	}

	function post_has_template_connector($post) {

		$template = !empty($post->page_template) ? $post->page_template : false;
		$post_connector_from_template = $this->get_post_connector_from_template( $template );
		return (bool) $post_connector_from_template;

	}

	function get_post_connector_from_template($template) {

		$template_file = locate_template($template);
		if ( is_file( $template_file ) ) {
			$template_data = get_file_data( $template_file, array("Name" => "Template Name", "PostConnector" => "EcoPhoto Fields Connector") );
			$post_connector = trim($template_data["PostConnector"]);
		} else {
			$post_connector = "";
		}

		$post_connector = apply_filters("get_post_connector_from_template", $post_connector, $template);

		return $post_connector;

	}

	function get_string($name = "", $value = "") {

		if ( $this->is_wpml_active() && function_exists("icl_t") ) {
			$value = icl_t($this->wpml_context, $name, $value);
			return $value;
		} else {
			return $value;
		}

	}

	public function is_wpml_active() {

		global $sitepress;
		return ( isset( $sitepress ) && $sitepress instanceof SitePress );

	}

	public function get_post_type_defaults() {
		return array(
			''
		);
	}

	public function get_post_connectors() {
		return array(
			'1' => array(
				'id' => '1',
				'key' => 'photos',
				'slug' => 'photos',
				'name' => 'Photos',
				'field_groups' => array
				(
					'1' => array (
						'id' => 1,
						'name' => 'Additional Photos',
						'deleted' => 0,
						'context' => 'normal',
						'priority' => 'low'
					)
				),
				'post_types' => array
					(
						'ecophoto'
					),

				'deleted' => false,
				'hide_editor' => false,
				'added_with_code' => false,
				'field_groups_count' => 1

			)
        );
	}

	public function get_field_groups() {
		return array(
			'1' => array
				(
					'id' => 1,
					'key' => 'photos',
					'slug' => 'photos',
					'name' => 'Additional Photos',
					'description' => '',
					'repeatable' => 1,
					'fields' => array
						(
							'1' => array
								(
									'name' => 'Photo description',
									'slug' => 'sdesc',
									'description' => '',
									'type' => 'text',
									'options' => array
										(
											'divider' => array
												(
													'appearance' => 'line'
												),

											'date_v2' => array
												(
													'show_as' => 'date',
													'show' => 'always',
													'default_date' => 'no_date'
												),

											'text' => array
												(
													'subtype' => 'text',
													'placeholder' => '',
													'attributes' => ''
												),

											'textarea' => array
												(
													'placeholder' => ''
												)

										),

									'type_textarea_options' => array
										(
											'size_height' => 'default'
										),

									'type_post_options' => array
										(
											'additional_arguments' => ''
										),

									'type_taxonomyterm_options' => array
										(
											'additional_arguments' => ''
										),

									'type_dropdown_options' => array
										(
											'enable_multiple' => 0
										),

									'id' => 1,
									'deleted' => 0
								),

							/*'2' => array
								(
									'name' => 'Photo thumbnail',
									'slug' => 'sthumb',
									'description' => '',
									'type' => 'file',
									'options' => array
										(
											'divider' => array
												(
													'appearance' => 'line'
												),

											'date_v2' => array
												(
													'show_as' => 'date',
													'show' => 'always',
													'default_date' => 'no_date'
												),

											'text' => array
												(
													'subtype' => 'text',
													'placeholder' => '',
													'attributes' => ''
												),

											'textarea' => array
												(
													'placeholder' => ''
												)

										),

									'type_textarea_options' => array
										(
											'size_height' => 'default'
										),

									'type_post_options' => array
										(
											'additional_arguments' => ''
										),

									'type_taxonomyterm_options' => array
										(
											'additional_arguments' => ''
										),

									'type_dropdown_options' => array
										(
											'enable_multiple' => 0
										),

									'id' => 2,
									'deleted' => 0
								),*/

							'3' => array
								(
									'name' => 'Photo image',
									'slug' => 'simage',
									'description' => '',
									'type' => 'file',
									'options' => array
										(
											'divider' => array
												(
													'appearance' => 'line'
												),

											'date_v2' => array
												(
													'show_as' => 'date',
													'show' => 'always',
													'default_date' => 'no_date'
												),

											'text' => array
												(
													'subtype' => 'text',
													'placeholder' => '',
													'attributes' => ''
												),

											'textarea' => array
												(
													'placeholder' => ''
												)

										),

									'type_textarea_options' => array
										(
											'size_height' => 'default'
										),

									'type_post_options' => array
										(
											'additional_arguments' => ''
										),

									'type_taxonomyterm_options' => array
										(
											'additional_arguments' => ''
										),

									'type_dropdown_options' => array
										(
											'enable_multiple' => 0
										),

									'id' => 3,
									'deleted' => 0
								)

						),

					'fields_by_slug' => array
						(
						),

					'deleted' => false,
					'gui_view' => 'list',
					'added_with_code' => false,
					'fields_count' => 3
				)
		);
	}

	function get_field_group($group_id) {

		$field_groups = $this->get_field_groups();
		$return = false;
		if (is_array($field_groups)) {
			foreach($field_groups as $field_group) {
				if (is_numeric($group_id)) {
					if ($field_group['id'] == $group_id) {
						$return = $field_group;
						break;
					}
				} else {
					if ($field_group['name'] == $group_id) {
						$return = $field_group;
						break;
					}
				}
			}
		}

		$return = apply_filters( "ecophoto_fields_get_field_group", $return );
		return $return;

	}
}

global $ef;
$ef = new ecophoto_fields();