<?php

if (!function_exists("ef_d")) {
	function ef_d($var, $heading = "") {

		$out = "";
		$out .= "\n<pre class='sf_box_debug'>\n";
		if ($heading && ! empty($heading)) {
			$out .= "<b>" . esc_html($heading) . ":</b>\n";
		}
		if (is_array($var) || is_object($var)) {
			$out .= htmlspecialchars( print_r($var, true), ENT_QUOTES, 'UTF-8' );
		} else if( is_null($var) ) {
			$out .= "Var is NULL";
		} else if ( is_bool($var)) {
			$out .= "Var is BOOLEAN ";
			$out .= $var ? "TRUE" : "FALSE";
		} else if ( is_string($var) ) {
			if (strlen($var) === 0)
				$out .= 'Var is empty string ("").';
			else
				$out .= "Var is string with length " . strlen($var) . ": " . htmlspecialchars( $var, ENT_QUOTES, 'UTF-8' );
		} else {
			$out .= htmlspecialchars( $var, ENT_QUOTES, 'UTF-8' );
		}
		$out .= "\n</pre>";
		echo apply_filters( "ecophoto_fields_debug_output", $out );

	}
}



function ecophoto_fields_get_post_value($post_id, $field_name_or_id, $single = true) {

	global $ef;

	$fetch_by_id = true;
	if (is_array($field_name_or_id) && sizeof($field_name_or_id) == 2) {
		$field_group_id = $field_name_or_id[0];
		$field_id = $field_name_or_id[1];
		$fetch_by_id = false;
	}

	$connector = ecophoto_fields_get_all_fields_and_values_for_post($post_id);

	$return_val = null;
	if ($connector) {
		foreach ($connector["field_groups"] as $one_field_group) {
			$is_found = false;
			foreach ($one_field_group["fields"] as $one_field) {

				if ($fetch_by_id && $one_field["name"] == $field_name_or_id) {
					$is_found = true;
				} else if (!$fetch_by_id && (intval($one_field_group["id"]) === intval($field_group_id)) && (intval($one_field["id"]) === intval($field_id))) {
					$is_found = true;
				}

				$saved_values = isset($one_field["saved_values"]) ? $one_field["saved_values"] : null;

				if ($one_field["type"] == "radiobuttons" || $one_field["type"] == "dropdown") {
					if ($one_field["type"] == "radiobuttons") {
						$get_value_key = "type_radiobuttons_options";
					} else if ($one_field["type"] == "dropdown") {
						$get_value_key = "type_dropdown_options";
					}
					for ($saved_i = 0; $saved_i < sizeof($saved_values); $saved_i++) {
						$saved_values[$saved_i] = $one_field[$get_value_key][$saved_values[$saved_i]]["value"];
					}
				}

				$parsed_options_for_this_field = array();
				$field_options_key = "type_".$one_field["type"]."_options";
				if (isset($one_field[$field_options_key])) {
					if (isset($one_field[$field_options_key]["enable_extended_return_values"]) && $one_field[$field_options_key]["enable_extended_return_values"]) {
						$parsed_options_for_this_field["extended_return"] = 1;
					}

					if (isset($parsed_options_for_this_field["extended_return"]) && $parsed_options_for_this_field["extended_return"]) {
						$num_values = count($saved_values);
						while ($num_values--) {
							$saved_values[$num_values] = $ef->get_extended_return_values_for_field($one_field, $saved_values[$num_values]);
						}
					}

				}

				if ($is_found && $single) {
					$return_val = $saved_values[0];
				} else if ($is_found) {
					$return_val = $saved_values;
				}

				if ($is_found) {
					$return_val = apply_filters( "ecophoto_fields_get_post_value", $return_val, $post_id, $field_name_or_id, $single);
					return $return_val;
				}


			}
		}
	}
	$return_val = NULL;
	$return_val = apply_filters( "ecophoto_fields_get_post_value", $return_val, $post_id, $field_name_or_id, $single);
	return $return_val;

}



function ecophoto_fields_get_post_group_values($post_id, $field_group_name_or_id, $use_name = true, $return_format = 1) {

	$fetch_by_id = false;
	if (is_numeric($field_group_name_or_id)) {
		$field_group_name_or_id = (int) $field_group_name_or_id;
		$fetch_by_id = true;
	}

	$connector = ecophoto_fields_get_all_fields_and_values_for_post($post_id);

	if (!$connector) {
		$return_val = apply_filters( "ecophoto_fields_get_post_group_values", array(), $post_id, $field_group_name_or_id, $use_name, $return_format);
		return $return_val;
	}

	foreach ($connector["field_groups"] as $one_field_group) {

		$is_found = false;
		if ($fetch_by_id && $one_field_group["id"] == $field_group_name_or_id) {
			$is_found = true;
		} else if (!$fetch_by_id && $field_group_name_or_id == $one_field_group["name"]) {
			$is_found = true;
		}

		if ($is_found) {
			$arr_return = array();
			foreach ($one_field_group["fields"] as $one_field) {

				$saved_values = isset( $one_field["saved_values"] ) ? $one_field["saved_values"] : null;

				if (is_null($saved_values)) {
					continue;
				}

				if ($one_field["type"] == "radiobuttons" || $one_field["type"] == "dropdown") {
					if ($one_field["type"] == "radiobuttons") {
						$get_value_key = "type_radiobuttons_options";
					} else if ($one_field["type"] == "dropdown") {
						$get_value_key = "type_dropdown_options";
					}
					for ($saved_i = 0; $saved_i < sizeof($saved_values); $saved_i++) {
						$saved_values[$saved_i] = $one_field[$get_value_key][$saved_values[$saved_i]]["value"];
					}
				}

				if ($use_name) {
					$arr_return[$one_field["name"]] = $saved_values;
				} else {
					$arr_return[$one_field["id"]] = $saved_values;
				}
			}

			$set_count = isset( $one_field["saved_values"] ) ? sizeof( $one_field["saved_values"] ) : 0;

			$arr_return2 = array();
			for ($i=0; $i<$set_count; $i++) {
				$arr_return2[$i] = array();
				foreach ($arr_return as $key => $val) {
					$arr_return2[$i][$key] = $val[$i];
				}
			}

			if ($return_format == 1) {

				$arr_return = apply_filters( "ecophoto_fields_get_post_group_values", $arr_return, $post_id, $field_group_name_or_id, $use_name, $return_format);
				return $arr_return;

			} elseif ($return_format == 2) {

				$arr_return2 = apply_filters( "ecophoto_fields_get_post_group_values", $arr_return2, $post_id, $field_group_name_or_id, $use_name, $return_format);
				return $arr_return2;

			}

		}
	}

}


function ecophoto_fields_get_all_fields_and_values_for_post($post_id, $args = "") {

	global $ef;
	$cache_key = 'ecophoto_fields_'.$ef->ns_key.'_get_all_fields_and_values_for_post_' . $post_id . "_" . md5(json_encode($args));
	$selected_post_connector = wp_cache_get( $cache_key , 'simple_fields' );

	if (FALSE === $selected_post_connector) {

		$defaults = array(
			"include_deleted" => TRUE
		);
		$args = wp_parse_args($args, $defaults);

		$post                     = get_post($post_id);
		$connector_to_use         = $ef->get_selected_connector_for_post($post);
		$existing_post_connectors = $ef->get_post_connectors();
		$field_groups             = $ef->get_field_groups();
		$selected_post_connector  = isset($existing_post_connectors[$connector_to_use]) ? $existing_post_connectors[$connector_to_use] : NULL;

		if ($selected_post_connector == null) {
			$return_val = FALSE;
			$return_val = apply_filters( "ecophoto_fields_get_all_fields_and_values_for_post", $return_val, $post_id, $args);
			return $return_val;
		}
		if (!$args["include_deleted"]) {

			$arr_field_groups_to_keep = array();
			foreach ($selected_post_connector["field_groups"] as $one_field_group_id => $one_field_group) {

				if ($one_field_group["deleted"]) continue;

				$arr_field_groups_to_keep[$one_field_group_id] = $one_field_group;

			}
			$selected_post_connector["field_groups"] = $arr_field_groups_to_keep;
		}
		foreach ($selected_post_connector["field_groups"] as $one_field_group) { // one_field_group = name, deleted, context, priority, id
			$selected_post_connector["field_groups"][ $one_field_group["id"] ] = array_merge( $selected_post_connector["field_groups"][ $one_field_group["id"] ], $field_groups[ $one_field_group["id"] ] );
			$field_group_slug = isset( $one_field_group["slug"] ) ? $one_field_group["slug"] : "";
			$num_added_field_groups = 0;
			$meta_key_num_added = $ef->get_meta_key_num_added( $one_field_group["id"], $field_group_slug );

			while (get_post_meta($post_id, "{$meta_key_num_added}{$num_added_field_groups}", true)) {
				$num_added_field_groups++;
			}
			if ($num_added_field_groups == 0 && (isset($one_field_group['repeatable']) && !$one_field_group['repeatable']) ) {
			    $num_added_field_groups++;
			}
			for ($num_in_set = 0; $num_in_set < $num_added_field_groups; $num_in_set++) {
				foreach ($selected_post_connector["field_groups"][$one_field_group["id"]]["fields"] as $one_field_id => $one_field_value) {

					$one_field_group_slug = isset( $one_field_group["slug"] ) ? $one_field_group["slug"] : "";
					$one_field_value_slug = isset( $one_field_value["slug"] ) ? $one_field_value["slug"] : "";

					$custom_field_key = $ef->get_meta_key( $one_field_group["id"], $one_field_id, $num_in_set, $one_field_group_slug, $one_field_value_slug );

					$saved_value = get_post_meta($post_id, $custom_field_key, true); // empty string if does not exist
					if ("textarea" === $one_field_value["type"]) {
						$match_count = preg_match_all('/http:\/\/[a-z0-9A-Z\.]+[a-z0-9A-Z\.\/%&=\?\-_#]+/i', $saved_value, $match);
						if ($match_count) {
							$links=$match[0];
							for ($j=0;$j<$match_count;$j++) {
								if (strpos($saved_value, 'href="'.$links[$j].'"') === false && strpos($saved_value, "href='".$links[$j]."'") === false) {
									$attr['discover'] = (apply_filters('embed_oembed_discover', false)) ? true : false;
									$oembed_html = wp_oembed_get($links[$j], $attr);
									if ($oembed_html) {
										$saved_value = str_replace($links[$j], apply_filters('embed_oembed_html', $oembed_html, $links[$j], $attr), $saved_value);
									}
								}
							}
						}
					} else if ("dropdown" === $one_field_value["type"]) {
						if (isset($one_field_value["type_dropdown_options"]["enable_multiple"]) && $one_field_value["type_dropdown_options"]["enable_multiple"]) {
							if (!is_array($saved_value)) $saved_value = array();

						}

					}

					$selected_post_connector["field_groups"][$one_field_group["id"]]["fields"][$one_field_id]["saved_values"][$num_in_set] = $saved_value;
					$selected_post_connector["field_groups"][$one_field_group["id"]]["fields"][$one_field_id]["meta_keys"][$num_in_set] = $custom_field_key;

				}
			}

		}
		wp_cache_set( $cache_key, $selected_post_connector, 'simple_fields' );
	}

	$selected_post_connector = apply_filters( "ecophoto_fields_get_all_fields_and_values_for_post", $selected_post_connector, $post_id, $args);
	return $selected_post_connector;
}


// TODO: A variable in ecophoto_fields_groups that keeps track of the most number
// of times a field has been repeated on any single post so that $num_in_set can
// be determined dynamically.
function ecophoto_fields_get_meta_query($group_id, $field_id, $value, $compare = "=", $type = "CHAR", $order = "", $num_in_set = 1) {

	global $ef;
	$field_group = $ef->get_field_group($group_id);
	$field 		 = $ef->get_field_in_group($field_group, $field_id);

	if (!is_array($field_group) || !is_array($field)) {
		return false;
	}

	if(!is_numeric($num_in_set) || $num_in_set < 1) {
		$num_in_set = 1;
	}

	if ($field["type"] == "radiobuttons") {
		$get_value_key = "type_radiobuttons_options";
	} else if ($field["type"] == "dropdown") {
		$get_value_key = "type_dropdown_options";
	}

	if (!empty($get_value_key) && is_array($field[$get_value_key])) {
		foreach($field[$get_value_key] as $option_key => $option) {
			if ($option['value'] == $value && (!isset($option['deleted']) || intval($option['deleted']) == 0)) {
				$value = $option_key;
			}
		}
	}

	$query_args = array('meta_query' => array('relation' => 'OR'));

	for($i=0;$i<$num_in_set;$i++) {
		$query_args['meta_query'][$i]['key'] = $ef->get_meta_key( $field_group['id'], $field['id'], $i, $field_group['slug'], $field['slug'] );
		$query_args['meta_query'][$i]['value'] = $value;
		$query_args['meta_query'][$i]['compare'] = $compare;
		$query_args['meta_query'][$i]['type'] = $type;
	}

	if ($order == "ASC" || $order == "DESC") {
		$query_args['meta_key'] = $query_args['meta_query'][0]['key'];
		$query_args['orderby'] = 'meta_value';
		$query_args['order'] = $order;
	}

	$query_args = apply_filters( "ecophoto_fields_get_meta_query", $query_args, $group_id, $field_id, $value, $compare, $type, $order, $num_in_set);
	return $query_args;
}


function ecophoto_fields_query_posts($query_args = array()) {

		$query_keys = array(
			'sf_group',
			'sf_field',
			'sf_value',
			'sf_compare',
			'sf_type',
			'sf_order',
			'sf_num_in_set'
		);

        foreach($query_keys as $key) {
			switch($key) {
				case "sf_group":
				case "sf_field":
				case "sf_value":
					if(empty($query_args[$key]))
						return false;
					break;
				case "sf_compare":
					if(empty($query_args[$key]))
						$query_args[$key] = "=";
					break;
				case "sf_type":
					if(empty($query_args[$key]))
						$query_args[$key] = "CHAR";
					break;
				case "sf_order":
					if($query_args[$key] != "ASC" && $query_args[$key] != "DESC")
						$query_args[$key] = "";
					break;
				case "sf_num_in_set":
					if(!is_numeric($query_args[$key]) || $query_args[$key] < 1)
						$query_args[$key] = 1;
					break;
			}
	}

	$meta_query_args = ecophoto_fields_get_meta_query($query_args['sf_group'], $query_args['sf_field'], $query_args['sf_value'], $query_args['sf_compare'], $query_args['sf_type'], $query_args['sf_order'], $query_args['sf_num_in_set']);
	$query_args = array_merge($query_args, $meta_query_args);

	$query_args = apply_filters( "ecophoto_fields_query_posts", $query_args);
	return new WP_Query($query_args);

}



function ecophoto_fields_merge_arrays($array1 = array(), $array2 = array()) {
	$array1 = (array) $array1;
	$array2 = (array) $array2;

	foreach($array2 as $key => $value) {

		if ( is_array($value) ) {
			if ( isset( $array1[$key] ) && isset( $array2[$key] ) ) {
				$array1[$key] = ecophoto_fields_merge_arrays( $array1[$key], $array2[$key] );
			} else {
				$array1[$key] = $array2[$key];
			}
		} else {
			$array1[$key] = $value;
		}
	}

	return $array1;
}



function ecophoto_fields_register_field_group($slug = "", $new_field_group = array()) {
	if ( ! is_string($slug) || ! is_array($new_field_group) ) {
		_doing_it_wrong( __FUNCTION__, __("Wrong type of arguments passed", "ecophoto-fields"),  1);
		return false;
	}

	global $ef;

	$field_groups = $ef->get_field_groups();
	$highest_id = 0;
	$is_new_field_group = TRUE;
	$errors = new WP_Error();
	foreach ($field_groups as $oneGroup) {

		if ($oneGroup["slug"] == $slug && !empty($slug)) {
			$field_group_id = $oneGroup["id"];
			$is_new_field_group = FALSE;
			break;

		} else if ($oneGroup["id"] > $highest_id) {
			$highest_id = $oneGroup["id"];

		}

	}
	if ($is_new_field_group) {
		$highest_id++;
		$field_group_id = $highest_id;
	}
	if (empty($slug)) {
		$slug = "field_group_" . $field_group_id;

	} else if ( ! isset($new_field_group["name"]) || empty($new_field_group["name"]) ) {
		$new_field_group["name"] = $slug;

	}
	$slug = sanitize_key($slug);
	if ($is_new_field_group) {

		$field_group_defaults = array(
			"id" => $field_group_id,
			"key" => $slug,
			"slug" => $slug,
			"name" => "Unnamed field group $field_group_id",
			"description" => "",
			"repeatable" => false,
			"fields" => array(),
			"fields_by_slug" => array(),
			"deleted" => false,
			"gui_view" => "list", // list | table
			"added_with_code" => true
		);

	} else {
		$field_group_defaults = $field_groups[$field_group_id];
		if ( ! isset( $field_group_defaults["added_with_code"] ) ) $field_group_defaults["added_with_code"] = true;
		$field_group_defaults["fields_by_slug"] = array();
		if ( is_array( $field_group_defaults["fields"] ) && sizeof( $field_group_defaults["fields"] > 0 ) ) {
			foreach ( $field_group_defaults["fields"] as $one_field_key => $one_field ) {

				if ( ! isset( $one_field["slug"] ) || empty( $one_field["slug"] ) ) continue;
				if ( ! isset( $new_field_group["fields"] ) || ! is_array( $new_field_group["fields"] ) ) continue;

				$old_field_was_found_among_new_fields = false;

				foreach ( $new_field_group["fields"] as $one_new_field ) {

					if ( isset( $one_new_field["slug"] ) && ! empty( $one_new_field["slug"] ) && $one_new_field["slug"] === $one_field["slug"] ) {
						$old_field_was_found_among_new_fields = true;
						break;
					}
				}

				if ( ! $old_field_was_found_among_new_fields) {
					$field_group_defaults["fields"][ $one_field_key ]["deleted"] = true;
				}

			} // foreach
			foreach ( $field_group_defaults["fields"] as $field_id => & $field_array ) {

				$field_array["id"] = $field_id;
				$field_slug = isset( $field_array["slug"] ) ? $field_array["slug"] : "field_$field_id";
				$field_group_defaults["fields_by_slug"][$field_slug] = $field_array;

			} // foreach

		}

	} // if new or not
	if ( isset($field_groups[$field_group_id]["fields"]) && is_array($field_groups[$field_group_id]["fields"]) && sizeof($field_groups[$field_group_id]["fields"]) > 0) {
		$field_id = max( array_keys( $field_groups[$field_group_id]["fields"] ) ) + 1;
	} else {
		$field_id = 0;
	}
	$new_field_group["fields_by_slug"] = array();
	if ( isset($new_field_group["fields"]) ) {
		foreach ( $new_field_group["fields"] as $field_array ) {
			$new_field_group["fields_by_slug"][$field_array["slug"]] = $field_array;
		}
	}
	$field_groups[$field_group_id] = ecophoto_fields_merge_arrays($field_group_defaults, $new_field_group);
	if ( isset($new_field_group["fields"]) && is_array($new_field_group["fields"]) && ! empty($new_field_group["fields"]) ) {
		foreach ( $new_field_group["fields_by_slug"] as $one_new_field ) {
			$field_defaults = array(
					"id"		  => "",
					"name"        => "",
					"slug"        => "",
					"description" => "",
					"type"        => "",
					"type_post_options" => array(
						"enabled_post_types" => array(),
						"additional_arguments" => ""
					),
					"type_taxonomyterm_options" => array(
						"additional_arguments" => ""
					),
					"id"      => NULL,
					"deleted" => 0,
					"options" => array(),
			);
			if ( isset( $field_groups[$field_group_id]["fields"] ) && is_array( $field_groups[$field_group_id]["fields"] ) ) {
				if ( isset( $field_groups[$field_group_id]["fields_by_slug"][$one_new_field["slug"]] ) ) {

					$existing_field_array_from_slug = & $field_groups[$field_group_id]["fields_by_slug"][$one_new_field["slug"]];
					$field_defaults = ecophoto_fields_merge_arrays($field_defaults, $existing_field_array_from_slug );
					foreach ($field_defaults as $oneDefaultFieldKey => $oneDefaultFieldValue) {

						if ($oneDefaultFieldKey === "id") {
							if ( is_null($oneDefaultFieldValue) || $oneDefaultFieldValue === "" ) {
								#echo "<br>new field - added id $field_id";
								$existing_field_array_from_slug["id"] = $field_id;
								$field_id++;
							}

						}
						if ( isset($one_new_field[$oneDefaultFieldKey]) && is_array($one_new_field[$oneDefaultFieldKey]) && !empty($one_new_field[$oneDefaultFieldKey]) ) {
							$options_type = preg_replace("/type_([a-z]+)_options/i", '$1', $oneDefaultFieldKey);
							if ( ! empty($options_type) ) {
								foreach ( array_keys($one_new_field[$oneDefaultFieldKey]) as $optionKey ) {
									if ( is_numeric($optionKey) ) {

										if ("radiobuttons" === $options_type) $options_type = "radiobutton";
										$newOptionKey = $options_type . "_num_" . $optionKey;

										$existing_field_array_from_slug[$oneDefaultFieldKey][$newOptionKey] = $one_new_field[$oneDefaultFieldKey][$optionKey];
										unset($existing_field_array_from_slug[$oneDefaultFieldKey][$optionKey]);
										$optionKey = $newOptionKey;

									}
									if ( isset( $existing_field_array_from_slug[$oneDefaultFieldKey][$optionKey]) && is_array($existing_field_array_from_slug[$oneDefaultFieldKey][$optionKey]) && ! empty($existing_field_array_from_slug[$oneDefaultFieldKey][$optionKey]["value"]) ) {

										if ( ! isset( $existing_field_array_from_slug[$oneDefaultFieldKey][$optionKey]["deleted"]) ) {
											$existing_field_array_from_slug[$oneDefaultFieldKey][$optionKey]["deleted"] = 0;
										}

									}

								} // foreach

							} // if not empty options type

						} // if isset
						if ( ! isset( $existing_field_array_from_slug[$oneDefaultFieldKey] ) ) {
							$existing_field_array_from_slug[$oneDefaultFieldKey] = $oneDefaultFieldValue;
						}

					} // foreach field default
					$arr_merged_options = isset($one_new_field["options"]) ? wp_parse_args( $one_new_field["options"] ) : array();
					if ( ! isset( $existing_field_array_from_slug["options"][ $existing_field_array_from_slug["type"] ] ) ) {
						$existing_field_array_from_slug["options"][ $existing_field_array_from_slug["type"] ] = array();
					}

#echo "<br>before:<br>";
#ef_d($arr_merged_options);
					if ( isset( $existing_field_array_from_slug[ "type_". $existing_field_array_from_slug["type"] . "_options" ] ) ) {
# denna ökar antalet dropdown values vid varje körning
						#ef_d( $existing_field_array_from_slug[ "type_". $existing_field_array_from_slug["type"] . "_options" ] );
						$arr_old_vals_to_merge = array();
						$arr_old_vals_to_merge_values = array();
						foreach ( $existing_field_array_from_slug[ "type_". $existing_field_array_from_slug["type"] . "_options" ] as $one_key => $one_val ) {
							if ( strpos( $one_key, "dropdown_num_" ) !== FALSE || strpos( $one_key, "radiobutton_num_" ) !== FALSE ) {
								$num = str_replace( array("dropdown_num_", "checkbox_num_"), "", $one_key );
								$one_val["num"] = $num;
								$arr_old_vals_to_merge_values[] = $one_val;
							} else {
								$arr_old_vals_to_merge[ $one_key ] = $one_val;
							}

							$arr_old_vals_to_merge["values"] = $arr_old_vals_to_merge_values;

						} // foreach

						#$arr_merged_options = array_merge( $existing_field_array_from_slug[ "type_". $existing_field_array_from_slug["type"] . "_options" ], $arr_merged_options );
#ef_d($arr_old_vals_to_merge);
						$arr_merged_options = array_merge( $arr_old_vals_to_merge, $arr_merged_options );
					}
#ef_d($arr_merged_options);
#echo "<br>after:<br>";
#ef_d($arr_merged_options);
# denna ökar antalet dropdown values vid varje körning
					$arr_merged_options = array_merge( $existing_field_array_from_slug["options"][ $existing_field_array_from_slug["type"] ], $arr_merged_options );
					$existing_field_array_from_slug["options"][ $existing_field_array_from_slug["type"] ] = $arr_merged_options;


					$new_options_keys = isset( $one_new_field["options"] ) ? array_keys( (array) $one_new_field["options"] ) : array();

					if ( isset( $one_new_field["type"] ) && ( $key_to_remove_pos = array_search( $one_new_field["type"], $new_options_keys ) ) !== FALSE ) {
						unset( $new_options_keys[ $key_to_remove_pos ] );
						unset( $existing_field_array_from_slug["options"][ $one_new_field["type"] ][ $one_new_field["type"] ] );
					}
					foreach ( $new_options_keys as $one_key_to_remove ) {
						unset( $existing_field_array_from_slug["options"][ $one_key_to_remove ] );
					}
					if ( isset( $one_new_field["type"] ) && ( $one_new_field["type"] === "dropdown" || $one_new_field["type"] === "radiobuttons" ) ) {

						if ( isset( $one_new_field["options"]["values"] ) && is_array( $one_new_field["options"]["values"] ) ) {

							$new_values = array();
							$did_set_checked_by_default = FALSE;
							foreach ( $one_new_field["options"]["values"] as $one_dropdown_or_radio_value ) {
								if ( ! isset( $one_dropdown_or_radio_value["value"] ) || ! isset( $one_dropdown_or_radio_value["value"] ) ) continue;

								$new_values_key = ( "radiobuttons" === $one_new_field["type"] ) ? "radiobutton_num_"  : "dropdown_num_";
								$new_values_key .=  isset( $one_dropdown_or_radio_value["num"] ) ? (int) $one_dropdown_or_radio_value["num"] : 0;
								$new_values[ $new_values_key ] = array(
									"value" => $one_dropdown_or_radio_value["value"],
									"deleted" => isset( $one_dropdown_or_radio_value["deleted"] ) ? (bool) $one_dropdown_or_radio_value["deleted"] : FALSE
								);
								if ( isset( $one_dropdown_or_radio_value["checked"] ) && TRUE === $one_dropdown_or_radio_value["checked"] ) {
									$new_values["checked_by_default_num"] = $new_values_key;
									$did_set_checked_by_default = TRUE;
								}

							} // foreach

							if ( FALSE === $did_set_checked_by_default ) $new_values["checked_by_default_num"] = NULL;
							foreach ( $arr_merged_options as $one_key => $one_val ) {
								if ( strpos( $one_key, "dropdown_num_" ) !== FALSE || strpos( $one_key, "radiobutton_num_" ) !== FALSE ) {
									unset( $arr_merged_options[ $one_key ] );
								}
							}

							$arr_merged_options = array_merge($arr_merged_options, $new_values);
#echo 111; ef_d($arr_merged_options);

						} // if

					} // if



#echo "<br>arr_merged_options:";
#ef_d($arr_merged_options);
					if ( isset( $one_new_field["type"] ) &&  $ef->field_type_is_core( $one_new_field["type"] ) ) {

						$arr_type_old_school_options = $arr_merged_options;
						if ( isset( $arr_type_old_school_options["values"] ) ) {
							foreach ( $arr_type_old_school_options["values"] as $one_key => $one_val ) {

								if ( is_int( $one_key ) ) {

									$key = ( "radiobuttons" === $one_new_field["type"] ) ? "radiobutton_num_"  : "dropdown_num_";
									$key .= $one_val["num"];
									$arr_type_old_school_options[ $key ] = $one_val;
									unset( $arr_type_old_school_options[ $one_key ] );

								}

							}

							unset( $arr_type_old_school_options["values"] );

						}

						$existing_field_array_from_slug[ "type_" . $one_new_field["type"] . "_options"] = $arr_type_old_school_options;
#echo "<br>arr_type_old_school_options:";ef_d( $arr_type_old_school_options );
					}

				} // if field exists among fields by slugs

			} // foreach field default

		} // for each field in a field grouo

		$merged_fields = $field_groups[$field_group_id]["fields_by_slug"];
		$field_groups[$field_group_id]["fields"] = array();
		foreach( $merged_fields as $one_field_array) {
			$field_groups[$field_group_id]["fields"][$one_field_array["id"]] = $one_field_array;
		}
		$fields_in_new_order = array();
		foreach ( $new_field_group["fields"] as $field_array ) {

			$this_field_id = $field_groups[ $field_group_id ]["fields_by_slug"][ $field_array["slug"] ]["id"];
			$fields_in_new_order[ $this_field_id ] = $field_groups[$field_group_id]["fields"][ $this_field_id ];

		}
		foreach ( $field_groups[ $field_group_id ]["fields"] as $field_array ) {

			if ( ! isset ($fields_in_new_order[ $field_array["id"] ] ) ) {
				$fields_in_new_order[ $field_array["id"] ] = $field_array;
			}

		}
		$field_groups[$field_group_id]["fields"] = $fields_in_new_order;
		$field_groups[$field_group_id]["fields_by_slug"] = array();
		foreach ( $field_groups[$field_group_id]["fields"] as $field_array) {
			$field_groups[$field_group_id]["fields_by_slug"][$field_array["slug"]] = $field_array;
		}

	} // if passed as arg field group has fields
	update_option("simple_fields_groups", $field_groups);
	$ef->clear_caches();
	$field_group_by_slug = $ef->get_field_group_by_slug($slug, true);

	return $field_group_by_slug;

}


function ecophoto_fields_register_post_connector($unique_name = "", $new_post_connector = array()) {
	#ef_d($new_post_connector);
	global $ef;

	$post_connectors = $ef->get_post_connectors();
	$connector_id = NULL;
	$highest_connector_id = 0;

	$is_new_connector = FALSE;
	foreach ($post_connectors as $oneConnector ) {

		if ( $oneConnector["slug"] == $unique_name && !empty( $unique_name) ) {
			$connector_id = $oneConnector["id"];
			break;

		} else if ( ! isset($connector_id) && $oneConnector["id"] > $highest_connector_id ) {
			$highest_connector_id = $oneConnector["id"];

		}

	}
	if ( ! isset($connector_id) || ! is_numeric($connector_id) ) {

		$is_new_connector = TRUE;

		if ( ! empty($post_connectors[$highest_connector_id]) || $highest_connector_id > 0 ) {
			$highest_connector_id++;
		}

		$connector_id = $highest_connector_id;
		if ( $connector_id === 0 ) $connector_id = 1;

	}
	if (empty($unique_name)) {
		$unique_name = "post_connector_" . $connector_id;
	}

	$unique_name = sanitize_key($unique_name);
	if (! isset($new_post_connector["name"]) || empty($new_post_connector["name"])) {
		$new_post_connector["name"] = $unique_name;
	}
	if ( isset($new_post_connector["post_types"]) && ! is_array($new_post_connector["post_types"]) ) {
		$new_post_connector["post_types"] = (array) $new_post_connector["post_types"];
	}
	if ($is_new_connector) {
		$post_connector_defaults = array(
			"id" => $connector_id,
			"key" => $unique_name,
			"slug" => $unique_name,
			"name" => $unique_name."_".$connector_id,
			"field_groups" => array(),
			"post_types" => array(),
			"deleted" => false,
			"hide_editor" => false,
			"added_with_code" => true
		);

	} else {
		$post_connector_defaults = $post_connectors[$connector_id];
		if ( ! isset( $post_connector_defaults["added_with_code"] ) ) $post_connector_defaults["added_with_code"] = true;

	}
	$post_connectors[$connector_id] = ecophoto_fields_merge_arrays($post_connector_defaults, $new_post_connector);
	$post_connectors[$connector_id]['post_types'] = array_unique($post_connectors[$connector_id]['post_types']);
	if (isset($new_post_connector["field_groups"]) && is_array($new_post_connector["field_groups"]) && !empty($new_post_connector["field_groups"])) {
		$field_group_connectors = array();
		$field_group_connector_defaults = array(
							"id" => "",
							"key" => "",
							"deleted" => 0,
							"context" => "normal",
							"priority" => "low"
						);
		foreach ( $new_post_connector["field_groups"] as $field_group_options ) {
			if ( isset( $field_group_options["key"] ) && ! empty( $field_group_options["key"] ) ) {
				$field_group_options["slug"] = $field_group_options["key"];
			} else if ( isset( $field_group_options["slug"] ) && ! empty( $field_group_options["slug"] ) ) {
				$field_group_options["key"] = $field_group_options["slug"];
			}
			$found_field_group = NULL;
			$found_field_group = isset( $field_group_options["id"] ) ? $ef->get_field_group_by_slug($field_group_options["id"]) : NULL;
			$found_field_group = ! isset( $found_field_group ) && isset( $field_group_options["slug"] ) ? $ef->get_field_group_by_slug($field_group_options["slug"]) : NULL;
			if ( isset( $found_field_group ) ) {

				$field_group_id = $found_field_group["id"];
				$field_group_slug = $found_field_group["slug"];
				$field_group_name = $found_field_group["name"];
				$default_field_group_connector = NULL;
				if ( isset( $field_group_connectors[$field_group_id] ) && ! $field_group_connectors[$field_group_id]["deleted"] ) {
					$default_field_group_connector = $field_group_connectors[$field_group_id];
				} else {
					$default_field_group_connector = $field_group_connector_defaults;
				}
				$field_group_connectors[$field_group_id]["id"] = $field_group_id;
				$field_group_connectors[$field_group_id]["slug"] = $field_group_slug;
				$field_group_connectors[$field_group_id]["key"] = $field_group_slug;
				$field_group_connectors[$field_group_id]["name"] = $field_group_name;
				foreach ($default_field_group_connector as $oneGroupConnectorDefaultKey => $oneGroupConnectorDefaultValue) {
					if ( in_array( $oneGroupConnectorDefaultKey, array("id", "slug", "name") ) ) {
						continue;
					}
					if ( isset( $field_group_options[$oneGroupConnectorDefaultKey] ) ) {
						$field_group_connectors[$field_group_id][$oneGroupConnectorDefaultKey] = $field_group_options[$oneGroupConnectorDefaultKey];

					} else {
						$field_group_connectors[$field_group_id][$oneGroupConnectorDefaultKey] = $oneGroupConnectorDefaultValue;

					}

				} // make sure all keys form default values exists

			} // if field group found

		} // foreach field group
		$post_connectors[$connector_id]["field_groups"] = $field_group_connectors;

	} // if field groups was sent along as arg
	$ef->clear_caches();
	update_option("simple_fields_post_connectors", $post_connectors);
	return $ef->get_connector_by_id($connector_id);

}


function ecophoto_fields_register_post_type_default($connector_id_or_special_type = "", $post_type = "post") {

	global $ef;

	if (is_numeric($connector_id_or_special_type)) {

		$connector_id_or_special_type = (int) $connector_id_or_special_type;

	} else if ( in_array($connector_id_or_special_type, array("__inherit__", "__none__") ) ) {

	} else if (!is_numeric($connector_id_or_special_type)) {

		if (empty($connector_id_or_special_type)) {
			return false;
		}
		$post_connectors = $ef->get_post_connectors();
		foreach ($post_connectors as $oneConnector) {
			if ($oneConnector["key"] == $connector_id_or_special_type) {
				$connector_id_or_special_type = $oneConnector["id"];
			}
		}

		if (!is_numeric($connector_id_or_special_type)) {
			return false;
		}

	}

	$post_type_defaults = $ef->get_post_type_defaults();

	$post_type_defaults[$post_type] = $connector_id_or_special_type;
	if (isset($post_type_defaults[0])) {
		unset($post_type_defaults[0]);
	}

	$ef->clear_caches();
	update_option("simple_fields_post_type_defaults", $post_type_defaults);

}



function ecophoto_fields_set_value($post_id, $field_slug, $new_numInSet = null, $new_post_connector = null, $new_value) {


	$set_post_connector_to = null;
	if (empty($saved_connector)) {

		if (empty($new_post_connector)) {
			$set_post_connector_to = $default_connector_to_use;
		} else {
			$set_post_connector_to = $new_post_connector;
		}

		update_post_meta($post->ID, "_simple_fields_selected_connector", $set_post_connector_to);

	}
	$post_connector_info = ecophoto_fields_get_all_fields_and_values_for_post($post_id);
	foreach ($post_connector_info["field_groups"] as $one_field_group) {

		$field_group_id = $one_field_group["id"];
		$meta_key_num_added = $ef->get_meta_key_num_added( $one_field_group["id"], $one_field_group["slug"] );
		$num_added_field_groups = 0;
		while (get_post_meta($post_id, "{$meta_key_num_added}{$num_added_field_groups}", true)) {
			$num_added_field_groups++;
		}
		foreach ($one_field_group["fields"] as $one_field_group_field) {
			if ($one_field_group_field["deleted"]) continue;

			if ($field_slug === $one_field_group_field["slug"]) {
				$field_id = $one_field_group_field["id"];
				if ( is_numeric( $new_numInSet ) ) {
					$num_in_set = $new_numInSet;
				} else {
					$num_in_set = $num_added_field_groups;
				}

				$meta_key = $ef->get_meta_key( $field_group_id, $field_id, $num_in_set, $one_field_group_field["slug"], $one_field_group_field["slug"] );
				update_post_meta($post_id, $meta_key, $new_value);
				update_post_meta($post_id, "{$meta_key_num_added}{$num_in_set}", 1);

				update_post_meta($post_id, "_simple_fields_been_saved", 1);
				$ef->clear_caches();
				return TRUE;

			} // if

		} // foreach

	} // foreach
}



function ecophoto_fields_value($field_slug = NULL, $post_id = NULL, $options = NULL) {

	$values = ecophoto_fields_values($field_slug, $post_id, $options);
	$value = isset($values[0]) ? $values[0] : "";
	$value = apply_filters( "ecophoto_fields_value", $value, $field_slug, $post_id, $options);
	return $value;

}


function ecophoto_fields_values($field_slug = NULL, $post_id = NULL, $options = NULL) {

	if (empty($field_slug)) {
		return FALSE;
	}

	if (is_null($post_id)) {
		$post_id = get_the_ID();
	}

	if (is_a($post_id, "WP_Post")) {
		$post_id = $post_id->ID;
	}
	if (strpos($field_slug, ",") !== FALSE) {

		$arr_comma_slugs_values = array();
		$arr_field_slugs = explode(",", $field_slug);
		if ($arr_field_slugs) {

			foreach ($arr_field_slugs as $one_of_the_comma_separated_slug) {

				$one_of_the_comma_separated_slug = trim($one_of_the_comma_separated_slug);

				$one_slug_values = ecophoto_fields_values($one_of_the_comma_separated_slug, $post_id, $options);
				if (empty($one_slug_values)) continue;

				$loopnum = 0;

				if (!isset($arr_comma_slugs_values[$loopnum]))
					$arr_comma_slugs_values[$loopnum] = array();
				$is_slashed_slug = ( strpos($one_of_the_comma_separated_slug, "/") !== false ) ? true : false;

				if ( $is_slashed_slug ) {
					list($unslashed_group_slug, $unslashed_field_slug) =  explode("/", $one_of_the_comma_separated_slug);
					$slug_for_array = $unslashed_field_slug;
				} else {
					$slug_for_array = $one_of_the_comma_separated_slug;
				}
				foreach ($one_slug_values as $one_slug_sub_value) {

					$arr_comma_slugs_values[$loopnum][$slug_for_array] = $one_slug_sub_value;
					$loopnum++;

				}

			}
		}


		$arr_comma_slugs_values = apply_filters( "ecophoto_fields_values", $arr_comma_slugs_values, $field_slug, $post_id, $options);
		return $arr_comma_slugs_values;

	}
	global $ef;
	$post_connector_info = ecophoto_fields_get_all_fields_and_values_for_post($post_id, "include_deleted=0");

	if ($post_connector_info === FALSE) {
		$return_val = apply_filters( "ecophoto_fields_values", FALSE, $field_slug, $post_id, $options);
		return $return_val;
	}

	$parsed_options = wp_parse_args($options);
	$is_slashed_slug = ( strpos($field_slug, "/") !== false ) ? true : false;
	if ( $is_slashed_slug )
		list($unslashed_group_slug, $unslashed_field_slug) =  explode("/", $field_slug);
	foreach ($post_connector_info["field_groups"] as $one_field_group) {
		if ( $is_slashed_slug && $unslashed_group_slug !== $one_field_group["slug"])
			continue;
		foreach ($one_field_group["fields"] as $one_field_group_field) {
			if ($one_field_group_field["deleted"]) continue;

			if ($field_slug === $one_field_group_field["slug"] || $is_slashed_slug && $unslashed_field_slug === $one_field_group_field["slug"]) {
				$parsed_options_for_this_field = array();
				$field_options_key = "type_".$one_field_group_field["type"]."_options";
				if (isset($one_field_group_field[$field_options_key])) {
					if (isset($one_field_group_field[$field_options_key]["enable_extended_return_values"]) && $one_field_group_field[$field_options_key]["enable_extended_return_values"]) {
						$parsed_options_for_this_field["extended_return"] = 1;
					}

				}
				foreach ($parsed_options as $key => $val) {
					if (!is_array($val)) {
						$parsed_options_for_this_field = array_merge($parsed_options_for_this_field, array($key => $val));
					}
				}
				if ( isset($parsed_options[$one_field_group_field["slug"]]) && is_array($parsed_options[$one_field_group_field["slug"]]) ) {
					$parsed_options_for_this_field = array_merge($parsed_options_for_this_field, $parsed_options[$one_field_group_field["slug"]]);
				}
				$saved_values = isset($one_field_group_field["saved_values"]) ? $one_field_group_field["saved_values"] : NULL;
				if (!sizeof($saved_values)) $saved_values = array();


				if ( isset($ef->registered_field_types[$one_field_group_field["type"]]) && isset($saved_values[0]) ) {
					$custom_field_type = $ef->registered_field_types[$one_field_group_field["type"]];
					$saved_values = $custom_field_type->return_values($saved_values, $parsed_options_for_this_field);

				} else {
					if ( isset($parsed_options_for_this_field["extended_return"]) && (bool) $parsed_options_for_this_field["extended_return"] ) {
						if ( in_array($one_field_group_field["type"], array("file", "radiobuttons", "dropdown", "post", "user", "taxonomy", "taxonomyterm", "date")) ) {

							foreach ($saved_values as $one_saved_value_key => $one_saved_value) {
								$saved_values[$one_saved_value_key] = $ef->get_extended_return_values_for_field($one_field_group_field, $one_saved_value);
							}

						}
					}

				}


				$saved_values = apply_filters("simple-fields-return-values-" . $one_field_group_field["type"], $saved_values);
				$saved_values = apply_filters( "ecophoto_fields_values", $saved_values, $field_slug, $post_id, $options, $one_field_group_field["type"]);

				return $saved_values;

			}
		}
	}

} // simple field values



function ecophoto_fields_connector($post_id = NULL) {

	global $post, $ef;

	if (is_numeric($post_id)) {
		$post_this = get_post($post_id);
	} else {
		$post_this = $post;
	}

	$connector_id = $ef->get_selected_connector_for_post($post_this);

	if ($connector_id == "__none__") {
		$return_val = FALSE;
		$return_val = apply_filters( "ecophoto_fields_connector", $return_val, $post_id);
		return $return_val;

	} else {
		$post_connectors = $ef->get_post_connectors();
		if (!isset($post_connectors[$connector_id])) {
			$return_val = FALSE;
			$return_val = apply_filters( "ecophoto_fields_connector", $return_val, $post_id);
			return $return_val;
		}

		$return_val = $post_connectors[$connector_id]["slug"];
		$return_val = apply_filters( "ecophoto_fields_connector", $return_val, $post_id);
		return $post_connectors[$connector_id]["slug"];

	}
}


function ecophoto_fields_is_connector($slug) {
	$connector_slug = ecophoto_fields_connector();
	return ($connector_slug === $slug);
}


function ecophoto_fields_fieldgroup($field_group_id_or_slug, $post_id = NULL, $options = array()) {

	if (!is_numeric($post_id)) {
		global $post;
		$post_id = $post->ID;
	}

	global $ef;
	$cache_key = "simple_fields_".$ef->ns_key."_fieldgroup_" . $field_group_id_or_slug . "_" . $post_id . "_" . md5(json_encode($options));
	$values = wp_cache_get( $cache_key, 'simple_fields');

	if (FALSE === $values) {

		$field_group = $ef->get_field_group_by_slug($field_group_id_or_slug);

		$arr_fields = array();
		if ( is_array( $field_group["fields"] ) ) {

			foreach ($field_group["fields"] as $one_field) {

				if ($one_field["deleted"]) continue;
				$arr_fields[] = trim($one_field["field_group"]["slug"]) . "/" . trim($one_field["slug"]);

			}

		}

		$str_field_slugs = join(",", $arr_fields);

		if ($field_group["repeatable"]) {
			$values = ecophoto_fields_values($str_field_slugs, $post_id);
		} else {
			$values = ecophoto_fields_value($str_field_slugs, $post_id);
		}

		wp_cache_set( $cache_key, $values, 'simple_fields' );

	}

	$values = apply_filters( "ecophoto_fields_fieldgroup", $values, $field_group_id_or_slug, $post_id, $options);
	return $values;

}


function ecophoto_fields_uasort($a, $b) {
	if ($a["name"] == $b["name"]) { return 0; }
	return strcasecmp($a["name"], $b["name"]);
}
