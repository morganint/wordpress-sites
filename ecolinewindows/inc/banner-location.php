<?php
/**
 * Header - Default
 *
 * @package ecolinewindows
 */

$background_image = (has_post_thumbnail()) ? get_the_post_thumbnail_url($post->ID, 'full') : '/wp-content/uploads/images/default.jpg';
$phone = get_field('phone');
$display_local_address = get_field('display_local_address');
$street_address = get_field('street_address');
$city = get_field('city');
$province = get_field('provice');
$postal = get_field('postal');
$short_description = get_field('short_description');

?>

<script type='application/ld+json'>
  {
    "@context": "http://www.schema.org",
    "@type": "LocalBusiness",
    "name": "<?php the_title(); ?>",
    "url": "<?php the_permalink() ?>",
    "logo": "<?php echo get_stylesheet_directory_uri() . '/images/logo.png'; ?>",
    "image": "<?php echo get_stylesheet_directory_uri() . '/images/preview.jpg'; ?>",
    "description": "<?php echo $short_description ?>",
    "address": {
      "@type": "PostalAddress",
      "streetAddress": "<?php echo $street_address ?>",
      "addressLocality": "<?php echo $city ?>",
      "addressRegion": "<?php echo $province ?>",
      "postalCode": "<?php echo $postal ?>",
      "addressCountry": "Canada"
    },
    "telephone": "<?php echo $phone ?>",
    "priceRange": "$$$"
  }
</script>

<header class="banner banner-overlayed banner-home bg-cover "
        style="background-image: url(<?php echo $background_image; ?>)">
  <div class="container">
    <header class="text-center" id="main-header">

      <h1 class="text-white p-name mb-lg-3"><?php the_title(); ?></h1>
      <h2>
        <span style="text-align:center;font-size:60%;display: block;margin-bottom: 15px;">
          <a class="text-uppercase text-white mt-3"
             href="https://www.ecolinewindows.ca/9-month-deferrals-at-no-cost-to-you/">
            Order Your Windows | Doors Now and Pay in 2021
          </a>
        </span>
      </h2>
    </header>

    <div class="row justify-content-center align-items-center">
      <?php if (!empty($phone)) { ?>
        <div class="col-md-6 col-lg-4 mb-lg-3 d-flex justify-content-center">
          <div class="d-flex align-items-center">
            <div class="p-2 flex-shrink-1">
              <a href="tel:<?php echo $phone ?>">
                  <span class="fa-stack text-2rem">
                    <i class="fa fa-circle text-primary fa-stack-2x"></i>
                    <i class="fa fa-mobile fa-stack-1x fa-inverse"></i>
                  </span>
              </a>
            </div>
            <div class="p-2 w-100">
              <a href="tel:<?php echo $phone; ?>"
                 class="location-phone text-white contact-phone-link">
                <?php echo $phone; ?>
              </a>
            </div>
          </div>
        </div>
      <?php } ?>

      <?php if (!empty($display_local_address)) { ?>
        <div class="col-md-6 col-lg-4 mb-lg-3 d-flex justify-content-center">
          <div class="d-flex align-items-center">
            <div class="p-2 flex-shrink-1">
                <span class="fa-stack text-2rem">
                  <i class="fa fa-circle text-primary fa-stack-2x"></i>
                  <i class="fa fa-map-marker fa-stack-1x fa-inverse"></i>
                </span>
            </div>
            <div class="p-2 w-100">
              <p class="h-adr text-white m-0 p-0">
                  <span class="p-street-address">
                    <?php echo $street_address; ?>
                  </span>
                <br/>
                <span class="p-locality">
                    <?php echo $city; ?>
                  </span>,
                <span><?php echo $province; ?></span>
                <span class="p-country-name">Canada</span>
                <span class="p-postal-code"><?php echo $postal; ?></span>
              </p>
            </div>
          </div>
        </div>
      <?php } ?>
    </div>

    <div class="text-center">
      <div class="list-inline">
        <p class="text-center">
          <a class="btn btn-outline-light bg-green" href="https://www.ecolinewindows.ca/quote/">
            Get a Free Quote
          </a>
        <p class="text-center">
          <img src="/wp-content/uploads/mapleleaf.png"
               alt="Built in Canada"
               width="20"
               height="20"/>
          WE BUILD ALL OUR WINDOWS RIGHT HERE IN CANADA
        </p><!--more-->

      </div>
    </div>
  </div>
</header>