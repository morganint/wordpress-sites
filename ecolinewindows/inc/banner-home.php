<?php
/**
 * Header - Home
 * 
 * @package ecolinewindows
 */
?>

<header class="banner banner-overlayed banner-home" style="background-image: url(<?php (has_post_thumbnail()) ? the_post_thumbnail_url('full') : print('/wp-content/uploads/images/default.jpg'); ?>)">
	<div class="container text-center text-white" id="main-header">
		<h1 class="text-center text-white p-name d-none"><?php echo get_the_title(); ?></h1>
		<?php dynamic_sidebar( 'widget-banner-home' ); ?>
	</div>
</header>