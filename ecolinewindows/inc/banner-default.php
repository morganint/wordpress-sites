<?php
/**
 * Header - Default
 * 
 * @package ecolinewindows
 */
?>

			<header class="banner banner-overlayed bg-cover ">
				<?php (has_post_thumbnail()) ? the_post_thumbnail('full',array('class'=>'flat_thumbnail')) : print('<img class="flat_thumbnail" src="/wp-content/uploads/images/default.jpg">'); ?>
				<div class="eco-overlay"></div>
				<div class="container-fluid">
					<div class="row justify-content-center">
						  <div class="col-lg-10">
							  <header id="main-header">
									<h1 class="text-center text-white p-name"><?php the_title(); ?></h1>								
							  </header>							    
						  </div>
					</div>
				</div>
			</header>
