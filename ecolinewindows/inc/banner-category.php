<?php
/**
 * Header - Category
 * 
 * @package ecolinewindows
 */
?>
			<header class="banner banner-overlayed bg-cover ">
				<?php (has_post_thumbnail()) ? the_post_thumbnail('full',array('class'=>'flat_thumbnail')) : print('<img class="flat_thumbnail" src="/wp-content/uploads/images/default.jpg">'); ?>
				<div class="eco-overlay"></div>
				<div class="container">
					<div class="row justify-content-center">
						  <div class="col-lg-10">
							  <header id="main-header">
									<div class="row">
										<div class="col-sm-6"><h1 class="text-right text-white p-name mt-0 pt-0"><?php single_cat_title(); ?></h1></div>										
										<div class="col-sm-6">
											<form role="search" method="get" class="search-form form  margin-top-1rem" action="<?php echo esc_url(home_url('/')); ?>">
												<label for="form-search-input" class="sr-only"><?php _ex('Search for', 'label', 'ecolinewindows'); ?></label>
												<div class="input-group">
													<input type="search" class="form-control"  id="form-search-input" placeholder="<?php echo esc_attr_x('Search &hellip;', 'placeholder', 'ecolinewindows'); ?>" value="<?php echo esc_attr(get_search_query()); ?>" name="s" title="<?php echo esc_attr_x('Search for:', 'label', 'ecolinewindows'); ?>">
													<span class="input-group-btn">
														<button type="submit" class="btn btn-default"><i class="fa fa-search" aria-hidden="true"></i></button>
													</span>
												</div>
											</form>
										</div>
									</div>
							  </header>
						  </div>
					</div>
				</div>
			</header>