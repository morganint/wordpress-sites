<?php
/**
 * EcolineWindows theme extras
 * Hook functions that replaced core features.
 * 
 * @package ecolinewindows
 */


if (!function_exists('ecolinewindowsCommentReplyLinkClass')) {
    /**
     * modify comment reply link by adding bootstrap button class.
     * 
     * @todo Change comment link class modification to use WordPress hook action/filter when it's available.
     * @param string $class
     * @return string
     */
    function ecolinewindowsCommentReplyLinkClass($class) 
    {
        $class = str_replace("class='comment-reply-link", "class='comment-reply-link btn btn-default btn-sm", $class);
        $class = str_replace("class=\"comment-reply-login", "class=\"comment-reply-login btn btn-default btn-sm", $class);

        return $class;
    }// ecolinewindowsCommentReplyLinkClass
}
add_filter('comment_reply_link', 'ecolinewindowsCommentReplyLinkClass');


if (!function_exists('ecolinewindowsExcerptMore')) {
    function ecolinewindowsExcerptMore($more) 
    {
        return ' &hellip;';
    }// ecolinewindowsExcerptMore
}
add_filter('excerpt_more', 'ecolinewindowsExcerptMore');


if (!function_exists('ecolinewindowsImageSendToEditor')) {
    /**
     * remove rel attachment that is not valid html element
     * @param string $html
     * @param integer $id
     * @return string
     */
    function ecolinewindowsImageSendToEditor($html, $id) 
    {
        if ($id > 0) {
            $html = str_replace('rel="attachment wp-att-'.$id.'"', '', $html);
        }

        return $html;
    }// ecolinewindowsImageSendToEditor
}
add_filter('image_send_to_editor', 'ecolinewindowsImageSendToEditor', 10, 2);


if (!function_exists('ecolinewindowsLinkPagesLink')) {
    /**
     * replace pagination in posts/pages content to support bootstrap pagination class.
     * 
     * @param string $link
     * @param integer $i
     * @return string
     */
    function ecolinewindowsLinkPagesLink($link, $i) 
    {
        if (strpos($link, '<a') === false) {
            return '<li class="active"><a href="#">' . $link . '</a></li>';
        } else {
            return '<li>' . $link . '</li>';
        }
    }// ecolinewindowsLinkPagesLink
}
add_filter('wp_link_pages_link', 'ecolinewindowsLinkPagesLink', 10, 2);


if (!function_exists('ecolinewindowsNavMenuCssClass')) {
    /**
     * Add custom class to nav menu
     * @param array $classes
     * @param object $menu_item
     * @return array
     */
    function ecolinewindowsNavMenuCssClass($classes = array(), $menu_item = false) 
    {
        if (!is_array($menu_item->classes)) {
            return $classes;
        }

        if(in_array('current-menu-item', $menu_item->classes)){
            $classes[] = 'active';
        }

        if (in_array('menu-item-has-children', $menu_item->classes)) {
            $classes[] = 'dropdown';
        }

        if (in_array('sub-menu', $menu_item->classes)) {
            $classes[] = 'dropdown-menu';
        }

        return $classes;
    }// ecolinewindowsNavMenuCssClass
}
add_filter('nav_menu_css_class', 'ecolinewindowsNavMenuCssClass', 10, 2);


if (!function_exists('ecolinewindowsWpTitle')) {
    /**
     * Filters wp_title to print a neat <title> tag based on what is being viewed.
     * 
     * copy from underscore theme.
     * 
     * @deprecated since version 1.0.10 (deprecated since WordPress 4.4)
     */
    function ecolinewindowsWpTitle($title, $sep) 
    {
        global $page, $paged;

        if (is_feed()) {
            return $title;
        }

        // Add the blog name
        $title .= get_bloginfo('name');

        // Add the blog description for the home/front page.
        $site_description = get_bloginfo('description', 'display');
        if ($site_description && (is_home() || is_front_page())) {
            $title .= " $sep $site_description";
        }

        // Add a page number if necessary:
        if ($paged >= 2 || $page >= 2) {
            $title .= " $sep " . sprintf(__('Page %s', 'ecolinewindows'), max($paged, $page));
        }

        return $title;
    }// ecolinewindowsWpTitle
}
add_filter('wp_title', 'ecolinewindowsWpTitle', 10, 2);


if (!function_exists('ecolinewindowsWpTitleSeparator')) {
    /**
     * Replace title separator from its original (-) to the new one (|).<br>
     * The old wp_title() has been deprecated. For more info please read at the link below
     * 
     * @link https://developer.wordpress.org/reference/hooks/wp_title/#comment-1076 wp_title has been deprecated.
     */
    function ecolinewindowsWpTitleSeparator($sep) 
    {
        return '|';
    }// ecolinewindowsWpTitleSeparator
}
add_filter('document_title_separator', 'ecolinewindowsWpTitleSeparator', 10, 1);

