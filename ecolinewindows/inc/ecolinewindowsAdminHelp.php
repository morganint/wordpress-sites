<?php

if (!class_exists('ecolinewindowsAdminHelp')) {
	class ecolinewindowsAdminHelp
	{


		public function themeHelpMenu()
		{
			add_theme_page(__('EcolineWindows help', 'ecolinewindows'), __('EcolineWindows help', 'ecolinewindows'), 'edit_posts', 'theme_help', array($this, 'themeHelpPage'));
		}// themeHelpMenu


		public function themeHelpPage()
		{
			// display page content to view file.
			include get_template_directory() . '/inc/views/ecolinewindowsAdminHelp_v.php';
		}// themeHelpPage


	}// end class -------------------------------------------------------------------------------
}