<?php
/**
 * Header - Blog & Search
 * 
 * @package ecolinewindows
 */
?>
			<header class="banner banner-overlayed bg-cover ">
				<?php (has_post_thumbnail()) ? the_post_thumbnail('full',array('class'=>'flat_thumbnail')) : print('<img class="flat_thumbnail" src="/wp-content/uploads/images/default.jpg">'); ?>
				<div class="eco-overlay"></div>
				<div class="container">
					<div class="row justify-content-center">
						  <div class="col-lg-10">
							  <header id="main-header">

									<div class="row align-items-center">
										<div class="col-sm-3"><h1 class="text-right text-white p-name mt-0 pt-0">Blog</h1></div>										
										<div class="col-sm-9">
											<form role="search" method="get" class="search-form form  margin-top-1rem" action="/blog/">
												<label for="form-search-input" class="sr-only"><?php _ex('Search for', 'label', 'ecolinewindows'); ?></label>
												<div class="input-group">
													<input type="search" class="form-control"  id="form-search-input" placeholder="<?php echo esc_attr_x('Search &hellip;', 'placeholder', 'ecolinewindows'); ?>" value="<?php echo esc_attr(get_search_query()); ?>" name="s" title="<?php echo esc_attr_x('Search for:', 'label', 'ecolinewindows'); ?>">
													<span class="input-group-btn">
														<button type="submit" class="btn btn-default"><i class="fa fa-search text-white" aria-hidden="true"></i></button>
													</span>
												</div>
											</form>
										</div>										
									</div>
									<hr />
									<div class="d-none d-lg-block">
										<div class="row">
											<div class="col-sm-3 text-right text-white"><strong>Categories:</strong></div>
											<div class="col-sm-9">
												<nav class="row list-unstyled" id="menu-categories">
													<?php wp_list_categories(array('hierarchical' => false, 'exclude' => '179, 177, 173 ,176, 175, 178, 174', 'title_li' => '', 'orderby' => 'name')); ?>
												</nav>
											</div>
										</div>
									</div>
								
							  </header>
						  </div>
					</div>
				</div>
			</header>