<?php
/**
 * Template for displaying pages
 * Template Name: Payment Terminal
 * @package ecolinewindows
 */

if(!((!empty($_SERVER['HTTPS']) && $_SERVER['HTTPS'] !== 'off') || $_SERVER['SERVER_PORT'] == 443)) { //is not loaded over https
    header('Location: https://'.$_SERVER['HTTP_HOST'].$_SERVER['REQUEST_URI']);
}

$aid = !empty($_REQUEST['aid'])?intval($_REQUEST['aid']):0;
?>
<!DOCTYPE html>
<html>
<head>
    <!-- SSL verification by Convergine - do not remove -->
    <meta name="_globalsign-domain-verification" content="ZNHOFZQNEO7z2SGp0SBDpCTrzFIb6n2WBsxdvSoKho" />
    <!-- SSL verification by Convergine - do not remove end -->
    <meta charset="<?php bloginfo('charset'); ?>">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <link rel="shortcut icon" href="<?php echo get_stylesheet_directory_uri(); ?>/images/128x128.png" type="image/x-icon">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <?php wp_head(); ?>

    <!-- pinterest snippet -->
    <meta name="p:domain_verify" content="6bbfa28d478f4efcb3f473a43a3e9674"/>

    <!-- Contact7 Thank You Page Redirect  -->
    <?php if(get_field('redirect_url') ){ ?>
        <script>
            document.addEventListener( 'wpcf7mailsent', function( event ) {
                location = 'https://www.ecolinewindows.ca/<?php the_field('redirect_url'); ?>';
            }, false );
        </script>
    <?php } ?>

</head>
<body <?php body_class(); ?> id="<?php (is_front_page()) ? print('page-home') : print('page-internal'); ?>">
<!--[if lt IE 8]>
<div class="alert alert-danger ancient-browser-alert">You are using an <strong>outdated</strong> browser. Please <a href="http://browsehappy.com/" rel="nofollow" target="_blank">upgrade your browser</a>.</div>
<![endif]-->
<header id="header" role="banner">
    <div class="container">
        <nav class="navbar navbar-expand-lg navbar-toggleable-lg" id="navbar-collapse-container" role="navigation">
            <a class="navbar-brand" href="<?php echo esc_url(home_url('/')); ?>"><img class="" src="<?php echo get_stylesheet_directory_uri(); ?>/images/logo.png" alt="Ecoline Windows"></a>
            <a class="d-block d-lg-none" id="btn-menu-cta" href="#" rel="nofollow" data-toggle="modal" data-target="#mobile-menu-cta"><i class="fa fa-phone" aria-hidden="true"></i> Contact us</a>

            <div class="collapse navbar-collapse" id="corporateNavContainer">
                <ul id="menu-corporate" class="nav navbar-nav mr-auto d-none d-xl-flex"></ul>
                <div class="navbar-text">
                    <ul class="navbar-nav" id="nav-action">
                        <li class="nav-item"><a class="nav-link link" href="/contact-us/"><i class="fa fa-mobile" aria-hidden="true"></i> +1 866 644 6418</a></li>
                    </ul>
                </div>
            </div>
        </nav>

    </div>
    <div class="d-none d-lg-block" id="productNavWrapper">
        <div class="container">
            <p style="font-size: 1.25rem;text-transform: uppercase;font-weight: 700;color:#fff;padding:23px 0;cursor:default;margin-bottom:0;text-align:center">Payment for order #<?=$aid?></p>
        </div>
    </div>
</header>
<main role="main" class="h-entry" id="main-wrapper">

	<section class="container margin-top-4rem" style="position: relative">
		<?php while (have_posts()) { ?>
			<?php the_post(); ?>
			<?php the_content(); ?>
		<?php } ?> 
	</section>

<?php get_footer(); ?>