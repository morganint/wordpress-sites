<?php
/**
 * Template for displaying pages
 * Template Name: Order Status
 * @package ecolinewindows
 */
?> 
<?php get_header(); ?>

	<section class="container margin-top-4rem">
		<?php while (have_posts()) { ?>
			<?php the_post();
            $split = explode("?",$_SERVER['REQUEST_URI']);
			$orderId = str_replace('/','',str_replace('order-status','',$split[0]));

			
			ob_start();

?>

			[jsoncontentimporter url="https://portal.projero.pro/api/order-status/<?php echo $orderId?>"]
<h2>Order Status: {status}</h2><br/>
<strong>Customer Name:</strong> {customername}<br/>
<strong>Customer ID:</strong> {aid}<br/>
<strong>City:</strong> {city}<br/>
<script>
var balance = '{balance}';
var financing = '{financing}';
if(balance!=''){document.write('<strong>Balance Due:</strong> ${balance}<br>');}
if(financing!=''){document.write('<strong>Financing:</strong> {financing}<br><br>');}
</script><br/>
<strong>Installation Date: </strong>
{installdate}<br/>
{installtime}<br/>
<div style="color: #ed0000;"><script>
var msg = '{msg}';
if(msg!=''){document.write('<strong>Service Alert:</strong> {msg}<br><br>');}
</script></div>

<script>
   var wo = '{wo}';
   if(wo!='') {
        document.write('<div>' +
            '    <b>Warranty Registration:</b><br>' +
            '    <b>Work Order Number:</b> O-{wo}<br>' +
            '    <b>Warranty registration link:</b> <a href="{warranty_registration_link}">{warranty_registration_link}</a><br>' +
            '    <br>' +
            '    <br>' +
            '</div>');
   }
</script>


<strong>RECEIVE $100 FOR REFERRING US</strong><br/>
If the people you referred end up getting windows with us, we will send you a <strong>cheque for $100</strong>.<br/>
No discounts or gimmicks, just <strong>real Canadian money</strong>.<br/><br/>

You can find out more about our referral program <a href="https://www.ecolinewindows.ca/about-us/#Referral">here</a>.

<!--
<a href="https://www.facebook.com/sharer/sharer.php?u=https://www.ecolinewindows.ca/referral-cert/{uniquehash}" target="_blank">
Share on Facebook
</a>

<a href="mailto:?subject=Referral certificate for Ecoline Windows from {customername}&amp;body=Download Referral Certificate at https://www.ecolinewindows.ca/referral-cert/{uniquehash}" title="Share by Email">
<img src="http://png-2.findicons.com/files/icons/573/must_have/48/mail.png">
</a>
<script>
if( /Android|webOS|iPhone|iPad|iPod|BlackBerry|IEMobile|Opera Mini/i.test(navigator.userAgent) ) {
  document.write('<a href="whatsapp://send?text=Referral certificate for Ecoline Windows from {customername} https://www.ecolinewindows.ca/referral- cert/{uniquehash}" target="_blank">Share on Whatsapp<img src="http://i1.ecolinewindows.ca/uploads/whatsapp2181732.jpeg"></a>');
}
</script>
-->
<div class="order-icons clearfix"><br/><br/>
<div class="left">
<h4>Download Referral Certificate:</h4>
<a href="https://www.ecolinewindows.ca/referral-cert/{uniquehash}" target="_blank" rel="noopener"><img src="https://www.ecolinewindows.ca/wp-content/uploads/images/order-status-icons-referral120.png" /></a>

</div>
<!--
<div class="visible-tp addthis_toolbox addthis_default_style addthis_32x32_style" addthis:url="https://www.ecolinewindows.ca/referral-cert/ {uniquehash}" addthis:title="Referral Certificate - Ecoline Windows">
<strong>Share Your Referral Certificate:</strong>

<a class="addthis_button_facebook"></a>
<a class="addthis_button_messenger"></a>
<a class="addthis_button_whatsapp"></a>
<a class="addthis_button_mailto"></a>
<a class="addthis_button_sms"></a>
<a class="addthis_button_print"></a>
<a class="addthis_button_compact"></a></div>
-->
<div class="right">

<script>
var labour = '{labour}';
var product = '{product}';
var invoice = '{invoice}';
var invoice1 = '{invoice1}';
if(labour!=''||product!=''||invoice!=''){document.write('</p><p></p><h4>Download After Installation Package:</h4><p></p><p>');}
if(labour!=''){document.write('<a href="'+labour+'" target="_blank"><img src="https://www.ecolinewindows.ca/wp-content/uploads/images/order-status-icons-warranty-la120.png" ></a><br>');}
if(product!=''){document.write('<a href="'+product+'" target="_blank"><img src="https://www.ecolinewindows.ca/wp-content/uploads/images/order-status-icons-warranty-pr120.png" ></a><br>');}
if(invoice!=''){document.write('<a href="'+invoice+'" target="_blank"><img src="https://www.ecolinewindows.ca/wp-content/uploads/images/order-status-icons-invoice120n1.png" ></a><br>');}
if(invoice1!=''){document.write('<a href="'+invoice1+'" target="_blank"><img src="https://www.ecolinewindows.ca/wp-content/uploads/images/order-status-icons-invoice120n2.png" ></a><br>');}
</script>

</div>
</div>
<strong>Share Your Referral Certificate:</strong>
<div class="addthis_toolbox addthis_default_style addthis_32x32_style" addthis:url="https://www.ecolinewindows.ca/referral-cert/{uniquehash}" addthis:title="Referral Certificate - Ecoline Windows">
<a class="addthis_button_facebook"></a>
  <a class="addthis_button_messenger"></a>
  <a class="addthis_button_whatsapp"></a>
  <a class="addthis_button_mailto"></a>
  <a class="addthis_button_sms"></a>
  <a class="addthis_button_print"></a>
  <a class="addthis_button_compact"></a>
</div>
[/jsoncontentimporter]
<?php 

$contents = ob_get_contents();
ob_end_clean();

// see the contents now
echo do_shortcode($contents);
the_content();
?>

<?php
		 } ?> 
	</section>
	<script src="https://s7.addthis.com/js/300/addthis_widget.js#async=1" type="text/javascript"></script>
<script type="text/javascript">
    // Call this function once the rest of the document is loaded
    function loadAddThis() {
        addthis.init()
    }
</script>
<?php dynamic_sidebar( 'widget-shadowed' ); ?>
<?php get_sidebar( 'contacts' ); ?>
<?php get_footer(); ?> 