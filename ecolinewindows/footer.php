<?php
/**
 * The theme footer
 *
 * @package ecolinewindows
 */
?>

		</main>

		<footer class="footer-page">
			<div class="container">
				<div class="row justify-content-center">
					<div class="col-md-2 text-center text-lg-left">
						<div class="row justify-content-center">
							<div class="col-6 col-sm">
							   <img src="<?php echo get_stylesheet_directory_uri(); ?>/images/logo.png" class="img-fluid" alt="<?php bloginfo('name'); ?>">
							</div>
						</div>
					   <div class="w-100"><br /></div>
					   <?php dynamic_sidebar( 'footer-smm' ); ?>
					</div>
					<div class="col-5 col-md d-none d-lg-block">
						<?php wp_nav_menu(array('theme_location' => 'about', 'container' => false, 'menu_class' => 'list-unstyled')); ?>
					</div>
					<div class="col d-none d-md-block">
						<?php wp_nav_menu(array('theme_location' => 'windows', 'container' => false, 'menu_class' => 'list-unstyled')); ?>
					</div>
					<div class="col d-none d-md-block">
						<?php wp_nav_menu(array('theme_location' => 'doors', 'container' => false, 'menu_class' => 'list-unstyled')); ?>
					</div>
					<div class="col d-none d-md-block">
						<?php wp_nav_menu(array('theme_location' => 'services', 'container' => false, 'menu_class' => 'list-unstyled')); ?>
					</div>
					<div class="col d-none d-md-block">
						<?php wp_nav_menu(array('theme_location' => 'pricing', 'container' => false, 'menu_class' => 'list-unstyled')); ?>
					</div>
				</div>
			</div>
			<div class="text-white legal">
				<div class="container">
					<div id="legal-wrapper" class="text-center"><span class="inline">Copyright &copy; <?php echo date("Y"); ?> <?php bloginfo('name'); ?> &nbsp;&nbsp;</span><?php wp_nav_menu(array('theme_location' => 'legal', 'container' => false, 'before' => '&nbsp;&nbsp;&nbsp;', 'after' => '', 'menu_class' => 'inline list-inline', 'item_spacing' => 'discard')); ?></div>
				</div>
			</div>
		</footer>

		<?php wp_footer(); ?>
                <script type="text/javascript" src="/wp-content/themes/ecolinewindows/js/jquery.cookie.js"></script>
                <script>
                    jQuery(document).ready(function(){
                        var google_id = jQuery.cookie('_ga');
                        var arr = google_id.split('.');
                        jQuery('body').find('input[name="ga_cid"]').val(arr[2]+'.'+arr[3]);
                    });
                </script>
	</body>
</html>