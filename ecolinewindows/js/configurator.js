(function($){
	var windowHeight = $( window ).height();
	var windowWidth = $( window ).width();
	var filtersOffset = $('.filters').offset().top;

	$('#message-empty').slideUp();

	var $container = $('#grid').isotope({
		itemSelector: '.window-item',
		layoutMode: 'fitRows',
		percentPosition: true,
		fitRows: {
		  gutter: 0
		}
	});

	// filter with selects and checkboxes
	var $selects = $('.filters select');
	var $checkboxes = $('#form-ui input');

	$selects.add( $checkboxes ).change( function() {
	  // map input values to an array
	  var exclusives = [];
	  var inclusives = [];
	  // exclusive filters from selects
	  $selects.each( function( i, elem ) {
		if ( elem.value ) {
		  exclusives.push( elem.value );
		}
	  });
	  // inclusive filters from checkboxes
	  $checkboxes.each( function( i, elem ) {
		// if checkbox, use value if checked
		if ( elem.checked ) {
		  inclusives.push( elem.value );
		}
	  });

	  // combine exclusive and inclusive filters

	  // first combine exclusives
	  exclusives = exclusives.join('');

	  var filterValue;
	  if ( inclusives.length ) {
		// map inclusives with exclusives for
		filterValue = $.map( inclusives, function( value ) {
		  return value + exclusives;
		});
		filterValue = filterValue.join(', ');
	  } else {
		filterValue = exclusives;
	  }

	  $container.isotope({ filter: filterValue })
		if(windowWidth < 992){
			$('body,html').animate({scrollTop: filtersOffset-70}, 400);
		}
	  
		if ( !$container.data('isotope').filteredItems.length ) {
			$('#message-empty').slideDown();				
		}else {
			$('#message-empty').slideUp();
		}
		
		$("#isotope-reset").click(function(){
			$('#grid').isotope({
				filter: '*'
			});
			$('#message-empty').slideUp();
			$('#filters-form').trigger("reset");
			if(windowWidth < 992){
				$('body,html').animate({scrollTop: filtersOffset-70}, 400);
			}
		});
	  
	});
})(jQuery);