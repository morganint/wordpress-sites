(function($) {
    $.isMobile = function(type) {
        var reg = [];
        var any = {
            blackberry: 'BlackBerry',
            android: 'Android',
            windows: 'IEMobile',
            opera: 'Opera Mini',
            ios: 'iPhone|iPad|iPod'
        };
        type = 'undefined' == $.type(type) ? '*' : type.toLowerCase();
        if ('*' == type) reg = $.map(any, function(v) { return v; });
        else if (type in any) reg.push(any[type]);
        return !!(reg.length && navigator.userAgent.match(new RegExp(reg.join('|'), 'i')));
    };

    $.isAppleDevice = function() {
        return (
            (navigator.userAgent.toLowerCase().indexOf("ipad") > -1) ||
            (navigator.userAgent.toLowerCase().indexOf("iphone") > -1) ||
            (navigator.userAgent.toLowerCase().indexOf("ipod") > -1)
        );
    }

    $(function() {

        var windowHeight = $(window).height();

        $('html').addClass($.isMobile() ? 'mobile' : 'desktop');


        if ($.isAppleDevice()) {
            $('.banner').removeClass('bg-cover');
        }


        $(window).resize().scroll();

        // smooth scroll

        $(document).click(function(e) {
            try {
                var target = e.target;

                if ($(target).parents().hasClass('carousel')) {
                    return;
                }
                do {
                    if (target.hash) {
                        var useBody = /#bottom|#top/g.test(target.hash);
                        $(useBody ? 'body' : target.hash).each(function() {
                            e.preventDefault();
                            // in css sticky navbar has height 64px
                            var stickyMenuHeight = $('#fixed-header').length ? 64 : 0;
                            var goTo = target.hash == '#bottom' ?
                                ($(this).height() - $(window).height()) :
                                ($(this).offset().top - stickyMenuHeight);
                            //Disable Accordion's and Tab's scroll
                            if ($(this).hasClass('panel-collapse') || $(this).hasClass('tab-pane')) { return };
                            $('html, body').stop().animate({
                                scrollTop: goTo
                            }, 800, 'easeInOutCubic');
                        });
                        break;
                    }
                } while (target = target.parentNode);
            } catch (e) {
                // throw e;
            }
        });

        // Mobile menu
        if ($(window).width() < 992) {
            $('body').addClass('mobile-site');
            var mobileMenuCorporate = $('#menu-corporate').html();
            var mobileMenuProducts = $('#menu-primary-menu').html();
            var mobileMenuContacts = $('#contacts').html();
            if ($('#footer-cta-wrapper').length) {
                var mobileMenuAction = $('#footer-cta-wrapper').html();
            } else {
                var mobileMenuAction = '';
            }
            $('#header').addClass('fixed-header-mobile');
            $('#contactNavContainer').append(mobileMenuContacts);
            $('#menu-corporate').prepend(mobileMenuProducts);

            $('#btn-menu-trigger').click(function() {
                $("i", this).toggleClass("fa-bars fa-times");
                $('#btn-menu-cta').animate({ opacity: "toggle" });
            });

            $('#btn-menu-cta').click(function() {
                $(this).toggleClass("icon-toggle");
                $("i", this).toggleClass("fa-phone fa-times").toggleClass("fa-lg");
                $("span.btn-label", this).toggle();
                $('#btn-menu-trigger').toggle();
            });


            $('#corporateNavContainer').on('shown.bs.collapse', function() {
                $(this).animate({ height: windowHeight });
                $('#contactNavContainer').collapse('hide');

            })
            $('#contactNavContainer').on('shown.bs.collapse', function() {
                $(this).animate({ height: windowHeight });
                $('#corporateNavContainer').collapse('hide');
            })


        } else {
            $(window).scroll(function() {
                var scrollPosition = $(window).scrollTop();
                var mainPosition = $('#main-wrapper').offset().top;
                var menuProducts = $('#menu-primary-menu').html();
                var menuAction = $('#nav-action').html();
                $('#fixed-header-nav').html(menuProducts);
                $('#fixed-header-nav-action').html(menuAction);
                if (scrollPosition > mainPosition) {
                    $('#fixed-header').fadeIn();
                } else {
                    $('#fixed-header').fadeOut();
                }
            });
            $("#sidebar-fixed").stick_in_parent({ parent: $(".row-fixedbar"), offset_top: 60 });
        }


    });

    $(document).ready(function() {

        //disable animation on scroll on mobiles
        if ($.isMobile()) {
            $('.d-sm-block').find('iframe').attr('src', 'about:blank');
            return;
            //enable animation on scroll
        }
    });


    $('.contact-city h3').each(function() {
        var h3 = $(this);
        var text = h3.text().split(' ');
        text[0] = '<strong class="clearfix">' + text[0] + '</strong>';
        h3.html(text.join(' '));
    });
    $("div.row-locations div.contact-city:odd").after('<div class="clearfix visible-sm-down"></div>');
    $("div.row-locations div.contact-city:nth-child(3)").after('<div class="clearfix visible-md-up"></div>');

    /************ GALLERY BEGIN **********/
    $(".gallery-item a").attr("data-fancybox", "images");
    $(".gallery-item img").addClass("img-fluid img-thumbnail");
    $('.card-link').each(
        function() { $(this).find('.img-gallery-overlay').addClass('fadeOut animated').find('.fa-search').addClass('fadeOut animated faster') }
    );
    $('.card-link').hover(
        function() { $(this).find('.img-gallery-overlay').removeClass('fadeOut').addClass('fadeIn').find('.fa-search').removeClass('fadeOut').addClass('fadeInUp') },
        function() { $(this).find('.img-gallery-overlay').addClass('fadeOut').removeClass('fadeIn').find('.fa-search').addClass('fadeOut').removeClass('fadeInUp') }
    );
    /************ GALLERY END **********/

    $("#blog-body").find("img").addClass('img-fluid');
    $(".pagination").find("a").addClass("page-link");
    $(".pagination span.current, .pagination span.dots").wrap('<a href="#" class="page-link"></a>');



    setTimeout(function() {
        //redirects
        //jQuery('article#post-4160 a').attr('href', 'https://www.ecolinewindows.ca/window-materials-advantages-disadvantages/'); //4160



        if (jQuery('.page-item:first-child .page-link, .page-item .page-link:contains("1")').attr('href') !== undefined &&jQuery('.page-item:first-child .page-link, .page-item .page-link:contains("1")').attr('href').includes('/category/styles/')) {

            let temp = jQuery('body.paged-2 .page-item:first-child .page-link, .page-item .page-link:contains("1")').attr('href').toString().split('/').slice(0, 6).join('/');
            temp += "/";
            jQuery('.page-item .page-link:contains("1")').attr('href', temp);
            if ('body.paged-2') {
                jQuery('body.paged-2 .page-item:first-child .page-link').attr('href', temp)
            }

            if (jQuery('.page-item:first-child .page-link, .page-item .page-link:contains("1")').attr('href').includes('/category/styles/page/')) {
                let temp = jQuery('body.paged-2 .page-item:first-child .page-link, .page-item .page-link:contains("1")').attr('href').toString().split('/').slice(0, 5).join('/');
                temp += "/";
                jQuery('.page-item .page-link:contains("1")').attr('href', temp);
                if (jQuery('body').hasClass('paged-2')) {
                    jQuery('body.paged-2 .page-item:first-child .page-link').attr('href', temp)
                }
            }

        }
        if (jQuery('.page-item .page-link').attr('href') !== undefined && jQuery('.page-item .page-link').attr('href').includes('/?s=')) {
            let temp = jQuery('.page-item .page-link').attr('href').toString().split('/').slice(0, 16).join('/');
            temp += "/";
            jQuery('.page-item .page-link:contains("1")').attr('href', temp);



        } else {
            let temp = jQuery('.page-item:first-child .page-link, .page-item .page-link:contains("1")').attr('href') ? jQuery('.page-item:first-child .page-link, .page-item .page-link:contains("1")').attr('href').toString().split('/').slice(0, 5).join('/') : '';
            temp += "/";
            jQuery('.page-item .page-link:contains("1")').attr('href', temp);
            if ('body.paged-2') {
                jQuery('body.paged-2 .page-item:first-child .page-link').attr('href', temp)
            }
        };


        if (jQuery('body').hasClass('blog') && jQuery('body').hasClass('paged-2')) {
            let temp = jQuery('.page-item:first-child .page-link').attr('href').toString().split('/').slice(0, 4).join('/');
            temp += "/";
            jQuery('.page-item:first-child .page-link').attr('href', temp);

            let temp2 = jQuery('.page-item .page-link:contains("1")').attr('href').toString().split('/').slice(0, 4).join('/');
            temp2 += "/";
            jQuery('.page-item .page-link:contains("1")').attr('href', temp2);
        } else if (jQuery('body').hasClass('blog')) {
            let temp = jQuery('.page-item .page-link:contains("1")').attr('href').toString().split('/').slice(0, 4).join('/');
            temp += "/";
            jQuery('.page-item .page-link:contains("1")').attr('href', temp);

            let temp2 = jQuery('.page-item:first-child .page-link').attr('href').toString().split('/').slice(0, 6).join('/');
            jQuery('.page-item:first-child .page-link').attr('href', temp2);
            temp2 += "/";
        }


    }, 400);


    $('#menu-categories').find('li').addClass('col-sm-4').find('a').addClass('text-white');
    $('.relatedthumb').attr('style', '').find('a').children().attr('style', '');
    $('.yuzo_related_post_widget').css({ 'text-align': 'center', 'max-width': '340px', 'overflow': 'hidden', 'margin': '0' }).find('.relatedthumb').css({ 'width': '100%', 'max-width': '340px', 'height': '170px', 'overflow': 'hidden', 'margin': '0' }).find('.yuzo-img-wrap').css({ 'width': '100%', 'max-width': '340px', 'height': '140px', 'margin-top': '0', 'margin-bottom': '0', 'overflow': 'hidden' }).find('.yuzo-img').css({ 'width': '100%', 'max-width': '340px', 'height': '140px', 'margin-bottom': '0', 'overflow': 'hidden' });
    $('.testimonial_group').addClass('row').find('.full-testimonial').addClass('col-sm-4 mb-2 px-4').find('.rr_review_name').append('<i class="fa fa-user-circle" aria-hidden="true"></i>');

    $('.wpcf7').submit(function() {
        $('.wpcf7-submit').val('Loading, please wait').addClass('disabled').prop('disabled', true);
    });
    document.addEventListener('wpcf7invalid', function(event) {
        $('.wpcf7-submit').val('Send Message').removeClass('disabled').prop('disabled', false);
    }, false);

    //YouTube videos in blogs
    function YouTubeGetID(url) {
        var ID = '';
        url = url.replace(/(>|<)/gi, '').split(/(vi\/|v=|\/v\/|youtu\.be\/|\/embed\/)/);
        if (url[2] !== undefined) {
            ID = url[2].split(/[^0-9a-z_\-]/i);
            ID = ID[0];
        } else {
            ID = url;
        }
        return ID;
    }

    $('#blog-body iframe').each(function() {
        var src = $(this).attr("src");
        var videoid = YouTubeGetID(src);
        $(this).attr("src", "").addClass("d-none");
        $(this).parent().after('<a data-fancybox href="https://www.youtube.com/watch?v=' + videoid + '&amp;autoplay=1&amp;rel=0&amp;controls=0&amp;showinfo=0"><div class="video_thumbnail"><img src="https://img.youtube.com/vi/' + videoid + '/maxresdefault.jpg" class="img-fluid img-thumbnail rounded" width="1280" height="720"><span class="fa fa-youtube-play text-primary text-6rem"></span></div></a>');
    });

    /* Read more/less toogle for <p class="additional-text"> wrapped text */
    $('.additional-text').each(function() {
        $(this).hide().wrap('<div class="additional-text-wrapper"></div>').after('<a class="btn btn-light btn-additional-text">Read more</a>');
    });

    $('.btn-additional-text').click(function() {
        var showhidetext = $(this).closest('.additional-text-wrapper').find('.additional-text');
        var label = showhidetext.is(':visible') ? 'Read more' : 'Show less';
        $(this).text(label);
        showhidetext.toggle('fast');
    });
    /* END Read more/less toogle for <p class="additional-text"> wrapped text */



    $('.slide-container').slick({
        slidesToShow: 1,
        centerMode: true,
        focusOnSelect: true,
        arrows: true,
        dots: true,
        variableWidth: true
    });

    $('.variants').slick({
        dots: false,
        infinite: true,
        mobileFirst: true,
        slidesToShow: 2,
        slidesToScroll: 2,
        responsive: [{
                breakpoint: 768,
                settings: {
                    slidesToShow: 3,
                    slidesToScroll: 3
                }
            },
            {
                breakpoint: 992,
                settings: {
                    slidesToShow: 6,
                    slidesToScroll: 6
                }
            }
        ]
    });


    $.cachedScript = function(url, options) {

        // Allow user to set any option except for dataType, cache, and url
        options = $.extend(options || {}, {
            crossDomain: true,
            dataType: "script",
            cache: true,
            url: url
        });

        // Use $.ajax() since it is more flexible than $.getScript
        // Return the jqXHR object so we can chain callbacks
        return jQuery.ajax(options);
    };

    setTimeout(function() {
        $.cachedScript('//code.jivosite.com/script/widget/' + jivosite_var.widget_id + '?plugin=wp', {});
    }, 10000);

})(jQuery);

document.addEventListener('wpcf7mailsent', function(event) {
    location = 'https://www.ecolinewindows.ca/thank-you/';
}, false);