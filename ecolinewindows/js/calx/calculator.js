(function ($) {
  var wCasementAwning =
    '<div class="img-wrapper"><img class="img-fluid" src="/wp-content/uploads/casement-awning-windows.jpg" alt="Casement &amp; Awning Windows" width="200" height="200"></div><span>Casement & Awning</span>';
  var wCasementFixed =
    '<div class="img-wrapper"><img class="img-fluid" src="/wp-content/uploads/images/casement-awning-windows.jpg" alt="Casement &amp; Awning Windows" width="200" height="200"></div><span>Casement & Fixed</span>';
  var wSliding =
    '<div class="img-wrapper"><img class="img-fluid" src="/wp-content/uploads/images/sliding-windows.jpg" alt="Sliding Windows" width="200" height="200"></div><span>Sliding</span>';
  var wHung =
    '<div class="img-wrapper"><img class="img-fluid" src="/wp-content/uploads/images/hung-window.jpg" alt="Hung Windows" width="200" height="200"></div><span>Hung</span>';
  var wCustom =
    '<div class="img-wrapper"><img class="img-fluid" src="/wp-content/uploads/images/custom-shape-windows.jpg" alt="Custom Shape Windows" width="200" height="200"></div><span>Custom Shape</span>';
  var wFixed =
    '<div class="img-wrapper"><img class="img-fluid" src="/wp-content/uploads/fixed-windows.jpg" alt="Fixed Windows" width="200" height="200"></div><span>Fixed</span>';
  var wBayBow =
    '<div class="img-wrapper"><img class="img-fluid" src="/wp-content/uploads/images/bay-windows.jpg" alt="Bay &amp; Bow Windows" width="200" height="200"></div><span>Bay & Bow</span>';
  var wPatioDoors =
    '<div class="img-wrapper"><img class="img-fluid" src="/wp-content/uploads/images/patio-doors.jpg" alt="Patio Doors" width="200" height="200"></div><span>Patio Doors</span>';
  var wHopper =
    '<div class="img-wrapper"><img class="img-fluid" src="/wp-content/uploads/preview-hopper.png" alt="Hopper" width="200" height="200"></div><span>Hopper</span>';

  $(".matrix-row").removeClass("d-none");

  var tableBase = $("#tablepress-8");
  tableBase.find("thead").remove();

  var maxtableBase = $("#tablepress-13");
  maxtableBase.find("thead").remove();

  $(".tablepress tr").each(function () {
    var table = $(this).closest("table").prop("id");
    $("th.column-1").remove();
    var rowClass = $(this).removeClass("even").removeClass("odd").attr("class");
    var rowNum = rowClass.substr(4);
    rowNum--;
    var cell = $(this).children("td");
    cell.each(function () {
      var cellValue = $(this).text();
      var cellClass = $(this).attr("class");
      if (cellClass == "column-1") {
        $(this).remove();
      }
      var cellLetter = cellClass.substr(7);
      if (cellLetter == 2) {
        cellLetter = "A";
      } else if (cellLetter == 3) {
        cellLetter = "B";
      } else if (cellLetter == 4) {
        cellLetter = "C";
      } else if (cellLetter == 5) {
        cellLetter = "D";
      } else if (cellLetter == 6) {
        cellLetter = "E";
      } else if (cellLetter == 7) {
        cellLetter = "F";
      } else if (cellLetter == 8) {
        cellLetter = "G";
      } else if (cellLetter == 9) {
        cellLetter = "H";
      } else if (cellLetter == 10) {
        cellLetter = "I";
      } else if (cellLetter == 11) {
        cellLetter = "J";
      } else if (cellLetter == 12) {
        cellLetter = "K";
      } else if (cellLetter == 13) {
        cellLetter = "L";
      } else if (cellLetter == 14) {
        cellLetter = "M";
      } else if (cellLetter == 15) {
        cellLetter = "N";
      } else if (cellLetter == 16) {
        cellLetter = "O";
      } else if (cellLetter == 17) {
        cellLetter = "P";
      } else if (cellLetter == 18) {
        cellLetter = "Q";
      } else if (cellLetter == 19) {
        cellLetter = "R";
      }
      var option = " option" + cellLetter;

      if (table == "tablepress-8") {
        $(this)
          .text("")
          .prepend(
            '<input type="radio" name="' +
              rowClass +
              rowNum +
              '" data-class="' +
              rowClass +
              '" data-cell="' +
              cellLetter +
              rowNum +
              '" id="' +
              cellLetter +
              rowNum +
              '" value="' +
              cellValue +
              '" title="' +
              cellValue +
              '" />'
          );
      } else if (
        table == "tablepress-9" ||
        table == "tablepress-11" ||
        table == "tablepress-13" ||
        table == "tablepress-14" ||
        table == "tablepress-17" ||
        table == "tablepress-18" ||
        table == "tablepress-19" ||
        table == "tablepress-20" ||
        table == "tablepress-21"
      ) {
        $(this)
          .text("")
          .prepend(
            '<input type="radio" name="' +
              rowClass +
              rowNum +
              '" class="' +
              rowClass +
              option +
              '" data-cell="' +
              cellLetter +
              rowNum +
              '" data-option="' +
              cellLetter +
              '" id="' +
              cellLetter +
              rowNum +
              '" value="' +
              cellValue +
              '" title="' +
              cellValue +
              '" />'
          );
      } else {
        $(this)
          .text("")
          .prepend(
            '<input type="checkbox" name="' +
              rowClass +
              cellLetter +
              rowNum +
              '" class="' +
              rowClass +
              option +
              '" data-cell="' +
              cellLetter +
              rowNum +
              '" data-option="' +
              cellLetter +
              '" id="' +
              cellLetter +
              rowNum +
              '" value="' +
              cellValue +
              '" title="' +
              cellValue +
              '" />'
          );
      }
    });
  });

  $(".tablepress")
    .addClass("table-details d-none")
    .find("tr")
    .addClass("d-none");

  tableBase
    .removeClass("tablepress d-none")
    .find("tr")
    .removeClass("d-none")
    .find("input")
    .attr("name", "windowGroup");
  maxtableBase
    .removeClass("tablepress d-none")
    .find("tr")
    .removeClass("d-none")
    .find("input")
    .attr("name", "windowGroup");

  $(".tablepress").each(function () {
    var th = $(this).find("th");
    var tr = $(this).find("tr");
    tr.each(function () {
      $(this)
        .find("td")
        .each(function (index) {
          $(this)
            .append(" " + th.eq(index).text() + "")
            .wrapInner('<label class="label-option"></label>');
          var target = $(this).find("input").prop("id");
          //$(this).find('label').attr('for',target)
        });
    });
  });

  $(".tablepress thead").remove();

  tableBase.find("input").click(function () {
    var windowType = $(this).attr("data-class");
    $(
      ".tablepress tr, .subtotal .row-2, .subtotal .row-3, .subtotal .row-4, .subtotal .row-5, .subtotal .row-6, .subtotal .row-7, .subtotal .row-8, .subtotal .row-9, .subtotal .row-10, .total .row-2, .total .row-3, .total .row-4, .total .row-5, .total .row-6, .total .row-7, .total .row-8, .total .row-9, .total .row-10"
    ).addClass("d-none");
    $(
      ".tablepress tr." +
        windowType +
        ", .subtotal ." +
        windowType +
        ", .total ." +
        windowType
    ).removeClass("d-none");
    $(".col-results").removeClass("d-none").addClass("animated slideInRight");
    $(
      "#minfeature, #minframe, #maxbase, #minext, #minloe, #maxroom, #maxfeature, #maxframe, #maxext, #maxloe"
    )
      .find("p")
      .removeClass("active-step")
      .closest("form")
      .find("table")
      .addClass("d-none")
      .closest("form")
      .find("label")
      .removeClass("label-option-selected");
  });

  /* Adding labels to base price table */
  tableBase
    .find("tr.row-2 td")
    .append(wCasementAwning)
    .wrapInner('<label class="window-label label-option"></label>');
  tableBase
    .find("tr.row-3 td")
    .append(wCasementFixed)
    .wrapInner('<label class="window-label label-option"></label>');
  tableBase
    .find("tr.row-4 td")
    .append(wSliding)
    .wrapInner('<label class="window-label label-option"></label>');
  tableBase
    .find("tr.row-5 td")
    .append(wHung)
    .wrapInner('<label class="window-label label-option"></label>');
  tableBase
    .find("tr.row-6 td")
    .append(wCustom)
    .wrapInner('<label class="window-label label-option"></label>');
  tableBase
    .find("tr.row-7 td")
    .append(wFixed)
    .wrapInner('<label class="window-label label-option"></label>');
  tableBase
    .find("tr.row-8 td")
    .append(wBayBow)
    .wrapInner('<label class="window-label label-option"></label>');
  tableBase
    .find("tr.row-9 td")
    .append(wPatioDoors)
    .wrapInner('<label class="window-label label-option"></label>');
  tableBase
    .find("tr.row-10 td")
    .append(wHopper)
    .wrapInner('<label class="window-label label-option"></label>');

  /* Converting base price table layout into Bootstrap div layout */
  tableBase
    .find("label.window-label")
    .unwrap()
    .unwrap()
    .wrap('<div class="col-6 col-sm-3 col-lg-4 col-xl"></div>')
    .closest("tbody")
    .addClass("row");

  /* Adding labels to base price table */
  maxtableBase
    .find("tr.row-2 td")
    .append(wCasementAwning)
    .wrapInner('<label class="window-label label-option"></label>');
  maxtableBase
    .find("tr.row-3 td")
    .append(wCasementFixed)
    .wrapInner('<label class="window-label label-option"></label>');
  maxtableBase
    .find("tr.row-4 td")
    .append(wSliding)
    .wrapInner('<label class="window-label label-option"></label>');
  maxtableBase
    .find("tr.row-5 td")
    .append(wHung)
    .wrapInner('<label class="window-label label-option"></label>');
  maxtableBase
    .find("tr.row-6 td")
    .append(wCustom)
    .wrapInner('<label class="window-label label-option"></label>');
  maxtableBase
    .find("tr.row-7 td")
    .append(wFixed)
    .wrapInner('<label class="window-label label-option"></label>');
  maxtableBase
    .find("tr.row-8 td")
    .append(wBayBow)
    .wrapInner('<label class="window-label label-option"></label>');
  maxtableBase
    .find("tr.row-9 td")
    .append(wPatioDoors)
    .wrapInner('<label class="window-label label-option"></label>');
  maxtableBase
    .find("tr.row-10 td")
    .append(wHopper)
    .wrapInner('<label class="window-label label-option"></label>');

  /* Converting base price table layout into Bootstrap div layout */
  maxtableBase
    .find("label.window-label")
    .unwrap()
    .unwrap()
    .wrap('<div class="col-6 col-sm-4  col-lg-4 col-xl"></div>')
    .closest("tbody")
    .addClass("row");

  /* Converting all options tables layout into Bootstrap div layout */
  $(".tablepress")
    .find("label")
    .unwrap()
    .wrap('<div class="col-6 col-md-4 col-lg-3 col-xl"></div>')
    .closest("tr")
    .addClass("row");

  /* Hiding radios and checkboxes with -1 value (N/A) */

  $("input:checkbox").each(function () {
    var inputvalue = $(this).val();
    if (inputvalue == -1) {
      $(this).closest(".col-6").addClass("d-none");
    }
  });

  /* Adding class to selected radio box in each group */
  $("label:has(input[type=radio])").click(function () {
    $(this).addClass("label-option-selected");
    parentTable = $(this).closest("table");
    parentTable.find("label").not($(this)).removeClass("label-option-selected");
  });

  /* Adding class to selected check boxes */

  $("input:checkbox").change(function () {
    if ($(this).is(":checked")) {
      $(this).parent("label").addClass("label-option-selected");
    } else {
      $(this).parent("label").removeClass("label-option-selected");
    }
  });

  // === @/cerxx work ===
  window.allClasses = {
    step1: "",
    step2: "",
    step2_1: "",
    step3: "",
    step4: "",
    step4old: "",
    A: "",
    B: "",
    C: "",
    D: "",
  };

  //   /*

  $(function () {
    $("#minbase input[type=radio]").each(function () {
      var namespan = $(this).parent("label").find("span").html(),
        namespan0 = namespan.replace(" &amp; ", "-").replace(/\s+/g, "-");
      //namespan1 = namespan0.replace(/\s+/g,'-');
      // allClasses.step1 = namespan0;
      $(this).addClass(namespan0).attr("data-track", namespan0);
    });

		/* add track classes*/
    $("#minbase, #minframe, #minext, #minfeature, #minloe").on("change",function () {
			var baseName = $("#minbase").find("input:checked").attr("class");
			var trackLetter = "", trackName = "";
			/* clear all classes*/
			$("#minframe").find("input").removeClass(allClasses.step2);
			$("#minext").find("input").removeClass(allClasses.step2_1);
			$("#minfeature").find("input").removeClass(allClasses.step3);
			$("#minloe").find("input").removeClass(allClasses.step4);
			$("#calc .btn-primary").removeClass(allClasses.step4);
			window.allClasses = {step1: "", step2: "", step2_1: "", step3: "", step4: "",step4old: "",};
			/* Step 1 */
			allClasses.step1 = baseName;
			/* Step 2 */
			$("#minframe")
				.find("input:checked")
				.each(function () {
					trackLetter = $(this).attr("data-option");
					trackName += "-type" + trackLetter;
				});
			allClasses.step2 = allClasses.step1 + trackName;
			$("#minframe").find("input:checked").addClass(allClasses.step2);
			trackName = "";
			/* Step 2.1 */
			if ($("#minframe input:checked").hasClass("optionA")) {
				allClasses.step2_1 = "";
			} else {
				$("#minext")
					.find("input:checked")
					.each(function () {
						trackLetter = $(this).attr("data-option");
						trackName += "-exterior" + trackLetter;
					});
				allClasses.step2_1 = allClasses.step2 + trackName;
			}
			$("#minext").find("input:checked").addClass(allClasses.step2_1);
			trackName = "";
			/* Step 3 */
			$("#minfeature")
				.find("input:checked")
				.each(function () {
					trackLetter = $(this).attr("data-option");
					trackName += "-option" + trackLetter;
				});
			if ($("#minframe input:checked").hasClass("optionA")) {
				allClasses.step3 = allClasses.step2 + trackName;
			} else {
				allClasses.step3 = allClasses.step2_1 + trackName;
			}
			$("#minfeature").find("input:checked").addClass(allClasses.step3);
			trackName = "";
			/* Step 4 */
			$("#minloe")
				.find("input:checked")
				.each(function () {
					trackLetter = $(this).attr("data-option");
					trackName += "-LoE" + trackLetter;
				});
			allClasses.step4 = allClasses.step3 + trackName;
			$("#minloe").find("input:checked").addClass(allClasses.step4);
			$("#calc .btn-primary").addClass(allClasses.step4);
			trackName = "";
    });
  });

  //  */

  // === */cerxx work ===

  $("input[type=checkbox], input[type=radio]").click(function () {
    var inputName = $(this).prop("name");
    var inputTableMin = $(this).closest("table").prop("id");
    var inputTableMax = "";
    var windowHeight = $(window).height();

    if (inputTableMin == "tablepress-8") {
      /* Step 1 */
      inputTableMax = "#tablepress-13";
      var nextstepoffset = $("#minframe").offset().top;
      $("#tablepress-11")
        .removeClass("d-none")
        .addClass("animated slideInDown");
      $(".label-step-2").addClass("active-step");
      $(
        "#minframe, #maxframe, #minext, #maxext, #minfeature, #maxfeature, #minloe, #maxloe"
      )
        .trigger("reset")
        .find("label")
        .removeClass("label-option-selected");
    } else if (inputTableMin == "tablepress-11") {
      /* Step 2 */
      inputTableMax = "#tablepress-17";
      /* hide step 6 if retrofit is selected */
      if ($(this).hasClass("optionA")) {
        $("#minext").hide();
        var nextstepoffset = $("#minfeature").offset().top;
        $("#tablepress-10")
          .removeClass("d-none")
          .addClass("animated slideInDown");
        $(".label-step-3").addClass("active-step");
        $("#tablepress-12 input, #tablepress-16 input").prop("checked", false);
      } else {
        $("#minext").show();
        var nextstepoffset = $("#minext").offset().top;
        $("#tablepress-18, .label-step-2-1")
          .removeClass("d-none")
          .addClass("animated slideInDown");
        $(".label-step-2-1").addClass("active-step");
      }
    } else if (inputTableMin == "tablepress-18") {
      /* Step 2.1 */
      inputTableMax = "#tablepress-19";
      var nextstepoffset = $("#minext").offset().top;
      $("#tablepress-10")
        .removeClass("d-none")
        .addClass("animated slideInDown");
      $(".label-step-3").addClass("active-step");
    } else if (inputTableMin == "tablepress-10") {
      /* Step 3 */
      inputTableMax = "#tablepress-15";
      var nextstepoffset = $("#minloe").offset().top;
      $("#tablepress-20")
        .removeClass("d-none")
        .addClass("animated slideInDown");
      $(".label-step-4").addClass("active-step");
    } else if (inputTableMin == "tablepress-20") {
      /* Step 4 */
      inputTableMax = "#tablepress-21";
      var nextstepoffset = $("#minloe").offset().top;
      $("#calc .btn-primary")
        .removeClass("d-none")
        .addClass("animated slideInDown");
    }

    $("body,html").animate(
      { scrollTop: nextstepoffset - windowHeight / 2 + 120 },
      400
    );

    var indexA = $(this).index(
      "#" + inputTableMin + " input[name=" + inputName + "]"
    );
    $(inputTableMax + " input[name=" + inputName + "]:eq(" + indexA + ")").prop(
      "checked",
      true
    );
    var indexB = $(
      inputTableMax + " input[name=" + inputName + "]:eq(" + indexA + ")"
    ).val();

    $(
      "#calc, #minbase, #minframe, #minext, #minfeature, #minloe, #maxbase, #maxframe, #maxext, #maxfeature, #maxloe"
    ).calx();
  });

  /* Reset button */
  $("#reset").click(function () {
    $(
      "#minbase, #minframe, #minext, #minfeature, #minloe, #maxbase, #maxframe, #maxext, #maxfeature, #maxloe"
    )
      .trigger("reset")
      .find("label")
      .removeClass("label-option-selected");
    $("#minframe, #minext, #minfeature, #minloe")
      .find("p")
      .removeClass("active-step")
      .closest("form")
      .find("table")
      .addClass("d-none");
    var windowHeight = $(window).height();
    var nextstepoffset = $("#minbase").offset().top;
    $("body,html").animate(
      { scrollTop: nextstepoffset - windowHeight / 2 + 120 },
      400
    );
    $(".col-results")
      .addClass("animated slideOutRight")
      .delay(2000)
      .removeClass("animated slideOutRight slideInRight")
      .addClass("d-none");

    checkAllTracksName();
  });

  $('[data-toggle="popover"]').each(function () {
    var hint = $(this).closest("form").find(".hint").html();
    $(this).popover({
      html: true,
      content: function () {
        return hint;
      },
    });
  });
})(jQuery);
