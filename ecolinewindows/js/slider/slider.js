(function($){
	$('.slide-container').slick({
	   slidesToShow: 1,
	   centerMode: true,
	   focusOnSelect: true,
	   arrows: true,
	   dots: true,
	   variableWidth: true
	});

	$('.variants').slick({
	   dots: false,
	   infinite: true,
	   mobileFirst: true,
	   slidesToShow: 2,
	   slidesToScroll: 2,
	   responsive: [
		  {
			 breakpoint: 768,
			 settings: {
			 slidesToShow: 3,
			 slidesToScroll: 3
			 }
		  },
		  {
			 breakpoint: 992,
			 settings: {
			 slidesToShow: 6,
			 slidesToScroll: 6
			 }
		  }
	   ]
	   });
})(jQuery);