<?php

include "inc/functions.php";



//$token = 'd09307254ce13454ed0cd1';



$form_action = !empty($_REQUEST['form_action'])?$_REQUEST['form_action']:"";



$token = !empty($_REQUEST['token'])?$_REQUEST['token']:"";

$oid = !empty($_REQUEST['oid'])?intval($_REQUEST['oid']):0;

$aid = !empty($_REQUEST['aid'])?intval($_REQUEST['aid']):0;

$customerID = !empty($_REQUEST['customerID'])?intval($_REQUEST['customerID']):0;

$companyID = !empty($_REQUEST['companyID'])?intval($_REQUEST['companyID']):0;

$amount = !empty($_REQUEST['amount'])?round(preg_replace('/[^\d\.]/','',$_REQUEST['amount']),2):0;

$description = !empty($_REQUEST['description'])?$_REQUEST['description']:"Down payment for order {$aid}";

$uploadType = !empty($_REQUEST['uploadType'])?$_REQUEST['uploadType']:'payment';

$generate = !empty($_REQUEST['generate'])?true:false;



//used to link back to emailed quote

$verify = !empty($_REQUEST['verify'])?$_REQUEST['verify']:"";



$response = !empty($_REQUEST['response'])?$_REQUEST['response']:"";

$responseMessage = !empty($_REQUEST['responseMessage'])?$_REQUEST['responseMessage']:"";

$noticeMessage = !empty($_REQUEST['noticeMessage'])?$_REQUEST['noticeMessage']:"";



//saas

$dqID = !empty($_REQUEST['dqID'])?intval($_REQUEST['dqID']):0;

$dq_down_paymentsID = !empty($_REQUEST['dq_down_paymentsID'])?intval($_REQUEST['dq_down_paymentsID']):0;

$update_token = !empty($_REQUEST['update_token'])?intval($_REQUEST['update_token']):0;



$approved = false;

$skip_payment = false;



$error_msg = "";

if($form_action=='do_payment'){

    if($response /*&& $responseMessage=='APPROVED'*/){

        $transactionId = $_REQUEST['transactionId'];

	    $approvalCode = $_REQUEST['approvalCode'];

	    $orderNumber = $_REQUEST['orderNumber'];

        //$customerCode = $_REQUEST['customerCode'];

        $cardNumber = $_REQUEST['cardNumber'];

        $cardType = $_REQUEST['cardType'];

        $approvalDate = $_REQUEST['date'].' '.$_REQUEST['time'];



//        echo '<pre>';

//        var_dump($_REQUEST);

//        echo '</pre>';



        $res = do_saas_call("add_payment/".$oid,array(

            "transactionID"=>$transactionId,

            "customerID"=>$customerID,

            "companyID"=>$companyID,

            "amount"=>$amount,

            "approvalCode"=>$approvalCode,

            "orderNumber"=>$orderNumber,

            "cardNumber"=>$cardNumber,

            "cardType"=>$cardType,

            "approvalDate"=>$approvalDate,

            "aid"=>$aid,

            "dqID"=>$dqID,

            "dq_down_paymentsID"=>$dq_down_paymentsID

        ));



        if($generate) { //send pdf and receipt

            $url = 'https://'.str_replace('www',($dev?'dev3':'apps'),$_SERVER['SERVER_NAME']).'/func/generateWTS.php?nopdf=true&receipt=true';

            $post = ['oid'=>$oid,'upload_type'=>$uploadType,'zz_transactionsID'=>$res['data']['paymentID']];

            $ch = curl_init();

            curl_setopt($ch,CURLOPT_URL, $url);

            curl_setopt($ch,CURLOPT_POST,1);

            curl_setopt($ch,CURLOPT_POSTFIELDS, $post);

            curl_setopt($ch,CURLOPT_RETURNTRANSFER, 1);

            curl_exec($ch);

            curl_close($ch);

        } else { //send receipt

            $url = 'https://'.str_replace('www',($dev?'dev3':'apps'),$_SERVER['SERVER_NAME']).'/func/sendHelcimReceipt.php';

            $post = ['oid'=>$oid,'aid'=>$aid,'upload_type'=>$uploadType,'zz_transactionsID'=>$res['data']['paymentID']];

            $ch = curl_init();

            curl_setopt($ch,CURLOPT_URL, $url);

            curl_setopt($ch,CURLOPT_POST,1);

            curl_setopt($ch,CURLOPT_POSTFIELDS, $post);

            curl_setopt($ch,CURLOPT_RETURNTRANSFER, 1);

            curl_exec($ch);

            curl_close($ch);

        }



        if(!empty($dqID) && !empty($dq_down_paymentsID)) {

            $url = 'https://portal.projero.pro/api/dq/'.$dqID.'/down-payment/'.$dq_down_paymentsID;

            $post = ['status'=>1,'update_token'=>$update_token,'zz_transactionsID'=>$res['data']['paymentID']];

            $ch = curl_init();

            curl_setopt($ch,CURLOPT_URL, $url);

            curl_setopt($ch,CURLOPT_POST,1);

            curl_setopt($ch,CURLOPT_POSTFIELDS, $post);

            curl_setopt($ch,CURLOPT_RETURNTRANSFER, 1);

            curl_exec($ch);

            curl_close($ch);

        }



        $approved = true;

    } else {

	    $error_msg=$responseMessage;

	    if($noticeMessage)$error_msg .= ' ('.$noticeMessage.')';

    }

} else if($form_action=='do_skip_payment') {

    if($generate) { //generate pdf

        $url = 'https://'.str_replace('www',($dev?'dev3':'apps'),$_SERVER['SERVER_NAME']).'/func/generateWTS.php?nopdf=true';

        $post = ['oid'=>$oid];

        $ch = curl_init();

        curl_setopt($ch,CURLOPT_URL, $url);

        curl_setopt($ch,CURLOPT_POST,1);

        curl_setopt($ch,CURLOPT_POSTFIELDS, $post);

        curl_setopt($ch,CURLOPT_RETURNTRANSFER, 1);

        curl_exec($ch);

        curl_close($ch);

    }

    $skip_payment = true;

}



if($approved) {

    ?>

    <h3>Thank-you!</h3>

    <p>Your payment of $<?=number_format($amount,2)?> has been successfully processed. You should receive an email receipt<?=($generate?' along with the signed contract':'')?> shortly.</p>

    <?php if($verify) {

        $url = 'https://'.str_replace('www',($dev?'dev3':'apps'),$_SERVER['SERVER_NAME']).'/sign/?q='.$oid.'&v='.$verify;

        echo '<p>Click <a href="'.$url.'">here</a> to return to your order.</p>';

    } ?>

    <br><br><br>

    <?php

} else if($skip_payment) {

    ?>

    <h3>Payment Skipped</h3>

    <?php if($verify) {

        $url = 'https://'.str_replace('www',($dev?'dev3':'apps'),$_SERVER['SERVER_NAME']).'/sign/?q='.$oid.'&v='.$verify;

        echo '<p>Click <a href="'.$url.'">here</a> to return to your order.</p>';

    } ?>

    <br><br><br>

    <?php

} else {

    ?>

    <style>

        #helcimForm {

            display: block;

            max-width: 400px;

            margin: 0 auto;

        }



        .helcim-div {

            display: block;

            max-width: 400px;

            margin: 0 auto 40px;

        }



        #helcimResults {

            font-size: 12px;

            color: red;

        }



        .helcim-row {

            position: relative;

            margin-bottom: 20px;

        }



        .helcim-row:after {

            content: "";

            display: block;

            clear: both;

        }



        .helcim-row label {

            display: block;

        }



        .helcim-row label.error {

            display: block;

            color: red;

            font-size: 12px;

        }



        .helcim-row input:not([type="submit"]):not([type="button"]) {

            display: block;

            width: 100%;

            background-color: #f5f5f5;

            border: 1px solid #e8e8e8;

            padding: 5px 10px;

        }



        .helcim-row textarea {

            display: block;

            background-color: #f5f5f5;

            border: 1px solid #e8e8e8;

            width: 100%;

            height: 60px;

            padding: 5px 10px;

        }



        .helcim-row input.error,

        .helcim-row textarea.error {

            border-color: #ff0000;;

        }



        .helcim-row input[readonly], .helcim-row textarea[readonly] {

            background-color: #eee;

            cursor: no-drop;

        }



        .helcim-row .helcim-col {

            width: 46%;

            float: left;

        }



        .helcim-row .helcim-col3 {

            width: 28%;

            float: left;

        }



        .helcim-row .helcim-col1 {

            width: 8%;

            float: left;

            text-align: center;

        }



        .helcim-row .helcim-col1-half {

            width: 5%;

            float: left;

            text-align: center;

        }



        .btn-skippayment {

            background: none;

            border: none;

            cursor: pointer;

            text-decoration: underline;

        }

        .ssl-certified {

            position: absolute;

            right: 140px;

            top: 0;

        }

        @media (max-width:1199px) {

            .ssl-certified {

                right: 60px;

            }

        }

        @media (max-width:991px) {

            .ssl-certified {

                position: initial;

                text-align: center;

                margin-bottom: 20px;

            }

        }

    </style>

    <?/*<script type="text/javascript" src="https://secure.myhelcim.com/js/version2.js"></script>*/ ?>

    <script type="text/javascript"

            src="<?php echo get_stylesheet_directory_uri(); ?>/payment_processor/js/helcim.js?v=23<?= time() ?>"></script>

    <script type="text/javascript"

            src="<?php echo get_stylesheet_directory_uri(); ?>/payment_processor/js/jquery.validate.min.js?t=<?= rand(100, 999) ?>"></script>

    <form name="helcimForm" id="helcimForm" action="" method="POST">

        <!--RESULTS-->

        <div id="helcimResults"><?= $error_msg ?></div>



        <!--SETTINGS-->

        <input type="hidden" id="token" value="<?= $token ?>">

        <input type="hidden" id="language" value="en">

        <input type="hidden" id="cardToken" value="1">

        <input type="hidden" id="test" value="0">

        <input type="hidden" name="form_action" value="do_payment">



        <input type="hidden" id="oid" value="<?= $oid ?>">





        <div class="helcim-row">

            <div class="helcim-col">

                <label>Amount</label>

                <input type="number" id="amount" name="amount" value="<?= $amount ?>" placeholder="00.00"

                    <?= !empty($aid)&&$amount>0 ? "readonly" : "" ?>

                       data-rule-required="true"

                       data-msg-required="Type amount to be paid"

                       data-rule-decimal="true"

                       data-msg-decimal="Only decimal accepted"

                >

            </div>

        </div>

        <div class="helcim-row">

            <label>Description</label>

            <textarea name="description" class="required" id="description"

                <?= !empty($d) ? "readonly" : "" ?>

                      data-rule-required="true"

                      data-msg-required="Type what are you payment for"

            ><?= $description ?></textarea>

        </div>



        <!--CARD-INFORMATION-->

        <div class="helcim-row">

            <label>Credit Card Number</label>

            <input type="text" id="cardNumber" value="" placeholder="xxxx xxxx xxxx xxxx" maxlength="16"

                   data-rule-required="true"

                   data-msg-required="Type credit card number"

            >

        </div>

        <div class="helcim-row">

            <div class="helcim-col3">

                <label>Expiry Month</label>

                <input type="text" id="cardExpiryMonth" value="" placeholder="MM" maxlength="2" max="12"

                       data-rule-required="true"

                       data-msg-required="Card expiry month">

            </div>

            <div class="helcim-col1">

                <label>&nbsp;</label>

                /

            </div>

            <div class="helcim-col3">

                <label>Expiry Year</label>

                <input type="text" id="cardExpiryYear" value="" placeholder="YY" maxlength="2"

                       data-rule-required="true"

                       data-msg-required="Card expiry year">

            </div>

            <div class="helcim-col1"><label>&nbsp;</label></div>

            <div class="helcim-col3">

                <label>CVV</label>

                <input type="text" id="cardCVV" value=""

                       data-rule-required="true"

                       data-msg-required="Card CVV">

            </div>

        </div>

    </form>



    <!--BUTTONS-->

    <div class="helcim-div">

        <div class="helcim-row" style="margin-bottom:10px">

            <div class="helcim-col">

                <input type="button" id="buttonProcess" value="Pay" class="btn btn-primary btn-block btn-lg">

            </div>

<!--            <div class="helcim-col1">&nbsp;</div>-->

<!--            <div class="helcim-col">-->

<!--                <form action="" method="POST">-->

<!--                    <input type="hidden" name="form_action" value="do_skip_payment">-->

<!--                    <input type="submit" id="buttonSkipPayment" value="Skip Payment" class="btn btn-primary btn-block btn-lg">-->

<!--                </form>-->

<!--            </div>-->

        </div>

<!--        <div class="helcim-row">-->

<!--            <div class="helcim-col">-->

<!--                <form action="" method="POST" style="text-align:center">-->

<!--                    <input type="hidden" name="form_action" value="do_skip_payment">-->

<!--                    <input type="submit" id="buttonSkipPayment" value="Skip payment" class="btn-skippayment">-->

<!--                </form>-->

<!--            </div>-->

<!--        </div>-->

    </div>



    <div class="ssl-certified">

        <img src="<?=get_stylesheet_directory_uri()?>/payment_processor/inc/ssl.png" />

    </div>



    <script>
document.addEventListener("DOMContentLoaded", function(event) { 
        jQuery(function ($)  {

            $("#helcimForm").validate({

                errorPlacement: function (error, element) {

                    $(element).after(error)

                },

                submitHandler: function (form) {

                    helcimProcess();

                }

            });



            $('#buttonProcess').click(function() {

                $('#helcimForm').submit();

            });

        });
});

    </script>



    <?php

}

//$content = ob_get_contents();

//ob_end_flush();