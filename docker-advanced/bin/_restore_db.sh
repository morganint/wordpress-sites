#!/usr/bin/env bash

set -ex

DB_HOST="$(printenv DB_HOST)"
DB_PORT="$(printenv DB_PORT)"
DB_USER="$(printenv MYSQL_USER)"
DB_PASS="$(printenv MYSQL_PASSWORD)"
IMPORT_DATA_DIR="$(printenv IMPORT_DATA_DIR)"
DB_NAME="${1}"
#IMPORT_FILE="${IMPORT_DATA_DIR}/${DB_NAME}.sql.gz"
IMPORT_FILE="${IMPORT_DATA_DIR}/${DB_NAME}.sql"

[[ ! -f "${IMPORT_FILE}" ]] && {
  echo "Error: ${IMPORT_FILE} file not found."
  exit 2
}

#gzip -cd "${IMPORT_FILE}" | mysql -h "${DB_HOST}" -P "${DB_PORT}" -u "${DB_USER}" --password="${DB_PASS}" "${DB_NAME}"
 
mysql -h "${DB_HOST}" -P "${DB_PORT}" -u "${DB_USER}" --password="${DB_PASS}" "${DB_NAME}" < ${IMPORT_FILE}


