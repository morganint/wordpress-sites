<?php
/**
 * @package Henderson
 * @since Henderson 1.0
 */

  get_header(); 
  
  $post = $wp_query->post;
 
  if (in_category('news')) { //ID категории
      include(TEMPLATEPATH.'/single-news.php');
  } else {
      include(TEMPLATEPATH.'/single-default.php');
  }

  get_footer(); ?>