<?php
/**
 * henderson functions and definitions
 *
 * @link https://developer.wordpress.org/themes/basics/theme-functions/
 *
 * @package henderson
 */
?>
</main>

    <div class="subscribe-popup">
      <div class="subscribe-popup__inner-wrapper">
        <div class="center-wrapper">
          <img class="subscribe-popup__logo-small" src="<?php echo esc_url(get_template_directory_uri()); ?>/img/logo-h-red.svg" alt="">
        </div>

        <b class="subscribe-popup__headline">
          Sign-up to receive updates and exclusive economic development content from the City&nbsp;of&nbsp;Henderson
        </b>

        <!--[if lte IE 8]>
        <script charset="utf-8" type="text/javascript" src="//js.hsforms.net/forms/v2-legacy.js"></script>
        <![endif]-->
        <script charset="utf-8" type="text/javascript" src="//js.hsforms.net/forms/v2.js"></script>
        <script>
          hbspt.forms.create({
          portalId: "4612990",
          formId: "94f4f4fd-1aca-4b99-9192-f22aa28560b0"
        });
        </script>

        <div class="center-wrapper">
          <p class="subscribe-popup__info">
            By clicking subscribe, you agree to periodically recieve communications
            from the City of Henderson.  To manage your subscription, please visit
            our <a href="#">Privacy Policy</a>.
          </p>

          <p class="subscribe-popup__error"></p>
        </div>

        <button class="subscribe-popup__close" title="Close">
          <svg xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" width="512px" version="1.1" height="512px" viewBox="0 0 64 64" enable-background="new 0 0 64 64">
            <g>
              <path fill="#1a1516" d="M28.941,31.786L0.613,60.114c-0.787,0.787-0.787,2.062,0,2.849c0.393,0.394,0.909,0.59,1.424,0.59   c0.516,0,1.031-0.196,1.424-0.59l28.541-28.541l28.541,28.541c0.394,0.394,0.909,0.59,1.424,0.59c0.515,0,1.031-0.196,1.424-0.59   c0.787-0.787,0.787-2.062,0-2.849L35.064,31.786L63.41,3.438c0.787-0.787,0.787-2.062,0-2.849c-0.787-0.786-2.062-0.786-2.848,0   L32.003,29.15L3.441,0.59c-0.787-0.786-2.061-0.786-2.848,0c-0.787,0.787-0.787,2.062,0,2.849L28.941,31.786z"/>
            </g>
          </svg>
        </button>

        <img class="subscribe-popup__logo" src="<?php echo esc_url(get_template_directory_uri()); ?>/img/logo-h-popup.svg" alt="">
      </div>
    </div>

    <footer class="page-footer" data-aos="fade-in" data-aos-delay="200">

    <?php if (have_rows('footer', 'option')): the_row();?>

			      <div class="page-footer__top">

			        <?php
    if (have_rows('navigation')):
        while (have_rows('navigation')): the_row();
            ?>

									          <div class="page-footer__col">
									            <b class="page-footer__col-title">Navigation</b>
									            <div class="page-footer__nav-grid">

									              <?php if (have_rows('navigation_left_col')): ?>

									                <div class="page-footer__nav-col">
									                  <ul class="page-footer__list">

									                  <?php while (have_rows('navigation_left_col')): the_row();?>
												                    <?php $link = get_sub_field('navigation_link');?>

												                    <li class="page-footer__list-item">
												                      <a href="<?php echo $link['url']; ?>" title="<?php echo $link['title']; ?>" class="page-footer__link"><?php echo $link['title']; ?></a>
												                    </li>

												                  <?php endwhile;?>

									                  </ul>
									                </div>

									              <?php endif;?>

						              <?php if (have_rows('navigation_right_col')): ?>

						                <div class="page-footer__nav-col">
						                  <ul class="page-footer__list">

						                  <?php while (have_rows('navigation_right_col')): the_row();?>
									                    <?php $link = get_sub_field('navigation_link');?>

									                    <li class="page-footer__list-item">
									                      <a href="<?php echo $link['url']; ?>" title="<?php echo $link['title']; ?>" class="page-footer__link"><?php echo $link['title']; ?></a>
									                    </li>

									                  <?php endwhile;?>

						                  </ul>
						                </div>

						              <?php endif;?>
			            </div>

			        </div>
			        <?php endwhile;?>
        <?php endif;?>
        <?php if (get_sub_field('address')): ?>

          <div class="page-footer__col">
            <b class="page-footer__col-title">Address</b>
            <ul class="page-footer__list">
              <?php while (has_sub_field('address')): ?>
                    <?php $row = get_sub_field('row');?>

                    <li class="page-footer__list-item">
                      <?php echo $row; ?>
                    </li>

              <?php endwhile;?>
            </ul>
          </div>
        <?php endif;?>

        <?php if (get_sub_field('socials_link')): ?>
          <div class="page-footer__col">
            <b class="page-footer__col-title">Stay connected</b>
            <?php $social = get_sub_field('socials_link');?>
            <ul class="social-list">
              <li class="social-list__item">
                <a href="<?php echo $social['facebook']; ?>" class="social-list__link social-list__link--fb" title="Facebook">Facebook</a>
              </li>
              <li class="social-list__item">
                <a href="<?php echo $social['twitter']; ?>" class="social-list__link social-list__link--tw" title="Twitter">Twitter</a>
              </li>
              <li class="social-list__item">
                <a href="<?php echo $social['linkedin']; ?>" class="social-list__link social-list__link--in" title="LinkedIn">LinkedIn</a>
              </li>
            </ul>

            <button class="btn btn--subscribe btn--red">Subscribe to our newsletter</button>

          <!-- div class="subscribe">
            <p class="subscribe__text">Subscribe to our newsletter</p>

            <div class="subscribe__input-wrapper">
              <input class="subscribe__input"  id="email-footer" name="email" type="text" autocomplete="off">
              <label class="subscribe__label" for="email-footer">Email address</label>
              <span class="validation-icon" title="Clear">
                <svg class="cross" xmlns="http://www.w3.org/2000/svg" width="212.982" height="212.982" viewBox="0 0 212.982 212.982">
                  <path d="M131.8,106.49l75.936-75.936A17.9,17.9,0,0,0,182.424,5.242L106.487,81.179,30.55,5.241A17.9,17.9,0,0,0,5.238,30.553l75.937,75.936L5.238,182.426A17.9,17.9,0,0,0,30.55,207.738L106.487,131.8l75.937,75.937a17.9,17.9,0,0,0,25.312-25.312L131.8,106.49Z" transform="translate(0.004 0.002)"/>
                </svg>
              </span>
              <span class="validation-icon" title="Submit">
                <svg class="check" enable-background="new 0 0 426.67 426.67" version="1.1" viewBox="0 0 426.67 426.67" xml:space="preserve" xmlns="http://www.w3.org/2000/svg">
                  <path d="m153.5 366.84c-8.657 0-17.323-3.302-23.927-9.911l-119.66-119.66c-13.218-13.218-13.218-34.645 0-47.863s34.645-13.218 47.863 0l95.727 95.727 215.39-215.39c13.218-13.214 34.65-13.218 47.859 0 13.222 13.218 13.222 34.65 0 47.863l-239.32 239.32c-6.609 6.605-15.271 9.911-23.932 9.911z" />
                </svg>
              </span>
            </div>


            <form action="https://hendersonecdev.us18.list-manage.com/subscribe/post?u=233920ccb15ea65d26754bced&amp;id=8e47e024c6"
              method="post" id="mc-embedded-subscribe-form" name="mc-embedded-subscribe-form" class="mce-form validate footer__colum__form"
              target="_blank" novalidate autocomplete="off">

              <div class="subscribe__input-wrapper">
                <input class="subscribe__input"  id="email" name="EMAIL" type="text">
                <label class="subscribe__label" for="email">Email address</label>
                <span class="validation-icon" title="Clear">
                  <svg class="cross" xmlns="http://www.w3.org/2000/svg" width="212.982" height="212.982" viewBox="0 0 212.982 212.982">
                    <path d="M131.8,106.49l75.936-75.936A17.9,17.9,0,0,0,182.424,5.242L106.487,81.179,30.55,5.241A17.9,17.9,0,0,0,5.238,30.553l75.937,75.936L5.238,182.426A17.9,17.9,0,0,0,30.55,207.738L106.487,131.8l75.937,75.937a17.9,17.9,0,0,0,25.312-25.312L131.8,106.49Z" transform="translate(0.004 0.002)"/>
                  </svg>
                </span>
                <span class="validation-icon" title="Submit">
                  <svg class="check" enable-background="new 0 0 426.67 426.67" version="1.1" viewBox="0 0 426.67 426.67" xml:space="preserve" xmlns="http://www.w3.org/2000/svg">
                    <path d="m153.5 366.84c-8.657 0-17.323-3.302-23.927-9.911l-119.66-119.66c-13.218-13.218-13.218-34.645 0-47.863s34.645-13.218 47.863 0l95.727 95.727 215.39-215.39c13.218-13.214 34.65-13.218 47.859 0 13.222 13.218 13.222 34.65 0 47.863l-239.32 239.32c-6.609 6.605-15.271 9.911-23.932 9.911z" />
                  </svg>
                </span>

                <button type="submit" id="mc-embedded-subscribe" class="subscribe__submit"></button>
              </div>

              <div id="mce-responses" class="clear">
                <div class="response" id="mce-error-response" style="display:none"></div>
                <div class="response" id="mce-success-response" style="display:none"></div>
              </div>
              <div style="position: absolute; left: -5000px;" aria-hidden="true">
                <input type="text" name="b_233920ccb15ea65d26754bced_8e47e024c6" tabindex="-1" value="">
              </div>
            </form>
          </div -->
        </div>

        <?php endif;?>

        <?php if (get_sub_field('logo')): ?>
          <a href="/" class="page-footer__logo">
            <img src="<?php the_sub_field('logo');?> " alt="Henderson logo">
          </a>
        <?php endif;?>
      </div>

      <?php endif;?>

      <div class="container">
        <p class="page-footer__copyright">© <?php the_field('copyright', 'option');?></p>
      </div>
    </footer>
  <?php wp_footer();?>
  <!-- <script src="<?php echo esc_url(get_template_directory_uri()); ?>/js/libs.min.js"></script>
  <script src="<?php echo esc_url(get_template_directory_uri()); ?>/js/main.js"></script> -->
</body>

</html>