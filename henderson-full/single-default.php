<?php 



$goLink = get_field('go_link'); 



if ($goLink) { ?>

    <script type="text/javascript">

        window.location.replace('<?php echo $goLink; ?>');

    </script>

<?php } ?>



<?php the_post(); ?>



<div class="news-article">

    <a data-aos="fade-in" data-aos-delay="1000" href="#" onclick="history.back();" class="news-article__back-link">Back</a>

    <h1 class="news-article__title" data-aos="flip-up" data-aos-delay="200" ><?php  the_title(); ?></h1>

    <span class="news-article__date" data-aos="flip-up" data-aos-delay="500" ><?php the_date('F j, Y'); ?></span>

    <div class="news-article__divider" data-aos="fade-in" data-aos-delay="800"></div>

    <div class="news-article__img" data-aos="fade-up" data-aos-duration="400" data-aos-delay="1000">

        <?php the_post_thumbnail('thumb_x2'); ?>

    </div>

    <div class="news-article__text" data-aos="fade-up" data-aos-duration="400"><?php the_content(); ?></div>

    <div data-aos="fade-in">
        <a href="#" onclick="history.back();" class="news-article__back-link">Back</a>
    </div>

</div>

