<?php
/**
 *
 * @package Henderson
 * @since Henderson 1.0
 */

get_header(); ?>

  <section class="section" id="media-resources">
    <div class="container">
      <h2 class="section__title" data-aos="flip-up">Press Releases</h2>
      <div class="filter" data-aos="flip-up">
        <form action="#" class="filter__form">
          <label class="filter__label" for="news-year">Filter by</label>
          <select name="year" id="news-year" class="filter__select">
            <option value="0">All Years</option>
              <?php 
                $years = get_posts_years_array();
                  foreach ($years as $year) {
                    echo '<option value="' . $year . '">' . $year . '</option>';
                  }
              ?>
          </select>
        </form>
                <script>
                    jQuery( document ).ready(function() {
                        console.log('ajax script ready');
                        jQuery('#news-year').on('change', function() {
                            var year = this.value;
                            var category = 4;
                            var ajaxurl = '<?php echo admin_url('admin-ajax.php'); ?>';

                            $.ajax({

                                url : ajaxurl,
                                type: 'post',
                                data : {
                                    year : year,
                                    category: category,
                                    action : 'ajax_year_filter'
                                },
                                error : function(response){
                                    console.log(response);
                                },
                                success : function(response){
                                    $('#news-container').html('');
                                    $('#news-container').append( response );

                                }

                            });
                        });
                    });
                </script>
      </div>
      <div class="section__divider" data-aos="fade-in"></div>
      <div class="news">
        <div  id="news-container" class="news__grid news__grid--slider news-container-anchor">
        <?php 
          $args = array( 
            'posts_per_page' => 99999,
            'category__in' => array(4),
            'orderby'     => 'date',
            'order'       => 'DESC',
            'post_type'   => 'post'
            );
          $posts = get_posts( $args );

          $delay = 0;

          foreach( $posts as $post ){ 
            $delay += 150;
            setup_postdata($post);
            $image = get_the_post_thumbnail_url($post->ID, 'thumb_414'); ?>

          <div class="news__item-wrapper"
            data-aos="fade-up"
            data-aos-duration="400" data-aos-delay="<?php echo $delay; ?>"
          >
            <div class="news__item news__item--with-img news__item--press">
              <div class="news__item-img" style="background-image: url(<?php echo $image; ?>)"></div>
              <div class="news__text-block">
                <span class="news__date"><?php the_date(); ?></span>

                <?php if(get_field('write_post')) { ?>

                    <a href="<?php the_permalink(); ?>" class="news__link">

                <?php } else { ?>

                    <a href="<?php the_field('go_link'); ?>"  target="_blank" class="news__link">

                <?php } ?>
                    <h3 class="news__title"><?php the_title(); ?></h3>
                </a>
                <div class="news__text"><?php the_excerpt(); ?></div>

              </div>
            </div>
          </div>
        <?php } wp_reset_postdata(); ?>

        </div>
        <div class="paginator" data-aos="flip-up">
          <button class="paginator__prev hidden">prev</button>
          <div class="paginator__nums-container"></div>
          <button class="paginator__next">next</button>
        </div>
      </div>
    </div>
  </section>
  <?php if ( have_rows('repeat') ) : ?>

    <section class="section" id="downloads">
      <div class="container">
        <h2 class="section__title" data-aos="flip-up">Downloads</h2>
        <div class="section__divider" data-aos="fade-in"></div>
        <div class="downloads">
          <div class="grid equal-height">

            <?php $delay = 0; while ( have_rows('repeat') ) : the_row(); 
              $delay += 150;
              $image = get_sub_field('image');
              $content = get_sub_field('content');
            ?>

              <div class="col-1-of-2 col-sm">
                <div class="downloads__item" data-aos-anchor=".downloads" data-aos="fade-up" data-aos-duration="400" data-aos-delay="<?php echo $delay; ?>">
                  <div class="text-with-img text-with-img--grey text-with-img--i34">
                    <div class="text-with-img__img" style="background-image: url(<?php echo $image; ?>)"></div>
                    <div class="text-with-img__text-block">
                      <b class="text-with-img__title"><?php echo $content['title']; ?></b>
                      <p class="text-with-img__text"><?php echo $content['text']; ?></p>
                      <a href="<?php echo $content['link']; ?>" class="text-with-img__link">Coming soon</a>
                    </div>
                  </div>
                </div>
              </div>

            <?php endwhile; ?>

          </div>
        </div>
      </div>
    </section>

  <?php endif; ?>

  <?php get_footer(); ?>