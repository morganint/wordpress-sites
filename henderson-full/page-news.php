<?php

/**

 *

 * @package Henderson

 * @since Henderson 1.0

 */



get_header(); ?>



    <section class="section" id="news-releases">

        <div class="container">

            <h2 class="section__title" data-aos="flip-up">In the News</h2>

            <div class="section__divider" data-aos="fade-in"></div>

            <div class="news">

                <div class="news__top">

                    <?php 

                        $post = get_field( 'headline_news' );

                        setup_postdata($post);

                        $image = get_the_post_thumbnail_url($post->ID, 'thumb_768'); 

                    ?>



                    <div class="news__item news__item--main" data-aos="fade-right" data-aos-duration="400">

                        <div class="news__item-img" style="background-image: url(<?php echo $image; ?>)"></div>

                        <div class="news__text-block">

                            <span class="news__date"><?php the_date(); ?></span>

                            <a href="<?php the_permalink(); ?>" class="news__link">

                                <h3 class="news__title"><?php the_title(); ?></h3>

                            </a>

                            <div class="news__text"><?php the_excerpt(); ?></div>

                            <a href="<?php the_permalink(); ?>" class="news__arrow"></a>

                        </div>

                        <div class="news__headline-badge">Headline news</div>

                    </div>



                    <?php 

                    

                    wp_reset_postdata(); ?>



                    <div class="news__popular">

                        <p class="news__popular-headline" data-aos="fade-left" data-aos-duration="400">Popular news</p>

                    

                        <?php get_most_viewed("num=4"); ?>



                    </div>



                </div>

            </div>

        </div>

    </section>

    <section class="section section--grey" id="announcements">

        <div class="container">

            <h2 class="section__title" data-aos="flip-up">Events & Announcements</h2>

            <div class="filter" data-aos="flip-up">

                <form action="#" class="filter__form">

                    <label class="filter__label" for="news-year">Filter by</label>

                    <select name="events-year" id="events-year" class="filter__select">

                        <option value="0">All Years</option>

                        <?php 

                            $years = get_posts_years_array();

                            foreach ($years as $year) {

                                echo '<option value="' . $year . '">' . $year . '</option>';

                            }

                        ?>

                    </select>

                </form>

                <script>

                    jQuery( document ).ready(function() {

                        console.log('ajax script ready');

                        jQuery('#events-year').on('change', function() {

                            var year = this.value;

                            var category = 3;

                            var ajaxurl = '<?php echo admin_url('admin-ajax.php'); ?>';



                            $.ajax({



                                url : ajaxurl,

                                type: 'post',

                                data : {

                                    year : year,

                                    category: category,

                                    action : 'ajax_year_filter'

                                },

                                error : function(response){

                                    console.log(response);

                                },

                                success : function(response){

                                    $('#events-container').slick('unslick').html('').append( response ).trigger('contentChanged');

                                }



                            });

                        });

                    });

                </script>

            </div>

            <div class="section__divider" data-aos="fade-in"></div>

            <div class="news news--grey">

                <div id="events-container" class="news__grid news__grid--slider events-container-anchor">

                <?php 

                    $args = array( 

                        'category__in' => array(3)

                     );

                    $posts = get_posts( $args );

                    $delay = 0;

                    foreach( $posts as $post ) :

                        $delay += 150;

                        setup_postdata($post);

                        $image = get_the_post_thumbnail_url($post->ID, 'thumb_500');

                        ?>                    

                    <div class="news__item-wrapper"
                        data-aos-anchor=".events-container-anchor" data-aos="fade-up"
                        data-aos-duration="400" data-aos-delay="<?php echo $delay; ?>"
                    >

                        <div class="news__item news__item--with-img">

                            <div class="news__item-img" style="background-image: url(<?php echo $image; ?>)"></div>

                            <div class="news__text-block">

                                <span class="news__date"><?php the_date(); ?></span>



                                    <a href="<?php the_permalink(); ?>" class="news__link">                                        

                                        <h3 class="news__title"><?php the_title(); ?></h3>

                                    </a>



                                    <div class="news__text"><?php the_excerpt(); ?></div>



                                    <a href="<?php the_permalink(); ?>" class="news__arrow"></a>

                            </div>

                        </div>

                    </div>

                    <?php endforeach; ?>

                </div>

                <div class="paginator" data-aos="flip-up">

                    <button class="paginator__prev hidden">prev</button>

                    <div class="paginator__nums-container"></div>

                    <button class="paginator__next">next</button>

                </div>

            </div>

        </div>

    </section>

    <section class="section" id="news-archive">

        <div class="container">

            <h2 class="section__title" data-aos="flip-up">News Archives</h2>

            <div class="filter" data-aos="flip-up">

                <form id="arhive_filter" action="" class="filter__form">

                    <label class="filter__label" for="news-year">Filter by</label>

                    <select name="year" id="archive-news-year" class="filter__select">

                        <option value="0">All Years</option>

                        <?php 

                            $years = get_posts_years_array();

                            foreach ($years as $year) {

                                echo '<option value="' . $year . '">' . $year . '</option>';

                            }

                        ?>

                    </select>

                </form>

                <script>

                    jQuery( document ).ready(function() {

                        console.log('ajax script ready');

                        jQuery('#archive-news-year').on('change', function() {

                            var year = this.value;

                            var category = 2;

                            var ajaxurl = '<?php echo admin_url('admin-ajax.php'); ?>';



                            $.ajax({



                                url : ajaxurl,

                                type: 'post',

                                data : {

                                    year : year,

                                    category: category,

                                    action : 'ajax_year_filter'

                                },

                                error : function(response){

                                    console.log(response);

                                },

                                success : function(response){

                                    $('#archive-container').slick('unslick').html('').append( response ).trigger('contentChanged');

                                }

                            });

                        });

                    });

                </script>

            </div>

            <div class="section__divider" data-aos="fade-in"></div>

            <div class="news">

                <div id="archive-container" class="news__grid news__grid--slider archive-container-anchor">

                <?php 

                    $args = array( 

                        'posts_per_page' => 999,

                        'category__in' => array(2),

                        'orderby'     => 'date',

                        'order'       => 'DESC',

                        'post_type'   => 'post'

                     );

                    $posts = get_posts( $args );

                    
                    $delay = 0;

                    foreach( $posts as $post ){ 

                        $delay += 150;

                        setup_postdata($post);

                        $image = get_the_post_thumbnail_url($post->ID, 'thumb_414');

                        ?>

                        <div class="news__item-wrapper"
                            data-aos-anchor=".archive-container-anchor" data-aos="fade-up"
                            data-aos-duration="400" data-aos-delay="<?php echo $delay; ?>"
                        >

                            <div class="news__item news__item--with-img">

                                <div class="news__item-img" style="background-image: url(<?php echo $image; ?>)"></div>

                                <div class="news__text-block">

                                    <span class="news__date"><?php the_date(); ?></span>



                                    <a href="<?php the_permalink(); ?>" class="news__link">

                                        <h3 class="news__title"><?php the_title(); ?></h3>

                                    </a>



                                    <div class="news__text"><?php the_excerpt(); ?></div>



                                    <a href="<?php the_permalink(); ?>" class="news__arrow"></a>

                                </div>

                            </div>

                        </div>

                        <?php 

                    }

                    wp_reset_postdata();

                    

                    ?>

                </div>

                <div class="paginator" data-aos="flip-up">

                    <button class="paginator__prev hidden">prev</button>

                    <div class="paginator__nums-container"></div>

                    <button class="paginator__next">next</button>

                </div>

            </div>

        </div>

    </section>



    <?php get_footer(); ?>