

<?php

/**

 * Template Name: Doing Business Here

 * Template Post Type: page

 *

 * @package Henderson

 * @since Henderson 1.0

 */

get_header();?>



    <?php if (get_field('hero_image')): ?>

    <div class="page-img parallax-window">
      <img src="<?php the_field('hero_image');?>">
    </div>

    <?php endif;?>

    <?php if (have_rows('page_menu')): ?>

        <div class="slick-nav-spacer">

            <nav class="slick-nav" data-aos="fade-in" data-aos-delay="200" data-aos-offset="-500">

                <ul class="slick-nav__list">

                <?php

                $i = 1;

                while (have_rows('page_menu')): the_row();

                    $title = get_sub_field('menu_title');

                    ?>

	                        <li class="slick-nav__item">

	                        <a href="#section-<?php echo $i; ?>" class="slick-nav__anchor" title="<?php echo $title; ?>"><?php echo $title; ?></a>

	                        </li>

	                    <?php

                            $i++;

                        endwhile;?>

                </ul>

            </nav>

        </div>



    <?php endif;?>

    <section class="section" id="section-1">

        <div class="container">

            <?php

                $first_section = get_field('first_section');

                if ($first_section): ?>

                <h2 class="section__title" data-aos="flip-up" data-aos-delay="200"><?php echo $first_section['title']; ?></h2>

                <div class="section__divider"></div>

                <div class="section__text" data-aos="fade-up" data-aos-delay="200"><?php echo $first_section['section_content']; ?></div>


            <?php endif;?>

            <?php

                $images = get_field('second_section');

                if ($images): ?>



                <h4 class="section__title section__title--small" data-aos="flip-up" data-aos-delay="200" id="partners"><?php the_field('partners_title');?></h4>

                <div class="section__divider"></div>

                <div class="partners">

                    <?php $delay = 0;foreach ($images as $image): $delay += 100;?>

	                        <div class="partners__item" data-aos="fade-up" data-aos-duration="400" data-aos-delay="<?php echo $delay; ?>">

	                            <div class="partners__img" style="background-image: url(<?php echo $image['sizes']['thumb_500']; ?>)"></div>

	                            <b class="partners__title"><?php echo $image['title']; ?></b>

	                        </div>

	                    <?php endforeach;?>

                </div>

            <?php endif;?>

        </div>

    </section>

    <?php if (have_rows('third_section')): ?>

        <?php $third_section = get_field('third_section')?>

        <section class="section" id="section-2">

            <div class="container">

                <h4 data-aos="flip-up" data-aos-delay="200" class="section__title section__title--small">Talking About Business in Henderson</h4>

                    <div class="section__divider"></div>

                    <div class="talking-about" data-aos="fade-up" data-aos-duration="400" data-aos-delay="200">

                        <ul class="talking-about__slider">

                            <?php while (have_rows('third_section')): the_row();?>

	                                <?php $content = get_sub_field('content');?>



	                                <li class="talking-about__item">

	                                    <div class="talking-about__item-wrap">

	                                        <div class="talking-about__person-pic" style="background-image: url(<?php the_sub_field('image');?>); background-size: contain;"></div>

	                                        <div class="talking-about__text-block">

	                                            <p class="talking-about__person-name"><?php echo $content['title']; ?></p>

	                                            <p class="talking-about__person-post"><?php echo $content['posotion']; ?></p>

	                                            <p class="talking-about__person-business-name">

	                                                <b><?php echo $content['company']; ?></b>

	                                            </p>

	                                            <p class="talking-about__person-text"><?php echo $content['text']; ?></p>

	                                        </div>

	                                    </div>

	                                </li>



	                            <?php endwhile;?>

                        </ul>

                        <span class="slider-navs__next"></span>

                        <span class="slider-navs__prev"></span>

                    </div>

            </div>

        </section>

    <?php endif;?>

    <?php if (have_rows('fouth_section')): ?>

        <section class="section section--grey" id="section-3">

            <div class="container">

                <h2 class="section__title" data-aos="flip-up" data-aos-delay="200">Key Industries</h2>

                <div class="section__divider"></div>

                <div class="key-industries">

                <?php $delay = 0; while (have_rows('fouth_section')): the_row();

                    $delay += 100; 
                    $content = get_sub_field('content');

                    $file = $content['file'];

                    if ($file) {?>

	                        <a href="<?php echo $file; ?>" class="key-industries__item key-industries__item--hover" data-aos="fade-up" data-aos-duration="400" data-aos-delay="<?php echo $delay; ?>">

	                    <?php } else {?>

	                        <div class="key-industries__item" data-aos="fade-up" data-aos-duration="400" data-aos-delay="<?php echo $delay; ?>">

	                    <?php }?>

	                        <div class="key-industries__img" style="background-image: url(<?php the_sub_field('image');?>)"></div>

	                        <div class="key-industries__text-block">

	                            <h5 class="key-industries__title"><?php echo $content['title']; ?></h5>

	                            <p class="key-industries__text"><?php echo $content['text']; ?></p>



	                            <?php if ($file): ?>

	                                <span class="key-industries__download">Download profile</span>

	                            <?php endif;?>



                        </div>

                    <?php

if ($file) {?>

                        </a>

                    <?php } else {?>

                        </div>

                    <?php }?>

                <?php endwhile;?>



                </div>
                <div data-aos="fade-up" data-aos-delay="300">
                    <a href="/contact/" class="btn btn--lifestyle">Contact for more info</a>
                </div>

            </div>

        </section>



    <?php endif;?>



    <?php if (have_rows('fifth_section')): ?>



        <section class="section pb0" id="section-4">

            <div class="container">

                <h2 class="section__title" data-aos="flip-up">Workforce</h2>

                <div class="section__divider"></div>

                <div class="chess-list">



                <?php while (have_rows('fifth_section')): the_row();?>



	                    <div class="chess-list__item">

	                        <div data-aos="fade-up" data-aos-duration="400" data-aos-delay="200" class="chess-list__pic" style="background-image: url(<?php the_sub_field('image');?>)"></div>

	                        <div data-aos="fade-up" data-aos-duration="400" data-aos-delay="300" class="chess-list__text-block">

	                            <div class="chess-list__text"><?php the_sub_field('text');?></div>

	                        </div>

	                    </div>



	                <?php endwhile;?>



                </div>

            </div>

        </section>



    <?php endif;?>



    <?php if (have_rows('sixth_section')): ?>



        <section class="section section pb0" id="section-5">

            <div class="container">

                <h2 class="section__title" data-aos="flip-up">Demographics</h2>

                <div class="section__divider"></div>

                <div class="chess-list">



                <?php while (have_rows('sixth_section')): the_row();?>



	                    <div class="chess-list__item">

	                        <div data-aos="fade-up" data-aos-duration="400" data-aos-delay="200" class="chess-list__pic" style="background-image: url(<?php the_sub_field('image');?>)"></div>

	                        <div data-aos="fade-up" data-aos-duration="400" data-aos-delay="300" class="chess-list__text-block">

	                            <div class="chess-list__text"><?php the_sub_field('text');?></div>

	                        </div>

	                    </div>



	                <?php endwhile;?>



                </div>

            </div>

        </section>



    <?php endif;?>



    <?php if (have_rows('demographics_table')):

    $content = get_field('demographics_table');

    $image = $content['image'];?>



	        <section class="section section--no-padd">

	            <div class="container">

	            <h2 class="section__title" data-aos="flip-up"><?php echo $content['title']; ?></h2>

	                <div class="section__divider"></div>

	                <div class="demographic-block" data-aos="fade-up" data-aos-duration="400">

	                    <img class="demographic-block__img" src="<?php echo $image['url']; ?>" alt="<?php echo $imagep['alt']; ?>">

	                    <a href="<?php echo $content['link']; ?>"

	                        class="demographic-block__link">See full report here</a>

	                </div>

	            </div>

	        </section>



	    <?php endif;?>



    <?php if (have_rows('seventh_section')):

    $content = get_field('seventh_section');

    $dataExplorer = $content['data_explorer'];?>



	        <section class="section" id="section-6">

	            <div class="container">

	                <h2 class="section__title" data-aos="flip-up"><?php echo $content['title']; ?></h2>

	                <div class="section__divider"></div>

	                <div class="section__text" data-aos="flip-up"><?php echo $content['content']; ?></div>

                    <div data-aos="fade-up" data-aos-duration="400">

	                <a href="<?php echo $dataExplorer['link']; ?>">

	                    <img src="<?php echo $dataExplorer['image']['url']; ?>" alt="<?php echo $dataExplorer['image']['alt']; ?>">

	                </a>
                    </div>


	            </div>

	        </section>



	    <?php endif;?>



    <?php get_footer();?>