<?php
/**
 *
 * @package Henderson
 * @since Henderson 1.0
 */

get_header(); ?>

  <div class="site-map">
    <ul class="site-map__list">
    <?php 
    
      $args = array(
        'sort_order'   => 'ASC',
        'sort_column'  => 'post_title',
        'hierarchical' => 1,
        'exclude'      => '',
        'include'      => '',
        'meta_key'     => '',
        'meta_value'   => '',
        'authors'      => '',
        'child_of'     => 0,
        'parent'       => -1,
        'exclude_tree' => '',
        'number'       => '',
        'offset'       => 0,
        'post_type'    => 'page',
        'post_status'  => 'publish',
      ); 
      $pages = get_pages( $args );
      foreach( $pages as $post ){
        setup_postdata( $post ); ?>
        <li class="site-map__list-item site-map__list-item--top">
          <a href="<?php the_permalink(); ?>" class="site-map__link site-map__link--top"><?php the_title(); ?></a>
          
          <?php if (have_rows('page_menu')) : ?>

            <ul class="site-map__list site-map__list--lower">

              <?php $i = 1; while ( have_rows('page_menu') ) : the_row();?>

                <li class="site-map__list-item">
                  <a href="<?php the_permalink(); ?>#section-<?php echo $i; ?>" class="site-map__link"><?php the_sub_field( 'menu_title' ); ?></a>
                </li>

              <?php $i++; endwhile; ?>

            </ul>

          <?php endif; ?>
        
        </li>
      <?php
      }  
      wp_reset_postdata(); 
    ?>
    <!-- <ul class="site-map__list">
      <li class="site-map__list-item site-map__list-item--top">
        <a href="/" class="site-map__link site-map__link--top">Home</a>
      </li>
      <li class="site-map__list-item site-map__list-item--top">
        <a href="available-properties.html" class="site-map__link site-map__link--top">Available Properties</a>
        <ul class="site-map__list site-map__list--lower">
          <li class="site-map__list-item">
            <a href="available-properties.html#office-space" class="site-map__link">Office Space - Market Overview</a>
            <ul class="site-map__list site-map__list--lower">
              <li class="site-map__list-item">
                <a href="available-properties.html#office-space" class="site-map__link">The District at Green Valley Ranch</a>
              </li>
            </ul>
          </li>
          <li class="site-map__list-item">
            <a href="available-properties.html#industrial-space" class="site-map__link">Industrial Space - Market Overview</a>
            <ul class="site-map__list site-map__list--lower">
              <li class="site-map__list-item">
                <a href="available-properties.html#industrial-space" class="site-map__link">Black Mountain Industrial Center</a>
              </li>
            </ul>
          </li>
          <li class="site-map__list-item">
            <a href="available-properties.html#search-business" class="site-map__link">Search for a business site</a>
            <ul class="site-map__list site-map__list--lower">
              <li class="site-map__list-item">
                <a href="available-properties.html#search-business" class="site-map__link">Interactive map</a>
              </li>
            </ul>
          </li>
        </ul>
      </li>
      <li class="site-map__list-item site-map__list-item--top">
        <a href="doing-business-here.html" class="site-map__link site-map__link--top">Who’s Here</a>
        <ul class="site-map__list site-map__list--lower">
          <li class="site-map__list-item">
            <a href="doing-business-here.html#pro-business" class="site-map__link">Henderson is pro-business</a>
          </li>
          <li class="site-map__list-item">
            <a href="doing-business-here.html#partners" class="site-map__link">Major Businesses (pick top 6 to showcase)</a>
          </li>
          <li class="site-map__list-item">
            <a href="doing-business-here.html#other-says" class="site-map__link">What others are saying about business in Henderson</a>
            <ul class="site-map__list site-map__list--lower">
              <li class="site-map__list-item">
                <a href="doing-business-here.html#other-says" class="site-map__link">Economic Development Strategy</a>
              </li>
              <li class="site-map__list-item">
                <a href="doing-business-here.html#other-says" class="site-map__link">2014 Kosmont-rose Cost of Doing Business Survey</a>
              </li>
              <li class="site-map__list-item">
                <a href="doing-business-here.html#other-says" class="site-map__link">The Tax Foundation (2015 Survey)</a>
              </li>
              <li class="site-map__list-item">
                <a href="doing-business-here.html#other-says" class="site-map__link">CEO Magazine</a>
              </li>
            </ul>
          </li>
          <li class="site-map__list-item">
            <a href="doing-business-here.html#key-industries" class="site-map__link">Key Industries</a>
            <ul class="site-map__list site-map__list--lower">
              <li class="site-map__list-item">
                <a href="doing-business-here.html#key-industries" class="site-map__link">Financial Services</a>
              </li>
              <li class="site-map__list-item">
                <a href="doing-business-here.html#key-industries" class="site-map__link">Healthcare, Medical & Life</a>
              </li>
              <li class="site-map__list-item">
                <a href="doing-business-here.html#key-industries" class="site-map__link">Information & Technology Services</a>
              </li>
              <li class="site-map__list-item">
                <a href="doing-business-here.html#key-industries" class="site-map__link">Software</a>
              </li>
              <li class="site-map__list-item">
                <a href="doing-business-here.html#key-industries" class="site-map__link">Manufacturing & Logistics</a>
              </li>
              <li class="site-map__list-item">
                <a href="doing-business-here.html#key-industries" class="site-map__link">Hospitality, Tourism & Retail</a>
              </li>
              <li class="site-map__list-item">
                <a href="doing-business-here.html#key-industries" class="site-map__link">Education</a>
              </li>
            </ul>
          </li>
        </ul>
      </li>
      <li class="site-map__list-item site-map__list-item--top">
        <a href="taxes-and-incentives.html" class="site-map__link site-map__link--top">Taxes & Incentives</a>
        <ul class="site-map__list site-map__list--lower">
          <li class="site-map__list-item">
            <a href="taxes-and-incentives.html#taxes" class="site-map__link">Taxes are less in Henderson</a>
          </li>
          <li class="site-map__list-item">
            <a href="taxes-and-incentives.html#no-taxes-on" class="site-map__link">No taxes on:</a>
          </li>
          <li class="site-map__list-item">
            <a href="taxes-and-incentives.html#advantages" class="site-map__link">Other Advantages</a>
            <ul class="site-map__list site-map__list--lower">
              <li class="site-map__list-item">
                <a href="taxes-and-incentives.html#advantages" class="site-map__link">Workers Compensation</a>
              </li>
              <li class="site-map__list-item">
                <a href="taxes-and-incentives.html#advantages" class="site-map__link">Easy access to the airport</a>
              </li>
              <li class="site-map__list-item">
                <a href="taxes-and-incentives.html#advantages" class="site-map__link">Major business markets</a>
              </li>
              <li class="site-map__list-item">
                <a href="taxes-and-incentives.html#advantages" class="site-map__link">Property tax rates</a>
              </li>
              <li class="site-map__list-item">
                <a href="taxes-and-incentives.html#advantages" class="site-map__link">Global exposure</a>
              </li>
              <li class="site-map__list-item">
                <a href="taxes-and-incentives.html#advantages" class="site-map__link">Shipping advantages</a>
              </li>
              <li class="site-map__list-item">
                <a href="taxes-and-incentives.html#advantages" class="site-map__link">Development Services Center</a>
              </li>
              <li class="site-map__list-item">
                <a href="taxes-and-incentives.html#advantages" class="site-map__link">Redevelopment programs</a>
              </li>
              <li class="site-map__list-item">
                <a href="taxes-and-incentives.html#advantages" class="site-map__link">Nevada GOED</a>
              </li>
              <li class="site-map__list-item">
                <a href="taxes-and-incentives.html#advantages" class="site-map__link">Public Improvement Trust</a>
              </li>
            </ul>
          </li>
        </ul>
      </li>
      <li class="site-map__list-item site-map__list-item--top">
        <a href="living-here.html" class="site-map__link site-map__link--top">Living Here</a>
        <ul class="site-map__list site-map__list--lower">
          <li class="site-map__list-item">
            <a href="living-here.html#lifestyle" class="site-map__link">Lifestyle</a>
          </li>
          <li class="site-map__list-item">
            <a href="living-here.html#lifestyle" class="site-map__link">Image gallery slideshow</a>
          </li>
          <li class="site-map__list-item">
            <a href="living-here.html#water-street" class="site-map__link">Water Street District</a>
          </li>
          <li class="site-map__list-item">
            <a href="living-here.html#location" class="site-map__link">Location & Transportation</a>
          </li>
          <li class="site-map__list-item">
            <a href="living-here.html#communities" class="site-map__link">Location hub for Henderson</a>
            <ul class="site-map__list site-map__list--lower">
              <li class="site-map__list-item">
                <a href="living-here.html#communities" class="site-map__link">By Air</a>
              </li>
              <li class="site-map__list-item">
                <a href="living-here.html#communities" class="site-map__link">By Land</a>
              </li>
            </ul>
          </li>
          <li class="site-map__list-item">
            <a href="living-here.html#recreation" class="site-map__link">Recreation & Entertainment</a>
          </li>
          <li class="site-map__list-item">
            <a href="living-here.html#workforce" class="site-map__link">Workforce & Demographics</a>
          </li>
          <li class="site-map__list-item">
            <a href="living-here.html#education" class="site-map__link">Education System</a>
          </li>
          <li class="site-map__list-item">
            <a href="living-here.html" class="site-map__link">Data Explorer</a>
            <ul class="site-map__list site-map__list--lower">
              <li class="site-map__list-item">
                <a href="living-here.html" class="site-map__link">Interactive chart for City of Henderson and Clark County</a>
              </li>
              <li class="site-map__list-item">
                <a href="living-here.html" class="site-map__link">Current Economic Update Report</a>
              </li>
            </ul>
          </li>
        </ul>
      </li>
      <li class="site-map__list-item site-map__list-item--top">
        <a href="news.html" class="site-map__link site-map__link--top">News</a>
        <ul class="site-map__list site-map__list--lower">
          <li class="site-map__list-item">
            <a href="news.html#in-the-news" class="site-map__link">News Releases</a>
          </li>
          <li class="site-map__list-item">
            <a href="news.html#in-the-news" class="site-map__link">In the News</a>
            <ul class="site-map__list site-map__list--lower">
              <li class="site-map__list-item">
                <a href="news.html#in-the-news" class="site-map__link">Henderson Newsletter</a>
              </li>
              <li class="site-map__list-item">
                <a href="news.html#announcements" class="site-map__link">Announcements</a>
              </li>
            </ul>
          </li>
          <li class="site-map__list-item">
            <a href="news.html#news-archive" class="site-map__link">News Archives</a>
          </li>
        </ul>
      </li>
      <li class="site-map__list-item site-map__list-item--top">
        <a href="contact.html" class="site-map__link site-map__link--top">Contact</a>
      </li>
      <li class="site-map__list-item site-map__list-item--top">
        <a href="start-tank.html" class="site-map__link site-map__link--top">Start Tank</a>
      </li>
      <li class="site-map__list-item site-map__list-item--top">
        <a href="press-room.html#downloads" class="site-map__link site-map__link--top">Brand Assets</a>
      </li>
      <li class="site-map__list-item site-map__list-item--top">
        <a href="press-room.html" class="site-map__link site-map__link--top">Press Room</a>
        <ul class="site-map__list site-map__list--lower">
          <li class="site-map__list-item">
            <a href="press-room.html#media-resources" class="site-map__link">Regional Media Resources</a>
            <ul class="site-map__list site-map__list--lower">
              <li class="site-map__list-item">
                <a href="press-room.html#media-resources" class="site-map__link">Las Vegas Review-Journal</a>
              </li>
              <li class="site-map__list-item">
                <a href="press-room.html#media-resources" class="site-map__link">Las Vegas Sun</a>
              </li>
              <li class="site-map__list-item">
                <a href="press-room.html#media-resources" class="site-map__link">Vegas, Inc</a>
              </li>
              <li class="site-map__list-item">
                <a href="press-room.html#media-resources" class="site-map__link">Las Vegas Business Press</a>
              </li>
              <li class="site-map__list-item">
                <a href="press-room.html#media-resources" class="site-map__link">Nevada Business Magazine</a>
              </li>
            </ul>
          </li>
        </ul>
      </li>
      <li class="site-map__list-item site-map__list-item--top">
        <a href="additional.html" class="site-map__link site-map__link--top">Additional Resources</a>
        <ul class="site-map__list site-map__list--lower">
          <li class="site-map__list-item">
            <a href="additional.html#business-resources" class="site-map__link">Business Resources</a>
            <ul class="site-map__list site-map__list--lower">
              <li class="site-map__list-item">
                <a href="#" class="site-map__link">Henderson Chamber of Commerce</a>
              </li>
              <li class="site-map__list-item">
                <a href="#" class="site-map__link">Small Business Development Center</a>
              </li>
              <li class="site-map__list-item">
                <a href="#" class="site-map__link">Service Corps of Retired Executives</a>
              </li>
              <li class="site-map__list-item">
                <a href="#" class="site-map__link">Henderson Business Resource Center</a>
              </li>
              <li class="site-map__list-item">
                <a href="#" class="site-map__link">Nevada JobConnect</a>
              </li>
              <li class="site-map__list-item">
                <a href="#" class="site-map__link">Silver State Works</a>
              </li>
              <li class="site-map__list-item">
                <a href="#" class="site-map__link">Nevada Industry Excellence</a>
              </li>
              <li class="site-map__list-item">
                <a href="#" class="site-map__link">Water Street Rall-e</a>
              </li>
              <li class="site-map__list-item">
                <a href="#" class="site-map__link">Entrepreneurs Assembly</a>
              </li>
            </ul>
          </li>
          <li class="site-map__list-item">
            <a href="additional.html#community-resources" class="site-map__link">Community Resources</a>
            <ul class="site-map__list site-map__list--lower">
              <li class="site-map__list-item">
                <a href="#" class="site-map__link">Clark County School District</a>
              </li>
              <li class="site-map__list-item">
                <a href="#" class="site-map__link">Clark County</a>
              </li>
              <li class="site-map__list-item">
                <a href="#" class="site-map__link">Henderson Libraries</a>
              </li>
              <li class="site-map__list-item">
                <a href="#" class="site-map__link">Henderson Strong</a>
              </li>
              <li class="site-map__list-item">
                <a href="#" class="site-map__link">Water Street District in Downtown Henderson</a>
              </li>
              <li class="site-map__list-item">
                <a href="#" class="site-map__link">VisitHenderson.com</a>
              </li>
            </ul>
          </li>
        </ul>
      </li>
    </ul> -->
  </div>
  
  <?php get_footer(); ?>