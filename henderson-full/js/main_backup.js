var OSName = '';

if (window.navigator.userAgent.indexOf('Win') != -1) OSName = 'win';

if (window.navigator.userAgent.indexOf('Mac') != -1) OSName = 'mac';




//  END SELECT IN SUBSCRIBE FORM

$(document).ready(function() {
  $('html').addClass(OSName);

  $('html').on('customLoaded', function() {
    AOS.init({
      once: true,
      duration: 800,
      delay: 200,
      offset: 300,
      easing: 'ease-out'
    });
  });

  function removePreloader() {
    $('#preloader').fadeOut(300, function() {
      $(this).remove();
    });

    $('html').trigger('customLoaded');
  }

  if ($('.page-img img').length) {
    $('.page-img img')
      .one('load', function() {
        $(this)
          .parent()
          .parallax({ imageSrc: $(this).prop('src') });
        $(this).remove();

        removePreloader();
      })
      .each(function() {
        if (this.complete) $(this).load();
      });
  } else if ($('.promo .promo__img').length) {
    $('.promo .promo__img')
      .one('load', function() {
        var height = $(this).height();
        $(this)
          .parent()
          .height(height)
          .parallax({
            imageSrc: $(this).prop('currentSrc'),
            speed: 0.4
          });
        $(this).remove();

        removePreloader();
      })
      .each(function() {
        if (this.complete) $(this).load();
      });
  } else {
    setTimeout(removePreloader, 1500);
  }

  jQuery.fn.shake = function(interval, distance, times) {
    interval = typeof interval == 'undefined' ? 100 : interval;
    distance = typeof distance == 'undefined' ? 10 : distance;
    times = typeof times == 'undefined' ? 3 : times;
    var jTarget = $(this);
    jTarget.css('position', 'relative');
    for (var iter = 0; iter < times + 1; iter++) {
      jTarget.animate(
        { left: iter % 2 == 0 ? distance : distance * -1 },
        interval
      );
    }
    return jTarget.animate({ left: 0 }, interval);
  };

  // +++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
  // +++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
  // +++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
  // ----------------------------------SUBSCRIBE--------------------------------------------
  // +++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
  // +++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
  // +++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
  var subscribeInput = $('.subscribe__input');

  subscribeInput.on('focus', function() {
    const wrapper = $(this).parent('.subscribe__input-wrapper');

    !wrapper.hasClass('focus') && wrapper.addClass('focus');
  });

  subscribeInput.on('blur', function(e) {
    const wrapper = $(this).parent('.subscribe__input-wrapper');

    if (!e.target.value) {
      wrapper.removeClass('focus');
    }
  });

  subscribeInput.on('input', function(e) {
    const wrapper = $(this).parent('.subscribe__input-wrapper');

    if (!e.target.value) {
      wrapper.removeClass('not-empty invalid');
    } else if (validateEmail(e.target.value)) {
      wrapper.removeClass('invalid');
    } else {
      !wrapper.hasClass('invalid') && wrapper.addClass('invalid');
      !wrapper.hasClass('not-empty') && wrapper.addClass('not-empty');
    }
  });

  $('.validation-icon .cross').on('click', function() {
    var wrapper = $(this).closest('.subscribe');

    wrapper.find('.subscribe__input').val('');
    wrapper
      .find('.subscribe__input-wrapper')
      .removeClass('not-empty invalid focus');
  });

  $('.validation-icon .check').on('click', function(e) {
    // var form = $(this).closest('form');
    // var enteredEmail = form.find('.subscribe__input').val();

    // if (!validateEmail(enteredEmail)) {
    //   e.preventDefault();
    //   wrapper.stop().shake();
    // } else {
    //   form.submit();
    // }

    e.preventDefault();

    var wrapper = $(this).closest('.subscribe');
    var enteredEmail = wrapper.find('.subscribe__input').val();

    if (validateEmail(enteredEmail)) {
      openSubscripePopup(enteredEmail);
    } else {
      wrapper.stop().shake();
    }
  });

  function validateEmail(email) {
    var re = /^(([^<>()\[\]\\.,;:\s@"]+(\.[^<>()\[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;
    return re.test(String(email).toLowerCase());
  }

  // $('.mce-form').on('submit', function(e) {
  //   var enteredEmail = $(this)
  //     .find('.subscribe__input')
  //     .val();

  //   if (!validateEmail(enteredEmail)) {
  //     $(this)
  //       .stop()
  //       .shake();
  //     e.preventDefault();
  //   }
  // });

  $('.subscribe__input').on('keypress', function(e) {
    if (e.keyCode !== 13) return;

    var wrapper = $(this).parent();
    var enteredEmail = $(this).val();

    if (validateEmail(enteredEmail)) {
      openSubscripePopup(enteredEmail);
    } else {
      wrapper.stop().shake();
    }
  });

  // $('.subscribe-form').on('submit', function(e) {
  //   e.preventDefault();

  //   var values = $(this).serialize();

  //   console.log(values);

  //   $('.subscribe__input').val('');
  //   $('.subscribe__input-wrapper').removeClass('not-empty invalid focus');

  //   $.magnificPopup.open({
  //     items: {
  //       src: '.popup-after-subscribe',
  //       type: 'inline',
  //       preloader: false,
  //       modal: true
  //     }
  //   });

  //   $('.popup-after-subscribe')
  //     .find('.btn')
  //     .on('click', function(e) {
  //       e.preventDefault();
  //       $.magnificPopup.close();
  //     });
  // });

  var overlay = $('.overlay');
  var menu = $('.menu');
  var subscribePopup = $('.subscribe-popup');
  var subscribeForm = $('.subscribe-form');
  var subscribeButton = subscribeForm.find('.subscribe-form__submit');
  var subscribeCloseButton = subscribePopup.find('.subscribe-popup__close');
  var emailField = subscribeForm.find('#subscribe-email');
  var nameField = subscribeForm.find('#subscribe-name');
  var errorField = subscribeForm.find('.subscribe-form__error');

  // nameField.add(emailField).on('keypress', function(e) {
  //   if (e.keyCode === 32) {
  //     e.preventDefault();
  //   }
  // });

  nameField.add(emailField).on('input', function(e) {
    validateSubscribeForm();
  });

  nameField.on('blur', function() {
    if ($(this).val().length === 0) {
      errorField.text('Please enter your name');
      $(this).addClass('invalid');
    }
  })

  emailField.on('blur', function() {
    if (!validateEmail($(this).val())) {
      errorField.text('Please enter a valid email address');
      $(this).addClass('invalid');
    }
  })

  function validateSubscribeForm() {
    var valid = true;

    if (nameField.val().length === 0) {
      valid = false;
    } else {
      $(nameField).removeClass('invalid');
    }

    if (!validateEmail(emailField.val())) {
      valid = false;
    } else {
      $(emailField).removeClass('invalid');
    }

    if (valid) {
      errorField.text('');
      subscribeForm.addClass('valid');
      subscribeButton.attr('disabled', false);
    } else {
      subscribeForm.removeClass('valid');
      subscribeButton.attr('disabled', true);
    }
  }

  function closeSubscribePopup() {
    subscribePopup.removeClass('active').fadeOut();
    subscribeForm.removeClass('valid');

    // if menu not open then remove overlay
    if (!menu.hasClass('active')) {
      overlay.removeClass('active ');
    }
  }

  subscribeCloseButton.on('click', closeSubscribePopup);

  function openSubscripePopup(email) {
    subscribeForm.find('[type="email"]').val(email);

    subscribePopup.addClass('active').fadeIn();

    nameField.focus();

    overlay.addClass('active');
  }

  subscribeForm.on('submit', function(e) {
    e.preventDefault();

    var values = $(this).serialize();

    console.log(values);

    subscribePopup.addClass('thanks');

    $('.subscribe__input').val('');
    $('.subscribe__input-wrapper').removeClass('not-empty invalid focus');
  })

  // +++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
  // +++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
  // +++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
  // ----------------------------------SUBSCRIBE SELECT=------------------------------------
  // +++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
  // +++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
  // +++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
  // +++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
  var customSelect = $('.custom-select');
  var customSelectInput = customSelect.find('input');
  var customSelectDropdown = customSelect.find('.custom-select__dropdown');

  customSelectDropdown.on('click', 'li', function() {
    var val = $(this).attr('data-value');

    $('.custom-select .same-as-selected').removeClass('same-as-selected');
    $(this).addClass('same-as-selected');

    if (val === 'Other') {
      customSelectInput.val('').removeClass('disabled').prop('placeholder', 'Enter title')
      customSelectInput.focus();
    } else {
      customSelectInput.val(val).addClass('disabled')
    }

    customSelect.toggleClass('active');
    customSelectDropdown.stop().slideToggle(200);
  })

  $('.custom-select__dropdown-btn').on('click', function() {
    customSelect.toggleClass('active');
    customSelectDropdown.stop().slideToggle(200);
  })

  customSelectInput.on('keypress', function(e) {
    if ($(this).hasClass('disabled')) {
      e.preventDefault();
    }
  })

  customSelectInput.on('click', function() {
    if ($(this).hasClass('disabled')) {
      customSelect.toggleClass('active');
      customSelectDropdown.stop().slideToggle(200);
    }
  })

  // End animations

  // +++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
  // +++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
  // +++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
  // ---------------add active class link to current page remove on wp----------------------
  // +++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
  // +++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
  // +++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
  var curPage = window.location.pathname.substr(1);

  curPage = curPage !== '' ? curPage : 'index.html';

  $('.menu__link').each(function() {
    if ($(this).attr('href') === curPage) {
      $(this)
        .addClass('active')

        .removeAttr('href');
    }
  });
  // end remove on wp

  // Trim titles in news blocks

  function trimText() {
    var trimAfter = 2;

    $('.news__title').text(function(i, text) {
      var title = $(this).attr('title');

      if (!title) {
        $(this).attr('title', text);

        title = $(this).attr('title');
      } else if (title !== text) {
        text = title;

        $(this).text(text);
      }

      var lh = parseFloat($(this).css('line-height'));

      lh = Math.ceil(lh);

      if ($(this).height() <= trimAfter * lh) {
        return text;
      }

      var oldText = title;

      while ($(this).height() > trimAfter * lh) {
        oldText = oldText.substring(0, oldText.length - 1);

        $(this).text(oldText);
      }

      return oldText.substring(0, oldText.length - 3) + '...';
    });
  }

  $(window).on('load resize', trimText);

  trimText();

  // end trim

  // add target blank to external links

  var siteUrl = window.location.hostname;

  $('a[href*="//"]:not([href*="' + siteUrl + '"])').attr('target', '_blank');

  // Accordion in menu

  $('.menu__link--toggle').click(function(e) {
    e.preventDefault();

    $('.menu__item')
      .not($(this).parent())

      .removeClass('menu__item--active')

      .find('.menu__sub-menu')

      .slideUp();

    $(this)
      .parent()

      .toggleClass('menu__item--active')

      .find('.menu__sub-menu')

      .slideToggle();
  });

  $('.menu').on('click', '.menu__sub-link', closeMenu);

  // Scroll to Id

  if ($('.slick-nav').length) {
    var pagePath = window.location.pathname.substring(1);

    var menuSubLinks = $('a[href*="' + pagePath + '"]');

    menuSubLinks.each(function() {
      var indx = $(this)
        .attr('href')

        .indexOf(pagePath);

      var href = $(this)
        .attr('href')

        .substring(indx + pagePath.length);

      href = href ? href : $(this).attr('data-rel');

      $(this).attr('href', href);
    });

    $('a.slick-nav__anchor')
      .add(menuSubLinks)

      .mPageScroll2id({
        offset: $('.slick-nav').outerHeight() + 20,

        keepHighlightUntilNext: true,

        highlightClass: 'active',

        scrollingEasing: 'easeInOutQuint'
      });

    // END SCROLL 2 ID

    // MAKE NAV STICKY

    $('.slick-nav-spacer').height($('.slick-nav').height());

    var slickNavOffset =
      $('.slick-nav').offset().top - $('.slick-nav').outerHeight();

    $(window).on('scroll', function() {
      if ($(window).scrollTop() >= slickNavOffset) {
        $('.slick-nav').addClass('slick-nav--fixed');
      } else {
        $('.slick-nav').removeClass('slick-nav--fixed');
      }
    });

    // END MAKE NAV STICKY

    // Scroll section a little bit lower when redirect from another page

    (function() {
      if (location.hash) {
        setTimeout(function() {
          var offset = $('.slick-nav').outerHeight() + 20;

          var section = document.getElementById(location.hash.substring(1));

          $('.slick-nav').addClass('slick-nav--fixed');

          window.scrollTo(0, section.offsetTop - offset);
        }, 100);
      }
    })();

    // End scroll section

    // CHANGE COLOUR ON DIFFERENT SECTIONS

    var sections = [];

    $(window).on('load', function() {
      // this must be only on load!

      $('.section').each(function(indx) {
        sections[indx] = {
          start: $(this).offset().top,

          end: $(this).offset().top + $(this).outerHeight(),

          grey: $(this).hasClass('section--grey')
        };
      });
    });

    $(window).on('scroll', function() {
      var navOffset = $('.slick-nav').offset().top;

      for (var i = 0; i < sections.length; i++) {
        if (navOffset >= sections[i].start && navOffset < sections[i].end) {
          if (sections[i].grey) {
            $('.slick-nav').addClass('bg-white');
          } else {
            $('.slick-nav').removeClass('bg-white');
          }
        }
      }
    });

    // END CHANGE COLOUR ON DIFFERENT SECTIONS
  }

  // Resize children blocks of container with class equal-height to be equal height

  function resizeEqualHeight() {
    $('.equal-height').each(function() {
      var from = 0;

      if ($(this).attr('data-equal-from')) {
        from = parseInt($(this).attr('data-equal-from'), 10);

        from = isNaN(from) ? 0 : from;
      }

      if ($(window).outerWidth() < from) {
        $(this)
          .children()

          .height('auto');

        return;
      }

      var maxHeight = Math.max.apply(
        null,

        $(this)
          .children()

          .height('auto') // reset height

          .map(function() {
            return $(this).height();
          })
      );

      $(this)
        .children()

        .height(maxHeight);
    });
  }

  $(window).on('load resize', resizeEqualHeight);

  // end equal-height

  $('.lifestyle-slider__list').slick({
    dots: false,

    infinite: true,

    arrows: true,

    speed: 1000,

    slidesToShow: 3,

    slidesToScroll: 3,

    autoplay: true,

    autoplaySpeed: 5000,

    cssEase: 'ease-in-out',

    pauseOnHover: true,

    //dotsClass: 'lifestyle-slider__dots',

    prevArrow: '.lifestyle-slider .slider-navs__prev',

    nextArrow: '.lifestyle-slider .slider-navs__next',

    // variableWidth: true,

    responsive: [
      {
        breakpoint: 992,

        settings: {
          slidesToShow: 2,

          slidesToScroll: 2
        }
      },

      {
        breakpoint: 576,

        settings: {
          slidesToShow: 1,

          slidesToScroll: 1
        }
      }
    ]
  });

  $('.talking-about__slider').slick({
    dots: true,

    infinite: true,

    arrows: true,

    speed: 800,

    fade: true,

    slidesToShow: 1,

    slidesToScroll: 1,

    // autoplay: true,

    // autoplaySpeed: 5000,

    // pauseOnHover: true,

    cssEase: 'ease-in',

    dotsClass: 'slider-navs__dots',

    prevArrow: '.talking-about .slider-navs__prev',

    nextArrow: '.talking-about .slider-navs__next'
  });

  $('.facts__slider').slick({
    dots: true,

    infinite: true,

    arrows: true,

    speed: 1000,

    slidesToShow: 2,

    slidesToScroll: 2,

    autoplay: true,

    pauseOnHover: true,

    autoplaySpeed: 5000,

    cssEase: 'ease-in-out',

    dotsClass: 'slider-navs__dots',

    prevArrow: '.facts .slider-navs__prev',

    nextArrow: '.facts .slider-navs__next',

    responsive: [
      {
        breakpoint: 1200,

        settings: {
          slidesToShow: 1,

          slidesToScroll: 1
        }
      }
    ]
  });

  // Sliders for news

  $('.news__grid--slider').each(function() {
    var container = $(this)
      .closest('.news')

      .find('.paginator');

    var prevBtn = container.find('.paginator__prev');

    var nextBtn = container.find('.paginator__next');

    var isSmall = container.closest('.section').attr('id') === 'news-releases';

    var slickOptions = {
      infinite: false,

      slidesToShow: isSmall ? 3 : 1,

      slidesToScroll: isSmall ? 3 : 1,

      slidesPerRow: isSmall ? 1 : 3,

      prevArrow: prevBtn,

      pauseOnHover: true,

      speed: 500,

      nextArrow: nextBtn,

      rows: isSmall ? 1 : 2,

      dots: true,

      appendDots: $(this)
        .closest('.news')

        .find('.paginator')

        .find('.paginator__nums-container'),

      responsive: [
        {
          breakpoint: 1200,

          settings: {
            slidesToShow: 1,

            slidesToScroll: 1,

            slidesPerRow: 1
          }
        },

        {
          breakpoint: 768,

          settings: {
            rows: 1,

            slidesPerRow: 1,

            slidesToScroll: 1,

            slidesToShow: 1
          }
        }
      ]
    };

    var slider = $(this).slick(slickOptions);

    function doPagination(cur, total) {
      if (total < 12) {
        return;
      }

      var maxItems = 11;

      var maxItemsWithoutDots = maxItems - 2;

      var range = 3;

      var array = [];

      var left, right;

      if (cur < maxItemsWithoutDots - 3) {
        cur = 0;

        right = maxItems - range;
      } else if (cur >= total - maxItemsWithoutDots + 3) {
        right = total;

        cur = total - maxItemsWithoutDots + 3;
      } else {
        right = cur + range;
      }

      left = cur - range;

      for (var i = 0; i < total; i++) {
        if (i === 0 || i === total - 1) {
          array.push(1);
        } else if (i < left || i > right) {
          array.push(0);
        } else {
          array.push(1);
        }
      }

      container.find('.slick-dots > button').css('display', 'inline-block');

      container.find('.slick-dots > li').each(function() {
        var index = $(this).index();

        switch (array[index]) {
          case 1:
            $(this)
              .find('button')

              .css('display', 'inline-block');

            break;

          case 0:
            $(this)
              .find('button')

              .css('display', 'none');

            if (array[index + 1]) {
              $(this).append('<span class="paginator__dots">...</span>');
            }

            break;

          default:
            return;
        }
      });
    }

    var active = container.find('.slick-active');

    var total = container.find('.slick-dots > li').length;

    doPagination(active.index(), total);

    function checkNavs(e, slick, currentSlide, nextSlide) {
      if (nextSlide === 0) {
        prevBtn.addClass('hidden');

        nextBtn.removeClass('hidden');
      } else if (
        currentSlide < nextSlide &&
        container

          .find('.slick-active')

          .next()

          .is(':last-child')
      ) {
        nextBtn.addClass('hidden');

        prevBtn.removeClass('hidden');
      } else {
        prevBtn.removeClass('hidden');

        nextBtn.removeClass('hidden');
      }
    }

    if (container.find('.slick-active').is(':last-child')) {
      nextBtn.addClass('hidden');
    }

    slider.on('beforeChange', checkNavs);

    slider.on('afterChange', function() {
      var active = container.find('.slick-active');

      var total = container

        .find('.paginator__dots')

        .remove()

        .end()

        .find('.slick-dots > li').length;

      doPagination(active.index(), total);
    });

    $(this).on('contentChanged', function() {
      $(this).slick(slickOptions);

      doPagination(active.index(), total);
    });
  });

  function scroll() {
    var cls = $(this)
      .closest('.promo')

      .next()

      .offset().top;

    $('html, body').animate({ scrollTop: cls }, 'slow');
  }

  $(document).keyup(function(e) {
    if (e.keyCode == 27) {
      closeMenu();
    }
  });

  $('.overlay').click(function() {
    if ($('.subscribe-popup').hasClass('active')) {
      return;
    }

    closeMenu();
  });

  function closeMenu() {
    $('.menu_btn').removeClass('active');

    $('.sandwich').removeClass('active');

    $('.overlay').removeClass('active');

    $('.menu').removeClass('active');
  }

  $('.promo__arrow').click(scroll);

  $('.menu_btn').click(function() {
    $('.menu_btn').toggleClass('active');

    $('.sandwich').toggleClass('active');

    $('.overlay').toggleClass('active');

    $('.menu').toggleClass('active');
  });

  $('.menu-list__link').click(function() {
    $('.menu_btn').toggleClass('active');

    $('.sandwich').toggleClass('active');

    $('.overlay').toggleClass('active');

    $('.menu').toggleClass('active');
  });

  $('.img-popup').magnificPopup({
    type: 'image',

    closeOnContentClick: true,

    mainClass: 'mfp-with-zoom' // this class is for CSS animation below

    // zoom: {

    //   enabled: true, // By default it's false, so don't forget to enable it

    //   duration: 300, // duration of the effect, in milliseconds

    //   easing: 'ease-in-out', // CSS transition easing function

    //   // The "opener" function should return the element from which popup will be zoomed in

    //   // and to which popup will be scaled down

    //   // By defailt it looks for an image tag:

    //   opener: function(openerElement) {

    //     // openerElement is the element on which popup was initialized, in this case its <a> tag

    //     // you don't need to add "opener" option if this code matches your needs, it's defailt one.

    //     return openerElement.is('img')

    //       ? openerElement

    //       : openerElement.find('img');

    //   }

    // }

    // other options
  });

  $('.pop-up-link').magnificPopup({
    type: 'inline',

    preloader: false,

    focus: '#s',

    showCloseBtn: false,

    callbacks: {
      beforeOpen: function() {
        if ($(window).width() < 700) {
          this.st.focus = false;
        } else {
          this.st.focus = '#name';
        }
      }
    }
  });

  // $('.popup-with-form').magnificPopup({

  //   type: 'inline',

  //   preloader: false

  // });

  // $('.zoom-popup').magnificPopup({

  //   type: 'inline',

  //   preloader: false,

  //   focus: '#name',

  //   // When elemened is focused, some mobile browsers in some cases zoom in

  //   // It looks not nice, so we disable it:

  //   callbacks: {

  //     beforeOpen: function() {

  //       if ($(window).width() < 700) {

  //         this.st.focus = false;

  //       } else {

  //         this.st.focus = '#name';

  //       }

  //     }

  //   }

  // });
});
