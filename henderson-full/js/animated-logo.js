var canvas, stage, exportRoot, anim_container, dom_overlay_container, fnStartAnimation;

function init() {
  canvas = document.getElementById("canvas");
  anim_container = document.querySelector(".animation-container");
  dom_overlay_container = document.querySelector(".animation-container__overlay");
  var comp = AdobeAn.getComposition("329A3B38FA005A488196959DEBA036E3");
  var lib = comp.getLibrary();
  handleComplete({}, comp);
}

function handleComplete(evt, comp) {
  //This function is always called, irrespective of the content. You can use the variable "stage" after it is created in token create_stage.
  var lib = comp.getLibrary();
  var ss = comp.getSpriteSheet();
  exportRoot = new lib.Logoanimation();
  stage = new lib.Stage(canvas);
  //Registers the "tick" event listener.
  fnStartAnimation = function () {
    stage.addChild(exportRoot);
    createjs.Ticker.setFPS(lib.properties.fps);
    createjs.Ticker.addEventListener("tick", stage);
  }
  //Code to support hidpi screens and responsive scaling.
  function makeResponsive(isResp, respDim, isScale, scaleType) {
    var lastW, lastH, lastS = 1;
    window.addEventListener('resize', resizeCanvas);
    resizeCanvas();
    function resizeCanvas() {
      var w = lib.properties.width, h = lib.properties.height;
      var iw = window.innerWidth, ih = window.innerHeight;
      var pRatio = window.devicePixelRatio || 1, xRatio = iw / w, yRatio = ih / h, sRatio = 1;
      if (isResp) {
        if ((respDim == 'width' && lastW == iw) || (respDim == 'height' && lastH == ih)) {
          sRatio = lastS;
        }
        else if (!isScale) {
          if (iw < w || ih < h)
            sRatio = Math.min(xRatio, yRatio);
        }
        else if (scaleType == 1) {
          sRatio = Math.min(xRatio, yRatio);
        }
        else if (scaleType == 2) {
          sRatio = Math.max(xRatio, yRatio);
        }
      }
      canvas.width = w * pRatio * sRatio;
      canvas.height = h * pRatio * sRatio;
      // canvas.style.width = dom_overlay_container.style.width = anim_container.style.width = w * sRatio + 'px';
      // canvas.style.height = anim_container.style.height = dom_overlay_container.style.height = h * sRatio + 'px';
      
      // Uncomment if it would be on hero img
      // $(window).width() < 992
      //       ? (canvas.style.width = '80%')
      //       : (canvas.style.width = '53.5%');
      canvas.style.width = '100%';

      stage.scaleX = pRatio * sRatio;
      stage.scaleY = pRatio * sRatio;
      lastW = iw; lastH = ih; lastS = sRatio;
      stage.tickOnUpdate = false;
      stage.update();
      stage.tickOnUpdate = true;
    }
  }
  makeResponsive(true, 'both', false, 1);
  AdobeAn.compositionLoaded(lib.properties.id);
  fnStartAnimation();
}

(function (cjs, an) {

  var p; // shortcut to reference prototypes
  var lib={};var ss={};var img={};
  lib.ssMetadata = [];
  
  // symbols:
  
  (lib.Tween82 = function(mode,startPosition,loop) {
    this.initialize(mode,startPosition,loop,{});
  
    // Layer_1
    this.shape = new cjs.Shape();
    this.shape.graphics.f("#fff").s().p("AA3B2IgRgzIhMAAIgRAzIhFAAIBZjrIBGAAIBaDrgAgVARIArAAIgWhFg");
  
    this.timeline.addTween(cjs.Tween.get(this.shape).wait(1));
  
  }).prototype = p = new cjs.MovieClip();
  p.nominalBounds = new cjs.Rectangle(-12.5,-11.8,25,23.6);
  
  
  (lib.Tween81 = function(mode,startPosition,loop) {
    this.initialize(mode,startPosition,loop,{});
  
    // Layer_1
    this.shape = new cjs.Shape();
    this.shape.graphics.f("#fff").s().p("AA3B2IgRgzIhMAAIgRAzIhFAAIBZjrIBGAAIBaDrgAgVARIArAAIgWhFg");
  
    this.timeline.addTween(cjs.Tween.get(this.shape).wait(1));
  
  }).prototype = p = new cjs.MovieClip();
  p.nominalBounds = new cjs.Rectangle(-12.5,-11.8,25,23.6);
  
  
  (lib.Tween80 = function(mode,startPosition,loop) {
    this.initialize(mode,startPosition,loop,{});
  
    // Layer_1
    this.shape = new cjs.Shape();
    this.shape.graphics.f("#fff").s().p("AAPB8Qg2AAgmgkQgmglAAgzQAAgyAmglQAmglA2AAQAdAAAaAMQAbANARAXIgmAoQgYgdglgBQgbABgTAUQgSAVAAAbQABAaATATQATATAZABIAAgBQAngBAZgdIAlApQgSAXgbAMQgZAMgbAAIgEgBg");
  
    this.timeline.addTween(cjs.Tween.get(this.shape).wait(1));
  
  }).prototype = p = new cjs.MovieClip();
  p.nominalBounds = new cjs.Rectangle(-11.6,-12.4,23.3,24.9);
  
  
  (lib.Tween79 = function(mode,startPosition,loop) {
    this.initialize(mode,startPosition,loop,{});
  
    // Layer_1
    this.shape = new cjs.Shape();
    this.shape.graphics.f("#fff").s().p("AAPB8Qg2AAgmgkQgmglAAgzQAAgyAmglQAmglA2AAQAdAAAaAMQAbANARAXIgmAoQgYgdglgBQgbABgTAUQgSAVAAAbQABAaATATQATATAZABIAAgBQAngBAZgdIAlApQgSAXgbAMQgZAMgbAAIgEgBg");
  
    this.timeline.addTween(cjs.Tween.get(this.shape).wait(1));
  
  }).prototype = p = new cjs.MovieClip();
  p.nominalBounds = new cjs.Rectangle(-11.6,-12.4,23.3,24.9);
  
  
  (lib.Tween78 = function(mode,startPosition,loop) {
    this.initialize(mode,startPosition,loop,{});
  
    // Layer_1
    this.shape = new cjs.Shape();
    this.shape.graphics.f("#fff").s().p("AhNBbQgegeAAgsIAAiJIBAAAIAACJQgCASANAOQALAOASABQASACAOgMQANgMACgSIAAiQIBAAAIAACJQAAAsgfAeQgeAegvAAQguAAgfgeg");
  
    this.timeline.addTween(cjs.Tween.get(this.shape).wait(1));
  
  }).prototype = p = new cjs.MovieClip();
  p.nominalBounds = new cjs.Rectangle(-10.8,-12.1,21.6,24.3);
  
  
  (lib.Tween77 = function(mode,startPosition,loop) {
    this.initialize(mode,startPosition,loop,{});
  
    // Layer_1
    this.shape = new cjs.Shape();
    this.shape.graphics.f("#fff").s().p("AhNBbQgegeAAgsIAAiJIBAAAIAACJQgCASANAOQALAOASABQASACAOgMQANgMACgSIAAiQIBAAAIAACJQAAAsgfAeQgeAegvAAQguAAgfgeg");
  
    this.timeline.addTween(cjs.Tween.get(this.shape).wait(1));
  
  }).prototype = p = new cjs.MovieClip();
  p.nominalBounds = new cjs.Rectangle(-10.8,-12.1,21.6,24.3);
  
  
  (lib.Tween76 = function(mode,startPosition,loop) {
    this.initialize(mode,startPosition,loop,{});
  
    // Layer_1
    this.shape = new cjs.Shape();
    this.shape.graphics.f("#fff").s().p("AhTB2IAAjrIBAAAIAACyIBnAAIAAA5g");
  
    this.timeline.addTween(cjs.Tween.get(this.shape).wait(1));
  
  }).prototype = p = new cjs.MovieClip();
  p.nominalBounds = new cjs.Rectangle(-8.4,-11.8,16.9,23.6);
  
  
  (lib.Tween75 = function(mode,startPosition,loop) {
    this.initialize(mode,startPosition,loop,{});
  
    // Layer_1
    this.shape = new cjs.Shape();
    this.shape.graphics.f("#fff").s().p("AhTB2IAAjrIBAAAIAACyIBnAAIAAA5g");
  
    this.timeline.addTween(cjs.Tween.get(this.shape).wait(1));
  
  }).prototype = p = new cjs.MovieClip();
  p.nominalBounds = new cjs.Rectangle(-8.4,-11.8,16.9,23.6);
  
  
  (lib.Tween74 = function(mode,startPosition,loop) {
    this.initialize(mode,startPosition,loop,{});
  
    // Layer_1
    this.shape = new cjs.Shape();
    this.shape.graphics.f("#fff").s().p("AgfB2IAAiyIhCAAIAAg5IDDAAIAAA5IhCAAIAACyg");
  
    this.timeline.addTween(cjs.Tween.get(this.shape).wait(1));
  
  }).prototype = p = new cjs.MovieClip();
  p.nominalBounds = new cjs.Rectangle(-9.8,-11.8,19.6,23.6);
  
  
  (lib.Tween73 = function(mode,startPosition,loop) {
    this.initialize(mode,startPosition,loop,{});
  
    // Layer_1
    this.shape = new cjs.Shape();
    this.shape.graphics.f("#fff").s().p("AgfB2IAAiyIhCAAIAAg5IDDAAIAAA5IhCAAIAACyg");
  
    this.timeline.addTween(cjs.Tween.get(this.shape).wait(1));
  
  }).prototype = p = new cjs.MovieClip();
  p.nominalBounds = new cjs.Rectangle(-9.8,-11.8,19.6,23.6);
  
  
  (lib.Tween72 = function(mode,startPosition,loop) {
    this.initialize(mode,startPosition,loop,{});
  
    // Layer_1
    this.shape = new cjs.Shape();
    this.shape.graphics.f("#fff").s().p("AhMBbQgfgeAAgsIAAiJIBAAAIAACJQgBASAMAOQALAOATABQARACAOgMQANgMACgSQABgEgBgDIAAAAIAAiJIBAAAIAACJQAAAsgfAeQgeAegvAAQgtAAgfgeg");
  
    this.timeline.addTween(cjs.Tween.get(this.shape).wait(1));
  
  }).prototype = p = new cjs.MovieClip();
  p.nominalBounds = new cjs.Rectangle(-10.7,-12.1,21.6,24.3);
  
  
  (lib.Tween71 = function(mode,startPosition,loop) {
    this.initialize(mode,startPosition,loop,{});
  
    // Layer_1
    this.shape = new cjs.Shape();
    this.shape.graphics.f("#fff").s().p("AhMBbQgfgeAAgsIAAiJIBAAAIAACJQgBASAMAOQALAOATABQARACAOgMQANgMACgSQABgEgBgDIAAAAIAAiJIBAAAIAACJQAAAsgfAeQgeAegvAAQgtAAgfgeg");
  
    this.timeline.addTween(cjs.Tween.get(this.shape).wait(1));
  
  }).prototype = p = new cjs.MovieClip();
  p.nominalBounds = new cjs.Rectangle(-10.7,-12.1,21.6,24.3);
  
  
  (lib.Tween70 = function(mode,startPosition,loop) {
    this.initialize(mode,startPosition,loop,{});
  
    // Layer_1
    this.shape = new cjs.Shape();
    this.shape.graphics.f("#fff").s().p("AAjB2IgnhKIgiAAIAABKIhAAAIAAjrIBuAAQAmAAAYAXQAYAYAAAiQABAVgLASQgLATgTAKIAxBWgAgmgNIAqAAQALACAHgHQAHgGABgKQABgKgHgIQgGgHgKgBIguAAg");
  
    this.timeline.addTween(cjs.Tween.get(this.shape).wait(1));
  
  }).prototype = p = new cjs.MovieClip();
  p.nominalBounds = new cjs.Rectangle(-10.3,-11.8,20.6,23.6);
  
  
  (lib.Tween69 = function(mode,startPosition,loop) {
    this.initialize(mode,startPosition,loop,{});
  
    // Layer_1
    this.shape = new cjs.Shape();
    this.shape.graphics.f("#fff").s().p("AAjB2IgnhKIgiAAIAABKIhAAAIAAjrIBuAAQAmAAAYAXQAYAYAAAiQABAVgLASQgLATgTAKIAxBWgAgmgNIAqAAQALACAHgHQAHgGABgKQABgKgHgIQgGgHgKgBIguAAg");
  
    this.timeline.addTween(cjs.Tween.get(this.shape).wait(1));
  
  }).prototype = p = new cjs.MovieClip();
  p.nominalBounds = new cjs.Rectangle(-10.3,-11.8,20.6,23.6);
  
  
  (lib.Tween68 = function(mode,startPosition,loop) {
    this.initialize(mode,startPosition,loop,{});
  
    // Layer_1
    this.shape = new cjs.Shape();
    this.shape.graphics.f("#fff").s().p("AhbB2IAAjrIC1AAIAAA2Ih1AAIAAAlIBZAAIAAAxIhZAAIAAApIB3AAIAAA2g");
  
    this.timeline.addTween(cjs.Tween.get(this.shape).wait(1));
  
  }).prototype = p = new cjs.MovieClip();
  p.nominalBounds = new cjs.Rectangle(-9.2,-11.8,18.5,23.6);
  
  
  (lib.Tween67 = function(mode,startPosition,loop) {
    this.initialize(mode,startPosition,loop,{});
  
    // Layer_1
    this.shape = new cjs.Shape();
    this.shape.graphics.f("#fff").s().p("AhbB2IAAjrIC1AAIAAA2Ih1AAIAAAlIBZAAIAAAxIhZAAIAAApIB3AAIAAA2g");
  
    this.timeline.addTween(cjs.Tween.get(this.shape).wait(1));
  
  }).prototype = p = new cjs.MovieClip();
  p.nominalBounds = new cjs.Rectangle(-9.2,-11.8,18.5,23.6);
  
  
  (lib.Tween66 = function(mode,startPosition,loop) {
    this.initialize(mode,startPosition,loop,{});
  
    // Layer_1
    this.shape = new cjs.Shape();
    this.shape.graphics.f("#fff").s().p("AAAB9QgmgBgfgSQgMgIgMgKQglglgBgzQABgyAlglQAMgKAMgIQAfgTAmAAQA3AAAmAlQAmAlAAAyQABAzgmAlQgnAkg2ABgAgsgwQgUATAAAdIgBAAQAAAbARAUQATATAbABIADAAQAbABAUgTQAVgUAAgcQABgagUgUQgTgVgcgBIgBAAQgaAAgUATg");
  
    this.timeline.addTween(cjs.Tween.get(this.shape).wait(1));
  
  }).prototype = p = new cjs.MovieClip();
  p.nominalBounds = new cjs.Rectangle(-13.1,-12.4,26.3,24.9);
  
  
  (lib.Tween65 = function(mode,startPosition,loop) {
    this.initialize(mode,startPosition,loop,{});
  
    // Layer_1
    this.shape = new cjs.Shape();
    this.shape.graphics.f("#fff").s().p("AAAB9QgmgBgfgSQgMgIgMgKQglglgBgzQABgyAlglQAMgKAMgIQAfgTAmAAQA3AAAmAlQAmAlAAAyQABAzgmAlQgnAkg2ABgAgsgwQgUATAAAdIgBAAQAAAbARAUQATATAbABIADAAQAbABAUgTQAVgUAAgcQABgagUgUQgTgVgcgBIgBAAQgaAAgUATg");
  
    this.timeline.addTween(cjs.Tween.get(this.shape).wait(1));
  
  }).prototype = p = new cjs.MovieClip();
  p.nominalBounds = new cjs.Rectangle(-13.1,-12.4,26.3,24.9);
  
  
  (lib.Tween64 = function(mode,startPosition,loop) {
    this.initialize(mode,startPosition,loop,{});
  
    // Layer_1
    this.shape = new cjs.Shape();
    this.shape.graphics.f("#fff").s().p("AhdBYQgmglAAgzQAAgyAmglQAnglA2AAQA3AAAnAlQAmAlAAAyQAAAzgmAlQgmAkg4ABQg2gBgngkgAgsgwQgUATgBAdIAAAAQgBAbATAUQASATAbABIACAAQAcABAUgTQAVgUAAgcQABgbgTgTQgUgVgcgBIgBAAQgaAAgUATg");
  
    this.timeline.addTween(cjs.Tween.get(this.shape).wait(1));
  
  }).prototype = p = new cjs.MovieClip();
  p.nominalBounds = new cjs.Rectangle(-13.1,-12.4,26.4,24.9);
  
  
  (lib.Tween63 = function(mode,startPosition,loop) {
    this.initialize(mode,startPosition,loop,{});
  
    // Layer_1
    this.shape = new cjs.Shape();
    this.shape.graphics.f("#fff").s().p("AhdBYQgmglAAgzQAAgyAmglQAnglA2AAQA3AAAnAlQAmAlAAAyQAAAzgmAlQgmAkg4ABQg2gBgngkgAgsgwQgUATgBAdIAAAAQgBAbATAUQASATAbABIACAAQAcABAUgTQAVgUAAgcQABgbgTgTQgUgVgcgBIgBAAQgaAAgUATg");
  
    this.timeline.addTween(cjs.Tween.get(this.shape).wait(1));
  
  }).prototype = p = new cjs.MovieClip();
  p.nominalBounds = new cjs.Rectangle(-13.1,-12.4,26.4,24.9);
  
  
  (lib.Tween62 = function(mode,startPosition,loop) {
    this.initialize(mode,startPosition,loop,{});
  
    // Layer_1
    this.shape = new cjs.Shape();
    this.shape.graphics.f("#fff").s().p("AhgB2IAAjrIBpAAQAnAAAYAXQAZAYAAAkQAAAigZAXQgYAYgnAAIgpAAIAABHgAgggJIAmAAQAKABAIgHQAJgHABgKQABgKgIgJQgGgIgLgBIgqAAg");
  
    this.timeline.addTween(cjs.Tween.get(this.shape).wait(1));
  
  }).prototype = p = new cjs.MovieClip();
  p.nominalBounds = new cjs.Rectangle(-9.7,-11.8,19.4,23.6);
  
  
  (lib.Tween61 = function(mode,startPosition,loop) {
    this.initialize(mode,startPosition,loop,{});
  
    // Layer_1
    this.shape = new cjs.Shape();
    this.shape.graphics.f("#fff").s().p("AhgB2IAAjrIBpAAQAnAAAYAXQAZAYAAAkQAAAigZAXQgYAYgnAAIgpAAIAABHgAgggJIAmAAQAKABAIgHQAJgHABgKQABgKgIgJQgGgIgLgBIgqAAg");
  
    this.timeline.addTween(cjs.Tween.get(this.shape).wait(1));
  
  }).prototype = p = new cjs.MovieClip();
  p.nominalBounds = new cjs.Rectangle(-9.7,-11.8,19.4,23.6);
  
  
  (lib.Tween60 = function(mode,startPosition,loop) {
    this.initialize(mode,startPosition,loop,{});
  
    // Layer_1
    this.shape = new cjs.Shape();
    this.shape.graphics.f("#fff").s().p("AhgB2IAAjrIBpAAQAnAAAZAXQAYAYAAAkQAAAigYAXQgZAYgnAAIgpAAIAABHgAgggJIAmAAQAKABAJgHQAHgHACgKQABgKgIgJQgGgIgLgBIgqAAg");
  
    this.timeline.addTween(cjs.Tween.get(this.shape).wait(1));
  
  }).prototype = p = new cjs.MovieClip();
  p.nominalBounds = new cjs.Rectangle(-9.7,-11.8,19.4,23.6);
  
  
  (lib.Tween59 = function(mode,startPosition,loop) {
    this.initialize(mode,startPosition,loop,{});
  
    // Layer_1
    this.shape = new cjs.Shape();
    this.shape.graphics.f("#fff").s().p("AhgB2IAAjrIBpAAQAnAAAZAXQAYAYAAAkQAAAigYAXQgZAYgnAAIgpAAIAABHgAgggJIAmAAQAKABAJgHQAHgHACgKQABgKgIgJQgGgIgLgBIgqAAg");
  
    this.timeline.addTween(cjs.Tween.get(this.shape).wait(1));
  
  }).prototype = p = new cjs.MovieClip();
  p.nominalBounds = new cjs.Rectangle(-9.7,-11.8,19.4,23.6);
  
  
  (lib.Tween58 = function(mode,startPosition,loop) {
    this.initialize(mode,startPosition,loop,{});
  
    // Layer_1
    this.shape = new cjs.Shape();
    this.shape.graphics.f("#fff").s().p("AhdBYQgmglAAgzQAAgyAmglQAnglA2AAQA3AAAnAlQAmAlAAAyQAAAzgmAlQgnAkg3ABQg2gBgngkgAgsgwQgUATgBAcIAAABQAAAbASAUQASATAbABIACAAQAcABAUgTQAVgTAAgcQABgbgTgUQgTgVgcgBIgCAAQgaAAgUATg");
  
    this.timeline.addTween(cjs.Tween.get(this.shape).wait(1));
  
  }).prototype = p = new cjs.MovieClip();
  p.nominalBounds = new cjs.Rectangle(-13.1,-12.4,26.4,24.9);
  
  
  (lib.Tween57 = function(mode,startPosition,loop) {
    this.initialize(mode,startPosition,loop,{});
  
    // Layer_1
    this.shape = new cjs.Shape();
    this.shape.graphics.f("#fff").s().p("AhdBYQgmglAAgzQAAgyAmglQAnglA2AAQA3AAAnAlQAmAlAAAyQAAAzgmAlQgnAkg3ABQg2gBgngkgAgsgwQgUATgBAcIAAABQAAAbASAUQASATAbABIACAAQAcABAUgTQAVgTAAgcQABgbgTgUQgTgVgcgBIgCAAQgaAAgUATg");
  
    this.timeline.addTween(cjs.Tween.get(this.shape).wait(1));
  
  }).prototype = p = new cjs.MovieClip();
  p.nominalBounds = new cjs.Rectangle(-13.1,-12.4,26.4,24.9);
  
  
  (lib.Tween56 = function(mode,startPosition,loop) {
    this.initialize(mode,startPosition,loop,{});
  
    // Layer_1
    this.shape = new cjs.Shape();
    this.shape.graphics.f("#fff").s().p("AAjB2IgmhKIgiAAIAABKIhAAAIgBjrIBuAAQAmAAAYAXQAYAYAAAiQABAVgLASQgLATgTAKIAxBWgAgmgNIArAAQAKACAHgHQAIgGAAgKQABgKgHgIQgGgHgKgBIguAAg");
  
    this.timeline.addTween(cjs.Tween.get(this.shape).wait(1));
  
  }).prototype = p = new cjs.MovieClip();
  p.nominalBounds = new cjs.Rectangle(-10.3,-11.8,20.6,23.6);
  
  
  (lib.Tween55 = function(mode,startPosition,loop) {
    this.initialize(mode,startPosition,loop,{});
  
    // Layer_1
    this.shape = new cjs.Shape();
    this.shape.graphics.f("#fff").s().p("AAjB2IgmhKIgiAAIAABKIhAAAIgBjrIBuAAQAmAAAYAXQAYAYAAAiQABAVgLASQgLATgTAKIAxBWgAgmgNIArAAQAKACAHgHQAIgGAAgKQABgKgHgIQgGgHgKgBIguAAg");
  
    this.timeline.addTween(cjs.Tween.get(this.shape).wait(1));
  
  }).prototype = p = new cjs.MovieClip();
  p.nominalBounds = new cjs.Rectangle(-10.3,-11.8,20.6,23.6);
  
  
  (lib.Tween54 = function(mode,startPosition,loop) {
    this.initialize(mode,startPosition,loop,{});
  
    // Layer_1
    this.shape = new cjs.Shape();
    this.shape.graphics.f("#fff").s().p("AgfB2IAAiyIhCAAIAAg5IDDAAIAAA5IhCAAIAACyg");
  
    this.timeline.addTween(cjs.Tween.get(this.shape).wait(1));
  
  }).prototype = p = new cjs.MovieClip();
  p.nominalBounds = new cjs.Rectangle(-9.7,-11.8,19.6,23.6);
  
  
  (lib.Tween53 = function(mode,startPosition,loop) {
    this.initialize(mode,startPosition,loop,{});
  
    // Layer_1
    this.shape = new cjs.Shape();
    this.shape.graphics.f("#fff").s().p("AgfB2IAAiyIhCAAIAAg5IDDAAIAAA5IhCAAIAACyg");
  
    this.timeline.addTween(cjs.Tween.get(this.shape).wait(1));
  
  }).prototype = p = new cjs.MovieClip();
  p.nominalBounds = new cjs.Rectangle(-9.7,-11.8,19.6,23.6);
  
  
  (lib.Tween52 = function(mode,startPosition,loop) {
    this.initialize(mode,startPosition,loop,{});
  
    // Layer_1
    this.shape = new cjs.Shape();
    this.shape.graphics.f("#fff").s().p("AhMBbQgfgeAAgsIAAiJIBAAAIAACJQgBASAMAOQALAOASABQASACANgMQAOgMACgSQABgEgBgDIAAiJIBAAAIAACJQAAAsgfAeQgfAeguAAQgtAAgfgeg");
  
    this.timeline.addTween(cjs.Tween.get(this.shape).wait(1));
  
  }).prototype = p = new cjs.MovieClip();
  p.nominalBounds = new cjs.Rectangle(-10.7,-12.1,21.6,24.3);
  
  
  (lib.Tween51 = function(mode,startPosition,loop) {
    this.initialize(mode,startPosition,loop,{});
  
    // Layer_1
    this.shape = new cjs.Shape();
    this.shape.graphics.f("#fff").s().p("AhMBbQgfgeAAgsIAAiJIBAAAIAACJQgBASAMAOQALAOASABQASACANgMQAOgMACgSQABgEgBgDIAAiJIBAAAIAACJQAAAsgfAeQgfAeguAAQgtAAgfgeg");
  
    this.timeline.addTween(cjs.Tween.get(this.shape).wait(1));
  
  }).prototype = p = new cjs.MovieClip();
  p.nominalBounds = new cjs.Rectangle(-10.7,-12.1,21.6,24.3);
  
  
  (lib.Tween50 = function(mode,startPosition,loop) {
    this.initialize(mode,startPosition,loop,{});
  
    // Layer_1
    this.shape = new cjs.Shape();
    this.shape.graphics.f("#fff").s().p("AA3B2IhliDIAACDIhAAAIAAjrIA4AAIBkCCIAAiCIBBAAIAADrg");
  
    this.timeline.addTween(cjs.Tween.get(this.shape).wait(1));
  
  }).prototype = p = new cjs.MovieClip();
  p.nominalBounds = new cjs.Rectangle(-11,-11.8,22.1,23.6);
  
  
  (lib.Tween49 = function(mode,startPosition,loop) {
    this.initialize(mode,startPosition,loop,{});
  
    // Layer_1
    this.shape = new cjs.Shape();
    this.shape.graphics.f("#fff").s().p("AA3B2IhliDIAACDIhAAAIAAjrIA4AAIBkCCIAAiCIBBAAIAADrg");
  
    this.timeline.addTween(cjs.Tween.get(this.shape).wait(1));
  
  }).prototype = p = new cjs.MovieClip();
  p.nominalBounds = new cjs.Rectangle(-11,-11.8,22.1,23.6);
  
  
  (lib.Tween48 = function(mode,startPosition,loop) {
    this.initialize(mode,startPosition,loop,{});
  
    // Layer_1
    this.shape = new cjs.Shape();
    this.shape.graphics.f("#fff").s().p("AgfB2IAAjrIA/AAIAADrg");
  
    this.timeline.addTween(cjs.Tween.get(this.shape).wait(1));
  
  }).prototype = p = new cjs.MovieClip();
  p.nominalBounds = new cjs.Rectangle(-3.2,-11.8,6.4,23.6);
  
  
  (lib.Tween47 = function(mode,startPosition,loop) {
    this.initialize(mode,startPosition,loop,{});
  
    // Layer_1
    this.shape = new cjs.Shape();
    this.shape.graphics.f("#fff").s().p("AgfB2IAAjrIA/AAIAADrg");
  
    this.timeline.addTween(cjs.Tween.get(this.shape).wait(1));
  
  }).prototype = p = new cjs.MovieClip();
  p.nominalBounds = new cjs.Rectangle(-3.2,-11.8,6.4,23.6);
  
  
  (lib.Tween46 = function(mode,startPosition,loop) {
    this.initialize(mode,startPosition,loop,{});
  
    // Layer_1
    this.shape = new cjs.Shape();
    this.shape.graphics.f("#fff").s().p("AgfB2IAAiyIhCAAIAAg5IDDAAIAAA5IhCAAIAACyg");
  
    this.timeline.addTween(cjs.Tween.get(this.shape).wait(1));
  
  }).prototype = p = new cjs.MovieClip();
  p.nominalBounds = new cjs.Rectangle(-9.7,-11.8,19.6,23.6);
  
  
  (lib.Tween45 = function(mode,startPosition,loop) {
    this.initialize(mode,startPosition,loop,{});
  
    // Layer_1
    this.shape = new cjs.Shape();
    this.shape.graphics.f("#fff").s().p("AgfB2IAAiyIhCAAIAAg5IDDAAIAAA5IhCAAIAACyg");
  
    this.timeline.addTween(cjs.Tween.get(this.shape).wait(1));
  
  }).prototype = p = new cjs.MovieClip();
  p.nominalBounds = new cjs.Rectangle(-9.7,-11.8,19.6,23.6);
  
  
  (lib.Tween44 = function(mode,startPosition,loop) {
    this.initialize(mode,startPosition,loop,{});
  
    // Layer_1
    this.shape = new cjs.Shape();
    this.shape.graphics.f("#fff").s().p("AgfB2IAAhUIhUiXIBFAAIAuBbIAvhbIBFAAIhUCXIAABUg");
  
    this.timeline.addTween(cjs.Tween.get(this.shape).wait(1));
  
  }).prototype = p = new cjs.MovieClip();
  p.nominalBounds = new cjs.Rectangle(-11.6,-11.8,23.3,23.6);
  
  
  (lib.Tween43 = function(mode,startPosition,loop) {
    this.initialize(mode,startPosition,loop,{});
  
    // Layer_1
    this.shape = new cjs.Shape();
    this.shape.graphics.f("#fff").s().p("AgfB2IAAhUIhUiXIBFAAIAuBbIAvhbIBFAAIhUCXIAABUg");
  
    this.timeline.addTween(cjs.Tween.get(this.shape).wait(1));
  
  }).prototype = p = new cjs.MovieClip();
  p.nominalBounds = new cjs.Rectangle(-11.6,-11.8,23.3,23.6);
  
  
  (lib.Tween41 = function(mode,startPosition,loop) {
    this.initialize(mode,startPosition,loop,{});
  
    // Layer_1
    this.shape = new cjs.Shape();
    this.shape.graphics.f("#fff").s().p("AhaB2IAAjrIC1AAIAAA5Ih1AAIAAAnIBaAAIAAA5IhaAAIAABSg");
  
    this.timeline.addTween(cjs.Tween.get(this.shape).wait(1));
  
  }).prototype = p = new cjs.MovieClip();
  p.nominalBounds = new cjs.Rectangle(-9.1,-11.8,18.3,23.6);
  
  
  (lib.Tween40 = function(mode,startPosition,loop) {
    this.initialize(mode,startPosition,loop,{});
  
    // Layer_1
    this.shape = new cjs.Shape();
    this.shape.graphics.f("#fff").s().p("ADmJaIAAmKIBNAAIAAE/ICFAAIAAmfIqeAAIAAHqIkdAAIAAyzIEdAAIAAG8IHMAAIAAm8IEeAAIAASzgAm3IPICFAAIAAnrILqAAIAAoxIiFAAIAAG8IplAAIAAm8IiFAAg");
  
    this.timeline.addTween(cjs.Tween.get(this.shape).wait(1));
  
  }).prototype = p = new cjs.MovieClip();
  p.nominalBounds = new cjs.Rectangle(-51.6,-60.2,103.3,120.5);
  
  
  (lib.Tween39 = function(mode,startPosition,loop) {
    this.initialize(mode,startPosition,loop,{});
  
    // Layer_1
    this.shape = new cjs.Shape();
    this.shape.graphics.f("#fff").s().p("ADmJaIAAmKIBNAAIAAE/ICFAAIAAmfIqeAAIAAHqIkdAAIAAyzIEdAAIAAG8IHMAAIAAm8IEeAAIAASzgAm3IPICFAAIAAnrILqAAIAAoxIiFAAIAAG8IplAAIAAm8IiFAAg");
  
    this.timeline.addTween(cjs.Tween.get(this.shape).wait(1));
  
  }).prototype = p = new cjs.MovieClip();
  p.nominalBounds = new cjs.Rectangle(-51.6,-60.2,103.3,120.5);
  
  
  (lib.Tween36 = function(mode,startPosition,loop) {
    this.initialize(mode,startPosition,loop,{});
  
    // Layer_1
    this.shape = new cjs.Shape();
    this.shape.graphics.f("#fff").s().p("AnBJaIAAhMIM3AAIAAh1Is3AAIAAl1IKrAAIAAh1IqrAAIAAhMIL3AAIAAENIqrAAIAADcIM3AAIAAEOgAnBlLIAAkOIN0AAIAAEOgAl1mXILcAAIAAh2IrcAAg");
  
    this.timeline.addTween(cjs.Tween.get(this.shape).wait(1));
  
  }).prototype = p = new cjs.MovieClip();
  p.nominalBounds = new cjs.Rectangle(-45,-60.2,90.1,120.5);
  
  
  (lib.Tween35 = function(mode,startPosition,loop) {
    this.initialize(mode,startPosition,loop,{});
  
    // Layer_1
    this.shape = new cjs.Shape();
    this.shape.graphics.f("#fff").s().p("AnBJaIAAhMIM3AAIAAh1Is3AAIAAl1IKrAAIAAh1IqrAAIAAhMIL3AAIAAENIqrAAIAADcIM3AAIAAEOgAnBlLIAAkOIN0AAIAAEOgAl1mXILcAAIAAh2IrcAAg");
  
    this.timeline.addTween(cjs.Tween.get(this.shape).wait(1));
  
  }).prototype = p = new cjs.MovieClip();
  p.nominalBounds = new cjs.Rectangle(-45,-60.2,90.1,120.5);
  
  
  (lib.Tween32 = function(mode,startPosition,loop) {
    this.initialize(mode,startPosition,loop,{});
  
    // Layer_1
    this.shape = new cjs.Shape();
    this.shape.graphics.f("#fff").s().p("AEgJaIoKqjIAAKjIkdAAIAAtwIBLBjIAALCICGAAIAAq6IjRkQIAAieIDpAAIIJKjIAAqjIBMAAIAAODIp6s3Ih5AAIAAA4IMBPkIB3AAIAAxoIBNAAIAASzg");
  
    this.timeline.addTween(cjs.Tween.get(this.shape).wait(1));
  
  }).prototype = p = new cjs.MovieClip();
  p.nominalBounds = new cjs.Rectangle(-52,-60.2,104.1,120.5);
  
  
  (lib.Tween31 = function(mode,startPosition,loop) {
    this.initialize(mode,startPosition,loop,{});
  
    // Layer_1
    this.shape = new cjs.Shape();
    this.shape.graphics.f("#fff").s().p("AEgJaIoKqjIAAKjIkdAAIAAtwIBLBjIAALCICGAAIAAq6IjRkQIAAieIDpAAIIJKjIAAqjIBMAAIAAODIp6s3Ih5AAIAAA4IMBPkIB3AAIAAxoIBNAAIAASzg");
  
    this.timeline.addTween(cjs.Tween.get(this.shape).wait(1));
  
  }).prototype = p = new cjs.MovieClip();
  p.nominalBounds = new cjs.Rectangle(-52,-60.2,104.1,120.5);
  
  
  (lib.Tween28 = function(mode,startPosition,loop) {
    this.initialize(mode,startPosition,loop,{});
  
    // Layer_1
    this.shape = new cjs.Shape();
    this.shape.graphics.f("#fff").s().p("AnJJbIAAhNIEMAAQDvAACmiYQCmiYAAjeQAAjcimiZQimiYjvAAIkMAAIAAhNIEMAAQEPAAC8CvQC8CvAAD8QAAD9i8CvQi8CvkPAAgAnJGoIAAswIEMAAQC7AAB9B1QB9B1AACuQAACuh9B1Qh9B1i7AAgAl8FcIC/AAQCdAABmhfQBmhdAAiQQAAiQhmheQhmheidAAIi/AAg");
  
    this.timeline.addTween(cjs.Tween.get(this.shape).wait(1));
  
  }).prototype = p = new cjs.MovieClip();
  p.nominalBounds = new cjs.Rectangle(-45.7,-60.2,91.6,120.5);
  
  
  (lib.Tween27 = function(mode,startPosition,loop) {
    this.initialize(mode,startPosition,loop,{});
  
    // Layer_1
    this.shape = new cjs.Shape();
    this.shape.graphics.f("#fff").s().p("AnJJbIAAhNIEMAAQDvAACmiYQCmiYAAjeQAAjcimiZQimiYjvAAIkMAAIAAhNIEMAAQEPAAC8CvQC8CvAAD8QAAD9i8CvQi8CvkPAAgAnJGoIAAswIEMAAQC7AAB9B1QB9B1AACuQAACuh9B1Qh9B1i7AAgAl8FcIC/AAQCdAABmhfQBmhdAAiQQAAiQhmheQhmheidAAIi/AAg");
  
    this.timeline.addTween(cjs.Tween.get(this.shape).wait(1));
  
  }).prototype = p = new cjs.MovieClip();
  p.nominalBounds = new cjs.Rectangle(-45.7,-60.2,91.6,120.5);
  
  
  (lib.Tween24 = function(mode,startPosition,loop) {
    this.initialize(mode,startPosition,loop,{});
  
    // Layer_1
    this.shape = new cjs.Shape();
    this.shape.graphics.f("#fff").s().p("AnBJaIAAkOIODAAIAAEOgAl1IOILrAAIAAh1IrrAAgAnBBwIAAhMIKsAAIAAh1IqsAAIAAlGIMpAAIAAh2IspAAIAAhMIN0AAIAAEOIsoAAIAACuIKsAAIAAENg");
  
    this.timeline.addTween(cjs.Tween.get(this.shape).wait(1));
  
  }).prototype = p = new cjs.MovieClip();
  p.nominalBounds = new cjs.Rectangle(-45,-60.2,90.1,120.5);
  
  
  (lib.Tween23 = function(mode,startPosition,loop) {
    this.initialize(mode,startPosition,loop,{});
  
    // Layer_1
    this.shape = new cjs.Shape();
    this.shape.graphics.f("#fff").s().p("AnBJaIAAkOIODAAIAAEOgAl1IOILrAAIAAh1IrrAAgAnBBwIAAhMIKsAAIAAh1IqsAAIAAlGIMpAAIAAh2IspAAIAAhMIN0AAIAAEOIsoAAIAACuIKsAAIAAENg");
  
    this.timeline.addTween(cjs.Tween.get(this.shape).wait(1));
  
  }).prototype = p = new cjs.MovieClip();
  p.nominalBounds = new cjs.Rectangle(-45,-60.2,90.1,120.5);
  
  
  (lib.Tween20 = function(mode,startPosition,loop) {
    this.initialize(mode,startPosition,loop,{});
  
    // Layer_1
    this.shape = new cjs.Shape();
    this.shape.graphics.f("#fff").s().p("AB4JaIjgmiIg8AAIAAGiIkdAAIAAnvIBMAAIAAGkICFAAIAAmkIC1AAIDiGkICZAAIjcmFIgVglIAngRQBXgmA0hOQA0hPgBhfQABiEhdhdQhchdiDgBIm6AAIAAhNIG6AAIAaABQCTAKBpBsQBCBDAbBSQAVA9gBBGQAABAgUA6QgMAmgVAkQg1BahbA0IEHHTgAnBgFIAAhMIGXAAQA1AAApgkQAqgmAAgzQAAgzgqgmQgpglg1AAImXAAIAAhMIGXAAQBUgBBAA8QBAA8AABTQAABThAA7QhAA7hUAAg");
  
    this.timeline.addTween(cjs.Tween.get(this.shape).wait(1));
  
  }).prototype = p = new cjs.MovieClip();
  p.nominalBounds = new cjs.Rectangle(-45,-60.2,90.1,120.5);
  
  
  (lib.Tween19 = function(mode,startPosition,loop) {
    this.initialize(mode,startPosition,loop,{});
  
    // Layer_1
    this.shape = new cjs.Shape();
    this.shape.graphics.f("#fff").s().p("AB4JaIjgmiIg8AAIAAGiIkdAAIAAnvIBMAAIAAGkICFAAIAAmkIC1AAIDiGkICZAAIjcmFIgVglIAngRQBXgmA0hOQA0hPgBhfQABiEhdhdQhchdiDgBIm6AAIAAhNIG6AAIAaABQCTAKBpBsQBCBDAbBSQAVA9gBBGQAABAgUA6QgMAmgVAkQg1BahbA0IEHHTgAnBgFIAAhMIGXAAQA1AAApgkQAqgmAAgzQAAgzgqgmQgpglg1AAImXAAIAAhMIGXAAQBUgBBAA8QBAA8AABTQAABThAA7QhAA7hUAAg");
  
    this.timeline.addTween(cjs.Tween.get(this.shape).wait(1));
  
  }).prototype = p = new cjs.MovieClip();
  p.nominalBounds = new cjs.Rectangle(-45,-60.2,90.1,120.5);
  
  
  (lib.Tween16 = function(mode,startPosition,loop) {
    this.initialize(mode,startPosition,loop,{});
  
    // Layer_1
    this.shape = new cjs.Shape();
    this.shape.graphics.f("#fff").s().p("AlGILQgugdgjglQgVgVgJgNIgGgJIA9guIAGAKQAJANAQAQQAfAeAnAaQB5BPCfAAQBcAABHgYQA9gVAtgmQA4guAZhAQATg0AAg/QAAiNhqhLQgYgRgogQIgkgPQgygThGgUQh+gkgugcQhNgvAAhTQAAg/A2guQA8gzBcAAQBMABBCAfIAVAMQAUALAQAOIASARIBThTQgpgqg6gfIgCgBQgRgJgTgHQhMgehXAAQiWAAheBOQhfBMgBB7QAACNBrBLQA+AsCdArQB/AkAuAdIAEACQAWAOAOAQQAlApgBA4QABA/glAtQgMAQgRANIgHAFQhBAwhiAAQhxAAhYgyQg2gfgjgqIA+gvQAZAhArAXQBGAnBaAAQBIAAAsggQAsgfAAg0QAAgpgrgZQgmgXhxggQivgxhNg4QiCheAAixQAAidB0hiQBAg3BVgYQBFgUBRAAQBOAABGAUIAQAEQATAGARAIQA6AXAzAlQA5AqAfAqIAKAPIi8C8IgTgTQgPgPgVgPIgLgIQhQg6hJAAQg5AAgmAaQgjAaAAAfQAAApArAZQAlAXByAgQA3AQAtAQIAkANQBKAdApAfQAzAkAfAwQAxBOABBtQAACvh6BpQhEA6hdAaQhKAVhZAAQi3AAiOhfg");
  
    this.timeline.addTween(cjs.Tween.get(this.shape).wait(1));
  
  }).prototype = p = new cjs.MovieClip();
  p.nominalBounds = new cjs.Rectangle(-44.4,-61.8,88.9,123.6);
  
  
  (lib.Tween15 = function(mode,startPosition,loop) {
    this.initialize(mode,startPosition,loop,{});
  
    // Layer_1
    this.shape = new cjs.Shape();
    this.shape.graphics.f("#fff").s().p("AlGILQgugdgjglQgVgVgJgNIgGgJIA9guIAGAKQAJANAQAQQAfAeAnAaQB5BPCfAAQBcAABHgYQA9gVAtgmQA4guAZhAQATg0AAg/QAAiNhqhLQgYgRgogQIgkgPQgygThGgUQh+gkgugcQhNgvAAhTQAAg/A2guQA8gzBcAAQBMABBCAfIAVAMQAUALAQAOIASARIBThTQgpgqg6gfIgCgBQgRgJgTgHQhMgehXAAQiWAAheBOQhfBMgBB7QAACNBrBLQA+AsCdArQB/AkAuAdIAEACQAWAOAOAQQAlApgBA4QABA/glAtQgMAQgRANIgHAFQhBAwhiAAQhxAAhYgyQg2gfgjgqIA+gvQAZAhArAXQBGAnBaAAQBIAAAsggQAsgfAAg0QAAgpgrgZQgmgXhxggQivgxhNg4QiCheAAixQAAidB0hiQBAg3BVgYQBFgUBRAAQBOAABGAUIAQAEQATAGARAIQA6AXAzAlQA5AqAfAqIAKAPIi8C8IgTgTQgPgPgVgPIgLgIQhQg6hJAAQg5AAgmAaQgjAaAAAfQAAApArAZQAlAXByAgQA3AQAtAQIAkANQBKAdApAfQAzAkAfAwQAxBOABBtQAACvh6BpQhEA6hdAaQhKAVhZAAQi3AAiOhfg");
  
    this.timeline.addTween(cjs.Tween.get(this.shape).wait(1));
  
  }).prototype = p = new cjs.MovieClip();
  p.nominalBounds = new cjs.Rectangle(-44.4,-61.8,88.9,123.6);
  
  
  (lib.Tween12 = function(mode,startPosition,loop) {
    this.initialize(mode,startPosition,loop,{});
  
    // Layer_1
    this.shape = new cjs.Shape();
    this.shape.graphics.f("#fff").s().p("AjaJGQhqglhXhHIAwg7QBNA+BdAhQBdAhBjgBQDqAAChidQChidAAjkQAAjjihidQihidjqAAQjbAAieCLQidCLgVDQIhLgIQAHhDATg8QAwibCAhyQC0ieD4AAQEKAAC3CzQC4CzAAEDIgBAAQAAEEi3CzQi4CzkJAAIgGAAQhtAAhmgkgAgJGoQiugCh5h8Qh6h9ACitQAAivB8h8QB8h9CwAAQCuAAB9B9QB8B8AACvIAAAIQgCCuh8B5Qh8B6itgCIgBABgAj7j0QhjBmABCOIAAAMQACCQBoBkQBoBkCPgCQCQgDBkhoQBkhngCiQIAAgHQgCiOhlhkQhmhkiOACIgHAAQiPABhkBmg");
  
    this.timeline.addTween(cjs.Tween.get(this.shape).wait(1));
  
  }).prototype = p = new cjs.MovieClip();
  p.nominalBounds = new cjs.Rectangle(-63.1,-61.8,126.4,123.6);
  
  
  (lib.Tween11 = function(mode,startPosition,loop) {
    this.initialize(mode,startPosition,loop,{});
  
    // Layer_1
    this.shape = new cjs.Shape();
    this.shape.graphics.f("#fff").s().p("AjaJGQhqglhXhHIAwg7QBNA+BdAhQBdAhBjgBQDqAAChidQChidAAjkQAAjjihidQihidjqAAQjbAAieCLQidCLgVDQIhLgIQAHhDATg8QAwibCAhyQC0ieD4AAQEKAAC3CzQC4CzAAEDIgBAAQAAEEi3CzQi4CzkJAAIgGAAQhtAAhmgkgAgJGoQiugCh5h8Qh6h9ACitQAAivB8h8QB8h9CwAAQCuAAB9B9QB8B8AACvIAAAIQgCCuh8B5Qh8B6itgCIgBABgAj7j0QhjBmABCOIAAAMQACCQBoBkQBoBkCPgCQCQgDBkhoQBkhngCiQIAAgHQgCiOhlhkQhmhkiOACIgHAAQiPABhkBmg");
  
    this.timeline.addTween(cjs.Tween.get(this.shape).wait(1));
  
  }).prototype = p = new cjs.MovieClip();
  p.nominalBounds = new cjs.Rectangle(-63.1,-61.8,126.4,123.6);
  
  
  (lib.Tween8 = function(mode,startPosition,loop) {
    this.initialize(mode,startPosition,loop,{});
  
    // Layer_1
    this.shape = new cjs.Shape();
    this.shape.graphics.f("#fff").s().p("AEgJaIoKqjIAAKjIhMAAIAAuDIJ7M3IB4AAIAAg5IsAviIh5AAIAARnIhMAAIAAyzIDqAAILaOzIAAtnIiFAAIAAIYIhMhiIAAoCIEeAAIAASzg");
  
    this.timeline.addTween(cjs.Tween.get(this.shape).wait(1));
  
  }).prototype = p = new cjs.MovieClip();
  p.nominalBounds = new cjs.Rectangle(-52.1,-60.2,104.2,120.5);
  
  
  (lib.Tween7 = function(mode,startPosition,loop) {
    this.initialize(mode,startPosition,loop,{});
  
    // Layer_1
    this.shape = new cjs.Shape();
    this.shape.graphics.f("#fff").s().p("AEgJaIoKqjIAAKjIhMAAIAAuDIJ7M3IB4AAIAAg5IsAviIh5AAIAARnIhMAAIAAyzIDqAAILaOzIAAtnIiFAAIAAIYIhMhiIAAoCIEeAAIAASzg");
  
    this.timeline.addTween(cjs.Tween.get(this.shape).wait(1));
  
  }).prototype = p = new cjs.MovieClip();
  p.nominalBounds = new cjs.Rectangle(-52.1,-60.2,104.2,120.5);
  
  
  (lib.Symbol1 = function(mode,startPosition,loop) {
    this.initialize(mode,startPosition,loop,{});
  
    // N mask copy (mask)
    var mask = new cjs.Shape();
    mask._off = true;
    var mask_graphics_1 = new cjs.Graphics().p("AghgGIAlhKIAdAlIABB8IAAABg");
    var mask_graphics_2 = new cjs.Graphics().p("Ag6gmIAohJIBLBjIABB7IAAABg");
    var mask_graphics_3 = new cjs.Graphics().p("AhNhDIArhDIBvCQIABB9IAAAAg");
    var mask_graphics_4 = new cjs.Graphics().p("AhkhhIAuhBICaDIIABB8IgBABg");
    var mask_graphics_5 = new cjs.Graphics().p("Ah8h+IArhEIDMEIIABB9IAAAAg");
    var mask_graphics_6 = new cjs.Graphics().p("AiSibIAqhFID6FEIABB9IAAAAg");
    var mask_graphics_7 = new cjs.Graphics().p("Airi5IAnhKIEvGJIABB9IAAABg");
    var mask_graphics_8 = new cjs.Graphics().p("AjGjeIAohHIFkHOIABB8IAAABg");
    var mask_graphics_9 = new cjs.Graphics().p("Ajdj5IAmhLIGUIMIABB8IAAABg");
    var mask_graphics_10 = new cjs.Graphics().p("Aj4kdIAlhKIHLJSIABB9IAAABg");
    var mask_graphics_11 = new cjs.Graphics().p("AkVlAIAjhOIABAAIIGKgIABB8IAAABg");
    var mask_graphics_12 = new cjs.Graphics().p("Aj+lCIgvAAIAAhMIBUAAIIGKgIABB8IAAABg");
    var mask_graphics_13 = new cjs.Graphics().p("AjZlCIh4AAIAAADIgBAAIAAhPICeAAIIGKgIABB8IAAABg");
    var mask_graphics_14 = new cjs.Graphics().p("Ai0lCIh4AAIAAADIhMAAIAAhPIDpAAIIHKgIABB8IgBABg");
    var mask_graphics_15 = new cjs.Graphics().p("Ai0lCIh4AAIAABYIhMAAIAAikIDpAAIIHKgIABB8IgBABg");
    var mask_graphics_16 = new cjs.Graphics().p("Ai0lCIh4AAIAACgIhMAAIAAjsIDpAAIIHKgIABB8IgBABg");
    var mask_graphics_17 = new cjs.Graphics().p("Ai0lCIh4AAIAADmIhMAAIAAkyIDpAAIIHKgIABB8IgBABg");
    var mask_graphics_18 = new cjs.Graphics().p("Ai0lCIh4AAIAAEuIhMAAIAAl6IDpAAIIHKgIABB8IgBABg");
    var mask_graphics_19 = new cjs.Graphics().p("Ai0lCIh4AAIAAF1IhMAAIAAnBIDpAAIIHKgIABB8IgBABg");
    var mask_graphics_20 = new cjs.Graphics().p("Ai0lCIh4AAIAAG7IhMAAIAAoHIDpAAIIHKgIABB8IgBABg");
    var mask_graphics_21 = new cjs.Graphics().p("Ai0lCIh4AAIAAH+IhMAAIAApKIDpAAIIHKgIABB8IgBABg");
    var mask_graphics_22 = new cjs.Graphics().p("Ai0lCIh4AAIAAJDIhMAAIAAqPIDpAAIIHKgIABB8IgBABg");
    var mask_graphics_23 = new cjs.Graphics().p("Ai0lCIh4AAIAAKQIhMAAIAArcIDpAAIIHKgIABB8IgBABg");
    var mask_graphics_24 = new cjs.Graphics().p("Al4GQIAAsfIDpAAIIHKgIABB8IgBABIosrRIh4AAIAALTg");
    var mask_graphics_25 = new cjs.Graphics().p("Al4G2IAAtrIDpAAIIHKgIABB8IgBABIosrRIh4AAIAAMfg");
    var mask_graphics_26 = new cjs.Graphics().p("Al4HVIAAupIDpAAIIHKfIABB9IgBAAIosrRIh4AAIAANeg");
    var mask_graphics_27 = new cjs.Graphics().p("Al4H4IAAvuIDpAAIIHKfIABB8IgBABIosrRIh4AAIAAOjg");
    var mask_graphics_28 = new cjs.Graphics().p("Al4IcIAAw2IDpAAIIHKfIABB8IgBABIosrRIh4AAIAAPrg");
    var mask_graphics_29 = new cjs.Graphics().p("Al4I5IAAxxIDpAAIIHKfIABB9IgBAAIosrRIh4AAIAAQmg");
    var mask_graphics_30 = new cjs.Graphics().p("Al4JaIAAyzIDpAAIIHKfIABB9IgBAAIosrRIh4AAIAARog");
    var mask_graphics_31 = new cjs.Graphics().p("AimJaIAAgyIBMAAIAAAygAl4JaIAAyzIDpAAIIHKfIABB9IgBAAIosrRIh4AAIAARog");
    var mask_graphics_32 = new cjs.Graphics().p("AimJaIAAhvIBMAAIAABvgAl4JaIAAyzIDpAAIIHKfIABB9IgBAAIosrRIh4AAIAARog");
    var mask_graphics_33 = new cjs.Graphics().p("AimJaIAAioIBMAAIAACogAl4JaIAAyzIDpAAIIHKfIABB9IgBAAIosrRIh4AAIAARog");
    var mask_graphics_34 = new cjs.Graphics().p("AimJaIAAjiIBMAAIAADigAl4JaIAAyzIDpAAIIHKfIABB9IgBAAIosrRIh4AAIAARog");
    var mask_graphics_35 = new cjs.Graphics().p("AimJaIAAkoIBMAAIAAEogAl4JaIAAyzIDpAAIIHKfIABB9IgBAAIosrRIh4AAIAARog");
    var mask_graphics_36 = new cjs.Graphics().p("AimJaIAAmFIBMAAIAAGFgAl4JaIAAyzIDpAAIIHKfIABB9IgBAAIosrRIh4AAIAARog");
    var mask_graphics_37 = new cjs.Graphics().p("AimJaIAAnTIBMAAIAAHTgAl4JaIAAyzIDpAAIIHKfIABB9IgBAAIosrRIh4AAIAARog");
    var mask_graphics_38 = new cjs.Graphics().p("AimJaIAAokIBMAAIAAIkgAl4JaIAAyzIDpAAIIHKfIABB9IgBAAIosrRIh4AAIAARog");
    var mask_graphics_39 = new cjs.Graphics().p("AimJaIAAp0IBMAAIAAJ0gAl4JaIAAyzIDpAAIIHKfIABB9IgBAAIosrRIh4AAIAARog");
    var mask_graphics_40 = new cjs.Graphics().p("AimJaIAArlIBMBEIAAKhgAl4JaIAAyzIDpAAIIHKfIABB9IgBAAIosrRIh4AAIAARog");
    var mask_graphics_41 = new cjs.Graphics().p("AimJaIAAuDIBMBjIAAB9IAAAAIAAKjgAl4JaIAAyzIDpAAIIHKfIABB9IgBAAIosrRIh4AAIAARog");
    var mask_graphics_42 = new cjs.Graphics().p("AimJaIAAuDIB0CXIAAB9Igog0IAAKjgAl4JaIAAyzIDpAAIIHKfIABB9IgBAAIosrRIh4AAIAARog");
    var mask_graphics_43 = new cjs.Graphics().p("AimJaIAAuDIChDRIAAB8IhVhtIAAKjgAl4JaIAAyzIDpAAIIHKfIABB9IgBAAIosrRIh4AAIAARog");
    var mask_graphics_44 = new cjs.Graphics().p("AimJaIAAuDIDdEgIAAB8IiRi8IAAKjgAl4JaIAAyzIDpAAIIHKfIABB9IgBAAIosrRIh4AAIAARog");
    var mask_graphics_45 = new cjs.Graphics().p("AimJaIAAuDIEbFwIAAB9IjPkNIAAKjgAl4JaIAAyzIDpAAIIHKfIABB9IgBAAIosrRIh4AAIAARog");
    var mask_graphics_46 = new cjs.Graphics().p("AimJaIAAuDIFcHDIAAB9IkQlgIAAKjgAl4JaIAAyzIDpAAIIHKfIABB9IgBAAIosrRIh4AAIAARog");
    var mask_graphics_47 = new cjs.Graphics().p("AimJaIAAuDIGUIMIAAB9IlImpIAAKjgAl4JaIAAyzIDpAAIIHKfIABB9IgBAAIosrRIh4AAIAARog");
    var mask_graphics_48 = new cjs.Graphics().p("AimJaIAAuDIHUJfIAAB9ImIn8IAAKjgAl4JaIAAyzIDpAAIIHKfIABB9IgBAAIosrRIh4AAIAARog");
    var mask_graphics_49 = new cjs.Graphics().p("AimJaIAAuDIIWK1IAAB9InKpSIAAKjgAl4JaIAAyzIDpAAIIHKfIABB9IgBAAIosrRIh4AAIAARog");
    var mask_graphics_50 = new cjs.Graphics().p("AGUJaIoKqjIAAKjIhMAAIAAuDIJXMIIAAB7gAmTJaIAAyzIDpAAIIGKfIABB9IAAAAIosrRIh5AAIAARog");
    var mask_graphics_51 = new cjs.Graphics().p("AFqJaIoKqjIAAKjIhMAAIAAuDIJ7M3IAvAAIAABMgAm9JaIAAyzIDpAAIIGKfIABB9IAAAAIosrRIh5AAIAARog");
    var mask_graphics_52 = new cjs.Graphics().p("AFIJaIoKqjIAAKjIhMAAIAAuDIJ7M3IBzAAIAABMgAnfJaIAAyzIDpAAIIGKfIABB9IAAAAIosrRIh4AAIAARog");
    var mask_graphics_53 = new cjs.Graphics().p("AEgJaIoKqjIAAKjIhMAAIAAuDIJ7M3IDEAAIAABMgAoHJaIAAyzIDpAAIIGKfIABB9IAAAAIosrRIh5AAIAARog");
    var mask_graphics_54 = new cjs.Graphics().p("AEgJaIoKqjIAAKjIhMAAIAAuDIJ7M3IB3AAIAAg4IgCgEIBPAAIAACIgAoHJaIAAyzIDpAAIIGKfIABB9IAAAAIosrRIh5AAIAARog");
    var mask_graphics_55 = new cjs.Graphics().p("AEgJaIoKqjIAAKjIhMAAIAAuDIJ7M3IB3AAIAAg0IAuhFIAfAnIAACegAoHJaIAAyzIDpAAIIGKfIABB9IAAAAIosrRIh5AAIAARog");
    var mask_graphics_56 = new cjs.Graphics().p("AEgJaIoKqjIAAKjIhMAAIAAuDIJ7M3IB3AAIAAg4IgcgmIAAh9IBpCJIAACegAoHJaIAAyzIDpAAIIGKfIABB9IAAAAIosrRIh5AAIAARog");
    var mask_graphics_57 = new cjs.Graphics().p("AEgJaIoKqjIAAKjIhMAAIAAuDIJ7M3IB3AAIAAg4Ihbh2IACh7ICmDXIAACegAoHJaIAAyzIDpAAIIGKfIABB9IAAAAIosrRIh5AAIAARog");
    var mask_graphics_58 = new cjs.Graphics().p("AEgJaIoKqjIAAKjIhMAAIAAuDIJ7M3IB3AAIAAg4IiMi2IAGh5IABAAIAAAFIDSEQIAACegAoHJaIAAyzIDpAAIIGKfIABB9IAAAAIosrRIh5AAIAARog");
    var mask_graphics_59 = new cjs.Graphics().p("AEgJaIoKqjIAAKjIhMAAIAAuDIJ7M3IB3AAIAAg4Ir/vkIh5AAIAARoIhLAAIAAyzIDpAAIIIKhIAAAAIABACIAAgCIACAAIBKBfIAAAFIDSEQIAACeg");
    var mask_graphics_60 = new cjs.Graphics().p("AEgJaIoKqjIAAKjIhMAAIAAuDIJ7M3IB3AAIAAg4Ir/vkIh5AAIAARoIhLAAIAAyzIDpAAIIIKhIAAAAIABACIAAgCIBMAAIAABkIDSEQIAACeg");
    var mask_graphics_61 = new cjs.Graphics().p("AEgJaIoKqjIAAKjIhMAAIAAuDIJ7M3IB3AAIAAg4Ir/vkIh5AAIAARoIhLAAIAAyzIDpAAIIJKjIAAhSIBMAAIAAC0IDSEQIAACeg");
    var mask_graphics_62 = new cjs.Graphics().p("AEgJaIoKqjIAAKjIhMAAIAAuDIJ7M3IB3AAIAAg4Ir/vkIh5AAIAARoIhLAAIAAyzIDpAAIIJKjIAAiSIBMAAIAAD0IDSEQIAACeg");
    var mask_graphics_63 = new cjs.Graphics().p("AEgJaIoKqjIAAKjIhMAAIAAuDIJ7M3IB3AAIAAg4Ir/vkIh5AAIAARoIhLAAIAAyzIDpAAIIJKjIAAjUIBMAAIAAE2IDSEQIAACeg");
    var mask_graphics_64 = new cjs.Graphics().p("AEgJaIoKqjIAAKjIhMAAIAAuDIJ7M3IB3AAIAAg4Ir/vkIh5AAIAARoIhLAAIAAyzIDpAAIIJKjIAAkjIBMAAIAAGFIDSEQIAACeg");
    var mask_graphics_65 = new cjs.Graphics().p("AEgJaIoKqjIAAKjIhMAAIAAuDIJ7M3IB3AAIAAg4Ir/vkIh5AAIAARoIhLAAIAAyzIDpAAIIJKjIAAltIBMAAIAAHPIDSEQIAACeg");
    var mask_graphics_66 = new cjs.Graphics().p("AEgJaIoKqjIAAKjIhMAAIAAuDIJ7M3IB3AAIAAg4Ir/vkIh5AAIAARoIhLAAIAAyzIDpAAIIJKjIAAm/IBMAAIAAIhIDSEQIAACeg");
    var mask_graphics_67 = new cjs.Graphics().p("AEgJaIoKqjIAAKjIhMAAIAAuDIJ7M3IB3AAIAAg4Ir/vkIh5AAIAARoIhLAAIAAyzIDpAAIIJKjIAAoJIBMAAIAAJrIDSEQIAACeg");
    var mask_graphics_68 = new cjs.Graphics().p("AEgJaIoKqjIAAKjIhMAAIAAuDIJ7M3IB3AAIAAg4Ir/vkIh5AAIAARoIhLAAIAAyzIDpAAIIJKjIAApUIBMAAIAAK2IDSEQIAACeg");
    var mask_graphics_69 = new cjs.Graphics().p("AEgJaIoKqjIAAKjIhMAAIAAuDIJ7M3IB3AAIAAg4Ir/vkIh5AAIAARoIhLAAIAAyzIDpAAIIJKjIAAqjIBMAAIAABLIAAAAIAAK6IDSEQIAACeg");
    var mask_graphics_70 = new cjs.Graphics().p("AEgJaIoKqjIAAKjIhMAAIAAuDIJ7M3IB3AAIAAg4Ir/vkIh5AAIAARoIhLAAIAAyzIDpAAIIJKjIAAqjICJAAIAABLIg9AAIAAK6IDSEQIAACeg");
    var mask_graphics_71 = new cjs.Graphics().p("AEgJaIoKqjIAAKjIhMAAIAAuDIJ7M3IB3AAIAAg4Ir/vkIh5AAIAARoIhLAAIAAyzIDpAAIIJKjIAAqjIDPAAIAABLIiDAAIAAK6IDSEQIAACeg");
    var mask_graphics_72 = new cjs.Graphics().p("AEgJaIoKqjIAAKjIhMAAIAAuDIJ7M3IB3AAIAAg4Ir/vkIh5AAIAARoIhLAAIAAyzIDpAAIIJKjIAAqjIEeAAIAABMIhNAAIAAgBIiFAAIAAK6IDSEQIAACeg");
    var mask_graphics_73 = new cjs.Graphics().p("AEgJaIoKqjIAAKjIhMAAIAAuDIJ7M3IB3AAIAAg4Ir/vkIh5AAIAARoIhLAAIAAyzIDpAAIIJKjIAAqjIEeAAIAACRIhNAAIAAhGIiFAAIAAK6IDSEQIAACeg");
    var mask_graphics_74 = new cjs.Graphics().p("AEgJaIoKqjIAAKjIhMAAIAAuDIJ7M3IB3AAIAAg4Ir/vkIh5AAIAARoIhLAAIAAyzIDpAAIIJKjIAAqjIEeAAIAADZIhNAAIAAiOIiFAAIAAK6IDSEQIAACeg");
    var mask_graphics_75 = new cjs.Graphics().p("AEgJaIoKqjIAAKjIhMAAIAAuDIJ7M3IB3AAIAAg4Ir/vkIh5AAIAARoIhLAAIAAyzIDpAAIIJKjIAAqjIEeAAIAAEgIhNAAIAAjVIiFAAIAAK6IDSEQIAACeg");
    var mask_graphics_76 = new cjs.Graphics().p("AEgJaIoKqjIAAKjIhMAAIAAuDIJ7M3IB3AAIAAg4Ir/vkIh5AAIAARoIhLAAIAAyzIDpAAIIJKjIAAqjIEeAAIAAF0IhNAAIAAkpIiFAAIAAK6IDSEQIAACeg");
    var mask_graphics_77 = new cjs.Graphics().p("AEgJaIoKqjIAAKjIhMAAIAAuDIJ7M3IB3AAIAAg4Ir/vkIh5AAIAARoIhLAAIAAyzIDpAAIIJKjIAAqjIEeAAIAAHRIhNAAIAAmGIiFAAIAAK6IDSEQIAACeg");
    var mask_graphics_78 = new cjs.Graphics().p("AEgJaIoKqjIAAKjIhMAAIAAuDIJ7M3IB3AAIAAg4Ir/vkIh5AAIAARoIhLAAIAAyzIDpAAIIJKjIAAqjIEeAAIAAIrIhNAAIAAngIiFAAIAAK6IDSEQIAACeg");
    var mask_graphics_79 = new cjs.Graphics().p("AEgJaIoKqjIAAKjIhMAAIAAuDIJ7M3IB3AAIAAg4Ir/vkIh5AAIAARoIhLAAIAAyzIDpAAIIJKjIAAqjIEeAAIAAJyIhNAAIAAonIiFAAIAAK6IDSEQIAACeg");
    var mask_graphics_80 = new cjs.Graphics().p("AEgJaIoKqjIAAKjIhMAAIAAuDIJ7M3IB3AAIAAg4Ir/vkIh5AAIAARoIhLAAIAAyzIDpAAIIJKjIAAqjIEeAAIAAMGIhNAAIAAq7IiFAAIAAK6IDSEQIAACeg");
    var mask_graphics_81 = new cjs.Graphics().p("AEfJaIoJqjIAAKjIhMAAIAAuDIJ6M3IB4AAIAAg4IsAvkIh4AAIAARoIhMAAIAAyzIDpAAIIKKjIAAqjIEdAAIABNRIhNAAIAAsGIiFAAIAAK6IDREQIAACeg");
    var mask_graphics_82 = new cjs.Graphics().p("AEfJaIoJqjIAAKjIhMAAIAAuDIJ6M3IB4AAIAAg4IsAvkIh4AAIAARoIhMAAIAAyzIDpAAIIKKjIAAqjIEdAAIABPOIg4AAIA3BHIAACegAE3CsICFCtIAAtnIiFAAg");
    var mask_graphics_83 = new cjs.Graphics().p("AEgJaIoKqjIAAKjIhMAAIAAuDIJ7M3IB3AAIAAg4Ir/vkIh5AAIAARoIhLAAIAAyzIDpAAIIJKjIAAqjIEeAAIAASzgAE3CsICFCtIAAtnIiFAAg");
    var mask_graphics_119 = new cjs.Graphics().p("AEgJaIoKqjIAAKjIhMAAIAAuDIJ7M3IB3AAIAAg4Ir/vkIh5AAIAARoIhLAAIAAyzIDpAAIIJKjIAAqjIEeAAIAASzgAE3CsICFCtIAAtnIiFAAg");
    var mask_graphics_120 = new cjs.Graphics().p("AEgJaIoKqjIAAKjIhMAAIAAuDIJ7M3IB3AAIAAg4Ir/vkIh5AAIAARoIhLAAIAAyzIDpAAIIJKjIAAqjIEeAAIAASzgAE3CsICFCtIAAtnIiFAAg");
    var mask_graphics_121 = new cjs.Graphics().p("AEgJaIoKqjIAAKjIhMAAIAAuDIJ7M3IB3AAIAAg4Ir/vkIh5AAIAARoIhLAAIAAyzIDpAAIIJKjIAAqjIEeAAIAASzgAE3CsICFCtIAAtnIiFAAg");
    var mask_graphics_122 = new cjs.Graphics().p("AEgJaIoKqjIAAKjIhMAAIAAuDIJ7M3IB3AAIAAg4Ir/vkIh5AAIAARoIhLAAIAAyzIDpAAIIJKjIAAqjIEeAAIAASzgAE3CsICFCtIAAtnIiFAAg");
    var mask_graphics_123 = new cjs.Graphics().p("AEgJaIoKqjIAAKjIhMAAIAAuDIJ7M3IB3AAIAAg4Ir/vkIh5AAIAARoIhLAAIAAyzIDpAAIIJKjIAAqjIEeAAIAASzgAE3CsICFCtIAAtnIiFAAg");
    var mask_graphics_124 = new cjs.Graphics().p("AEgJaIoKqjIAAKjIhMAAIAAuDIJ7M3IB3AAIAAg4Ir/vkIh5AAIAARoIhLAAIAAyzIDpAAIIJKjIAAqjIEeAAIAASzgAE3CsICFCtIAAtnIiFAAg");
    var mask_graphics_125 = new cjs.Graphics().p("AEgJaIoKqjIAAKjIhMAAIAAuDIJ7M3IB3AAIAAg4Ir/vkIh5AAIAARoIhLAAIAAyzIDpAAIIJKjIAAqjIEeAAIAASzgAE3CsICFCtIAAtnIiFAAg");
    var mask_graphics_126 = new cjs.Graphics().p("AEgJaIoKqjIAAKjIhMAAIAAuDIJ7M3IB3AAIAAg4Ir/vkIh5AAIAARoIhLAAIAAyzIDpAAIIJKjIAAqjIEeAAIAASzgAE3CsICFCtIAAtnIiFAAg");
    var mask_graphics_127 = new cjs.Graphics().p("AEgJaIoKqjIAAKjIhMAAIAAuDIJ7M3IB3AAIAAg4Ir/vkIh5AAIAARoIhLAAIAAyzIDpAAIIJKjIAAqjIEeAAIAASzgAE3CsICFCtIAAtnIiFAAg");
    var mask_graphics_128 = new cjs.Graphics().p("AEgJaIoKqjIAAKjIhMAAIAAuDIJ7M3IB3AAIAAg4Ir/vkIh5AAIAARoIhLAAIAAyzIDpAAIIJKjIAAqjIEeAAIAASzgAE3CsICFCtIAAtnIiFAAg");
    var mask_graphics_129 = new cjs.Graphics().p("AEgJaIoKqjIAAKjIhMAAIAAuDIJ7M3IB3AAIAAg4Ir/vkIh5AAIAARoIhLAAIAAyzIDpAAIIJKjIAAqjIEeAAIAASzgAE3CsICFCtIAAtnIiFAAg");
    var mask_graphics_130 = new cjs.Graphics().p("AEgJaIoKqjIAAKjIhMAAIAAuDIJ7M3IB3AAIAAg4Ir/vkIh5AAIAARoIhLAAIAAyzIDpAAIIJKjIAAqjIEeAAIAASzgAE3CsICFCtIAAtnIiFAAg");
    var mask_graphics_131 = new cjs.Graphics().p("AEgJaIoKqjIAAKjIhMAAIAAuDIJ7M3IB3AAIAAg4Ir/vkIh5AAIAARoIhLAAIAAyzIDpAAIIJKjIAAqjIEeAAIAASzgAE3CsICFCtIAAtnIiFAAg");
    var mask_graphics_132 = new cjs.Graphics().p("AEgJaIoKqjIAAKjIhMAAIAAuDIJ7M3IB3AAIAAg4Ir/vkIh5AAIAARoIhLAAIAAyzIDpAAIIJKjIAAqjIEeAAIAASzgAE3CsICFCtIAAtnIiFAAg");
    var mask_graphics_133 = new cjs.Graphics().p("AEgJaIoKqjIAAKjIhMAAIAAuDIJ7M3IB3AAIAAg4Ir/vkIh5AAIAARoIhLAAIAAyzIDpAAIIJKjIAAqjIEeAAIAASzgAE3CsICFCtIAAtnIiFAAg");
    var mask_graphics_134 = new cjs.Graphics().p("AEgJaIoKqjIAAKjIhMAAIAAuDIJ7M3IB3AAIAAg4Ir/vkIh5AAIAARoIhLAAIAAyzIDpAAIIJKjIAAqjIEeAAIAASzgAE3CsICFCtIAAtnIiFAAg");
  
    this.timeline.addTween(cjs.Tween.get(mask).to({graphics:null,x:0,y:0}).wait(1).to({graphics:mask_graphics_1,x:967.8,y:73.2}).wait(1).to({graphics:mask_graphics_2,x:965.4,y:70.2}).wait(1).to({graphics:mask_graphics_3,x:963.4,y:67.8}).wait(1).to({graphics:mask_graphics_4,x:961.1,y:65.1}).wait(1).to({graphics:mask_graphics_5,x:958.8,y:61.8}).wait(1).to({graphics:mask_graphics_6,x:956.5,y:58.8}).wait(1).to({graphics:mask_graphics_7,x:954,y:55.4}).wait(1).to({graphics:mask_graphics_8,x:951.3,y:52}).wait(1).to({graphics:mask_graphics_9,x:949,y:48.9}).wait(1).to({graphics:mask_graphics_10,x:946.3,y:45.3}).wait(1).to({graphics:mask_graphics_11,x:943.4,y:41.5}).wait(1).to({graphics:mask_graphics_12,x:941,y:41.5}).wait(1).to({graphics:mask_graphics_13,x:937.3,y:41.5}).wait(1).to({graphics:mask_graphics_14,x:933.5,y:41.5}).wait(1).to({graphics:mask_graphics_15,x:933.5,y:41.5}).wait(1).to({graphics:mask_graphics_16,x:933.5,y:41.5}).wait(1).to({graphics:mask_graphics_17,x:933.5,y:41.5}).wait(1).to({graphics:mask_graphics_18,x:933.5,y:41.5}).wait(1).to({graphics:mask_graphics_19,x:933.5,y:41.5}).wait(1).to({graphics:mask_graphics_20,x:933.5,y:41.5}).wait(1).to({graphics:mask_graphics_21,x:933.5,y:41.5}).wait(1).to({graphics:mask_graphics_22,x:933.5,y:41.5}).wait(1).to({graphics:mask_graphics_23,x:933.5,y:41.5}).wait(1).to({graphics:mask_graphics_24,x:933.5,y:41.6}).wait(1).to({graphics:mask_graphics_25,x:933.5,y:45.4}).wait(1).to({graphics:mask_graphics_26,x:933.5,y:48.5}).wait(1).to({graphics:mask_graphics_27,x:933.5,y:52}).wait(1).to({graphics:mask_graphics_28,x:933.5,y:55.6}).wait(1).to({graphics:mask_graphics_29,x:933.5,y:58.5}).wait(1).to({graphics:mask_graphics_30,x:933.5,y:61.8}).wait(1).to({graphics:mask_graphics_31,x:933.5,y:61.8}).wait(1).to({graphics:mask_graphics_32,x:933.5,y:61.8}).wait(1).to({graphics:mask_graphics_33,x:933.5,y:61.8}).wait(1).to({graphics:mask_graphics_34,x:933.5,y:61.8}).wait(1).to({graphics:mask_graphics_35,x:933.5,y:61.8}).wait(1).to({graphics:mask_graphics_36,x:933.5,y:61.8}).wait(1).to({graphics:mask_graphics_37,x:933.5,y:61.8}).wait(1).to({graphics:mask_graphics_38,x:933.5,y:61.8}).wait(1).to({graphics:mask_graphics_39,x:933.5,y:61.8}).wait(1).to({graphics:mask_graphics_40,x:933.5,y:61.8}).wait(1).to({graphics:mask_graphics_41,x:933.5,y:61.8}).wait(1).to({graphics:mask_graphics_42,x:933.5,y:61.8}).wait(1).to({graphics:mask_graphics_43,x:933.5,y:61.8}).wait(1).to({graphics:mask_graphics_44,x:933.5,y:61.8}).wait(1).to({graphics:mask_graphics_45,x:933.5,y:61.8}).wait(1).to({graphics:mask_graphics_46,x:933.5,y:61.8}).wait(1).to({graphics:mask_graphics_47,x:933.5,y:61.8}).wait(1).to({graphics:mask_graphics_48,x:933.5,y:61.8}).wait(1).to({graphics:mask_graphics_49,x:933.5,y:61.8}).wait(1).to({graphics:mask_graphics_50,x:936.3,y:61.8}).wait(1).to({graphics:mask_graphics_51,x:940.5,y:61.8}).wait(1).to({graphics:mask_graphics_52,x:943.9,y:61.8}).wait(1).to({graphics:mask_graphics_53,x:947.9,y:61.8}).wait(1).to({graphics:mask_graphics_54,x:947.9,y:61.8}).wait(1).to({graphics:mask_graphics_55,x:947.9,y:61.8}).wait(1).to({graphics:mask_graphics_56,x:947.9,y:61.8}).wait(1).to({graphics:mask_graphics_57,x:947.9,y:61.8}).wait(1).to({graphics:mask_graphics_58,x:947.9,y:61.8}).wait(1).to({graphics:mask_graphics_59,x:947.9,y:61.8}).wait(1).to({graphics:mask_graphics_60,x:947.9,y:61.8}).wait(1).to({graphics:mask_graphics_61,x:947.9,y:61.8}).wait(1).to({graphics:mask_graphics_62,x:947.9,y:61.8}).wait(1).to({graphics:mask_graphics_63,x:947.9,y:61.8}).wait(1).to({graphics:mask_graphics_64,x:947.9,y:61.8}).wait(1).to({graphics:mask_graphics_65,x:947.9,y:61.8}).wait(1).to({graphics:mask_graphics_66,x:947.9,y:61.8}).wait(1).to({graphics:mask_graphics_67,x:947.9,y:61.8}).wait(1).to({graphics:mask_graphics_68,x:947.9,y:61.8}).wait(1).to({graphics:mask_graphics_69,x:947.9,y:61.8}).wait(1).to({graphics:mask_graphics_70,x:947.9,y:61.8}).wait(1).to({graphics:mask_graphics_71,x:947.9,y:61.8}).wait(1).to({graphics:mask_graphics_72,x:947.9,y:61.8}).wait(1).to({graphics:mask_graphics_73,x:947.9,y:61.8}).wait(1).to({graphics:mask_graphics_74,x:947.9,y:61.8}).wait(1).to({graphics:mask_graphics_75,x:947.9,y:61.8}).wait(1).to({graphics:mask_graphics_76,x:947.9,y:61.8}).wait(1).to({graphics:mask_graphics_77,x:947.9,y:61.8}).wait(1).to({graphics:mask_graphics_78,x:947.9,y:61.8}).wait(1).to({graphics:mask_graphics_79,x:947.9,y:61.8}).wait(1).to({graphics:mask_graphics_80,x:947.9,y:61.8}).wait(1).to({graphics:mask_graphics_81,x:947.9,y:61.8}).wait(1).to({graphics:mask_graphics_82,x:947.9,y:61.8}).wait(1).to({graphics:mask_graphics_83,x:947.9,y:61.8}).wait(36).to({graphics:mask_graphics_119,x:947.9,y:61.8}).wait(1).to({graphics:mask_graphics_120,x:947.9,y:61.8}).wait(1).to({graphics:mask_graphics_121,x:947.9,y:61.8}).wait(1).to({graphics:mask_graphics_122,x:947.9,y:61.8}).wait(1).to({graphics:mask_graphics_123,x:947.9,y:61.8}).wait(1).to({graphics:mask_graphics_124,x:947.9,y:61.8}).wait(1).to({graphics:mask_graphics_125,x:947.9,y:61.8}).wait(1).to({graphics:mask_graphics_126,x:947.9,y:61.8}).wait(1).to({graphics:mask_graphics_127,x:947.9,y:61.8}).wait(1).to({graphics:mask_graphics_128,x:947.9,y:61.8}).wait(1).to({graphics:mask_graphics_129,x:947.9,y:61.8}).wait(1).to({graphics:mask_graphics_130,x:947.9,y:61.8}).wait(1).to({graphics:mask_graphics_131,x:947.9,y:61.8}).wait(1).to({graphics:mask_graphics_132,x:947.9,y:61.8}).wait(1).to({graphics:mask_graphics_133,x:947.9,y:61.8}).wait(1).to({graphics:mask_graphics_134,x:947.9,y:61.8}).wait(1));
  
    // N
    this.shape = new cjs.Shape();
    this.shape.graphics.f("#fff").s().p("AEgJaIoKqjIAAKjIhMAAIAAuDIJ7M3IB4AAIAAg5IsAviIh5AAIAARnIhMAAIAAyzIDqAAILaOzIAAtnIiFAAIAAIYIhMhiIAAoCIEeAAIAASzg");
    this.shape.setTransform(947.9,61.8);
  
    this.instance = new lib.Tween7("synched",0);
    this.instance.parent = this;
    this.instance.setTransform(947.9,61.8);
    this.instance._off = true;
  
    this.instance_1 = new lib.Tween8("synched",0);
    this.instance_1.parent = this;
    this.instance_1.setTransform(947.9,61.8);
    this.instance_1.alpha = 0;
  
    var maskedShapeInstanceList = [this.shape,this.instance,this.instance_1];
  
    for(var shapedInstanceItr = 0; shapedInstanceItr < maskedShapeInstanceList.length; shapedInstanceItr++) {
      maskedShapeInstanceList[shapedInstanceItr].mask = mask;
    }
  
    this.timeline.addTween(cjs.Tween.get({}).to({state:[]}).to({state:[{t:this.shape}]},1).to({state:[{t:this.instance}]},118).to({state:[{t:this.instance_1}]},15).wait(1));
    this.timeline.addTween(cjs.Tween.get(this.instance).wait(119).to({_off:false},0).to({_off:true,alpha:0},15).wait(1));
  
    // O mask (mask)
    var mask_1 = new cjs.Shape();
    mask_1._off = true;
    var mask_1_graphics_1 = new cjs.Graphics().p("AgkAFIgEgGIA6grIABABQAKAPAMAOIgrA7QgSgTgQgVg");
    var mask_1_graphics_2 = new cjs.Graphics().p("AgXAOQgRgWgNgZIBIgVIANAUQAKAPAMAOIgtA8QgRgUgPgVg");
    var mask_1_graphics_3 = new cjs.Graphics().p("AgKApQglgygTg7IBJgMQAOAmAYAjQAKAOAMAOIgsA9QgSgUgPgVg");
    var mask_1_graphics_4 = new cjs.Graphics().p("AgDBIQg2hJgQhaIBJgVQAKBLAqA8QAKAPAMAOIgtA9QgSgUgOgVg");
    var mask_1_graphics_5 = new cjs.Graphics().p("AAABuQhOhrACiLIAAgOIBLABIAAANIAAAMQABBpA3BQQAKAPAMAPIgsA8QgSgUgPgVg");
    var mask_1_graphics_6 = new cjs.Graphics().p("AAACWQhOhsACiLQAAgsAIgpIBNgHQgLAsABAxIAAAMQABBnA3BSQAKAPAMAOIgsA8QgSgTgPgVg");
    var mask_1_graphics_7 = new cjs.Graphics().p("AAAC3QhOhsACiLQAAhVAdhJIBHAVQgaA/ABBLIAAAMQABBnA3BSQAKAPAMAOIgsA8QgSgTgPgVg");
    var mask_1_graphics_8 = new cjs.Graphics().p("AAADeQhOhsACiLQAAiFBHhnIBAAkQg9BYABBxIAAAMQABBnA3BSQAKAPAMAOIgsA8QgSgTgPgVg");
    var mask_1_graphics_9 = new cjs.Graphics().p("AgID/QhOhsABiMQAAijBsh3IAPgQIAEgEIAsA9IgMANQhVBiABCDIAAAMQACBoA3BSQAKAPAMAOIgsA9QgTgUgOgVg");
    var mask_1_graphics_10 = new cjs.Graphics().p("AgiEWQhOhrABiMQAAivB7h8QAcgcAfgWIApA/QgaASgYAZQhjBmABCNIAAAMQACBpA3BRQAKAPAMAPIgrA8QgTgUgPgVg");
    var mask_1_graphics_11 = new cjs.Graphics().p("AhFEvQhPhsACiMQAAiuB8h9QBChDBTgfIAUBJQhCAag2A3QhjBmABCOIAAAMQACBoA3BSQALAPALAOIgsA8QgSgTgPgVg");
    var mask_1_graphics_12 = new cjs.Graphics().p("AhqE5QhPhsACiMQAAiuB8h8QBhhjCDgVIAQBLQhxAQhSBUQhjBmABCOIAAAMQABBoA4BSQAKAPAMAOIgsA9QgSgUgPgVg");
    var mask_1_graphics_13 = new cjs.Graphics().p("AiJE8QhOhsABiMQAAivB8h8QB7h8CwAAIAGAAIgPBMQiOAChiBmQhkBmABCNIAAAMQACBpA4BRQAKAQAMAOIgsA8QgTgUgPgUg");
    var mask_1_graphics_14 = new cjs.Graphics().p("AiwE8QhPhsACiMQAAivB8h8QB8h8CvAAQAlAAAjAGIAMACIgVBJQgfgFghAAIgHAAQiPAChjBmQhjBmABCNIAAAMQABBpA4BRQAKAQAMAOIgsA8QgSgUgPgUg");
    var mask_1_graphics_15 = new cjs.Graphics().p("AjaE8QhPhsACiMQAAivB8h8QB8h8CvAAQBbAABNAhIgwA/Qg4gUhBAAIgHAAQiOAChkBmQhjBmABCNIAAAMQACBpA3BRQAKAQANAOIgtA8QgSgUgPgUg");
    var mask_1_graphics_16 = new cjs.Graphics().p("Aj5E8QhPhsACiMQAAivB8h8QB8h8CvAAQCBAABlBDIgnBAIgVgMQhMgshfABIgHAAQiOAChkBmQhjBmABCNIAAAMQACBpA3BRQAKAQANAOIgtA8QgSgUgPgUg");
    var mask_1_graphics_17 = new cjs.Graphics().p("AkfE8QhOhsABiMQAAivB8h8QB8h8CvAAQCwAAB8B8IAGAGIhHAkQhjhbiIABIgIAAQiNAChkBmQhkBmABCNIAAAMQACBpA4BRQAKAQAMAOIgsA8QgTgUgPgUg");
    var mask_1_graphics_18 = new cjs.Graphics().p("Ak6E8QhPhsACiMQAAivB8h8QB8h8CvAAQCvAAB9B8QAjAkAZAnIhOATQgRgXgVgUQhmhkiPABIgHAAQiOAChkBmQhjBmABCNIAAAMQACBpA3BRQAKAQANAOIgtA8QgSgUgPgUg");
    var mask_1_graphics_19 = new cjs.Graphics().p("AlNE8QhPhsACiMQAAivB8h8QB8h8CvAAQCvAAB9B8QBEBFAfBUIhSABQgag4gvgvQhmhkiPABIgHAAQiOAChkBmQhjBmABCNIAAAMQABBpA4BRQAKAQAMAOIgsA8QgSgUgPgUg");
    var mask_1_graphics_20 = new cjs.Graphics().p("AlXE8QhPhsACiMQAAivB8h8QB8h8CvAAQCvAAB9B8QBiBjAUCDIhNABQgShnhPhNQhmhkiPABIgGAAQiPAChkBmQhjBmABCNIAAAMQABBpA4BRQAKAQAMAOIgsA8QgSgUgPgUg");
    var mask_1_graphics_21 = new cjs.Graphics().p("AlaE8QhPhsACiMQAAivB8h8QB8h8CvAAQCwAAB8B8QB8B8AACvIAAADIhNgGIAAgEQgBiOhmhjQhmhkiOABIgHAAQiOAChkBmQhkBmABCNIAAAMQACBpA3BRQALAQAMAOIgtA8QgSgUgPgUg");
    var mask_1_graphics_22 = new cjs.Graphics().p("AlaE8QhPhsACiMQAAivB8h8QB8h8CvAAQCwAAB8B8QB8B8AACvIAAAIQAAAvgKArIhGgrQAEgbgBgcIAAgHQgBiOhmhjQhmhkiOABIgHAAQiOAChkBmQhkBmABCNIAAAMQACBpA3BRQALAQAMAOIgtA8QgSgUgPgUg");
    var mask_1_graphics_23 = new cjs.Graphics().p("AlaE8QhPhsACiMQAAivB8h8QB8h8CvAAQCwAAB8B8QB8B8AACvIAAAIQgBBjgoBSIg8g0QAZg/gBhKIAAgHQgBiOhmhjQhmhkiOABIgHAAQiOAChkBmQhkBmABCNIAAAMQACBpA3BRQALAQAMAOIgtA8QgSgUgPgUg");
    var mask_1_graphics_24 = new cjs.Graphics().p("AlaE8QhPhsACiMQAAivB8h8QB8h8CvAAQCwAAB8B8QB8B8AACvIAAAIQgBCXheBwIgwg8QBEhbgCh4IAAgHQgBiOhmhjQhmhkiOABIgHAAQiOAChkBmQhkBmABCNIAAAMQACBpA3BRQALAQAMAOIgtA8QgSgUgPgUg");
    var mask_1_graphics_25 = new cjs.Graphics().p("ADhE6QANgLAMgMQBkhogDiPIAAgIQgBiNhmhkQhmhkiOABIgHAAQiOAChkBmQhkBmABCOIAAALQACBpA3BSQALAPAMAOIgtA8QgSgTgPgVQhPhsACiMQAAiuB8h9QB8h8CvAAQCwAAB8B8QB8B9AACuIAAAJQgBCth8B6QgTASgUAQg");
    var mask_1_graphics_26 = new cjs.Graphics().p("ACpFMQArgYAmgnQBkhngDiQIAAgIQgBiNhmhkQhmhkiOACIgHAAQiOABhkBmQhkBmABCOIAAAMQACBoA3BSQALAPAMAOIgtA8QgSgTgPgVQhPhsACiMQAAiuB8h8QB8h9CvAAQCwAAB8B9QB8B8AACuIAAAJQgBCuh8B5QgwAug2AdIgBgCIgCADIgDACg");
    var mask_1_graphics_27 = new cjs.Graphics().p("ABeFbQBYgXBEhHQBkhngDiQIAAgHQgBiOhmhkQhmhkiOACIgHAAQiOABhkBmQhkBmABCOIAAAMQACBoA3BSQALAPAMAOIgtA8QgSgTgPgVQhPhsACiMQAAiuB8h8QB8h9CvAAQCwAAB8B9QB8B8AACuIAAAJQgBCuh8B5QgwAug2AdIgBgCIgCADQgsAXgxALg");
    var mask_1_graphics_28 = new cjs.Graphics().p("AAZFiQCDgJBehhQBkhngDiQIAAgHQgBiOhmhkQhmhkiOACIgHAAQiOABhkBmQhkBmABCOIAAAMQACBoA3BSQALAPAMAOIgtA9QgSgUgPgVQhPhsACiMQAAiuB8h8QB8h9CvAAQCwAAB8B9QB8B8AACuIAAAJQgBCuh8B5QgwAvg2AcIgBgCIgCADQhLAnhaAFg");
    var mask_1_graphics_29 = new cjs.Graphics().p("AgHGoQglAAgigGIAThEQAfAFAhgBQCRgCBkhoQBkhogDiQIAAgGQgBiPhmhjQhmhkiOABIgHAAQiOAChkBmQhkBmABCNIAAAMQACBpA3BRQALAPAMAPIgtA8QgSgUgPgUQhPhsACiMQAAivB8h8QB8h8CvAAQCwAAB8B8QB8B8AACvIAAAIQgBCuh8B6QgwAug2AcIgBgCIgCAEQhYAthqgBIAAAAg");
    var mask_1_graphics_30 = new cjs.Graphics().p("AgHGoQhVgBhIgeIAig/QA/AaBIgCQCRgCBkhoQBkhogDiQIAAgGQgBiPhmhjQhmhkiOABIgHAAQiOAChkBmQhkBmABCNIAAAMQACBpA3BRQALAPAMAPIgtA8QgSgUgPgUQhPhsACiMQAAivB8h8QB8h8CvAAQCwAAB8B8QB8B8AACvIAAAIQgBCuh8B6QgwAug2AcIgBgCIgCAEQhYAthqgBIAAAAg");
    var mask_1_graphics_31 = new cjs.Graphics().p("AgHGoQiGgBhmhKIAmg+QBbBFB3gCQCRgCBkhoQBkhogDiQIAAgGQgBiPhmhjQhmhkiOABIgHAAQiOAChkBmQhkBmABCNIAAAMQACBpA3BRQALAPAMAPIgtA8QgSgUgPgUQhPhsACiMQAAivB8h8QB8h8CvAAQCwAAB8B8QB8B8AACvIAAAIQgBCuh8B6QgwAug2AcIgBgCIgCAEQhYAthqgBIAAAAg");
    var mask_1_graphics_32 = new cjs.Graphics().p("AgHGoQiKgBhphOQgcgVgZgaQgYgYgTgaQhPhsACiMQAAivB8h8QB8h8CvAAQCwAAB8B8QB8B8AACvIAAAIQgBCuh8B6QgwAug2AcIgBgCIgCAEQhYAthqgBIAAAAgAj5jzQhkBmABCNIAAAMQACBpA3BRQAVAfAdAbQAQAQARANQBdBHB4gCQCRgCBkhoQBkhogDiQIAAgGQgBiPhmhjQhmhkiOABIgHAAQiOAChkBmg");
    var mask_1_graphics_33 = new cjs.Graphics().p("AmZGsIAwg7QAYATAaARIgoBCQgegUgcgXgAgHF6QiugCh6h8Qh6h9ACitQAAivB8h8QB8h9CvAAQCwAAB8B9QB8B8AACvIAAAJQgBCth8B5QgwAug2AdIgBgCIgCADQhYAthqgBIAAABgAj5kiQhkBmABCPIAAAMQADCPBoBkQBnBkCPgCQCRgDBkhoQBkhngDiPIAAgIQgBiOhmhkQhmhkiOACIgHAAQiOABhkBmg");
    var mask_1_graphics_34 = new cjs.Graphics().p("AmZGaIAwg7QAvAnA2AbIgbBIQhBggg5gvgAgHFoQiugCh6h8Qh6h9ACitQAAivB8h8QB8h9CvAAQCwAAB8B9QB8B8AACvIAAAJQgBCth8B5QgwAvg2AcIgBgCIgCADQhYAthqgBIAAABgAj5k0QhkBmABCPIAAAMQADCPBoBkQBnBkCPgCQCRgDBkhoQBkhngDiPIAAgIQgBiOhmhkQhmhkiOACIgHAAQiOABhkBmg");
    var mask_1_graphics_35 = new cjs.Graphics().p("AjZH3QhqglhWhHIAwg7QBNA+BdAhIAMAEIgiBFIgEgBgAgHFZQiugCh6h8Qh6h9ACitQAAivB8h8QB8h9CvAAQCwAAB8B9QB8B8AACvIAAAJQgBCth8B5QgwAug2AdIgBgCIgCADQhYAthqgBIAAABgAj5lDQhkBmABCPIAAAMQADCPBoBkQBnBkCPgCQCRgDBkhoQBkhngDiPIAAgIQgBiOhmhkQhmhkiOACIgHAAQiOABhkBmg");
    var mask_1_graphics_36 = new cjs.Graphics().p("AjZHtQhqglhWhGIAwg8QBNA/BdAgQAnAOApAIIgkBGQgjgIgjgMgAgHFPQiugCh6h8Qh6h8ACitQAAiwB8h8QB8h8CvAAQCwAAB8B8QB8B8AACwIAAAIQgBCth8B6QgwAug2AcIgBgCIgCAEQhYAthqgBIAAAAgAj5lMQhkBmABCOIAAAMQADCPBoBkQBnBlCPgDQCRgCBkhoQBkhogDiPIAAgHQgBiPhmhjQhmhkiOABIgHAAQiOAChkBmg");
    var mask_1_graphics_37 = new cjs.Graphics().p("AjZHnQhqglhWhGIAwg8QBNA/BdAgQA0ATA3AIIAGBNQhHgIhEgYgAgHFJQiugCh6h8Qh6h8ACitQAAiwB8h8QB8h8CvAAQCwAAB8B8QB8B8AACwIAAAIQgBCth8B6QgwAug2AdIgBgDIgCAEQhYAthqgBIAAAAgAj5lSQhkBmABCOIAAAMQADCPBoBkQBnBlCPgDQCRgCBkhoQBkhogDiPIAAgHQgBiPhmhjQhmhkiOABIgHAAQiOAChkBmg");
    var mask_1_graphics_38 = new cjs.Graphics().p("AjZHlQhqglhWhGIAwg8QBNA/BdAgQBPAcBUAEIAVBMQhsAAhmgkgAgHFHQiugCh6h8Qh6h8ACitQAAiwB8h8QB8h8CvAAQCwAAB8B8QB8B8AACwIAAAIQgBCth8B6QgwAug2AcIgBgCIgCAEQhYAthqgBIAAAAgAj5lUQhkBmABCOIAAAMQADCPBoBkQBnBlCPgDQCRgCBkhoQBkhogDiPIAAgHQgBiPhmhjQhmhkiOABIgHAAQiOAChkBmg");
    var mask_1_graphics_39 = new cjs.Graphics().p("AjZHlQhqglhWhGIAwg8QBNA/BdAgQBdAhBigBQAYAAAXgCIAXBLQgiADgkAAIgFAAQhtAAhngkgAgHFHQiugCh6h8Qh6h8ACitQAAiwB8h8QB8h8CvAAQCwAAB8B8QB8B8AACwIAAAIQgBCth8B6QgwAug2AcIgBgCIgCAEQhYAthqgBIAAAAgAj5lUQhkBmABCOIAAAMQADCPBoBkQBnBlCPgDQCRgCBkhoQBkhogDiPIAAgHQgBiPhmhjQhmhkiOABIgHAAQiOAChkBmg");
    var mask_1_graphics_40 = new cjs.Graphics().p("AjZHlQhqglhWhGIAwg8QBNA/BdAgQBdAhBigBQA9AAA5gLIAYBJQhEAOhKAAIgFAAQhtAAhngkgAgHFHQiugCh6h8Qh6h8ACitQAAiwB8h8QB8h8CvAAQCwAAB8B8QB8B8AACwIAAAIQgBCth8B6QgwAug2AcIgBgCIgCAEQhYAthqgBIAAAAgAj5lUQhkBmABCOIAAAMQADCPBoBkQBnBlCPgDQCRgCBkhoQBkhogDiPIAAgHQgBiPhmhjQhmhkiOABIgHAAQiOAChkBmg");
    var mask_1_graphics_41 = new cjs.Graphics().p("AjZHlQhqglhWhGIAwg8QBNA/BdAgQBdAhBigBQBZAABPgXIAyBAQhlAjh1AAIgFAAQhtAAhngkgAgHFHQiugCh6h8Qh6h8ACitQAAiwB8h8QB8h8CvAAQCwAAB8B8QB8B8AACwIAAAIQgBCth8B6Qh9B6iugCIAAAAgAj5lUQhkBmABCOIAAAMQADCPBoBkQBnBlCPgDQCRgCBkhoQBkhogDiPIAAgHQgBiPhmhjQhmhkiOABIgHAAQiOAChkBmg");
    var mask_1_graphics_42 = new cjs.Graphics().p("AjZHlQhqglhWhGIAwg8QBNA/BdAgQBdAhBigBQCGAABug0IArBBQiBA/ieAAIgFAAQhtAAhngkgAgHFHQiugCh6h8Qh6h8ACitQAAiwB8h8QB8h8CvAAQCwAAB8B8QB8B8AACwIAAAIQgBCth8B6Qh9B6iugCIAAAAgAj5lUQhkBmABCOIAAAMQADCPBoBkQBnBlCPgDQCRgCBkhoQBkhogDiPIAAgHQgBiPhmhjQhmhkiOABIgHAAQiOAChkBmg");
    var mask_1_graphics_43 = new cjs.Graphics().p("AjZHlQhqglhWhGIAwg8QBNA/BdAgQBdAhBigBQC0AACJhdIAuA9QidBsjOAAIgFAAQhtAAhngkgAgHFHQiugCh6h8Qh6h8ACitQAAiwB8h8QB8h8CvAAQCwAAB8B8QB8B8AACwIAAAIQgBCth8B6Qh9B6iugCIAAAAgAj5lUQhkBmABCOIAAAMQADCPBoBkQBnBlCPgDQCRgCBkhoQBkhogDiPIAAgHQgBiPhmhjQhmhkiOABIgHAAQiOAChkBmg");
    var mask_1_graphics_44 = new cjs.Graphics().p("AjeHlQhqglhWhGIAwg8QBNA/BdAgQBdAhBjgBQDeAACdiPIA2A2QizClj+AAIgGAAQhtAAhngkgAgMFHQiugCh6h8Qh6h8ACitQAAiwB8h8QB8h8CwAAQCvAAB8B8QB8B8AACwIAAAIQgBCth8B6Qh9B6itgCIAAAAgAj+lUQhkBmABCOIAAAMQADCPBoBkQBnBlCPgDQCRgCBkhoQBkhogDiPIAAgHQgBiPhmhjQhmhkiNABIgIAAQiOAChkBmg");
    var mask_1_graphics_45 = new cjs.Graphics().p("Aj2HlQhqglhXhGIAwg8QBNA/BdAgQBdAhBjgBQDqAAChidIAZgZIA+AsQgQASgRARQi4CzkJAAIgGAAQhtAAhmgkgAglFHQiugCh5h8Qh6h8ACitQAAiwB8h8QB8h8CwAAQCuAAB9B8QB8B8AACwIAAAIQgCCth8B6Qh8B6itgCIgBAAgAkXlUQhjBmABCOIAAAMQACCPBoBkQBoBlCQgDQCPgCBkhoQBkhogCiPIAAgHQgCiPhlhjQhmhkiOABIgHAAQiPAChkBmg");
    var mask_1_graphics_46 = new cjs.Graphics().p("AkMHlQhqglhXhGIAwg8QBNA/BdAgQBdAhBjgBQDqAAChidQAmgkAcgpIBBApQghAvgsArQi4CzkJAAIgFAAQhtAAhngkgAg7FHQiugCh5h8Qh6h8ACitQAAiwB8h8QB8h8CwAAQCuAAB9B8QB8B8AACwIAAAIQgCCth8B6Qh8B6itgCIgBAAgAktlUQhjBmABCOIAAAMQACCPBoBkQBoBlCQgDQCPgCBkhoQBkhogCiPIAAgHQgCiPhlhjQhmhkiOABIgHAAQiPAChkBmg");
    var mask_1_graphics_47 = new cjs.Graphics().p("AkeHlQhqglhXhGIAwg8QBNA/BdAgQBdAhBjgBQDqAAChidQA7g5AlhDIBHAeQgrBQhGBEQi4CzkJAAIgFAAQhtAAhngkgAhNFHQiugCh5h8Qh6h8ACitQAAiwB8h8QB8h8CwAAQCuAAB9B8QB8B8AACwIAAAIQgCCth8B6Qh8B6itgCIgBAAgAk/lUQhjBmABCOIAAAMQACCPBoBkQBoBlCQgDQCPgCBkhoQBkhogCiPIAAgHQgCiPhlhjQhmhkiOABIgHAAQiPAChkBmg");
    var mask_1_graphics_48 = new cjs.Graphics().p("AkxHlQhqglhWhGIAwg8QBNA/BdAgQBdAhBjgBQDpAAChidQBZhWAohsIBMAPQgtCChqBnQi3CzkJAAIgGAAQhtAAhngkgAhfFHQiugCh6h8Qh6h8ACitQAAiwB8h8QB8h8CwAAQCvAAB8B8QB8B8AACwIAAAIQgBCth8B6Qh9B6itgCIAAAAgAlRlUQhkBmABCOIAAAMQADCPBoBkQBnBlCQgDQCQgCBkhoQBkhogDiPIAAgHQgBiPhmhjQhmhkiNABIgIAAQiOAChkBmg");
    var mask_1_graphics_49 = new cjs.Graphics().p("Ak7HlQhqglhWhGIAwg8QBNA/BdAgQBdAhBjgBQDpAAChidQB3hzAfiaIBLAQQgkCviHCEQi3CzkJAAIgGAAQhtAAhngkgAhpFHQiugCh6h8Qh6h8ACitQAAiwB8h8QB8h8CwAAQCvAAB8B8QB8B8AACwIAAAIQgBCth8B6Qh9B6itgCIAAAAgAlblUQhkBmABCOIAAAMQADCPBoBkQBnBlCQgDQCQgCBkhoQBkhogDiPIAAgHQgBiPhmhjQhmhkiNABIgIAAQiOAChkBmg");
    var mask_1_graphics_50 = new cjs.Graphics().p("AlAHlQhqglhXhGIAwg8QBNA/BdAgQBdAhBjgBQDqAAChidQCSiOAOjHIBLAHQgQDjilChQi4CzkJAAIgGAAQhtAAhmgkgAhvFHQiugCh5h8Qh6h8ACitQAAiwB8h8QB8h8CwAAQCuAAB9B8QB8B8AACwIAAAIQgCCth8B6Qh8B6itgCIgBAAgAlhlUQhjBmABCOIAAAMQACCPBoBkQBoBlCQgDQCPgCBkhoQBkhogCiPIAAgHQgCiPhlhjQhmhkiOABIgHAAQiPAChkBmg");
    var mask_1_graphics_51 = new cjs.Graphics().p("AlBHlQhqglhXhGIAwg8QBNA/BdAgQBdAhBjgBQDqAAChidQChidAAjiQAAgWgBgWIBMgIQACAZAAAaIgBAAQAAEDi3CzQi4CzkJAAIgGAAQhtAAhmgkgAhwFHQiugCh5h8Qh6h8ACitQAAiwB8h8QB8h8CwAAQCuAAB9B8QB8B8AACwIAAAIQgCCth8B6Qh8B6itgCIgBAAgAlilUQhjBmABCOIAAAMQACCPBoBkQBoBlCQgDQCPgCBkhoQBkhogCiPIAAgHQgCiPhlhjQhmhkiOABIgHAAQiPAChkBmg");
    var mask_1_graphics_52 = new cjs.Graphics().p("AlBHlQhqglhXhGIAwg8QBNA/BdAgQBdAhBjgBQDqAAChidQChidAAjiQAAg9gLg4IBKgUQAOBBAABHIgBAAQAAEDi3CzQi4CzkJAAIgGAAQhtAAhmgkgAhwFHQiugCh5h8Qh6h8ACitQAAiwB8h8QB8h8CwAAQCuAAB9B8QB8B8AACwIAAAIQgCCth8B6Qh8B6itgCIgBAAgAlilUQhjBmABCOIAAAMQACCPBoBkQBoBlCQgDQCPgCBkhoQBkhogCiPIAAgHQgCiPhlhjQhmhkiOABIgHAAQiPAChkBmg");
    var mask_1_graphics_53 = new cjs.Graphics().p("AlBHlQhqglhXhGIAwg8QBNA/BdAgQBdAhBjgBQDqAAChidQChidAAjiQAAhpgihaIAAAAIBIgcQAnBnAAB3IgBAAQAAEDi3CzQi4CzkJAAIgGAAQhtAAhmgkgAhwFHQiugCh5h8Qh6h8ACitQAAiwB8h8QB8h8CwAAQCuAAB9B8QB8B8AACwIAAAIQgCCth8B6Qh8B6itgCIgBAAgAlilUQhjBmABCOIAAAMQACCPBoBkQBoBlCQgDQCPgCBkhoQBkhogCiPIAAgHQgCiPhlhjQhmhkiOABIgHAAQiPAChkBmg");
    var mask_1_graphics_54 = new cjs.Graphics().p("AlBHlQhqglhXhGIAwg8QBNA/BdAgQBdAhBjgBQDqAAChidQChidAAjiQAAiRhBh1IBCgoQBMCGAACnIgBAAQAAEDi3CzQi4CzkJAAIgGAAQhtAAhmgkgAhwFHQiugCh5h8Qh6h8ACitQAAiwB8h8QB8h8CwAAQCuAAB9B8QB8B8AACwIAAAIQgCCth8B6Qh8B6itgCIgBAAgAlilUQhjBmABCOIAAAMQACCPBoBkQBoBlCQgDQCPgCBkhoQBkhogCiPIAAgHQgCiPhlhjQhmhkiOABIgHAAQiPAChkBmg");
    var mask_1_graphics_55 = new cjs.Graphics().p("AlBHlQhqglhXhGIAwg8QBNA/BdAgQBdAhBjgBQDqAAChidQChidAAjiQAAi5hpiKIA3g2QB/CgAADYIgBAAQAAEDi3CzQi4CzkJAAIgGAAQhtAAhmgkgAhwFHQiugCh5h8Qh6h8ACitQAAiwB8h8QB8h8CwAAQCuAAB9B8QB8B8AACwIAAAIQgCCth8B6Qh8B6itgCIgBAAgAlilUQhjBmABCOIAAAMQACCPBoBkQBoBlCQgDQCPgCBkhoQBkhogCiPIAAgHQgCiPhlhjQhmhkiOABIgHAAQiPAChkBmg");
    var mask_1_graphics_56 = new cjs.Graphics().p("AlBHuQhqglhXhHIAwg7QBNA+BdAhQBdAhBjgBQDqAAChidQChidAAjjQAAjjigicIAyg7IADADQC4CzAAEEIgBAAQAAEDi3CzQi4CzkJAAIgGAAQhtAAhmgkgAhwFQQiugCh5h8Qh6h9ACitQAAivB8h8QB8h9CwAAQCuAAB9B9QB8B8AACvIAAAJQgCCth8B5Qh8B6itgCIgBABgAlilMQhjBmABCPIAAAMQACCPBoBkQBoBkCQgCQCPgDBkhoQBkhngCiPIAAgIQgCiOhlhkQhmhkiOACIgHAAQiPABhkBmg");
    var mask_1_graphics_57 = new cjs.Graphics().p("AlBIGQhqglhXhHIAwg7QBNA+BdAhQBdAhBjgBQDqAAChidQChidAAjjQAAjkihidQgWgVgXgSIAqhCQAdAXAcAcQC4CzAAEEIgBAAQAAEDi3CzQi4CzkJAAIgGAAQhtAAhmgkgAhwFoQiugCh5h8Qh6h9ACitQAAivB8h8QB8h9CwAAQCuAAB9B9QB8B8AACvIAAAJQgCCth8B5Qh8B6itgCIgBABgAlik0QhjBmABCPIAAAMQACCPBoBkQBoBkCQgCQCPgDBkhoQBkhngCiPIAAgIQgCiOhlhkQhmhkiOACIgHAAQiPABhkBmg");
    var mask_1_graphics_58 = new cjs.Graphics().p("AlBIcQhqglhXhGIAwg8QBNA/BdAgQBdAhBjgBQDqAAChidQChidAAjiQAAjkihidQgxgvg3ghIAhhGQBDAmA6A5QC4CzAAEEIgBAAQAAEDi3CzQi4CzkJAAIgGAAQhtAAhmgkgAhwF+QiugCh5h8Qh6h8ACitQAAiwB8h8QB8h8CwAAQCuAAB9B8QB8B8AACwIAAAIQgCCth8B6Qh8B6itgCIgBAAgAlikdQhjBmABCOIAAAMQACCPBoBkQBoBlCQgDQCPgCBkhoQBkhogCiPIAAgHQgCiPhlhjQhmhkiOABIgHAAQiPAChkBmg");
    var mask_1_graphics_59 = new cjs.Graphics().p("AlBItQhqglhXhHIAwg7QBNA+BdAhQBdAhBjgBQDqAAChidQChidAAjjQAAjkihidQhMhJhbgnIgDgBIAihGQBoAtBWBUQC4CzAAEEIgBAAQAAEDi3CzQi4CzkJAAIgGAAQhtAAhmgkgAhwGPQiugCh5h8Qh6h9ACitQAAivB8h8QB8h9CwAAQCuAAB9B9QB8B8AACvIAAAJQgCCth8B5Qh8B6itgCIgBABgAlikNQhjBmABCPIAAAMQACCPBoBkQBoBkCQgCQCPgDBkhoQBkhngCiPIAAgIQgCiOhlhkQhmhkiOACIgHAAQiPABhkBmg");
    var mask_1_graphics_60 = new cjs.Graphics().p("AlBI5QhqglhXhGIAwg8QBNA/BdAgQBdAhBjgBQDqAAChidQChidAAjiQAAjkihidQhohliFgkIAehHQCSAqBzBvQC4CzAAEEIgBAAQAAEDi3CzQi4CzkJAAIgGAAQhtAAhmgkgAhwGbQiugCh5h8Qh6h8ACitQAAiwB8h8QB8h8CwAAQCuAAB9B8QB8B8AACwIAAAIQgCCth8B6Qh8B6itgCIgBAAgAlikAQhjBmABCOIAAAMQACCPBoBkQBoBlCQgDQCPgCBkhoQBkhogCiPIAAgHQgCiPhlhjQhmhkiOABIgHAAQiPAChkBmg");
    var mask_1_graphics_61 = new cjs.Graphics().p("AlBJDQhqglhXhGIAwg8QBNA/BdAgQBdAhBjgBQDqAAChidQChidAAjiQAAjkihidQiIiFi7gUIAVhLQDNAaCXCTQC4CzAAEEIgBAAQAAEDi3CzQi4CzkJAAIgGAAQhtAAhmgkgAhwGlQiugCh5h8Qh6h8ACitQAAiwB8h8QB8h8CwAAQCuAAB9B8QB8B8AACwIAAAHQgCCuh8B6Qh8B6itgCIgBAAgAlij2QhjBmABCOIAAALQACCQBoBkQBoBlCQgDQCPgCBkhoQBkhogCiPIAAgHQgCiPhlhjQhmhkiOABIgHAAQiPAChkBmg");
    var mask_1_graphics_62 = new cjs.Graphics().p("AlBJGQhqglhXhHIAwg7QBNA+BdAhQBdAhBjgBQDqAAChidQChidAAjkQAAjjihidQihicjogBIAJhMQECAEC0CvQC4CzAAEDIgBAAQAAEEi3CzQi4CzkJAAIgGAAQhtAAhmgkgAhwGoQiugCh5h8Qh6h9ACitQAAivB8h8QB8h9CwAAQCuAAB9B9QB8B8AACvIAAAIQgCCuh8B5Qh8B6itgCIgBABgAlij0QhjBmABCOIAAAMQACCQBoBkQBoBkCQgCQCPgDBkhoQBkhngCiQIAAgHQgCiOhlhkQhmhkiOACIgHAAQiPABhkBmg");
    var mask_1_graphics_63 = new cjs.Graphics().p("AlBJGQhqglhXhHIAwg7QBNA+BdAhQBdAhBjgBQDqAAChidQChidAAjkQAAjjihidQihidjqAAQgZAAgYACIgchKQAmgEAnAAQEKAAC3CzQC4CzAAEDIgBAAQAAEEi3CzQi4CzkJAAIgGAAQhtAAhmgkgAhwGoQiugCh5h8Qh6h9ACitQAAivB8h8QB8h9CwAAQCuAAB9B9QB8B8AACvIAAAIQgCCuh8B5Qh8B6itgCIgBABgAlij0QhjBmABCOIAAAMQACCQBoBkQBoBkCQgCQCPgDBkhoQBkhngCiQIAAgHQgCiOhlhkQhmhkiOACIgHAAQiPABhkBmg");
    var mask_1_graphics_64 = new cjs.Graphics().p("AlBJGQhqglhXhHIAwg7QBNA+BdAhQBdAhBjgBQDqAAChidQChidAAjkQAAjjihidQihidjqAAQgtAAgrAGIgIACIgghJIAmgGQAsgFAuAAQEKAAC3CzQC4CzAAEDIgBAAQAAEEi3CzQi4CzkJAAIgGAAQhtAAhmgkgAhwGoQiugCh5h8Qh6h9ACitQAAivB8h8QB8h9CwAAQCuAAB9B9QB8B8AACvIAAAIQgCCuh8B5Qh8B6itgCIgBABgAlij0QhjBmABCOIAAAMQACCQBoBkQBoBkCQgCQCPgDBkhoQBkhngCiQIAAgHQgCiOhlhkQhmhkiOACIgHAAQiPABhkBmg");
    var mask_1_graphics_65 = new cjs.Graphics().p("AlBJGQhqglhXhHIAwg7QBNA+BdAhQBdAhBjgBQDqAAChidQChidAAjkQAAjjihidQihidjqAAQhPAAhHASIg+g9QBjghBxAAQEKAAC3CzQC4CzAAEDIgBAAQAAEEi3CzQi4CzkJAAIgGAAQhtAAhmgkgAhwGoQiugCh5h8Qh6h9ACitQAAivB8h8QB8h9CwAAQCuAAB9B9QB8B8AACvIAAAIQgCCuh8B5Qh8B6itgCIgBABgAlij0QhjBmABCOIAAAMQACCQBoBkQBoBkCQgCQCPgDBkhoQBkhngCiQIAAgHQgCiOhlhkQhmhkiOACIgHAAQiPABhkBmg");
    var mask_1_graphics_66 = new cjs.Graphics().p("AlBJGQhqglhXhHIAwg7QBNA+BdAhQBdAhBjgBQDqAAChidQChidAAjkQAAjjihidQihidjqAAQh5AAhnArIhEg1QCEhCCgAAQEKAAC3CzQC4CzAAEDIgBAAQAAEEi3CzQi4CzkJAAIgGAAQhtAAhmgkgAhwGoQiugCh5h8Qh6h9ACitQAAivB8h8QB8h9CwAAQCuAAB9B9QB8B8AACvIAAAIQgCCuh8B5Qh8B6itgCIgBABgAlij0QhjBmABCOIAAAMQACCQBoBkQBoBkCQgCQCPgDBkhoQBkhngCiQIAAgHQgCiOhlhkQhmhkiOACIgHAAQiPABhkBmg");
    var mask_1_graphics_67 = new cjs.Graphics().p("AlBJGQhqglhXhHIAwg7QBNA+BdAhQBdAhBjgBQDqAAChidQChidAAjkQAAjjihidQihidjqAAQinAAiDBRIhDgwQCfhtDOAAQEKAAC3CzQC4CzAAEDIgBAAQAAEEi3CzQi4CzkJAAIgGAAQhtAAhmgkgAhwGoQiugCh5h8Qh6h9ACitQAAivB8h8QB8h9CwAAQCuAAB9B9QB8B8AACvIAAAIQgCCuh8B5Qh8B6itgCIgBABgAlij0QhjBmABCOIAAAMQACCQBoBkQBoBkCQgCQCPgDBkhoQBkhngCiQIAAgHQgCiOhlhkQhmhkiOACIgHAAQiPABhkBmg");
    var mask_1_graphics_68 = new cjs.Graphics().p("AlAJGQhqglhWhHIAwg7QBNA+BdAhQBdAhBjgBQDpAAChidQCiidAAjkQAAjjiiidQihidjpAAQjQAAiZB+IhCgtQCzidD4AAQEJAAC4CzQC3CzAAEDIAAAAQAAEEi4CzQi3CzkJAAIgGAAQhtAAhngkgAhuGoQiugCh6h8Qh6h9ACitQAAivB8h8QB8h9CwAAQCvAAB8B9QB8B8AACvIAAAIQgBCuh8B5Qh9B6itgCIAAABgAlgj0QhkBmABCOIAAAMQADCQBoBkQBnBkCQgCQCQgDBkhoQBkhngDiQIAAgHQgBiOhmhkQhmhkiNACIgIAAQiOABhkBmg");
    var mask_1_graphics_69 = new cjs.Graphics().p("AkcJGQhqglhXhHIAwg7QBNA+BdAhQBdAhBjgBQDqAAChidQChidAAjkQAAjjihidQihidjqAAQjbAAieCLQgeAbgZAdIhCgoQAggmAmgjQC0ieD4AAQEKAAC3CzQC4CzAAEDIgBAAQAAEEi3CzQi4CzkJAAIgGAAQhsAAhngkgAhLGoQiugCh5h8Qh6h9ACitQAAivB8h8QB8h9CwAAQCuAAB9B9QB8B8AACvIAAAIQgCCuh8B5Qh8B6itgCIgBABgAk9j0QhjBmABCOIAAAMQACCQBoBkQBoBkCQgCQCPgDBkhoQBkhngCiQIAAgHQgCiOhlhkQhmhkiOACIgHAAQiPABhkBmg");
    var mask_1_graphics_70 = new cjs.Graphics().p("AkBJGQhqglhXhHIAwg7QBNA+BdAhQBdAhBjgBQDqAAChidQChidAAjkQAAjjihidQihidjqAAQjbAAieCLQg8A2goA/IhLgZQAwhRBMhEQC0ieD4AAQEKAAC3CzQC4CzAAEDIgBAAQAAEEi3CzQi4CzkJAAIgGAAQhtAAhmgkgAgwGoQiugCh5h8Qh6h9ACitQAAivB8h8QB8h9CwAAQCuAAB9B9QB8B8AACvIAAAIQgCCuh8B5Qh8B6itgCIgBABgAkij0QhjBmABCOIAAAMQACCQBoBkQBoBkCQgCQCPgDBkhoQBkhngCiQIAAgHQgCiOhlhkQhmhkiOACIgHAAQiPABhkBmg");
    var mask_1_graphics_71 = new cjs.Graphics().p("AjuJGQhqglhXhHIAwg7QBNA+BdAhQBdAhBjgBQDqAAChidQChidAAjkQAAjjihidQihidjqAAQjbAAieCLQhcBSgtBqIhMgRQAziBBvhjQC0ieD4AAQEKAAC3CzQC4CzAAEDIgBAAQAAEEi3CzQi4CzkJAAIgGAAQhsAAhngkgAgdGoQiugCh5h8Qh6h9ACitQAAivB8h8QB8h9CwAAQCuAAB9B9QB8B8AACvIAAAIQgCCuh8B5Qh8B6itgCIgBABgAkPj0QhjBmABCOIAAAMQACCQBoBkQBoBkCQgCQCPgDBkhoQBkhngCiQIAAgHQgCiOhlhkQhmhkiOACIgHAAQiPABhkBmg");
    var mask_1_graphics_72 = new cjs.Graphics().p("AjhJGQhqglhXhHIAwg7QBNA+BdAhQBdAhBjgBQDqAAChidQChidAAjkQAAjjihidQihidjqAAQjbAAieCLQh9BvgnCbIhLgNQAFgVAHgUQAwibCAhyQC0ieD4AAQEKAAC3CzQC4CzAAEDIgBAAQAAEEi3CzQi4CzkJAAIgGAAQhtAAhmgkgAgQGoQiugCh5h8Qh6h9ACitQAAivB8h8QB8h9CwAAQCuAAB9B9QB8B8AACvIAAAIQgCCuh8B5Qh8B6itgCIgBABgAkCj0QhjBmABCOIAAAMQACCQBoBkQBoBkCQgCQCPgDBkhoQBkhngCiQIAAgHQgCiOhlhkQhmhkiOACIgHAAQiPABhkBmg");
    var mask_1_graphics_73 = new cjs.Graphics().p("AjaJGQhqglhXhHIAwg7QBNA+BdAhQBdAhBjgBQDqAAChidQChidAAjkQAAjjihidQihidjqAAQjbAAieCLQidCLgVDQIhLgIQAHhDATg8QAwibCAhyQC0ieD4AAQEKAAC3CzQC4CzAAEDIgBAAQAAEEi3CzQi4CzkJAAIgGAAQhtAAhmgkgAgJGoQiugCh5h8Qh6h9ACitQAAivB8h8QB8h9CwAAQCuAAB9B9QB8B8AACvIAAAIQgCCuh8B5Qh8B6itgCIgBABgAj7j0QhjBmABCOIAAAMQACCQBoBkQBoBkCPgCQCQgDBkhoQBkhngCiQIAAgHQgCiOhlhkQhmhkiOACIgHAAQiPABhkBmg");
    var mask_1_graphics_119 = new cjs.Graphics().p("AjaJGQhqglhXhHIAwg7QBNA+BdAhQBdAhBjgBQDqAAChidQChidAAjkQAAjjihidQihidjqAAQjbAAieCLQidCLgVDQIhLgIQAHhDATg8QAwibCAhyQC0ieD4AAQEKAAC3CzQC4CzAAEDIgBAAQAAEEi3CzQi4CzkJAAIgGAAQhtAAhmgkgAgJGoQiugCh5h8Qh6h9ACitQAAivB8h8QB8h9CwAAQCuAAB9B9QB8B8AACvIAAAIQgCCuh8B5Qh8B6itgCIgBABgAj7j0QhjBmABCOIAAAMQACCQBoBkQBoBkCPgCQCQgDBkhoQBkhngCiQIAAgHQgCiOhlhkQhmhkiOACIgHAAQiPABhkBmg");
    var mask_1_graphics_120 = new cjs.Graphics().p("AjaJGQhqglhXhHIAwg7QBNA+BdAhQBdAhBjgBQDqAAChidQChidAAjkQAAjjihidQihidjqAAQjbAAieCLQidCLgVDQIhLgIQAHhDATg8QAwibCAhyQC0ieD4AAQEKAAC3CzQC4CzAAEDIgBAAQAAEEi3CzQi4CzkJAAIgGAAQhtAAhmgkgAgJGoQiugCh5h8Qh6h9ACitQAAivB8h8QB8h9CwAAQCuAAB9B9QB8B8AACvIAAAIQgCCuh8B5Qh8B6itgCIgBABgAj7j0QhjBmABCOIAAAMQACCQBoBkQBoBkCPgCQCQgDBkhoQBkhngCiQIAAgHQgCiOhlhkQhmhkiOACIgHAAQiPABhkBmg");
    var mask_1_graphics_121 = new cjs.Graphics().p("AjaJGQhqglhXhHIAwg7QBNA+BdAhQBdAhBjgBQDqAAChidQChidAAjkQAAjjihidQihidjqAAQjbAAieCLQidCLgVDQIhLgIQAHhDATg8QAwibCAhyQC0ieD4AAQEKAAC3CzQC4CzAAEDIgBAAQAAEEi3CzQi4CzkJAAIgGAAQhtAAhmgkgAgJGoQiugCh5h8Qh6h9ACitQAAivB8h8QB8h9CwAAQCuAAB9B9QB8B8AACvIAAAIQgCCuh8B5Qh8B6itgCIgBABgAj7j0QhjBmABCOIAAAMQACCQBoBkQBoBkCPgCQCQgDBkhoQBkhngCiQIAAgHQgCiOhlhkQhmhkiOACIgHAAQiPABhkBmg");
    var mask_1_graphics_122 = new cjs.Graphics().p("AjaJGQhqglhXhHIAwg7QBNA+BdAhQBdAhBjgBQDqAAChidQChidAAjkQAAjjihidQihidjqAAQjbAAieCLQidCLgVDQIhLgIQAHhDATg8QAwibCAhyQC0ieD4AAQEKAAC3CzQC4CzAAEDIgBAAQAAEEi3CzQi4CzkJAAIgGAAQhtAAhmgkgAgJGoQiugCh5h8Qh6h9ACitQAAivB8h8QB8h9CwAAQCuAAB9B9QB8B8AACvIAAAIQgCCuh8B5Qh8B6itgCIgBABgAj7j0QhjBmABCOIAAAMQACCQBoBkQBoBkCPgCQCQgDBkhoQBkhngCiQIAAgHQgCiOhlhkQhmhkiOACIgHAAQiPABhkBmg");
    var mask_1_graphics_123 = new cjs.Graphics().p("AjaJGQhqglhXhHIAwg7QBNA+BdAhQBdAhBjgBQDqAAChidQChidAAjkQAAjjihidQihidjqAAQjbAAieCLQidCLgVDQIhLgIQAHhDATg8QAwibCAhyQC0ieD4AAQEKAAC3CzQC4CzAAEDIgBAAQAAEEi3CzQi4CzkJAAIgGAAQhtAAhmgkgAgJGoQiugCh5h8Qh6h9ACitQAAivB8h8QB8h9CwAAQCuAAB9B9QB8B8AACvIAAAIQgCCuh8B5Qh8B6itgCIgBABgAj7j0QhjBmABCOIAAAMQACCQBoBkQBoBkCPgCQCQgDBkhoQBkhngCiQIAAgHQgCiOhlhkQhmhkiOACIgHAAQiPABhkBmg");
    var mask_1_graphics_124 = new cjs.Graphics().p("AjaJGQhqglhXhHIAwg7QBNA+BdAhQBdAhBjgBQDqAAChidQChidAAjkQAAjjihidQihidjqAAQjbAAieCLQidCLgVDQIhLgIQAHhDATg8QAwibCAhyQC0ieD4AAQEKAAC3CzQC4CzAAEDIgBAAQAAEEi3CzQi4CzkJAAIgGAAQhtAAhmgkgAgJGoQiugCh5h8Qh6h9ACitQAAivB8h8QB8h9CwAAQCuAAB9B9QB8B8AACvIAAAIQgCCuh8B5Qh8B6itgCIgBABgAj7j0QhjBmABCOIAAAMQACCQBoBkQBoBkCPgCQCQgDBkhoQBkhngCiQIAAgHQgCiOhlhkQhmhkiOACIgHAAQiPABhkBmg");
    var mask_1_graphics_125 = new cjs.Graphics().p("AjaJGQhqglhXhHIAwg7QBNA+BdAhQBdAhBjgBQDqAAChidQChidAAjkQAAjjihidQihidjqAAQjbAAieCLQidCLgVDQIhLgIQAHhDATg8QAwibCAhyQC0ieD4AAQEKAAC3CzQC4CzAAEDIgBAAQAAEEi3CzQi4CzkJAAIgGAAQhtAAhmgkgAgJGoQiugCh5h8Qh6h9ACitQAAivB8h8QB8h9CwAAQCuAAB9B9QB8B8AACvIAAAIQgCCuh8B5Qh8B6itgCIgBABgAj7j0QhjBmABCOIAAAMQACCQBoBkQBoBkCPgCQCQgDBkhoQBkhngCiQIAAgHQgCiOhlhkQhmhkiOACIgHAAQiPABhkBmg");
    var mask_1_graphics_126 = new cjs.Graphics().p("AjaJGQhqglhXhHIAwg7QBNA+BdAhQBdAhBjgBQDqAAChidQChidAAjkQAAjjihidQihidjqAAQjbAAieCLQidCLgVDQIhLgIQAHhDATg8QAwibCAhyQC0ieD4AAQEKAAC3CzQC4CzAAEDIgBAAQAAEEi3CzQi4CzkJAAIgGAAQhtAAhmgkgAgJGoQiugCh5h8Qh6h9ACitQAAivB8h8QB8h9CwAAQCuAAB9B9QB8B8AACvIAAAIQgCCuh8B5Qh8B6itgCIgBABgAj7j0QhjBmABCOIAAAMQACCQBoBkQBoBkCPgCQCQgDBkhoQBkhngCiQIAAgHQgCiOhlhkQhmhkiOACIgHAAQiPABhkBmg");
    var mask_1_graphics_127 = new cjs.Graphics().p("AjaJGQhqglhXhHIAwg7QBNA+BdAhQBdAhBjgBQDqAAChidQChidAAjkQAAjjihidQihidjqAAQjbAAieCLQidCLgVDQIhLgIQAHhDATg8QAwibCAhyQC0ieD4AAQEKAAC3CzQC4CzAAEDIgBAAQAAEEi3CzQi4CzkJAAIgGAAQhtAAhmgkgAgJGoQiugCh5h8Qh6h9ACitQAAivB8h8QB8h9CwAAQCuAAB9B9QB8B8AACvIAAAIQgCCuh8B5Qh8B6itgCIgBABgAj7j0QhjBmABCOIAAAMQACCQBoBkQBoBkCPgCQCQgDBkhoQBkhngCiQIAAgHQgCiOhlhkQhmhkiOACIgHAAQiPABhkBmg");
    var mask_1_graphics_128 = new cjs.Graphics().p("AjaJGQhqglhXhHIAwg7QBNA+BdAhQBdAhBjgBQDqAAChidQChidAAjkQAAjjihidQihidjqAAQjbAAieCLQidCLgVDQIhLgIQAHhDATg8QAwibCAhyQC0ieD4AAQEKAAC3CzQC4CzAAEDIgBAAQAAEEi3CzQi4CzkJAAIgGAAQhtAAhmgkgAgJGoQiugCh5h8Qh6h9ACitQAAivB8h8QB8h9CwAAQCuAAB9B9QB8B8AACvIAAAIQgCCuh8B5Qh8B6itgCIgBABgAj7j0QhjBmABCOIAAAMQACCQBoBkQBoBkCPgCQCQgDBkhoQBkhngCiQIAAgHQgCiOhlhkQhmhkiOACIgHAAQiPABhkBmg");
    var mask_1_graphics_129 = new cjs.Graphics().p("AjaJGQhqglhXhHIAwg7QBNA+BdAhQBdAhBjgBQDqAAChidQChidAAjkQAAjjihidQihidjqAAQjbAAieCLQidCLgVDQIhLgIQAHhDATg8QAwibCAhyQC0ieD4AAQEKAAC3CzQC4CzAAEDIgBAAQAAEEi3CzQi4CzkJAAIgGAAQhtAAhmgkgAgJGoQiugCh5h8Qh6h9ACitQAAivB8h8QB8h9CwAAQCuAAB9B9QB8B8AACvIAAAIQgCCuh8B5Qh8B6itgCIgBABgAj7j0QhjBmABCOIAAAMQACCQBoBkQBoBkCPgCQCQgDBkhoQBkhngCiQIAAgHQgCiOhlhkQhmhkiOACIgHAAQiPABhkBmg");
    var mask_1_graphics_130 = new cjs.Graphics().p("AjaJGQhqglhXhHIAwg7QBNA+BdAhQBdAhBjgBQDqAAChidQChidAAjkQAAjjihidQihidjqAAQjbAAieCLQidCLgVDQIhLgIQAHhDATg8QAwibCAhyQC0ieD4AAQEKAAC3CzQC4CzAAEDIgBAAQAAEEi3CzQi4CzkJAAIgGAAQhtAAhmgkgAgJGoQiugCh5h8Qh6h9ACitQAAivB8h8QB8h9CwAAQCuAAB9B9QB8B8AACvIAAAIQgCCuh8B5Qh8B6itgCIgBABgAj7j0QhjBmABCOIAAAMQACCQBoBkQBoBkCPgCQCQgDBkhoQBkhngCiQIAAgHQgCiOhlhkQhmhkiOACIgHAAQiPABhkBmg");
    var mask_1_graphics_131 = new cjs.Graphics().p("AjaJGQhqglhXhHIAwg7QBNA+BdAhQBdAhBjgBQDqAAChidQChidAAjkQAAjjihidQihidjqAAQjbAAieCLQidCLgVDQIhLgIQAHhDATg8QAwibCAhyQC0ieD4AAQEKAAC3CzQC4CzAAEDIgBAAQAAEEi3CzQi4CzkJAAIgGAAQhtAAhmgkgAgJGoQiugCh5h8Qh6h9ACitQAAivB8h8QB8h9CwAAQCuAAB9B9QB8B8AACvIAAAIQgCCuh8B5Qh8B6itgCIgBABgAj7j0QhjBmABCOIAAAMQACCQBoBkQBoBkCPgCQCQgDBkhoQBkhngCiQIAAgHQgCiOhlhkQhmhkiOACIgHAAQiPABhkBmg");
    var mask_1_graphics_132 = new cjs.Graphics().p("AjaJGQhqglhXhHIAwg7QBNA+BdAhQBdAhBjgBQDqAAChidQChidAAjkQAAjjihidQihidjqAAQjbAAieCLQidCLgVDQIhLgIQAHhDATg8QAwibCAhyQC0ieD4AAQEKAAC3CzQC4CzAAEDIgBAAQAAEEi3CzQi4CzkJAAIgGAAQhtAAhmgkgAgJGoQiugCh5h8Qh6h9ACitQAAivB8h8QB8h9CwAAQCuAAB9B9QB8B8AACvIAAAIQgCCuh8B5Qh8B6itgCIgBABgAj7j0QhjBmABCOIAAAMQACCQBoBkQBoBkCPgCQCQgDBkhoQBkhngCiQIAAgHQgCiOhlhkQhmhkiOACIgHAAQiPABhkBmg");
    var mask_1_graphics_133 = new cjs.Graphics().p("AjaJGQhqglhXhHIAwg7QBNA+BdAhQBdAhBjgBQDqAAChidQChidAAjkQAAjjihidQihidjqAAQjbAAieCLQidCLgVDQIhLgIQAHhDATg8QAwibCAhyQC0ieD4AAQEKAAC3CzQC4CzAAEDIgBAAQAAEEi3CzQi4CzkJAAIgGAAQhtAAhmgkgAgJGoQiugCh5h8Qh6h9ACitQAAivB8h8QB8h9CwAAQCuAAB9B9QB8B8AACvIAAAIQgCCuh8B5Qh8B6itgCIgBABgAj7j0QhjBmABCOIAAAMQACCQBoBkQBoBkCPgCQCQgDBkhoQBkhngCiQIAAgHQgCiOhlhkQhmhkiOACIgHAAQiPABhkBmg");
    var mask_1_graphics_134 = new cjs.Graphics().p("AjaJGQhqglhXhHIAwg7QBNA+BdAhQBdAhBjgBQDqAAChidQChidAAjkQAAjjihidQihidjqAAQjbAAieCLQidCLgVDQIhLgIQAHhDATg8QAwibCAhyQC0ieD4AAQEKAAC3CzQC4CzAAEDIgBAAQAAEEi3CzQi4CzkJAAIgGAAQhtAAhmgkgAgJGoQiugCh5h8Qh6h9ACitQAAivB8h8QB8h9CwAAQCuAAB9B9QB8B8AACvIAAAIQgCCuh8B5Qh8B6itgCIgBABgAj7j0QhjBmABCOIAAAMQACCQBoBkQBoBkCPgCQCQgDBkhoQBkhngCiQIAAgHQgCiOhlhkQhmhkiOACIgHAAQiPABhkBmg");
  
    this.timeline.addTween(cjs.Tween.get(mask_1).to({graphics:null,x:0,y:0}).wait(1).to({graphics:mask_1_graphics_1,x:788,y:86.1}).wait(1).to({graphics:mask_1_graphics_2,x:786.7,y:85.1}).wait(1).to({graphics:mask_1_graphics_3,x:785.4,y:82.4}).wait(1).to({graphics:mask_1_graphics_4,x:784.7,y:79.3}).wait(1).to({graphics:mask_1_graphics_5,x:784.4,y:75.5}).wait(1).to({graphics:mask_1_graphics_6,x:784.4,y:71.5}).wait(1).to({graphics:mask_1_graphics_7,x:784.4,y:68.2}).wait(1).to({graphics:mask_1_graphics_8,x:784.4,y:64.3}).wait(1).to({graphics:mask_1_graphics_9,x:785.2,y:61}).wait(1).to({graphics:mask_1_graphics_10,x:787.8,y:58.7}).wait(1).to({graphics:mask_1_graphics_11,x:791.3,y:56.3}).wait(1).to({graphics:mask_1_graphics_12,x:795.1,y:55.2}).wait(1).to({graphics:mask_1_graphics_13,x:798.1,y:55}).wait(1).to({graphics:mask_1_graphics_14,x:802.1,y:55}).wait(1).to({graphics:mask_1_graphics_15,x:806.3,y:55}).wait(1).to({graphics:mask_1_graphics_16,x:809.4,y:55}).wait(1).to({graphics:mask_1_graphics_17,x:813.1,y:55}).wait(1).to({graphics:mask_1_graphics_18,x:815.9,y:55}).wait(1).to({graphics:mask_1_graphics_19,x:817.8,y:55}).wait(1).to({graphics:mask_1_graphics_20,x:818.8,y:55}).wait(1).to({graphics:mask_1_graphics_21,x:819,y:55}).wait(1).to({graphics:mask_1_graphics_22,x:819,y:55}).wait(1).to({graphics:mask_1_graphics_23,x:819,y:55}).wait(1).to({graphics:mask_1_graphics_24,x:819,y:55}).wait(1).to({graphics:mask_1_graphics_25,x:819,y:57.5}).wait(1).to({graphics:mask_1_graphics_26,x:819,y:59.6}).wait(1).to({graphics:mask_1_graphics_27,x:819,y:61.2}).wait(1).to({graphics:mask_1_graphics_28,x:819,y:61.7}).wait(1).to({graphics:mask_1_graphics_29,x:819,y:61.8}).wait(1).to({graphics:mask_1_graphics_30,x:819,y:61.8}).wait(1).to({graphics:mask_1_graphics_31,x:819,y:61.8}).wait(1).to({graphics:mask_1_graphics_32,x:819,y:61.8}).wait(1).to({graphics:mask_1_graphics_33,x:819,y:66.4}).wait(1).to({graphics:mask_1_graphics_34,x:819,y:68.2}).wait(1).to({graphics:mask_1_graphics_35,x:819,y:69.7}).wait(1).to({graphics:mask_1_graphics_36,x:819,y:70.7}).wait(1).to({graphics:mask_1_graphics_37,x:819,y:71.3}).wait(1).to({graphics:mask_1_graphics_38,x:819,y:71.5}).wait(1).to({graphics:mask_1_graphics_39,x:819,y:71.5}).wait(1).to({graphics:mask_1_graphics_40,x:819,y:71.5}).wait(1).to({graphics:mask_1_graphics_41,x:819,y:71.5}).wait(1).to({graphics:mask_1_graphics_42,x:819,y:71.5}).wait(1).to({graphics:mask_1_graphics_43,x:819,y:71.5}).wait(1).to({graphics:mask_1_graphics_44,x:819.5,y:71.5}).wait(1).to({graphics:mask_1_graphics_45,x:822,y:71.5}).wait(1).to({graphics:mask_1_graphics_46,x:824.2,y:71.5}).wait(1).to({graphics:mask_1_graphics_47,x:826,y:71.5}).wait(1).to({graphics:mask_1_graphics_48,x:827.8,y:71.5}).wait(1).to({graphics:mask_1_graphics_49,x:828.8,y:71.5}).wait(1).to({graphics:mask_1_graphics_50,x:829.4,y:71.5}).wait(1).to({graphics:mask_1_graphics_51,x:829.5,y:71.5}).wait(1).to({graphics:mask_1_graphics_52,x:829.5,y:71.5}).wait(1).to({graphics:mask_1_graphics_53,x:829.5,y:71.5}).wait(1).to({graphics:mask_1_graphics_54,x:829.5,y:71.5}).wait(1).to({graphics:mask_1_graphics_55,x:829.5,y:71.5}).wait(1).to({graphics:mask_1_graphics_56,x:829.5,y:70.6}).wait(1).to({graphics:mask_1_graphics_57,x:829.5,y:68.2}).wait(1).to({graphics:mask_1_graphics_58,x:829.5,y:66}).wait(1).to({graphics:mask_1_graphics_59,x:829.5,y:64.3}).wait(1).to({graphics:mask_1_graphics_60,x:829.5,y:63.1}).wait(1).to({graphics:mask_1_graphics_61,x:829.5,y:62.1}).wait(1).to({graphics:mask_1_graphics_62,x:829.5,y:61.8}).wait(1).to({graphics:mask_1_graphics_63,x:829.5,y:61.8}).wait(1).to({graphics:mask_1_graphics_64,x:829.5,y:61.8}).wait(1).to({graphics:mask_1_graphics_65,x:829.5,y:61.8}).wait(1).to({graphics:mask_1_graphics_66,x:829.5,y:61.8}).wait(1).to({graphics:mask_1_graphics_67,x:829.5,y:61.8}).wait(1).to({graphics:mask_1_graphics_68,x:829.3,y:61.8}).wait(1).to({graphics:mask_1_graphics_69,x:825.8,y:61.8}).wait(1).to({graphics:mask_1_graphics_70,x:823.1,y:61.8}).wait(1).to({graphics:mask_1_graphics_71,x:821.2,y:61.8}).wait(1).to({graphics:mask_1_graphics_72,x:819.9,y:61.8}).wait(1).to({graphics:mask_1_graphics_73,x:819.2,y:61.8}).wait(46).to({graphics:mask_1_graphics_119,x:819.2,y:61.8}).wait(1).to({graphics:mask_1_graphics_120,x:819.2,y:61.8}).wait(1).to({graphics:mask_1_graphics_121,x:819.2,y:61.8}).wait(1).to({graphics:mask_1_graphics_122,x:819.2,y:61.8}).wait(1).to({graphics:mask_1_graphics_123,x:819.2,y:61.8}).wait(1).to({graphics:mask_1_graphics_124,x:819.2,y:61.8}).wait(1).to({graphics:mask_1_graphics_125,x:819.2,y:61.8}).wait(1).to({graphics:mask_1_graphics_126,x:819.2,y:61.8}).wait(1).to({graphics:mask_1_graphics_127,x:819.2,y:61.8}).wait(1).to({graphics:mask_1_graphics_128,x:819.2,y:61.8}).wait(1).to({graphics:mask_1_graphics_129,x:819.2,y:61.8}).wait(1).to({graphics:mask_1_graphics_130,x:819.2,y:61.8}).wait(1).to({graphics:mask_1_graphics_131,x:819.2,y:61.8}).wait(1).to({graphics:mask_1_graphics_132,x:819.2,y:61.8}).wait(1).to({graphics:mask_1_graphics_133,x:819.2,y:61.8}).wait(1).to({graphics:mask_1_graphics_134,x:819.2,y:61.8}).wait(1));
  
    // O
    this.shape_1 = new cjs.Shape();
    this.shape_1.graphics.f("#fff").s().p("AjaJGQhqglhXhHIAwg7QBNA+BdAhQBdAhBjgBQDqAAChidQChidAAjkQAAjjihidQihidjqAAQjbAAieCLQidCLgVDQIhLgIQAHhDATg8QAwibCAhyQC0ieD4AAQEKAAC3CzQC4CzAAEDIgBAAQAAEEi3CzQi4CzkJAAIgGAAQhtAAhmgkgAgJGoQiugCh5h8Qh6h9ACitQAAivB8h8QB8h9CwAAQCuAAB9B9QB8B8AACvIAAAIQgCCuh8B5Qh8B6itgCIgBABgAj7j0QhjBmABCOIAAAMQACCQBoBkQBoBkCPgCQCQgDBkhoQBkhngCiQIAAgHQgCiOhlhkQhmhkiOACIgHAAQiPABhkBmg");
    this.shape_1.setTransform(819.2,61.8);
  
    this.instance_2 = new lib.Tween11("synched",0);
    this.instance_2.parent = this;
    this.instance_2.setTransform(819.2,61.8);
    this.instance_2._off = true;
  
    this.instance_3 = new lib.Tween12("synched",0);
    this.instance_3.parent = this;
    this.instance_3.setTransform(819.2,61.8);
    this.instance_3.alpha = 0;
  
    var maskedShapeInstanceList = [this.shape_1,this.instance_2,this.instance_3];
  
    for(var shapedInstanceItr = 0; shapedInstanceItr < maskedShapeInstanceList.length; shapedInstanceItr++) {
      maskedShapeInstanceList[shapedInstanceItr].mask = mask_1;
    }
  
    this.timeline.addTween(cjs.Tween.get({}).to({state:[]}).to({state:[{t:this.shape_1}]},1).to({state:[{t:this.instance_2}]},118).to({state:[{t:this.instance_3}]},15).wait(1));
    this.timeline.addTween(cjs.Tween.get(this.instance_2).wait(119).to({_off:false},0).to({_off:true,alpha:0},15).wait(1));
  
    // S mask (mask)
    var mask_2 = new cjs.Shape();
    mask_2._off = true;
    var mask_2_graphics_1 = new cjs.Graphics().p("AgzgGIA9guQARAVAZASIgkBCQgngagcghg");
    var mask_2_graphics_2 = new cjs.Graphics().p("AAMA0Qg1gfgjgpIA+guQAZAgArAXIALAGIAMAFIgkBDQgPgHgOgIg");
    var mask_2_graphics_3 = new cjs.Graphics().p("AgGAoQg1gfgkgqIA+guQAaAgAqAYQAbAPAfAIIACBQQg2gNgvgbg");
    var mask_2_graphics_4 = new cjs.Graphics().p("AgmAjQg1gfgjgpIA9guQAaAgAqAXQAzAcA8AHIANBNQhbgGhKgrg");
    var mask_2_graphics_5 = new cjs.Graphics().p("AhAAiQg2gegjgqIA+guQAaAgArAXQA+AjBQADIAiBMIgSAAQhxAAhXgzg");
    var mask_2_graphics_6 = new cjs.Graphics().p("AhhAiQg2gegjgqIA+guQAaAgArAXQBEAmBaAAQAcAAAZgFIAfBHQgnAKgtAAQhwAAhYgzg");
    var mask_2_graphics_7 = new cjs.Graphics().p("Ah4AiQg2gegjgqIA+guQAaAgArAXQBFAmBZAAQA6AAAogTIAgBEQg4AbhKAAQhwAAhYgzg");
    var mask_2_graphics_8 = new cjs.Graphics().p("AiVAiQg1gegjgqIA9guQAaAgArAXQBGAmBYAAQBCAAAqgXIBPAgIgQAOIgGAEQhBAxhkAAQhvAAhZgzg");
    var mask_2_graphics_9 = new cjs.Graphics().p("AioAiQg1gegjgqIA9guQAaAgArAXQBGAmBYAAQBKAAAsgeQAPgLAKgNIBSAHQgJAWgPASQgMAQgSAOIgGAEQhBAxhkAAQhvAAhZgzg");
    var mask_2_graphics_10 = new cjs.Graphics().p("AiuAvQg1gfgjgqIA9guQAaAgArAYQBGAlBYAAQBKAAAsgeQAqgeABgyIBMgHIAAAEQAAA+gkAsQgMAQgSAOIgGAFQhBAwhkAAQhvAAhZgyg");
    var mask_2_graphics_11 = new cjs.Graphics().p("AiuBTQg1gfgjgrIA9gtQAaAgArAXQBGAmBYAAQBKAAAsgfQArgeAAg0QAAgNgEgMIA8gzQAUAhAAArQAAA9gkAuQgMAPgSAOIgGAFQhBAwhkAAQhvAAhZgyg");
    var mask_2_graphics_12 = new cjs.Graphics().p("AiuBsQg1gfgjgqIA9gtQAaAfArAXQBGAnBYAAQBKAAAsgfQArggAAgzQAAgpgqgZIAug9IAAABQAVANAPARQAkAoAAA5QAAA9gkAtQgMAQgSAOIgGAEQhBAxhkAAQhvAAhZgzg");
    var mask_2_graphics_13 = new cjs.Graphics().p("AiuB6Qg1gegjgrIA9guQAaAgArAXQBGAnBYAAQBKAAAsgfQArgfAAgzQAAgpgqgaQgOgIgYgKIAZhHQAjAOAUAMIAEADQAVANAPARQAkAoAAA5QAAA9gkAtQgMAQgSAOIgGAEQhBAxhkAAQhvAAhZgzg");
    var mask_2_graphics_14 = new cjs.Graphics().p("AiuCHQg1gfgjgqIA9guQAaAgArAXQBGAnBYAAQBKAAAsgfQArggAAgzQAAgpgqgZQgggThTgZIAbhHQBcAcAmAYIAEACQAVAOAPARQAkAoAAA5QAAA9gkAtQgMAQgSAOIgGAEQhBAxhkAAQhvAAhZgzg");
    var mask_2_graphics_15 = new cjs.Graphics().p("AiuCRQg1gfgjgrIA9guQAaAgArAYQBGAmBYAAQBKAAAsgfQArgfAAg0QAAgogqgaQgmgWhxggIAAAAIgjgKIAihFIAUAGIAFABIARAFQBuAgAqAaIAEADQAVAOAPAQQAkAoAAA4QAAA+gkAuQgMAQgSANIgGAFQhBAwhkAAQhvAAhZgyg");
    var mask_2_graphics_16 = new cjs.Graphics().p("AiuCYQg1gegjgrIA9guQAaAgArAXQBGAnBYAAQBKAAAsgfQArgfAAg0QAAgogqgaQgmgWhxghQgwgNgogOIAlhDQAhALAlAKQCAAlAuAcIAEADQAVANAPARQAkAoAAA4QAAA+gkAtQgMAQgSAOIgGAEQhBAxhkAAQhvAAhZgzg");
    var mask_2_graphics_17 = new cjs.Graphics().p("AiuCjQg1gegjgrIA9guQAaAgArAXQBGAnBYAAQBKAAAsgfQArgfAAg0QAAgogqgaQgmgWhxghQhWgYg/gZIAmhDQA2AWBMAVQCAAlAuAcIAEADQAVANAPARQAkAoAAA4QAAA+gkAtQgMAQgSAOIgGAEQhBAxhkAAQhvAAhZgzg");
    var mask_2_graphics_18 = new cjs.Graphics().p("AiuCvQg1gfgjgqIA9guQAaAgArAXQBGAnBYAAQBKAAAsgfQArggAAg0QAAgogqgZQgmgXhxggQiMgnhNgrIgSgLIBHgvIAOAHQA/AeBqAeQCAAkAuAdIAEADQAVANAPARQAkAoAAA4QAAA+gkAtQgMAQgSAOIgGAEQhBAxhkAAQhvAAhZgzg");
    var mask_2_graphics_19 = new cjs.Graphics().p("AiWDFQg2gfgjgrIA+guQAaAgArAYQBFAmBZAAQBJAAAsgfQAsgfAAg0QAAgpgrgZQgmgWhyggQiugxhMg4QgdgVgXgZIBMggQANALAPAKQA+AsCdAtQB/AkAuAcIAFADQAVAOAPAQQAkAnAAA5QAAA+gkAuQgNAQgRANIgGAFQhCAwhjAAQhwAAhYgyg");
    var mask_2_graphics_20 = new cjs.Graphics().p("Ah+DhQg1gfgkgrIA+guQAaAgArAYQBFAmBZAAQBJAAAsgfQAsgfAAg0QAAgpgqgaQgngVhxggQivgxhMg4QhDgxghhHIBQgOQAaAtAuAgQA+AsCdAtQCAAkAtAcIAFADQAVAOAPAQQAkAnAAA5QAAA+gkAtQgNAQgRAOIgGAFQhBAwhkAAQhvAAhZgyg");
    var mask_2_graphics_21 = new cjs.Graphics().p("AhxEFQg2gfgjgqIA+gvQAaAhArAXQBFAnBZAAQBJAAAsggQAsgfAAg0QAAgpgrgZQgmgXhyggQiugwhMg4QhphMgViDIBQAIQAVBZBNA2QA+AsCdAsQB/AkAuAdIAFACQAVANAPAQQAkApAAA4QAAA/gkAtQgNAQgRANIgGAFQhBAwhkAAQhwAAhYgyg");
    var mask_2_graphics_22 = new cjs.Graphics().p("AhvEpQg1gfgjgqIA9guQAaAgArAXQBFAnBZAAQBKAAAsgfQArggAAg0QAAgpgqgZQgmgXhyggQivgwhMg3QiChfAAiyIAAgHIBMANQACCJBoBJQA+AsCdAsQCAAkAuAcIAEACQAVAOAPARQAkAoAAA5QAAA+gkAtQgMAQgSAOIgGAEQhBAxhkAAQhvAAhZgzg");
    var mask_2_graphics_23 = new cjs.Graphics().p("AhvFZQg1gfgjgrIA9guQAaAgArAYQBFAmBZAAQBKAAAsgfQArgfAAg0QAAgpgqgaQgmgWhyggQivgxhMg3QiChfAAiyQAAg2AOgvIBIAbQgKAjAAAoQAACNBqBKQA+AsCdAsQCAAkAuAcIAEADQAVAOAPAQQAkAoAAA5QAAA+gkAuQgMAQgSANIgGAFQhBAwhkAAQhvAAhZgyg");
    var mask_2_graphics_24 = new cjs.Graphics().p("AhvGDQg1gfgjgqIA9guQAaAgArAXQBFAnBZAAQBKAAAsgfQArggAAg0QAAgpgqgZQgmgXhyggQivgxhMg3QiCheAAiyQAAhrA2hQIAvBKQgZAzAAA+QAACNBqBLQA+ArCdAsQCAAkAuAdIAEACQAVAOAPARQAkAoAAA5QAAA+gkAtQgMAQgSAOIgGAEQhBAxhkAAQhvAAhZgzg");
    var mask_2_graphics_25 = new cjs.Graphics().p("AhvGhQg1gfgjgrIA9guQAaAgArAYQBFAmBZAAQBKAAAsgfQArgfAAg0QAAgpgqgaQgmgWhyggQivgxhMg4QiCheAAiyQAAiVBqhgIAmBIQhEBHAABnQAACNBqBKQA+ArCdAtQCAAkAuAcIAEADQAVAOAPAQQAkAoAAA5QAAA+gkAuQgMAQgSANIgGAFQhBAwhkAAQhvAAhZgyg");
    var mask_2_graphics_26 = new cjs.Graphics().p("AhvG5Qg1gfgjgqIA9guQAaAgArAXQBFAnBZAAQBKAAAsgfQArggAAg0QAAgpgqgZQgmgXhyggQivgxhMg3QiCheAAiyQAAicB0hjQAagWAdgSIAhBIQgRALgQANQhfBNAAB6QAACNBqBKQA+AsCdAsQCAAkAuAdIAEACQAVAOAPARQAkAoAAA5QAAA+gkAtQgMAQgSAOIgGAEQhBAxhkAAQhvAAhZgzg");
    var mask_2_graphics_27 = new cjs.Graphics().p("AhvHIQg1gfgjgqIA9gvQAaAhArAXQBFAnBZAAQBKAAAsggQArgfAAg0QAAgpgqgZQgmgXhyggQivgxhMg4QiCheAAixQAAidB0hiQA0gtBDgYIAXBKQgwATgnAgQhfBMAAB7QAACNBqBKQA+AsCdAsQCAAkAuAdIAEACQAVAOAPAQQAkApAAA4QAAA/gkAtQgMAQgSANIgGAFQhBAwhkAAQhvAAhZgyg");
    var mask_2_graphics_28 = new cjs.Graphics().p("AhvHSQg1gfgjgrIA9guQAaAgArAYQBFAmBZAAQBKAAAsgfQArgfAAg0QAAgpgqgaQgmgWhyggQivgxhMg4QiChfAAixQAAicB0hiQBAg3BVgZQATgFATgEIgFBRQhJATg2AsQhfBNAAB7QAACNBqBJQA+AsCdAtQCAAkAuAcIAEADQAVAOAPAQQAkAoAAA5QAAA+gkAtQgMAQgSAOIgGAFQhBAwhkAAQhvAAhZgyg");
    var mask_2_graphics_29 = new cjs.Graphics().p("AhvHXQg1gfgjgrIA9guQAaAgArAYQBFAmBZAAQBKAAAsgfQArgfAAg0QAAgpgqgaQgmgWhyggQivgxhMg4QiChfAAixQAAicB0hiQBAg3BVgZQA+gSBKgBIgSBOQh/AHhVBFQhfBNAAB7QAACNBqBJQA+AsCdAtQCAAkAuAcIAEADQAVAOAPAQQAkAoAAA5QAAA+gkAuQgMAPgSAOIgGAFQhBAwhkAAQhvAAhZgyg");
    var mask_2_graphics_30 = new cjs.Graphics().p("AhvHXQg1gfgjgrIA9guQAaAgArAYQBFAmBZAAQBKAAAsgfQArgfAAg0QAAgpgqgaQgmgWhyggQivgxhMg4QiChfAAixQAAicB0hiQBAg3BVgZQBDgTBSAAQAvAAAsAHIgkBJQgbgDgbAAQiVAAhfBNQhfBNAAB7QAACNBqBJQA+AsCdAtQCAAkAuAcIAEADQAVAOAPAQQAkAoAAA5QAAA+gkAuQgMAPgSAOIgGAFQhBAwhkAAQhvAAhZgyg");
    var mask_2_graphics_31 = new cjs.Graphics().p("AhvHXQg1gfgjgrIA9guQAaAgArAYQBFAmBZAAQBKAAAsgfQArgfAAg0QAAgpgqgaQgmgWhyggQivgxhMg4QiChfAAixQAAicB0hiQBAg3BVgZQBDgTBSAAQBOAABGATIARAFIADABIgsBEQg6gQhBAAQiVAAhfBNQhfBNAAB7QAACNBqBJQA+AsCdAtQCAAkAuAcIAEADQAVAOAPAQQAkAoAAA5QAAA+gkAuQgMAPgSAOIgGAFQhBAwhkAAQhvAAhZgyg");
    var mask_2_graphics_32 = new cjs.Graphics().p("AiKHXQg1gfgjgrIA9guQAaAgArAYQBGAmBYAAQBKAAAsgfQArgfAAg0QAAgpgqgaQgmgWhyggQivgxhMg4QiChfAAixQAAicB0hiQBAg3BVgZQBEgTBRAAQBOAABGATIARAFQASAGASAHQAUAIASAJIgxBAIgZgLQhMgehYAAQiVAAhfBNQhfBNAAB7QAACNBqBJQA+AsCdAtQCAAkAuAcIAEADQAVAOAPAQQAkAoAAA5QAAA+gkAuQgNAPgRAOIgGAFQhBAwhkAAQhvAAhZgygAFfm1IACACIgDAEg");
    var mask_2_graphics_33 = new cjs.Graphics().p("AiKHXQg1gfgjgrIA9guQAaAgArAYQBGAmBYAAQBKAAAsgfQArgfAAg0QAAgpgqgaQgmgWhyggQivgxhMg4QiChfAAixQAAicB0hiQBAg3BVgZQBEgTBRAAQBOAABGATIARAFQASAGASAHQAvATArAdIgwA/QgTgNgVgLIgCgBQgSgJgSgHQhMgehYAAQiVAAhfBNQhfBNAAB7QAACNBqBJQA+AsCdAtQCAAkAuAcIAEADQAVAOAPAQQAkAoAAA5QAAA+gkAuQgNAPgRAOIgGAFQhBAwhkAAQhvAAhZgyg");
    var mask_2_graphics_34 = new cjs.Graphics().p("AiqHXQg1gfgkgrIA+guQAaAgArAYQBFAmBZAAQBJAAAsgfQAsgfAAg0QAAgpgqgaQgngWhwggQiwgxhMg4QiChfgBixQAAicB0hiQBBg3BVgZQBEgTBQAAQBPAABGATIARAFQASAGASAHQA6AXAzAmQAZATAVATIg0A7IgdgbIgCACQgfgagngUIgCgBQgSgJgSgHQhMgehZAAQiVAAheBNQhfBNAAB7QAACNBqBJQA+AsCdAtQCAAkAtAcIAFADQAVAOAPAQQAkAoAAA5QAAA+gkAuQgNAPgRAOIgGAFQhBAwhkAAQhvAAhZgyg");
    var mask_2_graphics_35 = new cjs.Graphics().p("AjEHXQg2gfgjgrIA+guQAaAgArAYQBFAmBZAAQBJAAAsgfQAsgfAAg0QAAgpgrgaQgmgWhxggQivgxhMg4QiDhfAAixQAAicB0hiQBAg3BWgZQBEgTBQAAQBPAABGATIARAFQASAGASAHQA5AXA0AmQA4AqAfAqIALAPIgxAwIhUhNIgDACQgfgagmgUIgCgBQgSgJgSgHQhNgehYAAQiVAAheBNQhfBNAAB7QAACNBqBJQA+AsCeAtQB+AkAuAcIAFADQAVAOAPAQQAkAoAAA5QAAA+gkAuQgNAPgRAOIgGAFQhBAwhkAAQhwAAhYgyg");
    var mask_2_graphics_36 = new cjs.Graphics().p("AjEHXQg2gfgjgrIA+guQAaAgArAYQBFAmBZAAQBJAAAsgfQAsgfAAg0QAAgpgrgaQgmgWhxggQivgxhMg4QiDhfAAixQAAicB0hiQBAg3BWgZQBEgTBQAAQBPAABGATIARAFQASAGASAHQA5AXA0AmQA4AqAfAqIALAPIhbBaIg0g2IAkgkQgpgrg5geIgCgBQgSgJgSgHQhNgehYAAQiVAAheBNQhfBNAAB7QAACNBqBJQA+AsCeAtQB+AkAuAcIAFADQAVAOAPAQQAkAoAAA5QAAA+gkAuQgNAPgRAOIgGAFQhBAwhkAAQhwAAhYgyg");
    var mask_2_graphics_37 = new cjs.Graphics().p("AjEHXQg2gfgjgrIA+guQAaAgArAYQBFAmBZAAQBJAAAsgfQAsgfAAg0QAAgpgrgaQgmgWhxggQivgxhMg4QiDhfAAixQAAicB0hiQBAg3BWgZQBEgTBQAAQBPAABGATIARAFQASAGASAHQA5AXA0AmQA4AqAfAqIALAPIiLCLIg1g5IABgBIACABIBShSQgpgrg5geIgCgBQgSgJgSgHQhNgehYAAQiVAAheBNQhfBNAAB7QAACNBqBJQA+AsCeAtQB+AkAuAcIAFADQAVAOAPAQQAkAoAAA5QAAA+gkAuQgNAPgRAOIgGAFQhBAwhkAAQhwAAhYgyg");
    var mask_2_graphics_38 = new cjs.Graphics().p("AjEHXQg2gfgjgrIA+guQAaAgArAYQBFAmBZAAQBJAAAsgfQAsgfAAg0QAAgpgrgaQgmgWhxggQivgxhMg4QiDhfAAixQAAicB0hiQBAg3BWgZQBEgTBQAAQBPAABGATIARAFQASAGASAHQA5AXA0AmQA4AqAfAqIALAPIi9C8IgSgTIgQgOIAghKIACABIBShSQgpgrg5geIgCgBQgSgJgSgHQhNgehYAAQiVAAheBNQhfBNAAB7QAACNBqBJQA+AsCeAtQB+AkAuAcIAFADQAVAOAPAQQAkAoAAA5QAAA+gkAuQgNAPgRAOIgGAFQhBAwhkAAQhwAAhYgyg");
    var mask_2_graphics_39 = new cjs.Graphics().p("AjEHXQg2gfgjgrIA+guQAaAgArAYQBFAmBZAAQBJAAAsgfQAsgfAAg0QAAgpgrgaQgmgWhxggQivgxhMg4QiDhfAAixQAAicB0hiQBAg3BWgZQBEgTBQAAQBPAABGATIARAFQASAGASAHQA5AXA0AmQA4AqAfAqIALAPIi9C8IgSgTQgQgPgUgPIgMgIIgPgLIAchPQATALAQAOIASAQIBShSQgpgrg5geIgCgBQgSgJgSgHQhNgehYAAQiVAAheBNQhfBNAAB7QAACNBqBJQA+AsCeAtQB+AkAuAcIAFADQAVAOAPAQQAkAoAAA5QAAA+gkAuQgNAPgRAOIgGAFQhBAwhkAAQhwAAhYgyg");
    var mask_2_graphics_40 = new cjs.Graphics().p("AjEHXQg2gfgjgrIA+guQAaAgArAYQBFAmBZAAQBJAAAsgfQAsgfAAg0QAAgpgrgaQgmgWhxggQivgxhMg4QiDhfAAixQAAicB0hiQBAg3BWgZQBEgTBQAAQBPAABGATIARAFQASAGASAHQA5AXA0AmQA4AqAfAqIALAPIi9C8IgSgTQgQgPgUgPIgMgIQgkgagjgPIAQhPQAXAHAWALIAWALQATAMARAOIASAQIBShSQgpgrg5geIgCgBQgSgJgSgHQhNgehYAAQiVAAheBNQhfBNAAB7QAACNBqBJQA+AsCeAtQB+AkAuAcIAFADQAVAOAPAQQAkAoAAA5QAAA+gkAuQgNAPgRAOIgGAFQhBAwhkAAQhwAAhYgyg");
    var mask_2_graphics_41 = new cjs.Graphics().p("AjEHXQg2gfgjgrIA+guQAaAgArAYQBFAmBZAAQBJAAAsgfQAsgfAAg0QAAgpgrgaQgmgWhxggQivgxhMg4QiDhfAAixQAAicB0hiQBAg3BWgZQBEgTBQAAQBPAABGATIARAFQASAGASAHQA5AXA0AmQA4AqAfAqIALAPIi9C8IgSgTQgQgPgUgPIgMgIQg4gog0gNQgOgDgOgBIAHhMQBAAEA3AbIAWALQATAMARAOIASAQIBShSQgpgrg5geIgCgBQgSgJgSgHQhNgehYAAQiVAAheBNQhfBNAAB7QAACNBqBJQA+AsCeAtQB+AkAuAcIAFADQAVAOAPAQQAkAoAAA5QAAA+gkAuQgNAPgRAOIgGAFQhBAwhkAAQhwAAhYgyg");
    var mask_2_graphics_42 = new cjs.Graphics().p("AjEHXQg2gfgjgrIA+guQAaAgArAYQBFAmBZAAQBJAAAsgfQAsgfAAg0QAAgpgrgaQgmgWhxggQivgxhMg4QiDhfAAixQAAicB0hiQBAg3BWgZQBEgTBQAAQBPAABGATIARAFQASAGASAHQA5AXA0AmQA4AqAfAqIALAPIi9C8IgSgTQgQgPgUgPIgMgIQg4gog0gNQgPgDgPgCIhRhEQARgEASgCQAPgCAQAAQBOAABBAgIAWALQATAMARAOIASAQIBShSQgpgrg5geIgCgBQgSgJgSgHQhNgehYAAQiVAAheBNQhfBNAAB7QAACNBqBJQA+AsCeAtQB+AkAuAcIAFADQAVAOAPAQQAkAoAAA5QAAA+gkAuQgNAPgRAOIgGAFQhBAwhkAAQhwAAhYgyg");
    var mask_2_graphics_43 = new cjs.Graphics().p("AjEHXQg2gfgjgrIA+guQAaAgArAYQBFAmBZAAQBJAAAsgfQAsgfAAg0QAAgpgrgaQgmgWhxggQivgxhMg4QiDhfAAixQAAicB0hiQBAg3BWgZQBEgTBQAAQBPAABGATIARAFQASAGASAHQA5AXA0AmQA4AqAfAqIALAPIi9C8IgSgTQgQgPgUgPIgMgIQhQg6hKAAQggAAgbAJIhUgoQA6gtBWAAQBOAABBAgIAWALQATAMARAOIASAQIBShSQgpgrg5geIgCgBQgSgJgSgHQhNgehYAAQiVAAheBNQhfBNAAB7QAACNBqBJQA+AsCeAtQB+AkAuAcIAFADQAVAOAPAQQAkAoAAA5QAAA+gkAuQgNAPgRAOIgGAFQhBAwhkAAQhwAAhYgyg");
    var mask_2_graphics_44 = new cjs.Graphics().p("AjEHXQg2gfgjgrIA+guQAaAgArAYQBFAmBZAAQBJAAAsgfQAsgfAAg0QAAgpgrgaQgmgWhxggQivgxhMg4QiDhfAAixQAAicB0hiQBAg3BWgZQBEgTBQAAQBPAABGATIARAFQASAGASAHQA5AXA0AmQA4AqAfAqIALAPIi9C8IgSgTQgQgPgUgPIgMgIQhQg6hKAAQg4AAgmAaQgVAPgIASIhIgXQANghAhgcQA7gzBbAAQBOAABBAgIAWALQATAMARAOIASAQIBShSQgpgrg5geIgCgBQgSgJgSgHQhNgehYAAQiVAAheBNQhfBNAAB7QAACNBqBJQA+AsCeAtQB+AkAuAcIAFADQAVAOAPAQQAkAoAAA5QAAA+gkAuQgNAPgRAOIgGAFQhBAwhkAAQhwAAhYgyg");
    var mask_2_graphics_45 = new cjs.Graphics().p("AjEHXQg2gfgjgrIA+guQAaAgArAYQBFAmBZAAQBJAAAsgfQAsgfAAg0QAAgpgrgaQgmgWhxggQivgxhMg4QiDhfAAixQAAicB0hiQBAg3BWgZQBEgTBQAAQBPAABGATIARAFQASAGASAHQA5AXA0AmQA4AqAfAqIALAPIi9C8IgSgTQgQgPgUgPIgMgIQhQg6hKAAQg4AAgmAaQgjAZAAAgIABAKIhIAZQgEgQAAgTQAAg+A3guQA7gzBbAAQBOAABBAgIAWALQATAMARAOIASAQIBShSQgpgrg5geIgCgBQgSgJgSgHQhNgehYAAQiVAAheBNQhfBNAAB7QAACNBqBJQA+AsCeAtQB+AkAuAcIAFADQAVAOAPAQQAkAoAAA5QAAA+gkAuQgNAPgRAOIgGAFQhBAwhkAAQhwAAhYgyg");
    var mask_2_graphics_46 = new cjs.Graphics().p("AjEHXQg2gfgjgrIA+guQAaAgArAYQBFAmBZAAQBJAAAsgfQAsgfAAg0QAAgpgrgaQgmgWhxggQivgxhMg4QiDhfAAixQAAicB0hiQBAg3BWgZQBEgTBQAAQBPAABGATIARAFQASAGASAHQA5AXA0AmQA4AqAfAqIALAPIi9C8IgSgTQgQgPgUgPIgMgIQhQg6hKAAQg4AAgmAaQgjAZAAAgQAAAnAnAZIg1A4Qg9gtAAhLQAAg+A3guQA7gzBbAAQBOAABBAgIAWALQATAMARAOIASAQIBShSQgpgrg5geIgCgBQgSgJgSgHQhNgehYAAQiVAAheBNQhfBNAAB7QAACNBqBJQA+AsCeAtQB+AkAuAcIAFADQAVAOAPAQQAkAoAAA5QAAA+gkAuQgNAPgRAOIgGAFQhBAwhkAAQhwAAhYgyg");
    var mask_2_graphics_47 = new cjs.Graphics().p("AjEHXQg2gfgjgrIA+guQAaAgArAYQBFAmBZAAQBJAAAsgfQAsgfAAg0QAAgpgrgaQgmgWhxggQivgxhMg4QiDhfAAixQAAicB0hiQBAg3BWgZQBEgTBQAAQBPAABGATIARAFQASAGASAHQA5AXA0AmQA4AqAfAqIALAPIi9C8IgSgTQgQgPgUgPIgMgIQhQg6hKAAQg4AAgmAaQgjAZAAAgQAAApArAZQAYAOA1ASIgqBCQgzgSgZgPQhNgvAAhUQAAg+A3guQA7gzBbAAQBOAABBAgIAWALQATAMARAOIASAQIBShSQgpgrg5geIgCgBQgSgJgSgHQhNgehYAAQiVAAheBNQhfBNAAB7QAACNBqBJQA+AsCeAtQB+AkAuAcIAFADQAVAOAPAQQAkAoAAA5QAAA+gkAuQgNAPgRAOIgGAFQhBAwhkAAQhwAAhYgyg");
    var mask_2_graphics_48 = new cjs.Graphics().p("AjEHXQg2gfgjgrIA+guQAaAgArAYQBFAmBZAAQBJAAAsgfQAsgfAAg0QAAgpgrgaQgmgWhxggQivgxhMg4QiDhfAAixQAAicB0hiQBAg3BWgZQBEgTBQAAQBPAABGATIARAFQASAGASAHQA5AXA0AmQA4AqAfAqIALAPIi9C8IgSgTQgQgPgUgPIgMgIQhQg6hKAAQg4AAgmAaQgjAZAAAgQAAApArAZQAmAXBxAgIANADIg2A/QhtgegqgaQhNgvAAhUQAAg+A3guQA7gzBbAAQBOAABBAgIAWALQATAMARAOIASAQIBShSQgpgrg5geIgCgBQgSgJgSgHQhNgehYAAQiVAAheBNQhfBNAAB7QAACNBqBJQA+AsCeAtQB+AkAuAcIAFADQAVAOAPAQQAkAoAAA5QAAA+gkAuQgNAPgRAOIgGAFQhBAwhkAAQhwAAhYgyg");
    var mask_2_graphics_49 = new cjs.Graphics().p("AjEHXQg2gfgjgrIA+guQAaAgArAYQBFAmBZAAQBJAAAsgfQAsgfAAg0QAAgpgrgaQgmgWhxggQivgxhMg4QiDhfAAixQAAicB0hiQBAg3BWgZQBEgTBQAAQBPAABGATIARAFQASAGASAHQA5AXA0AmQA4AqAfAqIALAPIi9C8IgSgTQgQgPgUgPIgMgIQhQg6hKAAQg4AAgmAaQgjAZAAAgQAAApArAZQAmAXBxAgQA3APAuAQIAFADIg5A7IhFgUQh+gjgugcQhNgvAAhUQAAg+A3guQA7gzBbAAQBOAABBAgIAWALQATAMARAOIASAQIBShSQgpgrg5geIgCgBQgSgJgSgHQhNgehYAAQiVAAheBNQhfBNAAB7QAACNBqBJQA+AsCeAtQB+AkAuAcIAFADQAVAOAPAQQAkAoAAA5QAAA+gkAuQgNAPgRAOIgGAFQhBAwhkAAQhwAAhYgyg");
    var mask_2_graphics_50 = new cjs.Graphics().p("AjEHXQg2gfgjgrIA+guQAaAgArAYQBFAmBZAAQBJAAAsgfQAsgfAAg0QAAgpgrgaQgmgWhxggQivgxhMg4QiDhfAAixQAAicB0hiQBAg3BWgZQBEgTBQAAQBPAABGATIARAFQASAGASAHQA5AXA0AmQA4AqAfAqIALAPIi9C8IgSgTQgQgPgUgPIgMgIQhQg6hKAAQg4AAgmAaQgjAZAAAgQAAApArAZQAmAXBxAgQA3APAuAQIAkAOQAcAKAXALIhEA1IgTgHQgzgUhGgTQh+gjgugcQhNgvAAhUQAAg+A3guQA7gzBbAAQBOAABBAgIAWALQATAMARAOIASAQIBShSQgpgrg5geIgCgBQgSgJgSgHQhNgehYAAQiVAAheBNQhfBNAAB7QAACNBqBJQA+AsCeAtQB+AkAuAcIAFADQAVAOAPAQQAkAoAAA5QAAA+gkAuQgNAPgRAOIgGAFQhBAwhkAAQhwAAhYgyg");
    var mask_2_graphics_51 = new cjs.Graphics().p("AjEHXQg2gfgjgrIA+guQAaAgArAYQBFAmBZAAQBJAAAsgfQAsgfAAg0QAAgpgrgaQgmgWhxggQivgxhMg4QiDhfAAixQAAicB0hiQBAg3BWgZQBEgTBQAAQBPAABGATIARAFQASAGASAHQA5AXA0AmQA4AqAfAqIALAPIi9C8IgSgTQgQgPgUgPIgMgIQhQg6hKAAQg4AAgmAaQgjAZAAAgQAAApArAZQAmAXBxAgQA3APAuAQIAkAOQBJAcAqAeIASAPIhTAjQgVgNgdgOIgkgOQgzgUhGgTQh+gjgugcQhNgvAAhUQAAg+A3guQA7gzBbAAQBOAABBAgIAWALQATAMARAOIASAQIBShSQgpgrg5geIgCgBQgSgJgSgHQhNgehYAAQiVAAheBNQhfBNAAB7QAACNBqBJQA+AsCeAtQB+AkAuAcIAFADQAVAOAPAQQAkAoAAA5QAAA+gkAuQgNAPgRAOIgGAFQhBAwhkAAQhwAAhYgyg");
    var mask_2_graphics_52 = new cjs.Graphics().p("AjEHXQg2gfgjgrIA+guQAaAgArAYQBFAmBZAAQBJAAAsgfQAsgfAAg0QAAgpgrgaQgmgWhxggQivgxhMg4QiDhfAAixQAAicB0hiQBAg3BWgZQBEgTBQAAQBPAABGATIARAFQASAGASAHQA5AXA0AmQA4AqAfAqIALAPIi9C8IgSgTQgQgPgUgPIgMgIQhQg6hKAAQg4AAgmAaQgjAZAAAgQAAApArAZQAmAXBxAgQA3APAuAQIAkAOQBJAcAqAeQArAgAdApIhZAPQgPgQgUgOQgYgRgngSIgkgOQgzgUhGgTQh+gjgugcQhNgvAAhUQAAg+A3guQA7gzBbAAQBOAABBAgIAWALQATAMARAOIASAQIBShSQgpgrg5geIgCgBQgSgJgSgHQhNgehYAAQiVAAheBNQhfBNAAB7QAACNBqBJQA+AsCeAtQB+AkAuAcIAFADQAVAOAPAQQAkAoAAA5QAAA+gkAuQgNAPgRAOIgGAFQhBAwhkAAQhwAAhYgyg");
    var mask_2_graphics_53 = new cjs.Graphics().p("AjOHXQg1gfgkgrIA+guQAaAgArAYQBFAmBaAAQBIAAAsgfQAsgfAAg0QAAgpgqgaQgngWhwggQiwgxhMg4QiChfgBixQAAicB0hiQBBg3BVgZQBEgTBRAAQBOAABGATIARAFQASAGASAHQA6AXAzAmQA4AqAgAqIAKAPIi8C8IgTgTQgQgPgUgPIgMgIQhQg6hJAAQg4AAgmAaQgkAZABAgQgBApAsAZQAlAXByAgQA3APAtAQIAkAOQBJAcAqAeQAyAlAgAxQASAdAMAiIhUAAQgbg2g0glQgZgRgngSIgkgOQgzgUhGgTQh9gjgvgcQhMgvAAhUQAAg+A2guQA8gzBbAAQBNAABBAgIAWALQATAMARAOIASAQIBThSQgpgrg6geIgCgBQgSgJgSgHQhMgehYAAQiWAAheBNQhfBNAAB7QAACNBqBJQA+AsCeAtQB/AkAtAcIAFADQAVAOAPAQQAkAoAAA5QAAA+gkAuQgNAPgRAOIgGAFQhBAwhjAAQhwAAhZgyg");
    var mask_2_graphics_54 = new cjs.Graphics().p("AjWHXQg2gfgjgrIA+guQAZAgArAYQBGAmBaAAQBIAAAsgfQAsgfAAg0QAAgpgrgaQgmgWhxggQivgxhNg4QiChfAAixQAAicB0hiQBAg3BVgZQBFgTBRAAQBOAABGATIAQAFQATAGARAHQA6AXAzAmQA5AqAfAqIAKAPIi8C8IgTgTQgPgPgVgPIgLgIQhQg6hJAAQg5AAgmAaQgjAZAAAgQAAApArAZQAlAXByAgQA3APAtAQIAkAOQBKAcApAeQAzAlAfAxQAmA8AJBPIhQgNQgThghRg6QgYgRgogSIgkgOQgygUhGgTQh+gjgugcQhNgvAAhUQAAg+A2guQA8gzBcAAQBMAABCAgIAVALQAUAMAQAOIASAQIBThSQgpgrg6geIgCgBQgRgJgTgHQhMgehXAAQiWAAheBNQhfBNgBB7QAACNBqBJQA/AsCdAtQB/AkAuAcIAEADQAWAOAOAQQAlAogBA5QABA+glAuQgMAPgRAOIgHAFQhBAwhiAAQhxAAhYgyg");
    var mask_2_graphics_55 = new cjs.Graphics().p("AjYHXQg1gfgjgrIA9guQAaAgArAYQBGAmBZAAQBJAAAsgfQArgfAAg0QAAgpgqgaQgmgWhxggQiwgxhMg4QiChfAAixQAAicB0hiQBAg3BVgZQBEgTBSAAQBNAABGATIARAFQATAGARAHQA6AXAzAmQA5AqAfAqIAKAPIi8C8IgTgTQgQgPgUgPIgLgIQhRg6hIAAQg5AAgmAaQgjAZAAAgQAAApArAZQAlAXByAgQA3APAtAQIAkAOQBJAcAqAeQAzAlAfAxQAxBOAABtQAAAUgBAUIhMgUIABgTQAAiNhqhLQgZgRgngSIgkgOQgzgUhFgTQh+gjgvgcQhMgvAAhUQAAg+A2guQA8gzBcAAQBMAABCAgIAVALQAUAMAQAOIASAQIBThSQgpgrg6geIgCgBQgRgJgTgHQhMgehXAAQiWAAhfBNQhfBNAAB7QAACNBqBJQA+AsCeAtQB/AkAuAcIAEADQAVAOAPAQQAkAoAAA5QAAA+gkAuQgMAPgSAOIgGAFQhBAwhjAAQhwAAhZgyg");
    var mask_2_graphics_56 = new cjs.Graphics().p("AjYHXQg1gfgjgrIA9guQAaAgArAYQBGAmBZAAQBJAAAsgfQArgfAAg0QAAgpgqgaQgmgWhxggQiwgxhMg4QiChfAAixQAAicB0hiQBAg3BVgZQBEgTBSAAQBNAABGATIARAFQATAGARAHQA6AXAzAmQA5AqAfAqIAKAPIi8C8IgTgTQgQgPgUgPIgLgIQhRg6hIAAQg5AAgmAaQgjAZAAAgQAAApArAZQAlAXByAgQA3APAtAQIAkAOQBJAcAqAeQAzAlAfAxQAxBOAABtQAABJgVA+IhDgrQAMgqAAgxQAAiNhqhLQgZgRgngSIgkgOQgzgUhFgTQh+gjgvgcQhMgvAAhUQAAg+A2guQA8gzBcAAQBMAABCAgIAVALQAUAMAQAOIASAQIBThSQgpgrg6geIgCgBQgRgJgTgHQhMgehXAAQiWAAhfBNQhfBNAAB7QAACNBqBJQA+AsCeAtQB/AkAuAcIAEADQAVAOAPAQQAkAoAAA5QAAA+gkAuQgMAPgSAOIgGAFQhBAwhjAAQhwAAhZgyg");
    var mask_2_graphics_57 = new cjs.Graphics().p("AE5HcQANgVAJgYQAUg0AAg/QAAiNhqhLQgZgRgngRIgkgPQgzgThFgUQh+gjgvgcQhMgvAAhTQAAg/A2guQA8gzBcAAQBMABBCAfIAVAMQAUALAQAOIASARIBThTQgpgqg6gfIgCgBQgRgJgTgHQhMgehXAAQiWAAhfBOQhfBMAAB7QAACNBqBKQA+AsCeAsQB/AkAuAdIAEACQAVAOAPAQQAkApAAA4QAAA/gkAtQgMAQgSANIgGAFQhBAwhjAAQhwAAhZgyQg1gfgjgqIA9gvQAaAhArAXQBGAnBZAAQBJAAAsggQArgfAAg0QAAgpgqgZQgmgXhxggQiwgxhMg4QiCheAAixQAAidB0hiQBAg3BVgYQBEgUBSAAQBNAABGAUIARAEQATAGARAIQA6AXAzAlQA5AqAfAqIAKAPIi8C8IgTgTQgQgPgUgPIgLgIQhRg6hIAAQg5AAgmAaQgjAaAAAfQAAApArAZQAlAXByAgQA3AQAtAQIAkANQBJAcAqAfQAzAkAfAxQAxBOAABtQAACChCBbg");
    var mask_2_graphics_58 = new cjs.Graphics().p("AELHzQAugrAWg6QAUgzAAg/QAAiNhqhLQgZgRgngSIgkgOQgzgThFgTQh+gkgvgcQhMgvAAhUQAAg+A2guQA8gzBcAAQBMAABCAgIAVALQAUAMAQAOIASAQIBThSQgpgrg6geIgCgBQgRgJgTgHQhMgehXAAQiWAAhfBNQhfBNAAB7QAACNBqBJQA+AsCeAtQB/AkAuAcIAEADQAVAOAPAQQAkAoAAA5QAAA+gkAuQgMAPgSAOIgGAFQhBAwhjAAQhwAAhZgyQg1gfgjgrIA9guQAaAgArAYQBGAmBZAAQBJAAAsgfQArgfAAg0QAAgpgqgaQgmgWhxggQiwgxhMg4QiCheAAiyQAAicB0hiQBAg3BVgZQBEgTBSAAQBNAABGATIARAFQATAGARAHQA6AXAzAmQA5AqAfAqIAKAPIi8C8IgTgTQgQgPgUgPIgLgIQhRg6hIAAQg5AAgmAaQgjAZAAAgQAAApArAZQAlAXByAgQA3APAtAQIAkAOQBJAdAqAdQAzAlAfAxQAxBOAABtQAACvh5BpIgGAFg");
    var mask_2_graphics_59 = new cjs.Graphics().p("ADUIEQAXgNAUgRQA4guAYhBQAUgzAAg/QAAiNhqhLQgZgRgngSIgkgNQgzgUhFgTQh+gkgvgcQhMgvAAhUQAAg+A2guQA8gzBcAAQBMAABCAgIAVALQAUAMAQAOIASAQIBThSQgpgrg6geIgCgBQgRgJgTgHQhMgehXAAQiWAAhfBNQhfBNAAB7QAACNBqBKQA+ArCeAtQB/AkAuAcIAEADQAVAOAPAQQAkAoAAA5QAAA+gkAuQgMAPgSAOIgGAFQhBAwhjAAQhwAAhZgyQg1gfgjgrIA9guQAaAgArAYQBGAmBZAAQBJAAAsgfQArgfAAg0QAAgpgqgaQgmgWhxggQiwgxhMg4QiCheAAiyQAAicB0hiQBAg3BVgZQBEgTBSAAQBNAABGATIARAFQATAGARAHQA6AXAzAmQA5AqAfAqIAKAPIi8C8IgTgTQgQgPgUgPIgLgIQhRg6hIAAQg5AAgmAaQgjAZAAAgQAAApArAZQAlAXByAgQA3APAtAQIAkAOQBJAdAqAeQAzAkAfAxQAxBOAABtQAACvh5BpQgjAegpAVg");
    var mask_2_graphics_60 = new cjs.Graphics().p("ACRITIAEgBQA9gVAtgmQA4guAYhAQAUg0AAg/QAAiNhqhLQgZgRgngQIgkgPQgzgThFgUQh+gkgvgcQhMgvAAhTQAAg/A2guQA8gzBcAAQBMABBCAfIAVAMQAUALAQAOIASARIBThTQgpgqg6gfIgCgBQgRgJgTgHQhMgehXAAQiWAAhfBOQhfBMAAB7QAACNBqBLQA+ArCeAsQB/AkAuAdIAEACQAVAOAPAQQAkApAAA4QAAA/gkAtQgMAQgSANIgGAFQhBAwhjAAQhwAAhZgyQg1gfgjgqIA9gvQAaAhArAXQBGAnBZAAQBJAAAsggQArgfAAg0QAAgpgqgZQgmgXhxggQiwgxhMg4QiCheAAixQAAidB0hiQBAg3BVgYQBEgUBSAAQBNAABGAUIARAEQATAGARAIQA6AXAzAlQA5AqAfAqIAKAPIi8C8IgTgTQgQgPgUgPIgLgIQhRg6hIAAQg5AAgmAaQgjAaAAAfQAAApArAZQAlAXByAgQA3AQAtAQIAkANQBJAdAqAfQAzAjAfAxQAxBOAABtQAACvh5BpQhAA3hWAag");
    var mask_2_graphics_61 = new cjs.Graphics().p("ABSIZQAkgGAfgKQA9gVAtgmQA4guAYhBQAUgzAAg/QAAiNhqhLQgZgRgngQIgkgPQgzgUhFgTQh+gkgvgcQhMgvAAhUQAAg+A2guQA8gzBcAAQBMAABCAgIAVALQAUAMAQAOIASAQIBThSQgpgrg6geIgCgBQgRgJgTgHQhMgehXAAQiWAAhfBNQhfBNAAB7QAACNBqBKQA+AsCeAsQB/AkAuAcIAEADQAVAOAPAQQAkAoAAA5QAAA+gkAuQgMAQgSANIgGAFQhBAwhjAAQhwAAhZgyQg1gfgjgrIA9guQAaAgArAYQBGAmBZAAQBJAAAsgfQArgfAAg0QAAgpgqgaQgmgWhxggQiwgxhMg4QiCheAAiyQAAicB0hiQBAg3BVgZQBEgTBSAAQBNAABGATIARAFQATAGARAHQA6AYAzAlQA5AqAfAqIAKAPIi8C8IgTgTQgQgPgUgPIgLgIQhRg6hIAAQg5AAgmAaQgjAZAAAgQAAApArAZQAlAXByAgQA3AQAtAQIAkANQBJAdAqAeQAzAkAfAxQAxBOAABtQAACvh5BpQhEA6hdAaQgjAKgoAFg");
    var mask_2_graphics_62 = new cjs.Graphics().p("AAcIdQBDgEA2gTQA9gUAtgmQA4guAYhBQAUg0AAg/QAAiNhqhKQgZgRgngRIgkgPQgzgThFgUQh+gjgvgdQhMgvAAhTQAAg/A2guQA8gyBcAAQBMAABCAfIAVAMQAUALAQAOIASARIBThTQgpgqg6geIgCgBQgRgJgTgHQhMgfhXAAQiWAAhfBOQhfBNAAB6QAACNBqBLQA+AsCeArQB/AkAuAdIAEADQAVANAPARQAkAoAAA5QAAA+gkAtQgMAQgSAOIgGAEQhBAxhjAAQhwAAhZgzQg1gfgjgqIA9guQAaAgArAXQBGAnBZAAQBJAAAsgfQArggAAg0QAAgpgqgZQgmgXhxggQiwgxhMg3QiCheAAiyQAAicB0hjQBAg3BVgYQBEgUBSAAQBNAABGAUIARAFQATAGARAHQA6AXAzAmQA5AqAfApIAKAPIi8C9IgTgTQgQgPgUgPIgLgJQhRg6hIAAQg5AAgmAbQgjAZAAAfQAAAqArAZQAlAWByAgQA3AQAtAQIAkAOQBJAdAqAeQAzAlAfAwQAxBOAABtQAACvh5BpQhEA6hdAaQg8AQhHAEg");
    var mask_2_graphics_63 = new cjs.Graphics().p("Ag8JpIAYhLIAVAAQBcAABIgYQA9gVAtgmQA4guAYhAQAUg0AAg/QAAiNhqhLQgZgRgngQIgkgPQgzgThFgUQh+gkgvgcQhMgvAAhTQAAg/A2guQA8gzBcAAQBMABBCAfIAVAMQAUALAQAOIASARIBThTQgpgqg6gfIgCgBQgRgJgTgHQhMgehXAAQiWAAhfBOQhfBMAAB7QAACNBqBLQA+AsCeArQB/AkAuAdIAEACQAVAOAPAQQAkApAAA4QAAA/gkAtQgMAQgSANIgGAFQhBAwhjAAQhwAAhZgyQg1gfgjgqIA9gvQAaAhArAXQBGAnBZAAQBJAAAsggQArgfAAg0QAAgpgqgZQgmgXhxggQiwgxhMg4QiCheAAixQAAidB0hiQBAg3BVgYQBEgUBSAAQBNAABGAUIARAEQATAGARAIQA6AXAzAlQA5AqAfAqIAKAPIi8C8IgTgTQgQgPgUgPIgLgIQhRg6hIAAQg5AAgmAaQgjAaAAAfQAAApArAZQAlAXByAgQA3AQAtAQIAkANQBJAdAqAfQAzAkAfAwQAxBOAABtQAACvh5BpQhEA6hdAaQhKAVhaAAQgXAAgWgBg");
    var mask_2_graphics_64 = new cjs.Graphics().p("AiLJeIAbhIQAvAIAyAAQBcAABIgYQA9gVAtgmQA4guAYhAQAUg0AAg/QAAiNhqhLQgZgRgngQIgkgPQgzgThFgUQh+gkgvgcQhMgvAAhTQAAg/A2guQA8gzBcAAQBMABBCAfIAVAMQAUALAQAOIASARIBThTQgpgqg6gfIgCgBQgRgJgTgHQhMgehXAAQiWAAhfBOQhfBMAAB7QAACNBqBLQA+AsCeArQB/AkAuAdIAEACQAVAOAPAQQAkApAAA4QAAA/gkAtQgMAQgSANIgGAFQhBAwhjAAQhwAAhZgyQg1gfgjgqIA9gvQAaAhArAXQBGAnBZAAQBJAAAsggQArgfAAg0QAAgpgqgZQgmgXhxggQiwgxhMg4QiCheAAixQAAidB0hiQBAg3BVgYQBEgUBSAAQBNAABGAUIARAEQATAGARAIQA6AXAzAlQA5AqAfAqIAKAPIi8C8IgTgTQgQgPgUgPIgLgIQhRg6hIAAQg5AAgmAaQgjAaAAAfQAAApArAZQAlAXByAgQA3AQAtAQIAkANQBJAdAqAfQAzAkAfAwQAxBOAABtQAACvh5BpQhEA6hdAaQhKAVhaAAQhBAAg7gMg");
    var mask_2_graphics_65 = new cjs.Graphics().p("AjVJKIAehFQBOAZBaAAQBcAABIgYQA9gVAtgmQA4guAYhAQAUg0AAg/QAAiNhqhLQgZgRgngQIgkgPQgzgThFgUQh+gkgvgcQhMgvAAhTQAAg/A2guQA8gzBcAAQBMABBCAfIAVAMQAUALAQAOIASARIBThTQgpgqg6gfIgCgBQgRgJgTgHQhMgehXAAQiWAAhfBOQhfBMAAB7QAACNBqBLQA+AsCeArQB/AkAuAdIAEACQAVAOAPAQQAkApAAA4QAAA/gkAtQgMAQgSANIgGAFQhBAwhjAAQhwAAhZgyQg1gfgjgqIA9gvQAaAhArAXQBGAnBZAAQBJAAAsggQArgfAAg0QAAgpgqgZQgmgXhxggQiwgxhMg4QiCheAAixQAAidB0hiQBAg3BVgYQBEgUBSAAQBNAABGAUIARAEQATAGARAIQA6AXAzAlQA5AqAfAqIAKAPIi8C8IgTgTQgQgPgUgPIgLgIQhRg6hIAAQg5AAgmAaQgjAaAAAfQAAApArAZQAlAXByAgQA3AQAtAQIAkANQBJAdAqAfQAzAkAfAwQAxBOAABtQAACvh5BpQhEA6hdAaQhKAVhaAAQhqAAhcggg");
    var mask_2_graphics_66 = new cjs.Graphics().p("AkcIsIAlhCQBoA0CAAAQBcAABIgYQA9gVAtgmQA4guAYhAQAUg0AAg/QAAiNhqhLQgZgRgngQIgkgPQgzgThFgUQh+gkgvgcQhMgvAAhTQAAg/A2guQA8gzBcAAQBMABBCAfIAVAMQAUALAQAOIASARIBThTQgpgqg6gfIgCgBQgRgJgTgHQhMgehXAAQiWAAhfBOQhfBMAAB7QAACNBqBLQA+AsCeArQB/AkAuAdIAEACQAVAOAPAQQAkApAAA4QAAA/gkAtQgMAQgSANIgGAFQhBAwhjAAQhwAAhZgyQg1gfgjgqIA9gvQAaAhArAXQBGAnBZAAQBJAAAsggQArgfAAg0QAAgpgqgZQgmgXhxggQiwgxhMg4QiCheAAixQAAidB0hiQBAg3BVgYQBEgUBSAAQBNAABGAUIARAEQATAGARAIQA6AXAzAlQA5AqAfAqIAKAPIi8C8IgTgTQgQgPgUgPIgLgIQhRg6hIAAQg5AAgmAaQgjAaAAAfQAAApArAZQAlAXByAgQA3AQAtAQIAkANQBJAdAqAfQAzAkAfAwQAxBOAABtQAACvh5BpQhEA6hdAaQhKAVhaAAQiUAAh5g+g");
    var mask_2_graphics_67 = new cjs.Graphics().p("AlTILIgFgDIgDgCIAtg8IAHAFQB6BPCeAAQBcAABIgYQA9gVAtgmQA4guAYhAQAUg0AAg/QAAiNhqhLQgZgRgngQIgkgPQgzgThFgUQh+gkgvgcQhMgvAAhTQAAg/A2guQA8gzBcAAQBMABBCAfIAVAMQAUALAQAOIASARIBThTQgpgqg6gfIgCgBQgRgJgTgHQhMgehXAAQiWAAhfBOQhfBMAAB7QAACNBqBLQA+AsCeArQB/AkAuAdIAEACQAVAOAPAQQAkApAAA4QAAA/gkAtQgMAQgSANIgGAFQhBAwhjAAQhwAAhZgyQg1gfgjgqIA9gvQAaAhArAXQBGAnBZAAQBJAAAsggQArgfAAg0QAAgpgqgZQgmgXhxggQiwgxhMg4QiCheAAixQAAidB0hiQBAg3BVgYQBEgUBSAAQBNAABGAUIARAEQATAGARAIQA6AXAzAlQA5AqAfAqIAKAPIi8C8IgTgTQgQgPgUgPIgLgIQhRg6hIAAQg5AAgmAaQgjAaAAAfQAAApArAZQAlAXByAgQA3AQAtAQIAkANQBJAdAqAfQAzAkAfAwQAxBOAABtQAACvh5BpQhEA6hdAaQhKAVhaAAQi2AAiOhfg");
    var mask_2_graphics_68 = new cjs.Graphics().p("AlTILIgFgDQgRgLgQgNIgYgUIA5gzIAAABQAXAUAaARQB6BPCeAAQBcAABIgYQA9gVAtgmQA4guAYhAQAUg0AAg/QAAiNhqhLQgZgRgngQIgkgPQgzgThFgUQh+gkgvgcQhMgvAAhTQAAg/A2guQA8gzBcAAQBMABBCAfIAVAMQAUALAQAOIASARIBThTQgpgqg6gfIgCgBQgRgJgTgHQhMgehXAAQiWAAhfBOQhfBMAAB7QAACNBqBLQA+AsCeArQB/AkAuAdIAEACQAVAOAPAQQAkApAAA4QAAA/gkAtQgMAQgSANIgGAFQhBAwhjAAQhwAAhZgyQg1gfgjgqIA9gvQAaAhArAXQBGAnBZAAQBJAAAsggQArgfAAg0QAAgpgqgZQgmgXhxggQiwgxhMg4QiCheAAixQAAidB0hiQBAg3BVgYQBEgUBSAAQBNAABGAUIARAEQATAGARAIQA6AXAzAlQA5AqAfAqIAKAPIi8C8IgTgTQgQgPgUgPIgLgIQhRg6hIAAQg5AAgmAaQgjAaAAAfQAAApArAZQAlAXByAgQA3AQAtAQIAkANQBJAdAqAfQAzAkAfAwQAxBOAABtQAACvh5BpQhEA6hdAaQhKAVhaAAQi2AAiOhfg");
    var mask_2_graphics_69 = new cjs.Graphics().p("AlGILQgugdgjglQgVgVgJgNIgGgJIA9guIAGAKQAJANAQAQQAfAeAnAaQB5BPCfAAQBcAABHgYQA9gVAtgmQA4guAZhAQATg0AAg/QAAiNhqhLQgYgRgogQIgkgPQgygThGgUQh+gkgugcQhNgvAAhTQAAg/A2guQA8gzBcAAQBMABBCAfIAVAMQAUALAQAOIASARIBThTQgpgqg6gfIgCgBQgRgJgTgHQhMgehXAAQiWAAheBOQhfBMgBB7QAACNBrBLQA+AsCdArQB/AkAuAdIAEACQAWAOAOAQQAlApgBA4QABA/glAtQgMAQgRANIgHAFQhBAwhiAAQhxAAhYgyQg2gfgjgqIA+gvQAZAhArAXQBGAnBaAAQBIAAAsggQAsgfAAg0QAAgpgrgZQgmgXhxggQivgxhNg4QiCheAAixQAAidB0hiQBAg3BVgYQBFgUBRAAQBOAABGAUIAQAEQATAGARAIQA6AXAzAlQA5AqAfAqIAKAPIi8C8IgTgTQgPgPgVgPIgLgIQhQg6hJAAQg5AAgmAaQgjAaAAAfQAAApArAZQAlAXByAgQA3AQAtAQIAkANQBKAdApAfQAzAkAfAwQAxBOABBtQAACvh6BpQhEA6hdAaQhKAVhZAAQi3AAiOhfg");
    var mask_2_graphics_119 = new cjs.Graphics().p("AlGILQgugdgjglQgVgVgJgNIgGgJIA9guIAGAKQAJANAQAQQAfAeAnAaQB5BPCfAAQBcAABHgYQA9gVAtgmQA4guAZhAQATg0AAg/QAAiNhqhLQgYgRgogQIgkgPQgygThGgUQh+gkgugcQhNgvAAhTQAAg/A2guQA8gzBcAAQBMABBCAfIAVAMQAUALAQAOIASARIBThTQgpgqg6gfIgCgBQgRgJgTgHQhMgehXAAQiWAAheBOQhfBMgBB7QAACNBrBLQA+AsCdArQB/AkAuAdIAEACQAWAOAOAQQAlApgBA4QABA/glAtQgMAQgRANIgHAFQhBAwhiAAQhxAAhYgyQg2gfgjgqIA+gvQAZAhArAXQBGAnBaAAQBIAAAsggQAsgfAAg0QAAgpgrgZQgmgXhxggQivgxhNg4QiCheAAixQAAidB0hiQBAg3BVgYQBFgUBRAAQBOAABGAUIAQAEQATAGARAIQA6AXAzAlQA5AqAfAqIAKAPIi8C8IgTgTQgPgPgVgPIgLgIQhQg6hJAAQg5AAgmAaQgjAaAAAfQAAApArAZQAlAXByAgQA3AQAtAQIAkANQBKAdApAfQAzAkAfAwQAxBOABBtQAACvh6BpQhEA6hdAaQhKAVhZAAQi3AAiOhfg");
    var mask_2_graphics_120 = new cjs.Graphics().p("AlGILQgugdgjglQgVgVgJgNIgGgJIA9guIAGAKQAJANAQAQQAfAeAnAaQB5BPCfAAQBcAABHgYQA9gVAtgmQA4guAZhAQATg0AAg/QAAiNhqhLQgYgRgogQIgkgPQgygThGgUQh+gkgugcQhNgvAAhTQAAg/A2guQA8gzBcAAQBMABBCAfIAVAMQAUALAQAOIASARIBThTQgpgqg6gfIgCgBQgRgJgTgHQhMgehXAAQiWAAheBOQhfBMgBB7QAACNBrBLQA+AsCdArQB/AkAuAdIAEACQAWAOAOAQQAlApgBA4QABA/glAtQgMAQgRANIgHAFQhBAwhiAAQhxAAhYgyQg2gfgjgqIA+gvQAZAhArAXQBGAnBaAAQBIAAAsggQAsgfAAg0QAAgpgrgZQgmgXhxggQivgxhNg4QiCheAAixQAAidB0hiQBAg3BVgYQBFgUBRAAQBOAABGAUIAQAEQATAGARAIQA6AXAzAlQA5AqAfAqIAKAPIi8C8IgTgTQgPgPgVgPIgLgIQhQg6hJAAQg5AAgmAaQgjAaAAAfQAAApArAZQAlAXByAgQA3AQAtAQIAkANQBKAdApAfQAzAkAfAwQAxBOABBtQAACvh6BpQhEA6hdAaQhKAVhZAAQi3AAiOhfg");
    var mask_2_graphics_121 = new cjs.Graphics().p("AlGILQgugdgjglQgVgVgJgNIgGgJIA9guIAGAKQAJANAQAQQAfAeAnAaQB5BPCfAAQBcAABHgYQA9gVAtgmQA4guAZhAQATg0AAg/QAAiNhqhLQgYgRgogQIgkgPQgygThGgUQh+gkgugcQhNgvAAhTQAAg/A2guQA8gzBcAAQBMABBCAfIAVAMQAUALAQAOIASARIBThTQgpgqg6gfIgCgBQgRgJgTgHQhMgehXAAQiWAAheBOQhfBMgBB7QAACNBrBLQA+AsCdArQB/AkAuAdIAEACQAWAOAOAQQAlApgBA4QABA/glAtQgMAQgRANIgHAFQhBAwhiAAQhxAAhYgyQg2gfgjgqIA+gvQAZAhArAXQBGAnBaAAQBIAAAsggQAsgfAAg0QAAgpgrgZQgmgXhxggQivgxhNg4QiCheAAixQAAidB0hiQBAg3BVgYQBFgUBRAAQBOAABGAUIAQAEQATAGARAIQA6AXAzAlQA5AqAfAqIAKAPIi8C8IgTgTQgPgPgVgPIgLgIQhQg6hJAAQg5AAgmAaQgjAaAAAfQAAApArAZQAlAXByAgQA3AQAtAQIAkANQBKAdApAfQAzAkAfAwQAxBOABBtQAACvh6BpQhEA6hdAaQhKAVhZAAQi3AAiOhfg");
    var mask_2_graphics_122 = new cjs.Graphics().p("AlGILQgugdgjglQgVgVgJgNIgGgJIA9guIAGAKQAJANAQAQQAfAeAnAaQB5BPCfAAQBcAABHgYQA9gVAtgmQA4guAZhAQATg0AAg/QAAiNhqhLQgYgRgogQIgkgPQgygThGgUQh+gkgugcQhNgvAAhTQAAg/A2guQA8gzBcAAQBMABBCAfIAVAMQAUALAQAOIASARIBThTQgpgqg6gfIgCgBQgRgJgTgHQhMgehXAAQiWAAheBOQhfBMgBB7QAACNBrBLQA+AsCdArQB/AkAuAdIAEACQAWAOAOAQQAlApgBA4QABA/glAtQgMAQgRANIgHAFQhBAwhiAAQhxAAhYgyQg2gfgjgqIA+gvQAZAhArAXQBGAnBaAAQBIAAAsggQAsgfAAg0QAAgpgrgZQgmgXhxggQivgxhNg4QiCheAAixQAAidB0hiQBAg3BVgYQBFgUBRAAQBOAABGAUIAQAEQATAGARAIQA6AXAzAlQA5AqAfAqIAKAPIi8C8IgTgTQgPgPgVgPIgLgIQhQg6hJAAQg5AAgmAaQgjAaAAAfQAAApArAZQAlAXByAgQA3AQAtAQIAkANQBKAdApAfQAzAkAfAwQAxBOABBtQAACvh6BpQhEA6hdAaQhKAVhZAAQi3AAiOhfg");
    var mask_2_graphics_123 = new cjs.Graphics().p("AlGILQgugdgjglQgVgVgJgNIgGgJIA9guIAGAKQAJANAQAQQAfAeAnAaQB5BPCfAAQBcAABHgYQA9gVAtgmQA4guAZhAQATg0AAg/QAAiNhqhLQgYgRgogQIgkgPQgygThGgUQh+gkgugcQhNgvAAhTQAAg/A2guQA8gzBcAAQBMABBCAfIAVAMQAUALAQAOIASARIBThTQgpgqg6gfIgCgBQgRgJgTgHQhMgehXAAQiWAAheBOQhfBMgBB7QAACNBrBLQA+AsCdArQB/AkAuAdIAEACQAWAOAOAQQAlApgBA4QABA/glAtQgMAQgRANIgHAFQhBAwhiAAQhxAAhYgyQg2gfgjgqIA+gvQAZAhArAXQBGAnBaAAQBIAAAsggQAsgfAAg0QAAgpgrgZQgmgXhxggQivgxhNg4QiCheAAixQAAidB0hiQBAg3BVgYQBFgUBRAAQBOAABGAUIAQAEQATAGARAIQA6AXAzAlQA5AqAfAqIAKAPIi8C8IgTgTQgPgPgVgPIgLgIQhQg6hJAAQg5AAgmAaQgjAaAAAfQAAApArAZQAlAXByAgQA3AQAtAQIAkANQBKAdApAfQAzAkAfAwQAxBOABBtQAACvh6BpQhEA6hdAaQhKAVhZAAQi3AAiOhfg");
    var mask_2_graphics_124 = new cjs.Graphics().p("AlGILQgugdgjglQgVgVgJgNIgGgJIA9guIAGAKQAJANAQAQQAfAeAnAaQB5BPCfAAQBcAABHgYQA9gVAtgmQA4guAZhAQATg0AAg/QAAiNhqhLQgYgRgogQIgkgPQgygThGgUQh+gkgugcQhNgvAAhTQAAg/A2guQA8gzBcAAQBMABBCAfIAVAMQAUALAQAOIASARIBThTQgpgqg6gfIgCgBQgRgJgTgHQhMgehXAAQiWAAheBOQhfBMgBB7QAACNBrBLQA+AsCdArQB/AkAuAdIAEACQAWAOAOAQQAlApgBA4QABA/glAtQgMAQgRANIgHAFQhBAwhiAAQhxAAhYgyQg2gfgjgqIA+gvQAZAhArAXQBGAnBaAAQBIAAAsggQAsgfAAg0QAAgpgrgZQgmgXhxggQivgxhNg4QiCheAAixQAAidB0hiQBAg3BVgYQBFgUBRAAQBOAABGAUIAQAEQATAGARAIQA6AXAzAlQA5AqAfAqIAKAPIi8C8IgTgTQgPgPgVgPIgLgIQhQg6hJAAQg5AAgmAaQgjAaAAAfQAAApArAZQAlAXByAgQA3AQAtAQIAkANQBKAdApAfQAzAkAfAwQAxBOABBtQAACvh6BpQhEA6hdAaQhKAVhZAAQi3AAiOhfg");
    var mask_2_graphics_125 = new cjs.Graphics().p("AlGILQgugdgjglQgVgVgJgNIgGgJIA9guIAGAKQAJANAQAQQAfAeAnAaQB5BPCfAAQBcAABHgYQA9gVAtgmQA4guAZhAQATg0AAg/QAAiNhqhLQgYgRgogQIgkgPQgygThGgUQh+gkgugcQhNgvAAhTQAAg/A2guQA8gzBcAAQBMABBCAfIAVAMQAUALAQAOIASARIBThTQgpgqg6gfIgCgBQgRgJgTgHQhMgehXAAQiWAAheBOQhfBMgBB7QAACNBrBLQA+AsCdArQB/AkAuAdIAEACQAWAOAOAQQAlApgBA4QABA/glAtQgMAQgRANIgHAFQhBAwhiAAQhxAAhYgyQg2gfgjgqIA+gvQAZAhArAXQBGAnBaAAQBIAAAsggQAsgfAAg0QAAgpgrgZQgmgXhxggQivgxhNg4QiCheAAixQAAidB0hiQBAg3BVgYQBFgUBRAAQBOAABGAUIAQAEQATAGARAIQA6AXAzAlQA5AqAfAqIAKAPIi8C8IgTgTQgPgPgVgPIgLgIQhQg6hJAAQg5AAgmAaQgjAaAAAfQAAApArAZQAlAXByAgQA3AQAtAQIAkANQBKAdApAfQAzAkAfAwQAxBOABBtQAACvh6BpQhEA6hdAaQhKAVhZAAQi3AAiOhfg");
    var mask_2_graphics_126 = new cjs.Graphics().p("AlGILQgugdgjglQgVgVgJgNIgGgJIA9guIAGAKQAJANAQAQQAfAeAnAaQB5BPCfAAQBcAABHgYQA9gVAtgmQA4guAZhAQATg0AAg/QAAiNhqhLQgYgRgogQIgkgPQgygThGgUQh+gkgugcQhNgvAAhTQAAg/A2guQA8gzBcAAQBMABBCAfIAVAMQAUALAQAOIASARIBThTQgpgqg6gfIgCgBQgRgJgTgHQhMgehXAAQiWAAheBOQhfBMgBB7QAACNBrBLQA+AsCdArQB/AkAuAdIAEACQAWAOAOAQQAlApgBA4QABA/glAtQgMAQgRANIgHAFQhBAwhiAAQhxAAhYgyQg2gfgjgqIA+gvQAZAhArAXQBGAnBaAAQBIAAAsggQAsgfAAg0QAAgpgrgZQgmgXhxggQivgxhNg4QiCheAAixQAAidB0hiQBAg3BVgYQBFgUBRAAQBOAABGAUIAQAEQATAGARAIQA6AXAzAlQA5AqAfAqIAKAPIi8C8IgTgTQgPgPgVgPIgLgIQhQg6hJAAQg5AAgmAaQgjAaAAAfQAAApArAZQAlAXByAgQA3AQAtAQIAkANQBKAdApAfQAzAkAfAwQAxBOABBtQAACvh6BpQhEA6hdAaQhKAVhZAAQi3AAiOhfg");
    var mask_2_graphics_127 = new cjs.Graphics().p("AlGILQgugdgjglQgVgVgJgNIgGgJIA9guIAGAKQAJANAQAQQAfAeAnAaQB5BPCfAAQBcAABHgYQA9gVAtgmQA4guAZhAQATg0AAg/QAAiNhqhLQgYgRgogQIgkgPQgygThGgUQh+gkgugcQhNgvAAhTQAAg/A2guQA8gzBcAAQBMABBCAfIAVAMQAUALAQAOIASARIBThTQgpgqg6gfIgCgBQgRgJgTgHQhMgehXAAQiWAAheBOQhfBMgBB7QAACNBrBLQA+AsCdArQB/AkAuAdIAEACQAWAOAOAQQAlApgBA4QABA/glAtQgMAQgRANIgHAFQhBAwhiAAQhxAAhYgyQg2gfgjgqIA+gvQAZAhArAXQBGAnBaAAQBIAAAsggQAsgfAAg0QAAgpgrgZQgmgXhxggQivgxhNg4QiCheAAixQAAidB0hiQBAg3BVgYQBFgUBRAAQBOAABGAUIAQAEQATAGARAIQA6AXAzAlQA5AqAfAqIAKAPIi8C8IgTgTQgPgPgVgPIgLgIQhQg6hJAAQg5AAgmAaQgjAaAAAfQAAApArAZQAlAXByAgQA3AQAtAQIAkANQBKAdApAfQAzAkAfAwQAxBOABBtQAACvh6BpQhEA6hdAaQhKAVhZAAQi3AAiOhfg");
    var mask_2_graphics_128 = new cjs.Graphics().p("AlGILQgugdgjglQgVgVgJgNIgGgJIA9guIAGAKQAJANAQAQQAfAeAnAaQB5BPCfAAQBcAABHgYQA9gVAtgmQA4guAZhAQATg0AAg/QAAiNhqhLQgYgRgogQIgkgPQgygThGgUQh+gkgugcQhNgvAAhTQAAg/A2guQA8gzBcAAQBMABBCAfIAVAMQAUALAQAOIASARIBThTQgpgqg6gfIgCgBQgRgJgTgHQhMgehXAAQiWAAheBOQhfBMgBB7QAACNBrBLQA+AsCdArQB/AkAuAdIAEACQAWAOAOAQQAlApgBA4QABA/glAtQgMAQgRANIgHAFQhBAwhiAAQhxAAhYgyQg2gfgjgqIA+gvQAZAhArAXQBGAnBaAAQBIAAAsggQAsgfAAg0QAAgpgrgZQgmgXhxggQivgxhNg4QiCheAAixQAAidB0hiQBAg3BVgYQBFgUBRAAQBOAABGAUIAQAEQATAGARAIQA6AXAzAlQA5AqAfAqIAKAPIi8C8IgTgTQgPgPgVgPIgLgIQhQg6hJAAQg5AAgmAaQgjAaAAAfQAAApArAZQAlAXByAgQA3AQAtAQIAkANQBKAdApAfQAzAkAfAwQAxBOABBtQAACvh6BpQhEA6hdAaQhKAVhZAAQi3AAiOhfg");
    var mask_2_graphics_129 = new cjs.Graphics().p("AlGILQgugdgjglQgVgVgJgNIgGgJIA9guIAGAKQAJANAQAQQAfAeAnAaQB5BPCfAAQBcAABHgYQA9gVAtgmQA4guAZhAQATg0AAg/QAAiNhqhLQgYgRgogQIgkgPQgygThGgUQh+gkgugcQhNgvAAhTQAAg/A2guQA8gzBcAAQBMABBCAfIAVAMQAUALAQAOIASARIBThTQgpgqg6gfIgCgBQgRgJgTgHQhMgehXAAQiWAAheBOQhfBMgBB7QAACNBrBLQA+AsCdArQB/AkAuAdIAEACQAWAOAOAQQAlApgBA4QABA/glAtQgMAQgRANIgHAFQhBAwhiAAQhxAAhYgyQg2gfgjgqIA+gvQAZAhArAXQBGAnBaAAQBIAAAsggQAsgfAAg0QAAgpgrgZQgmgXhxggQivgxhNg4QiCheAAixQAAidB0hiQBAg3BVgYQBFgUBRAAQBOAABGAUIAQAEQATAGARAIQA6AXAzAlQA5AqAfAqIAKAPIi8C8IgTgTQgPgPgVgPIgLgIQhQg6hJAAQg5AAgmAaQgjAaAAAfQAAApArAZQAlAXByAgQA3AQAtAQIAkANQBKAdApAfQAzAkAfAwQAxBOABBtQAACvh6BpQhEA6hdAaQhKAVhZAAQi3AAiOhfg");
    var mask_2_graphics_130 = new cjs.Graphics().p("AlGILQgugdgjglQgVgVgJgNIgGgJIA9guIAGAKQAJANAQAQQAfAeAnAaQB5BPCfAAQBcAABHgYQA9gVAtgmQA4guAZhAQATg0AAg/QAAiNhqhLQgYgRgogQIgkgPQgygThGgUQh+gkgugcQhNgvAAhTQAAg/A2guQA8gzBcAAQBMABBCAfIAVAMQAUALAQAOIASARIBThTQgpgqg6gfIgCgBQgRgJgTgHQhMgehXAAQiWAAheBOQhfBMgBB7QAACNBrBLQA+AsCdArQB/AkAuAdIAEACQAWAOAOAQQAlApgBA4QABA/glAtQgMAQgRANIgHAFQhBAwhiAAQhxAAhYgyQg2gfgjgqIA+gvQAZAhArAXQBGAnBaAAQBIAAAsggQAsgfAAg0QAAgpgrgZQgmgXhxggQivgxhNg4QiCheAAixQAAidB0hiQBAg3BVgYQBFgUBRAAQBOAABGAUIAQAEQATAGARAIQA6AXAzAlQA5AqAfAqIAKAPIi8C8IgTgTQgPgPgVgPIgLgIQhQg6hJAAQg5AAgmAaQgjAaAAAfQAAApArAZQAlAXByAgQA3AQAtAQIAkANQBKAdApAfQAzAkAfAwQAxBOABBtQAACvh6BpQhEA6hdAaQhKAVhZAAQi3AAiOhfg");
    var mask_2_graphics_131 = new cjs.Graphics().p("AlGILQgugdgjglQgVgVgJgNIgGgJIA9guIAGAKQAJANAQAQQAfAeAnAaQB5BPCfAAQBcAABHgYQA9gVAtgmQA4guAZhAQATg0AAg/QAAiNhqhLQgYgRgogQIgkgPQgygThGgUQh+gkgugcQhNgvAAhTQAAg/A2guQA8gzBcAAQBMABBCAfIAVAMQAUALAQAOIASARIBThTQgpgqg6gfIgCgBQgRgJgTgHQhMgehXAAQiWAAheBOQhfBMgBB7QAACNBrBLQA+AsCdArQB/AkAuAdIAEACQAWAOAOAQQAlApgBA4QABA/glAtQgMAQgRANIgHAFQhBAwhiAAQhxAAhYgyQg2gfgjgqIA+gvQAZAhArAXQBGAnBaAAQBIAAAsggQAsgfAAg0QAAgpgrgZQgmgXhxggQivgxhNg4QiCheAAixQAAidB0hiQBAg3BVgYQBFgUBRAAQBOAABGAUIAQAEQATAGARAIQA6AXAzAlQA5AqAfAqIAKAPIi8C8IgTgTQgPgPgVgPIgLgIQhQg6hJAAQg5AAgmAaQgjAaAAAfQAAApArAZQAlAXByAgQA3AQAtAQIAkANQBKAdApAfQAzAkAfAwQAxBOABBtQAACvh6BpQhEA6hdAaQhKAVhZAAQi3AAiOhfg");
    var mask_2_graphics_132 = new cjs.Graphics().p("AlGILQgugdgjglQgVgVgJgNIgGgJIA9guIAGAKQAJANAQAQQAfAeAnAaQB5BPCfAAQBcAABHgYQA9gVAtgmQA4guAZhAQATg0AAg/QAAiNhqhLQgYgRgogQIgkgPQgygThGgUQh+gkgugcQhNgvAAhTQAAg/A2guQA8gzBcAAQBMABBCAfIAVAMQAUALAQAOIASARIBThTQgpgqg6gfIgCgBQgRgJgTgHQhMgehXAAQiWAAheBOQhfBMgBB7QAACNBrBLQA+AsCdArQB/AkAuAdIAEACQAWAOAOAQQAlApgBA4QABA/glAtQgMAQgRANIgHAFQhBAwhiAAQhxAAhYgyQg2gfgjgqIA+gvQAZAhArAXQBGAnBaAAQBIAAAsggQAsgfAAg0QAAgpgrgZQgmgXhxggQivgxhNg4QiCheAAixQAAidB0hiQBAg3BVgYQBFgUBRAAQBOAABGAUIAQAEQATAGARAIQA6AXAzAlQA5AqAfAqIAKAPIi8C8IgTgTQgPgPgVgPIgLgIQhQg6hJAAQg5AAgmAaQgjAaAAAfQAAApArAZQAlAXByAgQA3AQAtAQIAkANQBKAdApAfQAzAkAfAwQAxBOABBtQAACvh6BpQhEA6hdAaQhKAVhZAAQi3AAiOhfg");
    var mask_2_graphics_133 = new cjs.Graphics().p("AlGILQgugdgjglQgVgVgJgNIgGgJIA9guIAGAKQAJANAQAQQAfAeAnAaQB5BPCfAAQBcAABHgYQA9gVAtgmQA4guAZhAQATg0AAg/QAAiNhqhLQgYgRgogQIgkgPQgygThGgUQh+gkgugcQhNgvAAhTQAAg/A2guQA8gzBcAAQBMABBCAfIAVAMQAUALAQAOIASARIBThTQgpgqg6gfIgCgBQgRgJgTgHQhMgehXAAQiWAAheBOQhfBMgBB7QAACNBrBLQA+AsCdArQB/AkAuAdIAEACQAWAOAOAQQAlApgBA4QABA/glAtQgMAQgRANIgHAFQhBAwhiAAQhxAAhYgyQg2gfgjgqIA+gvQAZAhArAXQBGAnBaAAQBIAAAsggQAsgfAAg0QAAgpgrgZQgmgXhxggQivgxhNg4QiCheAAixQAAidB0hiQBAg3BVgYQBFgUBRAAQBOAABGAUIAQAEQATAGARAIQA6AXAzAlQA5AqAfAqIAKAPIi8C8IgTgTQgPgPgVgPIgLgIQhQg6hJAAQg5AAgmAaQgjAaAAAfQAAApArAZQAlAXByAgQA3AQAtAQIAkANQBKAdApAfQAzAkAfAwQAxBOABBtQAACvh6BpQhEA6hdAaQhKAVhZAAQi3AAiOhfg");
    var mask_2_graphics_134 = new cjs.Graphics().p("AlGILQgugdgjglQgVgVgJgNIgGgJIA9guIAGAKQAJANAQAQQAfAeAnAaQB5BPCfAAQBcAABHgYQA9gVAtgmQA4guAZhAQATg0AAg/QAAiNhqhLQgYgRgogQIgkgPQgygThGgUQh+gkgugcQhNgvAAhTQAAg/A2guQA8gzBcAAQBMABBCAfIAVAMQAUALAQAOIASARIBThTQgpgqg6gfIgCgBQgRgJgTgHQhMgehXAAQiWAAheBOQhfBMgBB7QAACNBrBLQA+AsCdArQB/AkAuAdIAEACQAWAOAOAQQAlApgBA4QABA/glAtQgMAQgRANIgHAFQhBAwhiAAQhxAAhYgyQg2gfgjgqIA+gvQAZAhArAXQBGAnBaAAQBIAAAsggQAsgfAAg0QAAgpgrgZQgmgXhxggQivgxhNg4QiCheAAixQAAidB0hiQBAg3BVgYQBFgUBRAAQBOAABGAUIAQAEQATAGARAIQA6AXAzAlQA5AqAfAqIAKAPIi8C8IgTgTQgPgPgVgPIgLgIQhQg6hJAAQg5AAgmAaQgjAaAAAfQAAApArAZQAlAXByAgQA3AQAtAQIAkANQBKAdApAfQAzAkAfAwQAxBOABBtQAACvh6BpQhEA6hdAaQhKAVhZAAQi3AAiOhfg");
  
    this.timeline.addTween(cjs.Tween.get(mask_2).to({graphics:null,x:0,y:0}).wait(1).to({graphics:mask_2_graphics_1,x:683.5,y:92.6}).wait(1).to({graphics:mask_2_graphics_2,x:686,y:94}).wait(1).to({graphics:mask_2_graphics_3,x:687.9,y:95.2}).wait(1).to({graphics:mask_2_graphics_4,x:691.1,y:95.7}).wait(1).to({graphics:mask_2_graphics_5,x:693.7,y:95.8}).wait(1).to({graphics:mask_2_graphics_6,x:697,y:95.8}).wait(1).to({graphics:mask_2_graphics_7,x:699.3,y:95.8}).wait(1).to({graphics:mask_2_graphics_8,x:702.2,y:95.8}).wait(1).to({graphics:mask_2_graphics_9,x:704.1,y:95.8}).wait(1).to({graphics:mask_2_graphics_10,x:704.7,y:94.5}).wait(1).to({graphics:mask_2_graphics_11,x:704.7,y:90.9}).wait(1).to({graphics:mask_2_graphics_12,x:704.7,y:88.4}).wait(1).to({graphics:mask_2_graphics_13,x:704.7,y:87}).wait(1).to({graphics:mask_2_graphics_14,x:704.7,y:85.7}).wait(1).to({graphics:mask_2_graphics_15,x:704.7,y:84.7}).wait(1).to({graphics:mask_2_graphics_16,x:704.7,y:84}).wait(1).to({graphics:mask_2_graphics_17,x:704.7,y:82.9}).wait(1).to({graphics:mask_2_graphics_18,x:704.7,y:81.7}).wait(1).to({graphics:mask_2_graphics_19,x:702.3,y:79.5}).wait(1).to({graphics:mask_2_graphics_20,x:699.9,y:76.7}).wait(1).to({graphics:mask_2_graphics_21,x:698.6,y:73.1}).wait(1).to({graphics:mask_2_graphics_22,x:698.4,y:69.5}).wait(1).to({graphics:mask_2_graphics_23,x:698.4,y:64.7}).wait(1).to({graphics:mask_2_graphics_24,x:698.4,y:60.5}).wait(1).to({graphics:mask_2_graphics_25,x:698.4,y:57.5}).wait(1).to({graphics:mask_2_graphics_26,x:698.4,y:55.1}).wait(1).to({graphics:mask_2_graphics_27,x:698.4,y:53.6}).wait(1).to({graphics:mask_2_graphics_28,x:698.4,y:52.6}).wait(1).to({graphics:mask_2_graphics_29,x:698.4,y:52.1}).wait(1).to({graphics:mask_2_graphics_30,x:698.4,y:52.1}).wait(1).to({graphics:mask_2_graphics_31,x:698.4,y:52.1}).wait(1).to({graphics:mask_2_graphics_32,x:701.1,y:52.1}).wait(1).to({graphics:mask_2_graphics_33,x:701.1,y:52.1}).wait(1).to({graphics:mask_2_graphics_34,x:704.3,y:52.1}).wait(1).to({graphics:mask_2_graphics_35,x:706.9,y:52.1}).wait(1).to({graphics:mask_2_graphics_36,x:706.9,y:52.1}).wait(1).to({graphics:mask_2_graphics_37,x:706.9,y:52.1}).wait(1).to({graphics:mask_2_graphics_38,x:706.9,y:52.1}).wait(1).to({graphics:mask_2_graphics_39,x:706.9,y:52.1}).wait(1).to({graphics:mask_2_graphics_40,x:706.9,y:52.1}).wait(1).to({graphics:mask_2_graphics_41,x:706.9,y:52.1}).wait(1).to({graphics:mask_2_graphics_42,x:706.9,y:52.1}).wait(1).to({graphics:mask_2_graphics_43,x:706.9,y:52.1}).wait(1).to({graphics:mask_2_graphics_44,x:706.9,y:52.1}).wait(1).to({graphics:mask_2_graphics_45,x:706.9,y:52.1}).wait(1).to({graphics:mask_2_graphics_46,x:706.9,y:52.1}).wait(1).to({graphics:mask_2_graphics_47,x:706.9,y:52.1}).wait(1).to({graphics:mask_2_graphics_48,x:706.9,y:52.1}).wait(1).to({graphics:mask_2_graphics_49,x:706.9,y:52.1}).wait(1).to({graphics:mask_2_graphics_50,x:706.9,y:52.1}).wait(1).to({graphics:mask_2_graphics_51,x:706.9,y:52.1}).wait(1).to({graphics:mask_2_graphics_52,x:706.9,y:52.1}).wait(1).to({graphics:mask_2_graphics_53,x:707.9,y:52.1}).wait(1).to({graphics:mask_2_graphics_54,x:708.8,y:52.1}).wait(1).to({graphics:mask_2_graphics_55,x:708.9,y:52.1}).wait(1).to({graphics:mask_2_graphics_56,x:708.9,y:52.1}).wait(1).to({graphics:mask_2_graphics_57,x:708.9,y:53.6}).wait(1).to({graphics:mask_2_graphics_58,x:708.9,y:56.8}).wait(1).to({graphics:mask_2_graphics_59,x:708.9,y:59.1}).wait(1).to({graphics:mask_2_graphics_60,x:708.9,y:60.6}).wait(1).to({graphics:mask_2_graphics_61,x:708.9,y:61.5}).wait(1).to({graphics:mask_2_graphics_62,x:708.9,y:61.8}).wait(1).to({graphics:mask_2_graphics_63,x:708.9,y:61.8}).wait(1).to({graphics:mask_2_graphics_64,x:708.9,y:61.8}).wait(1).to({graphics:mask_2_graphics_65,x:708.9,y:61.8}).wait(1).to({graphics:mask_2_graphics_66,x:708.9,y:61.8}).wait(1).to({graphics:mask_2_graphics_67,x:708.9,y:61.8}).wait(1).to({graphics:mask_2_graphics_68,x:708.9,y:61.8}).wait(1).to({graphics:mask_2_graphics_69,x:707.6,y:61.8}).wait(50).to({graphics:mask_2_graphics_119,x:707.6,y:61.8}).wait(1).to({graphics:mask_2_graphics_120,x:707.6,y:61.8}).wait(1).to({graphics:mask_2_graphics_121,x:707.6,y:61.8}).wait(1).to({graphics:mask_2_graphics_122,x:707.6,y:61.8}).wait(1).to({graphics:mask_2_graphics_123,x:707.6,y:61.8}).wait(1).to({graphics:mask_2_graphics_124,x:707.6,y:61.8}).wait(1).to({graphics:mask_2_graphics_125,x:707.6,y:61.8}).wait(1).to({graphics:mask_2_graphics_126,x:707.6,y:61.8}).wait(1).to({graphics:mask_2_graphics_127,x:707.6,y:61.8}).wait(1).to({graphics:mask_2_graphics_128,x:707.6,y:61.8}).wait(1).to({graphics:mask_2_graphics_129,x:707.6,y:61.8}).wait(1).to({graphics:mask_2_graphics_130,x:707.6,y:61.8}).wait(1).to({graphics:mask_2_graphics_131,x:707.6,y:61.8}).wait(1).to({graphics:mask_2_graphics_132,x:707.6,y:61.8}).wait(1).to({graphics:mask_2_graphics_133,x:707.6,y:61.8}).wait(1).to({graphics:mask_2_graphics_134,x:707.6,y:61.8}).wait(1));
  
    // S
    this.shape_2 = new cjs.Shape();
    this.shape_2.graphics.f("#fff").s().p("AlGILQgugdgjglQgVgVgJgNIgGgJIA9guIAGAKQAJANAQAQQAfAeAnAaQB5BPCfAAQBcAABHgYQA9gVAtgmQA4guAZhAQATg0AAg/QAAiNhqhLQgYgRgogQIgkgPQgygThGgUQh+gkgugcQhNgvAAhTQAAg/A2guQA8gzBcAAQBMABBCAfIAVAMQAUALAQAOIASARIBThTQgpgqg6gfIgCgBQgRgJgTgHQhMgehXAAQiWAAheBOQhfBMgBB7QAACNBrBLQA+AsCdArQB/AkAuAdIAEACQAWAOAOAQQAlApgBA4QABA/glAtQgMAQgRANIgHAFQhBAwhiAAQhxAAhYgyQg2gfgjgqIA+gvQAZAhArAXQBGAnBaAAQBIAAAsggQAsgfAAg0QAAgpgrgZQgmgXhxggQivgxhNg4QiCheAAixQAAidB0hiQBAg3BVgYQBFgUBRAAQBOAABGAUIAQAEQATAGARAIQA6AXAzAlQA5AqAfAqIAKAPIi8C8IgTgTQgPgPgVgPIgLgIQhQg6hJAAQg5AAgmAaQgjAaAAAfQAAApArAZQAlAXByAgQA3AQAtAQIAkANQBKAdApAfQAzAkAfAwQAxBOABBtQAACvh6BpQhEA6hdAaQhKAVhZAAQi3AAiOhfg");
    this.shape_2.setTransform(707.6,61.8);
  
    this.instance_4 = new lib.Tween15("synched",0);
    this.instance_4.parent = this;
    this.instance_4.setTransform(707.6,61.8);
    this.instance_4._off = true;
  
    this.instance_5 = new lib.Tween16("synched",0);
    this.instance_5.parent = this;
    this.instance_5.setTransform(707.6,61.8);
    this.instance_5.alpha = 0;
  
    var maskedShapeInstanceList = [this.shape_2,this.instance_4,this.instance_5];
  
    for(var shapedInstanceItr = 0; shapedInstanceItr < maskedShapeInstanceList.length; shapedInstanceItr++) {
      maskedShapeInstanceList[shapedInstanceItr].mask = mask_2;
    }
  
    this.timeline.addTween(cjs.Tween.get({}).to({state:[]}).to({state:[{t:this.shape_2}]},1).to({state:[{t:this.instance_4}]},118).to({state:[{t:this.instance_5}]},15).wait(1));
    this.timeline.addTween(cjs.Tween.get(this.instance_4).wait(119).to({_off:false},0).to({_off:true,alpha:0},15).wait(1));
  
    // R mask (mask)
    var mask_3 = new cjs.Shape();
    mask_3._off = true;
    var mask_3_graphics_1 = new cjs.Graphics().p("AglAmIAAhLIBLAAIAABLg");
    var mask_3_graphics_2 = new cjs.Graphics().p("AhQAmIAAhLICgAAIAABLg");
    var mask_3_graphics_3 = new cjs.Graphics().p("Ah9AmIAAhLID7AAIAABLg");
    var mask_3_graphics_4 = new cjs.Graphics().p("AioAmIAAhLIFRAAIAABLg");
    var mask_3_graphics_5 = new cjs.Graphics().p("AjUAmIAAhLIGXAAIASAAIgiBLg");
    var mask_3_graphics_6 = new cjs.Graphics().p("ACPAgImWAAIAAhMIGWAAQBEAAA1AmIg9AzQgcgNggAAg");
    var mask_3_graphics_7 = new cjs.Graphics().p("ADOAwQgpglg2AAImWAAIAAhLIGWAAQBVAABAA7QAWAUAOAXIhKAbQgHgJgJgIg");
    var mask_3_graphics_8 = new cjs.Graphics().p("ADBAVQgqgkg1AAImXAAIAAhNIGXAAQBVAAA/A8QA4AyAIBEIhMAHQgGgpgjgfg");
    var mask_3_graphics_9 = new cjs.Graphics().p("ADnBkQADgNAAgNQAAg0gqgkQgpglg2AAImWAAIAAhNIGWAAQBVAABAA8QBAA8AABSQAAAdgIAag");
    var mask_3_graphics_10 = new cjs.Graphics().p("ADHB5QAjgjAAgvQAAgygqgmQgpglg2AAImWAAIAAhMIGWAAQBVgBBAA8QBAA8AABSQAABBgoAzIgHAIg");
    var mask_3_graphics_11 = new cjs.Graphics().p("ADBBkQApglAAgzQAAgzgqglQgpglg2AAImWAAIAAhNIGWAAQBVAABAA8QBAA8AABSQAABThAA7QgZAXgcAOg");
    var mask_3_graphics_12 = new cjs.Graphics().p("AB7B8QAmgHAfgaQAqgmAAgzQAAgzgqglQgpglg2AAImWAAIAAhNIGWAAQBVAABAA8QBAA8AABSQAABThAA7Qg2AxhFAIg");
    var mask_3_graphics_13 = new cjs.Graphics().p("AA2DKIAAhMIArAAQA2AAApgkQAqgmAAgzQAAgzgqglQgpglg2AAImWAAIAAhNIGWAAQBVAABAA8QBAA8AABSQAABThAA7QhAA7hVAAg");
    var mask_3_graphics_14 = new cjs.Graphics().p("AgTDKIAAhMIB0AAQA2AAApgkQAqgmAAgzQAAgzgqglQgpglg2AAImWAAIAAhNIGWAAQBVAABAA8QBAA8AABSQAABThAA7QhAA7hVAAg");
    var mask_3_graphics_15 = new cjs.Graphics().p("AhkDKIAAhMIDFAAQA2AAApgkQAqgmAAgzQAAgzgqglQgpglg2AAImWAAIAAhNIGWAAQBVAABAA8QBAA8AABSQAABThAA7QhAA7hVAAg");
    var mask_3_graphics_16 = new cjs.Graphics().p("AiqDKIAAhMIELAAQA2AAApgkQAqgmAAgzQAAgzgqglQgpglg2AAImWAAIAAhNIGWAAQBVAABAA8QBAA8AABSQAABThAA7QhAA7hVAAg");
    var mask_3_graphics_17 = new cjs.Graphics().p("AjtDKIAAhMIFOAAQA2AAApgkQAqgmAAgzQAAgzgqglQgpglg2AAImWAAIAAhNIGWAAQBVAABAA8QBAA8AABSQAABThAA7QhAA7hVAAg");
    var mask_3_graphics_18 = new cjs.Graphics().p("Ak1DKIAAhMIGWAAQA2AAApgkQAqgmAAgzQAAgzgqglQgpglg2AAImWAAIAAhNIGWAAQBVAABAA8QBAA8AABSQAABThAA7QhAA7hVAAg");
    var mask_3_graphics_19 = new cjs.Graphics().p("Ak1EdIAAg2IBMAAIAAA2gAk1B2IAAhMIGWAAQA2AAApgkQAqgkAAg0QAAgzgqgmQgpglg2AAImWAAIAAhMIGWAAQBVAABAA7QBAA8AABTQAABShAA8QhAA6hVAAg");
    var mask_3_graphics_20 = new cjs.Graphics().p("Ak1E6IAAhvIBMAAIAABvgAk1BaIAAhMIGWAAQA2gBApgiQAqgmAAgzQAAg0gqglQgpglg2AAImWAAIAAhNIGWAAQBVAABAA8QBAA8AABTQAABThAA6QhAA6hVABg");
    var mask_3_graphics_21 = new cjs.Graphics().p("Ak1FgIAAi8IBMAAIAAC8gAk1AzIAAhLIGWAAQA2AAApgkQAqglAAg0QAAgzgqgmQgpglg2AAImWAAIAAhMIGWAAQBVAABAA7QBAA8AABTQAABThAA8QhAA5hVAAg");
    var mask_3_graphics_22 = new cjs.Graphics().p("Ak1GNIAAkVIBMAAIAAEVgAk1AHIAAhLIGWAAQA2AAApgkQAqgmAAgzQAAgzgqgmQgpglg2AAImWAAIAAhMIGWAAQBVgBBAA8QBAA8AABTQAABThAA7QhAA6hVAAg");
    var mask_3_graphics_23 = new cjs.Graphics().p("Ak1GyIAAlgIBMAAIAAFggAk1geIAAhMIGWAAQA2AAApgkQAqgmAAgzQAAgzgqgmQgpglg2AAImWAAIAAhMIGWAAQBVgBBAA8QBAA8AABTQAABThAA7QhAA7hVAAg");
    var mask_3_graphics_24 = new cjs.Graphics().p("Ak1HVIAAmmIBMAAIAAGkIABAAIAAACgAk1hBIAAhMIGWAAQA2AAApgkQAqglAAg0QAAgzgqgmQgpglg2AAImWAAIAAhMIGWAAQBVAABAA7QBAA8AABTQAABThAA8QhAA6hVAAg");
    var mask_3_graphics_25 = new cjs.Graphics().p("Ak1H6IAAnvIBMAAIAAGjIABAAIAABMgAk1hlIAAhMIGWAAQA2AAApgkQAqgmAAgzQAAg0gqglQgpglg2AAImWAAIAAhNIGWAAQBVAABAA8QBAA8AABTQAABThAA7QhAA7hVAAg");
    var mask_3_graphics_26 = new cjs.Graphics().p("Ak1H6IAAnvIBMAAIAAGjIA9AAIAABMgAk1hlIAAhMIGWAAQA2AAApgkQAqgmAAgzQAAg0gqglQgpglg2AAImWAAIAAhNIGWAAQBVAABAA8QBAA8AABTQAABThAA7QhAA7hVAAg");
    var mask_3_graphics_27 = new cjs.Graphics().p("Ak1H6IAAnvIBMAAIAAGjICCAAIAABMgAk1hlIAAhMIGWAAQA2AAApgkQAqgmAAgzQAAg0gqglQgpglg2AAImWAAIAAhNIGWAAQBVAABAA8QBAA8AABTQAABThAA7QhAA7hVAAg");
    var mask_3_graphics_28 = new cjs.Graphics().p("Ak1H6IAAnvIBMAAIAAGjIDRAAIAABMgAk1hlIAAhMIGWAAQA2AAApgkQAqgmAAgzQAAg0gqglQgpglg2AAImWAAIAAhNIGWAAQBVAABAA8QBAA8AABTQAABThAA7QhAA7hVAAg");
    var mask_3_graphics_29 = new cjs.Graphics().p("Ak1H6IAAnvIBMAAIAAGjICFAAIAAg7IBMAAIAACHgAk1hlIAAhMIGWAAQA2AAApgkQAqgmAAgzQAAg0gqglQgpglg2AAImWAAIAAhNIGWAAQBVAABAA8QBAA8AABTQAABThAA7QhAA7hVAAg");
    var mask_3_graphics_30 = new cjs.Graphics().p("Ak1H6IAAnvIBMAAIAAGjICFAAIAAh8IBMAAIAADIgAk1hlIAAhMIGWAAQA2AAApgkQAqgmAAgzQAAg0gqglQgpglg2AAImWAAIAAhNIGWAAQBVAABAA8QBAA8AABTQAABThAA7QhAA7hVAAg");
    var mask_3_graphics_31 = new cjs.Graphics().p("Ak1H6IAAnvIBMAAIAAGjICFAAIAAjIIBMAAIAAEUgAk1hlIAAhMIGWAAQA2AAApgkQAqgmAAgzQAAg0gqglQgpglg2AAImWAAIAAhNIGWAAQBVAABAA8QBAA8AABTQAABThAA7QhAA7hVAAg");
    var mask_3_graphics_32 = new cjs.Graphics().p("Ak1H6IAAnvIBMAAIAAGjICFAAIAAkSIBMAAIAAFegAk1hlIAAhMIGWAAQA2AAApgkQAqgmAAgzQAAg0gqglQgpglg2AAImWAAIAAhNIGWAAQBVAABAA8QBAA8AABTQAABThAA7QhAA7hVAAg");
    var mask_3_graphics_33 = new cjs.Graphics().p("Ak1H6IAAnvIBMAAIAAGjICFAAIAAlYIBPAAIAAABIgDAAIAAGjgAk1hlIAAhMIGWAAQA2AAApgkQAqgmAAgzQAAg0gqglQgpglg2AAImWAAIAAhNIGWAAQBVAABAA8QBAA8AABTQAABThAA7QhAA7hVAAg");
    var mask_3_graphics_34 = new cjs.Graphics().p("Ak1H6IAAnvIBMAAIAAGjICFAAIAAmjIBPAAIAABMIgDAAIAAGjgAk1hlIAAhMIGWAAQA2AAApgkQAqgmAAgzQAAg0gqglQgpglg2AAImWAAIAAhNIGWAAQBVAABAA8QBAA8AABTQAABThAA7QhAA7hVAAg");
    var mask_3_graphics_35 = new cjs.Graphics().p("Ak1H6IAAnvIBMAAIAAGjICFAAIAAmjICJAAIAABPIgBAAIgBgDIg7AAIAAGjgAk1hlIAAhMIGWAAQA2AAApgkQAqgmAAgzQAAg0gqglQgpglg2AAImWAAIAAhNIGWAAQBVAABAA8QBAA8AABTQAABThAA7QhAA7hVAAg");
    var mask_3_graphics_36 = new cjs.Graphics().p("Ak1H6IAAnvIBMAAIAAGjICFAAIAAmjIC0AAIAhA9IhNASIgBgDIg7AAIAAGjgAk1hlIAAhMIGWAAQA2AAApgkQAqgmAAgzQAAg0gqglQgpglg2AAImWAAIAAhNIGWAAQBVAABAA8QBAA8AABTQAABThAA7QhAA7hVAAg");
    var mask_3_graphics_37 = new cjs.Graphics().p("Ak1H6IAAnvIBMAAIAAGjICFAAIAAmjIC0AAIAvBWIhCAmIgagwIg7AAIAAGjgAk1hlIAAhMIGWAAQA2AAApgkQAqgmAAgzQAAg0gqglQgpglg2AAImWAAIAAhNIGWAAQBVAABAA8QBAA8AABTQAABThAA7QhAA7hVAAg");
    var mask_3_graphics_38 = new cjs.Graphics().p("Ak1H6IAAnvIBMAAIAAGjICFAAIAAmjIC0AAIBSCYIhBAoIg+h0Ig7AAIAAGjgAk1hlIAAhMIGWAAQA2AAApgkQAqgmAAgzQAAg0gqglQgpglg2AAImWAAIAAhNIGWAAQBVAABAA8QBAA8AABTQAABThAA7QhAA7hVAAg");
    var mask_3_graphics_39 = new cjs.Graphics().p("Ak1H6IAAnvIBMAAIAAGjICFAAIAAmjIC0AAIB0DXIhAApIhhi0Ig7AAIAAGjgAk1hlIAAhMIGWAAQA2AAApgkQAqgmAAgzQAAg0gqglQgpglg2AAImWAAIAAhNIGWAAQBVAABAA8QBAA8AABTQAABThAA7QhAA7hVAAg");
    var mask_3_graphics_40 = new cjs.Graphics().p("Ak1H6IAAnvIBMAAIAAGjICFAAIAAmjIC0AAICUESIhAAqIiBjwIg7AAIAAGjgAk1hlIAAhMIGWAAQA2AAApgkQAqgmAAgzQAAg0gqglQgpglg2AAImWAAIAAhNIGWAAQBVAABAA8QBAA8AABTQAABThAA7QhAA7hVAAg");
    var mask_3_graphics_41 = new cjs.Graphics().p("Ak1H6IAAnvIBMAAIAAGjICFAAIAAmjIC0AAIC+FfIhIAbIijkuIg7AAIAAGjgAk1hlIAAhMIGWAAQA2AAApgkQAqgmAAgzQAAg0gqglQgpglg2AAImWAAIAAhNIGWAAQBVAABAA8QBAA8AABTQAABThAA7QhAA7hVAAg");
    var mask_3_graphics_42 = new cjs.Graphics().p("Ak1H6IAAnvIBMAAIAAGjICFAAIAAmjIC0AAIDfGcIAAAEIhMASIjAlmIg7AAIAAGjgAk1hlIAAhMIGWAAQA2AAApgkQAqgmAAgzQAAg0gqglQgpglg2AAImWAAIAAhNIGWAAQBVAABAA8QBAA8AABTQAABThAA7QhAA7hVAAg");
    var mask_3_graphics_43 = new cjs.Graphics().p("ADvH6IjhmjIg7AAIAAGjIkeAAIAAnvIBNAAIAAGjICFAAIAAmjIC0AAIDjGjIAlAAIAIBMgAlLhlIAAhMIGXAAQA2gBApgjQAqgmAAgzQAAg0gqglQgpglg2AAImXAAIAAhNIGXAAQBVAAA/A8QBBA8AABTQAABThBA7Qg/A6hVABg");
    var mask_3_graphics_44 = new cjs.Graphics().p("ADOH6IjhmjIg7AAIAAGjIkeAAIAAnvIBMAAIAAGjICGAAIAAmjIC0AAIDiGjIBfAAIASBMgAlshlIAAhMIGXAAQA1gBAqgjQAqgmAAgzQAAg0gqglQgqglg1AAImXAAIAAhNIGXAAQBVAAA/A8QBBA8AABTQAABThBA7Qg/A6hVABg");
    var mask_3_graphics_45 = new cjs.Graphics().p("ACkH6IjgmjIg8AAIAAGjIkdAAIAAnvIBMAAIAAGjICFAAIAAmjIC1AAIDiGjIBpAAIAAABIAzAAIAnBLgAmVhlIAAhMIGWAAQA2gBApgjQAqgmAAgzQAAg0gqglQgpglg2AAImWAAIAAhNIGWAAQBVAABAA8QBAA8AABTQAABThAA7QhAA6hVABg");
    var mask_3_graphics_46 = new cjs.Graphics().p("AB4H6IjgmjIg8AAIAAGjIkdAAIAAnvIBMAAIAAGjICFAAIAAmjIC1AAIDiGjIBpAAIAAABICIAAIAqBLgAnBhlIAAhMIGXAAQA1gBApgjQAqgmAAgzQAAg0gqglQgpglg1AAImXAAIAAhNIGXAAQBUAABAA8QBAA8AABTQAABThAA7QhAA6hUABg");
    var mask_3_graphics_47 = new cjs.Graphics().p("AB4H6IjgmjIg8AAIAAGjIkdAAIAAnvIBMAAIAAGjICFAAIAAmjIC1AAIDiGjICZAAIgRgeIBMgVIBHB/gAnBhlIAAhMIGXAAQA1gBApgjQAqgmAAgzQAAg0gqglQgpglg1AAImXAAIAAhNIGXAAQBUAABAA8QBAA8AABTQAABThAA7QhAA6hUABg");
    var mask_3_graphics_48 = new cjs.Graphics().p("AB4H6IjgmjIg8AAIAAGjIkdAAIAAnvIBMAAIAAGjICFAAIAAmjIC1AAIDiGjICZAAIgthPIBFghIBqC8gAnBhlIAAhMIGXAAQA1gBApgjQAqgmAAgzQAAg0gqglQgpglg1AAImXAAIAAhNIGXAAQBUAABAA8QBAA8AABTQAABThAA7QhAA6hUABg");
    var mask_3_graphics_49 = new cjs.Graphics().p("AB4H6IjgmjIg8AAIAAGjIkdAAIAAnvIBMAAIAAGjICFAAIAAmjIC1AAIDiGjICZAAIhTiUIBDgjICSEDgAnBhlIAAhMIGXAAQA1gBApgjQAqgmAAgzQAAg0gqglQgpglg1AAImXAAIAAhNIGXAAQBUAABAA8QBAA8AABTQAABThAA7QhAA6hUABg");
    var mask_3_graphics_50 = new cjs.Graphics().p("AB4H6IjgmjIg8AAIAAGjIkdAAIAAnvIBMAAIAAGjICFAAIAAmjIC1AAIDiGjICZAAIh0jNIBHgeICvE3gAnBhlIAAhMIGXAAQA1gBApgjQAqgmAAgzQAAg0gqglQgpglg1AAImXAAIAAhNIGXAAQBUAABAA8QBAA8AABTQAABThAA7QhAA6hUABg");
    var mask_3_graphics_51 = new cjs.Graphics().p("AB4H6IjgmjIg8AAIAAGjIkdAAIAAnvIBMAAIAAGjICFAAIAAmjIC1AAIDiGjICZAAIiSkDIBDgjIDRFygAnBhlIAAhMIGXAAQA1gBApgjQAqgmAAgzQAAg0gqglQgpglg1AAImXAAIAAhNIGXAAQBUAABAA8QBAA8AABTQAABThAA7QhAA6hUABg");
    var mask_3_graphics_52 = new cjs.Graphics().p("AB4H6IjgmjIg8AAIAAGjIkdAAIAAnvIBMAAIAAGjICFAAIAAmjIC1AAIDiGjICZAAIi0k/IBJgaIDtGlgAnBhlIAAhMIGXAAQA1gBApgjQAqgmAAgzQAAg0gqglQgpglg1AAImXAAIAAhNIGXAAQBUAABAA8QBAA8AABTQAABThAA7QhAA6hUABg");
    var mask_3_graphics_53 = new cjs.Graphics().p("AB4H6IjgmjIg8AAIAAGjIkdAAIAAnvIBMAAIAAGjICFAAIAAmjIC1AAIDiGjICZAAIjRlzIBMgUIEHHTgAnBhlIAAhMIGXAAQA1gBApgjQAqgmAAgzQAAg0gqglQgpglg1AAImXAAIAAhNIGXAAQBUAABAA8QBAA8AABTQAABThAA7QhAA6hUABg");
    var mask_3_graphics_54 = new cjs.Graphics().p("AB4H6IjgmjIg8AAIAAGjIkdAAIAAnvIBMAAIAAGjICFAAIAAmjIC1AAIDiGjICZAAIjcmFIgVgkIAngRIAUgJIA/AzIgOAJIEHHTgAnBhlIAAhMIGXAAQA1gBApgjQAqgmAAgzQAAg0gqglQgpglg1AAImXAAIAAhNIGXAAQBUAABAA8QBAA8AABTQAABThAA7QhAA6hUABg");
    var mask_3_graphics_55 = new cjs.Graphics().p("AB4H6IjgmjIg8AAIAAGjIkdAAIAAnvIBMAAIAAGjICFAAIAAmjIC1AAIDiGjICZAAIjcmFIgVgkIAngRQAmgQAegXIA+AwQgcAXghATIEHHTgAnBhlIAAhMIGXAAQA1gBApgjQAqgmAAgzQAAg0gqglQgpglg1AAImXAAIAAhNIGXAAQBUAABAA8QBAA8AABTQAABThAA7QhAA6hUABg");
    var mask_3_graphics_56 = new cjs.Graphics().p("AB4H6IjgmjIg8AAIAAGjIkdAAIAAnvIBMAAIAAGjICFAAIAAmjIC1AAIDiGjICZAAIjcmFIgVgkIAngRQA8gZArguIBGAkQgsAzg8AjIEHHTgAnBhlIAAhMIGXAAQA1gBApgjQAqgmAAgzQAAg0gqglQgpglg1AAImXAAIAAhNIGXAAQBUAABAA8QBAA8AABTQAABThAA7QhAA6hUABg");
    var mask_3_graphics_57 = new cjs.Graphics().p("AB4H6IjgmjIg8AAIAAGjIkdAAIAAnvIBMAAIAAGjICFAAIAAmjIC1AAIDiGjICZAAIjcmFIgVgkIAngRQBVgkAzhMIBKAaQg1BXhYAyIEHHTgAnBhlIAAhMIGXAAQA1gBApgjQAqgmAAgzQAAg0gqglQgpglg1AAImXAAIAAhNIGXAAQBUAABAA8QBAA8AABTQAABThAA7QhAA6hUABg");
    var mask_3_graphics_58 = new cjs.Graphics().p("AB4H6IjgmjIg8AAIAAGjIkdAAIAAnvIBMAAIAAGjICFAAIAAmjIC1AAIDiGjICZAAIjcmFIgVgkIAngRQBXglA0hQQAMgSAKgUIBSABQgMAggSAfQg1BbhbAzIEHHTgAnBhlIAAhMIGXAAQA1gBApgjQAqgmAAgzQAAg0gqglQgpglg1AAImXAAIAAhNIGXAAQBUAABAA8QBAA8AABTQAABThAA7QhAA6hUABg");
    var mask_3_graphics_59 = new cjs.Graphics().p("AB4H6IjgmjIg8AAIAAGjIkdAAIAAnvIBMAAIAAGjICFAAIAAmjIC1AAIDiGjICZAAIjcmFIgVgkIAngRQBXglA0hQQAeguANg0IBQgPQgFAhgLAgQgMAmgVAkQg1BbhbAzIEHHTgAnBhlIAAhMIGXAAQA1gBApgjQAqgmAAgzQAAg0gqglQgpglg1AAImXAAIAAhNIGXAAQBUAABAA8QBAA8AABTQAABThAA7QhAA6hUABg");
    var mask_3_graphics_60 = new cjs.Graphics().p("AB4H6IjgmjIg8AAIAAGjIkdAAIAAnvIBMAAIAAGjICFAAIAAmjIC1AAIDiGjICZAAIjcmFIgVgkIAngRQBXglA0hQQAyhMABhbIBMgXIAAATQAABAgUA7QgMAmgVAkQg1BbhbAzIEHHTgAnBhlIAAhMIGXAAQA1gBApgjQAqgmAAgzQAAg0gqglQgpglg1AAImXAAIAAhNIGXAAQBUAABAA8QBAA8AABTQAABThAA7QhAA6hUABg");
    var mask_3_graphics_61 = new cjs.Graphics().p("AB4H6IjgmjIg8AAIAAGjIkdAAIAAnvIBMAAIAAGjICFAAIAAmjIC1AAIDiGjICZAAIjcmFIgVgkIAngRQBXglA0hQQA0hOgBhfQAAgegEgcIBHghQAKAtgBAwQAABAgUA7QgMAmgVAkQg1BbhbAzIEHHTgAnBhlIAAhMIGXAAQA1gBApgjQAqgmAAgzQAAg0gqglQgpglg1AAImXAAIAAhNIGXAAQBUAABAA8QBAA8AABTQAABThAA7QhAA6hUABg");
    var mask_3_graphics_62 = new cjs.Graphics().p("AB4H6IjgmjIg8AAIAAGjIkdAAIAAnvIBMAAIAAGjICFAAIAAmjIC1AAIDiGjICZAAIjcmFIgVgkIAngRQBXglA0hQQA0hOgBhfQAAgwgMgrIgHgYIA+gvQAHAQAGARIABACQAUA9gBBEQAABAgUA7QgMAmgVAkQg1BbhbAzIEHHTgAnBhlIAAhMIGXAAQA1gBApgjQAqgmAAgzQAAg0gqglQgpglg1AAImXAAIAAhNIGXAAQBUAABAA8QBAA8AABTQAABThAA7QhAA6hUABg");
    var mask_3_graphics_63 = new cjs.Graphics().p("AB4IAIjgmjIg8AAIAAGjIkdAAIAAnvIBMAAIAAGjICFAAIAAmjIC1AAIDiGjICZAAIjcmEIgVglIAngQQBXgmA0hPQA0hPgBhfQABhYgqhHIA2g4QAcApAPAuQAVA9gBBGQAABAgUA6QgMAmgVAkQg1BbhbAzIEHHTgAnBhfIAAhMIGXAAQA1AAApgkQAqgmAAgzQAAg0gqglQgpglg1AAImXAAIAAhNIGXAAQBUAABAA8QBAA8AABTQAABThAA7QhAA7hUAAg");
    var mask_3_graphics_64 = new cjs.Graphics().p("AB4IhIjgmjIg8AAIAAGjIkdAAIAAnvIBMAAIAAGjICFAAIAAmjIC1AAIDiGjICZAAIjcmFIgVgkIAngSQBXgkA0hPQA0hPgBhfQABhZgqhGIAAh6IAEAEQBCBCAbBSQAVA+gBBFQAABAgUA7QgMAmgVAkQg1BahbA0IEHHTgAnBg+IAAhMIGXAAQA1gBApgjQAqgmAAgzQAAg0gqglQgpglg1AAImXAAIAAhNIGXAAQBUAABAA8QBAA8AABTQAABThAA7QhAA6hUABg");
    var mask_3_graphics_65 = new cjs.Graphics().p("AB4I6IjgmjIg8AAIAAGiIkdAAIAAnvIBMAAIAAGkICFAAIAAmkIC1AAIDiGkICZAAIjcmFIgVglIAngRQBXglA0hPQA0hPgBhfQABiDhdhdIgIgIIAAhiQAhAXAeAeQBCBDAbBSQAVA9gBBGQAABAgUA7QgMAmgVAjQg1BahbA1IEHHTgAnBgmIAAhMIGXAAQA1AAApgkQAqglAAg0QAAgzgqgmQgpglg1AAImXAAIAAhMIGXAAQBUAABAA7QBAA8AABTQAABThAA8QhAA6hUAAg");
    var mask_3_graphics_66 = new cjs.Graphics().p("AB4JMIjgmjIg8AAIAAGjIkdAAIAAnvIBMAAIAAGjICFAAIAAmjIC1AAIDiGjICZAAIjcmFIgVgkIAngSQBXglA0hPQA0hOgBhfQABiEhdhdQgjgjgogXIAAhUQBHAeA7A8QBCBCAbBSQAVA+gBBFQAABAgUA7QgMAmgVAkQg1BahbA0IEHHTgAnBgTIAAhMIGXAAQA1gBApgjQAqgmAAgzQAAg0gqglQgpglg1AAImXAAIAAhNIGXAAQBUAABAA8QBAA8AABTQAABThAA7QhAA6hUABg");
    var mask_3_graphics_67 = new cjs.Graphics().p("AB4JXIjgmjIg8AAIAAGiIkdAAIAAnvIBMAAIAAGkICFAAIAAmkIC1AAIDiGkICZAAIjcmFIgVglIAngRQBXglA0hPQA0hPgBhfQABiDhdhdQhAhBhTgVIAAhOQBzAXBXBYQBCBDAbBSQAVA9gBBGQAABAgUA7QgMAmgVAjQg1BahbA1IEHHTgAnBgJIAAhMIGXAAQA1AAApgkQAqglAAg0QAAgzgqgmQgpglg1AAImXAAIAAhMIGXAAQBUAABAA7QBAA8AABTQAABThAA8QhAA6hUAAg");
    var mask_3_graphics_68 = new cjs.Graphics().p("AB4JaIjgmiIg8AAIAAGiIkdAAIAAnvIBMAAIAAGkICFAAIAAmkIC1AAIDiGkICZAAIjcmFIgVglIAngRQBXgmA0hOQA0hPgBhfQABiEhdhdQhahbiAgDIAAhNIAVABQCTAKBpBsQBCBDAbBSQAVA9gBBGQAABAgUA6QgMAmgVAkQg1BahbA0IEHHTgAnBgFIAAhMIGXAAQA1AAApgkQAqgmAAgzQAAgzgqgmQgpglg1AAImXAAIAAhMIGXAAQBUgBBAA8QBAA8AABTQAABThAA7QhAA7hUAAg");
    var mask_3_graphics_69 = new cjs.Graphics().p("AB4JaIjgmiIg8AAIAAGiIkdAAIAAnvIBMAAIAAGkICFAAIAAmkIC1AAIDiGkICZAAIjcmFIgVglIAngRQBXgmA0hOQA0hPgBhfQABiEhdhdQhchdiDgBIhFAAIAAhNIBFAAIAaABQCTAKBpBsQBCBDAbBSQAVA9gBBGQAABAgUA6QgMAmgVAkQg1BahbA0IEHHTgAnBgFIAAhMIGXAAQA1AAApgkQAqgmAAgzQAAgzgqgmQgpglg1AAImXAAIAAhMIGXAAQBUgBBAA8QBAA8AABTQAABThAA7QhAA7hUAAg");
    var mask_3_graphics_70 = new cjs.Graphics().p("AB4JaIjgmiIg8AAIAAGiIkdAAIAAnvIBMAAIAAGkICFAAIAAmkIC1AAIDiGkICZAAIjcmFIgVglIAngRQBXgmA0hOQA0hPgBhfQABiEhdhdQhchdiDgBIiWAAIAAhNICWAAIAaABQCTAKBpBsQBCBDAbBSQAVA9gBBGQAABAgUA6QgMAmgVAkQg1BahbA0IEHHTgAnBgFIAAhMIGXAAQA1AAApgkQAqgmAAgzQAAgzgqgmQgpglg1AAImXAAIAAhMIGXAAQBUgBBAA8QBAA8AABTQAABThAA7QhAA7hUAAg");
    var mask_3_graphics_71 = new cjs.Graphics().p("AB4JaIjgmiIg8AAIAAGiIkdAAIAAnvIBMAAIAAGkICFAAIAAmkIC1AAIDiGkICZAAIjcmFIgVglIAngRQBXgmA0hOQA0hPgBhfQABiEhdhdQhchdiDgBIjbAAIAAhNIDbAAIAaABQCTAKBpBsQBCBDAbBSQAVA9gBBGQAABAgUA6QgMAmgVAkQg1BahbA0IEHHTgAnBgFIAAhMIGXAAQA1AAApgkQAqgmAAgzQAAgzgqgmQgpglg1AAImXAAIAAhMIGXAAQBUgBBAA8QBAA8AABTQAABThAA7QhAA7hUAAg");
    var mask_3_graphics_72 = new cjs.Graphics().p("AB4JaIjgmiIg8AAIAAGiIkdAAIAAnvIBMAAIAAGkICFAAIAAmkIC1AAIDiGkICZAAIjcmFIgVglIAngRQBXgmA0hOQA0hPgBhfQABiEhdhdQhchdiDgBIkeAAIAAhNIEeAAIAaABQCTAKBpBsQBCBDAbBSQAVA9gBBGQAABAgUA6QgMAmgVAkQg1BahbA0IEHHTgAnBgFIAAhMIGXAAQA1AAApgkQAqgmAAgzQAAgzgqgmQgpglg1AAImXAAIAAhMIGXAAQBUgBBAA8QBAA8AABTQAABThAA7QhAA7hUAAg");
    var mask_3_graphics_73 = new cjs.Graphics().p("AB4JaIjgmiIg8AAIAAGiIkdAAIAAnvIBMAAIAAGkICFAAIAAmkIC1AAIDiGkICZAAIjcmFIgVglIAngRQBXgmA0hOQA0hPgBhfQABiEhdhdQhchdiDgBIltAAIAAhNIFtAAIAaABQCTAKBpBsQBCBDAbBSQAVA9gBBGQAABAgUA6QgMAmgVAkQg1BahbA0IEHHTgAnBgFIAAhMIGXAAQA1AAApgkQAqgmAAgzQAAgzgqgmQgpglg1AAImXAAIAAhMIGXAAQBUgBBAA8QBAA8AABTQAABThAA7QhAA7hUAAg");
    var mask_3_graphics_74 = new cjs.Graphics().p("AB4JaIjgmiIg8AAIAAGiIkdAAIAAnvIBMAAIAAGkICFAAIAAmkIC1AAIDiGkICZAAIjcmFIgVglIAngRQBXgmA0hOQA0hPgBhfQABiEhdhdQhchdiDgBIm6AAIAAhNIG6AAIAaABQCTAKBpBsQBCBDAbBSQAVA9gBBGQAABAgUA6QgMAmgVAkQg1BahbA0IEHHTgAnBgFIAAhMIGXAAQA1AAApgkQAqgmAAgzQAAgzgqgmQgpglg1AAImXAAIAAhMIGXAAQBUgBBAA8QBAA8AABTQAABThAA7QhAA7hUAAg");
    var mask_3_graphics_119 = new cjs.Graphics().p("AB4JaIjgmiIg8AAIAAGiIkdAAIAAnvIBMAAIAAGkICFAAIAAmkIC1AAIDiGkICZAAIjcmFIgVglIAngRQBXgmA0hOQA0hPgBhfQABiEhdhdQhchdiDgBIm6AAIAAhNIG6AAIAaABQCTAKBpBsQBCBDAbBSQAVA9gBBGQAABAgUA6QgMAmgVAkQg1BahbA0IEHHTgAnBgFIAAhMIGXAAQA1AAApgkQAqgmAAgzQAAgzgqgmQgpglg1AAImXAAIAAhMIGXAAQBUgBBAA8QBAA8AABTQAABThAA7QhAA7hUAAg");
    var mask_3_graphics_120 = new cjs.Graphics().p("AB4JaIjgmiIg8AAIAAGiIkdAAIAAnvIBMAAIAAGkICFAAIAAmkIC1AAIDiGkICZAAIjcmFIgVglIAngRQBXgmA0hOQA0hPgBhfQABiEhdhdQhchdiDgBIm6AAIAAhNIG6AAIAaABQCTAKBpBsQBCBDAbBSQAVA9gBBGQAABAgUA6QgMAmgVAkQg1BahbA0IEHHTgAnBgFIAAhMIGXAAQA1AAApgkQAqgmAAgzQAAgzgqgmQgpglg1AAImXAAIAAhMIGXAAQBUgBBAA8QBAA8AABTQAABThAA7QhAA7hUAAg");
    var mask_3_graphics_121 = new cjs.Graphics().p("AB4JaIjgmiIg8AAIAAGiIkdAAIAAnvIBMAAIAAGkICFAAIAAmkIC1AAIDiGkICZAAIjcmFIgVglIAngRQBXgmA0hOQA0hPgBhfQABiEhdhdQhchdiDgBIm6AAIAAhNIG6AAIAaABQCTAKBpBsQBCBDAbBSQAVA9gBBGQAABAgUA6QgMAmgVAkQg1BahbA0IEHHTgAnBgFIAAhMIGXAAQA1AAApgkQAqgmAAgzQAAgzgqgmQgpglg1AAImXAAIAAhMIGXAAQBUgBBAA8QBAA8AABTQAABThAA7QhAA7hUAAg");
    var mask_3_graphics_122 = new cjs.Graphics().p("AB4JaIjgmiIg8AAIAAGiIkdAAIAAnvIBMAAIAAGkICFAAIAAmkIC1AAIDiGkICZAAIjcmFIgVglIAngRQBXgmA0hOQA0hPgBhfQABiEhdhdQhchdiDgBIm6AAIAAhNIG6AAIAaABQCTAKBpBsQBCBDAbBSQAVA9gBBGQAABAgUA6QgMAmgVAkQg1BahbA0IEHHTgAnBgFIAAhMIGXAAQA1AAApgkQAqgmAAgzQAAgzgqgmQgpglg1AAImXAAIAAhMIGXAAQBUgBBAA8QBAA8AABTQAABThAA7QhAA7hUAAg");
    var mask_3_graphics_123 = new cjs.Graphics().p("AB4JaIjgmiIg8AAIAAGiIkdAAIAAnvIBMAAIAAGkICFAAIAAmkIC1AAIDiGkICZAAIjcmFIgVglIAngRQBXgmA0hOQA0hPgBhfQABiEhdhdQhchdiDgBIm6AAIAAhNIG6AAIAaABQCTAKBpBsQBCBDAbBSQAVA9gBBGQAABAgUA6QgMAmgVAkQg1BahbA0IEHHTgAnBgFIAAhMIGXAAQA1AAApgkQAqgmAAgzQAAgzgqgmQgpglg1AAImXAAIAAhMIGXAAQBUgBBAA8QBAA8AABTQAABThAA7QhAA7hUAAg");
    var mask_3_graphics_124 = new cjs.Graphics().p("AB4JaIjgmiIg8AAIAAGiIkdAAIAAnvIBMAAIAAGkICFAAIAAmkIC1AAIDiGkICZAAIjcmFIgVglIAngRQBXgmA0hOQA0hPgBhfQABiEhdhdQhchdiDgBIm6AAIAAhNIG6AAIAaABQCTAKBpBsQBCBDAbBSQAVA9gBBGQAABAgUA6QgMAmgVAkQg1BahbA0IEHHTgAnBgFIAAhMIGXAAQA1AAApgkQAqgmAAgzQAAgzgqgmQgpglg1AAImXAAIAAhMIGXAAQBUgBBAA8QBAA8AABTQAABThAA7QhAA7hUAAg");
    var mask_3_graphics_125 = new cjs.Graphics().p("AB4JaIjgmiIg8AAIAAGiIkdAAIAAnvIBMAAIAAGkICFAAIAAmkIC1AAIDiGkICZAAIjcmFIgVglIAngRQBXgmA0hOQA0hPgBhfQABiEhdhdQhchdiDgBIm6AAIAAhNIG6AAIAaABQCTAKBpBsQBCBDAbBSQAVA9gBBGQAABAgUA6QgMAmgVAkQg1BahbA0IEHHTgAnBgFIAAhMIGXAAQA1AAApgkQAqgmAAgzQAAgzgqgmQgpglg1AAImXAAIAAhMIGXAAQBUgBBAA8QBAA8AABTQAABThAA7QhAA7hUAAg");
    var mask_3_graphics_126 = new cjs.Graphics().p("AB4JaIjgmiIg8AAIAAGiIkdAAIAAnvIBMAAIAAGkICFAAIAAmkIC1AAIDiGkICZAAIjcmFIgVglIAngRQBXgmA0hOQA0hPgBhfQABiEhdhdQhchdiDgBIm6AAIAAhNIG6AAIAaABQCTAKBpBsQBCBDAbBSQAVA9gBBGQAABAgUA6QgMAmgVAkQg1BahbA0IEHHTgAnBgFIAAhMIGXAAQA1AAApgkQAqgmAAgzQAAgzgqgmQgpglg1AAImXAAIAAhMIGXAAQBUgBBAA8QBAA8AABTQAABThAA7QhAA7hUAAg");
    var mask_3_graphics_127 = new cjs.Graphics().p("AB4JaIjgmiIg8AAIAAGiIkdAAIAAnvIBMAAIAAGkICFAAIAAmkIC1AAIDiGkICZAAIjcmFIgVglIAngRQBXgmA0hOQA0hPgBhfQABiEhdhdQhchdiDgBIm6AAIAAhNIG6AAIAaABQCTAKBpBsQBCBDAbBSQAVA9gBBGQAABAgUA6QgMAmgVAkQg1BahbA0IEHHTgAnBgFIAAhMIGXAAQA1AAApgkQAqgmAAgzQAAgzgqgmQgpglg1AAImXAAIAAhMIGXAAQBUgBBAA8QBAA8AABTQAABThAA7QhAA7hUAAg");
    var mask_3_graphics_128 = new cjs.Graphics().p("AB4JaIjgmiIg8AAIAAGiIkdAAIAAnvIBMAAIAAGkICFAAIAAmkIC1AAIDiGkICZAAIjcmFIgVglIAngRQBXgmA0hOQA0hPgBhfQABiEhdhdQhchdiDgBIm6AAIAAhNIG6AAIAaABQCTAKBpBsQBCBDAbBSQAVA9gBBGQAABAgUA6QgMAmgVAkQg1BahbA0IEHHTgAnBgFIAAhMIGXAAQA1AAApgkQAqgmAAgzQAAgzgqgmQgpglg1AAImXAAIAAhMIGXAAQBUgBBAA8QBAA8AABTQAABThAA7QhAA7hUAAg");
    var mask_3_graphics_129 = new cjs.Graphics().p("AB4JaIjgmiIg8AAIAAGiIkdAAIAAnvIBMAAIAAGkICFAAIAAmkIC1AAIDiGkICZAAIjcmFIgVglIAngRQBXgmA0hOQA0hPgBhfQABiEhdhdQhchdiDgBIm6AAIAAhNIG6AAIAaABQCTAKBpBsQBCBDAbBSQAVA9gBBGQAABAgUA6QgMAmgVAkQg1BahbA0IEHHTgAnBgFIAAhMIGXAAQA1AAApgkQAqgmAAgzQAAgzgqgmQgpglg1AAImXAAIAAhMIGXAAQBUgBBAA8QBAA8AABTQAABThAA7QhAA7hUAAg");
    var mask_3_graphics_130 = new cjs.Graphics().p("AB4JaIjgmiIg8AAIAAGiIkdAAIAAnvIBMAAIAAGkICFAAIAAmkIC1AAIDiGkICZAAIjcmFIgVglIAngRQBXgmA0hOQA0hPgBhfQABiEhdhdQhchdiDgBIm6AAIAAhNIG6AAIAaABQCTAKBpBsQBCBDAbBSQAVA9gBBGQAABAgUA6QgMAmgVAkQg1BahbA0IEHHTgAnBgFIAAhMIGXAAQA1AAApgkQAqgmAAgzQAAgzgqgmQgpglg1AAImXAAIAAhMIGXAAQBUgBBAA8QBAA8AABTQAABThAA7QhAA7hUAAg");
    var mask_3_graphics_131 = new cjs.Graphics().p("AB4JaIjgmiIg8AAIAAGiIkdAAIAAnvIBMAAIAAGkICFAAIAAmkIC1AAIDiGkICZAAIjcmFIgVglIAngRQBXgmA0hOQA0hPgBhfQABiEhdhdQhchdiDgBIm6AAIAAhNIG6AAIAaABQCTAKBpBsQBCBDAbBSQAVA9gBBGQAABAgUA6QgMAmgVAkQg1BahbA0IEHHTgAnBgFIAAhMIGXAAQA1AAApgkQAqgmAAgzQAAgzgqgmQgpglg1AAImXAAIAAhMIGXAAQBUgBBAA8QBAA8AABTQAABThAA7QhAA7hUAAg");
    var mask_3_graphics_132 = new cjs.Graphics().p("AB4JaIjgmiIg8AAIAAGiIkdAAIAAnvIBMAAIAAGkICFAAIAAmkIC1AAIDiGkICZAAIjcmFIgVglIAngRQBXgmA0hOQA0hPgBhfQABiEhdhdQhchdiDgBIm6AAIAAhNIG6AAIAaABQCTAKBpBsQBCBDAbBSQAVA9gBBGQAABAgUA6QgMAmgVAkQg1BahbA0IEHHTgAnBgFIAAhMIGXAAQA1AAApgkQAqgmAAgzQAAgzgqgmQgpglg1AAImXAAIAAhMIGXAAQBUgBBAA8QBAA8AABTQAABThAA7QhAA7hUAAg");
    var mask_3_graphics_133 = new cjs.Graphics().p("AB4JaIjgmiIg8AAIAAGiIkdAAIAAnvIBMAAIAAGkICFAAIAAmkIC1AAIDiGkICZAAIjcmFIgVglIAngRQBXgmA0hOQA0hPgBhfQABiEhdhdQhchdiDgBIm6AAIAAhNIG6AAIAaABQCTAKBpBsQBCBDAbBSQAVA9gBBGQAABAgUA6QgMAmgVAkQg1BahbA0IEHHTgAnBgFIAAhMIGXAAQA1AAApgkQAqgmAAgzQAAgzgqgmQgpglg1AAImXAAIAAhMIGXAAQBUgBBAA8QBAA8AABTQAABThAA7QhAA7hUAAg");
    var mask_3_graphics_134 = new cjs.Graphics().p("AB4JaIjgmiIg8AAIAAGiIkdAAIAAnvIBMAAIAAGkICFAAIAAmkIC1AAIDiGkICZAAIjcmFIgVglIAngRQBXgmA0hOQA0hPgBhfQABiEhdhdQhchdiDgBIm6AAIAAhNIG6AAIAaABQCTAKBpBsQBCBDAbBSQAVA9gBBGQAABAgUA6QgMAmgVAkQg1BahbA0IEHHTgAnBgFIAAhMIGXAAQA1AAApgkQAqgmAAgzQAAgzgqgmQgpglg1AAImXAAIAAhMIGXAAQBUgBBAA8QBAA8AABTQAABThAA7QhAA7hUAAg");
  
    this.timeline.addTween(cjs.Tween.get(mask_3).to({graphics:null,x:0,y:0}).wait(1).to({graphics:mask_3_graphics_1,x:571.1,y:24.7}).wait(1).to({graphics:mask_3_graphics_2,x:575.4,y:24.7}).wait(1).to({graphics:mask_3_graphics_3,x:579.9,y:24.7}).wait(1).to({graphics:mask_3_graphics_4,x:584.2,y:24.7}).wait(1).to({graphics:mask_3_graphics_5,x:588.6,y:24.7}).wait(1).to({graphics:mask_3_graphics_6,x:593.7,y:25.3}).wait(1).to({graphics:mask_3_graphics_7,x:596.9,y:27.4}).wait(1).to({graphics:mask_3_graphics_8,x:598.3,y:30.2}).wait(1).to({graphics:mask_3_graphics_9,x:598.3,y:33.8}).wait(1).to({graphics:mask_3_graphics_10,x:598.3,y:37.2}).wait(1).to({graphics:mask_3_graphics_11,x:598.3,y:39.9}).wait(1).to({graphics:mask_3_graphics_12,x:598.3,y:41}).wait(1).to({graphics:mask_3_graphics_13,x:598.3,y:41}).wait(1).to({graphics:mask_3_graphics_14,x:598.3,y:41}).wait(1).to({graphics:mask_3_graphics_15,x:598.3,y:41}).wait(1).to({graphics:mask_3_graphics_16,x:598.3,y:41}).wait(1).to({graphics:mask_3_graphics_17,x:598.3,y:41}).wait(1).to({graphics:mask_3_graphics_18,x:598.3,y:41}).wait(1).to({graphics:mask_3_graphics_19,x:598.3,y:49.4}).wait(1).to({graphics:mask_3_graphics_20,x:598.3,y:52.3}).wait(1).to({graphics:mask_3_graphics_21,x:598.3,y:56.1}).wait(1).to({graphics:mask_3_graphics_22,x:598.3,y:60.5}).wait(1).to({graphics:mask_3_graphics_23,x:598.3,y:64.3}).wait(1).to({graphics:mask_3_graphics_24,x:598.3,y:67.8}).wait(1).to({graphics:mask_3_graphics_25,x:598.3,y:71.4}).wait(1).to({graphics:mask_3_graphics_26,x:598.3,y:71.4}).wait(1).to({graphics:mask_3_graphics_27,x:598.3,y:71.4}).wait(1).to({graphics:mask_3_graphics_28,x:598.3,y:71.4}).wait(1).to({graphics:mask_3_graphics_29,x:598.3,y:71.4}).wait(1).to({graphics:mask_3_graphics_30,x:598.3,y:71.4}).wait(1).to({graphics:mask_3_graphics_31,x:598.3,y:71.4}).wait(1).to({graphics:mask_3_graphics_32,x:598.3,y:71.4}).wait(1).to({graphics:mask_3_graphics_33,x:598.3,y:71.4}).wait(1).to({graphics:mask_3_graphics_34,x:598.3,y:71.4}).wait(1).to({graphics:mask_3_graphics_35,x:598.3,y:71.4}).wait(1).to({graphics:mask_3_graphics_36,x:598.3,y:71.4}).wait(1).to({graphics:mask_3_graphics_37,x:598.3,y:71.4}).wait(1).to({graphics:mask_3_graphics_38,x:598.3,y:71.4}).wait(1).to({graphics:mask_3_graphics_39,x:598.3,y:71.4}).wait(1).to({graphics:mask_3_graphics_40,x:598.3,y:71.4}).wait(1).to({graphics:mask_3_graphics_41,x:598.3,y:71.4}).wait(1).to({graphics:mask_3_graphics_42,x:598.3,y:71.4}).wait(1).to({graphics:mask_3_graphics_43,x:600.5,y:71.5}).wait(1).to({graphics:mask_3_graphics_44,x:603.8,y:71.5}).wait(1).to({graphics:mask_3_graphics_45,x:607.9,y:71.5}).wait(1).to({graphics:mask_3_graphics_46,x:612.3,y:71.5}).wait(1).to({graphics:mask_3_graphics_47,x:612.3,y:71.5}).wait(1).to({graphics:mask_3_graphics_48,x:612.3,y:71.5}).wait(1).to({graphics:mask_3_graphics_49,x:612.3,y:71.5}).wait(1).to({graphics:mask_3_graphics_50,x:612.3,y:71.5}).wait(1).to({graphics:mask_3_graphics_51,x:612.3,y:71.5}).wait(1).to({graphics:mask_3_graphics_52,x:612.3,y:71.5}).wait(1).to({graphics:mask_3_graphics_53,x:612.3,y:71.5}).wait(1).to({graphics:mask_3_graphics_54,x:612.3,y:71.5}).wait(1).to({graphics:mask_3_graphics_55,x:612.3,y:71.5}).wait(1).to({graphics:mask_3_graphics_56,x:612.3,y:71.5}).wait(1).to({graphics:mask_3_graphics_57,x:612.3,y:71.5}).wait(1).to({graphics:mask_3_graphics_58,x:612.3,y:71.5}).wait(1).to({graphics:mask_3_graphics_59,x:612.3,y:71.5}).wait(1).to({graphics:mask_3_graphics_60,x:612.3,y:71.5}).wait(1).to({graphics:mask_3_graphics_61,x:612.3,y:71.5}).wait(1).to({graphics:mask_3_graphics_62,x:612.3,y:71.5}).wait(1).to({graphics:mask_3_graphics_63,x:612.3,y:70.8}).wait(1).to({graphics:mask_3_graphics_64,x:612.3,y:67.6}).wait(1).to({graphics:mask_3_graphics_65,x:612.3,y:65.1}).wait(1).to({graphics:mask_3_graphics_66,x:612.3,y:63.3}).wait(1).to({graphics:mask_3_graphics_67,x:612.3,y:62.2}).wait(1).to({graphics:mask_3_graphics_68,x:612.3,y:61.8}).wait(1).to({graphics:mask_3_graphics_69,x:612.3,y:61.8}).wait(1).to({graphics:mask_3_graphics_70,x:612.3,y:61.8}).wait(1).to({graphics:mask_3_graphics_71,x:612.3,y:61.8}).wait(1).to({graphics:mask_3_graphics_72,x:612.3,y:61.8}).wait(1).to({graphics:mask_3_graphics_73,x:612.3,y:61.8}).wait(1).to({graphics:mask_3_graphics_74,x:612.3,y:61.8}).wait(45).to({graphics:mask_3_graphics_119,x:612.3,y:61.8}).wait(1).to({graphics:mask_3_graphics_120,x:612.3,y:61.8}).wait(1).to({graphics:mask_3_graphics_121,x:612.3,y:61.8}).wait(1).to({graphics:mask_3_graphics_122,x:612.3,y:61.8}).wait(1).to({graphics:mask_3_graphics_123,x:612.3,y:61.8}).wait(1).to({graphics:mask_3_graphics_124,x:612.3,y:61.8}).wait(1).to({graphics:mask_3_graphics_125,x:612.3,y:61.8}).wait(1).to({graphics:mask_3_graphics_126,x:612.3,y:61.8}).wait(1).to({graphics:mask_3_graphics_127,x:612.3,y:61.8}).wait(1).to({graphics:mask_3_graphics_128,x:612.3,y:61.8}).wait(1).to({graphics:mask_3_graphics_129,x:612.3,y:61.8}).wait(1).to({graphics:mask_3_graphics_130,x:612.3,y:61.8}).wait(1).to({graphics:mask_3_graphics_131,x:612.3,y:61.8}).wait(1).to({graphics:mask_3_graphics_132,x:612.3,y:61.8}).wait(1).to({graphics:mask_3_graphics_133,x:612.3,y:61.8}).wait(1).to({graphics:mask_3_graphics_134,x:612.3,y:61.8}).wait(1));
  
    // R
    this.shape_3 = new cjs.Shape();
    this.shape_3.graphics.f("#fff").s().p("AB4JaIjgmiIg8AAIAAGiIkdAAIAAnvIBMAAIAAGkICFAAIAAmkIC1AAIDiGkICZAAIjcmFIgVglIAngRQBXgmA0hOQA0hPgBhfQABiEhdhdQhchdiDgBIm6AAIAAhNIG6AAIAaABQCTAKBpBsQBCBDAbBSQAVA9gBBGQAABAgUA6QgMAmgVAkQg1BahbA0IEHHTgAnBgFIAAhMIGXAAQA1AAApgkQAqgmAAgzQAAgzgqgmQgpglg1AAImXAAIAAhMIGXAAQBUgBBAA8QBAA8AABTQAABThAA7QhAA7hUAAg");
    this.shape_3.setTransform(612.3,61.8);
  
    this.instance_6 = new lib.Tween19("synched",0);
    this.instance_6.parent = this;
    this.instance_6.setTransform(612.3,61.8);
    this.instance_6._off = true;
  
    this.instance_7 = new lib.Tween20("synched",0);
    this.instance_7.parent = this;
    this.instance_7.setTransform(612.3,61.8);
    this.instance_7.alpha = 0;
  
    var maskedShapeInstanceList = [this.shape_3,this.instance_6,this.instance_7];
  
    for(var shapedInstanceItr = 0; shapedInstanceItr < maskedShapeInstanceList.length; shapedInstanceItr++) {
      maskedShapeInstanceList[shapedInstanceItr].mask = mask_3;
    }
  
    this.timeline.addTween(cjs.Tween.get({}).to({state:[]}).to({state:[{t:this.shape_3}]},1).to({state:[{t:this.instance_6}]},118).to({state:[{t:this.instance_7}]},15).wait(1));
    this.timeline.addTween(cjs.Tween.get(this.instance_6).wait(119).to({_off:false},0).to({_off:true,alpha:0},15).wait(1));
  
    // E mask (mask)
    var mask_4 = new cjs.Shape();
    mask_4._off = true;
    var mask_4_graphics_1 = new cjs.Graphics().p("AglAcIAAg3IBLAAIAAAzIAAAAIAAAEg");
    var mask_4_graphics_2 = new cjs.Graphics().p("AglA7IAAh1IBLAAIAAA0IAAAAIAABBg");
    var mask_4_graphics_3 = new cjs.Graphics().p("AglBhIAAjBIBLAAIAAA0IAAAAIAACNg");
    var mask_4_graphics_4 = new cjs.Graphics().p("AhGBhIAAjBIBLAAIAAB1IBCAAIAABMg");
    var mask_4_graphics_5 = new cjs.Graphics().p("AhpBhIAAjBIBMAAIAAB1ICHAAIAABMg");
    var mask_4_graphics_6 = new cjs.Graphics().p("AiOBhIAAjBIBMAAIAAB1IDRAAIAABMg");
    var mask_4_graphics_7 = new cjs.Graphics().p("AiyBhIAAjBIBLAAIABB1IEaAAIAABMg");
    var mask_4_graphics_8 = new cjs.Graphics().p("AjXBhIAAjBIBMAAIABB1IFiAAIAABMg");
    var mask_4_graphics_9 = new cjs.Graphics().p("Aj0BhIAAjBIBMAAIAAB1IGdAAIAABMg");
    var mask_4_graphics_10 = new cjs.Graphics().p("AkVBhIAAjBIBMAAIABB1IHeAAIAABMg");
    var mask_4_graphics_11 = new cjs.Graphics().p("Ak5BhIAAjBIBMAAIABB1IImAAIAABMg");
    var mask_4_graphics_12 = new cjs.Graphics().p("AlbBhIAAjBIBMAAIABB1IJpAAIAABMg");
    var mask_4_graphics_13 = new cjs.Graphics().p("Al9BhIAAjBIBMAAIABB1IKuAAIAABMg");
    var mask_4_graphics_14 = new cjs.Graphics().p("AmcBhIAAjBIBMAAIABB1ILsAAIAABMg");
    var mask_4_graphics_15 = new cjs.Graphics().p("AnBBhIAAjBIBMAAIAAB1IM3AAIAABMg");
    var mask_4_graphics_16 = new cjs.Graphics().p("AnBBhIAAjBIBMAAIAAB1ILrAAIAAg/IBMAAIAACLg");
    var mask_4_graphics_17 = new cjs.Graphics().p("AnBBhIAAjBIBMAAIAAB1ILrAAIAAh1IBMAAIAADBg");
    var mask_4_graphics_18 = new cjs.Graphics().p("AnBCHIAAjAIBMAAIAAB0ILrAAIAAh0IAAAAIAAhNIBMAAIAAENg");
    var mask_4_graphics_19 = new cjs.Graphics().p("AnBCHIAAjAIBMAAIAAB0ILrAAIAAh0Ig5AAIAAhNICFAAIAAENg");
    var mask_4_graphics_20 = new cjs.Graphics().p("AnBCHIAAjAIBMAAIAAB0ILrAAIAAh0Ih+AAIAAhNIDKAAIAAENg");
    var mask_4_graphics_21 = new cjs.Graphics().p("AnBCHIAAjAIBMAAIAAB0ILrAAIAAh0IjFAAIAAhNIERAAIAAENg");
    var mask_4_graphics_22 = new cjs.Graphics().p("AnBCHIAAjAIBMAAIAAB0ILrAAIAAh0IkMAAIAAhNIFYAAIAAENg");
    var mask_4_graphics_23 = new cjs.Graphics().p("AnBCHIAAjAIBMAAIAAB0ILrAAIAAh0IlPAAIAAhNIGbAAIAAENg");
    var mask_4_graphics_24 = new cjs.Graphics().p("AnBCHIAAjAIBMAAIAAB0ILrAAIAAh0ImRAAIAAhNIHdAAIAAENg");
    var mask_4_graphics_25 = new cjs.Graphics().p("AnBCHIAAjAIBMAAIAAB0ILrAAIAAh0InXAAIAAhNIIjAAIAAENg");
    var mask_4_graphics_26 = new cjs.Graphics().p("AnBCHIAAjAIBMAAIAAB0ILrAAIAAh0IodAAIAAhNIJpAAIAAENg");
    var mask_4_graphics_27 = new cjs.Graphics().p("AnBCHIAAjAIBMAAIAAB0ILrAAIAAh0IpmAAIAAhNIKyAAIAAENg");
    var mask_4_graphics_28 = new cjs.Graphics().p("AnBCHIAAjAIBMAAIAAB0ILrAAIAAh0IqrAAIAAhNIL3AAIAAENg");
    var mask_4_graphics_29 = new cjs.Graphics().p("AnBCHIAAjAIBLAAIAAhNIM4AAIAAENgAl1A7ILrAAIAAh0IrrAAg");
    var mask_4_graphics_30 = new cjs.Graphics().p("AnBCHIAAkNIODAAIAAENgAl1A7ILrAAIAAh0IrrAAg");
    var mask_4_graphics_31 = new cjs.Graphics().p("AnBEbIAAkOIODAAIAAEOgAl1DPILrAAIAAh1IrrAAgAnBjOIAAhMIBLAAIAABMg");
    var mask_4_graphics_32 = new cjs.Graphics().p("AnBEbIAAkOIODAAIAAEOgAl1DPILrAAIAAh1IrrAAgAnBjOIAAhMICWAAIAABMg");
    var mask_4_graphics_33 = new cjs.Graphics().p("AnBEbIAAkOIODAAIAAEOgAl1DPILrAAIAAh1IrrAAgAnBjOIAAhMIDVAAIAABMg");
    var mask_4_graphics_34 = new cjs.Graphics().p("AnBEbIAAkOIODAAIAAEOgAl1DPILrAAIAAh1IrrAAgAnBjOIAAhMIEeAAIAABMg");
    var mask_4_graphics_35 = new cjs.Graphics().p("AnBEbIAAkOIODAAIAAEOgAl1DPILrAAIAAh1IrrAAgAnBjOIAAhMIFlAAIAABMg");
    var mask_4_graphics_36 = new cjs.Graphics().p("AnBEbIAAkOIODAAIAAEOgAl1DPILrAAIAAh1IrrAAgAnBjOIAAhMIGpAAIAABMg");
    var mask_4_graphics_37 = new cjs.Graphics().p("AnBEbIAAkOIODAAIAAEOgAl1DPILrAAIAAh1IrrAAgAnBjOIAAhMIHsAAIAABMg");
    var mask_4_graphics_38 = new cjs.Graphics().p("AnBEbIAAkOIODAAIAAEOgAl1DPILrAAIAAh1IrrAAgAnBjOIAAhMIIuAAIAABMg");
    var mask_4_graphics_39 = new cjs.Graphics().p("AnBEbIAAkOIODAAIAAEOgAl1DPILrAAIAAh1IrrAAgAnBjOIAAhMIJuAAIAABMg");
    var mask_4_graphics_40 = new cjs.Graphics().p("AnBEcIAAkOIODAAIAAEOgAl1DQILrAAIAAh2IrrAAgAnBjNIAAhNIKsAAIAAgBIABAAIAABOg");
    var mask_4_graphics_41 = new cjs.Graphics().p("AnBEcIAAkOIODAAIAAEOgAl1DQILrAAIAAh2IrrAAgAnBjNIAAhNIKsAAIAAgBIBMAAIAABOg");
    var mask_4_graphics_42 = new cjs.Graphics().p("AnBFXIAAkOIODAAIAAEOgAl1ELILrAAIAAh1IrrAAgAnBiSIAAhMIKsAAIAAh2IgBAAIAAgCIBNAAIAADEg");
    var mask_4_graphics_43 = new cjs.Graphics().p("AnBF8IAAkOIODAAIAAEOgAl1EwILrAAIAAh1IrrAAgAnBhtIAAhNIKsAAIAAh1IgBAAIAAhMIBNAAIAAEOg");
    var mask_4_graphics_44 = new cjs.Graphics().p("AnBF8IAAkOIODAAIAAEOgAl1EwILrAAIAAh1IrrAAgAnBhtIAAhNIKsAAIAAh1IhDAAIAAhMICPAAIAAEOg");
    var mask_4_graphics_45 = new cjs.Graphics().p("AnBF8IAAkOIODAAIAAEOgAl1EwILrAAIAAh1IrrAAgAnBhtIAAhNIKsAAIAAh1IiAAAIAAhMIDMAAIAAEOg");
    var mask_4_graphics_46 = new cjs.Graphics().p("AnBF8IAAkOIODAAIAAEOgAl1EwILrAAIAAh1IrrAAgAnBhtIAAhNIKsAAIAAh1Ii8AAIAAhMIEIAAIAAEOg");
    var mask_4_graphics_47 = new cjs.Graphics().p("AnBF8IAAkOIODAAIAAEOgAl1EwILrAAIAAh1IrrAAgAnBhtIAAhNIKsAAIAAh1Ij9AAIAAhMIFJAAIAAEOg");
    var mask_4_graphics_48 = new cjs.Graphics().p("AnBF8IAAkOIODAAIAAEOgAl1EwILrAAIAAh1IrrAAgAnBhtIAAhNIKsAAIAAh1Ik8AAIAAhMIGIAAIAAEOg");
    var mask_4_graphics_49 = new cjs.Graphics().p("AnBF8IAAkOIODAAIAAEOgAl1EwILrAAIAAh1IrrAAgAnBhtIAAhNIKsAAIAAh1IlzAAIAAhMIG/AAIAAEOg");
    var mask_4_graphics_50 = new cjs.Graphics().p("AnBF8IAAkOIODAAIAAEOgAl1EwILrAAIAAh1IrrAAgAnBhtIAAhNIKsAAIAAh1ImwAAIAAhMIH8AAIAAEOg");
    var mask_4_graphics_51 = new cjs.Graphics().p("AnBF8IAAkOIODAAIAAEOgAl1EwILrAAIAAh1IrrAAgAnBhtIAAhNIKsAAIAAh1InyAAIAAhMII+AAIAAEOg");
    var mask_4_graphics_52 = new cjs.Graphics().p("AnBF8IAAkOIODAAIAAEOgAl1EwILrAAIAAh1IrrAAgAnBhtIAAhNIKsAAIAAh1IolAAIAAhMIJxAAIAAEOg");
    var mask_4_graphics_53 = new cjs.Graphics().p("AnBF9IAAkOIODAAIAAEOgAl1EwILrAAIAAh1IrrAAgAnBhtIAAhMIKsAAIAAh1IphAAIAAhOIABAAIAAABIKsAAIAAEOg");
    var mask_4_graphics_54 = new cjs.Graphics().p("AnBF9IAAkOIODAAIAAEOgAl1EwILrAAIAAh1IrrAAgAnBhtIAAhMIKsAAIAAh1IqsAAIAAhOIBMAAIAAABIKsAAIAAEOg");
    var mask_4_graphics_55 = new cjs.Graphics().p("AnBGvIAAkOIODAAIAAEOgAl1FjILrAAIAAh2IrrAAgAnBg6IAAhNIKsAAIAAh1IqsAAIAAiyIBMAAIAABmIKsAAIAAEOg");
    var mask_4_graphics_56 = new cjs.Graphics().p("AnBHTIAAkOIODAAIAAEOgAl1GHILrAAIAAh2IrrAAgAnBgWIAAhNIKsAAIAAh1IqsAAIAAj6IBMAAIAACuIKsAAIAAEOg");
    var mask_4_graphics_57 = new cjs.Graphics().p("AnBH5IAAkOIODAAIAAEOgAl1GtILrAAIAAh1IrrAAgAnBAPIAAhLIKsAAIAAh2IqsAAIAAlGIBNAAIAABMIgBAAIAACuIKsAAIAAENg");
    var mask_4_graphics_58 = new cjs.Graphics().p("AnBH5IAAkOIODAAIAAEOgAl1GtILrAAIAAh1IrrAAgAnBAPIAAhLIKsAAIAAh2IqsAAIAAlGIB6AAIAABMIguAAIAACuIKsAAIAAENg");
    var mask_4_graphics_59 = new cjs.Graphics().p("AnBH5IAAkOIODAAIAAEOgAl1GtILrAAIAAh1IrrAAgAnBAPIAAhLIKsAAIAAh2IqsAAIAAlGIC4AAIAABMIhsAAIAACuIKsAAIAAENg");
    var mask_4_graphics_60 = new cjs.Graphics().p("AnBH5IAAkOIODAAIAAEOgAl1GtILrAAIAAh1IrrAAgAnBAPIAAhLIKsAAIAAh2IqsAAIAAlGID8AAIAABMIiwAAIAACuIKsAAIAAENg");
    var mask_4_graphics_61 = new cjs.Graphics().p("AnBH5IAAkOIODAAIAAEOgAl1GtILrAAIAAh1IrrAAgAnBAPIAAhLIKsAAIAAh2IqsAAIAAlGIFFAAIAABMIj5AAIAACuIKsAAIAAENg");
    var mask_4_graphics_62 = new cjs.Graphics().p("AnBH5IAAkOIODAAIAAEOgAl1GtILrAAIAAh1IrrAAgAnBAPIAAhLIKsAAIAAh2IqsAAIAAlGIGPAAIAABMIlDAAIAACuIKsAAIAAENg");
    var mask_4_graphics_63 = new cjs.Graphics().p("AnBH5IAAkOIODAAIAAEOgAl1GtILrAAIAAh1IrrAAgAnBAPIAAhLIKsAAIAAh2IqsAAIAAlGIHYAAIAABMImMAAIAACuIKsAAIAAENg");
    var mask_4_graphics_64 = new cjs.Graphics().p("AnBH5IAAkOIODAAIAAEOgAl1GtILrAAIAAh1IrrAAgAnBAPIAAhLIKsAAIAAh2IqsAAIAAlGIIeAAIAABMInSAAIAACuIKsAAIAAENg");
    var mask_4_graphics_65 = new cjs.Graphics().p("AnBH5IAAkOIODAAIAAEOgAl1GtILrAAIAAh1IrrAAgAnBAPIAAhLIKsAAIAAh2IqsAAIAAlGIJkAAIAABMIoYAAIAACuIKsAAIAAENg");
    var mask_4_graphics_66 = new cjs.Graphics().p("AnBH5IAAkOIODAAIAAEOgAl1GtILrAAIAAh1IrrAAgAnBAPIAAhLIKsAAIAAh2IqsAAIAAlGIKnAAIAABMIpbAAIAACuIKsAAIAAENg");
    var mask_4_graphics_67 = new cjs.Graphics().p("AnBH5IAAkOIODAAIAAEOgAl1GtILrAAIAAh1IrrAAgAnBAPIAAhLIKsAAIAAh2IqsAAIAAlGILpAAIAABMIqdAAIAACuIKsAAIAAENg");
    var mask_4_graphics_68 = new cjs.Graphics().p("AnBH6IAAkOIODAAIAAEOgAl1GuILrAAIAAh1IrrAAgAnBAQIAAhMIKsAAIAAh1IqsAAIAAlGIMpAAIAAgCIABAAIAABNIreAAIAACvIKsAAIAAENg");
    var mask_4_graphics_69 = new cjs.Graphics().p("AnBH6IAAkOIODAAIAAEOgAl1GuILrAAIAAh1IrrAAgAnBAQIAAhMIKsAAIAAh1IqsAAIAAlGIMpAAIAAgCIBLAAIAABNIsoAAIAACvIKsAAIAAENg");
    var mask_4_graphics_70 = new cjs.Graphics().p("AnBI1IAAkOIODAAIAAEOgAl1HpILrAAIAAh2IrrAAgAnBBLIAAhMIKsAAIAAh1IqsAAIAAlHIMpAAIAAhIIABAAIAAgvIBKAAIAADDIsoAAIAACvIKsAAIAAENg");
    var mask_4_graphics_71 = new cjs.Graphics().p("AnBJaIAAkOIODAAIAAEOgAl1IOILrAAIAAh1IrrAAgAnBBwIAAhMIKsAAIAAh1IqsAAIAAlGIMpAAIAAhJIABAAIAAh5IBKAAIAAEOIsoAAIAACuIKsAAIAAENg");
    var mask_4_graphics_72 = new cjs.Graphics().p("AnBJaIAAkOIODAAIAAEOgAl1IOILrAAIAAh1IrrAAgAnBBwIAAhMIKsAAIAAh1IqsAAIAAlGIMpAAIAAh2IguAAIAAhMIB5AAIAAEOIsoAAIAACuIKsAAIAAENg");
    var mask_4_graphics_73 = new cjs.Graphics().p("AnBJaIAAkOIODAAIAAEOgAl1IOILrAAIAAh1IrrAAgAnBBwIAAhMIKsAAIAAh1IqsAAIAAlGIMpAAIAAh2IhjAAIAAhMICuAAIAAEOIsoAAIAACuIKsAAIAAENg");
    var mask_4_graphics_74 = new cjs.Graphics().p("AnBJaIAAkOIODAAIAAEOgAl1IOILrAAIAAh1IrrAAgAnBBwIAAhMIKsAAIAAh1IqsAAIAAlGIMpAAIAAh2IieAAIAAhMIDpAAIAAEOIsoAAIAACuIKsAAIAAENg");
    var mask_4_graphics_75 = new cjs.Graphics().p("AnBJaIAAkOIODAAIAAEOgAl1IOILrAAIAAh1IrrAAgAnBBwIAAhMIKsAAIAAh1IqsAAIAAlGIMpAAIAAh2IjwAAIAAhMIE7AAIAAEOIsoAAIAACuIKsAAIAAENg");
    var mask_4_graphics_76 = new cjs.Graphics().p("AnBJaIAAkOIODAAIAAEOgAl1IOILrAAIAAh1IrrAAgAnBBwIAAhMIKsAAIAAh1IqsAAIAAlGIMpAAIAAh2Ik1AAIAAhMIGAAAIAAEOIsoAAIAACuIKsAAIAAENg");
    var mask_4_graphics_77 = new cjs.Graphics().p("AnBJaIAAkOIODAAIAAEOgAl1IOILrAAIAAh1IrrAAgAnBBwIAAhMIKsAAIAAh1IqsAAIAAlGIMpAAIAAh2ImBAAIAAhMIHMAAIAAEOIsoAAIAACuIKsAAIAAENg");
    var mask_4_graphics_78 = new cjs.Graphics().p("AnBJaIAAkOIODAAIAAEOgAl1IOILrAAIAAh1IrrAAgAnBBwIAAhMIKsAAIAAh1IqsAAIAAlGIMpAAIAAh2Im+AAIAAhMIIJAAIAAEOIsoAAIAACuIKsAAIAAENg");
    var mask_4_graphics_79 = new cjs.Graphics().p("AnBJaIAAkOIODAAIAAEOgAl1IOILrAAIAAh1IrrAAgAnBBwIAAhMIKsAAIAAh1IqsAAIAAlGIMpAAIAAh2IoIAAIAAhMIJTAAIAAEOIsoAAIAACuIKsAAIAAENg");
    var mask_4_graphics_80 = new cjs.Graphics().p("AnBJaIAAkOIODAAIAAEOgAl1IOILrAAIAAh1IrrAAgAnBBwIAAhMIKsAAIAAh1IqsAAIAAlGIMpAAIAAh2IpNAAIAAhMIKYAAIAAEOIsoAAIAACuIKsAAIAAENg");
    var mask_4_graphics_81 = new cjs.Graphics().p("AnBJaIAAkOIODAAIAAEOgAl1IOILrAAIAAh1IrrAAgAnBBwIAAhMIKsAAIAAh1IqsAAIAAlGIMpAAIAAh2IqVAAIAAhMILgAAIAAEOIsoAAIAACuIKsAAIAAENg");
    var mask_4_graphics_82 = new cjs.Graphics().p("AnBJaIAAkOIODAAIAAEOgAl1IOILrAAIAAh1IrrAAgAnBBwIAAhMIKsAAIAAh1IqsAAIAAlGIMpAAIAAh2IrgAAIAAhMIMrAAIAAEOIsoAAIAACuIKsAAIAAENg");
    var mask_4_graphics_83 = new cjs.Graphics().p("AnBJaIAAkOIODAAIAAEOgAl1IOILrAAIAAh1IrrAAgAnBBwIAAhMIKsAAIAAh1IqsAAIAAlGIMpAAIAAh2IspAAIAAhMIN0AAIAAEOIsoAAIAACuIKsAAIAAENg");
    var mask_4_graphics_119 = new cjs.Graphics().p("AnBJaIAAkOIODAAIAAEOgAl1IOILrAAIAAh1IrrAAgAnBBwIAAhMIKsAAIAAh1IqsAAIAAlGIMpAAIAAh2IspAAIAAhMIN0AAIAAEOIsoAAIAACuIKsAAIAAENg");
    var mask_4_graphics_120 = new cjs.Graphics().p("AnBJaIAAkOIODAAIAAEOgAl1IOILrAAIAAh1IrrAAgAnBBwIAAhMIKsAAIAAh1IqsAAIAAlGIMpAAIAAh2IspAAIAAhMIN0AAIAAEOIsoAAIAACuIKsAAIAAENg");
    var mask_4_graphics_121 = new cjs.Graphics().p("AnBJaIAAkOIODAAIAAEOgAl1IOILrAAIAAh1IrrAAgAnBBwIAAhMIKsAAIAAh1IqsAAIAAlGIMpAAIAAh2IspAAIAAhMIN0AAIAAEOIsoAAIAACuIKsAAIAAENg");
    var mask_4_graphics_122 = new cjs.Graphics().p("AnBJaIAAkOIODAAIAAEOgAl1IOILrAAIAAh1IrrAAgAnBBwIAAhMIKsAAIAAh1IqsAAIAAlGIMpAAIAAh2IspAAIAAhMIN0AAIAAEOIsoAAIAACuIKsAAIAAENg");
    var mask_4_graphics_123 = new cjs.Graphics().p("AnBJaIAAkOIODAAIAAEOgAl1IOILrAAIAAh1IrrAAgAnBBwIAAhMIKsAAIAAh1IqsAAIAAlGIMpAAIAAh2IspAAIAAhMIN0AAIAAEOIsoAAIAACuIKsAAIAAENg");
    var mask_4_graphics_124 = new cjs.Graphics().p("AnBJaIAAkOIODAAIAAEOgAl1IOILrAAIAAh1IrrAAgAnBBwIAAhMIKsAAIAAh1IqsAAIAAlGIMpAAIAAh2IspAAIAAhMIN0AAIAAEOIsoAAIAACuIKsAAIAAENg");
    var mask_4_graphics_125 = new cjs.Graphics().p("AnBJaIAAkOIODAAIAAEOgAl1IOILrAAIAAh1IrrAAgAnBBwIAAhMIKsAAIAAh1IqsAAIAAlGIMpAAIAAh2IspAAIAAhMIN0AAIAAEOIsoAAIAACuIKsAAIAAENg");
    var mask_4_graphics_126 = new cjs.Graphics().p("AnBJaIAAkOIODAAIAAEOgAl1IOILrAAIAAh1IrrAAgAnBBwIAAhMIKsAAIAAh1IqsAAIAAlGIMpAAIAAh2IspAAIAAhMIN0AAIAAEOIsoAAIAACuIKsAAIAAENg");
    var mask_4_graphics_127 = new cjs.Graphics().p("AnBJaIAAkOIODAAIAAEOgAl1IOILrAAIAAh1IrrAAgAnBBwIAAhMIKsAAIAAh1IqsAAIAAlGIMpAAIAAh2IspAAIAAhMIN0AAIAAEOIsoAAIAACuIKsAAIAAENg");
    var mask_4_graphics_128 = new cjs.Graphics().p("AnBJaIAAkOIODAAIAAEOgAl1IOILrAAIAAh1IrrAAgAnBBwIAAhMIKsAAIAAh1IqsAAIAAlGIMpAAIAAh2IspAAIAAhMIN0AAIAAEOIsoAAIAACuIKsAAIAAENg");
    var mask_4_graphics_129 = new cjs.Graphics().p("AnBJaIAAkOIODAAIAAEOgAl1IOILrAAIAAh1IrrAAgAnBBwIAAhMIKsAAIAAh1IqsAAIAAlGIMpAAIAAh2IspAAIAAhMIN0AAIAAEOIsoAAIAACuIKsAAIAAENg");
    var mask_4_graphics_130 = new cjs.Graphics().p("AnBJaIAAkOIODAAIAAEOgAl1IOILrAAIAAh1IrrAAgAnBBwIAAhMIKsAAIAAh1IqsAAIAAlGIMpAAIAAh2IspAAIAAhMIN0AAIAAEOIsoAAIAACuIKsAAIAAENg");
    var mask_4_graphics_131 = new cjs.Graphics().p("AnBJaIAAkOIODAAIAAEOgAl1IOILrAAIAAh1IrrAAgAnBBwIAAhMIKsAAIAAh1IqsAAIAAlGIMpAAIAAh2IspAAIAAhMIN0AAIAAEOIsoAAIAACuIKsAAIAAENg");
    var mask_4_graphics_132 = new cjs.Graphics().p("AnBJaIAAkOIODAAIAAEOgAl1IOILrAAIAAh1IrrAAgAnBBwIAAhMIKsAAIAAh1IqsAAIAAlGIMpAAIAAh2IspAAIAAhMIN0AAIAAEOIsoAAIAACuIKsAAIAAENg");
    var mask_4_graphics_133 = new cjs.Graphics().p("AnBJaIAAkOIODAAIAAEOgAl1IOILrAAIAAh1IrrAAgAnBBwIAAhMIKsAAIAAh1IqsAAIAAlGIMpAAIAAh2IspAAIAAhMIN0AAIAAEOIsoAAIAACuIKsAAIAAENg");
    var mask_4_graphics_134 = new cjs.Graphics().p("AnBJaIAAkOIODAAIAAEOgAl1IOILrAAIAAh1IrrAAgAnBBwIAAhMIKsAAIAAh1IqsAAIAAlGIMpAAIAAh2IspAAIAAhMIN0AAIAAEOIsoAAIAACuIKsAAIAAENg");
  
    this.timeline.addTween(cjs.Tween.get(mask_4).to({graphics:null,x:0,y:0}).wait(1).to({graphics:mask_4_graphics_1,x:462.8,y:105.4}).wait(1).to({graphics:mask_4_graphics_2,x:462.8,y:108.6}).wait(1).to({graphics:mask_4_graphics_3,x:462.8,y:112.3}).wait(1).to({graphics:mask_4_graphics_4,x:466.1,y:112.3}).wait(1).to({graphics:mask_4_graphics_5,x:469.6,y:112.3}).wait(1).to({graphics:mask_4_graphics_6,x:473.3,y:112.3}).wait(1).to({graphics:mask_4_graphics_7,x:477,y:112.3}).wait(1).to({graphics:mask_4_graphics_8,x:480.6,y:112.3}).wait(1).to({graphics:mask_4_graphics_9,x:483.5,y:112.3}).wait(1).to({graphics:mask_4_graphics_10,x:486.8,y:112.3}).wait(1).to({graphics:mask_4_graphics_11,x:490.4,y:112.3}).wait(1).to({graphics:mask_4_graphics_12,x:493.8,y:112.3}).wait(1).to({graphics:mask_4_graphics_13,x:497.2,y:112.3}).wait(1).to({graphics:mask_4_graphics_14,x:500.3,y:112.3}).wait(1).to({graphics:mask_4_graphics_15,x:504,y:112.3}).wait(1).to({graphics:mask_4_graphics_16,x:504,y:112.3}).wait(1).to({graphics:mask_4_graphics_17,x:504,y:112.3}).wait(1).to({graphics:mask_4_graphics_18,x:504,y:108.5}).wait(1).to({graphics:mask_4_graphics_19,x:504,y:108.5}).wait(1).to({graphics:mask_4_graphics_20,x:504,y:108.5}).wait(1).to({graphics:mask_4_graphics_21,x:504,y:108.5}).wait(1).to({graphics:mask_4_graphics_22,x:504,y:108.5}).wait(1).to({graphics:mask_4_graphics_23,x:504,y:108.5}).wait(1).to({graphics:mask_4_graphics_24,x:504,y:108.5}).wait(1).to({graphics:mask_4_graphics_25,x:504,y:108.5}).wait(1).to({graphics:mask_4_graphics_26,x:504,y:108.5}).wait(1).to({graphics:mask_4_graphics_27,x:504,y:108.5}).wait(1).to({graphics:mask_4_graphics_28,x:504,y:108.5}).wait(1).to({graphics:mask_4_graphics_29,x:504,y:108.5}).wait(1).to({graphics:mask_4_graphics_30,x:504,y:108.5}).wait(1).to({graphics:mask_4_graphics_31,x:504,y:93.7}).wait(1).to({graphics:mask_4_graphics_32,x:504,y:93.7}).wait(1).to({graphics:mask_4_graphics_33,x:504,y:93.7}).wait(1).to({graphics:mask_4_graphics_34,x:504,y:93.7}).wait(1).to({graphics:mask_4_graphics_35,x:504,y:93.7}).wait(1).to({graphics:mask_4_graphics_36,x:504,y:93.7}).wait(1).to({graphics:mask_4_graphics_37,x:504,y:93.7}).wait(1).to({graphics:mask_4_graphics_38,x:504,y:93.7}).wait(1).to({graphics:mask_4_graphics_39,x:504,y:93.7}).wait(1).to({graphics:mask_4_graphics_40,x:504,y:93.6}).wait(1).to({graphics:mask_4_graphics_41,x:504,y:93.6}).wait(1).to({graphics:mask_4_graphics_42,x:504,y:87.7}).wait(1).to({graphics:mask_4_graphics_43,x:504,y:84}).wait(1).to({graphics:mask_4_graphics_44,x:504,y:84}).wait(1).to({graphics:mask_4_graphics_45,x:504,y:84}).wait(1).to({graphics:mask_4_graphics_46,x:504,y:84}).wait(1).to({graphics:mask_4_graphics_47,x:504,y:84}).wait(1).to({graphics:mask_4_graphics_48,x:504,y:84}).wait(1).to({graphics:mask_4_graphics_49,x:504,y:84}).wait(1).to({graphics:mask_4_graphics_50,x:504,y:84}).wait(1).to({graphics:mask_4_graphics_51,x:504,y:84}).wait(1).to({graphics:mask_4_graphics_52,x:504,y:84}).wait(1).to({graphics:mask_4_graphics_53,x:504,y:84}).wait(1).to({graphics:mask_4_graphics_54,x:504,y:84}).wait(1).to({graphics:mask_4_graphics_55,x:504,y:78.9}).wait(1).to({graphics:mask_4_graphics_56,x:504,y:75.3}).wait(1).to({graphics:mask_4_graphics_57,x:504,y:71.5}).wait(1).to({graphics:mask_4_graphics_58,x:504,y:71.5}).wait(1).to({graphics:mask_4_graphics_59,x:504,y:71.5}).wait(1).to({graphics:mask_4_graphics_60,x:504,y:71.5}).wait(1).to({graphics:mask_4_graphics_61,x:504,y:71.5}).wait(1).to({graphics:mask_4_graphics_62,x:504,y:71.5}).wait(1).to({graphics:mask_4_graphics_63,x:504,y:71.5}).wait(1).to({graphics:mask_4_graphics_64,x:504,y:71.5}).wait(1).to({graphics:mask_4_graphics_65,x:504,y:71.5}).wait(1).to({graphics:mask_4_graphics_66,x:504,y:71.5}).wait(1).to({graphics:mask_4_graphics_67,x:504,y:71.5}).wait(1).to({graphics:mask_4_graphics_68,x:504,y:71.4}).wait(1).to({graphics:mask_4_graphics_69,x:504,y:71.4}).wait(1).to({graphics:mask_4_graphics_70,x:504,y:65.5}).wait(1).to({graphics:mask_4_graphics_71,x:504,y:61.8}).wait(1).to({graphics:mask_4_graphics_72,x:504,y:61.8}).wait(1).to({graphics:mask_4_graphics_73,x:504,y:61.8}).wait(1).to({graphics:mask_4_graphics_74,x:504,y:61.8}).wait(1).to({graphics:mask_4_graphics_75,x:504,y:61.8}).wait(1).to({graphics:mask_4_graphics_76,x:504,y:61.8}).wait(1).to({graphics:mask_4_graphics_77,x:504,y:61.8}).wait(1).to({graphics:mask_4_graphics_78,x:504,y:61.8}).wait(1).to({graphics:mask_4_graphics_79,x:504,y:61.8}).wait(1).to({graphics:mask_4_graphics_80,x:504,y:61.8}).wait(1).to({graphics:mask_4_graphics_81,x:504,y:61.8}).wait(1).to({graphics:mask_4_graphics_82,x:504,y:61.8}).wait(1).to({graphics:mask_4_graphics_83,x:504,y:61.8}).wait(36).to({graphics:mask_4_graphics_119,x:504,y:61.8}).wait(1).to({graphics:mask_4_graphics_120,x:504,y:61.8}).wait(1).to({graphics:mask_4_graphics_121,x:504,y:61.8}).wait(1).to({graphics:mask_4_graphics_122,x:504,y:61.8}).wait(1).to({graphics:mask_4_graphics_123,x:504,y:61.8}).wait(1).to({graphics:mask_4_graphics_124,x:504,y:61.8}).wait(1).to({graphics:mask_4_graphics_125,x:504,y:61.8}).wait(1).to({graphics:mask_4_graphics_126,x:504,y:61.8}).wait(1).to({graphics:mask_4_graphics_127,x:504,y:61.8}).wait(1).to({graphics:mask_4_graphics_128,x:504,y:61.8}).wait(1).to({graphics:mask_4_graphics_129,x:504,y:61.8}).wait(1).to({graphics:mask_4_graphics_130,x:504,y:61.8}).wait(1).to({graphics:mask_4_graphics_131,x:504,y:61.8}).wait(1).to({graphics:mask_4_graphics_132,x:504,y:61.8}).wait(1).to({graphics:mask_4_graphics_133,x:504,y:61.8}).wait(1).to({graphics:mask_4_graphics_134,x:504,y:61.8}).wait(1));
  
    // E
    this.shape_4 = new cjs.Shape();
    this.shape_4.graphics.f("#fff").s().p("AnBJaIAAkOIODAAIAAEOgAl1IOILrAAIAAh1IrrAAgAnBBwIAAhMIKsAAIAAh1IqsAAIAAlGIMpAAIAAh2IspAAIAAhMIN0AAIAAEOIsoAAIAACuIKsAAIAAENg");
    this.shape_4.setTransform(504,61.8);
  
    this.instance_8 = new lib.Tween23("synched",0);
    this.instance_8.parent = this;
    this.instance_8.setTransform(504,61.8);
    this.instance_8._off = true;
  
    this.instance_9 = new lib.Tween24("synched",0);
    this.instance_9.parent = this;
    this.instance_9.setTransform(504,61.8);
    this.instance_9.alpha = 0;
  
    var maskedShapeInstanceList = [this.shape_4,this.instance_8,this.instance_9];
  
    for(var shapedInstanceItr = 0; shapedInstanceItr < maskedShapeInstanceList.length; shapedInstanceItr++) {
      maskedShapeInstanceList[shapedInstanceItr].mask = mask_4;
    }
  
    this.timeline.addTween(cjs.Tween.get({}).to({state:[]}).to({state:[{t:this.shape_4}]},1).to({state:[{t:this.instance_8}]},118).to({state:[{t:this.instance_9}]},15).wait(1));
    this.timeline.addTween(cjs.Tween.get(this.instance_8).wait(119).to({_off:false},0).to({_off:true,alpha:0},15).wait(1));
  
    // D mask (mask)
    var mask_5 = new cjs.Shape();
    mask_5._off = true;
    var mask_5_graphics_1 = new cjs.Graphics().p("AgcAmIAAhLIA5AAIAABLg");
    var mask_5_graphics_2 = new cjs.Graphics().p("Ag7AmIAAhLIB3AAIgDBLg");
    var mask_5_graphics_3 = new cjs.Graphics().p("AhYAmIAAhLICxAAIgFBLg");
    var mask_5_graphics_4 = new cjs.Graphics().p("ABEAmIi+AAIAAhLIC+AAQAcAAAbADIglBIIgSAAg");
    var mask_5_graphics_5 = new cjs.Graphics().p("AAaAiIi9AAIAAhLIC9AAQBKAABAATIg4BAQgngIgrAAg");
    var mask_5_graphics_6 = new cjs.Graphics().p("ABjApQg3gQg+AAIi/AAIAAhKIC/AAQCAgBBjA3IhRAtIgdgJg");
    var mask_5_graphics_7 = new cjs.Graphics().p("ABEAaQg3gQg/AAIi+AAIAAhLIC+AAQCkAAB2BaIAJAIIhSAhQgqgagxgOg");
    var mask_5_graphics_8 = new cjs.Graphics().p("ACiBNQhihNiOAAIi+AAIAAhMIC+AAQC6AAB+B0QATASAQATg");
    var mask_5_graphics_9 = new cjs.Graphics().p("ADMBvIgKgNQgQgVgUgSQhnhdicAAIi+AAIAAhMIC+AAQC6AAB+B0QAtAqAdAyIAHANg");
    var mask_5_graphics_10 = new cjs.Graphics().p("ACSAdQhmhdidAAIi+AAIAAhMIC+AAQC7AAB9B1QBKBEAfBZIhPAHQgZg/g2gxg");
    var mask_5_graphics_11 = new cjs.Graphics().p("ADpCmQgUhfhKhEQhnhdicAAIi+AAIAAhMIC+AAQC6AAB+B1QBhBaAWB+g");
    var mask_5_graphics_12 = new cjs.Graphics().p("ADtCzQgLh6hahSQhnheicAAIi+AAIAAhMIC+AAQC6AAB+B1QB3BuAGCkg");
    var mask_5_graphics_13 = new cjs.Graphics().p("ADtDKIABgLIAAgNQAAiRhmhdQhnheicAAIi+AAIAAhMIC+AAQC6AAB+B1QB9B0AACvQAAATgBASIgCAQg");
    var mask_5_graphics_14 = new cjs.Graphics().p("ADmDeQAHgeABghIAAgOQAAiQhmhdQhnheicgBIi+AAIAAhLIC+AAQC6gBB+B2QB9B0AACuQAAATgBATQgEAqgLAmg");
    var mask_5_graphics_15 = new cjs.Graphics().p("ADOEAIAAgBQAghBAAhRQAAiQhmheQhnheicAAIi+AAIAAhMIC+AAQC6AAB+B1QB9B1AACuQAABqgvBVg");
    var mask_5_graphics_16 = new cjs.Graphics().p("ADODVQAghBAAhRQAAiQhmheQhnheicAAIi+AAIAAhMIC+AAQC6AAB+B1QB9B1AACuQAACihsBxg");
    var mask_5_graphics_17 = new cjs.Graphics().p("ACfEFQBPhYAAh/QAAiQhmheQhnheicAAIi+AAIAAhMIC+AAQC6AAB+B1QB9B1AACuQAACuh9B1QgPAOgPAMg");
    var mask_5_graphics_18 = new cjs.Graphics().p("ABlElQASgNARgPQBmheAAiQQAAiQhmheQhnheicAAIi+AAIAAhMIC+AAQC6AAB+B1QB9B1AACuQAACuh9B1QgpAmgvAag");
    var mask_5_graphics_19 = new cjs.Graphics().p("AAhE8QA5gYAugqQBmheAAiQQAAiPhmheQhnheicgBIi+AAIAAhLIC+AAQC6gBB+B2QB9B0AACuQAACvh9B1QhFBAhXAdg");
    var mask_5_graphics_20 = new cjs.Graphics().p("AgYFGQBbgVBFg/QBmhdAAiQQAAiQhmheQhnheicAAIi+AAIAAhMIC+AAQC6AAB+B1QB9B1AACuQAACuh9B1QhbBVh6AXg");
    var mask_5_graphics_21 = new cjs.Graphics().p("AhYFLIAGAAIAFgBQB+gMBXhQQBmhdAAiQQAAiQhmheQhnheicAAIi+AAIAAhMIC+AAQC6AAB+B1QB9B1AACuQAACuh9B1QhuBmicANIgFABIgGAAg");
    var mask_5_graphics_22 = new cjs.Graphics().p("AieGZIAAhNIAjAAQCcAABnheQBmheAAiQQAAiPhmheQhnheicgBIi+AAIAAhLIC+AAQC6gBB+B2QB9B0AACuQAACvh9B1Qh+B0i6ABg");
    var mask_5_graphics_23 = new cjs.Graphics().p("AjlGZIAAhNIBqAAQCcAABnheQBmheAAiQQAAiPhmheQhnheicgBIi+AAIAAhLIC+AAQC6gBB+B2QB9B0AACuQAACvh9B1Qh+B0i6ABg");
    var mask_5_graphics_24 = new cjs.Graphics().p("Ak5GZIAAhNIAEAAIAAAAIC6AAQCcAABnheQBmheAAiQQAAiPhmheQhnheicgBIi+AAIAAhLIC+AAQC6gBB+B2QB9B0AACuQAACvh9B1Qh+B0i6ABg");
    var mask_5_graphics_25 = new cjs.Graphics().p("AlgGZIAAhNIBSAAIAAAAIC6AAQCcAABmheQBnheAAiQQAAiPhnheQhmheicgBIi/AAIAAhLIC/AAQC6gBB9B2QB+B0AACuQAACvh+B1Qh9B0i6ABg");
    var mask_5_graphics_26 = new cjs.Graphics().p("AlgGZIAAh0IBMAAIAAAnIDAAAQCcAABmheQBnheAAiQQAAiPhnheQhmheicgBIi/AAIAAhLIC/AAQC6gBB9B2QB+B0AACuQAACvh+B1Qh9B0i6ABg");
    var mask_5_graphics_27 = new cjs.Graphics().p("AlgGZIAAilIBMAAIAABYIDAAAQCcAABmheQBnheAAiQQAAiPhnheQhmheicgBIi/AAIAAhLIC/AAQC6gBB9B2QB+B0AACuQAACvh+B1Qh9B0i6ABg");
    var mask_5_graphics_28 = new cjs.Graphics().p("AlgGZIAAjkIBMAAIAACXIDAAAQCcAABmheQBnheAAiQQAAiPhnheQhmheicgBIi/AAIAAhLIC/AAQC6gBB9B2QB+B0AACuQAACvh+B1Qh9B0i6ABg");
    var mask_5_graphics_29 = new cjs.Graphics().p("AlgGZIAAkiIBMAAIAADVIDAAAQCcAABmheQBnheAAiQQAAiPhnheQhmheicgBIi/AAIAAhLIC/AAQC6gBB9B2QB+B0AACuQAACvh+B1Qh9B0i6ABg");
    var mask_5_graphics_30 = new cjs.Graphics().p("AlgGZIAAldIBMAAIAAEQIDAAAQCcAABmheQBnheAAiQQAAiPhnheQhmheicgBIi/AAIAAhLIC/AAQC6gBB9B2QB+B0AACuQAACvh+B1Qh9B0i6ABg");
    var mask_5_graphics_31 = new cjs.Graphics().p("AlgGZIAAmZIBMAAIAAFMIDAAAQCcAABmheQBnheAAiQQAAiPhnheQhmheicgBIi/AAIAAhLIC/AAQC6gBB9B2QB+B0AACuQAACvh+B1Qh9B0i6ABg");
    var mask_5_graphics_32 = new cjs.Graphics().p("AlgGZIAAncIBMAAIAAGPIDAAAQCcAABmheQBnheAAiQQAAiPhnheQhmheicgBIi/AAIAAhLIC/AAQC6gBB9B2QB+B0AACuQAACvh+B1Qh9B0i6ABg");
    var mask_5_graphics_33 = new cjs.Graphics().p("AlgGZIAAogIBMAAIAAHTIDAAAQCcAABmheQBnheAAiQQAAiPhnheQhmheicgBIi/AAIAAhLIC/AAQC6gBB9B2QB+B0AACuQAACvh+B1Qh9B0i6ABg");
    var mask_5_graphics_34 = new cjs.Graphics().p("AlgGZIAApiIBMAAIAAIVIDAAAQCcAABmheQBnheAAiQQAAiPhnheQhmheicgBIi/AAIAAhLIC/AAQC6gBB9B2QB+B0AACuQAACvh+B1Qh9B0i6ABg");
    var mask_5_graphics_35 = new cjs.Graphics().p("AlgGZIAAqeIBMAAIAAJRIDAAAQCcAABmheQBnheAAiQQAAiPhnheQhmheicgBIi/AAIAAhLIC/AAQC6gBB9B2QB+B0AACuQAACvh+B1Qh9B0i6ABg");
    var mask_5_graphics_36 = new cjs.Graphics().p("AlgGZIAArkIBMAAIAAKXIDAAAQCcAABmheQBnheAAiQQAAiPhnheQhmheicgBIi/AAIAAhLIC/AAQC6gBB9B2QB+B0AACuQAACvh+B1Qh9B0i6ABg");
    var mask_5_graphics_37 = new cjs.Graphics().p("AlgGZIAAswIEMAAQC6gBB9B2QB+B0AACuQAACvh+B1Qh9B0i6ABgAkUFMIDAAAQCcAABmheQBnheAAiQQAAiPhnheQhmheicgBIjAAAg");
    var mask_5_graphics_38 = new cjs.Graphics().p("AlgIBIAAswIEMAAQC6AAB9B1QB+B1AACuQAACuh+B1Qh9B1i6AAgAkUG1IDAAAQCcAABmheQBnheAAiQQAAiQhnheQhmheicAAIjAAAgAlgm0IAAhMIA2AAIAABMg");
    var mask_5_graphics_39 = new cjs.Graphics().p("AlgIBIAAswIEMAAQC6AAB9B1QB+B1AACuQAACuh+B1Qh9B1i6AAgAkUG1IDAAAQCcAABmheQBnheAAiQQAAiQhnheQhmheicAAIjAAAgAlgm0IAAhMIB6AAIAABMg");
    var mask_5_graphics_40 = new cjs.Graphics().p("AlgIBIAAswIEMAAQC6AAB9B1QB+B1AACuQAACuh+B1Qh9B1i6AAgAkUG1IDAAAQCcAABmheQBnheAAiQQAAiQhnheQhmheicAAIjAAAgAlgm0IAAhMIDSAAIAABMg");
    var mask_5_graphics_41 = new cjs.Graphics().p("AlgIBIAAswIEMAAQC6AAB9B1QB+B1AACuQAACuh+B1Qh9B1i6AAgAkUG1IDAAAQCcAABmheQBnheAAiQQAAiQhnheQhmheicAAIjAAAgAlgm0IAAhMIEEAAIAABMg");
    var mask_5_graphics_42 = new cjs.Graphics().p("AlgIBIAAswIEMAAQC6AAB9B1QB+B1AACuQAACuh+B1Qh9B1i6AAgAkUG1IDAAAQCcAABmheQBnheAAiQQAAiQhnheQhmheicAAIjAAAgAhUm0IkMAAIAAhMIEMAAIAdAAIAABNIgdgBg");
    var mask_5_graphics_43 = new cjs.Graphics().p("AlgIBIAAswIEMAAQC6AAB9B1QB+B1AACuQAACuh+B1Qh9B1i6AAgAkUG1IDAAAQCcAABmheQBnheAAiQQAAiQhnheQhmheicAAIjAAAgAhUm0IkMAAIAAhMIEMAAQA2AAAyAHIAABNQgygIg2AAg");
    var mask_5_graphics_44 = new cjs.Graphics().p("AlgIBIAAswIEMAAQC6AAB9B1QB+B1AACuQAACuh+B1Qh9B1i6AAgAkUG1IDAAAQCcAABmheQBnheAAiQQAAiQhnheQhmheicAAIjAAAgAApmoQg8gMhBAAIkMAAIAAhMIEMAAQBGAABAAMIAABOIgJgCg");
    var mask_5_graphics_45 = new cjs.Graphics().p("AlgIBIAAswIEMAAQC6AAB9B1QB+B1AACuQAACuh+B1Qh9B1i6AAgAkUG1IDAAAQCcAABmheQBnheAAiQQAAiQhnheQhmheicAAIjAAAgAhUm0IkMAAIAAhMIEMAAQBiAABYAXIAABQQhXgbhjAAg");
    var mask_5_graphics_46 = new cjs.Graphics().p("AlgIBIAAswIEMAAQC6AAB9B1QB+B1AACuQAACuh+B1Qh9B1i6AAgAkUG1IDAAAQCcAABmheQBnheAAiQQAAiQhnheQhmheicAAIjAAAgAhUm0IkMAAIAAhMIEMAAQB9AABtAlIAABTQhrgsh/AAg");
    var mask_5_graphics_47 = new cjs.Graphics().p("AlgIBIAAswIEMAAQC6AAB9B1QB+B1AACuQAACuh+B1Qh9B1i6AAgAkUG1IDAAAQCcAABmheQBnheAAiQQAAiQhnheQhmheicAAIjAAAgAhUm0IkMAAIAAhMIEMAAQCiAACFA+IgfBHQh3g5iRAAg");
    var mask_5_graphics_48 = new cjs.Graphics().p("AlgIBIAAswIEMAAQC6AAB9B1QB+B1AACuQAACuh+B1Qh9B1i6AAgAkUG1IDAAAQCcAABmheQBnheAAiQQAAiQhnheQhmheicAAIjAAAgAhUm0IkMAAIAAhMIEMAAQDBAACYBZIAbARIhqAcQh3g6iTAAg");
    var mask_5_graphics_49 = new cjs.Graphics().p("AlgIBIAAswIEMAAQC6AAB9B1QB+B1AACuQAACuh+B1Qh9B1i6AAgAkUG1IDAAAQCcAABmheQBnheAAiQQAAiQhnheQhmheicAAIjAAAgAC8l3Qh6g9iWAAIkMAAIAAhMIEMAAQDwAACvCJg");
    var mask_5_graphics_50 = new cjs.Graphics().p("AloIBIAAswIEMAAQC6AAB9B1QB+B1AACuQAACuh+B1Qh9B1i6AAgAkcG1IDAAAQCcAABmheQBnheAAiQQAAiQhnheQhmheicAAIjAAAgADrlXQiNhdi6AAIkMAAIAAhMIEMAAQEGAAC4CjIAHAGg");
    var mask_5_graphics_51 = new cjs.Graphics().p("Al8IBIAAswIEMAAQC6AAB+B1QB9B1AACuQAACuh9B1Qh+B1i6AAgAkvG1IC/AAQCcAABnheQBmheAAiQQAAiQhmheQhnheicAAIi/AAgAENkwIgFgEQidiAjbAAIkMAAIAAhMIEMAAQEPAAC8CuIAeAeIAEAEg");
    var mask_5_graphics_52 = new cjs.Graphics().p("AmJIBIAAswIEMAAQC6AAB+B1QB9B1AACuQAACuh9B1Qh+B1i6AAgAk8G1IC/AAQCcAABnheQBmheAAiQQAAiQhmheQhnheicAAIi/AAgAEhkTIgJgIQimiZjvAAIkMAAIAAhMIEMAAQEPAAC8CuQAhAfAbAgg");
    var mask_5_graphics_53 = new cjs.Graphics().p("AmYIBIAAswIELAAQC7AAB9B1QB+B1AACuQAACuh+B1Qh9B1i7AAgAlMG1IC/AAQCdAABmheQBnheAAiQQAAiQhnheQhmheidAAIi/AAgAE4jqQgWgZgagYQiliZjwAAIkLAAIAAhMIELAAQEQAAC8CuQA0AxAmA3g");
    var mask_5_graphics_54 = new cjs.Graphics().p("AmsIBIAAswIEMAAQC6AAB+B1QB9B1AACuQAACuh9B1Qh+B1i6AAgAlfG1IC/AAQCdAABmheQBmheAAiQQAAiQhmheQhmheidAAIi/AAgAFVinQgmg+g6g2QimiZjvAAIkMAAIAAhMIEMAAQEPAAC8CuQBUBOAuBdg");
    var mask_5_graphics_55 = new cjs.Graphics().p("Am0IBIAAswIEMAAQC6AAB+B1QB+B1AACuQAACuh+B1Qh+B1i6AAgAlnG1IC/AAQCdAABmheQBmheAAiQQAAiQhmheQhmheidAAIi/AAgAFgiFQgphShKhEQimiZjvAAIkMAAIAAhMIEMAAQEPAAC8CuQBjBcAuBxg");
    var mask_5_graphics_56 = new cjs.Graphics().p("Am9IBIAAswIEMAAQC6AAB+B1QB9B1AACuQAACuh9B1Qh+B1i6AAgAlwG1IC/AAQCdAABmheQBmheAAiQQAAiQhmheQhmheidAAIi/AAgAFthTIgCgFQgphthehWQimiZjvAAIkMAAIAAhMIEMAAQEPAAC8CuQB1BtAsCLIABACIACAFg");
    var mask_5_graphics_57 = new cjs.Graphics().p("AnDIBIAAswIEMAAQC6AAB+B1QB+B1AACuQAACuh+B1Qh+B1i6AAgAl2G1IC/AAQCdAABmheQBmheAAiQQAAiQhmheQhmheidAAIi/AAgAF1gkQgjiNh0hqQimiZjvAAIkMAAIAAhMIEMAAQEPAAC8CuQCLCBAlCtg");
    var mask_5_graphics_58 = new cjs.Graphics().p("AnGIBIAAswIEMAAQC6AAB+B1QB9B1AACuQAACuh9B1Qh+B1i6AAgAl5G1IC/AAQCdAABmheQBmheAAiQQAAiQhmheQhmheidAAIi/AAgAF6ADIgCgJQgdifiAh2QimiZjvAAIkMAAIAAhMIEMAAQEPAAC8CuQCXCNAeC/IABAJg");
    var mask_5_graphics_59 = new cjs.Graphics().p("AnIIBIAAswIEMAAQC7AAB9B1QB9B1AACuQAACuh9B1Qh9B1i7AAgAl7G1IC/AAQCdAABmheQBmheAAiQQAAiQhmheQhmheidAAIi/AAgAF8AjQgSi5iRiFQimiZjvAAIkMAAIAAhMIEMAAQEPAAC8CuQCoCcASDZg");
    var mask_5_graphics_60 = new cjs.Graphics().p("AnJIBIAAswIEMAAQC7AAB9B1QB9B1AACuQAACuh9B1Qh9B1i7AAgAl8G1IC/AAQCdAABmheQBmheAAiQQAAiQhmheQhmheidAAIi/AAgAF+BIIgBgOIgBgNQgPi/iViJQimiZjvAAIkMAAIAAhMIEMAAQEPAAC8CuQCwCkALDoIABAOg");
    var mask_5_graphics_61 = new cjs.Graphics().p("AnJIBIAAswIEMAAQC7AAB9B1QB9B1AACuQAACuh9B1Qh9B1i7AAgAl8G1IC/AAQCdAABmheQBmheAAiQQAAiQhmheQhmheidAAIi/AAgAF+BiIAAgJIAAgEIgCgoQgPi/iViJQimiZjvAAIkMAAIAAhMIEMAAQEPAAC8CuQC6CtACD6IAAAEIAAAJg");
    var mask_5_graphics_62 = new cjs.Graphics().p("AnJIBIAAswIEMAAQC7AAB9B1QB9B1AACuQAACuh9B1Qh9B1i7AAgAl8G1IC/AAQCdAABmheQBmheAAiQQAAiQhmheQhmheidAAIi/AAgAF7CQQADgbAAgcQAAgWgCgWQgPi/iViJQimiZjvAAIkMAAIAAhMIEMAAQEPAAC8CuQC8CvAAD8QAAAcgCAbg");
    var mask_5_graphics_63 = new cjs.Graphics().p("AnJIBIAAswIEMAAQC7AAB9B1QB9B1AACuQAACuh9B1Qh9B1i7AAgAl8G1IC/AAQCdAABmheQBmheAAiQQAAiQhmheQhmheidAAIi/AAgAF2DKIAAgWQAIgsAAgvQAAgWgCgWQgPi/iViJQimiZjvAAIkMAAIAAhMIEMAAQEPAAC8CuQC8CvAAD8QAAA7gKA2g");
    var mask_5_graphics_64 = new cjs.Graphics().p("AnJIBIAAswIEMAAQC7AAB9B1QB9B1AACuQAACuh9B1Qh9B1i7AAgAl8G1IC/AAQCdAABmheQBmheAAiQQAAiQhmheQhmheidAAIi/AAgAF2C5IAAgFQAIgsAAgvQAAgWgCgWQgPi/iViJQimiZjvAAIkMAAIAAhMIEMAAQEPAAC8CuQC8CvAAD8QAABUgVBLg");
    var mask_5_graphics_65 = new cjs.Graphics().p("AnJIBIAAswIEMAAQC7AAB9B1QB9B1AACuQAACuh9B1Qh9B1i7AAgAl8G1IC/AAQCdAABmheQBmheAAiQQAAiQhmheQhmheidAAIi/AAgAF2DYIAAgkQAIgsAAgvQAAgWgCgWQgPi/iViJQimiZjvAAIkMAAIAAhMIEMAAQEPAAC8CuQC8CvAAD8QAABhgbBVQgJAbgMAbg");
    var mask_5_graphics_66 = new cjs.Graphics().p("AnJIBIAAswIEMAAQC7AAB9B1QB9B1AACuQAACuh9B1Qh9B1i7AAgAl8G1IC/AAQCdAABmheQBmheAAiQQAAiQhmheQhmheidAAIi/AAgAF2C0QAIgsAAgvQAAgWgCgWQgPi/iViJQimiZjvAAIkMAAIAAhMIEMAAQEPAAC8CuQC8CvAAD8QAACphUCHg");
    var mask_5_graphics_67 = new cjs.Graphics().p("AnJIBIAAswIEMAAQC7AAB9B1QB9B1AACuQAACuh9B1Qh9B1i7AAgAl8G1IC/AAQCdAABmheQBmheAAiQQAAiQhmheQhmheidAAIi/AAgAFkD8QAahMAAhXQAAgWgCgWQgPi/iViJQimiZjvAAIkMAAIAAhMIEMAAQEPAAC8CuQC8CvAAD8QAACzheCMIgIAMg");
    var mask_5_graphics_68 = new cjs.Graphics().p("AnJIBIAAswIEMAAQC7AAB9B1QB9B1AACuQAACuh9B1Qh9B1i7AAgAl8G1IC/AAQCdAABmheQBmheAAiQQAAiQhmheQhmheidAAIi/AAgAFGFCQA4hpAAiAQAAgWgCgWQgPi/iViJQimiZjvAAIkMAAIAAhMIEMAAQEPAAC8CuQC8CvAAD8QAADViECcg");
    var mask_5_graphics_69 = new cjs.Graphics().p("AnJIBIAAswIEMAAQC7AAB9B1QB9B1AACuQAACuh9B1Qh9B1i7AAgAl8G1IC/AAQCdAABmheQBmheAAiQQAAiQhmheQhmheidAAIi/AAgAE1FlIgDAAIAEgGQBIh0AAiSQAAgWgCgWQgPi/iViJQimiZjvAAIkMAAIAAhMIEMAAQEPAAC8CuQC8CvAAD8QAADeiQCiIgFAFg");
    var mask_5_graphics_70 = new cjs.Graphics().p("AEHGZIACgCQB1iKAAi5QAAgXgCgWQgPi/iViJQimiYjvAAIkMAAIAAhNIEMAAQEPAAC8CvQC8CvAAD8QAAD8i5CuIgDACIgHAGgAnJH8IAAswIEMAAQC7AAB9B1QB9B1AACuQAACuh9B1Qh9B1i7AAgAl8GwIC/AAQCdAABmhfQBmhdAAiQQAAiQhmheQhmheidAAIi/AAg");
    var mask_5_graphics_71 = new cjs.Graphics().p("ADwGpIADgEQCLiRAAjKQAAgXgCgWQgPi/iViJQimiYjvAAIkMAAIAAhNIEMAAQEPAAC8CvQC8CuAAD9QAAD5i2CtIgGAGIgeAbgAnJHyIAAswIEMAAQC7AAB9B1QB9B1AACtQAACvh9B1Qh9B1i7AAgAl8GlIC/AAQCdAABmheQBmhdAAiRQAAiPhmheQhmheidAAIi/AAg");
    var mask_5_graphics_72 = new cjs.Graphics().p("ADKG9IAOgLQCmiZAAjeQAAgWgCgWQgPi/iViJQimiYjvAAIkMAAIAAhNIEMAAQEPAAC8CvQC8CuAAD8QAAD+i8CuQggAfgkAZgAnJHkIAAswIEMAAQC7gBB9B2QB9B0AACuQAACvh9B1Qh9B0i7ABgAl8GXIC/AAQCdAABmheQBmheAAiQQAAiPhmheQhmheidgBIi/AAg");
    var mask_5_graphics_73 = new cjs.Graphics().p("ACzHJQATgPASgQQCmiZAAjeQAAgWgCgWQgPi/iViJQimiYjvAAIkMAAIAAhNIEMAAQEPAAC8CvQC8CuAAD8QAAD+i8CuQgTATgVAQQgZAUgaARgAnJHcIAAswIEMAAQC7gBB9B2QB9B0AACuQAACvh9B1Qh9B0i7ABgAl8GPIC/AAQCdAABmheQBmheAAiQQAAiPhmheQhmheidgBIi/AAg");
    var mask_5_graphics_74 = new cjs.Graphics().p("ACHHbQAqgbAngjQCmiZAAjdQAAgXgCgVQgPjAiViJQimiYjvAAIkMAAIAAhNIEMAAQEPAAC8CvQC8CuAAD9QAAD9i8CvQg9A5hGAmIgEACgAnJHPIAAswIEMAAQC7AAB9B1QB9B1AACtQAACvh9B1Qh9B1i7AAgAl8GCIC/AAQCdAABmheQBmhdAAiRQAAiPhmheQhmheidAAIi/AAg");
    var mask_5_graphics_75 = new cjs.Graphics().p("ABeHpQBBgiA5g0QCmiZAAjdQAAgXgCgVQgPjAiViJQimiYjvAAIkMAAIAAhNIEMAAQEPAAC8CvQC8CuAAD9QAAD9i8CvQhQBKhgArgAnJHFIAAswIEMAAQC7AAB9B1QB9B1AACtQAACvh9B1Qh9B1i7AAgAl8F4IC/AAQCdAABmheQBmhdAAiRQAAiPhmheQhmheidAAIi/AAg");
    var mask_5_graphics_76 = new cjs.Graphics().p("AA7HzQBVgmBIhCQCmiYAAjeQAAgVgCgWQgPjAiViJQimiZjvAAIkMAAIAAhMIEMAAQEPAAC8CuQC8CvAAD8QAAD+i8CuQhfBZh0ArgAnJG9IAAswIEMAAQC7AAB9B1QB9B1AACuQAACuh9B1Qh9B1i7AAgAl8FxIC/AAQCdAABmheQBmheAAiQQAAiQhmheQhmheidAAIi/AAg");
    var mask_5_graphics_77 = new cjs.Graphics().p("AgJIBQB9glBkhbQCmiZAAjdQAAgWgCgWQgPjAiViJQimiYjvAAIkMAAIAAhNIEMAAQEPAAC8CvQC8CuAAD9QAAD9i8CvQh6BxidAogAnJGzIAAswIEMAAQC7AAB9B1QB9B1AACtQAACvh9B1Qh9B1i7AAgAl8FmIC/AAQCdAABmheQBmhdAAiRQAAiPhmheQhmheidAAIi/AAg");
    var mask_5_graphics_78 = new cjs.Graphics().p("AhWIKQCtgaCBh3QCmiYAAjeQAAgVgCgWQgPjAiViJQimiZjvAAIkMAAIAAhMIEMAAQEPAAC8CuQC8CvAAD8QAAD+i8CuQiXCNjNAbgAnJGrIAAswIEMAAQC7AAB9B1QB9B1AACuQAACuh9B1Qh9B1i7AAgAl8FfIC/AAQCdAABmheQBmheAAiQQAAiQhmheQhmheidAAIi/AAg");
    var mask_5_graphics_79 = new cjs.Graphics().p("AigIOQDcgJCciPQCmiYAAjeQAAgVgCgWQgPjAiViJQimiZjvAAIkMAAIAAhMIEMAAQEPAAC8CuQC8CvAAD8QAAD+i8CuQiyClj8AJgAnJGoIAAswIEMAAQC7AAB9B1QB9B1AACuQAACuh9B1Qh9B1i7AAgAl8FcIC/AAQCdAABmheQBmheAAiQQAAiQhmheQhmheidAAIi/AAg");
    var mask_5_graphics_80 = new cjs.Graphics().p("AjtJbIAAhNIAwAAQDvAACmiYQCmiYAAjeQAAgWgCgWQgPjAiViJQimiYjvAAIkMAAIAAhNIEMAAQEPAAC8CvQC8CvAAD8QAAD9i8CvQi8CvkPAAgAnJGoIAAswIEMAAQC7AAB9B1QB9B1AACuQAACuh9B1Qh9B1i7AAgAl8FcIC/AAQCdAABmhfQBmhdAAiQQAAiQhmheQhmheidAAIi/AAg");
    var mask_5_graphics_81 = new cjs.Graphics().p("AkzJbIAAhNIB2AAQDvAACmiYQCmiYAAjeQAAgWgCgWQgPjAiViJQimiYjvAAIkMAAIAAhNIEMAAQEPAAC8CvQC8CvAAD8QAAD9i8CvQi8CvkPAAgAnJGoIAAswIEMAAQC7AAB9B1QB9B1AACuQAACuh9B1Qh9B1i7AAgAl8FcIC/AAQCdAABmhfQBmhdAAiQQAAiQhmheQhmheidAAIi/AAg");
    var mask_5_graphics_82 = new cjs.Graphics().p("AmAJbIAAhNIDDAAQDvAACmiYQCmiYAAjeQAAgWgCgWQgPjAiViJQimiYjvAAIkMAAIAAhNIEMAAQEPAAC8CvQC8CvAAD8QAAD9i8CvQi8CvkPAAgAnJGoIAAswIEMAAQC7AAB9B1QB9B1AACuQAACuh9B1Qh9B1i7AAgAl8FcIC/AAQCdAABmhfQBmhdAAiQQAAiQhmheQhmheidAAIi/AAg");
    var mask_5_graphics_83 = new cjs.Graphics().p("AnJJbIAAhNIEMAAQDvAACmiYQCmiYAAjeQAAgWgCgWQgPjAiViJQimiYjvAAIkMAAIAAhNIEMAAQEPAAC8CvQC8CvAAD8QAAD9i8CvQi8CvkPAAgAnJGoIAAswIEMAAQC7AAB9B1QB9B1AACuQAACuh9B1Qh9B1i7AAgAl8FcIC/AAQCdAABmhfQBmhdAAiQQAAiQhmheQhmheidAAIi/AAg");
    var mask_5_graphics_119 = new cjs.Graphics().p("AnJJbIAAhNIEMAAQDvAACmiYQCmiYAAjeQAAgWgCgWQgPjAiViJQimiYjvAAIkMAAIAAhNIEMAAQEPAAC8CvQC8CvAAD8QAAD9i8CvQi8CvkPAAgAnJGoIAAswIEMAAQC7AAB9B1QB9B1AACuQAACuh9B1Qh9B1i7AAgAl8FcIC/AAQCdAABmhfQBmhdAAiQQAAiQhmheQhmheidAAIi/AAg");
    var mask_5_graphics_120 = new cjs.Graphics().p("AnJJbIAAhNIEMAAQDvAACmiYQCmiYAAjeQAAgWgCgWQgPjAiViJQimiYjvAAIkMAAIAAhNIEMAAQEPAAC8CvQC8CvAAD8QAAD9i8CvQi8CvkPAAgAnJGoIAAswIEMAAQC7AAB9B1QB9B1AACuQAACuh9B1Qh9B1i7AAgAl8FcIC/AAQCdAABmhfQBmhdAAiQQAAiQhmheQhmheidAAIi/AAg");
    var mask_5_graphics_121 = new cjs.Graphics().p("AnJJbIAAhNIEMAAQDvAACmiYQCmiYAAjeQAAgWgCgWQgPjAiViJQimiYjvAAIkMAAIAAhNIEMAAQEPAAC8CvQC8CvAAD8QAAD9i8CvQi8CvkPAAgAnJGoIAAswIEMAAQC7AAB9B1QB9B1AACuQAACuh9B1Qh9B1i7AAgAl8FcIC/AAQCdAABmhfQBmhdAAiQQAAiQhmheQhmheidAAIi/AAg");
    var mask_5_graphics_122 = new cjs.Graphics().p("AnJJbIAAhNIEMAAQDvAACmiYQCmiYAAjeQAAgWgCgWQgPjAiViJQimiYjvAAIkMAAIAAhNIEMAAQEPAAC8CvQC8CvAAD8QAAD9i8CvQi8CvkPAAgAnJGoIAAswIEMAAQC7AAB9B1QB9B1AACuQAACuh9B1Qh9B1i7AAgAl8FcIC/AAQCdAABmhfQBmhdAAiQQAAiQhmheQhmheidAAIi/AAg");
    var mask_5_graphics_123 = new cjs.Graphics().p("AnJJbIAAhNIEMAAQDvAACmiYQCmiYAAjeQAAgWgCgWQgPjAiViJQimiYjvAAIkMAAIAAhNIEMAAQEPAAC8CvQC8CvAAD8QAAD9i8CvQi8CvkPAAgAnJGoIAAswIEMAAQC7AAB9B1QB9B1AACuQAACuh9B1Qh9B1i7AAgAl8FcIC/AAQCdAABmhfQBmhdAAiQQAAiQhmheQhmheidAAIi/AAg");
    var mask_5_graphics_124 = new cjs.Graphics().p("AnJJbIAAhNIEMAAQDvAACmiYQCmiYAAjeQAAgWgCgWQgPjAiViJQimiYjvAAIkMAAIAAhNIEMAAQEPAAC8CvQC8CvAAD8QAAD9i8CvQi8CvkPAAgAnJGoIAAswIEMAAQC7AAB9B1QB9B1AACuQAACuh9B1Qh9B1i7AAgAl8FcIC/AAQCdAABmhfQBmhdAAiQQAAiQhmheQhmheidAAIi/AAg");
    var mask_5_graphics_125 = new cjs.Graphics().p("AnJJbIAAhNIEMAAQDvAACmiYQCmiYAAjeQAAgWgCgWQgPjAiViJQimiYjvAAIkMAAIAAhNIEMAAQEPAAC8CvQC8CvAAD8QAAD9i8CvQi8CvkPAAgAnJGoIAAswIEMAAQC7AAB9B1QB9B1AACuQAACuh9B1Qh9B1i7AAgAl8FcIC/AAQCdAABmhfQBmhdAAiQQAAiQhmheQhmheidAAIi/AAg");
    var mask_5_graphics_126 = new cjs.Graphics().p("AnJJbIAAhNIEMAAQDvAACmiYQCmiYAAjeQAAgWgCgWQgPjAiViJQimiYjvAAIkMAAIAAhNIEMAAQEPAAC8CvQC8CvAAD8QAAD9i8CvQi8CvkPAAgAnJGoIAAswIEMAAQC7AAB9B1QB9B1AACuQAACuh9B1Qh9B1i7AAgAl8FcIC/AAQCdAABmhfQBmhdAAiQQAAiQhmheQhmheidAAIi/AAg");
    var mask_5_graphics_127 = new cjs.Graphics().p("AnJJbIAAhNIEMAAQDvAACmiYQCmiYAAjeQAAgWgCgWQgPjAiViJQimiYjvAAIkMAAIAAhNIEMAAQEPAAC8CvQC8CvAAD8QAAD9i8CvQi8CvkPAAgAnJGoIAAswIEMAAQC7AAB9B1QB9B1AACuQAACuh9B1Qh9B1i7AAgAl8FcIC/AAQCdAABmhfQBmhdAAiQQAAiQhmheQhmheidAAIi/AAg");
    var mask_5_graphics_128 = new cjs.Graphics().p("AnJJbIAAhNIEMAAQDvAACmiYQCmiYAAjeQAAgWgCgWQgPjAiViJQimiYjvAAIkMAAIAAhNIEMAAQEPAAC8CvQC8CvAAD8QAAD9i8CvQi8CvkPAAgAnJGoIAAswIEMAAQC7AAB9B1QB9B1AACuQAACuh9B1Qh9B1i7AAgAl8FcIC/AAQCdAABmhfQBmhdAAiQQAAiQhmheQhmheidAAIi/AAg");
    var mask_5_graphics_129 = new cjs.Graphics().p("AnJJbIAAhNIEMAAQDvAACmiYQCmiYAAjeQAAgWgCgWQgPjAiViJQimiYjvAAIkMAAIAAhNIEMAAQEPAAC8CvQC8CvAAD8QAAD9i8CvQi8CvkPAAgAnJGoIAAswIEMAAQC7AAB9B1QB9B1AACuQAACuh9B1Qh9B1i7AAgAl8FcIC/AAQCdAABmhfQBmhdAAiQQAAiQhmheQhmheidAAIi/AAg");
    var mask_5_graphics_130 = new cjs.Graphics().p("AnJJbIAAhNIEMAAQDvAACmiYQCmiYAAjeQAAgWgCgWQgPjAiViJQimiYjvAAIkMAAIAAhNIEMAAQEPAAC8CvQC8CvAAD8QAAD9i8CvQi8CvkPAAgAnJGoIAAswIEMAAQC7AAB9B1QB9B1AACuQAACuh9B1Qh9B1i7AAgAl8FcIC/AAQCdAABmhfQBmhdAAiQQAAiQhmheQhmheidAAIi/AAg");
    var mask_5_graphics_131 = new cjs.Graphics().p("AnJJbIAAhNIEMAAQDvAACmiYQCmiYAAjeQAAgWgCgWQgPjAiViJQimiYjvAAIkMAAIAAhNIEMAAQEPAAC8CvQC8CvAAD8QAAD9i8CvQi8CvkPAAgAnJGoIAAswIEMAAQC7AAB9B1QB9B1AACuQAACuh9B1Qh9B1i7AAgAl8FcIC/AAQCdAABmhfQBmhdAAiQQAAiQhmheQhmheidAAIi/AAg");
    var mask_5_graphics_132 = new cjs.Graphics().p("AnJJbIAAhNIEMAAQDvAACmiYQCmiYAAjeQAAgWgCgWQgPjAiViJQimiYjvAAIkMAAIAAhNIEMAAQEPAAC8CvQC8CvAAD8QAAD9i8CvQi8CvkPAAgAnJGoIAAswIEMAAQC7AAB9B1QB9B1AACuQAACuh9B1Qh9B1i7AAgAl8FcIC/AAQCdAABmhfQBmhdAAiQQAAiQhmheQhmheidAAIi/AAg");
    var mask_5_graphics_133 = new cjs.Graphics().p("AnJJbIAAhNIEMAAQDvAACmiYQCmiYAAjeQAAgWgCgWQgPjAiViJQimiYjvAAIkMAAIAAhNIEMAAQEPAAC8CvQC8CvAAD8QAAD9i8CvQi8CvkPAAgAnJGoIAAswIEMAAQC7AAB9B1QB9B1AACuQAACuh9B1Qh9B1i7AAgAl8FcIC/AAQCdAABmhfQBmhdAAiQQAAiQhmheQhmheidAAIi/AAg");
    var mask_5_graphics_134 = new cjs.Graphics().p("AnJJbIAAhNIEMAAQDvAACmiYQCmiYAAjeQAAgWgCgWQgPjAiViJQimiYjvAAIkMAAIAAhNIEMAAQEPAAC8CvQC8CvAAD8QAAD9i8CvQi8CvkPAAgAnJGoIAAswIEMAAQC7AAB9B1QB9B1AACuQAACuh9B1Qh9B1i7AAgAl8FcIC/AAQCdAABmhfQBmhdAAiQQAAiQhmheQhmheidAAIi/AAg");
  
    this.timeline.addTween(cjs.Tween.get(mask_5).to({graphics:null,x:0,y:0}).wait(1).to({graphics:mask_5_graphics_1,x:364.7,y:24.8}).wait(1).to({graphics:mask_5_graphics_2,x:367.8,y:24.8}).wait(1).to({graphics:mask_5_graphics_3,x:370.7,y:24.8}).wait(1).to({graphics:mask_5_graphics_4,x:374,y:24.8}).wait(1).to({graphics:mask_5_graphics_5,x:378.2,y:25.1}).wait(1).to({graphics:mask_5_graphics_6,x:382.7,y:26}).wait(1).to({graphics:mask_5_graphics_7,x:385.8,y:27.5}).wait(1).to({graphics:mask_5_graphics_8,x:388.7,y:28.6}).wait(1).to({graphics:mask_5_graphics_9,x:391,y:32.1}).wait(1).to({graphics:mask_5_graphics_10,x:392.1,y:35.1}).wait(1).to({graphics:mask_5_graphics_11,x:392.9,y:37.7}).wait(1).to({graphics:mask_5_graphics_12,x:393.2,y:40.5}).wait(1).to({graphics:mask_5_graphics_13,x:393.2,y:44}).wait(1).to({graphics:mask_5_graphics_14,x:393.2,y:47.3}).wait(1).to({graphics:mask_5_graphics_15,x:393.2,y:51}).wait(1).to({graphics:mask_5_graphics_16,x:393.2,y:55.1}).wait(1).to({graphics:mask_5_graphics_17,x:393.2,y:57.2}).wait(1).to({graphics:mask_5_graphics_18,x:393.2,y:59.1}).wait(1).to({graphics:mask_5_graphics_19,x:393.2,y:60.6}).wait(1).to({graphics:mask_5_graphics_20,x:393.2,y:61.4}).wait(1).to({graphics:mask_5_graphics_21,x:393.2,y:61.8}).wait(1).to({graphics:mask_5_graphics_22,x:393.2,y:61.8}).wait(1).to({graphics:mask_5_graphics_23,x:393.2,y:61.8}).wait(1).to({graphics:mask_5_graphics_24,x:393.2,y:61.8}).wait(1).to({graphics:mask_5_graphics_25,x:389.3,y:61.8}).wait(1).to({graphics:mask_5_graphics_26,x:389.3,y:61.8}).wait(1).to({graphics:mask_5_graphics_27,x:389.3,y:61.8}).wait(1).to({graphics:mask_5_graphics_28,x:389.3,y:61.8}).wait(1).to({graphics:mask_5_graphics_29,x:389.3,y:61.8}).wait(1).to({graphics:mask_5_graphics_30,x:389.3,y:61.8}).wait(1).to({graphics:mask_5_graphics_31,x:389.3,y:61.8}).wait(1).to({graphics:mask_5_graphics_32,x:389.3,y:61.8}).wait(1).to({graphics:mask_5_graphics_33,x:389.3,y:61.8}).wait(1).to({graphics:mask_5_graphics_34,x:389.3,y:61.8}).wait(1).to({graphics:mask_5_graphics_35,x:389.3,y:61.8}).wait(1).to({graphics:mask_5_graphics_36,x:389.3,y:61.8}).wait(1).to({graphics:mask_5_graphics_37,x:389.3,y:61.8}).wait(1).to({graphics:mask_5_graphics_38,x:389.3,y:51.3}).wait(1).to({graphics:mask_5_graphics_39,x:389.3,y:51.3}).wait(1).to({graphics:mask_5_graphics_40,x:389.3,y:51.3}).wait(1).to({graphics:mask_5_graphics_41,x:389.3,y:51.3}).wait(1).to({graphics:mask_5_graphics_42,x:389.3,y:51.3}).wait(1).to({graphics:mask_5_graphics_43,x:389.3,y:51.3}).wait(1).to({graphics:mask_5_graphics_44,x:389.3,y:51.3}).wait(1).to({graphics:mask_5_graphics_45,x:389.3,y:51.3}).wait(1).to({graphics:mask_5_graphics_46,x:389.3,y:51.3}).wait(1).to({graphics:mask_5_graphics_47,x:389.3,y:51.3}).wait(1).to({graphics:mask_5_graphics_48,x:389.3,y:51.3}).wait(1).to({graphics:mask_5_graphics_49,x:389.3,y:51.3}).wait(1).to({graphics:mask_5_graphics_50,x:390.1,y:51.3}).wait(1).to({graphics:mask_5_graphics_51,x:392.1,y:51.3}).wait(1).to({graphics:mask_5_graphics_52,x:393.4,y:51.3}).wait(1).to({graphics:mask_5_graphics_53,x:394.9,y:51.3}).wait(1).to({graphics:mask_5_graphics_54,x:396.9,y:51.3}).wait(1).to({graphics:mask_5_graphics_55,x:397.7,y:51.3}).wait(1).to({graphics:mask_5_graphics_56,x:398.6,y:51.3}).wait(1).to({graphics:mask_5_graphics_57,x:399.2,y:51.3}).wait(1).to({graphics:mask_5_graphics_58,x:399.5,y:51.3}).wait(1).to({graphics:mask_5_graphics_59,x:399.7,y:51.3}).wait(1).to({graphics:mask_5_graphics_60,x:399.8,y:51.3}).wait(1).to({graphics:mask_5_graphics_61,x:399.8,y:51.3}).wait(1).to({graphics:mask_5_graphics_62,x:399.8,y:51.3}).wait(1).to({graphics:mask_5_graphics_63,x:399.8,y:51.3}).wait(1).to({graphics:mask_5_graphics_64,x:399.8,y:51.3}).wait(1).to({graphics:mask_5_graphics_65,x:399.8,y:51.3}).wait(1).to({graphics:mask_5_graphics_66,x:399.8,y:51.3}).wait(1).to({graphics:mask_5_graphics_67,x:399.8,y:51.3}).wait(1).to({graphics:mask_5_graphics_68,x:399.8,y:51.3}).wait(1).to({graphics:mask_5_graphics_69,x:399.8,y:51.3}).wait(1).to({graphics:mask_5_graphics_70,x:399.8,y:51.9}).wait(1).to({graphics:mask_5_graphics_71,x:399.8,y:52.9}).wait(1).to({graphics:mask_5_graphics_72,x:399.8,y:54.3}).wait(1).to({graphics:mask_5_graphics_73,x:399.8,y:55.1}).wait(1).to({graphics:mask_5_graphics_74,x:399.8,y:56.4}).wait(1).to({graphics:mask_5_graphics_75,x:399.8,y:57.4}).wait(1).to({graphics:mask_5_graphics_76,x:399.8,y:58.1}).wait(1).to({graphics:mask_5_graphics_77,x:399.8,y:59.2}).wait(1).to({graphics:mask_5_graphics_78,x:399.8,y:59.9}).wait(1).to({graphics:mask_5_graphics_79,x:399.8,y:60.2}).wait(1).to({graphics:mask_5_graphics_80,x:399.8,y:60.3}).wait(1).to({graphics:mask_5_graphics_81,x:399.8,y:60.3}).wait(1).to({graphics:mask_5_graphics_82,x:399.8,y:60.3}).wait(1).to({graphics:mask_5_graphics_83,x:399.8,y:60.3}).wait(36).to({graphics:mask_5_graphics_119,x:399.8,y:60.3}).wait(1).to({graphics:mask_5_graphics_120,x:399.8,y:60.3}).wait(1).to({graphics:mask_5_graphics_121,x:399.8,y:60.3}).wait(1).to({graphics:mask_5_graphics_122,x:399.8,y:60.3}).wait(1).to({graphics:mask_5_graphics_123,x:399.8,y:60.3}).wait(1).to({graphics:mask_5_graphics_124,x:399.8,y:60.3}).wait(1).to({graphics:mask_5_graphics_125,x:399.8,y:60.3}).wait(1).to({graphics:mask_5_graphics_126,x:399.8,y:60.3}).wait(1).to({graphics:mask_5_graphics_127,x:399.8,y:60.3}).wait(1).to({graphics:mask_5_graphics_128,x:399.8,y:60.3}).wait(1).to({graphics:mask_5_graphics_129,x:399.8,y:60.3}).wait(1).to({graphics:mask_5_graphics_130,x:399.8,y:60.3}).wait(1).to({graphics:mask_5_graphics_131,x:399.8,y:60.3}).wait(1).to({graphics:mask_5_graphics_132,x:399.8,y:60.3}).wait(1).to({graphics:mask_5_graphics_133,x:399.8,y:60.3}).wait(1).to({graphics:mask_5_graphics_134,x:399.8,y:60.3}).wait(1));
  
    // D
    this.shape_5 = new cjs.Shape();
    this.shape_5.graphics.f("#fff").s().p("AnJJbIAAhNIEMAAQDvAACmiYQCmiYAAjeQAAjcimiZQimiYjvAAIkMAAIAAhNIEMAAQEPAAC8CvQC8CvAAD8QAAD9i8CvQi8CvkPAAgAnJGoIAAswIEMAAQC7AAB9B1QB9B1AACuQAACuh9B1Qh9B1i7AAgAl8FcIC/AAQCdAABmhfQBmhdAAiQQAAiQhmheQhmheidAAIi/AAg");
    this.shape_5.setTransform(399.8,60.3);
  
    this.instance_10 = new lib.Tween27("synched",0);
    this.instance_10.parent = this;
    this.instance_10.setTransform(399.8,60.3);
    this.instance_10._off = true;
  
    this.instance_11 = new lib.Tween28("synched",0);
    this.instance_11.parent = this;
    this.instance_11.setTransform(399.8,60.3);
    this.instance_11.alpha = 0;
  
    var maskedShapeInstanceList = [this.shape_5,this.instance_10,this.instance_11];
  
    for(var shapedInstanceItr = 0; shapedInstanceItr < maskedShapeInstanceList.length; shapedInstanceItr++) {
      maskedShapeInstanceList[shapedInstanceItr].mask = mask_5;
    }
  
    this.timeline.addTween(cjs.Tween.get({}).to({state:[]}).to({state:[{t:this.shape_5}]},1).to({state:[{t:this.instance_10}]},118).to({state:[{t:this.instance_11}]},15).wait(1));
    this.timeline.addTween(cjs.Tween.get(this.instance_10).wait(119).to({_off:false},0).to({_off:true,alpha:0},15).wait(1));
  
    // N mask (mask)
    var mask_6 = new cjs.Shape();
    mask_6._off = true;
    var mask_6_graphics_1 = new cjs.Graphics().p("AggAsIgBh8IAAAAIBDBXIglBLg");
    var mask_6_graphics_2 = new cjs.Graphics().p("Ag5ANIgBh8IABAAIB0CWIgoBJg");
    var mask_6_graphics_3 = new cjs.Graphics().p("AhMgJIgBh9IAAAAICcDKIgsBDg");
    var mask_6_graphics_4 = new cjs.Graphics().p("AhjglIgBh8IABgBIDIEEIguBBg");
    var mask_6_graphics_5 = new cjs.Graphics().p("Ah7hFIgBh9IABAAID4FBIgsBEg");
    var mask_6_graphics_6 = new cjs.Graphics().p("AiRhjIgBh9IAAAAIElF8IgqBFg");
    var mask_6_graphics_7 = new cjs.Graphics().p("AiqiFIgBh9IAAAAIFXG8IgmBJg");
    var mask_6_graphics_8 = new cjs.Graphics().p("AjFioIgBh9IAAAAIGOIEIgpBHg");
    var mask_6_graphics_9 = new cjs.Graphics().p("AjcjHIgBh8IAAgBIG7I+IgmBLg");
    var mask_6_graphics_10 = new cjs.Graphics().p("Aj4jqIgBh9IABAAIHyKFIgmBLg");
    var mask_6_graphics_11 = new cjs.Graphics().p("ADzGPIoHqgIgBh8IAAgBIIrLPIgjBOg");
    var mask_6_graphics_12 = new cjs.Graphics().p("ADaGPIoHqgIgBh8IABgBIIsLRIAwAAIAABMg");
    var mask_6_graphics_13 = new cjs.Graphics().p("AC1GPIoGqgIgBh8IAAgBIIsLRIB4AAIAAgDIABAAIAABPg");
    var mask_6_graphics_14 = new cjs.Graphics().p("ACQGPIoHqgIgBh8IABgBIIsLRIB4AAIAAgDIBMAAIAABPg");
    var mask_6_graphics_15 = new cjs.Graphics().p("ACQGPIoHqgIgBh8IABgBIIsLRIB4AAIAAhYIBMAAIAACkg");
    var mask_6_graphics_16 = new cjs.Graphics().p("ACQGPIoHqgIgBh8IABgBIIsLRIB4AAIAAigIBMAAIAADsg");
    var mask_6_graphics_17 = new cjs.Graphics().p("ACQGPIoHqgIgBh8IABgBIIsLRIB4AAIAAjmIBMAAIAAEyg");
    var mask_6_graphics_18 = new cjs.Graphics().p("ACQGPIoHqgIgBh8IABgBIIsLRIB4AAIAAkuIBMAAIAAF6g");
    var mask_6_graphics_19 = new cjs.Graphics().p("ACQGPIoHqgIgBh8IABgBIIsLRIB4AAIAAl1IBMAAIAAHBg");
    var mask_6_graphics_20 = new cjs.Graphics().p("ACQGPIoHqgIgBh8IABgBIIsLRIB4AAIAAm7IBMAAIAAIHg");
    var mask_6_graphics_21 = new cjs.Graphics().p("ACQGPIoHqgIgBh8IABgBIIsLRIB4AAIAAn+IBMAAIAAJKg");
    var mask_6_graphics_22 = new cjs.Graphics().p("ACQGPIoHqgIgBh8IABgBIIsLRIB4AAIAApDIBMAAIAAKPg");
    var mask_6_graphics_23 = new cjs.Graphics().p("ACQGPIoHqgIgBh8IABgBIIsLRIB4AAIAAqQIBMAAIAALcg");
    var mask_6_graphics_24 = new cjs.Graphics().p("ACQGQIoHqgIgBh9IABAAIIsLRIB4AAIAArTIBMAAIAAMfg");
    var mask_6_graphics_25 = new cjs.Graphics().p("ACQG2IoHqgIgBh9IABAAIIsLRIB4AAIAAsfIBMAAIAANrg");
    var mask_6_graphics_26 = new cjs.Graphics().p("ACQHVIoHqfIgBh9IABAAIIsLRIB4AAIAAteIBMAAIAAOpg");
    var mask_6_graphics_27 = new cjs.Graphics().p("ACQH4IoHqgIgBh9IABAAIIsLRIB4AAIAAuiIBMAAIAAPug");
    var mask_6_graphics_28 = new cjs.Graphics().p("ACQIcIoHqgIgBh9IABAAIIsLRIB4AAIAAvqIBMAAIAAQ2g");
    var mask_6_graphics_29 = new cjs.Graphics().p("ACQI5IoHqfIgBh9IABAAIIsLRIB4AAIAAwmIBMAAIAARxg");
    var mask_6_graphics_30 = new cjs.Graphics().p("ACQJaIoHqfIgBh9IABAAIIsLRIB4AAIAAxoIBMAAIAASzg");
    var mask_6_graphics_31 = new cjs.Graphics().p("ACQJaIoHqfIgBh9IABAAIIsLRIB4AAIAAxoIBMAAIAASzgABbonIAAgyIBMAAIAAAyg");
    var mask_6_graphics_32 = new cjs.Graphics().p("ACQJaIoHqfIgBh9IABAAIIsLRIB4AAIAAxoIBMAAIAASzgABbnqIAAhvIBMAAIAABvg");
    var mask_6_graphics_33 = new cjs.Graphics().p("ACQJaIoHqfIgBh9IABAAIIsLRIB4AAIAAxoIBMAAIAASzgABbmxIAAioIBMAAIAACog");
    var mask_6_graphics_34 = new cjs.Graphics().p("ACQJaIoHqfIgBh9IABAAIIsLRIB4AAIAAxoIBMAAIAASzgABbl3IAAjiIBMAAIAADig");
    var mask_6_graphics_35 = new cjs.Graphics().p("ACQJaIoHqfIgBh9IABAAIIsLRIB4AAIAAxoIBMAAIAASzgABbkxIAAkoIBMAAIAAEog");
    var mask_6_graphics_36 = new cjs.Graphics().p("ACQJaIoHqfIgBh9IABAAIIsLRIB4AAIAAxoIBMAAIAASzgABbjUIAAmFIBMAAIAAGFg");
    var mask_6_graphics_37 = new cjs.Graphics().p("ACQJaIoHqfIgBh9IABAAIIsLRIB4AAIAAxoIBMAAIAASzgABbiGIAAnTIBMAAIAAHTg");
    var mask_6_graphics_38 = new cjs.Graphics().p("ACQJaIoHqfIgBh9IABAAIIsLRIB4AAIAAxoIBMAAIAASzgABbg1IAAokIBMAAIAAIkg");
    var mask_6_graphics_39 = new cjs.Graphics().p("ACQJaIoHqfIgBh9IABAAIIsLRIB4AAIAAxoIBMAAIAASzgABbAbIAAp0IBMAAIAAJ0g");
    var mask_6_graphics_40 = new cjs.Graphics().p("ACQJaIoHqfIgBh9IABAAIIsLRIB4AAIAAxoIBMAAIAASzgABbBIIAAqhIBMAAIAALlg");
    var mask_6_graphics_41 = new cjs.Graphics().p("ACQJaIoHqfIgBh9IABAAIIsLRIB4AAIAAxoIBMAAIAASzgABbDHIAAh9IAAAAIAAqjIBMAAIAAODg");
    var mask_6_graphics_42 = new cjs.Graphics().p("ACQJaIoHqfIgBh9IABAAIIsLRIB4AAIAAxoIBMAAIAASzgAAzCTIAAh9IAoA0IAAqjIBMAAIAAODg");
    var mask_6_graphics_43 = new cjs.Graphics().p("ACQJaIoHqfIgBh9IABAAIIsLRIB4AAIAAxoIBMAAIAASzgAAGBZIAAh8IBVBtIAAqjIBMAAIAAODg");
    var mask_6_graphics_44 = new cjs.Graphics().p("ACQJaIoHqfIgBh9IABAAIIsLRIB4AAIAAxoIBMAAIAASzgAg2AKIAAh8ICRC8IAAqjIBMAAIAAODg");
    var mask_6_graphics_45 = new cjs.Graphics().p("ACQJaIoHqfIgBh9IABAAIIsLRIB4AAIAAxoIBMAAIAASzgAh0hGIAAh9IDPENIAAqjIBMAAIAAODg");
    var mask_6_graphics_46 = new cjs.Graphics().p("ACQJaIoHqfIgBh9IABAAIIsLRIB4AAIAAxoIBMAAIAASzgAi1iZIAAh9IEQFgIAAqjIBMAAIAAODg");
    var mask_6_graphics_47 = new cjs.Graphics().p("ACQJaIoHqfIgBh9IABAAIIsLRIB4AAIAAxoIBMAAIAASzgAjtjjIAAh9IFIGqIAAqjIBMAAIAAODg");
    var mask_6_graphics_48 = new cjs.Graphics().p("ACQJaIoHqfIgBh9IABAAIIsLRIB4AAIAAxoIBMAAIAASzgAktk1IAAh9IGIH8IAAqjIBMAAIAAODg");
    var mask_6_graphics_49 = new cjs.Graphics().p("ACQJaIoHqfIgBh9IABAAIIsLRIB4AAIAAxoIBMAAIAASzgAlvmLIAAh9IHKJSIAAqjIBMAAIAAODg");
    var mask_6_graphics_50 = new cjs.Graphics().p("ACsJaIoHqfIgBh9IAAAAIItLRIB3AAIAAxoIBNAAIAASzgAmTneIAAh7IABAAIIJKjIAAqjIBMAAIAAODg");
    var mask_6_graphics_51 = new cjs.Graphics().p("ADWJaIoHqfIgBh9IAAAAIItLRIB3AAIAAxoIBNAAIAASzgAmNoNIgxAAIAAhMIBWAAIIJKjIAAqjIBMAAIAAODg");
    var mask_6_graphics_52 = new cjs.Graphics().p("AD3JaIoGqfIgBh9IAAAAIIsLRIB4AAIAAxoIBMAAIAASzgAlsoNIhzAAIAAhMICYAAIIKKjIAAqjIBMAAIAAODg");
    var mask_6_graphics_53 = new cjs.Graphics().p("AEgJaIoHqfIgBh9IAAAAIItLRIB3AAIAAxoIBNAAIAASzgAlDoNIjEAAIAAhMIDpAAIIJKjIAAqjIBMAAIAAODg");
    var mask_6_graphics_54 = new cjs.Graphics().p("AEgJaIoHqfIgBh9IAAAAIItLRIB3AAIAAxoIBNAAIAASzgAlDoNIh5AAIAAA4IADAEIhOAAIAAiIIDpAAIIJKjIAAqjIBMAAIAAODg");
    var mask_6_graphics_55 = new cjs.Graphics().p("AEgJaIoHqfIgBh9IAAAAIItLRIB3AAIAAxoIBNAAIAASzgAlDoNIh5AAIAAA0IguBFIgdgnIAAieIDpAAIIJKjIAAqjIBMAAIAAODg");
    var mask_6_graphics_56 = new cjs.Graphics().p("AEgJaIoHqfIgBh9IAAAAIItLRIB3AAIAAxoIBNAAIAASzgAlDoNIh5AAIAAA4IAeAmIAAB8IhpiIIAAieIDpAAIIJKjIAAqjIBMAAIAAODg");
    var mask_6_graphics_57 = new cjs.Graphics().p("AEgJaIoHqfIgBh9IAAAAIItLRIB3AAIAAxoIBNAAIAASzgAlDoNIh5AAIAAA4IBbB2IgBB7IiljXIAAieIDpAAIIJKjIAAqjIBMAAIAAODg");
    var mask_6_graphics_58 = new cjs.Graphics().p("AEgJaIoHqfIgBh9IAAAAIItLRIB3AAIAAxoIBNAAIAASzgAlDoNIh5AAIAAA4ICNC2IgHB5IAAAAIAAgFIjRkQIAAieIDpAAIIJKjIAAqjIBMAAIAAODg");
    var mask_6_graphics_59 = new cjs.Graphics().p("AEgJaIoIqhIAAAAIgBAAIgBgCIAAACIgDAAIhJhfIAAgFIjRkQIAAieIDpAAIIJKjIAAqjIBMAAIAAODIp6s3Ih5AAIAAA4IMBPkIB3AAIAAxoIBNAAIAASzg");
    var mask_6_graphics_60 = new cjs.Graphics().p("AEgJaIoIqhIAAAAIgBAAIgBgCIAAACIhMAAIAAhkIjRkQIAAieIDpAAIIJKjIAAqjIBMAAIAAODIp6s3Ih5AAIAAA4IMBPkIB3AAIAAxoIBNAAIAASzg");
    var mask_6_graphics_61 = new cjs.Graphics().p("AEgJaIoKqjIAABSIhMAAIAAi0IjRkQIAAieIDpAAIIJKjIAAqjIBMAAIAAODIp6s3Ih5AAIAAA4IMBPkIB3AAIAAxoIBNAAIAASzg");
    var mask_6_graphics_62 = new cjs.Graphics().p("AEgJaIoKqjIAACSIhMAAIAAj0IjRkQIAAieIDpAAIIJKjIAAqjIBMAAIAAODIp6s3Ih5AAIAAA4IMBPkIB3AAIAAxoIBNAAIAASzg");
    var mask_6_graphics_63 = new cjs.Graphics().p("AEgJaIoKqjIAADUIhMAAIAAk2IjRkQIAAieIDpAAIIJKjIAAqjIBMAAIAAODIp6s3Ih5AAIAAA4IMBPkIB3AAIAAxoIBNAAIAASzg");
    var mask_6_graphics_64 = new cjs.Graphics().p("AEgJaIoKqjIAAEjIhMAAIAAmFIjRkQIAAieIDpAAIIJKjIAAqjIBMAAIAAODIp6s3Ih5AAIAAA4IMBPkIB3AAIAAxoIBNAAIAASzg");
    var mask_6_graphics_65 = new cjs.Graphics().p("AEgJaIoKqjIAAFtIhMAAIAAnPIjRkQIAAieIDpAAIIJKjIAAqjIBMAAIAAODIp6s3Ih5AAIAAA4IMBPkIB3AAIAAxoIBNAAIAASzg");
    var mask_6_graphics_66 = new cjs.Graphics().p("AEgJaIoKqjIAAG/IhMAAIAAohIjRkQIAAieIDpAAIIJKjIAAqjIBMAAIAAODIp6s3Ih5AAIAAA4IMBPkIB3AAIAAxoIBNAAIAASzg");
    var mask_6_graphics_67 = new cjs.Graphics().p("AEgJaIoKqjIAAIJIhMAAIAAprIjRkQIAAieIDpAAIIJKjIAAqjIBMAAIAAODIp6s3Ih5AAIAAA4IMBPkIB3AAIAAxoIBNAAIAASzg");
    var mask_6_graphics_68 = new cjs.Graphics().p("AEgJaIoKqjIAAJUIhMAAIAAq2IjRkQIAAieIDpAAIIJKjIAAqjIBMAAIAAODIp6s3Ih5AAIAAA4IMBPkIB3AAIAAxoIBNAAIAASzg");
    var mask_6_graphics_69 = new cjs.Graphics().p("AEgJaIoKqjIAAKjIhNAAIAAhLIABAAIAAq6IjRkQIAAieIDpAAIIJKjIAAqjIBMAAIAAODIp6s3Ih5AAIAAA4IMBPkIB3AAIAAxoIBNAAIAASzg");
    var mask_6_graphics_70 = new cjs.Graphics().p("AEgJaIoKqjIAAKjIiKAAIAAhLIA+AAIAAq6IjRkQIAAieIDpAAIIJKjIAAqjIBMAAIAAODIp6s3Ih5AAIAAA4IMBPkIB3AAIAAxoIBNAAIAASzg");
    var mask_6_graphics_71 = new cjs.Graphics().p("AEgJaIoKqjIAAKjIjPAAIAAhLICDAAIAAq6IjRkQIAAieIDpAAIIJKjIAAqjIBMAAIAAODIp6s3Ih5AAIAAA4IMBPkIB3AAIAAxoIBNAAIAASzg");
    var mask_6_graphics_72 = new cjs.Graphics().p("AEgJaIoKqjIAAKjIkdAAIAAhMIBLAAIAAABICGAAIAAq6IjRkQIAAieIDpAAIIJKjIAAqjIBMAAIAAODIp6s3Ih5AAIAAA4IMBPkIB3AAIAAxoIBNAAIAASzg");
    var mask_6_graphics_73 = new cjs.Graphics().p("AEgJaIoKqjIAAKjIkdAAIAAiRIBLAAIAABGICGAAIAAq6IjRkQIAAieIDpAAIIJKjIAAqjIBMAAIAAODIp6s3Ih5AAIAAA4IMBPkIB3AAIAAxoIBNAAIAASzg");
    var mask_6_graphics_74 = new cjs.Graphics().p("AEgJaIoKqjIAAKjIkdAAIAAjZIBLAAIAACOICGAAIAAq6IjRkQIAAieIDpAAIIJKjIAAqjIBMAAIAAODIp6s3Ih5AAIAAA4IMBPkIB3AAIAAxoIBNAAIAASzg");
    var mask_6_graphics_75 = new cjs.Graphics().p("AEgJaIoKqjIAAKjIkdAAIAAkgIBLAAIAADVICGAAIAAq6IjRkQIAAieIDpAAIIJKjIAAqjIBMAAIAAODIp6s3Ih5AAIAAA4IMBPkIB3AAIAAxoIBNAAIAASzg");
    var mask_6_graphics_76 = new cjs.Graphics().p("AEgJaIoKqjIAAKjIkdAAIAAl0IBLAAIAAEpICGAAIAAq6IjRkQIAAieIDpAAIIJKjIAAqjIBMAAIAAODIp6s3Ih5AAIAAA4IMBPkIB3AAIAAxoIBNAAIAASzg");
    var mask_6_graphics_77 = new cjs.Graphics().p("AEgJaIoKqjIAAKjIkdAAIAAnRIBLAAIAAGGICGAAIAAq6IjRkQIAAieIDpAAIIJKjIAAqjIBMAAIAAODIp6s3Ih5AAIAAA4IMBPkIB3AAIAAxoIBNAAIAASzg");
    var mask_6_graphics_78 = new cjs.Graphics().p("AEgJaIoKqjIAAKjIkdAAIAAorIBLAAIAAHgICGAAIAAq6IjRkQIAAieIDpAAIIJKjIAAqjIBMAAIAAODIp6s3Ih5AAIAAA4IMBPkIB3AAIAAxoIBNAAIAASzg");
    var mask_6_graphics_79 = new cjs.Graphics().p("AEgJaIoKqjIAAKjIkdAAIAApyIBLAAIAAInICGAAIAAq6IjRkQIAAieIDpAAIIJKjIAAqjIBMAAIAAODIp6s3Ih5AAIAAA4IMBPkIB3AAIAAxoIBNAAIAASzg");
    var mask_6_graphics_80 = new cjs.Graphics().p("AEgJaIoKqjIAAKjIkdAAIAAq/IBLAAIAAJ0ICGAAIAAq6IjRkQIAAieIDpAAIIJKjIAAqjIBMAAIAAODIp6s3Ih5AAIAAA4IMBPkIB3AAIAAxoIBNAAIAASzg");
    var mask_6_graphics_81 = new cjs.Graphics().p("AEgJaIoKqjIAAKjIkdAAIAAsGIBLAAIAAK7ICGAAIAAq6IjRkQIAAieIDpAAIIJKjIAAqjIBMAAIAAODIp6s3Ih5AAIAAA4IMBPkIB3AAIAAxoIBNAAIAASzg");
    var mask_6_graphics_82 = new cjs.Graphics().p("AEgJaIoKqjIAAKjIkdAAIAAtwIBLBjIAALCICGAAIAAq6IjRkQIAAieIDpAAIIJKjIAAqjIBMAAIAAODIp6s3Ih5AAIAAA4IMBPkIB3AAIAAxoIBNAAIAASzg");
    var mask_6_graphics_119 = new cjs.Graphics().p("AEgJaIoKqjIAAKjIkdAAIAAtwIBLBjIAALCICGAAIAAq6IjRkQIAAieIDpAAIIJKjIAAqjIBMAAIAAODIp6s3Ih5AAIAAA4IMBPkIB3AAIAAxoIBNAAIAASzg");
    var mask_6_graphics_120 = new cjs.Graphics().p("AEgJaIoKqjIAAKjIkdAAIAAtwIBLBjIAALCICGAAIAAq6IjRkQIAAieIDpAAIIJKjIAAqjIBMAAIAAODIp6s3Ih5AAIAAA4IMBPkIB3AAIAAxoIBNAAIAASzg");
    var mask_6_graphics_121 = new cjs.Graphics().p("AEgJaIoKqjIAAKjIkdAAIAAtwIBLBjIAALCICGAAIAAq6IjRkQIAAieIDpAAIIJKjIAAqjIBMAAIAAODIp6s3Ih5AAIAAA4IMBPkIB3AAIAAxoIBNAAIAASzg");
    var mask_6_graphics_122 = new cjs.Graphics().p("AEgJaIoKqjIAAKjIkdAAIAAtwIBLBjIAALCICGAAIAAq6IjRkQIAAieIDpAAIIJKjIAAqjIBMAAIAAODIp6s3Ih5AAIAAA4IMBPkIB3AAIAAxoIBNAAIAASzg");
    var mask_6_graphics_123 = new cjs.Graphics().p("AEgJaIoKqjIAAKjIkdAAIAAtwIBLBjIAALCICGAAIAAq6IjRkQIAAieIDpAAIIJKjIAAqjIBMAAIAAODIp6s3Ih5AAIAAA4IMBPkIB3AAIAAxoIBNAAIAASzg");
    var mask_6_graphics_124 = new cjs.Graphics().p("AEgJaIoKqjIAAKjIkdAAIAAtwIBLBjIAALCICGAAIAAq6IjRkQIAAieIDpAAIIJKjIAAqjIBMAAIAAODIp6s3Ih5AAIAAA4IMBPkIB3AAIAAxoIBNAAIAASzg");
    var mask_6_graphics_125 = new cjs.Graphics().p("AEgJaIoKqjIAAKjIkdAAIAAtwIBLBjIAALCICGAAIAAq6IjRkQIAAieIDpAAIIJKjIAAqjIBMAAIAAODIp6s3Ih5AAIAAA4IMBPkIB3AAIAAxoIBNAAIAASzg");
    var mask_6_graphics_126 = new cjs.Graphics().p("AEgJaIoKqjIAAKjIkdAAIAAtwIBLBjIAALCICGAAIAAq6IjRkQIAAieIDpAAIIJKjIAAqjIBMAAIAAODIp6s3Ih5AAIAAA4IMBPkIB3AAIAAxoIBNAAIAASzg");
    var mask_6_graphics_127 = new cjs.Graphics().p("AEgJaIoKqjIAAKjIkdAAIAAtwIBLBjIAALCICGAAIAAq6IjRkQIAAieIDpAAIIJKjIAAqjIBMAAIAAODIp6s3Ih5AAIAAA4IMBPkIB3AAIAAxoIBNAAIAASzg");
    var mask_6_graphics_128 = new cjs.Graphics().p("AEgJaIoKqjIAAKjIkdAAIAAtwIBLBjIAALCICGAAIAAq6IjRkQIAAieIDpAAIIJKjIAAqjIBMAAIAAODIp6s3Ih5AAIAAA4IMBPkIB3AAIAAxoIBNAAIAASzg");
    var mask_6_graphics_129 = new cjs.Graphics().p("AEgJaIoKqjIAAKjIkdAAIAAtwIBLBjIAALCICGAAIAAq6IjRkQIAAieIDpAAIIJKjIAAqjIBMAAIAAODIp6s3Ih5AAIAAA4IMBPkIB3AAIAAxoIBNAAIAASzg");
    var mask_6_graphics_130 = new cjs.Graphics().p("AEgJaIoKqjIAAKjIkdAAIAAtwIBLBjIAALCICGAAIAAq6IjRkQIAAieIDpAAIIJKjIAAqjIBMAAIAAODIp6s3Ih5AAIAAA4IMBPkIB3AAIAAxoIBNAAIAASzg");
    var mask_6_graphics_131 = new cjs.Graphics().p("AEgJaIoKqjIAAKjIkdAAIAAtwIBLBjIAALCICGAAIAAq6IjRkQIAAieIDpAAIIJKjIAAqjIBMAAIAAODIp6s3Ih5AAIAAA4IMBPkIB3AAIAAxoIBNAAIAASzg");
    var mask_6_graphics_132 = new cjs.Graphics().p("AEgJaIoKqjIAAKjIkdAAIAAtwIBLBjIAALCICGAAIAAq6IjRkQIAAieIDpAAIIJKjIAAqjIBMAAIAAODIp6s3Ih5AAIAAA4IMBPkIB3AAIAAxoIBNAAIAASzg");
    var mask_6_graphics_133 = new cjs.Graphics().p("AEgJaIoKqjIAAKjIkdAAIAAtwIBLBjIAALCICGAAIAAq6IjRkQIAAieIDpAAIIJKjIAAqjIBMAAIAAODIp6s3Ih5AAIAAA4IMBPkIB3AAIAAxoIBNAAIAASzg");
    var mask_6_graphics_134 = new cjs.Graphics().p("AEgJaIoKqjIAAKjIkdAAIAAtwIBLBjIAALCICGAAIAAq6IjRkQIAAieIDpAAIIJKjIAAqjIBMAAIAAODIp6s3Ih5AAIAAA4IMBPkIB3AAIAAxoIBNAAIAASzg");
  
    this.timeline.addTween(cjs.Tween.get(mask_6).to({graphics:null,x:0,y:0}).wait(1).to({graphics:mask_6_graphics_1,x:263.4,y:50.4}).wait(1).to({graphics:mask_6_graphics_2,x:265.8,y:53.4}).wait(1).to({graphics:mask_6_graphics_3,x:267.8,y:55.8}).wait(1).to({graphics:mask_6_graphics_4,x:270,y:58.5}).wait(1).to({graphics:mask_6_graphics_5,x:272.4,y:61.8}).wait(1).to({graphics:mask_6_graphics_6,x:274.7,y:64.8}).wait(1).to({graphics:mask_6_graphics_7,x:277.1,y:68.2}).wait(1).to({graphics:mask_6_graphics_8,x:279.9,y:71.6}).wait(1).to({graphics:mask_6_graphics_9,x:282.1,y:74.7}).wait(1).to({graphics:mask_6_graphics_10,x:284.9,y:78.3}).wait(1).to({graphics:mask_6_graphics_11,x:287.7,y:82.1}).wait(1).to({graphics:mask_6_graphics_12,x:290.2,y:82.1}).wait(1).to({graphics:mask_6_graphics_13,x:293.9,y:82.1}).wait(1).to({graphics:mask_6_graphics_14,x:297.6,y:82.1}).wait(1).to({graphics:mask_6_graphics_15,x:297.6,y:82.1}).wait(1).to({graphics:mask_6_graphics_16,x:297.6,y:82.1}).wait(1).to({graphics:mask_6_graphics_17,x:297.6,y:82.1}).wait(1).to({graphics:mask_6_graphics_18,x:297.6,y:82.1}).wait(1).to({graphics:mask_6_graphics_19,x:297.6,y:82.1}).wait(1).to({graphics:mask_6_graphics_20,x:297.6,y:82.1}).wait(1).to({graphics:mask_6_graphics_21,x:297.6,y:82.1}).wait(1).to({graphics:mask_6_graphics_22,x:297.6,y:82.1}).wait(1).to({graphics:mask_6_graphics_23,x:297.6,y:82.1}).wait(1).to({graphics:mask_6_graphics_24,x:297.6,y:82}).wait(1).to({graphics:mask_6_graphics_25,x:297.6,y:78.2}).wait(1).to({graphics:mask_6_graphics_26,x:297.6,y:75.1}).wait(1).to({graphics:mask_6_graphics_27,x:297.6,y:71.7}).wait(1).to({graphics:mask_6_graphics_28,x:297.6,y:68.1}).wait(1).to({graphics:mask_6_graphics_29,x:297.6,y:65.1}).wait(1).to({graphics:mask_6_graphics_30,x:297.6,y:61.8}).wait(1).to({graphics:mask_6_graphics_31,x:297.6,y:61.8}).wait(1).to({graphics:mask_6_graphics_32,x:297.6,y:61.8}).wait(1).to({graphics:mask_6_graphics_33,x:297.6,y:61.8}).wait(1).to({graphics:mask_6_graphics_34,x:297.6,y:61.8}).wait(1).to({graphics:mask_6_graphics_35,x:297.6,y:61.8}).wait(1).to({graphics:mask_6_graphics_36,x:297.6,y:61.8}).wait(1).to({graphics:mask_6_graphics_37,x:297.6,y:61.8}).wait(1).to({graphics:mask_6_graphics_38,x:297.6,y:61.8}).wait(1).to({graphics:mask_6_graphics_39,x:297.6,y:61.8}).wait(1).to({graphics:mask_6_graphics_40,x:297.6,y:61.8}).wait(1).to({graphics:mask_6_graphics_41,x:297.6,y:61.8}).wait(1).to({graphics:mask_6_graphics_42,x:297.6,y:61.8}).wait(1).to({graphics:mask_6_graphics_43,x:297.6,y:61.8}).wait(1).to({graphics:mask_6_graphics_44,x:297.6,y:61.8}).wait(1).to({graphics:mask_6_graphics_45,x:297.6,y:61.8}).wait(1).to({graphics:mask_6_graphics_46,x:297.6,y:61.8}).wait(1).to({graphics:mask_6_graphics_47,x:297.6,y:61.8}).wait(1).to({graphics:mask_6_graphics_48,x:297.6,y:61.8}).wait(1).to({graphics:mask_6_graphics_49,x:297.6,y:61.8}).wait(1).to({graphics:mask_6_graphics_50,x:294.9,y:61.8}).wait(1).to({graphics:mask_6_graphics_51,x:290.7,y:61.8}).wait(1).to({graphics:mask_6_graphics_52,x:287.3,y:61.8}).wait(1).to({graphics:mask_6_graphics_53,x:283.3,y:61.8}).wait(1).to({graphics:mask_6_graphics_54,x:283.3,y:61.8}).wait(1).to({graphics:mask_6_graphics_55,x:283.3,y:61.8}).wait(1).to({graphics:mask_6_graphics_56,x:283.3,y:61.8}).wait(1).to({graphics:mask_6_graphics_57,x:283.3,y:61.8}).wait(1).to({graphics:mask_6_graphics_58,x:283.3,y:61.8}).wait(1).to({graphics:mask_6_graphics_59,x:283.3,y:61.8}).wait(1).to({graphics:mask_6_graphics_60,x:283.3,y:61.8}).wait(1).to({graphics:mask_6_graphics_61,x:283.3,y:61.8}).wait(1).to({graphics:mask_6_graphics_62,x:283.3,y:61.8}).wait(1).to({graphics:mask_6_graphics_63,x:283.3,y:61.8}).wait(1).to({graphics:mask_6_graphics_64,x:283.3,y:61.8}).wait(1).to({graphics:mask_6_graphics_65,x:283.3,y:61.8}).wait(1).to({graphics:mask_6_graphics_66,x:283.3,y:61.8}).wait(1).to({graphics:mask_6_graphics_67,x:283.3,y:61.8}).wait(1).to({graphics:mask_6_graphics_68,x:283.3,y:61.8}).wait(1).to({graphics:mask_6_graphics_69,x:283.3,y:61.8}).wait(1).to({graphics:mask_6_graphics_70,x:283.3,y:61.8}).wait(1).to({graphics:mask_6_graphics_71,x:283.3,y:61.8}).wait(1).to({graphics:mask_6_graphics_72,x:283.3,y:61.8}).wait(1).to({graphics:mask_6_graphics_73,x:283.3,y:61.8}).wait(1).to({graphics:mask_6_graphics_74,x:283.3,y:61.8}).wait(1).to({graphics:mask_6_graphics_75,x:283.3,y:61.8}).wait(1).to({graphics:mask_6_graphics_76,x:283.3,y:61.8}).wait(1).to({graphics:mask_6_graphics_77,x:283.3,y:61.8}).wait(1).to({graphics:mask_6_graphics_78,x:283.3,y:61.8}).wait(1).to({graphics:mask_6_graphics_79,x:283.3,y:61.8}).wait(1).to({graphics:mask_6_graphics_80,x:283.3,y:61.8}).wait(1).to({graphics:mask_6_graphics_81,x:283.3,y:61.8}).wait(1).to({graphics:mask_6_graphics_82,x:283.3,y:61.8}).wait(37).to({graphics:mask_6_graphics_119,x:283.3,y:61.8}).wait(1).to({graphics:mask_6_graphics_120,x:283.3,y:61.8}).wait(1).to({graphics:mask_6_graphics_121,x:283.3,y:61.8}).wait(1).to({graphics:mask_6_graphics_122,x:283.3,y:61.8}).wait(1).to({graphics:mask_6_graphics_123,x:283.3,y:61.8}).wait(1).to({graphics:mask_6_graphics_124,x:283.3,y:61.8}).wait(1).to({graphics:mask_6_graphics_125,x:283.3,y:61.8}).wait(1).to({graphics:mask_6_graphics_126,x:283.3,y:61.8}).wait(1).to({graphics:mask_6_graphics_127,x:283.3,y:61.8}).wait(1).to({graphics:mask_6_graphics_128,x:283.3,y:61.8}).wait(1).to({graphics:mask_6_graphics_129,x:283.3,y:61.8}).wait(1).to({graphics:mask_6_graphics_130,x:283.3,y:61.8}).wait(1).to({graphics:mask_6_graphics_131,x:283.3,y:61.8}).wait(1).to({graphics:mask_6_graphics_132,x:283.3,y:61.8}).wait(1).to({graphics:mask_6_graphics_133,x:283.3,y:61.8}).wait(1).to({graphics:mask_6_graphics_134,x:283.3,y:61.8}).wait(1));
  
    // N
    this.shape_6 = new cjs.Shape();
    this.shape_6.graphics.f("#fff").s().p("AEgJaIoKqjIAAKjIkdAAIAAtwIBLBjIAALCICGAAIAAq6IjRkQIAAieIDpAAIIJKjIAAqjIBMAAIAAODIp6s3Ih5AAIAAA4IMBPkIB3AAIAAxoIBNAAIAASzg");
    this.shape_6.setTransform(283.3,61.8);
  
    this.instance_12 = new lib.Tween31("synched",0);
    this.instance_12.parent = this;
    this.instance_12.setTransform(283.3,61.8);
    this.instance_12._off = true;
  
    this.instance_13 = new lib.Tween32("synched",0);
    this.instance_13.parent = this;
    this.instance_13.setTransform(283.3,61.8);
    this.instance_13.alpha = 0;
  
    var maskedShapeInstanceList = [this.shape_6,this.instance_12,this.instance_13];
  
    for(var shapedInstanceItr = 0; shapedInstanceItr < maskedShapeInstanceList.length; shapedInstanceItr++) {
      maskedShapeInstanceList[shapedInstanceItr].mask = mask_6;
    }
  
    this.timeline.addTween(cjs.Tween.get({}).to({state:[]}).to({state:[{t:this.shape_6}]},1).to({state:[{t:this.instance_12}]},118).to({state:[{t:this.instance_13}]},15).wait(1));
    this.timeline.addTween(cjs.Tween.get(this.instance_12).wait(119).to({_off:false},0).to({_off:true,alpha:0},15).wait(1));
  
    // E mask (mask)
    var mask_7 = new cjs.Shape();
    mask_7._off = true;
    var mask_7_graphics_1 = new cjs.Graphics().p("AglAdIAAg5IBLAAIAAA5g");
    var mask_7_graphics_2 = new cjs.Graphics().p("AglA6IAAhzIBLAAIAABzg");
    var mask_7_graphics_3 = new cjs.Graphics().p("AgmBhIAAjBIBNAAIAABMIgCAAIAAB1g");
    var mask_7_graphics_4 = new cjs.Graphics().p("Ag/BhIAAjBIB/AAIAABMIg0AAIAAB1g");
    var mask_7_graphics_5 = new cjs.Graphics().p("AhjBhIAAjBIDHAAIAABMIh7AAIAAB1g");
    var mask_7_graphics_6 = new cjs.Graphics().p("AiIBhIAAjBIERAAIAABMIjGAAIAAB1g");
    var mask_7_graphics_7 = new cjs.Graphics().p("AitBhIAAjBIFbAAIAABMIkPAAIAAB1g");
    var mask_7_graphics_8 = new cjs.Graphics().p("AjUBhIAAjBIGpAAIAABMIldAAIAAB1g");
    var mask_7_graphics_9 = new cjs.Graphics().p("Aj2BhIAAjBIHuAAIAABMImjAAIAAB1g");
    var mask_7_graphics_10 = new cjs.Graphics().p("AkaBhIAAjBII2AAIAABMInrAAIAAB1g");
    var mask_7_graphics_11 = new cjs.Graphics().p("AlABhIAAjBIKBAAIAABMIo1AAIAAB1g");
    var mask_7_graphics_12 = new cjs.Graphics().p("AlrBhIAAjBILXAAIAABMIqLAAIAAB1g");
    var mask_7_graphics_13 = new cjs.Graphics().p("AmTBhIAAjBIMnAAIAABMIrbAAIAAB1g");
    var mask_7_graphics_14 = new cjs.Graphics().p("Am5BhIAAjBIN0AAIAABMIspAAIAAB1g");
    var mask_7_graphics_15 = new cjs.Graphics().p("Am5BhIAAjBIN0AAIAAB7IhMAAIAAgvIrdAAIAAB1g");
    var mask_7_graphics_16 = new cjs.Graphics().p("AFvBiIAAh2IrdAAIAAB1IhLAAIAAjCIN0AAIAADDg");
    var mask_7_graphics_17 = new cjs.Graphics().p("AFvCHIAAjBIrdAAIAAB1IhLAAIAAjBIN0AAIAAENg");
    var mask_7_graphics_18 = new cjs.Graphics().p("AEhCHIAAhMIBOAAIAAh1IrdAAIAAB1IhLAAIAAjBIN0AAIAAENg");
    var mask_7_graphics_19 = new cjs.Graphics().p("ADgCHIAAhMICPAAIAAh1IrdAAIAAB1IhLAAIAAjBIN0AAIAAENg");
    var mask_7_graphics_20 = new cjs.Graphics().p("ACcCHIAAhMIDTAAIAAh1IrdAAIAAB1IhLAAIAAjBIN0AAIAAENg");
    var mask_7_graphics_21 = new cjs.Graphics().p("ABOCHIAAhMIEhAAIAAh1IrdAAIAAB1IhLAAIAAjBIN0AAIAAENg");
    var mask_7_graphics_22 = new cjs.Graphics().p("AAFCHIAAhMIFqAAIAAh1IrdAAIAAB1IhLAAIAAjBIN0AAIAAENg");
    var mask_7_graphics_23 = new cjs.Graphics().p("Ag9CHIAAhMIGsAAIAAh1IrdAAIAAB1IhLAAIAAjBIN0AAIAAENg");
    var mask_7_graphics_24 = new cjs.Graphics().p("Ah+CHIAAhMIHtAAIAAh1IrdAAIAAB1IhLAAIAAjBIN0AAIAAENg");
    var mask_7_graphics_25 = new cjs.Graphics().p("AjNCHIAAhMII8AAIAAh1IrdAAIAAB1IhLAAIAAjBIN0AAIAAENg");
    var mask_7_graphics_26 = new cjs.Graphics().p("AkeCHIAAhMIKNAAIAAh1IrdAAIAAB1IhLAAIAAjBIN0AAIAAENg");
    var mask_7_graphics_27 = new cjs.Graphics().p("AlsCHIAAhMILbAAIAAh1IrdAAIAAB1IhLAAIAAjBIN0AAIAAENg");
    var mask_7_graphics_28 = new cjs.Graphics().p("Am5CHIAAkNIN0AAIAAENgAluA7ILdAAIAAh1IrdAAg");
    var mask_7_graphics_29 = new cjs.Graphics().p("Am5EEIAAhMIBPAAIAABMgAm5AKIAAkNIN0AAIAAENgAluhBILdAAIAAh2IrdAAg");
    var mask_7_graphics_30 = new cjs.Graphics().p("Am5EEIAAhMICYAAIAABMgAm5AKIAAkNIN0AAIAAENgAluhBILdAAIAAh2IrdAAg");
    var mask_7_graphics_31 = new cjs.Graphics().p("Am5EEIAAhMIDiAAIAABMgAm5AKIAAkNIN0AAIAAENgAluhBILdAAIAAh2IrdAAg");
    var mask_7_graphics_32 = new cjs.Graphics().p("Am5EEIAAhMIErAAIAABMgAm5AKIAAkNIN0AAIAAENgAluhBILdAAIAAh2IrdAAg");
    var mask_7_graphics_33 = new cjs.Graphics().p("Am5EEIAAhMIF5AAIAABMgAm5AKIAAkNIN0AAIAAENgAluhBILdAAIAAh2IrdAAg");
    var mask_7_graphics_34 = new cjs.Graphics().p("Am5EEIAAhMIHMAAIAABMgAm5AKIAAkNIN0AAIAAENgAluhBILdAAIAAh2IrdAAg");
    var mask_7_graphics_35 = new cjs.Graphics().p("Am5EEIAAhMIIbAAIAABMgAm5AKIAAkNIN0AAIAAENgAluhBILdAAIAAh2IrdAAg");
    var mask_7_graphics_36 = new cjs.Graphics().p("Am5EEIAAhMIJgAAIAABMgAm5AKIAAkNIN0AAIAAENgAluhBILdAAIAAh2IrdAAg");
    var mask_7_graphics_37 = new cjs.Graphics().p("Am5EEIAAhMIKqAAIAABMgAm5AKIAAkNIN0AAIAAENgAluhBILdAAIAAh2IrdAAg");
    var mask_7_graphics_38 = new cjs.Graphics().p("Am5EEIAAhMIL3AAIAABJIhQAAIAAADgAm5AKIAAkNIN0AAIAAENgAluhBILdAAIAAh2IrdAAg");
    var mask_7_graphics_39 = new cjs.Graphics().p("ADyEhIAAg6IqrAAIAAhMIL3AAIAACGgAm5gSIAAkOIN0AAIAAEOgAluheILdAAIAAh2IrdAAg");
    var mask_7_graphics_40 = new cjs.Graphics().p("ADyFAIAAh4IqrAAIAAhMIL3AAIAADEgAm5gxIAAkOIN0AAIAAEOgAluh9ILdAAIAAh2IrdAAg");
    var mask_7_graphics_41 = new cjs.Graphics().p("ADyFlIAAjCIqrAAIAAhMIL3AAIAAEOgAm5hWIAAkOIN0AAIAAEOgAluiiILdAAIAAh2IrdAAg");
    var mask_7_graphics_42 = new cjs.Graphics().p("ACwFlIAAhMIBCAAIAAh2IqrAAIAAhMIL3AAIAAEOgAm5hWIAAkOIN0AAIAAEOgAluiiILdAAIAAh2IrdAAg");
    var mask_7_graphics_43 = new cjs.Graphics().p("ABfFlIAAhMICTAAIAAh2IqrAAIAAhMIL3AAIAAEOgAm5hWIAAkOIN0AAIAAEOgAluiiILdAAIAAh2IrdAAg");
    var mask_7_graphics_44 = new cjs.Graphics().p("AAZFlIAAhMIDZAAIAAh2IqrAAIAAhMIL3AAIAAEOgAm5hWIAAkOIN0AAIAAEOgAluiiILdAAIAAh2IrdAAg");
    var mask_7_graphics_45 = new cjs.Graphics().p("Ag5FlIAAhMIErAAIAAh2IqrAAIAAhMIL3AAIAAEOgAm5hWIAAkOIN0AAIAAEOgAluiiILdAAIAAh2IrdAAg");
    var mask_7_graphics_46 = new cjs.Graphics().p("AiHFlIAAhMIF5AAIAAh2IqrAAIAAhMIL3AAIAAEOgAm5hWIAAkOIN0AAIAAEOgAluiiILdAAIAAh2IrdAAg");
    var mask_7_graphics_47 = new cjs.Graphics().p("AjYFlIAAhMIHKAAIAAh2IqrAAIAAhMIL3AAIAAEOgAm5hWIAAkOIN0AAIAAEOgAluiiILdAAIAAh2IrdAAg");
    var mask_7_graphics_48 = new cjs.Graphics().p("AkfFlIAAhMIIRAAIAAh2IqrAAIAAhMIL3AAIAAEOgAm5hWIAAkOIN0AAIAAEOgAluiiILdAAIAAh2IrdAAg");
    var mask_7_graphics_49 = new cjs.Graphics().p("AlqFlIAAhMIJcAAIAAh2IqrAAIAAhMIL3AAIAAEOgAm5hWIAAkOIN0AAIAAEOgAluiiILdAAIAAh2IrdAAg");
    var mask_7_graphics_50 = new cjs.Graphics().p("AlsFlIAAgDIhNAAIAAhJIKrAAIAAh2IqrAAIAAhMIL3AAIAAEOgAm5hWIAAkOIN0AAIAAEOgAluiiILdAAIAAh2IrdAAg");
    var mask_7_graphics_51 = new cjs.Graphics().p("Am5FzIAAhoIKrAAIAAh1IqrAAIAAhNIL3AAIAAEOIqsAAIAAAcgAm5hkIAAkOIN0AAIAAEOgAluiwILdAAIAAh2IrdAAg");
    var mask_7_graphics_52 = new cjs.Graphics().p("Am5GXIAAiwIKrAAIAAh1IqrAAIAAhMIL3AAIAAEOIqsAAIAABjgAm5iIIAAkOIN0AAIAAEOgAlujTILdAAIAAh2IrdAAg");
    var mask_7_graphics_53 = new cjs.Graphics().p("Am5GzIAAjoIKrAAIAAh2IqrAAIAAhMIL3AAIAAEOIqsAAIAACcgAm5ikIAAkOIN0AAIAAEOgAlujwILdAAIAAh2IrdAAg");
    var mask_7_graphics_54 = new cjs.Graphics().p("Am5HUIAAkqIKrAAIAAh2IqrAAIAAhLIL3AAIAAENIqsAAIAADcIACAAIAAACgAm5jFIAAkOIN0AAIAAEOgAlukRILdAAIAAh2IrdAAg");
    var mask_7_graphics_55 = new cjs.Graphics().p("Am5H6IAAl2IKrAAIAAh1IqrAAIAAhLIL3AAIAAENIqsAAIAADcIACAAIAABNgAm5jrIAAkOIN0AAIAAEOgAluk2ILdAAIAAh2IrdAAg");
    var mask_7_graphics_56 = new cjs.Graphics().p("Am5H6IAAl2IKrAAIAAh1IqrAAIAAhLIL3AAIAAENIqsAAIAADcIBNAAIAABNgAm5jrIAAkOIN0AAIAAEOgAluk2ILdAAIAAh2IrdAAg");
    var mask_7_graphics_57 = new cjs.Graphics().p("Am5H6IAAl2IKrAAIAAh1IqrAAIAAhLIL3AAIAAENIqsAAIAADcICWAAIAABNgAm5jrIAAkOIN0AAIAAEOgAluk2ILdAAIAAh2IrdAAg");
    var mask_7_graphics_58 = new cjs.Graphics().p("Am5H6IAAl2IKrAAIAAh1IqrAAIAAhLIL3AAIAAENIqsAAIAADcIDgAAIAABNgAm5jrIAAkOIN0AAIAAEOgAluk2ILdAAIAAh2IrdAAg");
    var mask_7_graphics_59 = new cjs.Graphics().p("Am5H6IAAl2IKrAAIAAh1IqrAAIAAhLIL3AAIAAENIqsAAIAADcIEwAAIAABNgAm5jrIAAkOIN0AAIAAEOgAluk2ILdAAIAAh2IrdAAg");
    var mask_7_graphics_60 = new cjs.Graphics().p("Am5H6IAAl2IKrAAIAAh1IqrAAIAAhLIL3AAIAAENIqsAAIAADcIF0AAIAABNgAm5jrIAAkOIN0AAIAAEOgAluk2ILdAAIAAh2IrdAAg");
    var mask_7_graphics_61 = new cjs.Graphics().p("Am5H6IAAl2IKrAAIAAh1IqrAAIAAhLIL3AAIAAENIqsAAIAADcIHBAAIAABNgAm5jrIAAkOIN0AAIAAEOgAluk2ILdAAIAAh2IrdAAg");
    var mask_7_graphics_62 = new cjs.Graphics().p("Am5H6IAAl2IKrAAIAAh1IqrAAIAAhLIL3AAIAAENIqsAAIAADcIIJAAIAABNgAm5jrIAAkOIN0AAIAAEOgAluk2ILdAAIAAh2IrdAAg");
    var mask_7_graphics_63 = new cjs.Graphics().p("Am5H6IAAl2IKrAAIAAh1IqrAAIAAhLIL3AAIAAENIqsAAIAADcIJaAAIAABNgAm5jrIAAkOIN0AAIAAEOgAluk2ILdAAIAAh2IrdAAg");
    var mask_7_graphics_64 = new cjs.Graphics().p("Am5H6IAAl2IKrAAIAAh1IqrAAIAAhLIL3AAIAAENIqsAAIAADcIKdAAIAABNgAm5jrIAAkOIN0AAIAAEOgAluk2ILdAAIAAh2IrdAAg");
    var mask_7_graphics_65 = new cjs.Graphics().p("Am5H6IAAl2IKrAAIAAh1IqrAAIAAhLIL3AAIAAENIqsAAIAADcILsAAIAABNgAm5jrIAAkOIN0AAIAAEOgAluk2ILdAAIAAh2IrdAAg");
    var mask_7_graphics_66 = new cjs.Graphics().p("AnBH6IAAl2IKrAAIAAh1IqrAAIAAhLIL3AAIAAENIqrAAIAADcIM3AAIAABNgAnBjrIAAkOIN0AAIAAEOgAl1k2ILcAAIAAh2IrcAAg");
    var mask_7_graphics_67 = new cjs.Graphics().p("AF2ITIAAgzIs3AAIAAl1IKrAAIAAh0IqrAAIAAhNIL3AAIAAENIqrAAIAADdIM3AAIAAB/gAnBkEIAAkOIN0AAIAAEOgAl1lQILcAAIAAh2IrcAAg");
    var mask_7_graphics_68 = new cjs.Graphics().p("AFyI1IAAgCIAEAAIAAh1Is3AAIAAl1IKrAAIAAh1IqrAAIAAhMIL3AAIAAENIqrAAIAADcIM3AAIAADEgAnBkmIAAkOIN0AAIAAEOgAl1lyILcAAIAAh2IrcAAg");
    var mask_7_graphics_69 = new cjs.Graphics().p("AFyJaIAAhMIAEAAIAAh1Is3AAIAAl1IKrAAIAAh1IqrAAIAAhMIL3AAIAAENIqrAAIAADcIM3AAIAAEOgAnBlLIAAkOIN0AAIAAEOgAl1mXILcAAIAAh2IrcAAg");
    var mask_7_graphics_70 = new cjs.Graphics().p("AElJaIAAhMIBRAAIAAh1Is3AAIAAl1IKrAAIAAh1IqrAAIAAhMIL3AAIAAENIqrAAIAADcIM3AAIAAEOgAnBlLIAAkOIN0AAIAAEOgAl1mXILcAAIAAh2IrcAAg");
    var mask_7_graphics_71 = new cjs.Graphics().p("ADUJaIAAhMICiAAIAAh1Is3AAIAAl1IKrAAIAAh1IqrAAIAAhMIL3AAIAAENIqrAAIAADcIM3AAIAAEOgAnBlLIAAkOIN0AAIAAEOgAl1mXILcAAIAAh2IrcAAg");
    var mask_7_graphics_72 = new cjs.Graphics().p("AB+JaIAAhMID4AAIAAh1Is3AAIAAl1IKrAAIAAh1IqrAAIAAhMIL3AAIAAENIqrAAIAADcIM3AAIAAEOgAnBlLIAAkOIN0AAIAAEOgAl1mXILcAAIAAh2IrcAAg");
    var mask_7_graphics_73 = new cjs.Graphics().p("AAkJaIAAhMIFSAAIAAh1Is3AAIAAl1IKrAAIAAh1IqrAAIAAhMIL3AAIAAENIqrAAIAADcIM3AAIAAEOgAnBlLIAAkOIN0AAIAAEOgAl1mXILcAAIAAh2IrcAAg");
    var mask_7_graphics_74 = new cjs.Graphics().p("AhDJaIAAhMIG5AAIAAh1Is3AAIAAl1IKrAAIAAh1IqrAAIAAhMIL3AAIAAENIqrAAIAADcIM3AAIAAEOgAnBlLIAAkOIN0AAIAAEOgAl1mXILcAAIAAh2IrcAAg");
    var mask_7_graphics_75 = new cjs.Graphics().p("AidJaIAAhMIITAAIAAh1Is3AAIAAl1IKrAAIAAh1IqrAAIAAhMIL3AAIAAENIqrAAIAADcIM3AAIAAEOgAnBlLIAAkOIN0AAIAAEOgAl1mXILcAAIAAh2IrcAAg");
    var mask_7_graphics_76 = new cjs.Graphics().p("AkFJaIAAhMIJ7AAIAAh1Is3AAIAAl1IKrAAIAAh1IqrAAIAAhMIL3AAIAAENIqrAAIAADcIM3AAIAAEOgAnBlLIAAkOIN0AAIAAEOgAl1mXILcAAIAAh2IrcAAg");
    var mask_7_graphics_77 = new cjs.Graphics().p("AlfJaIAAhMILVAAIAAh1Is3AAIAAl1IKrAAIAAh1IqrAAIAAhMIL3AAIAAENIqrAAIAADcIM3AAIAAEOgAnBlLIAAkOIN0AAIAAEOgAl1mXILcAAIAAh2IrcAAg");
    var mask_7_graphics_78 = new cjs.Graphics().p("AnBJaIAAhMIM3AAIAAh1Is3AAIAAl1IKrAAIAAh1IqrAAIAAhMIL3AAIAAENIqrAAIAADcIM3AAIAAEOgAnBlLIAAkOIN0AAIAAEOgAl1mXILcAAIAAh2IrcAAg");
    var mask_7_graphics_119 = new cjs.Graphics().p("AnBJaIAAhMIM3AAIAAh1Is3AAIAAl1IKrAAIAAh1IqrAAIAAhMIL3AAIAAENIqrAAIAADcIM3AAIAAEOgAnBlLIAAkOIN0AAIAAEOgAl1mXILcAAIAAh2IrcAAg");
    var mask_7_graphics_120 = new cjs.Graphics().p("AnBJaIAAhMIM3AAIAAh1Is3AAIAAl1IKrAAIAAh1IqrAAIAAhMIL3AAIAAENIqrAAIAADcIM3AAIAAEOgAnBlLIAAkOIN0AAIAAEOgAl1mXILcAAIAAh2IrcAAg");
    var mask_7_graphics_121 = new cjs.Graphics().p("AnBJaIAAhMIM3AAIAAh1Is3AAIAAl1IKrAAIAAh1IqrAAIAAhMIL3AAIAAENIqrAAIAADcIM3AAIAAEOgAnBlLIAAkOIN0AAIAAEOgAl1mXILcAAIAAh2IrcAAg");
    var mask_7_graphics_122 = new cjs.Graphics().p("AnBJaIAAhMIM3AAIAAh1Is3AAIAAl1IKrAAIAAh1IqrAAIAAhMIL3AAIAAENIqrAAIAADcIM3AAIAAEOgAnBlLIAAkOIN0AAIAAEOgAl1mXILcAAIAAh2IrcAAg");
    var mask_7_graphics_123 = new cjs.Graphics().p("AnBJaIAAhMIM3AAIAAh1Is3AAIAAl1IKrAAIAAh1IqrAAIAAhMIL3AAIAAENIqrAAIAADcIM3AAIAAEOgAnBlLIAAkOIN0AAIAAEOgAl1mXILcAAIAAh2IrcAAg");
    var mask_7_graphics_124 = new cjs.Graphics().p("AnBJaIAAhMIM3AAIAAh1Is3AAIAAl1IKrAAIAAh1IqrAAIAAhMIL3AAIAAENIqrAAIAADcIM3AAIAAEOgAnBlLIAAkOIN0AAIAAEOgAl1mXILcAAIAAh2IrcAAg");
    var mask_7_graphics_125 = new cjs.Graphics().p("AnBJaIAAhMIM3AAIAAh1Is3AAIAAl1IKrAAIAAh1IqrAAIAAhMIL3AAIAAENIqrAAIAADcIM3AAIAAEOgAnBlLIAAkOIN0AAIAAEOgAl1mXILcAAIAAh2IrcAAg");
    var mask_7_graphics_126 = new cjs.Graphics().p("AnBJaIAAhMIM3AAIAAh1Is3AAIAAl1IKrAAIAAh1IqrAAIAAhMIL3AAIAAENIqrAAIAADcIM3AAIAAEOgAnBlLIAAkOIN0AAIAAEOgAl1mXILcAAIAAh2IrcAAg");
    var mask_7_graphics_127 = new cjs.Graphics().p("AnBJaIAAhMIM3AAIAAh1Is3AAIAAl1IKrAAIAAh1IqrAAIAAhMIL3AAIAAENIqrAAIAADcIM3AAIAAEOgAnBlLIAAkOIN0AAIAAEOgAl1mXILcAAIAAh2IrcAAg");
    var mask_7_graphics_128 = new cjs.Graphics().p("AnBJaIAAhMIM3AAIAAh1Is3AAIAAl1IKrAAIAAh1IqrAAIAAhMIL3AAIAAENIqrAAIAADcIM3AAIAAEOgAnBlLIAAkOIN0AAIAAEOgAl1mXILcAAIAAh2IrcAAg");
    var mask_7_graphics_129 = new cjs.Graphics().p("AnBJaIAAhMIM3AAIAAh1Is3AAIAAl1IKrAAIAAh1IqrAAIAAhMIL3AAIAAENIqrAAIAADcIM3AAIAAEOgAnBlLIAAkOIN0AAIAAEOgAl1mXILcAAIAAh2IrcAAg");
    var mask_7_graphics_130 = new cjs.Graphics().p("AnBJaIAAhMIM3AAIAAh1Is3AAIAAl1IKrAAIAAh1IqrAAIAAhMIL3AAIAAENIqrAAIAADcIM3AAIAAEOgAnBlLIAAkOIN0AAIAAEOgAl1mXILcAAIAAh2IrcAAg");
    var mask_7_graphics_131 = new cjs.Graphics().p("AnBJaIAAhMIM3AAIAAh1Is3AAIAAl1IKrAAIAAh1IqrAAIAAhMIL3AAIAAENIqrAAIAADcIM3AAIAAEOgAnBlLIAAkOIN0AAIAAEOgAl1mXILcAAIAAh2IrcAAg");
    var mask_7_graphics_132 = new cjs.Graphics().p("AnBJaIAAhMIM3AAIAAh1Is3AAIAAl1IKrAAIAAh1IqrAAIAAhMIL3AAIAAENIqrAAIAADcIM3AAIAAEOgAnBlLIAAkOIN0AAIAAEOgAl1mXILcAAIAAh2IrcAAg");
    var mask_7_graphics_133 = new cjs.Graphics().p("AnBJaIAAhMIM3AAIAAh1Is3AAIAAl1IKrAAIAAh1IqrAAIAAhMIL3AAIAAENIqrAAIAADcIM3AAIAAEOgAnBlLIAAkOIN0AAIAAEOgAl1mXILcAAIAAh2IrcAAg");
    var mask_7_graphics_134 = new cjs.Graphics().p("AnBJaIAAhMIM3AAIAAh1Is3AAIAAl1IKrAAIAAh1IqrAAIAAhMIL3AAIAAENIqrAAIAADcIM3AAIAAEOgAnBlLIAAkOIN0AAIAAEOgAl1mXILcAAIAAh2IrcAAg");
  
    this.timeline.addTween(cjs.Tween.get(mask_7).to({graphics:null,x:0,y:0}).wait(1).to({graphics:mask_7_graphics_1,x:126.8,y:18.1}).wait(1).to({graphics:mask_7_graphics_2,x:126.8,y:15.2}).wait(1).to({graphics:mask_7_graphics_3,x:126.9,y:11.3}).wait(1).to({graphics:mask_7_graphics_4,x:129.4,y:11.3}).wait(1).to({graphics:mask_7_graphics_5,x:133,y:11.3}).wait(1).to({graphics:mask_7_graphics_6,x:136.8,y:11.3}).wait(1).to({graphics:mask_7_graphics_7,x:140.4,y:11.3}).wait(1).to({graphics:mask_7_graphics_8,x:144.3,y:11.3}).wait(1).to({graphics:mask_7_graphics_9,x:147.8,y:11.3}).wait(1).to({graphics:mask_7_graphics_10,x:151.4,y:11.3}).wait(1).to({graphics:mask_7_graphics_11,x:155.1,y:11.3}).wait(1).to({graphics:mask_7_graphics_12,x:159.4,y:11.3}).wait(1).to({graphics:mask_7_graphics_13,x:163.4,y:11.3}).wait(1).to({graphics:mask_7_graphics_14,x:167.3,y:11.3}).wait(1).to({graphics:mask_7_graphics_15,x:167.3,y:11.3}).wait(1).to({graphics:mask_7_graphics_16,x:167.3,y:11.3}).wait(1).to({graphics:mask_7_graphics_17,x:167.3,y:15.1}).wait(1).to({graphics:mask_7_graphics_18,x:167.3,y:15.1}).wait(1).to({graphics:mask_7_graphics_19,x:167.3,y:15.1}).wait(1).to({graphics:mask_7_graphics_20,x:167.3,y:15.1}).wait(1).to({graphics:mask_7_graphics_21,x:167.3,y:15.1}).wait(1).to({graphics:mask_7_graphics_22,x:167.3,y:15.1}).wait(1).to({graphics:mask_7_graphics_23,x:167.3,y:15.1}).wait(1).to({graphics:mask_7_graphics_24,x:167.3,y:15.1}).wait(1).to({graphics:mask_7_graphics_25,x:167.3,y:15.1}).wait(1).to({graphics:mask_7_graphics_26,x:167.3,y:15.1}).wait(1).to({graphics:mask_7_graphics_27,x:167.3,y:15.1}).wait(1).to({graphics:mask_7_graphics_28,x:167.3,y:15.1}).wait(1).to({graphics:mask_7_graphics_29,x:167.3,y:27.6}).wait(1).to({graphics:mask_7_graphics_30,x:167.3,y:27.6}).wait(1).to({graphics:mask_7_graphics_31,x:167.3,y:27.6}).wait(1).to({graphics:mask_7_graphics_32,x:167.3,y:27.6}).wait(1).to({graphics:mask_7_graphics_33,x:167.3,y:27.6}).wait(1).to({graphics:mask_7_graphics_34,x:167.3,y:27.6}).wait(1).to({graphics:mask_7_graphics_35,x:167.3,y:27.6}).wait(1).to({graphics:mask_7_graphics_36,x:167.3,y:27.6}).wait(1).to({graphics:mask_7_graphics_37,x:167.3,y:27.6}).wait(1).to({graphics:mask_7_graphics_38,x:167.3,y:27.6}).wait(1).to({graphics:mask_7_graphics_39,x:167.3,y:30.5}).wait(1).to({graphics:mask_7_graphics_40,x:167.3,y:33.6}).wait(1).to({graphics:mask_7_graphics_41,x:167.3,y:37.3}).wait(1).to({graphics:mask_7_graphics_42,x:167.3,y:37.3}).wait(1).to({graphics:mask_7_graphics_43,x:167.3,y:37.3}).wait(1).to({graphics:mask_7_graphics_44,x:167.3,y:37.3}).wait(1).to({graphics:mask_7_graphics_45,x:167.3,y:37.3}).wait(1).to({graphics:mask_7_graphics_46,x:167.3,y:37.3}).wait(1).to({graphics:mask_7_graphics_47,x:167.3,y:37.3}).wait(1).to({graphics:mask_7_graphics_48,x:167.3,y:37.3}).wait(1).to({graphics:mask_7_graphics_49,x:167.3,y:37.3}).wait(1).to({graphics:mask_7_graphics_50,x:167.3,y:37.3}).wait(1).to({graphics:mask_7_graphics_51,x:167.3,y:38.7}).wait(1).to({graphics:mask_7_graphics_52,x:167.3,y:42.2}).wait(1).to({graphics:mask_7_graphics_53,x:167.3,y:45.1}).wait(1).to({graphics:mask_7_graphics_54,x:167.3,y:48.4}).wait(1).to({graphics:mask_7_graphics_55,x:167.3,y:52.1}).wait(1).to({graphics:mask_7_graphics_56,x:167.3,y:52.1}).wait(1).to({graphics:mask_7_graphics_57,x:167.3,y:52.1}).wait(1).to({graphics:mask_7_graphics_58,x:167.3,y:52.1}).wait(1).to({graphics:mask_7_graphics_59,x:167.3,y:52.1}).wait(1).to({graphics:mask_7_graphics_60,x:167.3,y:52.1}).wait(1).to({graphics:mask_7_graphics_61,x:167.3,y:52.1}).wait(1).to({graphics:mask_7_graphics_62,x:167.3,y:52.1}).wait(1).to({graphics:mask_7_graphics_63,x:167.3,y:52.1}).wait(1).to({graphics:mask_7_graphics_64,x:167.3,y:52.1}).wait(1).to({graphics:mask_7_graphics_65,x:167.3,y:52.1}).wait(1).to({graphics:mask_7_graphics_66,x:168,y:52.1}).wait(1).to({graphics:mask_7_graphics_67,x:168,y:54.7}).wait(1).to({graphics:mask_7_graphics_68,x:168,y:58.1}).wait(1).to({graphics:mask_7_graphics_69,x:168,y:61.8}).wait(1).to({graphics:mask_7_graphics_70,x:168,y:61.8}).wait(1).to({graphics:mask_7_graphics_71,x:168,y:61.8}).wait(1).to({graphics:mask_7_graphics_72,x:168,y:61.8}).wait(1).to({graphics:mask_7_graphics_73,x:168,y:61.8}).wait(1).to({graphics:mask_7_graphics_74,x:168,y:61.8}).wait(1).to({graphics:mask_7_graphics_75,x:168,y:61.8}).wait(1).to({graphics:mask_7_graphics_76,x:168,y:61.8}).wait(1).to({graphics:mask_7_graphics_77,x:168,y:61.8}).wait(1).to({graphics:mask_7_graphics_78,x:168,y:61.8}).wait(41).to({graphics:mask_7_graphics_119,x:168,y:61.8}).wait(1).to({graphics:mask_7_graphics_120,x:168,y:61.8}).wait(1).to({graphics:mask_7_graphics_121,x:168,y:61.8}).wait(1).to({graphics:mask_7_graphics_122,x:168,y:61.8}).wait(1).to({graphics:mask_7_graphics_123,x:168,y:61.8}).wait(1).to({graphics:mask_7_graphics_124,x:168,y:61.8}).wait(1).to({graphics:mask_7_graphics_125,x:168,y:61.8}).wait(1).to({graphics:mask_7_graphics_126,x:168,y:61.8}).wait(1).to({graphics:mask_7_graphics_127,x:168,y:61.8}).wait(1).to({graphics:mask_7_graphics_128,x:168,y:61.8}).wait(1).to({graphics:mask_7_graphics_129,x:168,y:61.8}).wait(1).to({graphics:mask_7_graphics_130,x:168,y:61.8}).wait(1).to({graphics:mask_7_graphics_131,x:168,y:61.8}).wait(1).to({graphics:mask_7_graphics_132,x:168,y:61.8}).wait(1).to({graphics:mask_7_graphics_133,x:168,y:61.8}).wait(1).to({graphics:mask_7_graphics_134,x:168,y:61.8}).wait(1));
  
    // E
    this.shape_7 = new cjs.Shape();
    this.shape_7.graphics.f("#fff").s().p("AnBJaIAAhMIM3AAIAAh1Is3AAIAAl1IKrAAIAAh1IqrAAIAAhMIL3AAIAAENIqrAAIAADcIM3AAIAAEOgAnBlLIAAkOIN0AAIAAEOgAl1mXILcAAIAAh2IrcAAg");
    this.shape_7.setTransform(168,61.8);
  
    this.instance_14 = new lib.Tween35("synched",0);
    this.instance_14.parent = this;
    this.instance_14.setTransform(168,61.8);
    this.instance_14._off = true;
  
    this.instance_15 = new lib.Tween36("synched",0);
    this.instance_15.parent = this;
    this.instance_15.setTransform(168,61.8);
    this.instance_15.alpha = 0;
  
    var maskedShapeInstanceList = [this.shape_7,this.instance_14,this.instance_15];
  
    for(var shapedInstanceItr = 0; shapedInstanceItr < maskedShapeInstanceList.length; shapedInstanceItr++) {
      maskedShapeInstanceList[shapedInstanceItr].mask = mask_7;
    }
  
    this.timeline.addTween(cjs.Tween.get({}).to({state:[]}).to({state:[{t:this.shape_7}]},1).to({state:[{t:this.instance_14}]},118).to({state:[{t:this.instance_15}]},15).wait(1));
    this.timeline.addTween(cjs.Tween.get(this.instance_14).wait(119).to({_off:false},0).to({_off:true,alpha:0},15).wait(1));
  
    // H mask (mask)
    var mask_8 = new cjs.Shape();
    mask_8._off = true;
    var mask_8_graphics_1 = new cjs.Graphics().p("AgVAmIAAhLIArAAIAABLg");
    var mask_8_graphics_2 = new cjs.Graphics().p("AhKAmIAAhLICVAAIAABLg");
    var mask_8_graphics_3 = new cjs.Graphics().p("AhvAmIAAhLIDfAAIAABLg");
    var mask_8_graphics_4 = new cjs.Graphics().p("AitAmIAAhLIFbAAIAABLg");
    var mask_8_graphics_5 = new cjs.Graphics().p("AjMAmIAAhLIFnAAIAAAAIAyAAIAABLg");
    var mask_8_graphics_6 = new cjs.Graphics().p("AjzAmIAAhLIHnAAIAABLg");
    var mask_8_graphics_7 = new cjs.Graphics().p("AklAmIAAhLIIfAAIAAAAIAsAAIAABLg");
    var mask_8_graphics_8 = new cjs.Graphics().p("AlNAmIAAhLIKbAAIAABLg");
    var mask_8_graphics_9 = new cjs.Graphics().p("Al1AoIAAhPIK/AAIAAABIAsAAIAABLIqfAAIAAADg");
    var mask_8_graphics_10 = new cjs.Graphics().p("Al1BKIAAiTILrAAIAABLIqfAAIAABIg");
    var mask_8_graphics_11 = new cjs.Graphics().p("Al1BrIAAjVILrAAIAABMIqfAAIAACJg");
    var mask_8_graphics_12 = new cjs.Graphics().p("Al1CTIAAklILrAAIAABMIqfAAIAADZg");
    var mask_8_graphics_13 = new cjs.Graphics().p("Al1CyIAAljILrAAIAABNIqfAAIAAEWg");
    var mask_8_graphics_14 = new cjs.Graphics().p("Al1CyIAAljILrAAIAABNIqfAAIAAEWg");
    var mask_8_graphics_15 = new cjs.Graphics().p("Al1DVIAAmpILrAAIAABMIqfAAIAAFdg");
    var mask_8_graphics_16 = new cjs.Graphics().p("Al1DzIAAnlILrAAIAABNIqfAAIAAGYg");
    var mask_8_graphics_17 = new cjs.Graphics().p("Al1EbIAAo1ILrAAIAABMIqfAAIAAHpg");
    var mask_8_graphics_18 = new cjs.Graphics().p("AmQEbIAAhLIA2AAIAAnqILsAAIAABMIqgAAIAAHpg");
    var mask_8_graphics_19 = new cjs.Graphics().p("AmZEbIAAhIIggAAIAAgkIACAAIAAAhICFAAIAAnqILsAAIAABMIqfAAIAAHpg");
    var mask_8_graphics_20 = new cjs.Graphics().p("AneEbIAAgpIBKAAIAAhDIACAAIAAAhICFAAIAAnqILsAAIAABMIqfAAIAAHpg");
    var mask_8_graphics_21 = new cjs.Graphics().p("AneEbIAAhsIBMAAIAAAhICFAAIAAnqILsAAIAABMIqfAAIAAHpg");
    var mask_8_graphics_22 = new cjs.Graphics().p("AneEbIAAjRIBMAAIAACGICFAAIAAnqILsAAIAABMIqfAAIAAHpg");
    var mask_8_graphics_23 = new cjs.Graphics().p("AneEbIAAkfIBMAAIAADUICFAAIAAnqILsAAIAABMIqfAAIAAHpg");
    var mask_8_graphics_24 = new cjs.Graphics().p("AneEbIAAmaIBMAAIAAFPICFAAIAAnqILsAAIAABMIqfAAIAAHpg");
    var mask_8_graphics_25 = new cjs.Graphics().p("AneEbIAAnsIBMAAIAAGhICFAAIAAnqILsAAIAABMIqfAAIAAHpg");
    var mask_8_graphics_26 = new cjs.Graphics().p("AneE8IAAp3IBMAAIAAIsICFAAIAAnqILsAAIAABMIqfAAIAAHpg");
    var mask_8_graphics_27 = new cjs.Graphics().p("AneFhIAArBIBMAAIAAJ1ICFAAIAAnqILsAAIAABNIqfAAIAAHpg");
    var mask_8_graphics_28 = new cjs.Graphics().p("AneGMIAAsXIBMAAIAALMICFAAIAAnrILsAAIAABNIqfAAIAAHpg");
    var mask_8_graphics_29 = new cjs.Graphics().p("AneGwIAAtfIBMAAIAAMUICFAAIAAnrILsAAIAABNIqfAAIAAHpg");
    var mask_8_graphics_30 = new cjs.Graphics().p("AneHgIAAu/IBMAAIAAN0ICFAAIAAnqILsAAIAABMIqfAAIAAHpg");
    var mask_8_graphics_31 = new cjs.Graphics().p("AneIDIAAwFIBMAAIAAO6ICFAAIAAnrILsAAIAABMIqfAAIAAHqg");
    var mask_8_graphics_32 = new cjs.Graphics().p("AneIuIAAxbIBMAAIAAQQICFAAIAAnqILsAAIAABLIqfAAIAAHqg");
    var mask_8_graphics_33 = new cjs.Graphics().p("AneJaIAAyzIBAAAIAABPIAMAAIAAQZICFAAIAAnrILsAAIAABMIqfAAIAAHqg");
    var mask_8_graphics_34 = new cjs.Graphics().p("AneJaIAAyzICFAAIAABMIg5AAIAAQcICFAAIAAnrILsAAIAABMIqfAAIAAHqg");
    var mask_8_graphics_35 = new cjs.Graphics().p("AneJaIAAyzIDPAAIAABMIiDAAIAAQcICFAAIAAnrILsAAIAABMIqfAAIAAHqg");
    var mask_8_graphics_36 = new cjs.Graphics().p("AneJaIAAyzIEeAAIAABRIhNAAIAAgFIiFAAIAAQcICFAAIAAnrILsAAIAABMIqfAAIAAHqg");
    var mask_8_graphics_37 = new cjs.Graphics().p("AneJaIAAyzIEeAAIAACWIhNAAIAAhKIiFAAIAAQcICFAAIAAnrILsAAIAABMIqfAAIAAHqg");
    var mask_8_graphics_38 = new cjs.Graphics().p("AneJaIAAyzIEeAAIAAD3IhNAAIAAirIiFAAIAAQcICFAAIAAnrILsAAIAABMIqfAAIAAHqg");
    var mask_8_graphics_39 = new cjs.Graphics().p("AneJaIAAyzIEeAAIAAF6IhNAAIAAkuIiFAAIAAQcICFAAIAAnrILsAAIAABMIqfAAIAAHqg");
    var mask_8_graphics_40 = new cjs.Graphics().p("AneJaIAAyzIEeAAIAAG8IAEAAIAAABIhRAAIAAlxIiFAAIAAQcICFAAIAAnrILsAAIAABMIqfAAIAAHqg");
    var mask_8_graphics_41 = new cjs.Graphics().p("AneJaIAAyzIEeAAIAAG8IAEAAIAABMIhRAAIAAm8IiFAAIAAQcICFAAIAAnrILsAAIAABMIqfAAIAAHqg");
    var mask_8_graphics_42 = new cjs.Graphics().p("AneJaIAAyzIEeAAIAAG8IAoAAIAABMIh1AAIAAm8IiFAAIAAQcICFAAIAAnrILsAAIAABMIqfAAIAAHqg");
    var mask_8_graphics_43 = new cjs.Graphics().p("AneJaIAAyzIEeAAIAAG8IB7AAIAABMIjIAAIAAm8IiFAAIAAQcICFAAIAAnrILsAAIAABMIqfAAIAAHqg");
    var mask_8_graphics_44 = new cjs.Graphics().p("AneJaIAAyzIEeAAIAAG8IDAAAIAABMIkNAAIAAm8IiFAAIAAQcICFAAIAAnrILsAAIAABMIqfAAIAAHqg");
    var mask_8_graphics_45 = new cjs.Graphics().p("AneJaIAAyzIEeAAIAAG8IEiAAIAABMIlvAAIAAm8IiFAAIAAQcICFAAIAAnrILsAAIAABMIqfAAIAAHqg");
    var mask_8_graphics_46 = new cjs.Graphics().p("AneJaIAAyzIEeAAIAAG8IFnAAIAABMIm0AAIAAm8IiFAAIAAQcICFAAIAAnrILsAAIAABMIqfAAIAAHqg");
    var mask_8_graphics_47 = new cjs.Graphics().p("AneJaIAAyzIEeAAIAAG8IG2AAIAAABIAQAAIAABLIoTAAIAAm8IiFAAIAAQcICFAAIAAnrILsAAIAABMIqfAAIAAHqg");
    var mask_8_graphics_48 = new cjs.Graphics().p("AneJaIAAyzIEeAAIAAG8IG2AAIAAABIBiAAIAABLIplAAIAAm8IiFAAIAAQcICFAAIAAnrILsAAIAABMIqfAAIAAHqg");
    var mask_8_graphics_49 = new cjs.Graphics().p("AneJaIAAyzIEeAAIAAG8IHMAAIAAhoIBMAAIAAC0IplAAIAAm8IiFAAIAAQcICFAAIAAnrILsAAIAABMIqfAAIAAHqg");
    var mask_8_graphics_50 = new cjs.Graphics().p("AneJaIAAyzIEeAAIAAG8IHMAAIAAi7IBMAAIAAEHIplAAIAAm8IiFAAIAAQcICFAAIAAnrILsAAIAABMIqfAAIAAHqg");
    var mask_8_graphics_51 = new cjs.Graphics().p("AneJaIAAyzIEeAAIAAG8IHMAAIAAkGIBMAAIAAFSIplAAIAAm8IiFAAIAAQcICFAAIAAnrILsAAIAABMIqfAAIAAHqg");
    var mask_8_graphics_52 = new cjs.Graphics().p("AneJaIAAyzIEeAAIAAG8IHMAAIAAlRIBMAAIAAGdIplAAIAAm8IiFAAIAAQcICFAAIAAnrILsAAIAABMIqfAAIAAHqg");
    var mask_8_graphics_53 = new cjs.Graphics().p("AneJaIAAyzIEeAAIAAG8IHMAAIAAm8IBLAAIAABgIABAAIAAGoIplAAIAAm8IiFAAIAAQcICFAAIAAnrILsAAIAABMIqfAAIAAHqg");
    var mask_8_graphics_54 = new cjs.Graphics().p("AneJaIAAyzIEeAAIAAG8IHMAAIAAm8IB5AAIAABMIgtAAIAAG8IplAAIAAm8IiFAAIAAQcICFAAIAAnrILsAAIAABMIqfAAIAAHqg");
    var mask_8_graphics_55 = new cjs.Graphics().p("AneJaIAAyzIEeAAIAAG8IHMAAIAAm8IDDAAIAABMIh3AAIAAG8IplAAIAAm8IiFAAIAAQcICFAAIAAnrILsAAIAABMIqfAAIAAHqg");
    var mask_8_graphics_56 = new cjs.Graphics().p("AoDJaIAAyzIEdAAIAAG8IHMAAIAAm8IEeAAIAABMIjRAAIAAG8IplAAIAAm8IiFAAIAAQcICFAAIAAnrILrAAIAABMIqfAAIAAHqg");
    var mask_8_graphics_57 = new cjs.Graphics().p("AoDJaIAAyzIEdAAIAAG8IHMAAIAAm8IEeAAIAAClIhMAAIAAhZIiFAAIAAG8IplAAIAAm8IiFAAIAAQcICFAAIAAnrILrAAIAABMIqfAAIAAHqg");
    var mask_8_graphics_58 = new cjs.Graphics().p("AoDJaIAAyzIEdAAIAAG8IHMAAIAAm8IEeAAIAADlIhMAAIAAiZIiFAAIAAG8IplAAIAAm8IiFAAIAAQcICFAAIAAnrILrAAIAABMIqfAAIAAHqg");
    var mask_8_graphics_59 = new cjs.Graphics().p("AoDJaIAAyzIEdAAIAAG8IHMAAIAAm8IEeAAIAAE5IhMAAIAAjtIiFAAIAAG8IplAAIAAm8IiFAAIAAQcICFAAIAAnrILrAAIAABMIqfAAIAAHqg");
    var mask_8_graphics_60 = new cjs.Graphics().p("AoDJaIAAyzIEdAAIAAG8IHMAAIAAm8IEeAAIAAF1IhMAAIAAkpIiFAAIAAG8IplAAIAAm8IiFAAIAAQcICFAAIAAnrILrAAIAABMIqfAAIAAHqg");
    var mask_8_graphics_61 = new cjs.Graphics().p("AoDJaIAAyzIEdAAIAAG8IHMAAIAAm8IEeAAIAAG3IhMAAIAAlrIiFAAIAAG8IplAAIAAm8IiFAAIAAQcICFAAIAAnrILrAAIAABMIqfAAIAAHqg");
    var mask_8_graphics_62 = new cjs.Graphics().p("AoDJaIAAyzIEdAAIAAG8IHMAAIAAm8IEeAAIAAHsIhLAAIAAABIgBAAIAAmhIiFAAIAAG8IplAAIAAm8IiFAAIAAQcICFAAIAAnrILrAAIAABMIqfAAIAAHqg");
    var mask_8_graphics_63 = new cjs.Graphics().p("AoDJaIAAyzIEdAAIAAG8IHMAAIAAm8IEeAAIAAI8IhMAAIAAnwIiFAAIAAG8IplAAIAAm8IiFAAIAAQcICFAAIAAnrILnAAIAABMIqbAAIAAHqg");
    var mask_8_graphics_64 = new cjs.Graphics().p("AoDJaIAAyzIEdAAIAAG8IHMAAIAAm8IEeAAIAAJvIhMAAIAAojIiFAAIAAG8IplAAIAAm8IiFAAIAAQcICFAAIAAnrILnAAIAABMIqbAAIAAHqg");
    var mask_8_graphics_65 = new cjs.Graphics().p("AoDJaIAAyzIEdAAIAAG8IHMAAIAAm8IEeAAIAAKzIhPAAIAAAWIqbAAIAAHqgAm3IPICFAAIAAnrILqAAIAAoxIiFAAIAAG8IplAAIAAm8IiFAAg");
    var mask_8_graphics_66 = new cjs.Graphics().p("AoDJaIAAyzIEdAAIAAG8IHMAAIAAm8IEeAAIAALlIhMAAIAAgcIqeAAIAAHqgAm3IPICFAAIAAnrILqAAIAAoxIiFAAIAAG8IplAAIAAm8IiFAAg");
    var mask_8_graphics_67 = new cjs.Graphics().p("AoDJaIAAyzIEdAAIAAG8IHMAAIAAm8IEeAAIAAMYIhMAAIAAhPIqeAAIAAHqgAm3IPICFAAIAAnrILqAAIAAoxIiFAAIAAG8IplAAIAAm8IiFAAg");
    var mask_8_graphics_68 = new cjs.Graphics().p("AoDJaIAAyzIEdAAIAAG8IHMAAIAAm8IEeAAIAANWIhMAAIAAiNIqeAAIAAHqgAm3IPICFAAIAAnrILqAAIAAoxIiFAAIAAG8IplAAIAAm8IiFAAg");
    var mask_8_graphics_69 = new cjs.Graphics().p("AoDJaIAAyzIEdAAIAAG8IHMAAIAAm8IEeAAIAAOtIhMAAIAAjkIqeAAIAAHqgAm3IPICFAAIAAnrILqAAIAAoxIiFAAIAAG8IplAAIAAm8IiFAAg");
    var mask_8_graphics_70 = new cjs.Graphics().p("AoDJaIAAyzIEdAAIAAG8IHMAAIAAm8IEeAAIAAP5IhLAAIAAgfIgBAAIAAkRIqeAAIAAHqgAm3IPICFAAIAAnrILqAAIAAoxIiFAAIAAG8IplAAIAAm8IiFAAgAEyGBIAAgNIABAAIAAANg");
    var mask_8_graphics_71 = new cjs.Graphics().p("AoDJaIAAyzIEdAAIAAG8IHMAAIAAm8IEeAAIAAP5IhLAAIAAgfIgBAAIAAkRIqeAAIAAHqgAm3IPICFAAIAAnrILqAAIAAoxIiFAAIAAG8IplAAIAAm8IiFAAgAEyGBIAAgNIABAAIAAANg");
    var mask_8_graphics_72 = new cjs.Graphics().p("AoDJaIAAyzIEdAAIAAG8IHMAAIAAm8IEeAAIAAQuIhLAAIAAhUIgBAAIAAkRIqeAAIAAHqgAm3IPICFAAIAAnrILqAAIAAoxIiFAAIAAG8IplAAIAAm8IiFAAgAEyGBIAAgNIABAAIAAANg");
    var mask_8_graphics_73 = new cjs.Graphics().p("AoDJaIAAyzIEdAAIAAG8IHMAAIAAm8IEeAAIAASEIhLAAIAAiqIgBAAIAAkRIqeAAIAAHqgAm3IPICFAAIAAnrILqAAIAAoxIiFAAIAAG8IplAAIAAm8IiFAAgAEyGBIAAgNIABAAIAAANg");
    var mask_8_graphics_74 = new cjs.Graphics().p("AFwJaIAAhLIBIAAIAAmfIqeAAIAAHqIkdAAIAAyzIEdAAIAAG8IHMAAIAAm8IEeAAIAASzgAm3IPICFAAIAAnrILqAAIAAoxIiFAAIAAG8IplAAIAAm8IiFAAgAEyHTIAAhfIABAAIAABfg");
    var mask_8_graphics_75 = new cjs.Graphics().p("AEyJaIAAjmIABAAIAACbICFAAIAAmfIqeAAIAAHqIkdAAIAAyzIEdAAIAAG8IHMAAIAAm8IEeAAIAASzgAm3IPICFAAIAAnrILqAAIAAoxIiFAAIAAG8IplAAIAAm8IiFAAg");
    var mask_8_graphics_76 = new cjs.Graphics().p("ADmJaIAAhKIBYAAIAAgBIB6AAIAAmfIqeAAIAAHqIkdAAIAAyzIEdAAIAAG8IHMAAIAAm8IEeAAIAASzgAm3IPICFAAIAAnrILqAAIAAoxIiFAAIAAG8IplAAIAAm8IiFAAg");
    var mask_8_graphics_77 = new cjs.Graphics().p("ADmJaIAAiJIBNAAIAAA+ICFAAIAAmfIqeAAIAAHqIkdAAIAAyzIEdAAIAAG8IHMAAIAAm8IEeAAIAASzgAm3IPICFAAIAAnrILqAAIAAoxIiFAAIAAG8IplAAIAAm8IiFAAg");
    var mask_8_graphics_78 = new cjs.Graphics().p("ADmJaIAAjmIBNAAIAACbICFAAIAAmfIqeAAIAAHqIkdAAIAAyzIEdAAIAAG8IHMAAIAAm8IEeAAIAASzgAm3IPICFAAIAAnrILqAAIAAoxIiFAAIAAG8IplAAIAAm8IiFAAg");
    var mask_8_graphics_79 = new cjs.Graphics().p("ADmJaIAAkwIBNAAIAADlICFAAIAAmfIqeAAIAAHqIkdAAIAAyzIEdAAIAAG8IHMAAIAAm8IEeAAIAASzgAm3IPICFAAIAAnrILqAAIAAoxIiFAAIAAG8IplAAIAAm8IiFAAg");
    var mask_8_graphics_80 = new cjs.Graphics().p("ADmJaIAAmKIBNAAIAAE/ICFAAIAAmfIqeAAIAAHqIkdAAIAAyzIEdAAIAAG8IHMAAIAAm8IEeAAIAASzgAm3IPICFAAIAAnrILqAAIAAoxIiFAAIAAG8IplAAIAAm8IiFAAg");
    var mask_8_graphics_119 = new cjs.Graphics().p("ADmJaIAAmKIBNAAIAAE/ICFAAIAAmfIqeAAIAAHqIkdAAIAAyzIEdAAIAAG8IHMAAIAAm8IEeAAIAASzgAm3IPICFAAIAAnrILqAAIAAoxIiFAAIAAG8IplAAIAAm8IiFAAg");
    var mask_8_graphics_120 = new cjs.Graphics().p("ADmJaIAAmKIBNAAIAAE/ICFAAIAAmfIqeAAIAAHqIkdAAIAAyzIEdAAIAAG8IHMAAIAAm8IEeAAIAASzgAm3IPICFAAIAAnrILqAAIAAoxIiFAAIAAG8IplAAIAAm8IiFAAg");
    var mask_8_graphics_121 = new cjs.Graphics().p("ADmJaIAAmKIBNAAIAAE/ICFAAIAAmfIqeAAIAAHqIkdAAIAAyzIEdAAIAAG8IHMAAIAAm8IEeAAIAASzgAm3IPICFAAIAAnrILqAAIAAoxIiFAAIAAG8IplAAIAAm8IiFAAg");
    var mask_8_graphics_122 = new cjs.Graphics().p("ADmJaIAAmKIBNAAIAAE/ICFAAIAAmfIqeAAIAAHqIkdAAIAAyzIEdAAIAAG8IHMAAIAAm8IEeAAIAASzgAm3IPICFAAIAAnrILqAAIAAoxIiFAAIAAG8IplAAIAAm8IiFAAg");
    var mask_8_graphics_123 = new cjs.Graphics().p("ADmJaIAAmKIBNAAIAAE/ICFAAIAAmfIqeAAIAAHqIkdAAIAAyzIEdAAIAAG8IHMAAIAAm8IEeAAIAASzgAm3IPICFAAIAAnrILqAAIAAoxIiFAAIAAG8IplAAIAAm8IiFAAg");
    var mask_8_graphics_124 = new cjs.Graphics().p("ADmJaIAAmKIBNAAIAAE/ICFAAIAAmfIqeAAIAAHqIkdAAIAAyzIEdAAIAAG8IHMAAIAAm8IEeAAIAASzgAm3IPICFAAIAAnrILqAAIAAoxIiFAAIAAG8IplAAIAAm8IiFAAg");
    var mask_8_graphics_125 = new cjs.Graphics().p("ADmJaIAAmKIBNAAIAAE/ICFAAIAAmfIqeAAIAAHqIkdAAIAAyzIEdAAIAAG8IHMAAIAAm8IEeAAIAASzgAm3IPICFAAIAAnrILqAAIAAoxIiFAAIAAG8IplAAIAAm8IiFAAg");
    var mask_8_graphics_126 = new cjs.Graphics().p("ADmJaIAAmKIBNAAIAAE/ICFAAIAAmfIqeAAIAAHqIkdAAIAAyzIEdAAIAAG8IHMAAIAAm8IEeAAIAASzgAm3IPICFAAIAAnrILqAAIAAoxIiFAAIAAG8IplAAIAAm8IiFAAg");
    var mask_8_graphics_127 = new cjs.Graphics().p("ADmJaIAAmKIBNAAIAAE/ICFAAIAAmfIqeAAIAAHqIkdAAIAAyzIEdAAIAAG8IHMAAIAAm8IEeAAIAASzgAm3IPICFAAIAAnrILqAAIAAoxIiFAAIAAG8IplAAIAAm8IiFAAg");
    var mask_8_graphics_128 = new cjs.Graphics().p("ADmJaIAAmKIBNAAIAAE/ICFAAIAAmfIqeAAIAAHqIkdAAIAAyzIEdAAIAAG8IHMAAIAAm8IEeAAIAASzgAm3IPICFAAIAAnrILqAAIAAoxIiFAAIAAG8IplAAIAAm8IiFAAg");
    var mask_8_graphics_129 = new cjs.Graphics().p("ADmJaIAAmKIBNAAIAAE/ICFAAIAAmfIqeAAIAAHqIkdAAIAAyzIEdAAIAAG8IHMAAIAAm8IEeAAIAASzgAm3IPICFAAIAAnrILqAAIAAoxIiFAAIAAG8IplAAIAAm8IiFAAg");
    var mask_8_graphics_130 = new cjs.Graphics().p("ADmJaIAAmKIBNAAIAAE/ICFAAIAAmfIqeAAIAAHqIkdAAIAAyzIEdAAIAAG8IHMAAIAAm8IEeAAIAASzgAm3IPICFAAIAAnrILqAAIAAoxIiFAAIAAG8IplAAIAAm8IiFAAg");
    var mask_8_graphics_131 = new cjs.Graphics().p("ADmJaIAAmKIBNAAIAAE/ICFAAIAAmfIqeAAIAAHqIkdAAIAAyzIEdAAIAAG8IHMAAIAAm8IEeAAIAASzgAm3IPICFAAIAAnrILqAAIAAoxIiFAAIAAG8IplAAIAAm8IiFAAg");
    var mask_8_graphics_132 = new cjs.Graphics().p("ADmJaIAAmKIBNAAIAAE/ICFAAIAAmfIqeAAIAAHqIkdAAIAAyzIEdAAIAAG8IHMAAIAAm8IEeAAIAASzgAm3IPICFAAIAAnrILqAAIAAoxIiFAAIAAG8IplAAIAAm8IiFAAg");
    var mask_8_graphics_133 = new cjs.Graphics().p("ADmJaIAAmKIBNAAIAAE/ICFAAIAAmfIqeAAIAAHqIkdAAIAAyzIEdAAIAAG8IHMAAIAAm8IEeAAIAASzgAm3IPICFAAIAAnrILqAAIAAoxIiFAAIAAG8IplAAIAAm8IiFAAg");
    var mask_8_graphics_134 = new cjs.Graphics().p("ADmJaIAAmKIBNAAIAAE/ICFAAIAAmfIqeAAIAAHqIkdAAIAAyzIEdAAIAAG8IHMAAIAAm8IEeAAIAASzgAm3IPICFAAIAAnrILqAAIAAoxIiFAAIAAG8IplAAIAAm8IiFAAg");
  
    this.timeline.addTween(cjs.Tween.get(mask_8).to({graphics:null,x:0,y:0}).wait(1).to({graphics:mask_8_graphics_1,x:93.6,y:69.2}).wait(1).to({graphics:mask_8_graphics_2,x:88.2,y:69.2}).wait(1).to({graphics:mask_8_graphics_3,x:84.6,y:69.2}).wait(1).to({graphics:mask_8_graphics_4,x:78.3,y:69.2}).wait(1).to({graphics:mask_8_graphics_5,x:75.3,y:69.2}).wait(1).to({graphics:mask_8_graphics_6,x:71.4,y:69.2}).wait(1).to({graphics:mask_8_graphics_7,x:66.3,y:69.2}).wait(1).to({graphics:mask_8_graphics_8,x:62.4,y:69.2}).wait(1).to({graphics:mask_8_graphics_9,x:58.3,y:69.3}).wait(1).to({graphics:mask_8_graphics_10,x:58.3,y:72.8}).wait(1).to({graphics:mask_8_graphics_11,x:58.3,y:76.1}).wait(1).to({graphics:mask_8_graphics_12,x:58.3,y:80.1}).wait(1).to({graphics:mask_8_graphics_13,x:58.3,y:83.1}).wait(1).to({graphics:mask_8_graphics_14,x:58.3,y:83.1}).wait(1).to({graphics:mask_8_graphics_15,x:58.3,y:86.7}).wait(1).to({graphics:mask_8_graphics_16,x:58.3,y:89.6}).wait(1).to({graphics:mask_8_graphics_17,x:58.3,y:93.7}).wait(1).to({graphics:mask_8_graphics_18,x:55.6,y:93.7}).wait(1).to({graphics:mask_8_graphics_19,x:51.6,y:93.7}).wait(1).to({graphics:mask_8_graphics_20,x:47.9,y:93.7}).wait(1).to({graphics:mask_8_graphics_21,x:47.9,y:93.7}).wait(1).to({graphics:mask_8_graphics_22,x:47.9,y:93.7}).wait(1).to({graphics:mask_8_graphics_23,x:47.9,y:93.7}).wait(1).to({graphics:mask_8_graphics_24,x:47.9,y:93.7}).wait(1).to({graphics:mask_8_graphics_25,x:47.9,y:93.7}).wait(1).to({graphics:mask_8_graphics_26,x:47.9,y:90.4}).wait(1).to({graphics:mask_8_graphics_27,x:47.9,y:86.7}).wait(1).to({graphics:mask_8_graphics_28,x:47.9,y:82.4}).wait(1).to({graphics:mask_8_graphics_29,x:47.9,y:78.8}).wait(1).to({graphics:mask_8_graphics_30,x:47.9,y:74}).wait(1).to({graphics:mask_8_graphics_31,x:47.9,y:70.5}).wait(1).to({graphics:mask_8_graphics_32,x:47.9,y:66.2}).wait(1).to({graphics:mask_8_graphics_33,x:47.9,y:61.8}).wait(1).to({graphics:mask_8_graphics_34,x:47.9,y:61.8}).wait(1).to({graphics:mask_8_graphics_35,x:47.9,y:61.8}).wait(1).to({graphics:mask_8_graphics_36,x:47.9,y:61.8}).wait(1).to({graphics:mask_8_graphics_37,x:47.9,y:61.8}).wait(1).to({graphics:mask_8_graphics_38,x:47.9,y:61.8}).wait(1).to({graphics:mask_8_graphics_39,x:47.9,y:61.8}).wait(1).to({graphics:mask_8_graphics_40,x:47.9,y:61.8}).wait(1).to({graphics:mask_8_graphics_41,x:47.9,y:61.8}).wait(1).to({graphics:mask_8_graphics_42,x:47.9,y:61.8}).wait(1).to({graphics:mask_8_graphics_43,x:47.9,y:61.8}).wait(1).to({graphics:mask_8_graphics_44,x:47.9,y:61.8}).wait(1).to({graphics:mask_8_graphics_45,x:47.9,y:61.8}).wait(1).to({graphics:mask_8_graphics_46,x:47.9,y:61.8}).wait(1).to({graphics:mask_8_graphics_47,x:47.9,y:61.8}).wait(1).to({graphics:mask_8_graphics_48,x:47.9,y:61.8}).wait(1).to({graphics:mask_8_graphics_49,x:47.9,y:61.8}).wait(1).to({graphics:mask_8_graphics_50,x:47.9,y:61.8}).wait(1).to({graphics:mask_8_graphics_51,x:47.9,y:61.8}).wait(1).to({graphics:mask_8_graphics_52,x:47.9,y:61.8}).wait(1).to({graphics:mask_8_graphics_53,x:47.9,y:61.8}).wait(1).to({graphics:mask_8_graphics_54,x:47.9,y:61.8}).wait(1).to({graphics:mask_8_graphics_55,x:47.9,y:61.8}).wait(1).to({graphics:mask_8_graphics_56,x:51.6,y:61.8}).wait(1).to({graphics:mask_8_graphics_57,x:51.6,y:61.8}).wait(1).to({graphics:mask_8_graphics_58,x:51.6,y:61.8}).wait(1).to({graphics:mask_8_graphics_59,x:51.6,y:61.8}).wait(1).to({graphics:mask_8_graphics_60,x:51.6,y:61.8}).wait(1).to({graphics:mask_8_graphics_61,x:51.6,y:61.8}).wait(1).to({graphics:mask_8_graphics_62,x:51.6,y:61.8}).wait(1).to({graphics:mask_8_graphics_63,x:51.6,y:61.8}).wait(1).to({graphics:mask_8_graphics_64,x:51.6,y:61.8}).wait(1).to({graphics:mask_8_graphics_65,x:51.6,y:61.8}).wait(1).to({graphics:mask_8_graphics_66,x:51.6,y:61.8}).wait(1).to({graphics:mask_8_graphics_67,x:51.6,y:61.8}).wait(1).to({graphics:mask_8_graphics_68,x:51.6,y:61.8}).wait(1).to({graphics:mask_8_graphics_69,x:51.6,y:61.8}).wait(1).to({graphics:mask_8_graphics_70,x:51.6,y:61.8}).wait(1).to({graphics:mask_8_graphics_71,x:51.6,y:61.8}).wait(1).to({graphics:mask_8_graphics_72,x:51.6,y:61.8}).wait(1).to({graphics:mask_8_graphics_73,x:51.6,y:61.8}).wait(1).to({graphics:mask_8_graphics_74,x:51.6,y:61.8}).wait(1).to({graphics:mask_8_graphics_75,x:51.6,y:61.8}).wait(1).to({graphics:mask_8_graphics_76,x:51.6,y:61.8}).wait(1).to({graphics:mask_8_graphics_77,x:51.6,y:61.8}).wait(1).to({graphics:mask_8_graphics_78,x:51.6,y:61.8}).wait(1).to({graphics:mask_8_graphics_79,x:51.6,y:61.8}).wait(1).to({graphics:mask_8_graphics_80,x:51.6,y:61.8}).wait(39).to({graphics:mask_8_graphics_119,x:51.6,y:61.8}).wait(1).to({graphics:mask_8_graphics_120,x:51.6,y:61.8}).wait(1).to({graphics:mask_8_graphics_121,x:51.6,y:61.8}).wait(1).to({graphics:mask_8_graphics_122,x:51.6,y:61.8}).wait(1).to({graphics:mask_8_graphics_123,x:51.6,y:61.8}).wait(1).to({graphics:mask_8_graphics_124,x:51.6,y:61.8}).wait(1).to({graphics:mask_8_graphics_125,x:51.6,y:61.8}).wait(1).to({graphics:mask_8_graphics_126,x:51.6,y:61.8}).wait(1).to({graphics:mask_8_graphics_127,x:51.6,y:61.8}).wait(1).to({graphics:mask_8_graphics_128,x:51.6,y:61.8}).wait(1).to({graphics:mask_8_graphics_129,x:51.6,y:61.8}).wait(1).to({graphics:mask_8_graphics_130,x:51.6,y:61.8}).wait(1).to({graphics:mask_8_graphics_131,x:51.6,y:61.8}).wait(1).to({graphics:mask_8_graphics_132,x:51.6,y:61.8}).wait(1).to({graphics:mask_8_graphics_133,x:51.6,y:61.8}).wait(1).to({graphics:mask_8_graphics_134,x:51.6,y:61.8}).wait(1));
  
    // H
    this.shape_8 = new cjs.Shape();
    this.shape_8.graphics.f("#fff").s().p("ADmJaIAAmKIBNAAIAAE/ICFAAIAAmfIqeAAIAAHqIkdAAIAAyzIEdAAIAAG8IHMAAIAAm8IEeAAIAASzgAm3IPICFAAIAAnrILqAAIAAoxIiFAAIAAG8IplAAIAAm8IiFAAg");
    this.shape_8.setTransform(51.6,61.8);
  
    this.instance_16 = new lib.Tween39("synched",0);
    this.instance_16.parent = this;
    this.instance_16.setTransform(51.6,61.8);
    this.instance_16._off = true;
  
    this.instance_17 = new lib.Tween40("synched",0);
    this.instance_17.parent = this;
    this.instance_17.setTransform(51.6,61.8);
    this.instance_17.alpha = 0;
  
    var maskedShapeInstanceList = [this.shape_8,this.instance_16,this.instance_17];
  
    for(var shapedInstanceItr = 0; shapedInstanceItr < maskedShapeInstanceList.length; shapedInstanceItr++) {
      maskedShapeInstanceList[shapedInstanceItr].mask = mask_8;
    }
  
    this.timeline.addTween(cjs.Tween.get({}).to({state:[]}).to({state:[{t:this.shape_8}]},1).to({state:[{t:this.instance_16}]},118).to({state:[{t:this.instance_17}]},15).wait(1));
    this.timeline.addTween(cjs.Tween.get(this.instance_16).wait(119).to({_off:false},0).to({_off:true,alpha:0},15).wait(1));
  
  }).prototype = p = new cjs.MovieClip();
  p.nominalBounds = null;
  
  
  (lib.Acultureofopportunity = function(mode,startPosition,loop) {
    this.initialize(mode,startPosition,loop,{});
  
    // Y
    this.instance = new lib.Tween43("synched",0);
    this.instance.parent = this;
    this.instance.setTransform(742.4,12.5);
    this.instance.alpha = 0;
    this.instance._off = true;
  
    this.instance_1 = new lib.Tween44("synched",0);
    this.instance_1.parent = this;
    this.instance_1.setTransform(742.4,12.5);
  
    this.timeline.addTween(cjs.Tween.get({}).to({state:[]}).to({state:[{t:this.instance}]},75).to({state:[{t:this.instance_1}]},9).to({state:[]},31).wait(20));
    this.timeline.addTween(cjs.Tween.get(this.instance).wait(75).to({_off:false},0).to({_off:true,alpha:1},9).wait(51));
  
    // T
    this.instance_2 = new lib.Tween45("synched",0);
    this.instance_2.parent = this;
    this.instance_2.setTransform(710.2,12.5);
    this.instance_2.alpha = 0;
    this.instance_2._off = true;
  
    this.instance_3 = new lib.Tween46("synched",0);
    this.instance_3.parent = this;
    this.instance_3.setTransform(710.2,12.5);
  
    this.timeline.addTween(cjs.Tween.get({}).to({state:[]}).to({state:[{t:this.instance_2}]},70).to({state:[{t:this.instance_3}]},9).to({state:[]},37).wait(19));
    this.timeline.addTween(cjs.Tween.get(this.instance_2).wait(70).to({_off:false},0).to({_off:true,alpha:1},9).wait(56));
  
    // I
    this.instance_4 = new lib.Tween47("synched",0);
    this.instance_4.parent = this;
    this.instance_4.setTransform(683.7,12.5);
    this.instance_4.alpha = 0;
    this.instance_4._off = true;
  
    this.instance_5 = new lib.Tween48("synched",0);
    this.instance_5.parent = this;
    this.instance_5.setTransform(683.7,12.5);
  
    this.timeline.addTween(cjs.Tween.get({}).to({state:[]}).to({state:[{t:this.instance_4}]},67).to({state:[{t:this.instance_5}]},9).to({state:[]},41).wait(18));
    this.timeline.addTween(cjs.Tween.get(this.instance_4).wait(67).to({_off:false},0).to({_off:true,alpha:1},9).wait(59));
  
    // N
    this.instance_6 = new lib.Tween49("synched",0);
    this.instance_6.parent = this;
    this.instance_6.setTransform(654,12.5);
    this.instance_6.alpha = 0;
    this.instance_6._off = true;
  
    this.instance_7 = new lib.Tween50("synched",0);
    this.instance_7.parent = this;
    this.instance_7.setTransform(654,12.5);
  
    this.timeline.addTween(cjs.Tween.get({}).to({state:[]}).to({state:[{t:this.instance_6}]},63).to({state:[{t:this.instance_7}]},9).to({state:[]},46).wait(17));
    this.timeline.addTween(cjs.Tween.get(this.instance_6).wait(63).to({_off:false},0).to({_off:true,alpha:1},9).wait(63));
  
    // U
    this.instance_8 = new lib.Tween51("synched",0);
    this.instance_8.parent = this;
    this.instance_8.setTransform(616.7,12.8);
    this.instance_8.alpha = 0;
    this.instance_8._off = true;
  
    this.instance_9 = new lib.Tween52("synched",0);
    this.instance_9.parent = this;
    this.instance_9.setTransform(616.7,12.8);
  
    this.timeline.addTween(cjs.Tween.get({}).to({state:[]}).to({state:[{t:this.instance_8}]},59).to({state:[{t:this.instance_9}]},9).to({state:[]},51).wait(16));
    this.timeline.addTween(cjs.Tween.get(this.instance_8).wait(59).to({_off:false},0).to({_off:true,alpha:1},9).wait(67));
  
    // T
    this.instance_10 = new lib.Tween53("synched",0);
    this.instance_10.parent = this;
    this.instance_10.setTransform(582.8,12.5);
    this.instance_10.alpha = 0;
    this.instance_10._off = true;
  
    this.instance_11 = new lib.Tween54("synched",0);
    this.instance_11.parent = this;
    this.instance_11.setTransform(582.8,12.5);
  
    this.timeline.addTween(cjs.Tween.get({}).to({state:[]}).to({state:[{t:this.instance_10}]},55).to({state:[{t:this.instance_11}]},9).to({state:[]},56).wait(15));
    this.timeline.addTween(cjs.Tween.get(this.instance_10).wait(55).to({_off:false},0).to({_off:true,alpha:1},9).wait(71));
  
    // R
    this.instance_12 = new lib.Tween55("synched",0);
    this.instance_12.parent = this;
    this.instance_12.setTransform(551.6,12.5);
    this.instance_12.alpha = 0;
    this.instance_12._off = true;
  
    this.instance_13 = new lib.Tween56("synched",0);
    this.instance_13.parent = this;
    this.instance_13.setTransform(551.6,12.5);
  
    this.timeline.addTween(cjs.Tween.get({}).to({state:[]}).to({state:[{t:this.instance_12}]},51).to({state:[{t:this.instance_13}]},9).to({state:[]},61).wait(14));
    this.timeline.addTween(cjs.Tween.get(this.instance_12).wait(51).to({_off:false},0).to({_off:true,alpha:1},9).wait(75));
  
    // O
    this.instance_14 = new lib.Tween57("synched",0);
    this.instance_14.parent = this;
    this.instance_14.setTransform(514,12.5);
    this.instance_14.alpha = 0;
    this.instance_14._off = true;
  
    this.instance_15 = new lib.Tween58("synched",0);
    this.instance_15.parent = this;
    this.instance_15.setTransform(514,12.5);
  
    this.timeline.addTween(cjs.Tween.get({}).to({state:[]}).to({state:[{t:this.instance_14}]},47).to({state:[{t:this.instance_15}]},9).to({state:[]},66).wait(13));
    this.timeline.addTween(cjs.Tween.get(this.instance_14).wait(47).to({_off:false},0).to({_off:true,alpha:1},9).wait(79));
  
    // P
    this.instance_16 = new lib.Tween59("synched",0);
    this.instance_16.parent = this;
    this.instance_16.setTransform(478.3,12.5);
    this.instance_16.alpha = 0;
    this.instance_16._off = true;
  
    this.instance_17 = new lib.Tween60("synched",0);
    this.instance_17.parent = this;
    this.instance_17.setTransform(478.3,12.5);
  
    this.timeline.addTween(cjs.Tween.get({}).to({state:[]}).to({state:[{t:this.instance_16}]},43).to({state:[{t:this.instance_17}]},9).to({state:[]},71).wait(12));
    this.timeline.addTween(cjs.Tween.get(this.instance_16).wait(43).to({_off:false},0).to({_off:true,alpha:1},9).wait(83));
  
    // P
    this.instance_18 = new lib.Tween61("synched",0);
    this.instance_18.parent = this;
    this.instance_18.setTransform(444.8,12.5);
    this.instance_18.alpha = 0;
    this.instance_18._off = true;
  
    this.instance_19 = new lib.Tween62("synched",0);
    this.instance_19.parent = this;
    this.instance_19.setTransform(444.8,12.5);
  
    this.timeline.addTween(cjs.Tween.get({}).to({state:[]}).to({state:[{t:this.instance_18}]},39).to({state:[{t:this.instance_19}]},9).to({state:[]},76).wait(11));
    this.timeline.addTween(cjs.Tween.get(this.instance_18).wait(39).to({_off:false},0).to({_off:true,alpha:1},9).wait(87));
  
    // O
    this.instance_20 = new lib.Tween63("synched",0);
    this.instance_20.parent = this;
    this.instance_20.setTransform(407.7,12.5);
    this.instance_20.alpha = 0;
    this.instance_20._off = true;
  
    this.instance_21 = new lib.Tween64("synched",0);
    this.instance_21.parent = this;
    this.instance_21.setTransform(407.7,12.5);
  
    this.timeline.addTween(cjs.Tween.get({}).to({state:[]}).to({state:[{t:this.instance_20}]},35).to({state:[{t:this.instance_21}]},9).to({state:[]},81).wait(10));
    this.timeline.addTween(cjs.Tween.get(this.instance_20).wait(35).to({_off:false},0).to({_off:true,alpha:1},9).wait(91));
  
    // F
    this.instance_22 = new lib.Tween41("synched",0);
    this.instance_22.parent = this;
    this.instance_22.setTransform(350.4,12.5);
    this.instance_22.alpha = 0;
    this.instance_22._off = true;
  
    this.timeline.addTween(cjs.Tween.get(this.instance_22).wait(31).to({_off:false},0).to({alpha:1},9).to({_off:true},86).wait(9));
  
    // O
    this.instance_23 = new lib.Tween65("synched",0);
    this.instance_23.parent = this;
    this.instance_23.setTransform(319.4,12.5);
    this.instance_23.alpha = 0;
    this.instance_23._off = true;
  
    this.instance_24 = new lib.Tween66("synched",0);
    this.instance_24.parent = this;
    this.instance_24.setTransform(319.4,12.5);
  
    this.timeline.addTween(cjs.Tween.get({}).to({state:[]}).to({state:[{t:this.instance_23}]},27).to({state:[{t:this.instance_24}]},9).to({state:[]},91).wait(8));
    this.timeline.addTween(cjs.Tween.get(this.instance_23).wait(27).to({_off:false},0).to({_off:true,alpha:1},9).wait(99));
  
    // E
    this.instance_25 = new lib.Tween67("synched",0);
    this.instance_25.parent = this;
    this.instance_25.setTransform(266.9,12.5);
    this.instance_25.alpha = 0;
    this.instance_25._off = true;
  
    this.instance_26 = new lib.Tween68("synched",0);
    this.instance_26.parent = this;
    this.instance_26.setTransform(266.9,12.5);
  
    this.timeline.addTween(cjs.Tween.get({}).to({state:[]}).to({state:[{t:this.instance_25}]},24).to({state:[{t:this.instance_26}]},9).to({state:[]},95).wait(7));
    this.timeline.addTween(cjs.Tween.get(this.instance_25).wait(24).to({_off:false},0).to({_off:true,alpha:1},9).wait(102));
  
    // R
    this.instance_27 = new lib.Tween69("synched",0);
    this.instance_27.parent = this;
    this.instance_27.setTransform(233.6,12.5);
    this.instance_27.alpha = 0;
    this.instance_27._off = true;
  
    this.instance_28 = new lib.Tween70("synched",0);
    this.instance_28.parent = this;
    this.instance_28.setTransform(233.6,12.5);
  
    this.timeline.addTween(cjs.Tween.get({}).to({state:[]}).to({state:[{t:this.instance_27}]},20).to({state:[{t:this.instance_28}]},9).to({state:[]},100).wait(6));
    this.timeline.addTween(cjs.Tween.get(this.instance_27).wait(20).to({_off:false},0).to({_off:true,alpha:1},9).wait(106));
  
    // U
    this.instance_29 = new lib.Tween71("synched",0);
    this.instance_29.parent = this;
    this.instance_29.setTransform(197.1,12.8);
    this.instance_29.alpha = 0;
    this.instance_29._off = true;
  
    this.instance_30 = new lib.Tween72("synched",0);
    this.instance_30.parent = this;
    this.instance_30.setTransform(197.1,12.8);
  
    this.timeline.addTween(cjs.Tween.get({}).to({state:[]}).to({state:[{t:this.instance_29}]},16).to({state:[{t:this.instance_30}]},9).to({state:[]},105).wait(5));
    this.timeline.addTween(cjs.Tween.get(this.instance_29).wait(16).to({_off:false},0).to({_off:true,alpha:1},9).wait(110));
  
    // T
    this.instance_31 = new lib.Tween73("synched",0);
    this.instance_31.parent = this;
    this.instance_31.setTransform(163.2,12.5);
    this.instance_31.alpha = 0;
    this.instance_31._off = true;
  
    this.instance_32 = new lib.Tween74("synched",0);
    this.instance_32.parent = this;
    this.instance_32.setTransform(163.2,12.5);
  
    this.timeline.addTween(cjs.Tween.get({}).to({state:[]}).to({state:[{t:this.instance_31}]},13).to({state:[{t:this.instance_32}]},9).to({state:[]},109).wait(4));
    this.timeline.addTween(cjs.Tween.get(this.instance_31).wait(13).to({_off:false},0).to({_off:true,alpha:1},9).wait(113));
  
    // L
    this.instance_33 = new lib.Tween75("synched",0);
    this.instance_33.parent = this;
    this.instance_33.setTransform(135.6,12.5);
    this.instance_33.alpha = 0;
    this.instance_33._off = true;
  
    this.instance_34 = new lib.Tween76("synched",0);
    this.instance_34.parent = this;
    this.instance_34.setTransform(135.6,12.5);
  
    this.timeline.addTween(cjs.Tween.get({}).to({state:[]}).to({state:[{t:this.instance_33}]},9).to({state:[{t:this.instance_34}]},9).to({state:[]},114).wait(3));
    this.timeline.addTween(cjs.Tween.get(this.instance_33).wait(9).to({_off:false},0).to({_off:true,alpha:1},9).wait(117));
  
    // U
    this.instance_35 = new lib.Tween77("synched",0);
    this.instance_35.parent = this;
    this.instance_35.setTransform(101,12.8);
    this.instance_35.alpha = 0;
    this.instance_35._off = true;
  
    this.instance_36 = new lib.Tween78("synched",0);
    this.instance_36.parent = this;
    this.instance_36.setTransform(101,12.8);
  
    this.timeline.addTween(cjs.Tween.get({}).to({state:[]}).to({state:[{t:this.instance_35}]},6).to({state:[{t:this.instance_36}]},9).to({state:[]},118).wait(2));
    this.timeline.addTween(cjs.Tween.get(this.instance_35).wait(6).to({_off:false},0).to({_off:true,alpha:1},9).wait(120));
  
    // C
    this.instance_37 = new lib.Tween79("synched",0);
    this.instance_37.parent = this;
    this.instance_37.setTransform(65.3,12.5);
    this.instance_37.alpha = 0;
    this.instance_37._off = true;
  
    this.instance_38 = new lib.Tween80("synched",0);
    this.instance_38.parent = this;
    this.instance_38.setTransform(65.3,12.5);
  
    this.timeline.addTween(cjs.Tween.get({}).to({state:[]}).to({state:[{t:this.instance_37}]},3).to({state:[{t:this.instance_38}]},9).to({state:[]},122).wait(1));
    this.timeline.addTween(cjs.Tween.get(this.instance_37).wait(3).to({_off:false},0).to({_off:true,alpha:1},9).wait(123));
  
    // A
    this.instance_39 = new lib.Tween81("synched",0);
    this.instance_39.parent = this;
    this.instance_39.setTransform(12.5,12.5);
    this.instance_39.alpha = 0;
  
    this.instance_40 = new lib.Tween82("synched",0);
    this.instance_40.parent = this;
    this.instance_40.setTransform(12.5,12.5);
  
    this.timeline.addTween(cjs.Tween.get({}).to({state:[{t:this.instance_39}]}).to({state:[{t:this.instance_40}]},9).wait(126));
    this.timeline.addTween(cjs.Tween.get(this.instance_39).to({_off:true,alpha:1},9).wait(126));
  
  }).prototype = p = new cjs.MovieClip();
  p.nominalBounds = new cjs.Rectangle(0,0.7,25,23.6);
  
  
  // stage content:
  (lib.Logoanimation = function(mode,startPosition,loop) {
    this.initialize(mode,startPosition,loop,{});
  
    // HENDERSON
    this.instance = new lib.Symbol1();
    this.instance.parent = this;
  
    this.timeline.addTween(cjs.Tween.get(this.instance).wait(135));
  
    // A CULTURE OF OPPORTUNITY
    this.instance_1 = new lib.Acultureofopportunity();
    this.instance_1.parent = this;
    this.instance_1.setTransform(500,174.3,1,1,0,0,0,377,12.5);
  
    this.timeline.addTween(cjs.Tween.get(this.instance_1).wait(135));
  
  }).prototype = p = new cjs.MovieClip();
  p.nominalBounds = new cjs.Rectangle(623,255.9,25,23.6);
  // library properties:
  lib.properties = {
    id: '329A3B38FA005A488196959DEBA036E3',
    width: 1000,
    height: 187,
    fps: 24,
    color: "#FFFFFF",
    opacity: 1.00,
    manifest: [],
    preloads: []
  };
  
  
  
  // bootstrap callback support:
  
  (lib.Stage = function(canvas) {
    createjs.Stage.call(this, canvas);
  }).prototype = p = new createjs.Stage();
  
  p.setAutoPlay = function(autoPlay) {
    this.tickEnabled = autoPlay;
  }
  p.play = function() { this.tickEnabled = true; this.getChildAt(0).gotoAndPlay(this.getTimelinePosition()) }
  p.stop = function(ms) { if(ms) this.seek(ms); this.tickEnabled = false; }
  p.seek = function(ms) { this.tickEnabled = true; this.getChildAt(0).gotoAndStop(lib.properties.fps * ms / 1000); }
  p.getDuration = function() { return this.getChildAt(0).totalFrames / lib.properties.fps * 1000; }
  
  p.getTimelinePosition = function() { return this.getChildAt(0).currentFrame / lib.properties.fps * 1000; }
  
  an.bootcompsLoaded = an.bootcompsLoaded || [];
  if(!an.bootstrapListeners) {
    an.bootstrapListeners=[];
  }
  
  an.bootstrapCallback=function(fnCallback) {
    an.bootstrapListeners.push(fnCallback);
    if(an.bootcompsLoaded.length > 0) {
      for(var i=0; i<an.bootcompsLoaded.length; ++i) {
        fnCallback(an.bootcompsLoaded[i]);
      }
    }
  };
  
  an.compositions = an.compositions || {};
  an.compositions['329A3B38FA005A488196959DEBA036E3'] = {
    getStage: function() { return exportRoot.getStage(); },
    getLibrary: function() { return lib; },
    getSpriteSheet: function() { return ss; },
    getImages: function() { return img; }
  };
  
  an.compositionLoaded = function(id) {
    an.bootcompsLoaded.push(id);
    for(var j=0; j<an.bootstrapListeners.length; j++) {
      an.bootstrapListeners[j](id);
    }
  }
  
  an.getComposition = function(id) {
    return an.compositions[id];
  }
  
})(createjs = createjs || {}, AdobeAn = AdobeAn || {});

var createjs, AdobeAn;(function (cjs, an) {

  var p; // shortcut to reference prototypes
  var lib={};var ss={};var img={};
  lib.ssMetadata = [];
  
  
  // symbols:
  
  
  
  (lib.Tween82 = function(mode,startPosition,loop) {
    this.initialize(mode,startPosition,loop,{});
  
    // Layer_1
    this.shape = new cjs.Shape();
    this.shape.graphics.f("#fff").s().p("AA3B2IgRgzIhMAAIgRAzIhFAAIBZjrIBGAAIBaDrgAgVARIArAAIgWhFg");
  
    this.timeline.addTween(cjs.Tween.get(this.shape).wait(1));
  
  }).prototype = p = new cjs.MovieClip();
  p.nominalBounds = new cjs.Rectangle(-12.5,-11.8,25,23.6);
  
  
  (lib.Tween81 = function(mode,startPosition,loop) {
    this.initialize(mode,startPosition,loop,{});
  
    // Layer_1
    this.shape = new cjs.Shape();
    this.shape.graphics.f("#fff").s().p("AA3B2IgRgzIhMAAIgRAzIhFAAIBZjrIBGAAIBaDrgAgVARIArAAIgWhFg");
  
    this.timeline.addTween(cjs.Tween.get(this.shape).wait(1));
  
  }).prototype = p = new cjs.MovieClip();
  p.nominalBounds = new cjs.Rectangle(-12.5,-11.8,25,23.6);
  
  
  (lib.Tween80 = function(mode,startPosition,loop) {
    this.initialize(mode,startPosition,loop,{});
  
    // Layer_1
    this.shape = new cjs.Shape();
    this.shape.graphics.f("#fff").s().p("AAPB8Qg2AAgmgkQgmglAAgzQAAgyAmglQAmglA2AAQAdAAAaAMQAbANARAXIgmAoQgYgdglgBQgbABgTAUQgSAVAAAbQABAaATATQATATAZABIAAgBQAngBAZgdIAlApQgSAXgbAMQgZAMgbAAIgEgBg");
  
    this.timeline.addTween(cjs.Tween.get(this.shape).wait(1));
  
  }).prototype = p = new cjs.MovieClip();
  p.nominalBounds = new cjs.Rectangle(-11.6,-12.4,23.3,24.9);
  
  
  (lib.Tween79 = function(mode,startPosition,loop) {
    this.initialize(mode,startPosition,loop,{});
  
    // Layer_1
    this.shape = new cjs.Shape();
    this.shape.graphics.f("#fff").s().p("AAPB8Qg2AAgmgkQgmglAAgzQAAgyAmglQAmglA2AAQAdAAAaAMQAbANARAXIgmAoQgYgdglgBQgbABgTAUQgSAVAAAbQABAaATATQATATAZABIAAgBQAngBAZgdIAlApQgSAXgbAMQgZAMgbAAIgEgBg");
  
    this.timeline.addTween(cjs.Tween.get(this.shape).wait(1));
  
  }).prototype = p = new cjs.MovieClip();
  p.nominalBounds = new cjs.Rectangle(-11.6,-12.4,23.3,24.9);
  
  
  (lib.Tween78 = function(mode,startPosition,loop) {
    this.initialize(mode,startPosition,loop,{});
  
    // Layer_1
    this.shape = new cjs.Shape();
    this.shape.graphics.f("#fff").s().p("AhNBbQgegeAAgsIAAiJIBAAAIAACJQgCASANAOQALAOASABQASACAOgMQANgMACgSIAAiQIBAAAIAACJQAAAsgfAeQgeAegvAAQguAAgfgeg");
  
    this.timeline.addTween(cjs.Tween.get(this.shape).wait(1));
  
  }).prototype = p = new cjs.MovieClip();
  p.nominalBounds = new cjs.Rectangle(-10.8,-12.1,21.6,24.3);
  
  
  (lib.Tween77 = function(mode,startPosition,loop) {
    this.initialize(mode,startPosition,loop,{});
  
    // Layer_1
    this.shape = new cjs.Shape();
    this.shape.graphics.f("#fff").s().p("AhNBbQgegeAAgsIAAiJIBAAAIAACJQgCASANAOQALAOASABQASACAOgMQANgMACgSIAAiQIBAAAIAACJQAAAsgfAeQgeAegvAAQguAAgfgeg");
  
    this.timeline.addTween(cjs.Tween.get(this.shape).wait(1));
  
  }).prototype = p = new cjs.MovieClip();
  p.nominalBounds = new cjs.Rectangle(-10.8,-12.1,21.6,24.3);
  
  
  (lib.Tween76 = function(mode,startPosition,loop) {
    this.initialize(mode,startPosition,loop,{});
  
    // Layer_1
    this.shape = new cjs.Shape();
    this.shape.graphics.f("#fff").s().p("AhTB2IAAjrIBAAAIAACyIBnAAIAAA5g");
  
    this.timeline.addTween(cjs.Tween.get(this.shape).wait(1));
  
  }).prototype = p = new cjs.MovieClip();
  p.nominalBounds = new cjs.Rectangle(-8.4,-11.8,16.9,23.6);
  
  
  (lib.Tween75 = function(mode,startPosition,loop) {
    this.initialize(mode,startPosition,loop,{});
  
    // Layer_1
    this.shape = new cjs.Shape();
    this.shape.graphics.f("#fff").s().p("AhTB2IAAjrIBAAAIAACyIBnAAIAAA5g");
  
    this.timeline.addTween(cjs.Tween.get(this.shape).wait(1));
  
  }).prototype = p = new cjs.MovieClip();
  p.nominalBounds = new cjs.Rectangle(-8.4,-11.8,16.9,23.6);
  
  
  (lib.Tween74 = function(mode,startPosition,loop) {
    this.initialize(mode,startPosition,loop,{});
  
    // Layer_1
    this.shape = new cjs.Shape();
    this.shape.graphics.f("#fff").s().p("AgfB2IAAiyIhCAAIAAg5IDDAAIAAA5IhCAAIAACyg");
  
    this.timeline.addTween(cjs.Tween.get(this.shape).wait(1));
  
  }).prototype = p = new cjs.MovieClip();
  p.nominalBounds = new cjs.Rectangle(-9.8,-11.8,19.6,23.6);
  
  
  (lib.Tween73 = function(mode,startPosition,loop) {
    this.initialize(mode,startPosition,loop,{});
  
    // Layer_1
    this.shape = new cjs.Shape();
    this.shape.graphics.f("#fff").s().p("AgfB2IAAiyIhCAAIAAg5IDDAAIAAA5IhCAAIAACyg");
  
    this.timeline.addTween(cjs.Tween.get(this.shape).wait(1));
  
  }).prototype = p = new cjs.MovieClip();
  p.nominalBounds = new cjs.Rectangle(-9.8,-11.8,19.6,23.6);
  
  
  (lib.Tween72 = function(mode,startPosition,loop) {
    this.initialize(mode,startPosition,loop,{});
  
    // Layer_1
    this.shape = new cjs.Shape();
    this.shape.graphics.f("#fff").s().p("AhMBbQgfgeAAgsIAAiJIBAAAIAACJQgBASAMAOQALAOATABQARACAOgMQANgMACgSQABgEgBgDIAAAAIAAiJIBAAAIAACJQAAAsgfAeQgeAegvAAQgtAAgfgeg");
  
    this.timeline.addTween(cjs.Tween.get(this.shape).wait(1));
  
  }).prototype = p = new cjs.MovieClip();
  p.nominalBounds = new cjs.Rectangle(-10.7,-12.1,21.6,24.3);
  
  
  (lib.Tween71 = function(mode,startPosition,loop) {
    this.initialize(mode,startPosition,loop,{});
  
    // Layer_1
    this.shape = new cjs.Shape();
    this.shape.graphics.f("#fff").s().p("AhMBbQgfgeAAgsIAAiJIBAAAIAACJQgBASAMAOQALAOATABQARACAOgMQANgMACgSQABgEgBgDIAAAAIAAiJIBAAAIAACJQAAAsgfAeQgeAegvAAQgtAAgfgeg");
  
    this.timeline.addTween(cjs.Tween.get(this.shape).wait(1));
  
  }).prototype = p = new cjs.MovieClip();
  p.nominalBounds = new cjs.Rectangle(-10.7,-12.1,21.6,24.3);
  
  
  (lib.Tween70 = function(mode,startPosition,loop) {
    this.initialize(mode,startPosition,loop,{});
  
    // Layer_1
    this.shape = new cjs.Shape();
    this.shape.graphics.f("#fff").s().p("AAjB2IgnhKIgiAAIAABKIhAAAIAAjrIBuAAQAmAAAYAXQAYAYAAAiQABAVgLASQgLATgTAKIAxBWgAgmgNIAqAAQALACAHgHQAHgGABgKQABgKgHgIQgGgHgKgBIguAAg");
  
    this.timeline.addTween(cjs.Tween.get(this.shape).wait(1));
  
  }).prototype = p = new cjs.MovieClip();
  p.nominalBounds = new cjs.Rectangle(-10.3,-11.8,20.6,23.6);
  
  
  (lib.Tween69 = function(mode,startPosition,loop) {
    this.initialize(mode,startPosition,loop,{});
  
    // Layer_1
    this.shape = new cjs.Shape();
    this.shape.graphics.f("#fff").s().p("AAjB2IgnhKIgiAAIAABKIhAAAIAAjrIBuAAQAmAAAYAXQAYAYAAAiQABAVgLASQgLATgTAKIAxBWgAgmgNIAqAAQALACAHgHQAHgGABgKQABgKgHgIQgGgHgKgBIguAAg");
  
    this.timeline.addTween(cjs.Tween.get(this.shape).wait(1));
  
  }).prototype = p = new cjs.MovieClip();
  p.nominalBounds = new cjs.Rectangle(-10.3,-11.8,20.6,23.6);
  
  
  (lib.Tween68 = function(mode,startPosition,loop) {
    this.initialize(mode,startPosition,loop,{});
  
    // Layer_1
    this.shape = new cjs.Shape();
    this.shape.graphics.f("#fff").s().p("AhbB2IAAjrIC1AAIAAA2Ih1AAIAAAlIBZAAIAAAxIhZAAIAAApIB3AAIAAA2g");
  
    this.timeline.addTween(cjs.Tween.get(this.shape).wait(1));
  
  }).prototype = p = new cjs.MovieClip();
  p.nominalBounds = new cjs.Rectangle(-9.2,-11.8,18.5,23.6);
  
  
  (lib.Tween67 = function(mode,startPosition,loop) {
    this.initialize(mode,startPosition,loop,{});
  
    // Layer_1
    this.shape = new cjs.Shape();
    this.shape.graphics.f("#fff").s().p("AhbB2IAAjrIC1AAIAAA2Ih1AAIAAAlIBZAAIAAAxIhZAAIAAApIB3AAIAAA2g");
  
    this.timeline.addTween(cjs.Tween.get(this.shape).wait(1));
  
  }).prototype = p = new cjs.MovieClip();
  p.nominalBounds = new cjs.Rectangle(-9.2,-11.8,18.5,23.6);
  
  
  (lib.Tween66 = function(mode,startPosition,loop) {
    this.initialize(mode,startPosition,loop,{});
  
    // Layer_1
    this.shape = new cjs.Shape();
    this.shape.graphics.f("#fff").s().p("AAAB9QgmgBgfgSQgMgIgMgKQglglgBgzQABgyAlglQAMgKAMgIQAfgTAmAAQA3AAAmAlQAmAlAAAyQABAzgmAlQgnAkg2ABgAgsgwQgUATAAAdIgBAAQAAAbARAUQATATAbABIADAAQAbABAUgTQAVgUAAgcQABgagUgUQgTgVgcgBIgBAAQgaAAgUATg");
  
    this.timeline.addTween(cjs.Tween.get(this.shape).wait(1));
  
  }).prototype = p = new cjs.MovieClip();
  p.nominalBounds = new cjs.Rectangle(-13.1,-12.4,26.3,24.9);
  
  
  (lib.Tween65 = function(mode,startPosition,loop) {
    this.initialize(mode,startPosition,loop,{});
  
    // Layer_1
    this.shape = new cjs.Shape();
    this.shape.graphics.f("#fff").s().p("AAAB9QgmgBgfgSQgMgIgMgKQglglgBgzQABgyAlglQAMgKAMgIQAfgTAmAAQA3AAAmAlQAmAlAAAyQABAzgmAlQgnAkg2ABgAgsgwQgUATAAAdIgBAAQAAAbARAUQATATAbABIADAAQAbABAUgTQAVgUAAgcQABgagUgUQgTgVgcgBIgBAAQgaAAgUATg");
  
    this.timeline.addTween(cjs.Tween.get(this.shape).wait(1));
  
  }).prototype = p = new cjs.MovieClip();
  p.nominalBounds = new cjs.Rectangle(-13.1,-12.4,26.3,24.9);
  
  
  (lib.Tween64 = function(mode,startPosition,loop) {
    this.initialize(mode,startPosition,loop,{});
  
    // Layer_1
    this.shape = new cjs.Shape();
    this.shape.graphics.f("#fff").s().p("AhdBYQgmglAAgzQAAgyAmglQAnglA2AAQA3AAAnAlQAmAlAAAyQAAAzgmAlQgmAkg4ABQg2gBgngkgAgsgwQgUATgBAdIAAAAQgBAbATAUQASATAbABIACAAQAcABAUgTQAVgUAAgcQABgbgTgTQgUgVgcgBIgBAAQgaAAgUATg");
  
    this.timeline.addTween(cjs.Tween.get(this.shape).wait(1));
  
  }).prototype = p = new cjs.MovieClip();
  p.nominalBounds = new cjs.Rectangle(-13.1,-12.4,26.4,24.9);
  
  
  (lib.Tween63 = function(mode,startPosition,loop) {
    this.initialize(mode,startPosition,loop,{});
  
    // Layer_1
    this.shape = new cjs.Shape();
    this.shape.graphics.f("#fff").s().p("AhdBYQgmglAAgzQAAgyAmglQAnglA2AAQA3AAAnAlQAmAlAAAyQAAAzgmAlQgmAkg4ABQg2gBgngkgAgsgwQgUATgBAdIAAAAQgBAbATAUQASATAbABIACAAQAcABAUgTQAVgUAAgcQABgbgTgTQgUgVgcgBIgBAAQgaAAgUATg");
  
    this.timeline.addTween(cjs.Tween.get(this.shape).wait(1));
  
  }).prototype = p = new cjs.MovieClip();
  p.nominalBounds = new cjs.Rectangle(-13.1,-12.4,26.4,24.9);
  
  
  (lib.Tween62 = function(mode,startPosition,loop) {
    this.initialize(mode,startPosition,loop,{});
  
    // Layer_1
    this.shape = new cjs.Shape();
    this.shape.graphics.f("#fff").s().p("AhgB2IAAjrIBpAAQAnAAAYAXQAZAYAAAkQAAAigZAXQgYAYgnAAIgpAAIAABHgAgggJIAmAAQAKABAIgHQAJgHABgKQABgKgIgJQgGgIgLgBIgqAAg");
  
    this.timeline.addTween(cjs.Tween.get(this.shape).wait(1));
  
  }).prototype = p = new cjs.MovieClip();
  p.nominalBounds = new cjs.Rectangle(-9.7,-11.8,19.4,23.6);
  
  
  (lib.Tween61 = function(mode,startPosition,loop) {
    this.initialize(mode,startPosition,loop,{});
  
    // Layer_1
    this.shape = new cjs.Shape();
    this.shape.graphics.f("#fff").s().p("AhgB2IAAjrIBpAAQAnAAAYAXQAZAYAAAkQAAAigZAXQgYAYgnAAIgpAAIAABHgAgggJIAmAAQAKABAIgHQAJgHABgKQABgKgIgJQgGgIgLgBIgqAAg");
  
    this.timeline.addTween(cjs.Tween.get(this.shape).wait(1));
  
  }).prototype = p = new cjs.MovieClip();
  p.nominalBounds = new cjs.Rectangle(-9.7,-11.8,19.4,23.6);
  
  
  (lib.Tween60 = function(mode,startPosition,loop) {
    this.initialize(mode,startPosition,loop,{});
  
    // Layer_1
    this.shape = new cjs.Shape();
    this.shape.graphics.f("#fff").s().p("AhgB2IAAjrIBpAAQAnAAAZAXQAYAYAAAkQAAAigYAXQgZAYgnAAIgpAAIAABHgAgggJIAmAAQAKABAJgHQAHgHACgKQABgKgIgJQgGgIgLgBIgqAAg");
  
    this.timeline.addTween(cjs.Tween.get(this.shape).wait(1));
  
  }).prototype = p = new cjs.MovieClip();
  p.nominalBounds = new cjs.Rectangle(-9.7,-11.8,19.4,23.6);
  
  
  (lib.Tween59 = function(mode,startPosition,loop) {
    this.initialize(mode,startPosition,loop,{});
  
    // Layer_1
    this.shape = new cjs.Shape();
    this.shape.graphics.f("#fff").s().p("AhgB2IAAjrIBpAAQAnAAAZAXQAYAYAAAkQAAAigYAXQgZAYgnAAIgpAAIAABHgAgggJIAmAAQAKABAJgHQAHgHACgKQABgKgIgJQgGgIgLgBIgqAAg");
  
    this.timeline.addTween(cjs.Tween.get(this.shape).wait(1));
  
  }).prototype = p = new cjs.MovieClip();
  p.nominalBounds = new cjs.Rectangle(-9.7,-11.8,19.4,23.6);
  
  
  (lib.Tween58 = function(mode,startPosition,loop) {
    this.initialize(mode,startPosition,loop,{});
  
    // Layer_1
    this.shape = new cjs.Shape();
    this.shape.graphics.f("#fff").s().p("AhdBYQgmglAAgzQAAgyAmglQAnglA2AAQA3AAAnAlQAmAlAAAyQAAAzgmAlQgnAkg3ABQg2gBgngkgAgsgwQgUATgBAcIAAABQAAAbASAUQASATAbABIACAAQAcABAUgTQAVgTAAgcQABgbgTgUQgTgVgcgBIgCAAQgaAAgUATg");
  
    this.timeline.addTween(cjs.Tween.get(this.shape).wait(1));
  
  }).prototype = p = new cjs.MovieClip();
  p.nominalBounds = new cjs.Rectangle(-13.1,-12.4,26.4,24.9);
  
  
  (lib.Tween57 = function(mode,startPosition,loop) {
    this.initialize(mode,startPosition,loop,{});
  
    // Layer_1
    this.shape = new cjs.Shape();
    this.shape.graphics.f("#fff").s().p("AhdBYQgmglAAgzQAAgyAmglQAnglA2AAQA3AAAnAlQAmAlAAAyQAAAzgmAlQgnAkg3ABQg2gBgngkgAgsgwQgUATgBAcIAAABQAAAbASAUQASATAbABIACAAQAcABAUgTQAVgTAAgcQABgbgTgUQgTgVgcgBIgCAAQgaAAgUATg");
  
    this.timeline.addTween(cjs.Tween.get(this.shape).wait(1));
  
  }).prototype = p = new cjs.MovieClip();
  p.nominalBounds = new cjs.Rectangle(-13.1,-12.4,26.4,24.9);
  
  
  (lib.Tween56 = function(mode,startPosition,loop) {
    this.initialize(mode,startPosition,loop,{});
  
    // Layer_1
    this.shape = new cjs.Shape();
    this.shape.graphics.f("#fff").s().p("AAjB2IgmhKIgiAAIAABKIhAAAIgBjrIBuAAQAmAAAYAXQAYAYAAAiQABAVgLASQgLATgTAKIAxBWgAgmgNIArAAQAKACAHgHQAIgGAAgKQABgKgHgIQgGgHgKgBIguAAg");
  
    this.timeline.addTween(cjs.Tween.get(this.shape).wait(1));
  
  }).prototype = p = new cjs.MovieClip();
  p.nominalBounds = new cjs.Rectangle(-10.3,-11.8,20.6,23.6);
  
  
  (lib.Tween55 = function(mode,startPosition,loop) {
    this.initialize(mode,startPosition,loop,{});
  
    // Layer_1
    this.shape = new cjs.Shape();
    this.shape.graphics.f("#fff").s().p("AAjB2IgmhKIgiAAIAABKIhAAAIgBjrIBuAAQAmAAAYAXQAYAYAAAiQABAVgLASQgLATgTAKIAxBWgAgmgNIArAAQAKACAHgHQAIgGAAgKQABgKgHgIQgGgHgKgBIguAAg");
  
    this.timeline.addTween(cjs.Tween.get(this.shape).wait(1));
  
  }).prototype = p = new cjs.MovieClip();
  p.nominalBounds = new cjs.Rectangle(-10.3,-11.8,20.6,23.6);
  
  
  (lib.Tween54 = function(mode,startPosition,loop) {
    this.initialize(mode,startPosition,loop,{});
  
    // Layer_1
    this.shape = new cjs.Shape();
    this.shape.graphics.f("#fff").s().p("AgfB2IAAiyIhCAAIAAg5IDDAAIAAA5IhCAAIAACyg");
  
    this.timeline.addTween(cjs.Tween.get(this.shape).wait(1));
  
  }).prototype = p = new cjs.MovieClip();
  p.nominalBounds = new cjs.Rectangle(-9.7,-11.8,19.6,23.6);
  
  
  (lib.Tween53 = function(mode,startPosition,loop) {
    this.initialize(mode,startPosition,loop,{});
  
    // Layer_1
    this.shape = new cjs.Shape();
    this.shape.graphics.f("#fff").s().p("AgfB2IAAiyIhCAAIAAg5IDDAAIAAA5IhCAAIAACyg");
  
    this.timeline.addTween(cjs.Tween.get(this.shape).wait(1));
  
  }).prototype = p = new cjs.MovieClip();
  p.nominalBounds = new cjs.Rectangle(-9.7,-11.8,19.6,23.6);
  
  
  (lib.Tween52 = function(mode,startPosition,loop) {
    this.initialize(mode,startPosition,loop,{});
  
    // Layer_1
    this.shape = new cjs.Shape();
    this.shape.graphics.f("#fff").s().p("AhMBbQgfgeAAgsIAAiJIBAAAIAACJQgBASAMAOQALAOASABQASACANgMQAOgMACgSQABgEgBgDIAAiJIBAAAIAACJQAAAsgfAeQgfAeguAAQgtAAgfgeg");
  
    this.timeline.addTween(cjs.Tween.get(this.shape).wait(1));
  
  }).prototype = p = new cjs.MovieClip();
  p.nominalBounds = new cjs.Rectangle(-10.7,-12.1,21.6,24.3);
  
  
  (lib.Tween51 = function(mode,startPosition,loop) {
    this.initialize(mode,startPosition,loop,{});
  
    // Layer_1
    this.shape = new cjs.Shape();
    this.shape.graphics.f("#fff").s().p("AhMBbQgfgeAAgsIAAiJIBAAAIAACJQgBASAMAOQALAOASABQASACANgMQAOgMACgSQABgEgBgDIAAiJIBAAAIAACJQAAAsgfAeQgfAeguAAQgtAAgfgeg");
  
    this.timeline.addTween(cjs.Tween.get(this.shape).wait(1));
  
  }).prototype = p = new cjs.MovieClip();
  p.nominalBounds = new cjs.Rectangle(-10.7,-12.1,21.6,24.3);
  
  
  (lib.Tween50 = function(mode,startPosition,loop) {
    this.initialize(mode,startPosition,loop,{});
  
    // Layer_1
    this.shape = new cjs.Shape();
    this.shape.graphics.f("#fff").s().p("AA3B2IhliDIAACDIhAAAIAAjrIA4AAIBkCCIAAiCIBBAAIAADrg");
  
    this.timeline.addTween(cjs.Tween.get(this.shape).wait(1));
  
  }).prototype = p = new cjs.MovieClip();
  p.nominalBounds = new cjs.Rectangle(-11,-11.8,22.1,23.6);
  
  
  (lib.Tween49 = function(mode,startPosition,loop) {
    this.initialize(mode,startPosition,loop,{});
  
    // Layer_1
    this.shape = new cjs.Shape();
    this.shape.graphics.f("#fff").s().p("AA3B2IhliDIAACDIhAAAIAAjrIA4AAIBkCCIAAiCIBBAAIAADrg");
  
    this.timeline.addTween(cjs.Tween.get(this.shape).wait(1));
  
  }).prototype = p = new cjs.MovieClip();
  p.nominalBounds = new cjs.Rectangle(-11,-11.8,22.1,23.6);
  
  
  (lib.Tween48 = function(mode,startPosition,loop) {
    this.initialize(mode,startPosition,loop,{});
  
    // Layer_1
    this.shape = new cjs.Shape();
    this.shape.graphics.f("#fff").s().p("AgfB2IAAjrIA/AAIAADrg");
  
    this.timeline.addTween(cjs.Tween.get(this.shape).wait(1));
  
  }).prototype = p = new cjs.MovieClip();
  p.nominalBounds = new cjs.Rectangle(-3.2,-11.8,6.4,23.6);
  
  
  (lib.Tween47 = function(mode,startPosition,loop) {
    this.initialize(mode,startPosition,loop,{});
  
    // Layer_1
    this.shape = new cjs.Shape();
    this.shape.graphics.f("#fff").s().p("AgfB2IAAjrIA/AAIAADrg");
  
    this.timeline.addTween(cjs.Tween.get(this.shape).wait(1));
  
  }).prototype = p = new cjs.MovieClip();
  p.nominalBounds = new cjs.Rectangle(-3.2,-11.8,6.4,23.6);
  
  
  (lib.Tween46 = function(mode,startPosition,loop) {
    this.initialize(mode,startPosition,loop,{});
  
    // Layer_1
    this.shape = new cjs.Shape();
    this.shape.graphics.f("#fff").s().p("AgfB2IAAiyIhCAAIAAg5IDDAAIAAA5IhCAAIAACyg");
  
    this.timeline.addTween(cjs.Tween.get(this.shape).wait(1));
  
  }).prototype = p = new cjs.MovieClip();
  p.nominalBounds = new cjs.Rectangle(-9.7,-11.8,19.6,23.6);
  
  
  (lib.Tween45 = function(mode,startPosition,loop) {
    this.initialize(mode,startPosition,loop,{});
  
    // Layer_1
    this.shape = new cjs.Shape();
    this.shape.graphics.f("#fff").s().p("AgfB2IAAiyIhCAAIAAg5IDDAAIAAA5IhCAAIAACyg");
  
    this.timeline.addTween(cjs.Tween.get(this.shape).wait(1));
  
  }).prototype = p = new cjs.MovieClip();
  p.nominalBounds = new cjs.Rectangle(-9.7,-11.8,19.6,23.6);
  
  
  (lib.Tween44 = function(mode,startPosition,loop) {
    this.initialize(mode,startPosition,loop,{});
  
    // Layer_1
    this.shape = new cjs.Shape();
    this.shape.graphics.f("#fff").s().p("AgfB2IAAhUIhUiXIBFAAIAuBbIAvhbIBFAAIhUCXIAABUg");
  
    this.timeline.addTween(cjs.Tween.get(this.shape).wait(1));
  
  }).prototype = p = new cjs.MovieClip();
  p.nominalBounds = new cjs.Rectangle(-11.6,-11.8,23.3,23.6);
  
  
  (lib.Tween43 = function(mode,startPosition,loop) {
    this.initialize(mode,startPosition,loop,{});
  
    // Layer_1
    this.shape = new cjs.Shape();
    this.shape.graphics.f("#fff").s().p("AgfB2IAAhUIhUiXIBFAAIAuBbIAvhbIBFAAIhUCXIAABUg");
  
    this.timeline.addTween(cjs.Tween.get(this.shape).wait(1));
  
  }).prototype = p = new cjs.MovieClip();
  p.nominalBounds = new cjs.Rectangle(-11.6,-11.8,23.3,23.6);
  
  
  (lib.Tween41 = function(mode,startPosition,loop) {
    this.initialize(mode,startPosition,loop,{});
  
    // Layer_1
    this.shape = new cjs.Shape();
    this.shape.graphics.f("#fff").s().p("AhaB2IAAjrIC1AAIAAA5Ih1AAIAAAnIBaAAIAAA5IhaAAIAABSg");
  
    this.timeline.addTween(cjs.Tween.get(this.shape).wait(1));
  
  }).prototype = p = new cjs.MovieClip();
  p.nominalBounds = new cjs.Rectangle(-9.1,-11.8,18.3,23.6);
  
  
  (lib.Tween40 = function(mode,startPosition,loop) {
    this.initialize(mode,startPosition,loop,{});
  
    // Layer_1
    this.shape = new cjs.Shape();
    this.shape.graphics.f("#fff").s().p("ADmJaIAAmKIBNAAIAAE/ICFAAIAAmfIqeAAIAAHqIkdAAIAAyzIEdAAIAAG8IHMAAIAAm8IEeAAIAASzgAm3IPICFAAIAAnrILqAAIAAoxIiFAAIAAG8IplAAIAAm8IiFAAg");
  
    this.timeline.addTween(cjs.Tween.get(this.shape).wait(1));
  
  }).prototype = p = new cjs.MovieClip();
  p.nominalBounds = new cjs.Rectangle(-51.6,-60.2,103.3,120.5);
  
  
  (lib.Tween39 = function(mode,startPosition,loop) {
    this.initialize(mode,startPosition,loop,{});
  
    // Layer_1
    this.shape = new cjs.Shape();
    this.shape.graphics.f("#fff").s().p("ADmJaIAAmKIBNAAIAAE/ICFAAIAAmfIqeAAIAAHqIkdAAIAAyzIEdAAIAAG8IHMAAIAAm8IEeAAIAASzgAm3IPICFAAIAAnrILqAAIAAoxIiFAAIAAG8IplAAIAAm8IiFAAg");
  
    this.timeline.addTween(cjs.Tween.get(this.shape).wait(1));
  
  }).prototype = p = new cjs.MovieClip();
  p.nominalBounds = new cjs.Rectangle(-51.6,-60.2,103.3,120.5);
  
  
  (lib.Tween36 = function(mode,startPosition,loop) {
    this.initialize(mode,startPosition,loop,{});
  
    // Layer_1
    this.shape = new cjs.Shape();
    this.shape.graphics.f("#fff").s().p("AnBJaIAAhMIM3AAIAAh1Is3AAIAAl1IKrAAIAAh1IqrAAIAAhMIL3AAIAAENIqrAAIAADcIM3AAIAAEOgAnBlLIAAkOIN0AAIAAEOgAl1mXILcAAIAAh2IrcAAg");
  
    this.timeline.addTween(cjs.Tween.get(this.shape).wait(1));
  
  }).prototype = p = new cjs.MovieClip();
  p.nominalBounds = new cjs.Rectangle(-45,-60.2,90.1,120.5);
  
  
  (lib.Tween35 = function(mode,startPosition,loop) {
    this.initialize(mode,startPosition,loop,{});
  
    // Layer_1
    this.shape = new cjs.Shape();
    this.shape.graphics.f("#fff").s().p("AnBJaIAAhMIM3AAIAAh1Is3AAIAAl1IKrAAIAAh1IqrAAIAAhMIL3AAIAAENIqrAAIAADcIM3AAIAAEOgAnBlLIAAkOIN0AAIAAEOgAl1mXILcAAIAAh2IrcAAg");
  
    this.timeline.addTween(cjs.Tween.get(this.shape).wait(1));
  
  }).prototype = p = new cjs.MovieClip();
  p.nominalBounds = new cjs.Rectangle(-45,-60.2,90.1,120.5);
  
  
  (lib.Tween32 = function(mode,startPosition,loop) {
    this.initialize(mode,startPosition,loop,{});
  
    // Layer_1
    this.shape = new cjs.Shape();
    this.shape.graphics.f("#fff").s().p("AEgJaIoKqjIAAKjIkdAAIAAtwIBLBjIAALCICGAAIAAq6IjRkQIAAieIDpAAIIJKjIAAqjIBMAAIAAODIp6s3Ih5AAIAAA4IMBPkIB3AAIAAxoIBNAAIAASzg");
  
    this.timeline.addTween(cjs.Tween.get(this.shape).wait(1));
  
  }).prototype = p = new cjs.MovieClip();
  p.nominalBounds = new cjs.Rectangle(-52,-60.2,104.1,120.5);
  
  
  (lib.Tween31 = function(mode,startPosition,loop) {
    this.initialize(mode,startPosition,loop,{});
  
    // Layer_1
    this.shape = new cjs.Shape();
    this.shape.graphics.f("#fff").s().p("AEgJaIoKqjIAAKjIkdAAIAAtwIBLBjIAALCICGAAIAAq6IjRkQIAAieIDpAAIIJKjIAAqjIBMAAIAAODIp6s3Ih5AAIAAA4IMBPkIB3AAIAAxoIBNAAIAASzg");
  
    this.timeline.addTween(cjs.Tween.get(this.shape).wait(1));
  
  }).prototype = p = new cjs.MovieClip();
  p.nominalBounds = new cjs.Rectangle(-52,-60.2,104.1,120.5);
  
  
  (lib.Tween28 = function(mode,startPosition,loop) {
    this.initialize(mode,startPosition,loop,{});
  
    // Layer_1
    this.shape = new cjs.Shape();
    this.shape.graphics.f("#fff").s().p("AnJJbIAAhNIEMAAQDvAACmiYQCmiYAAjeQAAjcimiZQimiYjvAAIkMAAIAAhNIEMAAQEPAAC8CvQC8CvAAD8QAAD9i8CvQi8CvkPAAgAnJGoIAAswIEMAAQC7AAB9B1QB9B1AACuQAACuh9B1Qh9B1i7AAgAl8FcIC/AAQCdAABmhfQBmhdAAiQQAAiQhmheQhmheidAAIi/AAg");
  
    this.timeline.addTween(cjs.Tween.get(this.shape).wait(1));
  
  }).prototype = p = new cjs.MovieClip();
  p.nominalBounds = new cjs.Rectangle(-45.7,-60.2,91.6,120.5);
  
  
  (lib.Tween27 = function(mode,startPosition,loop) {
    this.initialize(mode,startPosition,loop,{});
  
    // Layer_1
    this.shape = new cjs.Shape();
    this.shape.graphics.f("#fff").s().p("AnJJbIAAhNIEMAAQDvAACmiYQCmiYAAjeQAAjcimiZQimiYjvAAIkMAAIAAhNIEMAAQEPAAC8CvQC8CvAAD8QAAD9i8CvQi8CvkPAAgAnJGoIAAswIEMAAQC7AAB9B1QB9B1AACuQAACuh9B1Qh9B1i7AAgAl8FcIC/AAQCdAABmhfQBmhdAAiQQAAiQhmheQhmheidAAIi/AAg");
  
    this.timeline.addTween(cjs.Tween.get(this.shape).wait(1));
  
  }).prototype = p = new cjs.MovieClip();
  p.nominalBounds = new cjs.Rectangle(-45.7,-60.2,91.6,120.5);
  
  
  (lib.Tween24 = function(mode,startPosition,loop) {
    this.initialize(mode,startPosition,loop,{});
  
    // Layer_1
    this.shape = new cjs.Shape();
    this.shape.graphics.f("#fff").s().p("AnBJaIAAkOIODAAIAAEOgAl1IOILrAAIAAh1IrrAAgAnBBwIAAhMIKsAAIAAh1IqsAAIAAlGIMpAAIAAh2IspAAIAAhMIN0AAIAAEOIsoAAIAACuIKsAAIAAENg");
  
    this.timeline.addTween(cjs.Tween.get(this.shape).wait(1));
  
  }).prototype = p = new cjs.MovieClip();
  p.nominalBounds = new cjs.Rectangle(-45,-60.2,90.1,120.5);
  
  
  (lib.Tween23 = function(mode,startPosition,loop) {
    this.initialize(mode,startPosition,loop,{});
  
    // Layer_1
    this.shape = new cjs.Shape();
    this.shape.graphics.f("#fff").s().p("AnBJaIAAkOIODAAIAAEOgAl1IOILrAAIAAh1IrrAAgAnBBwIAAhMIKsAAIAAh1IqsAAIAAlGIMpAAIAAh2IspAAIAAhMIN0AAIAAEOIsoAAIAACuIKsAAIAAENg");
  
    this.timeline.addTween(cjs.Tween.get(this.shape).wait(1));
  
  }).prototype = p = new cjs.MovieClip();
  p.nominalBounds = new cjs.Rectangle(-45,-60.2,90.1,120.5);
  
  
  (lib.Tween20 = function(mode,startPosition,loop) {
    this.initialize(mode,startPosition,loop,{});
  
    // Layer_1
    this.shape = new cjs.Shape();
    this.shape.graphics.f("#fff").s().p("AB4JaIjgmiIg8AAIAAGiIkdAAIAAnvIBMAAIAAGkICFAAIAAmkIC1AAIDiGkICZAAIjcmFIgVglIAngRQBXgmA0hOQA0hPgBhfQABiEhdhdQhchdiDgBIm6AAIAAhNIG6AAIAaABQCTAKBpBsQBCBDAbBSQAVA9gBBGQAABAgUA6QgMAmgVAkQg1BahbA0IEHHTgAnBgFIAAhMIGXAAQA1AAApgkQAqgmAAgzQAAgzgqgmQgpglg1AAImXAAIAAhMIGXAAQBUgBBAA8QBAA8AABTQAABThAA7QhAA7hUAAg");
  
    this.timeline.addTween(cjs.Tween.get(this.shape).wait(1));
  
  }).prototype = p = new cjs.MovieClip();
  p.nominalBounds = new cjs.Rectangle(-45,-60.2,90.1,120.5);
  
  
  (lib.Tween19 = function(mode,startPosition,loop) {
    this.initialize(mode,startPosition,loop,{});
  
    // Layer_1
    this.shape = new cjs.Shape();
    this.shape.graphics.f("#fff").s().p("AB4JaIjgmiIg8AAIAAGiIkdAAIAAnvIBMAAIAAGkICFAAIAAmkIC1AAIDiGkICZAAIjcmFIgVglIAngRQBXgmA0hOQA0hPgBhfQABiEhdhdQhchdiDgBIm6AAIAAhNIG6AAIAaABQCTAKBpBsQBCBDAbBSQAVA9gBBGQAABAgUA6QgMAmgVAkQg1BahbA0IEHHTgAnBgFIAAhMIGXAAQA1AAApgkQAqgmAAgzQAAgzgqgmQgpglg1AAImXAAIAAhMIGXAAQBUgBBAA8QBAA8AABTQAABThAA7QhAA7hUAAg");
  
    this.timeline.addTween(cjs.Tween.get(this.shape).wait(1));
  
  }).prototype = p = new cjs.MovieClip();
  p.nominalBounds = new cjs.Rectangle(-45,-60.2,90.1,120.5);
  
  
  (lib.Tween16 = function(mode,startPosition,loop) {
    this.initialize(mode,startPosition,loop,{});
  
    // Layer_1
    this.shape = new cjs.Shape();
    this.shape.graphics.f("#fff").s().p("AlGILQgugdgjglQgVgVgJgNIgGgJIA9guIAGAKQAJANAQAQQAfAeAnAaQB5BPCfAAQBcAABHgYQA9gVAtgmQA4guAZhAQATg0AAg/QAAiNhqhLQgYgRgogQIgkgPQgygThGgUQh+gkgugcQhNgvAAhTQAAg/A2guQA8gzBcAAQBMABBCAfIAVAMQAUALAQAOIASARIBThTQgpgqg6gfIgCgBQgRgJgTgHQhMgehXAAQiWAAheBOQhfBMgBB7QAACNBrBLQA+AsCdArQB/AkAuAdIAEACQAWAOAOAQQAlApgBA4QABA/glAtQgMAQgRANIgHAFQhBAwhiAAQhxAAhYgyQg2gfgjgqIA+gvQAZAhArAXQBGAnBaAAQBIAAAsggQAsgfAAg0QAAgpgrgZQgmgXhxggQivgxhNg4QiCheAAixQAAidB0hiQBAg3BVgYQBFgUBRAAQBOAABGAUIAQAEQATAGARAIQA6AXAzAlQA5AqAfAqIAKAPIi8C8IgTgTQgPgPgVgPIgLgIQhQg6hJAAQg5AAgmAaQgjAaAAAfQAAApArAZQAlAXByAgQA3AQAtAQIAkANQBKAdApAfQAzAkAfAwQAxBOABBtQAACvh6BpQhEA6hdAaQhKAVhZAAQi3AAiOhfg");
  
    this.timeline.addTween(cjs.Tween.get(this.shape).wait(1));
  
  }).prototype = p = new cjs.MovieClip();
  p.nominalBounds = new cjs.Rectangle(-44.4,-61.8,88.9,123.6);
  
  
  (lib.Tween15 = function(mode,startPosition,loop) {
    this.initialize(mode,startPosition,loop,{});
  
    // Layer_1
    this.shape = new cjs.Shape();
    this.shape.graphics.f("#fff").s().p("AlGILQgugdgjglQgVgVgJgNIgGgJIA9guIAGAKQAJANAQAQQAfAeAnAaQB5BPCfAAQBcAABHgYQA9gVAtgmQA4guAZhAQATg0AAg/QAAiNhqhLQgYgRgogQIgkgPQgygThGgUQh+gkgugcQhNgvAAhTQAAg/A2guQA8gzBcAAQBMABBCAfIAVAMQAUALAQAOIASARIBThTQgpgqg6gfIgCgBQgRgJgTgHQhMgehXAAQiWAAheBOQhfBMgBB7QAACNBrBLQA+AsCdArQB/AkAuAdIAEACQAWAOAOAQQAlApgBA4QABA/glAtQgMAQgRANIgHAFQhBAwhiAAQhxAAhYgyQg2gfgjgqIA+gvQAZAhArAXQBGAnBaAAQBIAAAsggQAsgfAAg0QAAgpgrgZQgmgXhxggQivgxhNg4QiCheAAixQAAidB0hiQBAg3BVgYQBFgUBRAAQBOAABGAUIAQAEQATAGARAIQA6AXAzAlQA5AqAfAqIAKAPIi8C8IgTgTQgPgPgVgPIgLgIQhQg6hJAAQg5AAgmAaQgjAaAAAfQAAApArAZQAlAXByAgQA3AQAtAQIAkANQBKAdApAfQAzAkAfAwQAxBOABBtQAACvh6BpQhEA6hdAaQhKAVhZAAQi3AAiOhfg");
  
    this.timeline.addTween(cjs.Tween.get(this.shape).wait(1));
  
  }).prototype = p = new cjs.MovieClip();
  p.nominalBounds = new cjs.Rectangle(-44.4,-61.8,88.9,123.6);
  
  
  (lib.Tween12 = function(mode,startPosition,loop) {
    this.initialize(mode,startPosition,loop,{});
  
    // Layer_1
    this.shape = new cjs.Shape();
    this.shape.graphics.f("#fff").s().p("AjaJGQhqglhXhHIAwg7QBNA+BdAhQBdAhBjgBQDqAAChidQChidAAjkQAAjjihidQihidjqAAQjbAAieCLQidCLgVDQIhLgIQAHhDATg8QAwibCAhyQC0ieD4AAQEKAAC3CzQC4CzAAEDIgBAAQAAEEi3CzQi4CzkJAAIgGAAQhtAAhmgkgAgJGoQiugCh5h8Qh6h9ACitQAAivB8h8QB8h9CwAAQCuAAB9B9QB8B8AACvIAAAIQgCCuh8B5Qh8B6itgCIgBABgAj7j0QhjBmABCOIAAAMQACCQBoBkQBoBkCPgCQCQgDBkhoQBkhngCiQIAAgHQgCiOhlhkQhmhkiOACIgHAAQiPABhkBmg");
  
    this.timeline.addTween(cjs.Tween.get(this.shape).wait(1));
  
  }).prototype = p = new cjs.MovieClip();
  p.nominalBounds = new cjs.Rectangle(-63.1,-61.8,126.4,123.6);
  
  
  (lib.Tween11 = function(mode,startPosition,loop) {
    this.initialize(mode,startPosition,loop,{});
  
    // Layer_1
    this.shape = new cjs.Shape();
    this.shape.graphics.f("#fff").s().p("AjaJGQhqglhXhHIAwg7QBNA+BdAhQBdAhBjgBQDqAAChidQChidAAjkQAAjjihidQihidjqAAQjbAAieCLQidCLgVDQIhLgIQAHhDATg8QAwibCAhyQC0ieD4AAQEKAAC3CzQC4CzAAEDIgBAAQAAEEi3CzQi4CzkJAAIgGAAQhtAAhmgkgAgJGoQiugCh5h8Qh6h9ACitQAAivB8h8QB8h9CwAAQCuAAB9B9QB8B8AACvIAAAIQgCCuh8B5Qh8B6itgCIgBABgAj7j0QhjBmABCOIAAAMQACCQBoBkQBoBkCPgCQCQgDBkhoQBkhngCiQIAAgHQgCiOhlhkQhmhkiOACIgHAAQiPABhkBmg");
  
    this.timeline.addTween(cjs.Tween.get(this.shape).wait(1));
  
  }).prototype = p = new cjs.MovieClip();
  p.nominalBounds = new cjs.Rectangle(-63.1,-61.8,126.4,123.6);
  
  
  (lib.Tween8 = function(mode,startPosition,loop) {
    this.initialize(mode,startPosition,loop,{});
  
    // Layer_1
    this.shape = new cjs.Shape();
    this.shape.graphics.f("#fff").s().p("AEgJaIoKqjIAAKjIhMAAIAAuDIJ7M3IB4AAIAAg5IsAviIh5AAIAARnIhMAAIAAyzIDqAAILaOzIAAtnIiFAAIAAIYIhMhiIAAoCIEeAAIAASzg");
  
    this.timeline.addTween(cjs.Tween.get(this.shape).wait(1));
  
  }).prototype = p = new cjs.MovieClip();
  p.nominalBounds = new cjs.Rectangle(-52.1,-60.2,104.2,120.5);
  
  
  (lib.Tween7 = function(mode,startPosition,loop) {
    this.initialize(mode,startPosition,loop,{});
  
    // Layer_1
    this.shape = new cjs.Shape();
    this.shape.graphics.f("#fff").s().p("AEgJaIoKqjIAAKjIhMAAIAAuDIJ7M3IB4AAIAAg5IsAviIh5AAIAARnIhMAAIAAyzIDqAAILaOzIAAtnIiFAAIAAIYIhMhiIAAoCIEeAAIAASzg");
  
    this.timeline.addTween(cjs.Tween.get(this.shape).wait(1));
  
  }).prototype = p = new cjs.MovieClip();
  p.nominalBounds = new cjs.Rectangle(-52.1,-60.2,104.2,120.5);
  
  
  (lib.Symbol1 = function(mode,startPosition,loop) {
    this.initialize(mode,startPosition,loop,{});
  
    // N mask copy (mask)
    var mask = new cjs.Shape();
    mask._off = true;
    var mask_graphics_1 = new cjs.Graphics().p("AghgGIAlhKIAdAlIABB8IAAABg");
    var mask_graphics_2 = new cjs.Graphics().p("Ag6gmIAohJIBLBjIABB7IAAABg");
    var mask_graphics_3 = new cjs.Graphics().p("AhNhDIArhDIBvCQIABB9IAAAAg");
    var mask_graphics_4 = new cjs.Graphics().p("AhkhhIAuhBICaDIIABB8IgBABg");
    var mask_graphics_5 = new cjs.Graphics().p("Ah8h+IArhEIDMEIIABB9IAAAAg");
    var mask_graphics_6 = new cjs.Graphics().p("AiSibIAqhFID6FEIABB9IAAAAg");
    var mask_graphics_7 = new cjs.Graphics().p("Airi5IAnhKIEvGJIABB9IAAABg");
    var mask_graphics_8 = new cjs.Graphics().p("AjGjeIAohHIFkHOIABB8IAAABg");
    var mask_graphics_9 = new cjs.Graphics().p("Ajdj5IAmhLIGUIMIABB8IAAABg");
    var mask_graphics_10 = new cjs.Graphics().p("Aj4kdIAlhKIHLJSIABB9IAAABg");
    var mask_graphics_11 = new cjs.Graphics().p("AkVlAIAjhOIABAAIIGKgIABB8IAAABg");
    var mask_graphics_12 = new cjs.Graphics().p("Aj+lCIgvAAIAAhMIBUAAIIGKgIABB8IAAABg");
    var mask_graphics_13 = new cjs.Graphics().p("AjZlCIh4AAIAAADIgBAAIAAhPICeAAIIGKgIABB8IAAABg");
    var mask_graphics_14 = new cjs.Graphics().p("Ai0lCIh4AAIAAADIhMAAIAAhPIDpAAIIHKgIABB8IgBABg");
    var mask_graphics_15 = new cjs.Graphics().p("Ai0lCIh4AAIAABYIhMAAIAAikIDpAAIIHKgIABB8IgBABg");
    var mask_graphics_16 = new cjs.Graphics().p("Ai0lCIh4AAIAACgIhMAAIAAjsIDpAAIIHKgIABB8IgBABg");
    var mask_graphics_17 = new cjs.Graphics().p("Ai0lCIh4AAIAADmIhMAAIAAkyIDpAAIIHKgIABB8IgBABg");
    var mask_graphics_18 = new cjs.Graphics().p("Ai0lCIh4AAIAAEuIhMAAIAAl6IDpAAIIHKgIABB8IgBABg");
    var mask_graphics_19 = new cjs.Graphics().p("Ai0lCIh4AAIAAF1IhMAAIAAnBIDpAAIIHKgIABB8IgBABg");
    var mask_graphics_20 = new cjs.Graphics().p("Ai0lCIh4AAIAAG7IhMAAIAAoHIDpAAIIHKgIABB8IgBABg");
    var mask_graphics_21 = new cjs.Graphics().p("Ai0lCIh4AAIAAH+IhMAAIAApKIDpAAIIHKgIABB8IgBABg");
    var mask_graphics_22 = new cjs.Graphics().p("Ai0lCIh4AAIAAJDIhMAAIAAqPIDpAAIIHKgIABB8IgBABg");
    var mask_graphics_23 = new cjs.Graphics().p("Ai0lCIh4AAIAAKQIhMAAIAArcIDpAAIIHKgIABB8IgBABg");
    var mask_graphics_24 = new cjs.Graphics().p("Al4GQIAAsfIDpAAIIHKgIABB8IgBABIosrRIh4AAIAALTg");
    var mask_graphics_25 = new cjs.Graphics().p("Al4G2IAAtrIDpAAIIHKgIABB8IgBABIosrRIh4AAIAAMfg");
    var mask_graphics_26 = new cjs.Graphics().p("Al4HVIAAupIDpAAIIHKfIABB9IgBAAIosrRIh4AAIAANeg");
    var mask_graphics_27 = new cjs.Graphics().p("Al4H4IAAvuIDpAAIIHKfIABB8IgBABIosrRIh4AAIAAOjg");
    var mask_graphics_28 = new cjs.Graphics().p("Al4IcIAAw2IDpAAIIHKfIABB8IgBABIosrRIh4AAIAAPrg");
    var mask_graphics_29 = new cjs.Graphics().p("Al4I5IAAxxIDpAAIIHKfIABB9IgBAAIosrRIh4AAIAAQmg");
    var mask_graphics_30 = new cjs.Graphics().p("Al4JaIAAyzIDpAAIIHKfIABB9IgBAAIosrRIh4AAIAARog");
    var mask_graphics_31 = new cjs.Graphics().p("AimJaIAAgyIBMAAIAAAygAl4JaIAAyzIDpAAIIHKfIABB9IgBAAIosrRIh4AAIAARog");
    var mask_graphics_32 = new cjs.Graphics().p("AimJaIAAhvIBMAAIAABvgAl4JaIAAyzIDpAAIIHKfIABB9IgBAAIosrRIh4AAIAARog");
    var mask_graphics_33 = new cjs.Graphics().p("AimJaIAAioIBMAAIAACogAl4JaIAAyzIDpAAIIHKfIABB9IgBAAIosrRIh4AAIAARog");
    var mask_graphics_34 = new cjs.Graphics().p("AimJaIAAjiIBMAAIAADigAl4JaIAAyzIDpAAIIHKfIABB9IgBAAIosrRIh4AAIAARog");
    var mask_graphics_35 = new cjs.Graphics().p("AimJaIAAkoIBMAAIAAEogAl4JaIAAyzIDpAAIIHKfIABB9IgBAAIosrRIh4AAIAARog");
    var mask_graphics_36 = new cjs.Graphics().p("AimJaIAAmFIBMAAIAAGFgAl4JaIAAyzIDpAAIIHKfIABB9IgBAAIosrRIh4AAIAARog");
    var mask_graphics_37 = new cjs.Graphics().p("AimJaIAAnTIBMAAIAAHTgAl4JaIAAyzIDpAAIIHKfIABB9IgBAAIosrRIh4AAIAARog");
    var mask_graphics_38 = new cjs.Graphics().p("AimJaIAAokIBMAAIAAIkgAl4JaIAAyzIDpAAIIHKfIABB9IgBAAIosrRIh4AAIAARog");
    var mask_graphics_39 = new cjs.Graphics().p("AimJaIAAp0IBMAAIAAJ0gAl4JaIAAyzIDpAAIIHKfIABB9IgBAAIosrRIh4AAIAARog");
    var mask_graphics_40 = new cjs.Graphics().p("AimJaIAArlIBMBEIAAKhgAl4JaIAAyzIDpAAIIHKfIABB9IgBAAIosrRIh4AAIAARog");
    var mask_graphics_41 = new cjs.Graphics().p("AimJaIAAuDIBMBjIAAB9IAAAAIAAKjgAl4JaIAAyzIDpAAIIHKfIABB9IgBAAIosrRIh4AAIAARog");
    var mask_graphics_42 = new cjs.Graphics().p("AimJaIAAuDIB0CXIAAB9Igog0IAAKjgAl4JaIAAyzIDpAAIIHKfIABB9IgBAAIosrRIh4AAIAARog");
    var mask_graphics_43 = new cjs.Graphics().p("AimJaIAAuDIChDRIAAB8IhVhtIAAKjgAl4JaIAAyzIDpAAIIHKfIABB9IgBAAIosrRIh4AAIAARog");
    var mask_graphics_44 = new cjs.Graphics().p("AimJaIAAuDIDdEgIAAB8IiRi8IAAKjgAl4JaIAAyzIDpAAIIHKfIABB9IgBAAIosrRIh4AAIAARog");
    var mask_graphics_45 = new cjs.Graphics().p("AimJaIAAuDIEbFwIAAB9IjPkNIAAKjgAl4JaIAAyzIDpAAIIHKfIABB9IgBAAIosrRIh4AAIAARog");
    var mask_graphics_46 = new cjs.Graphics().p("AimJaIAAuDIFcHDIAAB9IkQlgIAAKjgAl4JaIAAyzIDpAAIIHKfIABB9IgBAAIosrRIh4AAIAARog");
    var mask_graphics_47 = new cjs.Graphics().p("AimJaIAAuDIGUIMIAAB9IlImpIAAKjgAl4JaIAAyzIDpAAIIHKfIABB9IgBAAIosrRIh4AAIAARog");
    var mask_graphics_48 = new cjs.Graphics().p("AimJaIAAuDIHUJfIAAB9ImIn8IAAKjgAl4JaIAAyzIDpAAIIHKfIABB9IgBAAIosrRIh4AAIAARog");
    var mask_graphics_49 = new cjs.Graphics().p("AimJaIAAuDIIWK1IAAB9InKpSIAAKjgAl4JaIAAyzIDpAAIIHKfIABB9IgBAAIosrRIh4AAIAARog");
    var mask_graphics_50 = new cjs.Graphics().p("AGUJaIoKqjIAAKjIhMAAIAAuDIJXMIIAAB7gAmTJaIAAyzIDpAAIIGKfIABB9IAAAAIosrRIh5AAIAARog");
    var mask_graphics_51 = new cjs.Graphics().p("AFqJaIoKqjIAAKjIhMAAIAAuDIJ7M3IAvAAIAABMgAm9JaIAAyzIDpAAIIGKfIABB9IAAAAIosrRIh5AAIAARog");
    var mask_graphics_52 = new cjs.Graphics().p("AFIJaIoKqjIAAKjIhMAAIAAuDIJ7M3IBzAAIAABMgAnfJaIAAyzIDpAAIIGKfIABB9IAAAAIosrRIh4AAIAARog");
    var mask_graphics_53 = new cjs.Graphics().p("AEgJaIoKqjIAAKjIhMAAIAAuDIJ7M3IDEAAIAABMgAoHJaIAAyzIDpAAIIGKfIABB9IAAAAIosrRIh5AAIAARog");
    var mask_graphics_54 = new cjs.Graphics().p("AEgJaIoKqjIAAKjIhMAAIAAuDIJ7M3IB3AAIAAg4IgCgEIBPAAIAACIgAoHJaIAAyzIDpAAIIGKfIABB9IAAAAIosrRIh5AAIAARog");
    var mask_graphics_55 = new cjs.Graphics().p("AEgJaIoKqjIAAKjIhMAAIAAuDIJ7M3IB3AAIAAg0IAuhFIAfAnIAACegAoHJaIAAyzIDpAAIIGKfIABB9IAAAAIosrRIh5AAIAARog");
    var mask_graphics_56 = new cjs.Graphics().p("AEgJaIoKqjIAAKjIhMAAIAAuDIJ7M3IB3AAIAAg4IgcgmIAAh9IBpCJIAACegAoHJaIAAyzIDpAAIIGKfIABB9IAAAAIosrRIh5AAIAARog");
    var mask_graphics_57 = new cjs.Graphics().p("AEgJaIoKqjIAAKjIhMAAIAAuDIJ7M3IB3AAIAAg4Ihbh2IACh7ICmDXIAACegAoHJaIAAyzIDpAAIIGKfIABB9IAAAAIosrRIh5AAIAARog");
    var mask_graphics_58 = new cjs.Graphics().p("AEgJaIoKqjIAAKjIhMAAIAAuDIJ7M3IB3AAIAAg4IiMi2IAGh5IABAAIAAAFIDSEQIAACegAoHJaIAAyzIDpAAIIGKfIABB9IAAAAIosrRIh5AAIAARog");
    var mask_graphics_59 = new cjs.Graphics().p("AEgJaIoKqjIAAKjIhMAAIAAuDIJ7M3IB3AAIAAg4Ir/vkIh5AAIAARoIhLAAIAAyzIDpAAIIIKhIAAAAIABACIAAgCIACAAIBKBfIAAAFIDSEQIAACeg");
    var mask_graphics_60 = new cjs.Graphics().p("AEgJaIoKqjIAAKjIhMAAIAAuDIJ7M3IB3AAIAAg4Ir/vkIh5AAIAARoIhLAAIAAyzIDpAAIIIKhIAAAAIABACIAAgCIBMAAIAABkIDSEQIAACeg");
    var mask_graphics_61 = new cjs.Graphics().p("AEgJaIoKqjIAAKjIhMAAIAAuDIJ7M3IB3AAIAAg4Ir/vkIh5AAIAARoIhLAAIAAyzIDpAAIIJKjIAAhSIBMAAIAAC0IDSEQIAACeg");
    var mask_graphics_62 = new cjs.Graphics().p("AEgJaIoKqjIAAKjIhMAAIAAuDIJ7M3IB3AAIAAg4Ir/vkIh5AAIAARoIhLAAIAAyzIDpAAIIJKjIAAiSIBMAAIAAD0IDSEQIAACeg");
    var mask_graphics_63 = new cjs.Graphics().p("AEgJaIoKqjIAAKjIhMAAIAAuDIJ7M3IB3AAIAAg4Ir/vkIh5AAIAARoIhLAAIAAyzIDpAAIIJKjIAAjUIBMAAIAAE2IDSEQIAACeg");
    var mask_graphics_64 = new cjs.Graphics().p("AEgJaIoKqjIAAKjIhMAAIAAuDIJ7M3IB3AAIAAg4Ir/vkIh5AAIAARoIhLAAIAAyzIDpAAIIJKjIAAkjIBMAAIAAGFIDSEQIAACeg");
    var mask_graphics_65 = new cjs.Graphics().p("AEgJaIoKqjIAAKjIhMAAIAAuDIJ7M3IB3AAIAAg4Ir/vkIh5AAIAARoIhLAAIAAyzIDpAAIIJKjIAAltIBMAAIAAHPIDSEQIAACeg");
    var mask_graphics_66 = new cjs.Graphics().p("AEgJaIoKqjIAAKjIhMAAIAAuDIJ7M3IB3AAIAAg4Ir/vkIh5AAIAARoIhLAAIAAyzIDpAAIIJKjIAAm/IBMAAIAAIhIDSEQIAACeg");
    var mask_graphics_67 = new cjs.Graphics().p("AEgJaIoKqjIAAKjIhMAAIAAuDIJ7M3IB3AAIAAg4Ir/vkIh5AAIAARoIhLAAIAAyzIDpAAIIJKjIAAoJIBMAAIAAJrIDSEQIAACeg");
    var mask_graphics_68 = new cjs.Graphics().p("AEgJaIoKqjIAAKjIhMAAIAAuDIJ7M3IB3AAIAAg4Ir/vkIh5AAIAARoIhLAAIAAyzIDpAAIIJKjIAApUIBMAAIAAK2IDSEQIAACeg");
    var mask_graphics_69 = new cjs.Graphics().p("AEgJaIoKqjIAAKjIhMAAIAAuDIJ7M3IB3AAIAAg4Ir/vkIh5AAIAARoIhLAAIAAyzIDpAAIIJKjIAAqjIBMAAIAABLIAAAAIAAK6IDSEQIAACeg");
    var mask_graphics_70 = new cjs.Graphics().p("AEgJaIoKqjIAAKjIhMAAIAAuDIJ7M3IB3AAIAAg4Ir/vkIh5AAIAARoIhLAAIAAyzIDpAAIIJKjIAAqjICJAAIAABLIg9AAIAAK6IDSEQIAACeg");
    var mask_graphics_71 = new cjs.Graphics().p("AEgJaIoKqjIAAKjIhMAAIAAuDIJ7M3IB3AAIAAg4Ir/vkIh5AAIAARoIhLAAIAAyzIDpAAIIJKjIAAqjIDPAAIAABLIiDAAIAAK6IDSEQIAACeg");
    var mask_graphics_72 = new cjs.Graphics().p("AEgJaIoKqjIAAKjIhMAAIAAuDIJ7M3IB3AAIAAg4Ir/vkIh5AAIAARoIhLAAIAAyzIDpAAIIJKjIAAqjIEeAAIAABMIhNAAIAAgBIiFAAIAAK6IDSEQIAACeg");
    var mask_graphics_73 = new cjs.Graphics().p("AEgJaIoKqjIAAKjIhMAAIAAuDIJ7M3IB3AAIAAg4Ir/vkIh5AAIAARoIhLAAIAAyzIDpAAIIJKjIAAqjIEeAAIAACRIhNAAIAAhGIiFAAIAAK6IDSEQIAACeg");
    var mask_graphics_74 = new cjs.Graphics().p("AEgJaIoKqjIAAKjIhMAAIAAuDIJ7M3IB3AAIAAg4Ir/vkIh5AAIAARoIhLAAIAAyzIDpAAIIJKjIAAqjIEeAAIAADZIhNAAIAAiOIiFAAIAAK6IDSEQIAACeg");
    var mask_graphics_75 = new cjs.Graphics().p("AEgJaIoKqjIAAKjIhMAAIAAuDIJ7M3IB3AAIAAg4Ir/vkIh5AAIAARoIhLAAIAAyzIDpAAIIJKjIAAqjIEeAAIAAEgIhNAAIAAjVIiFAAIAAK6IDSEQIAACeg");
    var mask_graphics_76 = new cjs.Graphics().p("AEgJaIoKqjIAAKjIhMAAIAAuDIJ7M3IB3AAIAAg4Ir/vkIh5AAIAARoIhLAAIAAyzIDpAAIIJKjIAAqjIEeAAIAAF0IhNAAIAAkpIiFAAIAAK6IDSEQIAACeg");
    var mask_graphics_77 = new cjs.Graphics().p("AEgJaIoKqjIAAKjIhMAAIAAuDIJ7M3IB3AAIAAg4Ir/vkIh5AAIAARoIhLAAIAAyzIDpAAIIJKjIAAqjIEeAAIAAHRIhNAAIAAmGIiFAAIAAK6IDSEQIAACeg");
    var mask_graphics_78 = new cjs.Graphics().p("AEgJaIoKqjIAAKjIhMAAIAAuDIJ7M3IB3AAIAAg4Ir/vkIh5AAIAARoIhLAAIAAyzIDpAAIIJKjIAAqjIEeAAIAAIrIhNAAIAAngIiFAAIAAK6IDSEQIAACeg");
    var mask_graphics_79 = new cjs.Graphics().p("AEgJaIoKqjIAAKjIhMAAIAAuDIJ7M3IB3AAIAAg4Ir/vkIh5AAIAARoIhLAAIAAyzIDpAAIIJKjIAAqjIEeAAIAAJyIhNAAIAAonIiFAAIAAK6IDSEQIAACeg");
    var mask_graphics_80 = new cjs.Graphics().p("AEgJaIoKqjIAAKjIhMAAIAAuDIJ7M3IB3AAIAAg4Ir/vkIh5AAIAARoIhLAAIAAyzIDpAAIIJKjIAAqjIEeAAIAAMGIhNAAIAAq7IiFAAIAAK6IDSEQIAACeg");
    var mask_graphics_81 = new cjs.Graphics().p("AEfJaIoJqjIAAKjIhMAAIAAuDIJ6M3IB4AAIAAg4IsAvkIh4AAIAARoIhMAAIAAyzIDpAAIIKKjIAAqjIEdAAIABNRIhNAAIAAsGIiFAAIAAK6IDREQIAACeg");
    var mask_graphics_82 = new cjs.Graphics().p("AEfJaIoJqjIAAKjIhMAAIAAuDIJ6M3IB4AAIAAg4IsAvkIh4AAIAARoIhMAAIAAyzIDpAAIIKKjIAAqjIEdAAIABPOIg4AAIA3BHIAACegAE3CsICFCtIAAtnIiFAAg");
    var mask_graphics_83 = new cjs.Graphics().p("AEgJaIoKqjIAAKjIhMAAIAAuDIJ7M3IB3AAIAAg4Ir/vkIh5AAIAARoIhLAAIAAyzIDpAAIIJKjIAAqjIEeAAIAASzgAE3CsICFCtIAAtnIiFAAg");
    var mask_graphics_119 = new cjs.Graphics().p("AEgJaIoKqjIAAKjIhMAAIAAuDIJ7M3IB3AAIAAg4Ir/vkIh5AAIAARoIhLAAIAAyzIDpAAIIJKjIAAqjIEeAAIAASzgAE3CsICFCtIAAtnIiFAAg");
    var mask_graphics_120 = new cjs.Graphics().p("AEgJaIoKqjIAAKjIhMAAIAAuDIJ7M3IB3AAIAAg4Ir/vkIh5AAIAARoIhLAAIAAyzIDpAAIIJKjIAAqjIEeAAIAASzgAE3CsICFCtIAAtnIiFAAg");
    var mask_graphics_121 = new cjs.Graphics().p("AEgJaIoKqjIAAKjIhMAAIAAuDIJ7M3IB3AAIAAg4Ir/vkIh5AAIAARoIhLAAIAAyzIDpAAIIJKjIAAqjIEeAAIAASzgAE3CsICFCtIAAtnIiFAAg");
    var mask_graphics_122 = new cjs.Graphics().p("AEgJaIoKqjIAAKjIhMAAIAAuDIJ7M3IB3AAIAAg4Ir/vkIh5AAIAARoIhLAAIAAyzIDpAAIIJKjIAAqjIEeAAIAASzgAE3CsICFCtIAAtnIiFAAg");
    var mask_graphics_123 = new cjs.Graphics().p("AEgJaIoKqjIAAKjIhMAAIAAuDIJ7M3IB3AAIAAg4Ir/vkIh5AAIAARoIhLAAIAAyzIDpAAIIJKjIAAqjIEeAAIAASzgAE3CsICFCtIAAtnIiFAAg");
    var mask_graphics_124 = new cjs.Graphics().p("AEgJaIoKqjIAAKjIhMAAIAAuDIJ7M3IB3AAIAAg4Ir/vkIh5AAIAARoIhLAAIAAyzIDpAAIIJKjIAAqjIEeAAIAASzgAE3CsICFCtIAAtnIiFAAg");
    var mask_graphics_125 = new cjs.Graphics().p("AEgJaIoKqjIAAKjIhMAAIAAuDIJ7M3IB3AAIAAg4Ir/vkIh5AAIAARoIhLAAIAAyzIDpAAIIJKjIAAqjIEeAAIAASzgAE3CsICFCtIAAtnIiFAAg");
    var mask_graphics_126 = new cjs.Graphics().p("AEgJaIoKqjIAAKjIhMAAIAAuDIJ7M3IB3AAIAAg4Ir/vkIh5AAIAARoIhLAAIAAyzIDpAAIIJKjIAAqjIEeAAIAASzgAE3CsICFCtIAAtnIiFAAg");
    var mask_graphics_127 = new cjs.Graphics().p("AEgJaIoKqjIAAKjIhMAAIAAuDIJ7M3IB3AAIAAg4Ir/vkIh5AAIAARoIhLAAIAAyzIDpAAIIJKjIAAqjIEeAAIAASzgAE3CsICFCtIAAtnIiFAAg");
    var mask_graphics_128 = new cjs.Graphics().p("AEgJaIoKqjIAAKjIhMAAIAAuDIJ7M3IB3AAIAAg4Ir/vkIh5AAIAARoIhLAAIAAyzIDpAAIIJKjIAAqjIEeAAIAASzgAE3CsICFCtIAAtnIiFAAg");
    var mask_graphics_129 = new cjs.Graphics().p("AEgJaIoKqjIAAKjIhMAAIAAuDIJ7M3IB3AAIAAg4Ir/vkIh5AAIAARoIhLAAIAAyzIDpAAIIJKjIAAqjIEeAAIAASzgAE3CsICFCtIAAtnIiFAAg");
    var mask_graphics_130 = new cjs.Graphics().p("AEgJaIoKqjIAAKjIhMAAIAAuDIJ7M3IB3AAIAAg4Ir/vkIh5AAIAARoIhLAAIAAyzIDpAAIIJKjIAAqjIEeAAIAASzgAE3CsICFCtIAAtnIiFAAg");
    var mask_graphics_131 = new cjs.Graphics().p("AEgJaIoKqjIAAKjIhMAAIAAuDIJ7M3IB3AAIAAg4Ir/vkIh5AAIAARoIhLAAIAAyzIDpAAIIJKjIAAqjIEeAAIAASzgAE3CsICFCtIAAtnIiFAAg");
    var mask_graphics_132 = new cjs.Graphics().p("AEgJaIoKqjIAAKjIhMAAIAAuDIJ7M3IB3AAIAAg4Ir/vkIh5AAIAARoIhLAAIAAyzIDpAAIIJKjIAAqjIEeAAIAASzgAE3CsICFCtIAAtnIiFAAg");
    var mask_graphics_133 = new cjs.Graphics().p("AEgJaIoKqjIAAKjIhMAAIAAuDIJ7M3IB3AAIAAg4Ir/vkIh5AAIAARoIhLAAIAAyzIDpAAIIJKjIAAqjIEeAAIAASzgAE3CsICFCtIAAtnIiFAAg");
    var mask_graphics_134 = new cjs.Graphics().p("AEgJaIoKqjIAAKjIhMAAIAAuDIJ7M3IB3AAIAAg4Ir/vkIh5AAIAARoIhLAAIAAyzIDpAAIIJKjIAAqjIEeAAIAASzgAE3CsICFCtIAAtnIiFAAg");
  
    this.timeline.addTween(cjs.Tween.get(mask).to({graphics:null,x:0,y:0}).wait(1).to({graphics:mask_graphics_1,x:967.8,y:73.2}).wait(1).to({graphics:mask_graphics_2,x:965.4,y:70.2}).wait(1).to({graphics:mask_graphics_3,x:963.4,y:67.8}).wait(1).to({graphics:mask_graphics_4,x:961.1,y:65.1}).wait(1).to({graphics:mask_graphics_5,x:958.8,y:61.8}).wait(1).to({graphics:mask_graphics_6,x:956.5,y:58.8}).wait(1).to({graphics:mask_graphics_7,x:954,y:55.4}).wait(1).to({graphics:mask_graphics_8,x:951.3,y:52}).wait(1).to({graphics:mask_graphics_9,x:949,y:48.9}).wait(1).to({graphics:mask_graphics_10,x:946.3,y:45.3}).wait(1).to({graphics:mask_graphics_11,x:943.4,y:41.5}).wait(1).to({graphics:mask_graphics_12,x:941,y:41.5}).wait(1).to({graphics:mask_graphics_13,x:937.3,y:41.5}).wait(1).to({graphics:mask_graphics_14,x:933.5,y:41.5}).wait(1).to({graphics:mask_graphics_15,x:933.5,y:41.5}).wait(1).to({graphics:mask_graphics_16,x:933.5,y:41.5}).wait(1).to({graphics:mask_graphics_17,x:933.5,y:41.5}).wait(1).to({graphics:mask_graphics_18,x:933.5,y:41.5}).wait(1).to({graphics:mask_graphics_19,x:933.5,y:41.5}).wait(1).to({graphics:mask_graphics_20,x:933.5,y:41.5}).wait(1).to({graphics:mask_graphics_21,x:933.5,y:41.5}).wait(1).to({graphics:mask_graphics_22,x:933.5,y:41.5}).wait(1).to({graphics:mask_graphics_23,x:933.5,y:41.5}).wait(1).to({graphics:mask_graphics_24,x:933.5,y:41.6}).wait(1).to({graphics:mask_graphics_25,x:933.5,y:45.4}).wait(1).to({graphics:mask_graphics_26,x:933.5,y:48.5}).wait(1).to({graphics:mask_graphics_27,x:933.5,y:52}).wait(1).to({graphics:mask_graphics_28,x:933.5,y:55.6}).wait(1).to({graphics:mask_graphics_29,x:933.5,y:58.5}).wait(1).to({graphics:mask_graphics_30,x:933.5,y:61.8}).wait(1).to({graphics:mask_graphics_31,x:933.5,y:61.8}).wait(1).to({graphics:mask_graphics_32,x:933.5,y:61.8}).wait(1).to({graphics:mask_graphics_33,x:933.5,y:61.8}).wait(1).to({graphics:mask_graphics_34,x:933.5,y:61.8}).wait(1).to({graphics:mask_graphics_35,x:933.5,y:61.8}).wait(1).to({graphics:mask_graphics_36,x:933.5,y:61.8}).wait(1).to({graphics:mask_graphics_37,x:933.5,y:61.8}).wait(1).to({graphics:mask_graphics_38,x:933.5,y:61.8}).wait(1).to({graphics:mask_graphics_39,x:933.5,y:61.8}).wait(1).to({graphics:mask_graphics_40,x:933.5,y:61.8}).wait(1).to({graphics:mask_graphics_41,x:933.5,y:61.8}).wait(1).to({graphics:mask_graphics_42,x:933.5,y:61.8}).wait(1).to({graphics:mask_graphics_43,x:933.5,y:61.8}).wait(1).to({graphics:mask_graphics_44,x:933.5,y:61.8}).wait(1).to({graphics:mask_graphics_45,x:933.5,y:61.8}).wait(1).to({graphics:mask_graphics_46,x:933.5,y:61.8}).wait(1).to({graphics:mask_graphics_47,x:933.5,y:61.8}).wait(1).to({graphics:mask_graphics_48,x:933.5,y:61.8}).wait(1).to({graphics:mask_graphics_49,x:933.5,y:61.8}).wait(1).to({graphics:mask_graphics_50,x:936.3,y:61.8}).wait(1).to({graphics:mask_graphics_51,x:940.5,y:61.8}).wait(1).to({graphics:mask_graphics_52,x:943.9,y:61.8}).wait(1).to({graphics:mask_graphics_53,x:947.9,y:61.8}).wait(1).to({graphics:mask_graphics_54,x:947.9,y:61.8}).wait(1).to({graphics:mask_graphics_55,x:947.9,y:61.8}).wait(1).to({graphics:mask_graphics_56,x:947.9,y:61.8}).wait(1).to({graphics:mask_graphics_57,x:947.9,y:61.8}).wait(1).to({graphics:mask_graphics_58,x:947.9,y:61.8}).wait(1).to({graphics:mask_graphics_59,x:947.9,y:61.8}).wait(1).to({graphics:mask_graphics_60,x:947.9,y:61.8}).wait(1).to({graphics:mask_graphics_61,x:947.9,y:61.8}).wait(1).to({graphics:mask_graphics_62,x:947.9,y:61.8}).wait(1).to({graphics:mask_graphics_63,x:947.9,y:61.8}).wait(1).to({graphics:mask_graphics_64,x:947.9,y:61.8}).wait(1).to({graphics:mask_graphics_65,x:947.9,y:61.8}).wait(1).to({graphics:mask_graphics_66,x:947.9,y:61.8}).wait(1).to({graphics:mask_graphics_67,x:947.9,y:61.8}).wait(1).to({graphics:mask_graphics_68,x:947.9,y:61.8}).wait(1).to({graphics:mask_graphics_69,x:947.9,y:61.8}).wait(1).to({graphics:mask_graphics_70,x:947.9,y:61.8}).wait(1).to({graphics:mask_graphics_71,x:947.9,y:61.8}).wait(1).to({graphics:mask_graphics_72,x:947.9,y:61.8}).wait(1).to({graphics:mask_graphics_73,x:947.9,y:61.8}).wait(1).to({graphics:mask_graphics_74,x:947.9,y:61.8}).wait(1).to({graphics:mask_graphics_75,x:947.9,y:61.8}).wait(1).to({graphics:mask_graphics_76,x:947.9,y:61.8}).wait(1).to({graphics:mask_graphics_77,x:947.9,y:61.8}).wait(1).to({graphics:mask_graphics_78,x:947.9,y:61.8}).wait(1).to({graphics:mask_graphics_79,x:947.9,y:61.8}).wait(1).to({graphics:mask_graphics_80,x:947.9,y:61.8}).wait(1).to({graphics:mask_graphics_81,x:947.9,y:61.8}).wait(1).to({graphics:mask_graphics_82,x:947.9,y:61.8}).wait(1).to({graphics:mask_graphics_83,x:947.9,y:61.8}).wait(36).to({graphics:mask_graphics_119,x:947.9,y:61.8}).wait(1).to({graphics:mask_graphics_120,x:947.9,y:61.8}).wait(1).to({graphics:mask_graphics_121,x:947.9,y:61.8}).wait(1).to({graphics:mask_graphics_122,x:947.9,y:61.8}).wait(1).to({graphics:mask_graphics_123,x:947.9,y:61.8}).wait(1).to({graphics:mask_graphics_124,x:947.9,y:61.8}).wait(1).to({graphics:mask_graphics_125,x:947.9,y:61.8}).wait(1).to({graphics:mask_graphics_126,x:947.9,y:61.8}).wait(1).to({graphics:mask_graphics_127,x:947.9,y:61.8}).wait(1).to({graphics:mask_graphics_128,x:947.9,y:61.8}).wait(1).to({graphics:mask_graphics_129,x:947.9,y:61.8}).wait(1).to({graphics:mask_graphics_130,x:947.9,y:61.8}).wait(1).to({graphics:mask_graphics_131,x:947.9,y:61.8}).wait(1).to({graphics:mask_graphics_132,x:947.9,y:61.8}).wait(1).to({graphics:mask_graphics_133,x:947.9,y:61.8}).wait(1).to({graphics:mask_graphics_134,x:947.9,y:61.8}).wait(1));
  
    // N
    this.shape = new cjs.Shape();
    this.shape.graphics.f("#fff").s().p("AEgJaIoKqjIAAKjIhMAAIAAuDIJ7M3IB4AAIAAg5IsAviIh5AAIAARnIhMAAIAAyzIDqAAILaOzIAAtnIiFAAIAAIYIhMhiIAAoCIEeAAIAASzg");
    this.shape.setTransform(947.9,61.8);
  
    this.instance = new lib.Tween7("synched",0);
    this.instance.parent = this;
    this.instance.setTransform(947.9,61.8);
    this.instance._off = true;
  
    this.instance_1 = new lib.Tween8("synched",0);
    this.instance_1.parent = this;
    this.instance_1.setTransform(947.9,61.8);
    this.instance_1.alpha = 0;
  
    var maskedShapeInstanceList = [this.shape,this.instance,this.instance_1];
  
    for(var shapedInstanceItr = 0; shapedInstanceItr < maskedShapeInstanceList.length; shapedInstanceItr++) {
      maskedShapeInstanceList[shapedInstanceItr].mask = mask;
    }
  
    this.timeline.addTween(cjs.Tween.get({}).to({state:[]}).to({state:[{t:this.shape}]},1).to({state:[{t:this.instance}]},118).to({state:[{t:this.instance_1}]},15).wait(1));
    this.timeline.addTween(cjs.Tween.get(this.instance).wait(119).to({_off:false},0).to({_off:true,alpha:0},15).wait(1));
  
    // O mask (mask)
    var mask_1 = new cjs.Shape();
    mask_1._off = true;
    var mask_1_graphics_1 = new cjs.Graphics().p("AgkAFIgEgGIA6grIABABQAKAPAMAOIgrA7QgSgTgQgVg");
    var mask_1_graphics_2 = new cjs.Graphics().p("AgXAOQgRgWgNgZIBIgVIANAUQAKAPAMAOIgtA8QgRgUgPgVg");
    var mask_1_graphics_3 = new cjs.Graphics().p("AgKApQglgygTg7IBJgMQAOAmAYAjQAKAOAMAOIgsA9QgSgUgPgVg");
    var mask_1_graphics_4 = new cjs.Graphics().p("AgDBIQg2hJgQhaIBJgVQAKBLAqA8QAKAPAMAOIgtA9QgSgUgOgVg");
    var mask_1_graphics_5 = new cjs.Graphics().p("AAABuQhOhrACiLIAAgOIBLABIAAANIAAAMQABBpA3BQQAKAPAMAPIgsA8QgSgUgPgVg");
    var mask_1_graphics_6 = new cjs.Graphics().p("AAACWQhOhsACiLQAAgsAIgpIBNgHQgLAsABAxIAAAMQABBnA3BSQAKAPAMAOIgsA8QgSgTgPgVg");
    var mask_1_graphics_7 = new cjs.Graphics().p("AAAC3QhOhsACiLQAAhVAdhJIBHAVQgaA/ABBLIAAAMQABBnA3BSQAKAPAMAOIgsA8QgSgTgPgVg");
    var mask_1_graphics_8 = new cjs.Graphics().p("AAADeQhOhsACiLQAAiFBHhnIBAAkQg9BYABBxIAAAMQABBnA3BSQAKAPAMAOIgsA8QgSgTgPgVg");
    var mask_1_graphics_9 = new cjs.Graphics().p("AgID/QhOhsABiMQAAijBsh3IAPgQIAEgEIAsA9IgMANQhVBiABCDIAAAMQACBoA3BSQAKAPAMAOIgsA9QgTgUgOgVg");
    var mask_1_graphics_10 = new cjs.Graphics().p("AgiEWQhOhrABiMQAAivB7h8QAcgcAfgWIApA/QgaASgYAZQhjBmABCNIAAAMQACBpA3BRQAKAPAMAPIgrA8QgTgUgPgVg");
    var mask_1_graphics_11 = new cjs.Graphics().p("AhFEvQhPhsACiMQAAiuB8h9QBChDBTgfIAUBJQhCAag2A3QhjBmABCOIAAAMQACBoA3BSQALAPALAOIgsA8QgSgTgPgVg");
    var mask_1_graphics_12 = new cjs.Graphics().p("AhqE5QhPhsACiMQAAiuB8h8QBhhjCDgVIAQBLQhxAQhSBUQhjBmABCOIAAAMQABBoA4BSQAKAPAMAOIgsA9QgSgUgPgVg");
    var mask_1_graphics_13 = new cjs.Graphics().p("AiJE8QhOhsABiMQAAivB8h8QB7h8CwAAIAGAAIgPBMQiOAChiBmQhkBmABCNIAAAMQACBpA4BRQAKAQAMAOIgsA8QgTgUgPgUg");
    var mask_1_graphics_14 = new cjs.Graphics().p("AiwE8QhPhsACiMQAAivB8h8QB8h8CvAAQAlAAAjAGIAMACIgVBJQgfgFghAAIgHAAQiPAChjBmQhjBmABCNIAAAMQABBpA4BRQAKAQAMAOIgsA8QgSgUgPgUg");
    var mask_1_graphics_15 = new cjs.Graphics().p("AjaE8QhPhsACiMQAAivB8h8QB8h8CvAAQBbAABNAhIgwA/Qg4gUhBAAIgHAAQiOAChkBmQhjBmABCNIAAAMQACBpA3BRQAKAQANAOIgtA8QgSgUgPgUg");
    var mask_1_graphics_16 = new cjs.Graphics().p("Aj5E8QhPhsACiMQAAivB8h8QB8h8CvAAQCBAABlBDIgnBAIgVgMQhMgshfABIgHAAQiOAChkBmQhjBmABCNIAAAMQACBpA3BRQAKAQANAOIgtA8QgSgUgPgUg");
    var mask_1_graphics_17 = new cjs.Graphics().p("AkfE8QhOhsABiMQAAivB8h8QB8h8CvAAQCwAAB8B8IAGAGIhHAkQhjhbiIABIgIAAQiNAChkBmQhkBmABCNIAAAMQACBpA4BRQAKAQAMAOIgsA8QgTgUgPgUg");
    var mask_1_graphics_18 = new cjs.Graphics().p("Ak6E8QhPhsACiMQAAivB8h8QB8h8CvAAQCvAAB9B8QAjAkAZAnIhOATQgRgXgVgUQhmhkiPABIgHAAQiOAChkBmQhjBmABCNIAAAMQACBpA3BRQAKAQANAOIgtA8QgSgUgPgUg");
    var mask_1_graphics_19 = new cjs.Graphics().p("AlNE8QhPhsACiMQAAivB8h8QB8h8CvAAQCvAAB9B8QBEBFAfBUIhSABQgag4gvgvQhmhkiPABIgHAAQiOAChkBmQhjBmABCNIAAAMQABBpA4BRQAKAQAMAOIgsA8QgSgUgPgUg");
    var mask_1_graphics_20 = new cjs.Graphics().p("AlXE8QhPhsACiMQAAivB8h8QB8h8CvAAQCvAAB9B8QBiBjAUCDIhNABQgShnhPhNQhmhkiPABIgGAAQiPAChkBmQhjBmABCNIAAAMQABBpA4BRQAKAQAMAOIgsA8QgSgUgPgUg");
    var mask_1_graphics_21 = new cjs.Graphics().p("AlaE8QhPhsACiMQAAivB8h8QB8h8CvAAQCwAAB8B8QB8B8AACvIAAADIhNgGIAAgEQgBiOhmhjQhmhkiOABIgHAAQiOAChkBmQhkBmABCNIAAAMQACBpA3BRQALAQAMAOIgtA8QgSgUgPgUg");
    var mask_1_graphics_22 = new cjs.Graphics().p("AlaE8QhPhsACiMQAAivB8h8QB8h8CvAAQCwAAB8B8QB8B8AACvIAAAIQAAAvgKArIhGgrQAEgbgBgcIAAgHQgBiOhmhjQhmhkiOABIgHAAQiOAChkBmQhkBmABCNIAAAMQACBpA3BRQALAQAMAOIgtA8QgSgUgPgUg");
    var mask_1_graphics_23 = new cjs.Graphics().p("AlaE8QhPhsACiMQAAivB8h8QB8h8CvAAQCwAAB8B8QB8B8AACvIAAAIQgBBjgoBSIg8g0QAZg/gBhKIAAgHQgBiOhmhjQhmhkiOABIgHAAQiOAChkBmQhkBmABCNIAAAMQACBpA3BRQALAQAMAOIgtA8QgSgUgPgUg");
    var mask_1_graphics_24 = new cjs.Graphics().p("AlaE8QhPhsACiMQAAivB8h8QB8h8CvAAQCwAAB8B8QB8B8AACvIAAAIQgBCXheBwIgwg8QBEhbgCh4IAAgHQgBiOhmhjQhmhkiOABIgHAAQiOAChkBmQhkBmABCNIAAAMQACBpA3BRQALAQAMAOIgtA8QgSgUgPgUg");
    var mask_1_graphics_25 = new cjs.Graphics().p("ADhE6QANgLAMgMQBkhogDiPIAAgIQgBiNhmhkQhmhkiOABIgHAAQiOAChkBmQhkBmABCOIAAALQACBpA3BSQALAPAMAOIgtA8QgSgTgPgVQhPhsACiMQAAiuB8h9QB8h8CvAAQCwAAB8B8QB8B9AACuIAAAJQgBCth8B6QgTASgUAQg");
    var mask_1_graphics_26 = new cjs.Graphics().p("ACpFMQArgYAmgnQBkhngDiQIAAgIQgBiNhmhkQhmhkiOACIgHAAQiOABhkBmQhkBmABCOIAAAMQACBoA3BSQALAPAMAOIgtA8QgSgTgPgVQhPhsACiMQAAiuB8h8QB8h9CvAAQCwAAB8B9QB8B8AACuIAAAJQgBCuh8B5QgwAug2AdIgBgCIgCADIgDACg");
    var mask_1_graphics_27 = new cjs.Graphics().p("ABeFbQBYgXBEhHQBkhngDiQIAAgHQgBiOhmhkQhmhkiOACIgHAAQiOABhkBmQhkBmABCOIAAAMQACBoA3BSQALAPAMAOIgtA8QgSgTgPgVQhPhsACiMQAAiuB8h8QB8h9CvAAQCwAAB8B9QB8B8AACuIAAAJQgBCuh8B5QgwAug2AdIgBgCIgCADQgsAXgxALg");
    var mask_1_graphics_28 = new cjs.Graphics().p("AAZFiQCDgJBehhQBkhngDiQIAAgHQgBiOhmhkQhmhkiOACIgHAAQiOABhkBmQhkBmABCOIAAAMQACBoA3BSQALAPAMAOIgtA9QgSgUgPgVQhPhsACiMQAAiuB8h8QB8h9CvAAQCwAAB8B9QB8B8AACuIAAAJQgBCuh8B5QgwAvg2AcIgBgCIgCADQhLAnhaAFg");
    var mask_1_graphics_29 = new cjs.Graphics().p("AgHGoQglAAgigGIAThEQAfAFAhgBQCRgCBkhoQBkhogDiQIAAgGQgBiPhmhjQhmhkiOABIgHAAQiOAChkBmQhkBmABCNIAAAMQACBpA3BRQALAPAMAPIgtA8QgSgUgPgUQhPhsACiMQAAivB8h8QB8h8CvAAQCwAAB8B8QB8B8AACvIAAAIQgBCuh8B6QgwAug2AcIgBgCIgCAEQhYAthqgBIAAAAg");
    var mask_1_graphics_30 = new cjs.Graphics().p("AgHGoQhVgBhIgeIAig/QA/AaBIgCQCRgCBkhoQBkhogDiQIAAgGQgBiPhmhjQhmhkiOABIgHAAQiOAChkBmQhkBmABCNIAAAMQACBpA3BRQALAPAMAPIgtA8QgSgUgPgUQhPhsACiMQAAivB8h8QB8h8CvAAQCwAAB8B8QB8B8AACvIAAAIQgBCuh8B6QgwAug2AcIgBgCIgCAEQhYAthqgBIAAAAg");
    var mask_1_graphics_31 = new cjs.Graphics().p("AgHGoQiGgBhmhKIAmg+QBbBFB3gCQCRgCBkhoQBkhogDiQIAAgGQgBiPhmhjQhmhkiOABIgHAAQiOAChkBmQhkBmABCNIAAAMQACBpA3BRQALAPAMAPIgtA8QgSgUgPgUQhPhsACiMQAAivB8h8QB8h8CvAAQCwAAB8B8QB8B8AACvIAAAIQgBCuh8B6QgwAug2AcIgBgCIgCAEQhYAthqgBIAAAAg");
    var mask_1_graphics_32 = new cjs.Graphics().p("AgHGoQiKgBhphOQgcgVgZgaQgYgYgTgaQhPhsACiMQAAivB8h8QB8h8CvAAQCwAAB8B8QB8B8AACvIAAAIQgBCuh8B6QgwAug2AcIgBgCIgCAEQhYAthqgBIAAAAgAj5jzQhkBmABCNIAAAMQACBpA3BRQAVAfAdAbQAQAQARANQBdBHB4gCQCRgCBkhoQBkhogDiQIAAgGQgBiPhmhjQhmhkiOABIgHAAQiOAChkBmg");
    var mask_1_graphics_33 = new cjs.Graphics().p("AmZGsIAwg7QAYATAaARIgoBCQgegUgcgXgAgHF6QiugCh6h8Qh6h9ACitQAAivB8h8QB8h9CvAAQCwAAB8B9QB8B8AACvIAAAJQgBCth8B5QgwAug2AdIgBgCIgCADQhYAthqgBIAAABgAj5kiQhkBmABCPIAAAMQADCPBoBkQBnBkCPgCQCRgDBkhoQBkhngDiPIAAgIQgBiOhmhkQhmhkiOACIgHAAQiOABhkBmg");
    var mask_1_graphics_34 = new cjs.Graphics().p("AmZGaIAwg7QAvAnA2AbIgbBIQhBggg5gvgAgHFoQiugCh6h8Qh6h9ACitQAAivB8h8QB8h9CvAAQCwAAB8B9QB8B8AACvIAAAJQgBCth8B5QgwAvg2AcIgBgCIgCADQhYAthqgBIAAABgAj5k0QhkBmABCPIAAAMQADCPBoBkQBnBkCPgCQCRgDBkhoQBkhngDiPIAAgIQgBiOhmhkQhmhkiOACIgHAAQiOABhkBmg");
    var mask_1_graphics_35 = new cjs.Graphics().p("AjZH3QhqglhWhHIAwg7QBNA+BdAhIAMAEIgiBFIgEgBgAgHFZQiugCh6h8Qh6h9ACitQAAivB8h8QB8h9CvAAQCwAAB8B9QB8B8AACvIAAAJQgBCth8B5QgwAug2AdIgBgCIgCADQhYAthqgBIAAABgAj5lDQhkBmABCPIAAAMQADCPBoBkQBnBkCPgCQCRgDBkhoQBkhngDiPIAAgIQgBiOhmhkQhmhkiOACIgHAAQiOABhkBmg");
    var mask_1_graphics_36 = new cjs.Graphics().p("AjZHtQhqglhWhGIAwg8QBNA/BdAgQAnAOApAIIgkBGQgjgIgjgMgAgHFPQiugCh6h8Qh6h8ACitQAAiwB8h8QB8h8CvAAQCwAAB8B8QB8B8AACwIAAAIQgBCth8B6QgwAug2AcIgBgCIgCAEQhYAthqgBIAAAAgAj5lMQhkBmABCOIAAAMQADCPBoBkQBnBlCPgDQCRgCBkhoQBkhogDiPIAAgHQgBiPhmhjQhmhkiOABIgHAAQiOAChkBmg");
    var mask_1_graphics_37 = new cjs.Graphics().p("AjZHnQhqglhWhGIAwg8QBNA/BdAgQA0ATA3AIIAGBNQhHgIhEgYgAgHFJQiugCh6h8Qh6h8ACitQAAiwB8h8QB8h8CvAAQCwAAB8B8QB8B8AACwIAAAIQgBCth8B6QgwAug2AdIgBgDIgCAEQhYAthqgBIAAAAgAj5lSQhkBmABCOIAAAMQADCPBoBkQBnBlCPgDQCRgCBkhoQBkhogDiPIAAgHQgBiPhmhjQhmhkiOABIgHAAQiOAChkBmg");
    var mask_1_graphics_38 = new cjs.Graphics().p("AjZHlQhqglhWhGIAwg8QBNA/BdAgQBPAcBUAEIAVBMQhsAAhmgkgAgHFHQiugCh6h8Qh6h8ACitQAAiwB8h8QB8h8CvAAQCwAAB8B8QB8B8AACwIAAAIQgBCth8B6QgwAug2AcIgBgCIgCAEQhYAthqgBIAAAAgAj5lUQhkBmABCOIAAAMQADCPBoBkQBnBlCPgDQCRgCBkhoQBkhogDiPIAAgHQgBiPhmhjQhmhkiOABIgHAAQiOAChkBmg");
    var mask_1_graphics_39 = new cjs.Graphics().p("AjZHlQhqglhWhGIAwg8QBNA/BdAgQBdAhBigBQAYAAAXgCIAXBLQgiADgkAAIgFAAQhtAAhngkgAgHFHQiugCh6h8Qh6h8ACitQAAiwB8h8QB8h8CvAAQCwAAB8B8QB8B8AACwIAAAIQgBCth8B6QgwAug2AcIgBgCIgCAEQhYAthqgBIAAAAgAj5lUQhkBmABCOIAAAMQADCPBoBkQBnBlCPgDQCRgCBkhoQBkhogDiPIAAgHQgBiPhmhjQhmhkiOABIgHAAQiOAChkBmg");
    var mask_1_graphics_40 = new cjs.Graphics().p("AjZHlQhqglhWhGIAwg8QBNA/BdAgQBdAhBigBQA9AAA5gLIAYBJQhEAOhKAAIgFAAQhtAAhngkgAgHFHQiugCh6h8Qh6h8ACitQAAiwB8h8QB8h8CvAAQCwAAB8B8QB8B8AACwIAAAIQgBCth8B6QgwAug2AcIgBgCIgCAEQhYAthqgBIAAAAgAj5lUQhkBmABCOIAAAMQADCPBoBkQBnBlCPgDQCRgCBkhoQBkhogDiPIAAgHQgBiPhmhjQhmhkiOABIgHAAQiOAChkBmg");
    var mask_1_graphics_41 = new cjs.Graphics().p("AjZHlQhqglhWhGIAwg8QBNA/BdAgQBdAhBigBQBZAABPgXIAyBAQhlAjh1AAIgFAAQhtAAhngkgAgHFHQiugCh6h8Qh6h8ACitQAAiwB8h8QB8h8CvAAQCwAAB8B8QB8B8AACwIAAAIQgBCth8B6Qh9B6iugCIAAAAgAj5lUQhkBmABCOIAAAMQADCPBoBkQBnBlCPgDQCRgCBkhoQBkhogDiPIAAgHQgBiPhmhjQhmhkiOABIgHAAQiOAChkBmg");
    var mask_1_graphics_42 = new cjs.Graphics().p("AjZHlQhqglhWhGIAwg8QBNA/BdAgQBdAhBigBQCGAABug0IArBBQiBA/ieAAIgFAAQhtAAhngkgAgHFHQiugCh6h8Qh6h8ACitQAAiwB8h8QB8h8CvAAQCwAAB8B8QB8B8AACwIAAAIQgBCth8B6Qh9B6iugCIAAAAgAj5lUQhkBmABCOIAAAMQADCPBoBkQBnBlCPgDQCRgCBkhoQBkhogDiPIAAgHQgBiPhmhjQhmhkiOABIgHAAQiOAChkBmg");
    var mask_1_graphics_43 = new cjs.Graphics().p("AjZHlQhqglhWhGIAwg8QBNA/BdAgQBdAhBigBQC0AACJhdIAuA9QidBsjOAAIgFAAQhtAAhngkgAgHFHQiugCh6h8Qh6h8ACitQAAiwB8h8QB8h8CvAAQCwAAB8B8QB8B8AACwIAAAIQgBCth8B6Qh9B6iugCIAAAAgAj5lUQhkBmABCOIAAAMQADCPBoBkQBnBlCPgDQCRgCBkhoQBkhogDiPIAAgHQgBiPhmhjQhmhkiOABIgHAAQiOAChkBmg");
    var mask_1_graphics_44 = new cjs.Graphics().p("AjeHlQhqglhWhGIAwg8QBNA/BdAgQBdAhBjgBQDeAACdiPIA2A2QizClj+AAIgGAAQhtAAhngkgAgMFHQiugCh6h8Qh6h8ACitQAAiwB8h8QB8h8CwAAQCvAAB8B8QB8B8AACwIAAAIQgBCth8B6Qh9B6itgCIAAAAgAj+lUQhkBmABCOIAAAMQADCPBoBkQBnBlCPgDQCRgCBkhoQBkhogDiPIAAgHQgBiPhmhjQhmhkiNABIgIAAQiOAChkBmg");
    var mask_1_graphics_45 = new cjs.Graphics().p("Aj2HlQhqglhXhGIAwg8QBNA/BdAgQBdAhBjgBQDqAAChidIAZgZIA+AsQgQASgRARQi4CzkJAAIgGAAQhtAAhmgkgAglFHQiugCh5h8Qh6h8ACitQAAiwB8h8QB8h8CwAAQCuAAB9B8QB8B8AACwIAAAIQgCCth8B6Qh8B6itgCIgBAAgAkXlUQhjBmABCOIAAAMQACCPBoBkQBoBlCQgDQCPgCBkhoQBkhogCiPIAAgHQgCiPhlhjQhmhkiOABIgHAAQiPAChkBmg");
    var mask_1_graphics_46 = new cjs.Graphics().p("AkMHlQhqglhXhGIAwg8QBNA/BdAgQBdAhBjgBQDqAAChidQAmgkAcgpIBBApQghAvgsArQi4CzkJAAIgFAAQhtAAhngkgAg7FHQiugCh5h8Qh6h8ACitQAAiwB8h8QB8h8CwAAQCuAAB9B8QB8B8AACwIAAAIQgCCth8B6Qh8B6itgCIgBAAgAktlUQhjBmABCOIAAAMQACCPBoBkQBoBlCQgDQCPgCBkhoQBkhogCiPIAAgHQgCiPhlhjQhmhkiOABIgHAAQiPAChkBmg");
    var mask_1_graphics_47 = new cjs.Graphics().p("AkeHlQhqglhXhGIAwg8QBNA/BdAgQBdAhBjgBQDqAAChidQA7g5AlhDIBHAeQgrBQhGBEQi4CzkJAAIgFAAQhtAAhngkgAhNFHQiugCh5h8Qh6h8ACitQAAiwB8h8QB8h8CwAAQCuAAB9B8QB8B8AACwIAAAIQgCCth8B6Qh8B6itgCIgBAAgAk/lUQhjBmABCOIAAAMQACCPBoBkQBoBlCQgDQCPgCBkhoQBkhogCiPIAAgHQgCiPhlhjQhmhkiOABIgHAAQiPAChkBmg");
    var mask_1_graphics_48 = new cjs.Graphics().p("AkxHlQhqglhWhGIAwg8QBNA/BdAgQBdAhBjgBQDpAAChidQBZhWAohsIBMAPQgtCChqBnQi3CzkJAAIgGAAQhtAAhngkgAhfFHQiugCh6h8Qh6h8ACitQAAiwB8h8QB8h8CwAAQCvAAB8B8QB8B8AACwIAAAIQgBCth8B6Qh9B6itgCIAAAAgAlRlUQhkBmABCOIAAAMQADCPBoBkQBnBlCQgDQCQgCBkhoQBkhogDiPIAAgHQgBiPhmhjQhmhkiNABIgIAAQiOAChkBmg");
    var mask_1_graphics_49 = new cjs.Graphics().p("Ak7HlQhqglhWhGIAwg8QBNA/BdAgQBdAhBjgBQDpAAChidQB3hzAfiaIBLAQQgkCviHCEQi3CzkJAAIgGAAQhtAAhngkgAhpFHQiugCh6h8Qh6h8ACitQAAiwB8h8QB8h8CwAAQCvAAB8B8QB8B8AACwIAAAIQgBCth8B6Qh9B6itgCIAAAAgAlblUQhkBmABCOIAAAMQADCPBoBkQBnBlCQgDQCQgCBkhoQBkhogDiPIAAgHQgBiPhmhjQhmhkiNABIgIAAQiOAChkBmg");
    var mask_1_graphics_50 = new cjs.Graphics().p("AlAHlQhqglhXhGIAwg8QBNA/BdAgQBdAhBjgBQDqAAChidQCSiOAOjHIBLAHQgQDjilChQi4CzkJAAIgGAAQhtAAhmgkgAhvFHQiugCh5h8Qh6h8ACitQAAiwB8h8QB8h8CwAAQCuAAB9B8QB8B8AACwIAAAIQgCCth8B6Qh8B6itgCIgBAAgAlhlUQhjBmABCOIAAAMQACCPBoBkQBoBlCQgDQCPgCBkhoQBkhogCiPIAAgHQgCiPhlhjQhmhkiOABIgHAAQiPAChkBmg");
    var mask_1_graphics_51 = new cjs.Graphics().p("AlBHlQhqglhXhGIAwg8QBNA/BdAgQBdAhBjgBQDqAAChidQChidAAjiQAAgWgBgWIBMgIQACAZAAAaIgBAAQAAEDi3CzQi4CzkJAAIgGAAQhtAAhmgkgAhwFHQiugCh5h8Qh6h8ACitQAAiwB8h8QB8h8CwAAQCuAAB9B8QB8B8AACwIAAAIQgCCth8B6Qh8B6itgCIgBAAgAlilUQhjBmABCOIAAAMQACCPBoBkQBoBlCQgDQCPgCBkhoQBkhogCiPIAAgHQgCiPhlhjQhmhkiOABIgHAAQiPAChkBmg");
    var mask_1_graphics_52 = new cjs.Graphics().p("AlBHlQhqglhXhGIAwg8QBNA/BdAgQBdAhBjgBQDqAAChidQChidAAjiQAAg9gLg4IBKgUQAOBBAABHIgBAAQAAEDi3CzQi4CzkJAAIgGAAQhtAAhmgkgAhwFHQiugCh5h8Qh6h8ACitQAAiwB8h8QB8h8CwAAQCuAAB9B8QB8B8AACwIAAAIQgCCth8B6Qh8B6itgCIgBAAgAlilUQhjBmABCOIAAAMQACCPBoBkQBoBlCQgDQCPgCBkhoQBkhogCiPIAAgHQgCiPhlhjQhmhkiOABIgHAAQiPAChkBmg");
    var mask_1_graphics_53 = new cjs.Graphics().p("AlBHlQhqglhXhGIAwg8QBNA/BdAgQBdAhBjgBQDqAAChidQChidAAjiQAAhpgihaIAAAAIBIgcQAnBnAAB3IgBAAQAAEDi3CzQi4CzkJAAIgGAAQhtAAhmgkgAhwFHQiugCh5h8Qh6h8ACitQAAiwB8h8QB8h8CwAAQCuAAB9B8QB8B8AACwIAAAIQgCCth8B6Qh8B6itgCIgBAAgAlilUQhjBmABCOIAAAMQACCPBoBkQBoBlCQgDQCPgCBkhoQBkhogCiPIAAgHQgCiPhlhjQhmhkiOABIgHAAQiPAChkBmg");
    var mask_1_graphics_54 = new cjs.Graphics().p("AlBHlQhqglhXhGIAwg8QBNA/BdAgQBdAhBjgBQDqAAChidQChidAAjiQAAiRhBh1IBCgoQBMCGAACnIgBAAQAAEDi3CzQi4CzkJAAIgGAAQhtAAhmgkgAhwFHQiugCh5h8Qh6h8ACitQAAiwB8h8QB8h8CwAAQCuAAB9B8QB8B8AACwIAAAIQgCCth8B6Qh8B6itgCIgBAAgAlilUQhjBmABCOIAAAMQACCPBoBkQBoBlCQgDQCPgCBkhoQBkhogCiPIAAgHQgCiPhlhjQhmhkiOABIgHAAQiPAChkBmg");
    var mask_1_graphics_55 = new cjs.Graphics().p("AlBHlQhqglhXhGIAwg8QBNA/BdAgQBdAhBjgBQDqAAChidQChidAAjiQAAi5hpiKIA3g2QB/CgAADYIgBAAQAAEDi3CzQi4CzkJAAIgGAAQhtAAhmgkgAhwFHQiugCh5h8Qh6h8ACitQAAiwB8h8QB8h8CwAAQCuAAB9B8QB8B8AACwIAAAIQgCCth8B6Qh8B6itgCIgBAAgAlilUQhjBmABCOIAAAMQACCPBoBkQBoBlCQgDQCPgCBkhoQBkhogCiPIAAgHQgCiPhlhjQhmhkiOABIgHAAQiPAChkBmg");
    var mask_1_graphics_56 = new cjs.Graphics().p("AlBHuQhqglhXhHIAwg7QBNA+BdAhQBdAhBjgBQDqAAChidQChidAAjjQAAjjigicIAyg7IADADQC4CzAAEEIgBAAQAAEDi3CzQi4CzkJAAIgGAAQhtAAhmgkgAhwFQQiugCh5h8Qh6h9ACitQAAivB8h8QB8h9CwAAQCuAAB9B9QB8B8AACvIAAAJQgCCth8B5Qh8B6itgCIgBABgAlilMQhjBmABCPIAAAMQACCPBoBkQBoBkCQgCQCPgDBkhoQBkhngCiPIAAgIQgCiOhlhkQhmhkiOACIgHAAQiPABhkBmg");
    var mask_1_graphics_57 = new cjs.Graphics().p("AlBIGQhqglhXhHIAwg7QBNA+BdAhQBdAhBjgBQDqAAChidQChidAAjjQAAjkihidQgWgVgXgSIAqhCQAdAXAcAcQC4CzAAEEIgBAAQAAEDi3CzQi4CzkJAAIgGAAQhtAAhmgkgAhwFoQiugCh5h8Qh6h9ACitQAAivB8h8QB8h9CwAAQCuAAB9B9QB8B8AACvIAAAJQgCCth8B5Qh8B6itgCIgBABgAlik0QhjBmABCPIAAAMQACCPBoBkQBoBkCQgCQCPgDBkhoQBkhngCiPIAAgIQgCiOhlhkQhmhkiOACIgHAAQiPABhkBmg");
    var mask_1_graphics_58 = new cjs.Graphics().p("AlBIcQhqglhXhGIAwg8QBNA/BdAgQBdAhBjgBQDqAAChidQChidAAjiQAAjkihidQgxgvg3ghIAhhGQBDAmA6A5QC4CzAAEEIgBAAQAAEDi3CzQi4CzkJAAIgGAAQhtAAhmgkgAhwF+QiugCh5h8Qh6h8ACitQAAiwB8h8QB8h8CwAAQCuAAB9B8QB8B8AACwIAAAIQgCCth8B6Qh8B6itgCIgBAAgAlikdQhjBmABCOIAAAMQACCPBoBkQBoBlCQgDQCPgCBkhoQBkhogCiPIAAgHQgCiPhlhjQhmhkiOABIgHAAQiPAChkBmg");
    var mask_1_graphics_59 = new cjs.Graphics().p("AlBItQhqglhXhHIAwg7QBNA+BdAhQBdAhBjgBQDqAAChidQChidAAjjQAAjkihidQhMhJhbgnIgDgBIAihGQBoAtBWBUQC4CzAAEEIgBAAQAAEDi3CzQi4CzkJAAIgGAAQhtAAhmgkgAhwGPQiugCh5h8Qh6h9ACitQAAivB8h8QB8h9CwAAQCuAAB9B9QB8B8AACvIAAAJQgCCth8B5Qh8B6itgCIgBABgAlikNQhjBmABCPIAAAMQACCPBoBkQBoBkCQgCQCPgDBkhoQBkhngCiPIAAgIQgCiOhlhkQhmhkiOACIgHAAQiPABhkBmg");
    var mask_1_graphics_60 = new cjs.Graphics().p("AlBI5QhqglhXhGIAwg8QBNA/BdAgQBdAhBjgBQDqAAChidQChidAAjiQAAjkihidQhohliFgkIAehHQCSAqBzBvQC4CzAAEEIgBAAQAAEDi3CzQi4CzkJAAIgGAAQhtAAhmgkgAhwGbQiugCh5h8Qh6h8ACitQAAiwB8h8QB8h8CwAAQCuAAB9B8QB8B8AACwIAAAIQgCCth8B6Qh8B6itgCIgBAAgAlikAQhjBmABCOIAAAMQACCPBoBkQBoBlCQgDQCPgCBkhoQBkhogCiPIAAgHQgCiPhlhjQhmhkiOABIgHAAQiPAChkBmg");
    var mask_1_graphics_61 = new cjs.Graphics().p("AlBJDQhqglhXhGIAwg8QBNA/BdAgQBdAhBjgBQDqAAChidQChidAAjiQAAjkihidQiIiFi7gUIAVhLQDNAaCXCTQC4CzAAEEIgBAAQAAEDi3CzQi4CzkJAAIgGAAQhtAAhmgkgAhwGlQiugCh5h8Qh6h8ACitQAAiwB8h8QB8h8CwAAQCuAAB9B8QB8B8AACwIAAAHQgCCuh8B6Qh8B6itgCIgBAAgAlij2QhjBmABCOIAAALQACCQBoBkQBoBlCQgDQCPgCBkhoQBkhogCiPIAAgHQgCiPhlhjQhmhkiOABIgHAAQiPAChkBmg");
    var mask_1_graphics_62 = new cjs.Graphics().p("AlBJGQhqglhXhHIAwg7QBNA+BdAhQBdAhBjgBQDqAAChidQChidAAjkQAAjjihidQihicjogBIAJhMQECAEC0CvQC4CzAAEDIgBAAQAAEEi3CzQi4CzkJAAIgGAAQhtAAhmgkgAhwGoQiugCh5h8Qh6h9ACitQAAivB8h8QB8h9CwAAQCuAAB9B9QB8B8AACvIAAAIQgCCuh8B5Qh8B6itgCIgBABgAlij0QhjBmABCOIAAAMQACCQBoBkQBoBkCQgCQCPgDBkhoQBkhngCiQIAAgHQgCiOhlhkQhmhkiOACIgHAAQiPABhkBmg");
    var mask_1_graphics_63 = new cjs.Graphics().p("AlBJGQhqglhXhHIAwg7QBNA+BdAhQBdAhBjgBQDqAAChidQChidAAjkQAAjjihidQihidjqAAQgZAAgYACIgchKQAmgEAnAAQEKAAC3CzQC4CzAAEDIgBAAQAAEEi3CzQi4CzkJAAIgGAAQhtAAhmgkgAhwGoQiugCh5h8Qh6h9ACitQAAivB8h8QB8h9CwAAQCuAAB9B9QB8B8AACvIAAAIQgCCuh8B5Qh8B6itgCIgBABgAlij0QhjBmABCOIAAAMQACCQBoBkQBoBkCQgCQCPgDBkhoQBkhngCiQIAAgHQgCiOhlhkQhmhkiOACIgHAAQiPABhkBmg");
    var mask_1_graphics_64 = new cjs.Graphics().p("AlBJGQhqglhXhHIAwg7QBNA+BdAhQBdAhBjgBQDqAAChidQChidAAjkQAAjjihidQihidjqAAQgtAAgrAGIgIACIgghJIAmgGQAsgFAuAAQEKAAC3CzQC4CzAAEDIgBAAQAAEEi3CzQi4CzkJAAIgGAAQhtAAhmgkgAhwGoQiugCh5h8Qh6h9ACitQAAivB8h8QB8h9CwAAQCuAAB9B9QB8B8AACvIAAAIQgCCuh8B5Qh8B6itgCIgBABgAlij0QhjBmABCOIAAAMQACCQBoBkQBoBkCQgCQCPgDBkhoQBkhngCiQIAAgHQgCiOhlhkQhmhkiOACIgHAAQiPABhkBmg");
    var mask_1_graphics_65 = new cjs.Graphics().p("AlBJGQhqglhXhHIAwg7QBNA+BdAhQBdAhBjgBQDqAAChidQChidAAjkQAAjjihidQihidjqAAQhPAAhHASIg+g9QBjghBxAAQEKAAC3CzQC4CzAAEDIgBAAQAAEEi3CzQi4CzkJAAIgGAAQhtAAhmgkgAhwGoQiugCh5h8Qh6h9ACitQAAivB8h8QB8h9CwAAQCuAAB9B9QB8B8AACvIAAAIQgCCuh8B5Qh8B6itgCIgBABgAlij0QhjBmABCOIAAAMQACCQBoBkQBoBkCQgCQCPgDBkhoQBkhngCiQIAAgHQgCiOhlhkQhmhkiOACIgHAAQiPABhkBmg");
    var mask_1_graphics_66 = new cjs.Graphics().p("AlBJGQhqglhXhHIAwg7QBNA+BdAhQBdAhBjgBQDqAAChidQChidAAjkQAAjjihidQihidjqAAQh5AAhnArIhEg1QCEhCCgAAQEKAAC3CzQC4CzAAEDIgBAAQAAEEi3CzQi4CzkJAAIgGAAQhtAAhmgkgAhwGoQiugCh5h8Qh6h9ACitQAAivB8h8QB8h9CwAAQCuAAB9B9QB8B8AACvIAAAIQgCCuh8B5Qh8B6itgCIgBABgAlij0QhjBmABCOIAAAMQACCQBoBkQBoBkCQgCQCPgDBkhoQBkhngCiQIAAgHQgCiOhlhkQhmhkiOACIgHAAQiPABhkBmg");
    var mask_1_graphics_67 = new cjs.Graphics().p("AlBJGQhqglhXhHIAwg7QBNA+BdAhQBdAhBjgBQDqAAChidQChidAAjkQAAjjihidQihidjqAAQinAAiDBRIhDgwQCfhtDOAAQEKAAC3CzQC4CzAAEDIgBAAQAAEEi3CzQi4CzkJAAIgGAAQhtAAhmgkgAhwGoQiugCh5h8Qh6h9ACitQAAivB8h8QB8h9CwAAQCuAAB9B9QB8B8AACvIAAAIQgCCuh8B5Qh8B6itgCIgBABgAlij0QhjBmABCOIAAAMQACCQBoBkQBoBkCQgCQCPgDBkhoQBkhngCiQIAAgHQgCiOhlhkQhmhkiOACIgHAAQiPABhkBmg");
    var mask_1_graphics_68 = new cjs.Graphics().p("AlAJGQhqglhWhHIAwg7QBNA+BdAhQBdAhBjgBQDpAAChidQCiidAAjkQAAjjiiidQihidjpAAQjQAAiZB+IhCgtQCzidD4AAQEJAAC4CzQC3CzAAEDIAAAAQAAEEi4CzQi3CzkJAAIgGAAQhtAAhngkgAhuGoQiugCh6h8Qh6h9ACitQAAivB8h8QB8h9CwAAQCvAAB8B9QB8B8AACvIAAAIQgBCuh8B5Qh9B6itgCIAAABgAlgj0QhkBmABCOIAAAMQADCQBoBkQBnBkCQgCQCQgDBkhoQBkhngDiQIAAgHQgBiOhmhkQhmhkiNACIgIAAQiOABhkBmg");
    var mask_1_graphics_69 = new cjs.Graphics().p("AkcJGQhqglhXhHIAwg7QBNA+BdAhQBdAhBjgBQDqAAChidQChidAAjkQAAjjihidQihidjqAAQjbAAieCLQgeAbgZAdIhCgoQAggmAmgjQC0ieD4AAQEKAAC3CzQC4CzAAEDIgBAAQAAEEi3CzQi4CzkJAAIgGAAQhsAAhngkgAhLGoQiugCh5h8Qh6h9ACitQAAivB8h8QB8h9CwAAQCuAAB9B9QB8B8AACvIAAAIQgCCuh8B5Qh8B6itgCIgBABgAk9j0QhjBmABCOIAAAMQACCQBoBkQBoBkCQgCQCPgDBkhoQBkhngCiQIAAgHQgCiOhlhkQhmhkiOACIgHAAQiPABhkBmg");
    var mask_1_graphics_70 = new cjs.Graphics().p("AkBJGQhqglhXhHIAwg7QBNA+BdAhQBdAhBjgBQDqAAChidQChidAAjkQAAjjihidQihidjqAAQjbAAieCLQg8A2goA/IhLgZQAwhRBMhEQC0ieD4AAQEKAAC3CzQC4CzAAEDIgBAAQAAEEi3CzQi4CzkJAAIgGAAQhtAAhmgkgAgwGoQiugCh5h8Qh6h9ACitQAAivB8h8QB8h9CwAAQCuAAB9B9QB8B8AACvIAAAIQgCCuh8B5Qh8B6itgCIgBABgAkij0QhjBmABCOIAAAMQACCQBoBkQBoBkCQgCQCPgDBkhoQBkhngCiQIAAgHQgCiOhlhkQhmhkiOACIgHAAQiPABhkBmg");
    var mask_1_graphics_71 = new cjs.Graphics().p("AjuJGQhqglhXhHIAwg7QBNA+BdAhQBdAhBjgBQDqAAChidQChidAAjkQAAjjihidQihidjqAAQjbAAieCLQhcBSgtBqIhMgRQAziBBvhjQC0ieD4AAQEKAAC3CzQC4CzAAEDIgBAAQAAEEi3CzQi4CzkJAAIgGAAQhsAAhngkgAgdGoQiugCh5h8Qh6h9ACitQAAivB8h8QB8h9CwAAQCuAAB9B9QB8B8AACvIAAAIQgCCuh8B5Qh8B6itgCIgBABgAkPj0QhjBmABCOIAAAMQACCQBoBkQBoBkCQgCQCPgDBkhoQBkhngCiQIAAgHQgCiOhlhkQhmhkiOACIgHAAQiPABhkBmg");
    var mask_1_graphics_72 = new cjs.Graphics().p("AjhJGQhqglhXhHIAwg7QBNA+BdAhQBdAhBjgBQDqAAChidQChidAAjkQAAjjihidQihidjqAAQjbAAieCLQh9BvgnCbIhLgNQAFgVAHgUQAwibCAhyQC0ieD4AAQEKAAC3CzQC4CzAAEDIgBAAQAAEEi3CzQi4CzkJAAIgGAAQhtAAhmgkgAgQGoQiugCh5h8Qh6h9ACitQAAivB8h8QB8h9CwAAQCuAAB9B9QB8B8AACvIAAAIQgCCuh8B5Qh8B6itgCIgBABgAkCj0QhjBmABCOIAAAMQACCQBoBkQBoBkCQgCQCPgDBkhoQBkhngCiQIAAgHQgCiOhlhkQhmhkiOACIgHAAQiPABhkBmg");
    var mask_1_graphics_73 = new cjs.Graphics().p("AjaJGQhqglhXhHIAwg7QBNA+BdAhQBdAhBjgBQDqAAChidQChidAAjkQAAjjihidQihidjqAAQjbAAieCLQidCLgVDQIhLgIQAHhDATg8QAwibCAhyQC0ieD4AAQEKAAC3CzQC4CzAAEDIgBAAQAAEEi3CzQi4CzkJAAIgGAAQhtAAhmgkgAgJGoQiugCh5h8Qh6h9ACitQAAivB8h8QB8h9CwAAQCuAAB9B9QB8B8AACvIAAAIQgCCuh8B5Qh8B6itgCIgBABgAj7j0QhjBmABCOIAAAMQACCQBoBkQBoBkCPgCQCQgDBkhoQBkhngCiQIAAgHQgCiOhlhkQhmhkiOACIgHAAQiPABhkBmg");
    var mask_1_graphics_119 = new cjs.Graphics().p("AjaJGQhqglhXhHIAwg7QBNA+BdAhQBdAhBjgBQDqAAChidQChidAAjkQAAjjihidQihidjqAAQjbAAieCLQidCLgVDQIhLgIQAHhDATg8QAwibCAhyQC0ieD4AAQEKAAC3CzQC4CzAAEDIgBAAQAAEEi3CzQi4CzkJAAIgGAAQhtAAhmgkgAgJGoQiugCh5h8Qh6h9ACitQAAivB8h8QB8h9CwAAQCuAAB9B9QB8B8AACvIAAAIQgCCuh8B5Qh8B6itgCIgBABgAj7j0QhjBmABCOIAAAMQACCQBoBkQBoBkCPgCQCQgDBkhoQBkhngCiQIAAgHQgCiOhlhkQhmhkiOACIgHAAQiPABhkBmg");
    var mask_1_graphics_120 = new cjs.Graphics().p("AjaJGQhqglhXhHIAwg7QBNA+BdAhQBdAhBjgBQDqAAChidQChidAAjkQAAjjihidQihidjqAAQjbAAieCLQidCLgVDQIhLgIQAHhDATg8QAwibCAhyQC0ieD4AAQEKAAC3CzQC4CzAAEDIgBAAQAAEEi3CzQi4CzkJAAIgGAAQhtAAhmgkgAgJGoQiugCh5h8Qh6h9ACitQAAivB8h8QB8h9CwAAQCuAAB9B9QB8B8AACvIAAAIQgCCuh8B5Qh8B6itgCIgBABgAj7j0QhjBmABCOIAAAMQACCQBoBkQBoBkCPgCQCQgDBkhoQBkhngCiQIAAgHQgCiOhlhkQhmhkiOACIgHAAQiPABhkBmg");
    var mask_1_graphics_121 = new cjs.Graphics().p("AjaJGQhqglhXhHIAwg7QBNA+BdAhQBdAhBjgBQDqAAChidQChidAAjkQAAjjihidQihidjqAAQjbAAieCLQidCLgVDQIhLgIQAHhDATg8QAwibCAhyQC0ieD4AAQEKAAC3CzQC4CzAAEDIgBAAQAAEEi3CzQi4CzkJAAIgGAAQhtAAhmgkgAgJGoQiugCh5h8Qh6h9ACitQAAivB8h8QB8h9CwAAQCuAAB9B9QB8B8AACvIAAAIQgCCuh8B5Qh8B6itgCIgBABgAj7j0QhjBmABCOIAAAMQACCQBoBkQBoBkCPgCQCQgDBkhoQBkhngCiQIAAgHQgCiOhlhkQhmhkiOACIgHAAQiPABhkBmg");
    var mask_1_graphics_122 = new cjs.Graphics().p("AjaJGQhqglhXhHIAwg7QBNA+BdAhQBdAhBjgBQDqAAChidQChidAAjkQAAjjihidQihidjqAAQjbAAieCLQidCLgVDQIhLgIQAHhDATg8QAwibCAhyQC0ieD4AAQEKAAC3CzQC4CzAAEDIgBAAQAAEEi3CzQi4CzkJAAIgGAAQhtAAhmgkgAgJGoQiugCh5h8Qh6h9ACitQAAivB8h8QB8h9CwAAQCuAAB9B9QB8B8AACvIAAAIQgCCuh8B5Qh8B6itgCIgBABgAj7j0QhjBmABCOIAAAMQACCQBoBkQBoBkCPgCQCQgDBkhoQBkhngCiQIAAgHQgCiOhlhkQhmhkiOACIgHAAQiPABhkBmg");
    var mask_1_graphics_123 = new cjs.Graphics().p("AjaJGQhqglhXhHIAwg7QBNA+BdAhQBdAhBjgBQDqAAChidQChidAAjkQAAjjihidQihidjqAAQjbAAieCLQidCLgVDQIhLgIQAHhDATg8QAwibCAhyQC0ieD4AAQEKAAC3CzQC4CzAAEDIgBAAQAAEEi3CzQi4CzkJAAIgGAAQhtAAhmgkgAgJGoQiugCh5h8Qh6h9ACitQAAivB8h8QB8h9CwAAQCuAAB9B9QB8B8AACvIAAAIQgCCuh8B5Qh8B6itgCIgBABgAj7j0QhjBmABCOIAAAMQACCQBoBkQBoBkCPgCQCQgDBkhoQBkhngCiQIAAgHQgCiOhlhkQhmhkiOACIgHAAQiPABhkBmg");
    var mask_1_graphics_124 = new cjs.Graphics().p("AjaJGQhqglhXhHIAwg7QBNA+BdAhQBdAhBjgBQDqAAChidQChidAAjkQAAjjihidQihidjqAAQjbAAieCLQidCLgVDQIhLgIQAHhDATg8QAwibCAhyQC0ieD4AAQEKAAC3CzQC4CzAAEDIgBAAQAAEEi3CzQi4CzkJAAIgGAAQhtAAhmgkgAgJGoQiugCh5h8Qh6h9ACitQAAivB8h8QB8h9CwAAQCuAAB9B9QB8B8AACvIAAAIQgCCuh8B5Qh8B6itgCIgBABgAj7j0QhjBmABCOIAAAMQACCQBoBkQBoBkCPgCQCQgDBkhoQBkhngCiQIAAgHQgCiOhlhkQhmhkiOACIgHAAQiPABhkBmg");
    var mask_1_graphics_125 = new cjs.Graphics().p("AjaJGQhqglhXhHIAwg7QBNA+BdAhQBdAhBjgBQDqAAChidQChidAAjkQAAjjihidQihidjqAAQjbAAieCLQidCLgVDQIhLgIQAHhDATg8QAwibCAhyQC0ieD4AAQEKAAC3CzQC4CzAAEDIgBAAQAAEEi3CzQi4CzkJAAIgGAAQhtAAhmgkgAgJGoQiugCh5h8Qh6h9ACitQAAivB8h8QB8h9CwAAQCuAAB9B9QB8B8AACvIAAAIQgCCuh8B5Qh8B6itgCIgBABgAj7j0QhjBmABCOIAAAMQACCQBoBkQBoBkCPgCQCQgDBkhoQBkhngCiQIAAgHQgCiOhlhkQhmhkiOACIgHAAQiPABhkBmg");
    var mask_1_graphics_126 = new cjs.Graphics().p("AjaJGQhqglhXhHIAwg7QBNA+BdAhQBdAhBjgBQDqAAChidQChidAAjkQAAjjihidQihidjqAAQjbAAieCLQidCLgVDQIhLgIQAHhDATg8QAwibCAhyQC0ieD4AAQEKAAC3CzQC4CzAAEDIgBAAQAAEEi3CzQi4CzkJAAIgGAAQhtAAhmgkgAgJGoQiugCh5h8Qh6h9ACitQAAivB8h8QB8h9CwAAQCuAAB9B9QB8B8AACvIAAAIQgCCuh8B5Qh8B6itgCIgBABgAj7j0QhjBmABCOIAAAMQACCQBoBkQBoBkCPgCQCQgDBkhoQBkhngCiQIAAgHQgCiOhlhkQhmhkiOACIgHAAQiPABhkBmg");
    var mask_1_graphics_127 = new cjs.Graphics().p("AjaJGQhqglhXhHIAwg7QBNA+BdAhQBdAhBjgBQDqAAChidQChidAAjkQAAjjihidQihidjqAAQjbAAieCLQidCLgVDQIhLgIQAHhDATg8QAwibCAhyQC0ieD4AAQEKAAC3CzQC4CzAAEDIgBAAQAAEEi3CzQi4CzkJAAIgGAAQhtAAhmgkgAgJGoQiugCh5h8Qh6h9ACitQAAivB8h8QB8h9CwAAQCuAAB9B9QB8B8AACvIAAAIQgCCuh8B5Qh8B6itgCIgBABgAj7j0QhjBmABCOIAAAMQACCQBoBkQBoBkCPgCQCQgDBkhoQBkhngCiQIAAgHQgCiOhlhkQhmhkiOACIgHAAQiPABhkBmg");
    var mask_1_graphics_128 = new cjs.Graphics().p("AjaJGQhqglhXhHIAwg7QBNA+BdAhQBdAhBjgBQDqAAChidQChidAAjkQAAjjihidQihidjqAAQjbAAieCLQidCLgVDQIhLgIQAHhDATg8QAwibCAhyQC0ieD4AAQEKAAC3CzQC4CzAAEDIgBAAQAAEEi3CzQi4CzkJAAIgGAAQhtAAhmgkgAgJGoQiugCh5h8Qh6h9ACitQAAivB8h8QB8h9CwAAQCuAAB9B9QB8B8AACvIAAAIQgCCuh8B5Qh8B6itgCIgBABgAj7j0QhjBmABCOIAAAMQACCQBoBkQBoBkCPgCQCQgDBkhoQBkhngCiQIAAgHQgCiOhlhkQhmhkiOACIgHAAQiPABhkBmg");
    var mask_1_graphics_129 = new cjs.Graphics().p("AjaJGQhqglhXhHIAwg7QBNA+BdAhQBdAhBjgBQDqAAChidQChidAAjkQAAjjihidQihidjqAAQjbAAieCLQidCLgVDQIhLgIQAHhDATg8QAwibCAhyQC0ieD4AAQEKAAC3CzQC4CzAAEDIgBAAQAAEEi3CzQi4CzkJAAIgGAAQhtAAhmgkgAgJGoQiugCh5h8Qh6h9ACitQAAivB8h8QB8h9CwAAQCuAAB9B9QB8B8AACvIAAAIQgCCuh8B5Qh8B6itgCIgBABgAj7j0QhjBmABCOIAAAMQACCQBoBkQBoBkCPgCQCQgDBkhoQBkhngCiQIAAgHQgCiOhlhkQhmhkiOACIgHAAQiPABhkBmg");
    var mask_1_graphics_130 = new cjs.Graphics().p("AjaJGQhqglhXhHIAwg7QBNA+BdAhQBdAhBjgBQDqAAChidQChidAAjkQAAjjihidQihidjqAAQjbAAieCLQidCLgVDQIhLgIQAHhDATg8QAwibCAhyQC0ieD4AAQEKAAC3CzQC4CzAAEDIgBAAQAAEEi3CzQi4CzkJAAIgGAAQhtAAhmgkgAgJGoQiugCh5h8Qh6h9ACitQAAivB8h8QB8h9CwAAQCuAAB9B9QB8B8AACvIAAAIQgCCuh8B5Qh8B6itgCIgBABgAj7j0QhjBmABCOIAAAMQACCQBoBkQBoBkCPgCQCQgDBkhoQBkhngCiQIAAgHQgCiOhlhkQhmhkiOACIgHAAQiPABhkBmg");
    var mask_1_graphics_131 = new cjs.Graphics().p("AjaJGQhqglhXhHIAwg7QBNA+BdAhQBdAhBjgBQDqAAChidQChidAAjkQAAjjihidQihidjqAAQjbAAieCLQidCLgVDQIhLgIQAHhDATg8QAwibCAhyQC0ieD4AAQEKAAC3CzQC4CzAAEDIgBAAQAAEEi3CzQi4CzkJAAIgGAAQhtAAhmgkgAgJGoQiugCh5h8Qh6h9ACitQAAivB8h8QB8h9CwAAQCuAAB9B9QB8B8AACvIAAAIQgCCuh8B5Qh8B6itgCIgBABgAj7j0QhjBmABCOIAAAMQACCQBoBkQBoBkCPgCQCQgDBkhoQBkhngCiQIAAgHQgCiOhlhkQhmhkiOACIgHAAQiPABhkBmg");
    var mask_1_graphics_132 = new cjs.Graphics().p("AjaJGQhqglhXhHIAwg7QBNA+BdAhQBdAhBjgBQDqAAChidQChidAAjkQAAjjihidQihidjqAAQjbAAieCLQidCLgVDQIhLgIQAHhDATg8QAwibCAhyQC0ieD4AAQEKAAC3CzQC4CzAAEDIgBAAQAAEEi3CzQi4CzkJAAIgGAAQhtAAhmgkgAgJGoQiugCh5h8Qh6h9ACitQAAivB8h8QB8h9CwAAQCuAAB9B9QB8B8AACvIAAAIQgCCuh8B5Qh8B6itgCIgBABgAj7j0QhjBmABCOIAAAMQACCQBoBkQBoBkCPgCQCQgDBkhoQBkhngCiQIAAgHQgCiOhlhkQhmhkiOACIgHAAQiPABhkBmg");
    var mask_1_graphics_133 = new cjs.Graphics().p("AjaJGQhqglhXhHIAwg7QBNA+BdAhQBdAhBjgBQDqAAChidQChidAAjkQAAjjihidQihidjqAAQjbAAieCLQidCLgVDQIhLgIQAHhDATg8QAwibCAhyQC0ieD4AAQEKAAC3CzQC4CzAAEDIgBAAQAAEEi3CzQi4CzkJAAIgGAAQhtAAhmgkgAgJGoQiugCh5h8Qh6h9ACitQAAivB8h8QB8h9CwAAQCuAAB9B9QB8B8AACvIAAAIQgCCuh8B5Qh8B6itgCIgBABgAj7j0QhjBmABCOIAAAMQACCQBoBkQBoBkCPgCQCQgDBkhoQBkhngCiQIAAgHQgCiOhlhkQhmhkiOACIgHAAQiPABhkBmg");
    var mask_1_graphics_134 = new cjs.Graphics().p("AjaJGQhqglhXhHIAwg7QBNA+BdAhQBdAhBjgBQDqAAChidQChidAAjkQAAjjihidQihidjqAAQjbAAieCLQidCLgVDQIhLgIQAHhDATg8QAwibCAhyQC0ieD4AAQEKAAC3CzQC4CzAAEDIgBAAQAAEEi3CzQi4CzkJAAIgGAAQhtAAhmgkgAgJGoQiugCh5h8Qh6h9ACitQAAivB8h8QB8h9CwAAQCuAAB9B9QB8B8AACvIAAAIQgCCuh8B5Qh8B6itgCIgBABgAj7j0QhjBmABCOIAAAMQACCQBoBkQBoBkCPgCQCQgDBkhoQBkhngCiQIAAgHQgCiOhlhkQhmhkiOACIgHAAQiPABhkBmg");
  
    this.timeline.addTween(cjs.Tween.get(mask_1).to({graphics:null,x:0,y:0}).wait(1).to({graphics:mask_1_graphics_1,x:788,y:86.1}).wait(1).to({graphics:mask_1_graphics_2,x:786.7,y:85.1}).wait(1).to({graphics:mask_1_graphics_3,x:785.4,y:82.4}).wait(1).to({graphics:mask_1_graphics_4,x:784.7,y:79.3}).wait(1).to({graphics:mask_1_graphics_5,x:784.4,y:75.5}).wait(1).to({graphics:mask_1_graphics_6,x:784.4,y:71.5}).wait(1).to({graphics:mask_1_graphics_7,x:784.4,y:68.2}).wait(1).to({graphics:mask_1_graphics_8,x:784.4,y:64.3}).wait(1).to({graphics:mask_1_graphics_9,x:785.2,y:61}).wait(1).to({graphics:mask_1_graphics_10,x:787.8,y:58.7}).wait(1).to({graphics:mask_1_graphics_11,x:791.3,y:56.3}).wait(1).to({graphics:mask_1_graphics_12,x:795.1,y:55.2}).wait(1).to({graphics:mask_1_graphics_13,x:798.1,y:55}).wait(1).to({graphics:mask_1_graphics_14,x:802.1,y:55}).wait(1).to({graphics:mask_1_graphics_15,x:806.3,y:55}).wait(1).to({graphics:mask_1_graphics_16,x:809.4,y:55}).wait(1).to({graphics:mask_1_graphics_17,x:813.1,y:55}).wait(1).to({graphics:mask_1_graphics_18,x:815.9,y:55}).wait(1).to({graphics:mask_1_graphics_19,x:817.8,y:55}).wait(1).to({graphics:mask_1_graphics_20,x:818.8,y:55}).wait(1).to({graphics:mask_1_graphics_21,x:819,y:55}).wait(1).to({graphics:mask_1_graphics_22,x:819,y:55}).wait(1).to({graphics:mask_1_graphics_23,x:819,y:55}).wait(1).to({graphics:mask_1_graphics_24,x:819,y:55}).wait(1).to({graphics:mask_1_graphics_25,x:819,y:57.5}).wait(1).to({graphics:mask_1_graphics_26,x:819,y:59.6}).wait(1).to({graphics:mask_1_graphics_27,x:819,y:61.2}).wait(1).to({graphics:mask_1_graphics_28,x:819,y:61.7}).wait(1).to({graphics:mask_1_graphics_29,x:819,y:61.8}).wait(1).to({graphics:mask_1_graphics_30,x:819,y:61.8}).wait(1).to({graphics:mask_1_graphics_31,x:819,y:61.8}).wait(1).to({graphics:mask_1_graphics_32,x:819,y:61.8}).wait(1).to({graphics:mask_1_graphics_33,x:819,y:66.4}).wait(1).to({graphics:mask_1_graphics_34,x:819,y:68.2}).wait(1).to({graphics:mask_1_graphics_35,x:819,y:69.7}).wait(1).to({graphics:mask_1_graphics_36,x:819,y:70.7}).wait(1).to({graphics:mask_1_graphics_37,x:819,y:71.3}).wait(1).to({graphics:mask_1_graphics_38,x:819,y:71.5}).wait(1).to({graphics:mask_1_graphics_39,x:819,y:71.5}).wait(1).to({graphics:mask_1_graphics_40,x:819,y:71.5}).wait(1).to({graphics:mask_1_graphics_41,x:819,y:71.5}).wait(1).to({graphics:mask_1_graphics_42,x:819,y:71.5}).wait(1).to({graphics:mask_1_graphics_43,x:819,y:71.5}).wait(1).to({graphics:mask_1_graphics_44,x:819.5,y:71.5}).wait(1).to({graphics:mask_1_graphics_45,x:822,y:71.5}).wait(1).to({graphics:mask_1_graphics_46,x:824.2,y:71.5}).wait(1).to({graphics:mask_1_graphics_47,x:826,y:71.5}).wait(1).to({graphics:mask_1_graphics_48,x:827.8,y:71.5}).wait(1).to({graphics:mask_1_graphics_49,x:828.8,y:71.5}).wait(1).to({graphics:mask_1_graphics_50,x:829.4,y:71.5}).wait(1).to({graphics:mask_1_graphics_51,x:829.5,y:71.5}).wait(1).to({graphics:mask_1_graphics_52,x:829.5,y:71.5}).wait(1).to({graphics:mask_1_graphics_53,x:829.5,y:71.5}).wait(1).to({graphics:mask_1_graphics_54,x:829.5,y:71.5}).wait(1).to({graphics:mask_1_graphics_55,x:829.5,y:71.5}).wait(1).to({graphics:mask_1_graphics_56,x:829.5,y:70.6}).wait(1).to({graphics:mask_1_graphics_57,x:829.5,y:68.2}).wait(1).to({graphics:mask_1_graphics_58,x:829.5,y:66}).wait(1).to({graphics:mask_1_graphics_59,x:829.5,y:64.3}).wait(1).to({graphics:mask_1_graphics_60,x:829.5,y:63.1}).wait(1).to({graphics:mask_1_graphics_61,x:829.5,y:62.1}).wait(1).to({graphics:mask_1_graphics_62,x:829.5,y:61.8}).wait(1).to({graphics:mask_1_graphics_63,x:829.5,y:61.8}).wait(1).to({graphics:mask_1_graphics_64,x:829.5,y:61.8}).wait(1).to({graphics:mask_1_graphics_65,x:829.5,y:61.8}).wait(1).to({graphics:mask_1_graphics_66,x:829.5,y:61.8}).wait(1).to({graphics:mask_1_graphics_67,x:829.5,y:61.8}).wait(1).to({graphics:mask_1_graphics_68,x:829.3,y:61.8}).wait(1).to({graphics:mask_1_graphics_69,x:825.8,y:61.8}).wait(1).to({graphics:mask_1_graphics_70,x:823.1,y:61.8}).wait(1).to({graphics:mask_1_graphics_71,x:821.2,y:61.8}).wait(1).to({graphics:mask_1_graphics_72,x:819.9,y:61.8}).wait(1).to({graphics:mask_1_graphics_73,x:819.2,y:61.8}).wait(46).to({graphics:mask_1_graphics_119,x:819.2,y:61.8}).wait(1).to({graphics:mask_1_graphics_120,x:819.2,y:61.8}).wait(1).to({graphics:mask_1_graphics_121,x:819.2,y:61.8}).wait(1).to({graphics:mask_1_graphics_122,x:819.2,y:61.8}).wait(1).to({graphics:mask_1_graphics_123,x:819.2,y:61.8}).wait(1).to({graphics:mask_1_graphics_124,x:819.2,y:61.8}).wait(1).to({graphics:mask_1_graphics_125,x:819.2,y:61.8}).wait(1).to({graphics:mask_1_graphics_126,x:819.2,y:61.8}).wait(1).to({graphics:mask_1_graphics_127,x:819.2,y:61.8}).wait(1).to({graphics:mask_1_graphics_128,x:819.2,y:61.8}).wait(1).to({graphics:mask_1_graphics_129,x:819.2,y:61.8}).wait(1).to({graphics:mask_1_graphics_130,x:819.2,y:61.8}).wait(1).to({graphics:mask_1_graphics_131,x:819.2,y:61.8}).wait(1).to({graphics:mask_1_graphics_132,x:819.2,y:61.8}).wait(1).to({graphics:mask_1_graphics_133,x:819.2,y:61.8}).wait(1).to({graphics:mask_1_graphics_134,x:819.2,y:61.8}).wait(1));
  
    // O
    this.shape_1 = new cjs.Shape();
    this.shape_1.graphics.f("#fff").s().p("AjaJGQhqglhXhHIAwg7QBNA+BdAhQBdAhBjgBQDqAAChidQChidAAjkQAAjjihidQihidjqAAQjbAAieCLQidCLgVDQIhLgIQAHhDATg8QAwibCAhyQC0ieD4AAQEKAAC3CzQC4CzAAEDIgBAAQAAEEi3CzQi4CzkJAAIgGAAQhtAAhmgkgAgJGoQiugCh5h8Qh6h9ACitQAAivB8h8QB8h9CwAAQCuAAB9B9QB8B8AACvIAAAIQgCCuh8B5Qh8B6itgCIgBABgAj7j0QhjBmABCOIAAAMQACCQBoBkQBoBkCPgCQCQgDBkhoQBkhngCiQIAAgHQgCiOhlhkQhmhkiOACIgHAAQiPABhkBmg");
    this.shape_1.setTransform(819.2,61.8);
  
    this.instance_2 = new lib.Tween11("synched",0);
    this.instance_2.parent = this;
    this.instance_2.setTransform(819.2,61.8);
    this.instance_2._off = true;
  
    this.instance_3 = new lib.Tween12("synched",0);
    this.instance_3.parent = this;
    this.instance_3.setTransform(819.2,61.8);
    this.instance_3.alpha = 0;
  
    var maskedShapeInstanceList = [this.shape_1,this.instance_2,this.instance_3];
  
    for(var shapedInstanceItr = 0; shapedInstanceItr < maskedShapeInstanceList.length; shapedInstanceItr++) {
      maskedShapeInstanceList[shapedInstanceItr].mask = mask_1;
    }
  
    this.timeline.addTween(cjs.Tween.get({}).to({state:[]}).to({state:[{t:this.shape_1}]},1).to({state:[{t:this.instance_2}]},118).to({state:[{t:this.instance_3}]},15).wait(1));
    this.timeline.addTween(cjs.Tween.get(this.instance_2).wait(119).to({_off:false},0).to({_off:true,alpha:0},15).wait(1));
  
    // S mask (mask)
    var mask_2 = new cjs.Shape();
    mask_2._off = true;
    var mask_2_graphics_1 = new cjs.Graphics().p("AgzgGIA9guQARAVAZASIgkBCQgngagcghg");
    var mask_2_graphics_2 = new cjs.Graphics().p("AAMA0Qg1gfgjgpIA+guQAZAgArAXIALAGIAMAFIgkBDQgPgHgOgIg");
    var mask_2_graphics_3 = new cjs.Graphics().p("AgGAoQg1gfgkgqIA+guQAaAgAqAYQAbAPAfAIIACBQQg2gNgvgbg");
    var mask_2_graphics_4 = new cjs.Graphics().p("AgmAjQg1gfgjgpIA9guQAaAgAqAXQAzAcA8AHIANBNQhbgGhKgrg");
    var mask_2_graphics_5 = new cjs.Graphics().p("AhAAiQg2gegjgqIA+guQAaAgArAXQA+AjBQADIAiBMIgSAAQhxAAhXgzg");
    var mask_2_graphics_6 = new cjs.Graphics().p("AhhAiQg2gegjgqIA+guQAaAgArAXQBEAmBaAAQAcAAAZgFIAfBHQgnAKgtAAQhwAAhYgzg");
    var mask_2_graphics_7 = new cjs.Graphics().p("Ah4AiQg2gegjgqIA+guQAaAgArAXQBFAmBZAAQA6AAAogTIAgBEQg4AbhKAAQhwAAhYgzg");
    var mask_2_graphics_8 = new cjs.Graphics().p("AiVAiQg1gegjgqIA9guQAaAgArAXQBGAmBYAAQBCAAAqgXIBPAgIgQAOIgGAEQhBAxhkAAQhvAAhZgzg");
    var mask_2_graphics_9 = new cjs.Graphics().p("AioAiQg1gegjgqIA9guQAaAgArAXQBGAmBYAAQBKAAAsgeQAPgLAKgNIBSAHQgJAWgPASQgMAQgSAOIgGAEQhBAxhkAAQhvAAhZgzg");
    var mask_2_graphics_10 = new cjs.Graphics().p("AiuAvQg1gfgjgqIA9guQAaAgArAYQBGAlBYAAQBKAAAsgeQAqgeABgyIBMgHIAAAEQAAA+gkAsQgMAQgSAOIgGAFQhBAwhkAAQhvAAhZgyg");
    var mask_2_graphics_11 = new cjs.Graphics().p("AiuBTQg1gfgjgrIA9gtQAaAgArAXQBGAmBYAAQBKAAAsgfQArgeAAg0QAAgNgEgMIA8gzQAUAhAAArQAAA9gkAuQgMAPgSAOIgGAFQhBAwhkAAQhvAAhZgyg");
    var mask_2_graphics_12 = new cjs.Graphics().p("AiuBsQg1gfgjgqIA9gtQAaAfArAXQBGAnBYAAQBKAAAsgfQArggAAgzQAAgpgqgZIAug9IAAABQAVANAPARQAkAoAAA5QAAA9gkAtQgMAQgSAOIgGAEQhBAxhkAAQhvAAhZgzg");
    var mask_2_graphics_13 = new cjs.Graphics().p("AiuB6Qg1gegjgrIA9guQAaAgArAXQBGAnBYAAQBKAAAsgfQArgfAAgzQAAgpgqgaQgOgIgYgKIAZhHQAjAOAUAMIAEADQAVANAPARQAkAoAAA5QAAA9gkAtQgMAQgSAOIgGAEQhBAxhkAAQhvAAhZgzg");
    var mask_2_graphics_14 = new cjs.Graphics().p("AiuCHQg1gfgjgqIA9guQAaAgArAXQBGAnBYAAQBKAAAsgfQArggAAgzQAAgpgqgZQgggThTgZIAbhHQBcAcAmAYIAEACQAVAOAPARQAkAoAAA5QAAA9gkAtQgMAQgSAOIgGAEQhBAxhkAAQhvAAhZgzg");
    var mask_2_graphics_15 = new cjs.Graphics().p("AiuCRQg1gfgjgrIA9guQAaAgArAYQBGAmBYAAQBKAAAsgfQArgfAAg0QAAgogqgaQgmgWhxggIAAAAIgjgKIAihFIAUAGIAFABIARAFQBuAgAqAaIAEADQAVAOAPAQQAkAoAAA4QAAA+gkAuQgMAQgSANIgGAFQhBAwhkAAQhvAAhZgyg");
    var mask_2_graphics_16 = new cjs.Graphics().p("AiuCYQg1gegjgrIA9guQAaAgArAXQBGAnBYAAQBKAAAsgfQArgfAAg0QAAgogqgaQgmgWhxghQgwgNgogOIAlhDQAhALAlAKQCAAlAuAcIAEADQAVANAPARQAkAoAAA4QAAA+gkAtQgMAQgSAOIgGAEQhBAxhkAAQhvAAhZgzg");
    var mask_2_graphics_17 = new cjs.Graphics().p("AiuCjQg1gegjgrIA9guQAaAgArAXQBGAnBYAAQBKAAAsgfQArgfAAg0QAAgogqgaQgmgWhxghQhWgYg/gZIAmhDQA2AWBMAVQCAAlAuAcIAEADQAVANAPARQAkAoAAA4QAAA+gkAtQgMAQgSAOIgGAEQhBAxhkAAQhvAAhZgzg");
    var mask_2_graphics_18 = new cjs.Graphics().p("AiuCvQg1gfgjgqIA9guQAaAgArAXQBGAnBYAAQBKAAAsgfQArggAAg0QAAgogqgZQgmgXhxggQiMgnhNgrIgSgLIBHgvIAOAHQA/AeBqAeQCAAkAuAdIAEADQAVANAPARQAkAoAAA4QAAA+gkAtQgMAQgSAOIgGAEQhBAxhkAAQhvAAhZgzg");
    var mask_2_graphics_19 = new cjs.Graphics().p("AiWDFQg2gfgjgrIA+guQAaAgArAYQBFAmBZAAQBJAAAsgfQAsgfAAg0QAAgpgrgZQgmgWhyggQiugxhMg4QgdgVgXgZIBMggQANALAPAKQA+AsCdAtQB/AkAuAcIAFADQAVAOAPAQQAkAnAAA5QAAA+gkAuQgNAQgRANIgGAFQhCAwhjAAQhwAAhYgyg");
    var mask_2_graphics_20 = new cjs.Graphics().p("Ah+DhQg1gfgkgrIA+guQAaAgArAYQBFAmBZAAQBJAAAsgfQAsgfAAg0QAAgpgqgaQgngVhxggQivgxhMg4QhDgxghhHIBQgOQAaAtAuAgQA+AsCdAtQCAAkAtAcIAFADQAVAOAPAQQAkAnAAA5QAAA+gkAtQgNAQgRAOIgGAFQhBAwhkAAQhvAAhZgyg");
    var mask_2_graphics_21 = new cjs.Graphics().p("AhxEFQg2gfgjgqIA+gvQAaAhArAXQBFAnBZAAQBJAAAsggQAsgfAAg0QAAgpgrgZQgmgXhyggQiugwhMg4QhphMgViDIBQAIQAVBZBNA2QA+AsCdAsQB/AkAuAdIAFACQAVANAPAQQAkApAAA4QAAA/gkAtQgNAQgRANIgGAFQhBAwhkAAQhwAAhYgyg");
    var mask_2_graphics_22 = new cjs.Graphics().p("AhvEpQg1gfgjgqIA9guQAaAgArAXQBFAnBZAAQBKAAAsgfQArggAAg0QAAgpgqgZQgmgXhyggQivgwhMg3QiChfAAiyIAAgHIBMANQACCJBoBJQA+AsCdAsQCAAkAuAcIAEACQAVAOAPARQAkAoAAA5QAAA+gkAtQgMAQgSAOIgGAEQhBAxhkAAQhvAAhZgzg");
    var mask_2_graphics_23 = new cjs.Graphics().p("AhvFZQg1gfgjgrIA9guQAaAgArAYQBFAmBZAAQBKAAAsgfQArgfAAg0QAAgpgqgaQgmgWhyggQivgxhMg3QiChfAAiyQAAg2AOgvIBIAbQgKAjAAAoQAACNBqBKQA+AsCdAsQCAAkAuAcIAEADQAVAOAPAQQAkAoAAA5QAAA+gkAuQgMAQgSANIgGAFQhBAwhkAAQhvAAhZgyg");
    var mask_2_graphics_24 = new cjs.Graphics().p("AhvGDQg1gfgjgqIA9guQAaAgArAXQBFAnBZAAQBKAAAsgfQArggAAg0QAAgpgqgZQgmgXhyggQivgxhMg3QiCheAAiyQAAhrA2hQIAvBKQgZAzAAA+QAACNBqBLQA+ArCdAsQCAAkAuAdIAEACQAVAOAPARQAkAoAAA5QAAA+gkAtQgMAQgSAOIgGAEQhBAxhkAAQhvAAhZgzg");
    var mask_2_graphics_25 = new cjs.Graphics().p("AhvGhQg1gfgjgrIA9guQAaAgArAYQBFAmBZAAQBKAAAsgfQArgfAAg0QAAgpgqgaQgmgWhyggQivgxhMg4QiCheAAiyQAAiVBqhgIAmBIQhEBHAABnQAACNBqBKQA+ArCdAtQCAAkAuAcIAEADQAVAOAPAQQAkAoAAA5QAAA+gkAuQgMAQgSANIgGAFQhBAwhkAAQhvAAhZgyg");
    var mask_2_graphics_26 = new cjs.Graphics().p("AhvG5Qg1gfgjgqIA9guQAaAgArAXQBFAnBZAAQBKAAAsgfQArggAAg0QAAgpgqgZQgmgXhyggQivgxhMg3QiCheAAiyQAAicB0hjQAagWAdgSIAhBIQgRALgQANQhfBNAAB6QAACNBqBKQA+AsCdAsQCAAkAuAdIAEACQAVAOAPARQAkAoAAA5QAAA+gkAtQgMAQgSAOIgGAEQhBAxhkAAQhvAAhZgzg");
    var mask_2_graphics_27 = new cjs.Graphics().p("AhvHIQg1gfgjgqIA9gvQAaAhArAXQBFAnBZAAQBKAAAsggQArgfAAg0QAAgpgqgZQgmgXhyggQivgxhMg4QiCheAAixQAAidB0hiQA0gtBDgYIAXBKQgwATgnAgQhfBMAAB7QAACNBqBKQA+AsCdAsQCAAkAuAdIAEACQAVAOAPAQQAkApAAA4QAAA/gkAtQgMAQgSANIgGAFQhBAwhkAAQhvAAhZgyg");
    var mask_2_graphics_28 = new cjs.Graphics().p("AhvHSQg1gfgjgrIA9guQAaAgArAYQBFAmBZAAQBKAAAsgfQArgfAAg0QAAgpgqgaQgmgWhyggQivgxhMg4QiChfAAixQAAicB0hiQBAg3BVgZQATgFATgEIgFBRQhJATg2AsQhfBNAAB7QAACNBqBJQA+AsCdAtQCAAkAuAcIAEADQAVAOAPAQQAkAoAAA5QAAA+gkAtQgMAQgSAOIgGAFQhBAwhkAAQhvAAhZgyg");
    var mask_2_graphics_29 = new cjs.Graphics().p("AhvHXQg1gfgjgrIA9guQAaAgArAYQBFAmBZAAQBKAAAsgfQArgfAAg0QAAgpgqgaQgmgWhyggQivgxhMg4QiChfAAixQAAicB0hiQBAg3BVgZQA+gSBKgBIgSBOQh/AHhVBFQhfBNAAB7QAACNBqBJQA+AsCdAtQCAAkAuAcIAEADQAVAOAPAQQAkAoAAA5QAAA+gkAuQgMAPgSAOIgGAFQhBAwhkAAQhvAAhZgyg");
    var mask_2_graphics_30 = new cjs.Graphics().p("AhvHXQg1gfgjgrIA9guQAaAgArAYQBFAmBZAAQBKAAAsgfQArgfAAg0QAAgpgqgaQgmgWhyggQivgxhMg4QiChfAAixQAAicB0hiQBAg3BVgZQBDgTBSAAQAvAAAsAHIgkBJQgbgDgbAAQiVAAhfBNQhfBNAAB7QAACNBqBJQA+AsCdAtQCAAkAuAcIAEADQAVAOAPAQQAkAoAAA5QAAA+gkAuQgMAPgSAOIgGAFQhBAwhkAAQhvAAhZgyg");
    var mask_2_graphics_31 = new cjs.Graphics().p("AhvHXQg1gfgjgrIA9guQAaAgArAYQBFAmBZAAQBKAAAsgfQArgfAAg0QAAgpgqgaQgmgWhyggQivgxhMg4QiChfAAixQAAicB0hiQBAg3BVgZQBDgTBSAAQBOAABGATIARAFIADABIgsBEQg6gQhBAAQiVAAhfBNQhfBNAAB7QAACNBqBJQA+AsCdAtQCAAkAuAcIAEADQAVAOAPAQQAkAoAAA5QAAA+gkAuQgMAPgSAOIgGAFQhBAwhkAAQhvAAhZgyg");
    var mask_2_graphics_32 = new cjs.Graphics().p("AiKHXQg1gfgjgrIA9guQAaAgArAYQBGAmBYAAQBKAAAsgfQArgfAAg0QAAgpgqgaQgmgWhyggQivgxhMg4QiChfAAixQAAicB0hiQBAg3BVgZQBEgTBRAAQBOAABGATIARAFQASAGASAHQAUAIASAJIgxBAIgZgLQhMgehYAAQiVAAhfBNQhfBNAAB7QAACNBqBJQA+AsCdAtQCAAkAuAcIAEADQAVAOAPAQQAkAoAAA5QAAA+gkAuQgNAPgRAOIgGAFQhBAwhkAAQhvAAhZgygAFfm1IACACIgDAEg");
    var mask_2_graphics_33 = new cjs.Graphics().p("AiKHXQg1gfgjgrIA9guQAaAgArAYQBGAmBYAAQBKAAAsgfQArgfAAg0QAAgpgqgaQgmgWhyggQivgxhMg4QiChfAAixQAAicB0hiQBAg3BVgZQBEgTBRAAQBOAABGATIARAFQASAGASAHQAvATArAdIgwA/QgTgNgVgLIgCgBQgSgJgSgHQhMgehYAAQiVAAhfBNQhfBNAAB7QAACNBqBJQA+AsCdAtQCAAkAuAcIAEADQAVAOAPAQQAkAoAAA5QAAA+gkAuQgNAPgRAOIgGAFQhBAwhkAAQhvAAhZgyg");
    var mask_2_graphics_34 = new cjs.Graphics().p("AiqHXQg1gfgkgrIA+guQAaAgArAYQBFAmBZAAQBJAAAsgfQAsgfAAg0QAAgpgqgaQgngWhwggQiwgxhMg4QiChfgBixQAAicB0hiQBBg3BVgZQBEgTBQAAQBPAABGATIARAFQASAGASAHQA6AXAzAmQAZATAVATIg0A7IgdgbIgCACQgfgagngUIgCgBQgSgJgSgHQhMgehZAAQiVAAheBNQhfBNAAB7QAACNBqBJQA+AsCdAtQCAAkAtAcIAFADQAVAOAPAQQAkAoAAA5QAAA+gkAuQgNAPgRAOIgGAFQhBAwhkAAQhvAAhZgyg");
    var mask_2_graphics_35 = new cjs.Graphics().p("AjEHXQg2gfgjgrIA+guQAaAgArAYQBFAmBZAAQBJAAAsgfQAsgfAAg0QAAgpgrgaQgmgWhxggQivgxhMg4QiDhfAAixQAAicB0hiQBAg3BWgZQBEgTBQAAQBPAABGATIARAFQASAGASAHQA5AXA0AmQA4AqAfAqIALAPIgxAwIhUhNIgDACQgfgagmgUIgCgBQgSgJgSgHQhNgehYAAQiVAAheBNQhfBNAAB7QAACNBqBJQA+AsCeAtQB+AkAuAcIAFADQAVAOAPAQQAkAoAAA5QAAA+gkAuQgNAPgRAOIgGAFQhBAwhkAAQhwAAhYgyg");
    var mask_2_graphics_36 = new cjs.Graphics().p("AjEHXQg2gfgjgrIA+guQAaAgArAYQBFAmBZAAQBJAAAsgfQAsgfAAg0QAAgpgrgaQgmgWhxggQivgxhMg4QiDhfAAixQAAicB0hiQBAg3BWgZQBEgTBQAAQBPAABGATIARAFQASAGASAHQA5AXA0AmQA4AqAfAqIALAPIhbBaIg0g2IAkgkQgpgrg5geIgCgBQgSgJgSgHQhNgehYAAQiVAAheBNQhfBNAAB7QAACNBqBJQA+AsCeAtQB+AkAuAcIAFADQAVAOAPAQQAkAoAAA5QAAA+gkAuQgNAPgRAOIgGAFQhBAwhkAAQhwAAhYgyg");
    var mask_2_graphics_37 = new cjs.Graphics().p("AjEHXQg2gfgjgrIA+guQAaAgArAYQBFAmBZAAQBJAAAsgfQAsgfAAg0QAAgpgrgaQgmgWhxggQivgxhMg4QiDhfAAixQAAicB0hiQBAg3BWgZQBEgTBQAAQBPAABGATIARAFQASAGASAHQA5AXA0AmQA4AqAfAqIALAPIiLCLIg1g5IABgBIACABIBShSQgpgrg5geIgCgBQgSgJgSgHQhNgehYAAQiVAAheBNQhfBNAAB7QAACNBqBJQA+AsCeAtQB+AkAuAcIAFADQAVAOAPAQQAkAoAAA5QAAA+gkAuQgNAPgRAOIgGAFQhBAwhkAAQhwAAhYgyg");
    var mask_2_graphics_38 = new cjs.Graphics().p("AjEHXQg2gfgjgrIA+guQAaAgArAYQBFAmBZAAQBJAAAsgfQAsgfAAg0QAAgpgrgaQgmgWhxggQivgxhMg4QiDhfAAixQAAicB0hiQBAg3BWgZQBEgTBQAAQBPAABGATIARAFQASAGASAHQA5AXA0AmQA4AqAfAqIALAPIi9C8IgSgTIgQgOIAghKIACABIBShSQgpgrg5geIgCgBQgSgJgSgHQhNgehYAAQiVAAheBNQhfBNAAB7QAACNBqBJQA+AsCeAtQB+AkAuAcIAFADQAVAOAPAQQAkAoAAA5QAAA+gkAuQgNAPgRAOIgGAFQhBAwhkAAQhwAAhYgyg");
    var mask_2_graphics_39 = new cjs.Graphics().p("AjEHXQg2gfgjgrIA+guQAaAgArAYQBFAmBZAAQBJAAAsgfQAsgfAAg0QAAgpgrgaQgmgWhxggQivgxhMg4QiDhfAAixQAAicB0hiQBAg3BWgZQBEgTBQAAQBPAABGATIARAFQASAGASAHQA5AXA0AmQA4AqAfAqIALAPIi9C8IgSgTQgQgPgUgPIgMgIIgPgLIAchPQATALAQAOIASAQIBShSQgpgrg5geIgCgBQgSgJgSgHQhNgehYAAQiVAAheBNQhfBNAAB7QAACNBqBJQA+AsCeAtQB+AkAuAcIAFADQAVAOAPAQQAkAoAAA5QAAA+gkAuQgNAPgRAOIgGAFQhBAwhkAAQhwAAhYgyg");
    var mask_2_graphics_40 = new cjs.Graphics().p("AjEHXQg2gfgjgrIA+guQAaAgArAYQBFAmBZAAQBJAAAsgfQAsgfAAg0QAAgpgrgaQgmgWhxggQivgxhMg4QiDhfAAixQAAicB0hiQBAg3BWgZQBEgTBQAAQBPAABGATIARAFQASAGASAHQA5AXA0AmQA4AqAfAqIALAPIi9C8IgSgTQgQgPgUgPIgMgIQgkgagjgPIAQhPQAXAHAWALIAWALQATAMARAOIASAQIBShSQgpgrg5geIgCgBQgSgJgSgHQhNgehYAAQiVAAheBNQhfBNAAB7QAACNBqBJQA+AsCeAtQB+AkAuAcIAFADQAVAOAPAQQAkAoAAA5QAAA+gkAuQgNAPgRAOIgGAFQhBAwhkAAQhwAAhYgyg");
    var mask_2_graphics_41 = new cjs.Graphics().p("AjEHXQg2gfgjgrIA+guQAaAgArAYQBFAmBZAAQBJAAAsgfQAsgfAAg0QAAgpgrgaQgmgWhxggQivgxhMg4QiDhfAAixQAAicB0hiQBAg3BWgZQBEgTBQAAQBPAABGATIARAFQASAGASAHQA5AXA0AmQA4AqAfAqIALAPIi9C8IgSgTQgQgPgUgPIgMgIQg4gog0gNQgOgDgOgBIAHhMQBAAEA3AbIAWALQATAMARAOIASAQIBShSQgpgrg5geIgCgBQgSgJgSgHQhNgehYAAQiVAAheBNQhfBNAAB7QAACNBqBJQA+AsCeAtQB+AkAuAcIAFADQAVAOAPAQQAkAoAAA5QAAA+gkAuQgNAPgRAOIgGAFQhBAwhkAAQhwAAhYgyg");
    var mask_2_graphics_42 = new cjs.Graphics().p("AjEHXQg2gfgjgrIA+guQAaAgArAYQBFAmBZAAQBJAAAsgfQAsgfAAg0QAAgpgrgaQgmgWhxggQivgxhMg4QiDhfAAixQAAicB0hiQBAg3BWgZQBEgTBQAAQBPAABGATIARAFQASAGASAHQA5AXA0AmQA4AqAfAqIALAPIi9C8IgSgTQgQgPgUgPIgMgIQg4gog0gNQgPgDgPgCIhRhEQARgEASgCQAPgCAQAAQBOAABBAgIAWALQATAMARAOIASAQIBShSQgpgrg5geIgCgBQgSgJgSgHQhNgehYAAQiVAAheBNQhfBNAAB7QAACNBqBJQA+AsCeAtQB+AkAuAcIAFADQAVAOAPAQQAkAoAAA5QAAA+gkAuQgNAPgRAOIgGAFQhBAwhkAAQhwAAhYgyg");
    var mask_2_graphics_43 = new cjs.Graphics().p("AjEHXQg2gfgjgrIA+guQAaAgArAYQBFAmBZAAQBJAAAsgfQAsgfAAg0QAAgpgrgaQgmgWhxggQivgxhMg4QiDhfAAixQAAicB0hiQBAg3BWgZQBEgTBQAAQBPAABGATIARAFQASAGASAHQA5AXA0AmQA4AqAfAqIALAPIi9C8IgSgTQgQgPgUgPIgMgIQhQg6hKAAQggAAgbAJIhUgoQA6gtBWAAQBOAABBAgIAWALQATAMARAOIASAQIBShSQgpgrg5geIgCgBQgSgJgSgHQhNgehYAAQiVAAheBNQhfBNAAB7QAACNBqBJQA+AsCeAtQB+AkAuAcIAFADQAVAOAPAQQAkAoAAA5QAAA+gkAuQgNAPgRAOIgGAFQhBAwhkAAQhwAAhYgyg");
    var mask_2_graphics_44 = new cjs.Graphics().p("AjEHXQg2gfgjgrIA+guQAaAgArAYQBFAmBZAAQBJAAAsgfQAsgfAAg0QAAgpgrgaQgmgWhxggQivgxhMg4QiDhfAAixQAAicB0hiQBAg3BWgZQBEgTBQAAQBPAABGATIARAFQASAGASAHQA5AXA0AmQA4AqAfAqIALAPIi9C8IgSgTQgQgPgUgPIgMgIQhQg6hKAAQg4AAgmAaQgVAPgIASIhIgXQANghAhgcQA7gzBbAAQBOAABBAgIAWALQATAMARAOIASAQIBShSQgpgrg5geIgCgBQgSgJgSgHQhNgehYAAQiVAAheBNQhfBNAAB7QAACNBqBJQA+AsCeAtQB+AkAuAcIAFADQAVAOAPAQQAkAoAAA5QAAA+gkAuQgNAPgRAOIgGAFQhBAwhkAAQhwAAhYgyg");
    var mask_2_graphics_45 = new cjs.Graphics().p("AjEHXQg2gfgjgrIA+guQAaAgArAYQBFAmBZAAQBJAAAsgfQAsgfAAg0QAAgpgrgaQgmgWhxggQivgxhMg4QiDhfAAixQAAicB0hiQBAg3BWgZQBEgTBQAAQBPAABGATIARAFQASAGASAHQA5AXA0AmQA4AqAfAqIALAPIi9C8IgSgTQgQgPgUgPIgMgIQhQg6hKAAQg4AAgmAaQgjAZAAAgIABAKIhIAZQgEgQAAgTQAAg+A3guQA7gzBbAAQBOAABBAgIAWALQATAMARAOIASAQIBShSQgpgrg5geIgCgBQgSgJgSgHQhNgehYAAQiVAAheBNQhfBNAAB7QAACNBqBJQA+AsCeAtQB+AkAuAcIAFADQAVAOAPAQQAkAoAAA5QAAA+gkAuQgNAPgRAOIgGAFQhBAwhkAAQhwAAhYgyg");
    var mask_2_graphics_46 = new cjs.Graphics().p("AjEHXQg2gfgjgrIA+guQAaAgArAYQBFAmBZAAQBJAAAsgfQAsgfAAg0QAAgpgrgaQgmgWhxggQivgxhMg4QiDhfAAixQAAicB0hiQBAg3BWgZQBEgTBQAAQBPAABGATIARAFQASAGASAHQA5AXA0AmQA4AqAfAqIALAPIi9C8IgSgTQgQgPgUgPIgMgIQhQg6hKAAQg4AAgmAaQgjAZAAAgQAAAnAnAZIg1A4Qg9gtAAhLQAAg+A3guQA7gzBbAAQBOAABBAgIAWALQATAMARAOIASAQIBShSQgpgrg5geIgCgBQgSgJgSgHQhNgehYAAQiVAAheBNQhfBNAAB7QAACNBqBJQA+AsCeAtQB+AkAuAcIAFADQAVAOAPAQQAkAoAAA5QAAA+gkAuQgNAPgRAOIgGAFQhBAwhkAAQhwAAhYgyg");
    var mask_2_graphics_47 = new cjs.Graphics().p("AjEHXQg2gfgjgrIA+guQAaAgArAYQBFAmBZAAQBJAAAsgfQAsgfAAg0QAAgpgrgaQgmgWhxggQivgxhMg4QiDhfAAixQAAicB0hiQBAg3BWgZQBEgTBQAAQBPAABGATIARAFQASAGASAHQA5AXA0AmQA4AqAfAqIALAPIi9C8IgSgTQgQgPgUgPIgMgIQhQg6hKAAQg4AAgmAaQgjAZAAAgQAAApArAZQAYAOA1ASIgqBCQgzgSgZgPQhNgvAAhUQAAg+A3guQA7gzBbAAQBOAABBAgIAWALQATAMARAOIASAQIBShSQgpgrg5geIgCgBQgSgJgSgHQhNgehYAAQiVAAheBNQhfBNAAB7QAACNBqBJQA+AsCeAtQB+AkAuAcIAFADQAVAOAPAQQAkAoAAA5QAAA+gkAuQgNAPgRAOIgGAFQhBAwhkAAQhwAAhYgyg");
    var mask_2_graphics_48 = new cjs.Graphics().p("AjEHXQg2gfgjgrIA+guQAaAgArAYQBFAmBZAAQBJAAAsgfQAsgfAAg0QAAgpgrgaQgmgWhxggQivgxhMg4QiDhfAAixQAAicB0hiQBAg3BWgZQBEgTBQAAQBPAABGATIARAFQASAGASAHQA5AXA0AmQA4AqAfAqIALAPIi9C8IgSgTQgQgPgUgPIgMgIQhQg6hKAAQg4AAgmAaQgjAZAAAgQAAApArAZQAmAXBxAgIANADIg2A/QhtgegqgaQhNgvAAhUQAAg+A3guQA7gzBbAAQBOAABBAgIAWALQATAMARAOIASAQIBShSQgpgrg5geIgCgBQgSgJgSgHQhNgehYAAQiVAAheBNQhfBNAAB7QAACNBqBJQA+AsCeAtQB+AkAuAcIAFADQAVAOAPAQQAkAoAAA5QAAA+gkAuQgNAPgRAOIgGAFQhBAwhkAAQhwAAhYgyg");
    var mask_2_graphics_49 = new cjs.Graphics().p("AjEHXQg2gfgjgrIA+guQAaAgArAYQBFAmBZAAQBJAAAsgfQAsgfAAg0QAAgpgrgaQgmgWhxggQivgxhMg4QiDhfAAixQAAicB0hiQBAg3BWgZQBEgTBQAAQBPAABGATIARAFQASAGASAHQA5AXA0AmQA4AqAfAqIALAPIi9C8IgSgTQgQgPgUgPIgMgIQhQg6hKAAQg4AAgmAaQgjAZAAAgQAAApArAZQAmAXBxAgQA3APAuAQIAFADIg5A7IhFgUQh+gjgugcQhNgvAAhUQAAg+A3guQA7gzBbAAQBOAABBAgIAWALQATAMARAOIASAQIBShSQgpgrg5geIgCgBQgSgJgSgHQhNgehYAAQiVAAheBNQhfBNAAB7QAACNBqBJQA+AsCeAtQB+AkAuAcIAFADQAVAOAPAQQAkAoAAA5QAAA+gkAuQgNAPgRAOIgGAFQhBAwhkAAQhwAAhYgyg");
    var mask_2_graphics_50 = new cjs.Graphics().p("AjEHXQg2gfgjgrIA+guQAaAgArAYQBFAmBZAAQBJAAAsgfQAsgfAAg0QAAgpgrgaQgmgWhxggQivgxhMg4QiDhfAAixQAAicB0hiQBAg3BWgZQBEgTBQAAQBPAABGATIARAFQASAGASAHQA5AXA0AmQA4AqAfAqIALAPIi9C8IgSgTQgQgPgUgPIgMgIQhQg6hKAAQg4AAgmAaQgjAZAAAgQAAApArAZQAmAXBxAgQA3APAuAQIAkAOQAcAKAXALIhEA1IgTgHQgzgUhGgTQh+gjgugcQhNgvAAhUQAAg+A3guQA7gzBbAAQBOAABBAgIAWALQATAMARAOIASAQIBShSQgpgrg5geIgCgBQgSgJgSgHQhNgehYAAQiVAAheBNQhfBNAAB7QAACNBqBJQA+AsCeAtQB+AkAuAcIAFADQAVAOAPAQQAkAoAAA5QAAA+gkAuQgNAPgRAOIgGAFQhBAwhkAAQhwAAhYgyg");
    var mask_2_graphics_51 = new cjs.Graphics().p("AjEHXQg2gfgjgrIA+guQAaAgArAYQBFAmBZAAQBJAAAsgfQAsgfAAg0QAAgpgrgaQgmgWhxggQivgxhMg4QiDhfAAixQAAicB0hiQBAg3BWgZQBEgTBQAAQBPAABGATIARAFQASAGASAHQA5AXA0AmQA4AqAfAqIALAPIi9C8IgSgTQgQgPgUgPIgMgIQhQg6hKAAQg4AAgmAaQgjAZAAAgQAAApArAZQAmAXBxAgQA3APAuAQIAkAOQBJAcAqAeIASAPIhTAjQgVgNgdgOIgkgOQgzgUhGgTQh+gjgugcQhNgvAAhUQAAg+A3guQA7gzBbAAQBOAABBAgIAWALQATAMARAOIASAQIBShSQgpgrg5geIgCgBQgSgJgSgHQhNgehYAAQiVAAheBNQhfBNAAB7QAACNBqBJQA+AsCeAtQB+AkAuAcIAFADQAVAOAPAQQAkAoAAA5QAAA+gkAuQgNAPgRAOIgGAFQhBAwhkAAQhwAAhYgyg");
    var mask_2_graphics_52 = new cjs.Graphics().p("AjEHXQg2gfgjgrIA+guQAaAgArAYQBFAmBZAAQBJAAAsgfQAsgfAAg0QAAgpgrgaQgmgWhxggQivgxhMg4QiDhfAAixQAAicB0hiQBAg3BWgZQBEgTBQAAQBPAABGATIARAFQASAGASAHQA5AXA0AmQA4AqAfAqIALAPIi9C8IgSgTQgQgPgUgPIgMgIQhQg6hKAAQg4AAgmAaQgjAZAAAgQAAApArAZQAmAXBxAgQA3APAuAQIAkAOQBJAcAqAeQArAgAdApIhZAPQgPgQgUgOQgYgRgngSIgkgOQgzgUhGgTQh+gjgugcQhNgvAAhUQAAg+A3guQA7gzBbAAQBOAABBAgIAWALQATAMARAOIASAQIBShSQgpgrg5geIgCgBQgSgJgSgHQhNgehYAAQiVAAheBNQhfBNAAB7QAACNBqBJQA+AsCeAtQB+AkAuAcIAFADQAVAOAPAQQAkAoAAA5QAAA+gkAuQgNAPgRAOIgGAFQhBAwhkAAQhwAAhYgyg");
    var mask_2_graphics_53 = new cjs.Graphics().p("AjOHXQg1gfgkgrIA+guQAaAgArAYQBFAmBaAAQBIAAAsgfQAsgfAAg0QAAgpgqgaQgngWhwggQiwgxhMg4QiChfgBixQAAicB0hiQBBg3BVgZQBEgTBRAAQBOAABGATIARAFQASAGASAHQA6AXAzAmQA4AqAgAqIAKAPIi8C8IgTgTQgQgPgUgPIgMgIQhQg6hJAAQg4AAgmAaQgkAZABAgQgBApAsAZQAlAXByAgQA3APAtAQIAkAOQBJAcAqAeQAyAlAgAxQASAdAMAiIhUAAQgbg2g0glQgZgRgngSIgkgOQgzgUhGgTQh9gjgvgcQhMgvAAhUQAAg+A2guQA8gzBbAAQBNAABBAgIAWALQATAMARAOIASAQIBThSQgpgrg6geIgCgBQgSgJgSgHQhMgehYAAQiWAAheBNQhfBNAAB7QAACNBqBJQA+AsCeAtQB/AkAtAcIAFADQAVAOAPAQQAkAoAAA5QAAA+gkAuQgNAPgRAOIgGAFQhBAwhjAAQhwAAhZgyg");
    var mask_2_graphics_54 = new cjs.Graphics().p("AjWHXQg2gfgjgrIA+guQAZAgArAYQBGAmBaAAQBIAAAsgfQAsgfAAg0QAAgpgrgaQgmgWhxggQivgxhNg4QiChfAAixQAAicB0hiQBAg3BVgZQBFgTBRAAQBOAABGATIAQAFQATAGARAHQA6AXAzAmQA5AqAfAqIAKAPIi8C8IgTgTQgPgPgVgPIgLgIQhQg6hJAAQg5AAgmAaQgjAZAAAgQAAApArAZQAlAXByAgQA3APAtAQIAkAOQBKAcApAeQAzAlAfAxQAmA8AJBPIhQgNQgThghRg6QgYgRgogSIgkgOQgygUhGgTQh+gjgugcQhNgvAAhUQAAg+A2guQA8gzBcAAQBMAABCAgIAVALQAUAMAQAOIASAQIBThSQgpgrg6geIgCgBQgRgJgTgHQhMgehXAAQiWAAheBNQhfBNgBB7QAACNBqBJQA/AsCdAtQB/AkAuAcIAEADQAWAOAOAQQAlAogBA5QABA+glAuQgMAPgRAOIgHAFQhBAwhiAAQhxAAhYgyg");
    var mask_2_graphics_55 = new cjs.Graphics().p("AjYHXQg1gfgjgrIA9guQAaAgArAYQBGAmBZAAQBJAAAsgfQArgfAAg0QAAgpgqgaQgmgWhxggQiwgxhMg4QiChfAAixQAAicB0hiQBAg3BVgZQBEgTBSAAQBNAABGATIARAFQATAGARAHQA6AXAzAmQA5AqAfAqIAKAPIi8C8IgTgTQgQgPgUgPIgLgIQhRg6hIAAQg5AAgmAaQgjAZAAAgQAAApArAZQAlAXByAgQA3APAtAQIAkAOQBJAcAqAeQAzAlAfAxQAxBOAABtQAAAUgBAUIhMgUIABgTQAAiNhqhLQgZgRgngSIgkgOQgzgUhFgTQh+gjgvgcQhMgvAAhUQAAg+A2guQA8gzBcAAQBMAABCAgIAVALQAUAMAQAOIASAQIBThSQgpgrg6geIgCgBQgRgJgTgHQhMgehXAAQiWAAhfBNQhfBNAAB7QAACNBqBJQA+AsCeAtQB/AkAuAcIAEADQAVAOAPAQQAkAoAAA5QAAA+gkAuQgMAPgSAOIgGAFQhBAwhjAAQhwAAhZgyg");
    var mask_2_graphics_56 = new cjs.Graphics().p("AjYHXQg1gfgjgrIA9guQAaAgArAYQBGAmBZAAQBJAAAsgfQArgfAAg0QAAgpgqgaQgmgWhxggQiwgxhMg4QiChfAAixQAAicB0hiQBAg3BVgZQBEgTBSAAQBNAABGATIARAFQATAGARAHQA6AXAzAmQA5AqAfAqIAKAPIi8C8IgTgTQgQgPgUgPIgLgIQhRg6hIAAQg5AAgmAaQgjAZAAAgQAAApArAZQAlAXByAgQA3APAtAQIAkAOQBJAcAqAeQAzAlAfAxQAxBOAABtQAABJgVA+IhDgrQAMgqAAgxQAAiNhqhLQgZgRgngSIgkgOQgzgUhFgTQh+gjgvgcQhMgvAAhUQAAg+A2guQA8gzBcAAQBMAABCAgIAVALQAUAMAQAOIASAQIBThSQgpgrg6geIgCgBQgRgJgTgHQhMgehXAAQiWAAhfBNQhfBNAAB7QAACNBqBJQA+AsCeAtQB/AkAuAcIAEADQAVAOAPAQQAkAoAAA5QAAA+gkAuQgMAPgSAOIgGAFQhBAwhjAAQhwAAhZgyg");
    var mask_2_graphics_57 = new cjs.Graphics().p("AE5HcQANgVAJgYQAUg0AAg/QAAiNhqhLQgZgRgngRIgkgPQgzgThFgUQh+gjgvgcQhMgvAAhTQAAg/A2guQA8gzBcAAQBMABBCAfIAVAMQAUALAQAOIASARIBThTQgpgqg6gfIgCgBQgRgJgTgHQhMgehXAAQiWAAhfBOQhfBMAAB7QAACNBqBKQA+AsCeAsQB/AkAuAdIAEACQAVAOAPAQQAkApAAA4QAAA/gkAtQgMAQgSANIgGAFQhBAwhjAAQhwAAhZgyQg1gfgjgqIA9gvQAaAhArAXQBGAnBZAAQBJAAAsggQArgfAAg0QAAgpgqgZQgmgXhxggQiwgxhMg4QiCheAAixQAAidB0hiQBAg3BVgYQBEgUBSAAQBNAABGAUIARAEQATAGARAIQA6AXAzAlQA5AqAfAqIAKAPIi8C8IgTgTQgQgPgUgPIgLgIQhRg6hIAAQg5AAgmAaQgjAaAAAfQAAApArAZQAlAXByAgQA3AQAtAQIAkANQBJAcAqAfQAzAkAfAxQAxBOAABtQAACChCBbg");
    var mask_2_graphics_58 = new cjs.Graphics().p("AELHzQAugrAWg6QAUgzAAg/QAAiNhqhLQgZgRgngSIgkgOQgzgThFgTQh+gkgvgcQhMgvAAhUQAAg+A2guQA8gzBcAAQBMAABCAgIAVALQAUAMAQAOIASAQIBThSQgpgrg6geIgCgBQgRgJgTgHQhMgehXAAQiWAAhfBNQhfBNAAB7QAACNBqBJQA+AsCeAtQB/AkAuAcIAEADQAVAOAPAQQAkAoAAA5QAAA+gkAuQgMAPgSAOIgGAFQhBAwhjAAQhwAAhZgyQg1gfgjgrIA9guQAaAgArAYQBGAmBZAAQBJAAAsgfQArgfAAg0QAAgpgqgaQgmgWhxggQiwgxhMg4QiCheAAiyQAAicB0hiQBAg3BVgZQBEgTBSAAQBNAABGATIARAFQATAGARAHQA6AXAzAmQA5AqAfAqIAKAPIi8C8IgTgTQgQgPgUgPIgLgIQhRg6hIAAQg5AAgmAaQgjAZAAAgQAAApArAZQAlAXByAgQA3APAtAQIAkAOQBJAdAqAdQAzAlAfAxQAxBOAABtQAACvh5BpIgGAFg");
    var mask_2_graphics_59 = new cjs.Graphics().p("ADUIEQAXgNAUgRQA4guAYhBQAUgzAAg/QAAiNhqhLQgZgRgngSIgkgNQgzgUhFgTQh+gkgvgcQhMgvAAhUQAAg+A2guQA8gzBcAAQBMAABCAgIAVALQAUAMAQAOIASAQIBThSQgpgrg6geIgCgBQgRgJgTgHQhMgehXAAQiWAAhfBNQhfBNAAB7QAACNBqBKQA+ArCeAtQB/AkAuAcIAEADQAVAOAPAQQAkAoAAA5QAAA+gkAuQgMAPgSAOIgGAFQhBAwhjAAQhwAAhZgyQg1gfgjgrIA9guQAaAgArAYQBGAmBZAAQBJAAAsgfQArgfAAg0QAAgpgqgaQgmgWhxggQiwgxhMg4QiCheAAiyQAAicB0hiQBAg3BVgZQBEgTBSAAQBNAABGATIARAFQATAGARAHQA6AXAzAmQA5AqAfAqIAKAPIi8C8IgTgTQgQgPgUgPIgLgIQhRg6hIAAQg5AAgmAaQgjAZAAAgQAAApArAZQAlAXByAgQA3APAtAQIAkAOQBJAdAqAeQAzAkAfAxQAxBOAABtQAACvh5BpQgjAegpAVg");
    var mask_2_graphics_60 = new cjs.Graphics().p("ACRITIAEgBQA9gVAtgmQA4guAYhAQAUg0AAg/QAAiNhqhLQgZgRgngQIgkgPQgzgThFgUQh+gkgvgcQhMgvAAhTQAAg/A2guQA8gzBcAAQBMABBCAfIAVAMQAUALAQAOIASARIBThTQgpgqg6gfIgCgBQgRgJgTgHQhMgehXAAQiWAAhfBOQhfBMAAB7QAACNBqBLQA+ArCeAsQB/AkAuAdIAEACQAVAOAPAQQAkApAAA4QAAA/gkAtQgMAQgSANIgGAFQhBAwhjAAQhwAAhZgyQg1gfgjgqIA9gvQAaAhArAXQBGAnBZAAQBJAAAsggQArgfAAg0QAAgpgqgZQgmgXhxggQiwgxhMg4QiCheAAixQAAidB0hiQBAg3BVgYQBEgUBSAAQBNAABGAUIARAEQATAGARAIQA6AXAzAlQA5AqAfAqIAKAPIi8C8IgTgTQgQgPgUgPIgLgIQhRg6hIAAQg5AAgmAaQgjAaAAAfQAAApArAZQAlAXByAgQA3AQAtAQIAkANQBJAdAqAfQAzAjAfAxQAxBOAABtQAACvh5BpQhAA3hWAag");
    var mask_2_graphics_61 = new cjs.Graphics().p("ABSIZQAkgGAfgKQA9gVAtgmQA4guAYhBQAUgzAAg/QAAiNhqhLQgZgRgngQIgkgPQgzgUhFgTQh+gkgvgcQhMgvAAhUQAAg+A2guQA8gzBcAAQBMAABCAgIAVALQAUAMAQAOIASAQIBThSQgpgrg6geIgCgBQgRgJgTgHQhMgehXAAQiWAAhfBNQhfBNAAB7QAACNBqBKQA+AsCeAsQB/AkAuAcIAEADQAVAOAPAQQAkAoAAA5QAAA+gkAuQgMAQgSANIgGAFQhBAwhjAAQhwAAhZgyQg1gfgjgrIA9guQAaAgArAYQBGAmBZAAQBJAAAsgfQArgfAAg0QAAgpgqgaQgmgWhxggQiwgxhMg4QiCheAAiyQAAicB0hiQBAg3BVgZQBEgTBSAAQBNAABGATIARAFQATAGARAHQA6AYAzAlQA5AqAfAqIAKAPIi8C8IgTgTQgQgPgUgPIgLgIQhRg6hIAAQg5AAgmAaQgjAZAAAgQAAApArAZQAlAXByAgQA3AQAtAQIAkANQBJAdAqAeQAzAkAfAxQAxBOAABtQAACvh5BpQhEA6hdAaQgjAKgoAFg");
    var mask_2_graphics_62 = new cjs.Graphics().p("AAcIdQBDgEA2gTQA9gUAtgmQA4guAYhBQAUg0AAg/QAAiNhqhKQgZgRgngRIgkgPQgzgThFgUQh+gjgvgdQhMgvAAhTQAAg/A2guQA8gyBcAAQBMAABCAfIAVAMQAUALAQAOIASARIBThTQgpgqg6geIgCgBQgRgJgTgHQhMgfhXAAQiWAAhfBOQhfBNAAB6QAACNBqBLQA+AsCeArQB/AkAuAdIAEADQAVANAPARQAkAoAAA5QAAA+gkAtQgMAQgSAOIgGAEQhBAxhjAAQhwAAhZgzQg1gfgjgqIA9guQAaAgArAXQBGAnBZAAQBJAAAsgfQArggAAg0QAAgpgqgZQgmgXhxggQiwgxhMg3QiCheAAiyQAAicB0hjQBAg3BVgYQBEgUBSAAQBNAABGAUIARAFQATAGARAHQA6AXAzAmQA5AqAfApIAKAPIi8C9IgTgTQgQgPgUgPIgLgJQhRg6hIAAQg5AAgmAbQgjAZAAAfQAAAqArAZQAlAWByAgQA3AQAtAQIAkAOQBJAdAqAeQAzAlAfAwQAxBOAABtQAACvh5BpQhEA6hdAaQg8AQhHAEg");
    var mask_2_graphics_63 = new cjs.Graphics().p("Ag8JpIAYhLIAVAAQBcAABIgYQA9gVAtgmQA4guAYhAQAUg0AAg/QAAiNhqhLQgZgRgngQIgkgPQgzgThFgUQh+gkgvgcQhMgvAAhTQAAg/A2guQA8gzBcAAQBMABBCAfIAVAMQAUALAQAOIASARIBThTQgpgqg6gfIgCgBQgRgJgTgHQhMgehXAAQiWAAhfBOQhfBMAAB7QAACNBqBLQA+AsCeArQB/AkAuAdIAEACQAVAOAPAQQAkApAAA4QAAA/gkAtQgMAQgSANIgGAFQhBAwhjAAQhwAAhZgyQg1gfgjgqIA9gvQAaAhArAXQBGAnBZAAQBJAAAsggQArgfAAg0QAAgpgqgZQgmgXhxggQiwgxhMg4QiCheAAixQAAidB0hiQBAg3BVgYQBEgUBSAAQBNAABGAUIARAEQATAGARAIQA6AXAzAlQA5AqAfAqIAKAPIi8C8IgTgTQgQgPgUgPIgLgIQhRg6hIAAQg5AAgmAaQgjAaAAAfQAAApArAZQAlAXByAgQA3AQAtAQIAkANQBJAdAqAfQAzAkAfAwQAxBOAABtQAACvh5BpQhEA6hdAaQhKAVhaAAQgXAAgWgBg");
    var mask_2_graphics_64 = new cjs.Graphics().p("AiLJeIAbhIQAvAIAyAAQBcAABIgYQA9gVAtgmQA4guAYhAQAUg0AAg/QAAiNhqhLQgZgRgngQIgkgPQgzgThFgUQh+gkgvgcQhMgvAAhTQAAg/A2guQA8gzBcAAQBMABBCAfIAVAMQAUALAQAOIASARIBThTQgpgqg6gfIgCgBQgRgJgTgHQhMgehXAAQiWAAhfBOQhfBMAAB7QAACNBqBLQA+AsCeArQB/AkAuAdIAEACQAVAOAPAQQAkApAAA4QAAA/gkAtQgMAQgSANIgGAFQhBAwhjAAQhwAAhZgyQg1gfgjgqIA9gvQAaAhArAXQBGAnBZAAQBJAAAsggQArgfAAg0QAAgpgqgZQgmgXhxggQiwgxhMg4QiCheAAixQAAidB0hiQBAg3BVgYQBEgUBSAAQBNAABGAUIARAEQATAGARAIQA6AXAzAlQA5AqAfAqIAKAPIi8C8IgTgTQgQgPgUgPIgLgIQhRg6hIAAQg5AAgmAaQgjAaAAAfQAAApArAZQAlAXByAgQA3AQAtAQIAkANQBJAdAqAfQAzAkAfAwQAxBOAABtQAACvh5BpQhEA6hdAaQhKAVhaAAQhBAAg7gMg");
    var mask_2_graphics_65 = new cjs.Graphics().p("AjVJKIAehFQBOAZBaAAQBcAABIgYQA9gVAtgmQA4guAYhAQAUg0AAg/QAAiNhqhLQgZgRgngQIgkgPQgzgThFgUQh+gkgvgcQhMgvAAhTQAAg/A2guQA8gzBcAAQBMABBCAfIAVAMQAUALAQAOIASARIBThTQgpgqg6gfIgCgBQgRgJgTgHQhMgehXAAQiWAAhfBOQhfBMAAB7QAACNBqBLQA+AsCeArQB/AkAuAdIAEACQAVAOAPAQQAkApAAA4QAAA/gkAtQgMAQgSANIgGAFQhBAwhjAAQhwAAhZgyQg1gfgjgqIA9gvQAaAhArAXQBGAnBZAAQBJAAAsggQArgfAAg0QAAgpgqgZQgmgXhxggQiwgxhMg4QiCheAAixQAAidB0hiQBAg3BVgYQBEgUBSAAQBNAABGAUIARAEQATAGARAIQA6AXAzAlQA5AqAfAqIAKAPIi8C8IgTgTQgQgPgUgPIgLgIQhRg6hIAAQg5AAgmAaQgjAaAAAfQAAApArAZQAlAXByAgQA3AQAtAQIAkANQBJAdAqAfQAzAkAfAwQAxBOAABtQAACvh5BpQhEA6hdAaQhKAVhaAAQhqAAhcggg");
    var mask_2_graphics_66 = new cjs.Graphics().p("AkcIsIAlhCQBoA0CAAAQBcAABIgYQA9gVAtgmQA4guAYhAQAUg0AAg/QAAiNhqhLQgZgRgngQIgkgPQgzgThFgUQh+gkgvgcQhMgvAAhTQAAg/A2guQA8gzBcAAQBMABBCAfIAVAMQAUALAQAOIASARIBThTQgpgqg6gfIgCgBQgRgJgTgHQhMgehXAAQiWAAhfBOQhfBMAAB7QAACNBqBLQA+AsCeArQB/AkAuAdIAEACQAVAOAPAQQAkApAAA4QAAA/gkAtQgMAQgSANIgGAFQhBAwhjAAQhwAAhZgyQg1gfgjgqIA9gvQAaAhArAXQBGAnBZAAQBJAAAsggQArgfAAg0QAAgpgqgZQgmgXhxggQiwgxhMg4QiCheAAixQAAidB0hiQBAg3BVgYQBEgUBSAAQBNAABGAUIARAEQATAGARAIQA6AXAzAlQA5AqAfAqIAKAPIi8C8IgTgTQgQgPgUgPIgLgIQhRg6hIAAQg5AAgmAaQgjAaAAAfQAAApArAZQAlAXByAgQA3AQAtAQIAkANQBJAdAqAfQAzAkAfAwQAxBOAABtQAACvh5BpQhEA6hdAaQhKAVhaAAQiUAAh5g+g");
    var mask_2_graphics_67 = new cjs.Graphics().p("AlTILIgFgDIgDgCIAtg8IAHAFQB6BPCeAAQBcAABIgYQA9gVAtgmQA4guAYhAQAUg0AAg/QAAiNhqhLQgZgRgngQIgkgPQgzgThFgUQh+gkgvgcQhMgvAAhTQAAg/A2guQA8gzBcAAQBMABBCAfIAVAMQAUALAQAOIASARIBThTQgpgqg6gfIgCgBQgRgJgTgHQhMgehXAAQiWAAhfBOQhfBMAAB7QAACNBqBLQA+AsCeArQB/AkAuAdIAEACQAVAOAPAQQAkApAAA4QAAA/gkAtQgMAQgSANIgGAFQhBAwhjAAQhwAAhZgyQg1gfgjgqIA9gvQAaAhArAXQBGAnBZAAQBJAAAsggQArgfAAg0QAAgpgqgZQgmgXhxggQiwgxhMg4QiCheAAixQAAidB0hiQBAg3BVgYQBEgUBSAAQBNAABGAUIARAEQATAGARAIQA6AXAzAlQA5AqAfAqIAKAPIi8C8IgTgTQgQgPgUgPIgLgIQhRg6hIAAQg5AAgmAaQgjAaAAAfQAAApArAZQAlAXByAgQA3AQAtAQIAkANQBJAdAqAfQAzAkAfAwQAxBOAABtQAACvh5BpQhEA6hdAaQhKAVhaAAQi2AAiOhfg");
    var mask_2_graphics_68 = new cjs.Graphics().p("AlTILIgFgDQgRgLgQgNIgYgUIA5gzIAAABQAXAUAaARQB6BPCeAAQBcAABIgYQA9gVAtgmQA4guAYhAQAUg0AAg/QAAiNhqhLQgZgRgngQIgkgPQgzgThFgUQh+gkgvgcQhMgvAAhTQAAg/A2guQA8gzBcAAQBMABBCAfIAVAMQAUALAQAOIASARIBThTQgpgqg6gfIgCgBQgRgJgTgHQhMgehXAAQiWAAhfBOQhfBMAAB7QAACNBqBLQA+AsCeArQB/AkAuAdIAEACQAVAOAPAQQAkApAAA4QAAA/gkAtQgMAQgSANIgGAFQhBAwhjAAQhwAAhZgyQg1gfgjgqIA9gvQAaAhArAXQBGAnBZAAQBJAAAsggQArgfAAg0QAAgpgqgZQgmgXhxggQiwgxhMg4QiCheAAixQAAidB0hiQBAg3BVgYQBEgUBSAAQBNAABGAUIARAEQATAGARAIQA6AXAzAlQA5AqAfAqIAKAPIi8C8IgTgTQgQgPgUgPIgLgIQhRg6hIAAQg5AAgmAaQgjAaAAAfQAAApArAZQAlAXByAgQA3AQAtAQIAkANQBJAdAqAfQAzAkAfAwQAxBOAABtQAACvh5BpQhEA6hdAaQhKAVhaAAQi2AAiOhfg");
    var mask_2_graphics_69 = new cjs.Graphics().p("AlGILQgugdgjglQgVgVgJgNIgGgJIA9guIAGAKQAJANAQAQQAfAeAnAaQB5BPCfAAQBcAABHgYQA9gVAtgmQA4guAZhAQATg0AAg/QAAiNhqhLQgYgRgogQIgkgPQgygThGgUQh+gkgugcQhNgvAAhTQAAg/A2guQA8gzBcAAQBMABBCAfIAVAMQAUALAQAOIASARIBThTQgpgqg6gfIgCgBQgRgJgTgHQhMgehXAAQiWAAheBOQhfBMgBB7QAACNBrBLQA+AsCdArQB/AkAuAdIAEACQAWAOAOAQQAlApgBA4QABA/glAtQgMAQgRANIgHAFQhBAwhiAAQhxAAhYgyQg2gfgjgqIA+gvQAZAhArAXQBGAnBaAAQBIAAAsggQAsgfAAg0QAAgpgrgZQgmgXhxggQivgxhNg4QiCheAAixQAAidB0hiQBAg3BVgYQBFgUBRAAQBOAABGAUIAQAEQATAGARAIQA6AXAzAlQA5AqAfAqIAKAPIi8C8IgTgTQgPgPgVgPIgLgIQhQg6hJAAQg5AAgmAaQgjAaAAAfQAAApArAZQAlAXByAgQA3AQAtAQIAkANQBKAdApAfQAzAkAfAwQAxBOABBtQAACvh6BpQhEA6hdAaQhKAVhZAAQi3AAiOhfg");
    var mask_2_graphics_119 = new cjs.Graphics().p("AlGILQgugdgjglQgVgVgJgNIgGgJIA9guIAGAKQAJANAQAQQAfAeAnAaQB5BPCfAAQBcAABHgYQA9gVAtgmQA4guAZhAQATg0AAg/QAAiNhqhLQgYgRgogQIgkgPQgygThGgUQh+gkgugcQhNgvAAhTQAAg/A2guQA8gzBcAAQBMABBCAfIAVAMQAUALAQAOIASARIBThTQgpgqg6gfIgCgBQgRgJgTgHQhMgehXAAQiWAAheBOQhfBMgBB7QAACNBrBLQA+AsCdArQB/AkAuAdIAEACQAWAOAOAQQAlApgBA4QABA/glAtQgMAQgRANIgHAFQhBAwhiAAQhxAAhYgyQg2gfgjgqIA+gvQAZAhArAXQBGAnBaAAQBIAAAsggQAsgfAAg0QAAgpgrgZQgmgXhxggQivgxhNg4QiCheAAixQAAidB0hiQBAg3BVgYQBFgUBRAAQBOAABGAUIAQAEQATAGARAIQA6AXAzAlQA5AqAfAqIAKAPIi8C8IgTgTQgPgPgVgPIgLgIQhQg6hJAAQg5AAgmAaQgjAaAAAfQAAApArAZQAlAXByAgQA3AQAtAQIAkANQBKAdApAfQAzAkAfAwQAxBOABBtQAACvh6BpQhEA6hdAaQhKAVhZAAQi3AAiOhfg");
    var mask_2_graphics_120 = new cjs.Graphics().p("AlGILQgugdgjglQgVgVgJgNIgGgJIA9guIAGAKQAJANAQAQQAfAeAnAaQB5BPCfAAQBcAABHgYQA9gVAtgmQA4guAZhAQATg0AAg/QAAiNhqhLQgYgRgogQIgkgPQgygThGgUQh+gkgugcQhNgvAAhTQAAg/A2guQA8gzBcAAQBMABBCAfIAVAMQAUALAQAOIASARIBThTQgpgqg6gfIgCgBQgRgJgTgHQhMgehXAAQiWAAheBOQhfBMgBB7QAACNBrBLQA+AsCdArQB/AkAuAdIAEACQAWAOAOAQQAlApgBA4QABA/glAtQgMAQgRANIgHAFQhBAwhiAAQhxAAhYgyQg2gfgjgqIA+gvQAZAhArAXQBGAnBaAAQBIAAAsggQAsgfAAg0QAAgpgrgZQgmgXhxggQivgxhNg4QiCheAAixQAAidB0hiQBAg3BVgYQBFgUBRAAQBOAABGAUIAQAEQATAGARAIQA6AXAzAlQA5AqAfAqIAKAPIi8C8IgTgTQgPgPgVgPIgLgIQhQg6hJAAQg5AAgmAaQgjAaAAAfQAAApArAZQAlAXByAgQA3AQAtAQIAkANQBKAdApAfQAzAkAfAwQAxBOABBtQAACvh6BpQhEA6hdAaQhKAVhZAAQi3AAiOhfg");
    var mask_2_graphics_121 = new cjs.Graphics().p("AlGILQgugdgjglQgVgVgJgNIgGgJIA9guIAGAKQAJANAQAQQAfAeAnAaQB5BPCfAAQBcAABHgYQA9gVAtgmQA4guAZhAQATg0AAg/QAAiNhqhLQgYgRgogQIgkgPQgygThGgUQh+gkgugcQhNgvAAhTQAAg/A2guQA8gzBcAAQBMABBCAfIAVAMQAUALAQAOIASARIBThTQgpgqg6gfIgCgBQgRgJgTgHQhMgehXAAQiWAAheBOQhfBMgBB7QAACNBrBLQA+AsCdArQB/AkAuAdIAEACQAWAOAOAQQAlApgBA4QABA/glAtQgMAQgRANIgHAFQhBAwhiAAQhxAAhYgyQg2gfgjgqIA+gvQAZAhArAXQBGAnBaAAQBIAAAsggQAsgfAAg0QAAgpgrgZQgmgXhxggQivgxhNg4QiCheAAixQAAidB0hiQBAg3BVgYQBFgUBRAAQBOAABGAUIAQAEQATAGARAIQA6AXAzAlQA5AqAfAqIAKAPIi8C8IgTgTQgPgPgVgPIgLgIQhQg6hJAAQg5AAgmAaQgjAaAAAfQAAApArAZQAlAXByAgQA3AQAtAQIAkANQBKAdApAfQAzAkAfAwQAxBOABBtQAACvh6BpQhEA6hdAaQhKAVhZAAQi3AAiOhfg");
    var mask_2_graphics_122 = new cjs.Graphics().p("AlGILQgugdgjglQgVgVgJgNIgGgJIA9guIAGAKQAJANAQAQQAfAeAnAaQB5BPCfAAQBcAABHgYQA9gVAtgmQA4guAZhAQATg0AAg/QAAiNhqhLQgYgRgogQIgkgPQgygThGgUQh+gkgugcQhNgvAAhTQAAg/A2guQA8gzBcAAQBMABBCAfIAVAMQAUALAQAOIASARIBThTQgpgqg6gfIgCgBQgRgJgTgHQhMgehXAAQiWAAheBOQhfBMgBB7QAACNBrBLQA+AsCdArQB/AkAuAdIAEACQAWAOAOAQQAlApgBA4QABA/glAtQgMAQgRANIgHAFQhBAwhiAAQhxAAhYgyQg2gfgjgqIA+gvQAZAhArAXQBGAnBaAAQBIAAAsggQAsgfAAg0QAAgpgrgZQgmgXhxggQivgxhNg4QiCheAAixQAAidB0hiQBAg3BVgYQBFgUBRAAQBOAABGAUIAQAEQATAGARAIQA6AXAzAlQA5AqAfAqIAKAPIi8C8IgTgTQgPgPgVgPIgLgIQhQg6hJAAQg5AAgmAaQgjAaAAAfQAAApArAZQAlAXByAgQA3AQAtAQIAkANQBKAdApAfQAzAkAfAwQAxBOABBtQAACvh6BpQhEA6hdAaQhKAVhZAAQi3AAiOhfg");
    var mask_2_graphics_123 = new cjs.Graphics().p("AlGILQgugdgjglQgVgVgJgNIgGgJIA9guIAGAKQAJANAQAQQAfAeAnAaQB5BPCfAAQBcAABHgYQA9gVAtgmQA4guAZhAQATg0AAg/QAAiNhqhLQgYgRgogQIgkgPQgygThGgUQh+gkgugcQhNgvAAhTQAAg/A2guQA8gzBcAAQBMABBCAfIAVAMQAUALAQAOIASARIBThTQgpgqg6gfIgCgBQgRgJgTgHQhMgehXAAQiWAAheBOQhfBMgBB7QAACNBrBLQA+AsCdArQB/AkAuAdIAEACQAWAOAOAQQAlApgBA4QABA/glAtQgMAQgRANIgHAFQhBAwhiAAQhxAAhYgyQg2gfgjgqIA+gvQAZAhArAXQBGAnBaAAQBIAAAsggQAsgfAAg0QAAgpgrgZQgmgXhxggQivgxhNg4QiCheAAixQAAidB0hiQBAg3BVgYQBFgUBRAAQBOAABGAUIAQAEQATAGARAIQA6AXAzAlQA5AqAfAqIAKAPIi8C8IgTgTQgPgPgVgPIgLgIQhQg6hJAAQg5AAgmAaQgjAaAAAfQAAApArAZQAlAXByAgQA3AQAtAQIAkANQBKAdApAfQAzAkAfAwQAxBOABBtQAACvh6BpQhEA6hdAaQhKAVhZAAQi3AAiOhfg");
    var mask_2_graphics_124 = new cjs.Graphics().p("AlGILQgugdgjglQgVgVgJgNIgGgJIA9guIAGAKQAJANAQAQQAfAeAnAaQB5BPCfAAQBcAABHgYQA9gVAtgmQA4guAZhAQATg0AAg/QAAiNhqhLQgYgRgogQIgkgPQgygThGgUQh+gkgugcQhNgvAAhTQAAg/A2guQA8gzBcAAQBMABBCAfIAVAMQAUALAQAOIASARIBThTQgpgqg6gfIgCgBQgRgJgTgHQhMgehXAAQiWAAheBOQhfBMgBB7QAACNBrBLQA+AsCdArQB/AkAuAdIAEACQAWAOAOAQQAlApgBA4QABA/glAtQgMAQgRANIgHAFQhBAwhiAAQhxAAhYgyQg2gfgjgqIA+gvQAZAhArAXQBGAnBaAAQBIAAAsggQAsgfAAg0QAAgpgrgZQgmgXhxggQivgxhNg4QiCheAAixQAAidB0hiQBAg3BVgYQBFgUBRAAQBOAABGAUIAQAEQATAGARAIQA6AXAzAlQA5AqAfAqIAKAPIi8C8IgTgTQgPgPgVgPIgLgIQhQg6hJAAQg5AAgmAaQgjAaAAAfQAAApArAZQAlAXByAgQA3AQAtAQIAkANQBKAdApAfQAzAkAfAwQAxBOABBtQAACvh6BpQhEA6hdAaQhKAVhZAAQi3AAiOhfg");
    var mask_2_graphics_125 = new cjs.Graphics().p("AlGILQgugdgjglQgVgVgJgNIgGgJIA9guIAGAKQAJANAQAQQAfAeAnAaQB5BPCfAAQBcAABHgYQA9gVAtgmQA4guAZhAQATg0AAg/QAAiNhqhLQgYgRgogQIgkgPQgygThGgUQh+gkgugcQhNgvAAhTQAAg/A2guQA8gzBcAAQBMABBCAfIAVAMQAUALAQAOIASARIBThTQgpgqg6gfIgCgBQgRgJgTgHQhMgehXAAQiWAAheBOQhfBMgBB7QAACNBrBLQA+AsCdArQB/AkAuAdIAEACQAWAOAOAQQAlApgBA4QABA/glAtQgMAQgRANIgHAFQhBAwhiAAQhxAAhYgyQg2gfgjgqIA+gvQAZAhArAXQBGAnBaAAQBIAAAsggQAsgfAAg0QAAgpgrgZQgmgXhxggQivgxhNg4QiCheAAixQAAidB0hiQBAg3BVgYQBFgUBRAAQBOAABGAUIAQAEQATAGARAIQA6AXAzAlQA5AqAfAqIAKAPIi8C8IgTgTQgPgPgVgPIgLgIQhQg6hJAAQg5AAgmAaQgjAaAAAfQAAApArAZQAlAXByAgQA3AQAtAQIAkANQBKAdApAfQAzAkAfAwQAxBOABBtQAACvh6BpQhEA6hdAaQhKAVhZAAQi3AAiOhfg");
    var mask_2_graphics_126 = new cjs.Graphics().p("AlGILQgugdgjglQgVgVgJgNIgGgJIA9guIAGAKQAJANAQAQQAfAeAnAaQB5BPCfAAQBcAABHgYQA9gVAtgmQA4guAZhAQATg0AAg/QAAiNhqhLQgYgRgogQIgkgPQgygThGgUQh+gkgugcQhNgvAAhTQAAg/A2guQA8gzBcAAQBMABBCAfIAVAMQAUALAQAOIASARIBThTQgpgqg6gfIgCgBQgRgJgTgHQhMgehXAAQiWAAheBOQhfBMgBB7QAACNBrBLQA+AsCdArQB/AkAuAdIAEACQAWAOAOAQQAlApgBA4QABA/glAtQgMAQgRANIgHAFQhBAwhiAAQhxAAhYgyQg2gfgjgqIA+gvQAZAhArAXQBGAnBaAAQBIAAAsggQAsgfAAg0QAAgpgrgZQgmgXhxggQivgxhNg4QiCheAAixQAAidB0hiQBAg3BVgYQBFgUBRAAQBOAABGAUIAQAEQATAGARAIQA6AXAzAlQA5AqAfAqIAKAPIi8C8IgTgTQgPgPgVgPIgLgIQhQg6hJAAQg5AAgmAaQgjAaAAAfQAAApArAZQAlAXByAgQA3AQAtAQIAkANQBKAdApAfQAzAkAfAwQAxBOABBtQAACvh6BpQhEA6hdAaQhKAVhZAAQi3AAiOhfg");
    var mask_2_graphics_127 = new cjs.Graphics().p("AlGILQgugdgjglQgVgVgJgNIgGgJIA9guIAGAKQAJANAQAQQAfAeAnAaQB5BPCfAAQBcAABHgYQA9gVAtgmQA4guAZhAQATg0AAg/QAAiNhqhLQgYgRgogQIgkgPQgygThGgUQh+gkgugcQhNgvAAhTQAAg/A2guQA8gzBcAAQBMABBCAfIAVAMQAUALAQAOIASARIBThTQgpgqg6gfIgCgBQgRgJgTgHQhMgehXAAQiWAAheBOQhfBMgBB7QAACNBrBLQA+AsCdArQB/AkAuAdIAEACQAWAOAOAQQAlApgBA4QABA/glAtQgMAQgRANIgHAFQhBAwhiAAQhxAAhYgyQg2gfgjgqIA+gvQAZAhArAXQBGAnBaAAQBIAAAsggQAsgfAAg0QAAgpgrgZQgmgXhxggQivgxhNg4QiCheAAixQAAidB0hiQBAg3BVgYQBFgUBRAAQBOAABGAUIAQAEQATAGARAIQA6AXAzAlQA5AqAfAqIAKAPIi8C8IgTgTQgPgPgVgPIgLgIQhQg6hJAAQg5AAgmAaQgjAaAAAfQAAApArAZQAlAXByAgQA3AQAtAQIAkANQBKAdApAfQAzAkAfAwQAxBOABBtQAACvh6BpQhEA6hdAaQhKAVhZAAQi3AAiOhfg");
    var mask_2_graphics_128 = new cjs.Graphics().p("AlGILQgugdgjglQgVgVgJgNIgGgJIA9guIAGAKQAJANAQAQQAfAeAnAaQB5BPCfAAQBcAABHgYQA9gVAtgmQA4guAZhAQATg0AAg/QAAiNhqhLQgYgRgogQIgkgPQgygThGgUQh+gkgugcQhNgvAAhTQAAg/A2guQA8gzBcAAQBMABBCAfIAVAMQAUALAQAOIASARIBThTQgpgqg6gfIgCgBQgRgJgTgHQhMgehXAAQiWAAheBOQhfBMgBB7QAACNBrBLQA+AsCdArQB/AkAuAdIAEACQAWAOAOAQQAlApgBA4QABA/glAtQgMAQgRANIgHAFQhBAwhiAAQhxAAhYgyQg2gfgjgqIA+gvQAZAhArAXQBGAnBaAAQBIAAAsggQAsgfAAg0QAAgpgrgZQgmgXhxggQivgxhNg4QiCheAAixQAAidB0hiQBAg3BVgYQBFgUBRAAQBOAABGAUIAQAEQATAGARAIQA6AXAzAlQA5AqAfAqIAKAPIi8C8IgTgTQgPgPgVgPIgLgIQhQg6hJAAQg5AAgmAaQgjAaAAAfQAAApArAZQAlAXByAgQA3AQAtAQIAkANQBKAdApAfQAzAkAfAwQAxBOABBtQAACvh6BpQhEA6hdAaQhKAVhZAAQi3AAiOhfg");
    var mask_2_graphics_129 = new cjs.Graphics().p("AlGILQgugdgjglQgVgVgJgNIgGgJIA9guIAGAKQAJANAQAQQAfAeAnAaQB5BPCfAAQBcAABHgYQA9gVAtgmQA4guAZhAQATg0AAg/QAAiNhqhLQgYgRgogQIgkgPQgygThGgUQh+gkgugcQhNgvAAhTQAAg/A2guQA8gzBcAAQBMABBCAfIAVAMQAUALAQAOIASARIBThTQgpgqg6gfIgCgBQgRgJgTgHQhMgehXAAQiWAAheBOQhfBMgBB7QAACNBrBLQA+AsCdArQB/AkAuAdIAEACQAWAOAOAQQAlApgBA4QABA/glAtQgMAQgRANIgHAFQhBAwhiAAQhxAAhYgyQg2gfgjgqIA+gvQAZAhArAXQBGAnBaAAQBIAAAsggQAsgfAAg0QAAgpgrgZQgmgXhxggQivgxhNg4QiCheAAixQAAidB0hiQBAg3BVgYQBFgUBRAAQBOAABGAUIAQAEQATAGARAIQA6AXAzAlQA5AqAfAqIAKAPIi8C8IgTgTQgPgPgVgPIgLgIQhQg6hJAAQg5AAgmAaQgjAaAAAfQAAApArAZQAlAXByAgQA3AQAtAQIAkANQBKAdApAfQAzAkAfAwQAxBOABBtQAACvh6BpQhEA6hdAaQhKAVhZAAQi3AAiOhfg");
    var mask_2_graphics_130 = new cjs.Graphics().p("AlGILQgugdgjglQgVgVgJgNIgGgJIA9guIAGAKQAJANAQAQQAfAeAnAaQB5BPCfAAQBcAABHgYQA9gVAtgmQA4guAZhAQATg0AAg/QAAiNhqhLQgYgRgogQIgkgPQgygThGgUQh+gkgugcQhNgvAAhTQAAg/A2guQA8gzBcAAQBMABBCAfIAVAMQAUALAQAOIASARIBThTQgpgqg6gfIgCgBQgRgJgTgHQhMgehXAAQiWAAheBOQhfBMgBB7QAACNBrBLQA+AsCdArQB/AkAuAdIAEACQAWAOAOAQQAlApgBA4QABA/glAtQgMAQgRANIgHAFQhBAwhiAAQhxAAhYgyQg2gfgjgqIA+gvQAZAhArAXQBGAnBaAAQBIAAAsggQAsgfAAg0QAAgpgrgZQgmgXhxggQivgxhNg4QiCheAAixQAAidB0hiQBAg3BVgYQBFgUBRAAQBOAABGAUIAQAEQATAGARAIQA6AXAzAlQA5AqAfAqIAKAPIi8C8IgTgTQgPgPgVgPIgLgIQhQg6hJAAQg5AAgmAaQgjAaAAAfQAAApArAZQAlAXByAgQA3AQAtAQIAkANQBKAdApAfQAzAkAfAwQAxBOABBtQAACvh6BpQhEA6hdAaQhKAVhZAAQi3AAiOhfg");
    var mask_2_graphics_131 = new cjs.Graphics().p("AlGILQgugdgjglQgVgVgJgNIgGgJIA9guIAGAKQAJANAQAQQAfAeAnAaQB5BPCfAAQBcAABHgYQA9gVAtgmQA4guAZhAQATg0AAg/QAAiNhqhLQgYgRgogQIgkgPQgygThGgUQh+gkgugcQhNgvAAhTQAAg/A2guQA8gzBcAAQBMABBCAfIAVAMQAUALAQAOIASARIBThTQgpgqg6gfIgCgBQgRgJgTgHQhMgehXAAQiWAAheBOQhfBMgBB7QAACNBrBLQA+AsCdArQB/AkAuAdIAEACQAWAOAOAQQAlApgBA4QABA/glAtQgMAQgRANIgHAFQhBAwhiAAQhxAAhYgyQg2gfgjgqIA+gvQAZAhArAXQBGAnBaAAQBIAAAsggQAsgfAAg0QAAgpgrgZQgmgXhxggQivgxhNg4QiCheAAixQAAidB0hiQBAg3BVgYQBFgUBRAAQBOAABGAUIAQAEQATAGARAIQA6AXAzAlQA5AqAfAqIAKAPIi8C8IgTgTQgPgPgVgPIgLgIQhQg6hJAAQg5AAgmAaQgjAaAAAfQAAApArAZQAlAXByAgQA3AQAtAQIAkANQBKAdApAfQAzAkAfAwQAxBOABBtQAACvh6BpQhEA6hdAaQhKAVhZAAQi3AAiOhfg");
    var mask_2_graphics_132 = new cjs.Graphics().p("AlGILQgugdgjglQgVgVgJgNIgGgJIA9guIAGAKQAJANAQAQQAfAeAnAaQB5BPCfAAQBcAABHgYQA9gVAtgmQA4guAZhAQATg0AAg/QAAiNhqhLQgYgRgogQIgkgPQgygThGgUQh+gkgugcQhNgvAAhTQAAg/A2guQA8gzBcAAQBMABBCAfIAVAMQAUALAQAOIASARIBThTQgpgqg6gfIgCgBQgRgJgTgHQhMgehXAAQiWAAheBOQhfBMgBB7QAACNBrBLQA+AsCdArQB/AkAuAdIAEACQAWAOAOAQQAlApgBA4QABA/glAtQgMAQgRANIgHAFQhBAwhiAAQhxAAhYgyQg2gfgjgqIA+gvQAZAhArAXQBGAnBaAAQBIAAAsggQAsgfAAg0QAAgpgrgZQgmgXhxggQivgxhNg4QiCheAAixQAAidB0hiQBAg3BVgYQBFgUBRAAQBOAABGAUIAQAEQATAGARAIQA6AXAzAlQA5AqAfAqIAKAPIi8C8IgTgTQgPgPgVgPIgLgIQhQg6hJAAQg5AAgmAaQgjAaAAAfQAAApArAZQAlAXByAgQA3AQAtAQIAkANQBKAdApAfQAzAkAfAwQAxBOABBtQAACvh6BpQhEA6hdAaQhKAVhZAAQi3AAiOhfg");
    var mask_2_graphics_133 = new cjs.Graphics().p("AlGILQgugdgjglQgVgVgJgNIgGgJIA9guIAGAKQAJANAQAQQAfAeAnAaQB5BPCfAAQBcAABHgYQA9gVAtgmQA4guAZhAQATg0AAg/QAAiNhqhLQgYgRgogQIgkgPQgygThGgUQh+gkgugcQhNgvAAhTQAAg/A2guQA8gzBcAAQBMABBCAfIAVAMQAUALAQAOIASARIBThTQgpgqg6gfIgCgBQgRgJgTgHQhMgehXAAQiWAAheBOQhfBMgBB7QAACNBrBLQA+AsCdArQB/AkAuAdIAEACQAWAOAOAQQAlApgBA4QABA/glAtQgMAQgRANIgHAFQhBAwhiAAQhxAAhYgyQg2gfgjgqIA+gvQAZAhArAXQBGAnBaAAQBIAAAsggQAsgfAAg0QAAgpgrgZQgmgXhxggQivgxhNg4QiCheAAixQAAidB0hiQBAg3BVgYQBFgUBRAAQBOAABGAUIAQAEQATAGARAIQA6AXAzAlQA5AqAfAqIAKAPIi8C8IgTgTQgPgPgVgPIgLgIQhQg6hJAAQg5AAgmAaQgjAaAAAfQAAApArAZQAlAXByAgQA3AQAtAQIAkANQBKAdApAfQAzAkAfAwQAxBOABBtQAACvh6BpQhEA6hdAaQhKAVhZAAQi3AAiOhfg");
    var mask_2_graphics_134 = new cjs.Graphics().p("AlGILQgugdgjglQgVgVgJgNIgGgJIA9guIAGAKQAJANAQAQQAfAeAnAaQB5BPCfAAQBcAABHgYQA9gVAtgmQA4guAZhAQATg0AAg/QAAiNhqhLQgYgRgogQIgkgPQgygThGgUQh+gkgugcQhNgvAAhTQAAg/A2guQA8gzBcAAQBMABBCAfIAVAMQAUALAQAOIASARIBThTQgpgqg6gfIgCgBQgRgJgTgHQhMgehXAAQiWAAheBOQhfBMgBB7QAACNBrBLQA+AsCdArQB/AkAuAdIAEACQAWAOAOAQQAlApgBA4QABA/glAtQgMAQgRANIgHAFQhBAwhiAAQhxAAhYgyQg2gfgjgqIA+gvQAZAhArAXQBGAnBaAAQBIAAAsggQAsgfAAg0QAAgpgrgZQgmgXhxggQivgxhNg4QiCheAAixQAAidB0hiQBAg3BVgYQBFgUBRAAQBOAABGAUIAQAEQATAGARAIQA6AXAzAlQA5AqAfAqIAKAPIi8C8IgTgTQgPgPgVgPIgLgIQhQg6hJAAQg5AAgmAaQgjAaAAAfQAAApArAZQAlAXByAgQA3AQAtAQIAkANQBKAdApAfQAzAkAfAwQAxBOABBtQAACvh6BpQhEA6hdAaQhKAVhZAAQi3AAiOhfg");
  
    this.timeline.addTween(cjs.Tween.get(mask_2).to({graphics:null,x:0,y:0}).wait(1).to({graphics:mask_2_graphics_1,x:683.5,y:92.6}).wait(1).to({graphics:mask_2_graphics_2,x:686,y:94}).wait(1).to({graphics:mask_2_graphics_3,x:687.9,y:95.2}).wait(1).to({graphics:mask_2_graphics_4,x:691.1,y:95.7}).wait(1).to({graphics:mask_2_graphics_5,x:693.7,y:95.8}).wait(1).to({graphics:mask_2_graphics_6,x:697,y:95.8}).wait(1).to({graphics:mask_2_graphics_7,x:699.3,y:95.8}).wait(1).to({graphics:mask_2_graphics_8,x:702.2,y:95.8}).wait(1).to({graphics:mask_2_graphics_9,x:704.1,y:95.8}).wait(1).to({graphics:mask_2_graphics_10,x:704.7,y:94.5}).wait(1).to({graphics:mask_2_graphics_11,x:704.7,y:90.9}).wait(1).to({graphics:mask_2_graphics_12,x:704.7,y:88.4}).wait(1).to({graphics:mask_2_graphics_13,x:704.7,y:87}).wait(1).to({graphics:mask_2_graphics_14,x:704.7,y:85.7}).wait(1).to({graphics:mask_2_graphics_15,x:704.7,y:84.7}).wait(1).to({graphics:mask_2_graphics_16,x:704.7,y:84}).wait(1).to({graphics:mask_2_graphics_17,x:704.7,y:82.9}).wait(1).to({graphics:mask_2_graphics_18,x:704.7,y:81.7}).wait(1).to({graphics:mask_2_graphics_19,x:702.3,y:79.5}).wait(1).to({graphics:mask_2_graphics_20,x:699.9,y:76.7}).wait(1).to({graphics:mask_2_graphics_21,x:698.6,y:73.1}).wait(1).to({graphics:mask_2_graphics_22,x:698.4,y:69.5}).wait(1).to({graphics:mask_2_graphics_23,x:698.4,y:64.7}).wait(1).to({graphics:mask_2_graphics_24,x:698.4,y:60.5}).wait(1).to({graphics:mask_2_graphics_25,x:698.4,y:57.5}).wait(1).to({graphics:mask_2_graphics_26,x:698.4,y:55.1}).wait(1).to({graphics:mask_2_graphics_27,x:698.4,y:53.6}).wait(1).to({graphics:mask_2_graphics_28,x:698.4,y:52.6}).wait(1).to({graphics:mask_2_graphics_29,x:698.4,y:52.1}).wait(1).to({graphics:mask_2_graphics_30,x:698.4,y:52.1}).wait(1).to({graphics:mask_2_graphics_31,x:698.4,y:52.1}).wait(1).to({graphics:mask_2_graphics_32,x:701.1,y:52.1}).wait(1).to({graphics:mask_2_graphics_33,x:701.1,y:52.1}).wait(1).to({graphics:mask_2_graphics_34,x:704.3,y:52.1}).wait(1).to({graphics:mask_2_graphics_35,x:706.9,y:52.1}).wait(1).to({graphics:mask_2_graphics_36,x:706.9,y:52.1}).wait(1).to({graphics:mask_2_graphics_37,x:706.9,y:52.1}).wait(1).to({graphics:mask_2_graphics_38,x:706.9,y:52.1}).wait(1).to({graphics:mask_2_graphics_39,x:706.9,y:52.1}).wait(1).to({graphics:mask_2_graphics_40,x:706.9,y:52.1}).wait(1).to({graphics:mask_2_graphics_41,x:706.9,y:52.1}).wait(1).to({graphics:mask_2_graphics_42,x:706.9,y:52.1}).wait(1).to({graphics:mask_2_graphics_43,x:706.9,y:52.1}).wait(1).to({graphics:mask_2_graphics_44,x:706.9,y:52.1}).wait(1).to({graphics:mask_2_graphics_45,x:706.9,y:52.1}).wait(1).to({graphics:mask_2_graphics_46,x:706.9,y:52.1}).wait(1).to({graphics:mask_2_graphics_47,x:706.9,y:52.1}).wait(1).to({graphics:mask_2_graphics_48,x:706.9,y:52.1}).wait(1).to({graphics:mask_2_graphics_49,x:706.9,y:52.1}).wait(1).to({graphics:mask_2_graphics_50,x:706.9,y:52.1}).wait(1).to({graphics:mask_2_graphics_51,x:706.9,y:52.1}).wait(1).to({graphics:mask_2_graphics_52,x:706.9,y:52.1}).wait(1).to({graphics:mask_2_graphics_53,x:707.9,y:52.1}).wait(1).to({graphics:mask_2_graphics_54,x:708.8,y:52.1}).wait(1).to({graphics:mask_2_graphics_55,x:708.9,y:52.1}).wait(1).to({graphics:mask_2_graphics_56,x:708.9,y:52.1}).wait(1).to({graphics:mask_2_graphics_57,x:708.9,y:53.6}).wait(1).to({graphics:mask_2_graphics_58,x:708.9,y:56.8}).wait(1).to({graphics:mask_2_graphics_59,x:708.9,y:59.1}).wait(1).to({graphics:mask_2_graphics_60,x:708.9,y:60.6}).wait(1).to({graphics:mask_2_graphics_61,x:708.9,y:61.5}).wait(1).to({graphics:mask_2_graphics_62,x:708.9,y:61.8}).wait(1).to({graphics:mask_2_graphics_63,x:708.9,y:61.8}).wait(1).to({graphics:mask_2_graphics_64,x:708.9,y:61.8}).wait(1).to({graphics:mask_2_graphics_65,x:708.9,y:61.8}).wait(1).to({graphics:mask_2_graphics_66,x:708.9,y:61.8}).wait(1).to({graphics:mask_2_graphics_67,x:708.9,y:61.8}).wait(1).to({graphics:mask_2_graphics_68,x:708.9,y:61.8}).wait(1).to({graphics:mask_2_graphics_69,x:707.6,y:61.8}).wait(50).to({graphics:mask_2_graphics_119,x:707.6,y:61.8}).wait(1).to({graphics:mask_2_graphics_120,x:707.6,y:61.8}).wait(1).to({graphics:mask_2_graphics_121,x:707.6,y:61.8}).wait(1).to({graphics:mask_2_graphics_122,x:707.6,y:61.8}).wait(1).to({graphics:mask_2_graphics_123,x:707.6,y:61.8}).wait(1).to({graphics:mask_2_graphics_124,x:707.6,y:61.8}).wait(1).to({graphics:mask_2_graphics_125,x:707.6,y:61.8}).wait(1).to({graphics:mask_2_graphics_126,x:707.6,y:61.8}).wait(1).to({graphics:mask_2_graphics_127,x:707.6,y:61.8}).wait(1).to({graphics:mask_2_graphics_128,x:707.6,y:61.8}).wait(1).to({graphics:mask_2_graphics_129,x:707.6,y:61.8}).wait(1).to({graphics:mask_2_graphics_130,x:707.6,y:61.8}).wait(1).to({graphics:mask_2_graphics_131,x:707.6,y:61.8}).wait(1).to({graphics:mask_2_graphics_132,x:707.6,y:61.8}).wait(1).to({graphics:mask_2_graphics_133,x:707.6,y:61.8}).wait(1).to({graphics:mask_2_graphics_134,x:707.6,y:61.8}).wait(1));
  
    // S
    this.shape_2 = new cjs.Shape();
    this.shape_2.graphics.f("#fff").s().p("AlGILQgugdgjglQgVgVgJgNIgGgJIA9guIAGAKQAJANAQAQQAfAeAnAaQB5BPCfAAQBcAABHgYQA9gVAtgmQA4guAZhAQATg0AAg/QAAiNhqhLQgYgRgogQIgkgPQgygThGgUQh+gkgugcQhNgvAAhTQAAg/A2guQA8gzBcAAQBMABBCAfIAVAMQAUALAQAOIASARIBThTQgpgqg6gfIgCgBQgRgJgTgHQhMgehXAAQiWAAheBOQhfBMgBB7QAACNBrBLQA+AsCdArQB/AkAuAdIAEACQAWAOAOAQQAlApgBA4QABA/glAtQgMAQgRANIgHAFQhBAwhiAAQhxAAhYgyQg2gfgjgqIA+gvQAZAhArAXQBGAnBaAAQBIAAAsggQAsgfAAg0QAAgpgrgZQgmgXhxggQivgxhNg4QiCheAAixQAAidB0hiQBAg3BVgYQBFgUBRAAQBOAABGAUIAQAEQATAGARAIQA6AXAzAlQA5AqAfAqIAKAPIi8C8IgTgTQgPgPgVgPIgLgIQhQg6hJAAQg5AAgmAaQgjAaAAAfQAAApArAZQAlAXByAgQA3AQAtAQIAkANQBKAdApAfQAzAkAfAwQAxBOABBtQAACvh6BpQhEA6hdAaQhKAVhZAAQi3AAiOhfg");
    this.shape_2.setTransform(707.6,61.8);
  
    this.instance_4 = new lib.Tween15("synched",0);
    this.instance_4.parent = this;
    this.instance_4.setTransform(707.6,61.8);
    this.instance_4._off = true;
  
    this.instance_5 = new lib.Tween16("synched",0);
    this.instance_5.parent = this;
    this.instance_5.setTransform(707.6,61.8);
    this.instance_5.alpha = 0;
  
    var maskedShapeInstanceList = [this.shape_2,this.instance_4,this.instance_5];
  
    for(var shapedInstanceItr = 0; shapedInstanceItr < maskedShapeInstanceList.length; shapedInstanceItr++) {
      maskedShapeInstanceList[shapedInstanceItr].mask = mask_2;
    }
  
    this.timeline.addTween(cjs.Tween.get({}).to({state:[]}).to({state:[{t:this.shape_2}]},1).to({state:[{t:this.instance_4}]},118).to({state:[{t:this.instance_5}]},15).wait(1));
    this.timeline.addTween(cjs.Tween.get(this.instance_4).wait(119).to({_off:false},0).to({_off:true,alpha:0},15).wait(1));
  
    // R mask (mask)
    var mask_3 = new cjs.Shape();
    mask_3._off = true;
    var mask_3_graphics_1 = new cjs.Graphics().p("AglAmIAAhLIBLAAIAABLg");
    var mask_3_graphics_2 = new cjs.Graphics().p("AhQAmIAAhLICgAAIAABLg");
    var mask_3_graphics_3 = new cjs.Graphics().p("Ah9AmIAAhLID7AAIAABLg");
    var mask_3_graphics_4 = new cjs.Graphics().p("AioAmIAAhLIFRAAIAABLg");
    var mask_3_graphics_5 = new cjs.Graphics().p("AjUAmIAAhLIGXAAIASAAIgiBLg");
    var mask_3_graphics_6 = new cjs.Graphics().p("ACPAgImWAAIAAhMIGWAAQBEAAA1AmIg9AzQgcgNggAAg");
    var mask_3_graphics_7 = new cjs.Graphics().p("ADOAwQgpglg2AAImWAAIAAhLIGWAAQBVAABAA7QAWAUAOAXIhKAbQgHgJgJgIg");
    var mask_3_graphics_8 = new cjs.Graphics().p("ADBAVQgqgkg1AAImXAAIAAhNIGXAAQBVAAA/A8QA4AyAIBEIhMAHQgGgpgjgfg");
    var mask_3_graphics_9 = new cjs.Graphics().p("ADnBkQADgNAAgNQAAg0gqgkQgpglg2AAImWAAIAAhNIGWAAQBVAABAA8QBAA8AABSQAAAdgIAag");
    var mask_3_graphics_10 = new cjs.Graphics().p("ADHB5QAjgjAAgvQAAgygqgmQgpglg2AAImWAAIAAhMIGWAAQBVgBBAA8QBAA8AABSQAABBgoAzIgHAIg");
    var mask_3_graphics_11 = new cjs.Graphics().p("ADBBkQApglAAgzQAAgzgqglQgpglg2AAImWAAIAAhNIGWAAQBVAABAA8QBAA8AABSQAABThAA7QgZAXgcAOg");
    var mask_3_graphics_12 = new cjs.Graphics().p("AB7B8QAmgHAfgaQAqgmAAgzQAAgzgqglQgpglg2AAImWAAIAAhNIGWAAQBVAABAA8QBAA8AABSQAABThAA7Qg2AxhFAIg");
    var mask_3_graphics_13 = new cjs.Graphics().p("AA2DKIAAhMIArAAQA2AAApgkQAqgmAAgzQAAgzgqglQgpglg2AAImWAAIAAhNIGWAAQBVAABAA8QBAA8AABSQAABThAA7QhAA7hVAAg");
    var mask_3_graphics_14 = new cjs.Graphics().p("AgTDKIAAhMIB0AAQA2AAApgkQAqgmAAgzQAAgzgqglQgpglg2AAImWAAIAAhNIGWAAQBVAABAA8QBAA8AABSQAABThAA7QhAA7hVAAg");
    var mask_3_graphics_15 = new cjs.Graphics().p("AhkDKIAAhMIDFAAQA2AAApgkQAqgmAAgzQAAgzgqglQgpglg2AAImWAAIAAhNIGWAAQBVAABAA8QBAA8AABSQAABThAA7QhAA7hVAAg");
    var mask_3_graphics_16 = new cjs.Graphics().p("AiqDKIAAhMIELAAQA2AAApgkQAqgmAAgzQAAgzgqglQgpglg2AAImWAAIAAhNIGWAAQBVAABAA8QBAA8AABSQAABThAA7QhAA7hVAAg");
    var mask_3_graphics_17 = new cjs.Graphics().p("AjtDKIAAhMIFOAAQA2AAApgkQAqgmAAgzQAAgzgqglQgpglg2AAImWAAIAAhNIGWAAQBVAABAA8QBAA8AABSQAABThAA7QhAA7hVAAg");
    var mask_3_graphics_18 = new cjs.Graphics().p("Ak1DKIAAhMIGWAAQA2AAApgkQAqgmAAgzQAAgzgqglQgpglg2AAImWAAIAAhNIGWAAQBVAABAA8QBAA8AABSQAABThAA7QhAA7hVAAg");
    var mask_3_graphics_19 = new cjs.Graphics().p("Ak1EdIAAg2IBMAAIAAA2gAk1B2IAAhMIGWAAQA2AAApgkQAqgkAAg0QAAgzgqgmQgpglg2AAImWAAIAAhMIGWAAQBVAABAA7QBAA8AABTQAABShAA8QhAA6hVAAg");
    var mask_3_graphics_20 = new cjs.Graphics().p("Ak1E6IAAhvIBMAAIAABvgAk1BaIAAhMIGWAAQA2gBApgiQAqgmAAgzQAAg0gqglQgpglg2AAImWAAIAAhNIGWAAQBVAABAA8QBAA8AABTQAABThAA6QhAA6hVABg");
    var mask_3_graphics_21 = new cjs.Graphics().p("Ak1FgIAAi8IBMAAIAAC8gAk1AzIAAhLIGWAAQA2AAApgkQAqglAAg0QAAgzgqgmQgpglg2AAImWAAIAAhMIGWAAQBVAABAA7QBAA8AABTQAABThAA8QhAA5hVAAg");
    var mask_3_graphics_22 = new cjs.Graphics().p("Ak1GNIAAkVIBMAAIAAEVgAk1AHIAAhLIGWAAQA2AAApgkQAqgmAAgzQAAgzgqgmQgpglg2AAImWAAIAAhMIGWAAQBVgBBAA8QBAA8AABTQAABThAA7QhAA6hVAAg");
    var mask_3_graphics_23 = new cjs.Graphics().p("Ak1GyIAAlgIBMAAIAAFggAk1geIAAhMIGWAAQA2AAApgkQAqgmAAgzQAAgzgqgmQgpglg2AAImWAAIAAhMIGWAAQBVgBBAA8QBAA8AABTQAABThAA7QhAA7hVAAg");
    var mask_3_graphics_24 = new cjs.Graphics().p("Ak1HVIAAmmIBMAAIAAGkIABAAIAAACgAk1hBIAAhMIGWAAQA2AAApgkQAqglAAg0QAAgzgqgmQgpglg2AAImWAAIAAhMIGWAAQBVAABAA7QBAA8AABTQAABThAA8QhAA6hVAAg");
    var mask_3_graphics_25 = new cjs.Graphics().p("Ak1H6IAAnvIBMAAIAAGjIABAAIAABMgAk1hlIAAhMIGWAAQA2AAApgkQAqgmAAgzQAAg0gqglQgpglg2AAImWAAIAAhNIGWAAQBVAABAA8QBAA8AABTQAABThAA7QhAA7hVAAg");
    var mask_3_graphics_26 = new cjs.Graphics().p("Ak1H6IAAnvIBMAAIAAGjIA9AAIAABMgAk1hlIAAhMIGWAAQA2AAApgkQAqgmAAgzQAAg0gqglQgpglg2AAImWAAIAAhNIGWAAQBVAABAA8QBAA8AABTQAABThAA7QhAA7hVAAg");
    var mask_3_graphics_27 = new cjs.Graphics().p("Ak1H6IAAnvIBMAAIAAGjICCAAIAABMgAk1hlIAAhMIGWAAQA2AAApgkQAqgmAAgzQAAg0gqglQgpglg2AAImWAAIAAhNIGWAAQBVAABAA8QBAA8AABTQAABThAA7QhAA7hVAAg");
    var mask_3_graphics_28 = new cjs.Graphics().p("Ak1H6IAAnvIBMAAIAAGjIDRAAIAABMgAk1hlIAAhMIGWAAQA2AAApgkQAqgmAAgzQAAg0gqglQgpglg2AAImWAAIAAhNIGWAAQBVAABAA8QBAA8AABTQAABThAA7QhAA7hVAAg");
    var mask_3_graphics_29 = new cjs.Graphics().p("Ak1H6IAAnvIBMAAIAAGjICFAAIAAg7IBMAAIAACHgAk1hlIAAhMIGWAAQA2AAApgkQAqgmAAgzQAAg0gqglQgpglg2AAImWAAIAAhNIGWAAQBVAABAA8QBAA8AABTQAABThAA7QhAA7hVAAg");
    var mask_3_graphics_30 = new cjs.Graphics().p("Ak1H6IAAnvIBMAAIAAGjICFAAIAAh8IBMAAIAADIgAk1hlIAAhMIGWAAQA2AAApgkQAqgmAAgzQAAg0gqglQgpglg2AAImWAAIAAhNIGWAAQBVAABAA8QBAA8AABTQAABThAA7QhAA7hVAAg");
    var mask_3_graphics_31 = new cjs.Graphics().p("Ak1H6IAAnvIBMAAIAAGjICFAAIAAjIIBMAAIAAEUgAk1hlIAAhMIGWAAQA2AAApgkQAqgmAAgzQAAg0gqglQgpglg2AAImWAAIAAhNIGWAAQBVAABAA8QBAA8AABTQAABThAA7QhAA7hVAAg");
    var mask_3_graphics_32 = new cjs.Graphics().p("Ak1H6IAAnvIBMAAIAAGjICFAAIAAkSIBMAAIAAFegAk1hlIAAhMIGWAAQA2AAApgkQAqgmAAgzQAAg0gqglQgpglg2AAImWAAIAAhNIGWAAQBVAABAA8QBAA8AABTQAABThAA7QhAA7hVAAg");
    var mask_3_graphics_33 = new cjs.Graphics().p("Ak1H6IAAnvIBMAAIAAGjICFAAIAAlYIBPAAIAAABIgDAAIAAGjgAk1hlIAAhMIGWAAQA2AAApgkQAqgmAAgzQAAg0gqglQgpglg2AAImWAAIAAhNIGWAAQBVAABAA8QBAA8AABTQAABThAA7QhAA7hVAAg");
    var mask_3_graphics_34 = new cjs.Graphics().p("Ak1H6IAAnvIBMAAIAAGjICFAAIAAmjIBPAAIAABMIgDAAIAAGjgAk1hlIAAhMIGWAAQA2AAApgkQAqgmAAgzQAAg0gqglQgpglg2AAImWAAIAAhNIGWAAQBVAABAA8QBAA8AABTQAABThAA7QhAA7hVAAg");
    var mask_3_graphics_35 = new cjs.Graphics().p("Ak1H6IAAnvIBMAAIAAGjICFAAIAAmjICJAAIAABPIgBAAIgBgDIg7AAIAAGjgAk1hlIAAhMIGWAAQA2AAApgkQAqgmAAgzQAAg0gqglQgpglg2AAImWAAIAAhNIGWAAQBVAABAA8QBAA8AABTQAABThAA7QhAA7hVAAg");
    var mask_3_graphics_36 = new cjs.Graphics().p("Ak1H6IAAnvIBMAAIAAGjICFAAIAAmjIC0AAIAhA9IhNASIgBgDIg7AAIAAGjgAk1hlIAAhMIGWAAQA2AAApgkQAqgmAAgzQAAg0gqglQgpglg2AAImWAAIAAhNIGWAAQBVAABAA8QBAA8AABTQAABThAA7QhAA7hVAAg");
    var mask_3_graphics_37 = new cjs.Graphics().p("Ak1H6IAAnvIBMAAIAAGjICFAAIAAmjIC0AAIAvBWIhCAmIgagwIg7AAIAAGjgAk1hlIAAhMIGWAAQA2AAApgkQAqgmAAgzQAAg0gqglQgpglg2AAImWAAIAAhNIGWAAQBVAABAA8QBAA8AABTQAABThAA7QhAA7hVAAg");
    var mask_3_graphics_38 = new cjs.Graphics().p("Ak1H6IAAnvIBMAAIAAGjICFAAIAAmjIC0AAIBSCYIhBAoIg+h0Ig7AAIAAGjgAk1hlIAAhMIGWAAQA2AAApgkQAqgmAAgzQAAg0gqglQgpglg2AAImWAAIAAhNIGWAAQBVAABAA8QBAA8AABTQAABThAA7QhAA7hVAAg");
    var mask_3_graphics_39 = new cjs.Graphics().p("Ak1H6IAAnvIBMAAIAAGjICFAAIAAmjIC0AAIB0DXIhAApIhhi0Ig7AAIAAGjgAk1hlIAAhMIGWAAQA2AAApgkQAqgmAAgzQAAg0gqglQgpglg2AAImWAAIAAhNIGWAAQBVAABAA8QBAA8AABTQAABThAA7QhAA7hVAAg");
    var mask_3_graphics_40 = new cjs.Graphics().p("Ak1H6IAAnvIBMAAIAAGjICFAAIAAmjIC0AAICUESIhAAqIiBjwIg7AAIAAGjgAk1hlIAAhMIGWAAQA2AAApgkQAqgmAAgzQAAg0gqglQgpglg2AAImWAAIAAhNIGWAAQBVAABAA8QBAA8AABTQAABThAA7QhAA7hVAAg");
    var mask_3_graphics_41 = new cjs.Graphics().p("Ak1H6IAAnvIBMAAIAAGjICFAAIAAmjIC0AAIC+FfIhIAbIijkuIg7AAIAAGjgAk1hlIAAhMIGWAAQA2AAApgkQAqgmAAgzQAAg0gqglQgpglg2AAImWAAIAAhNIGWAAQBVAABAA8QBAA8AABTQAABThAA7QhAA7hVAAg");
    var mask_3_graphics_42 = new cjs.Graphics().p("Ak1H6IAAnvIBMAAIAAGjICFAAIAAmjIC0AAIDfGcIAAAEIhMASIjAlmIg7AAIAAGjgAk1hlIAAhMIGWAAQA2AAApgkQAqgmAAgzQAAg0gqglQgpglg2AAImWAAIAAhNIGWAAQBVAABAA8QBAA8AABTQAABThAA7QhAA7hVAAg");
    var mask_3_graphics_43 = new cjs.Graphics().p("ADvH6IjhmjIg7AAIAAGjIkeAAIAAnvIBNAAIAAGjICFAAIAAmjIC0AAIDjGjIAlAAIAIBMgAlLhlIAAhMIGXAAQA2gBApgjQAqgmAAgzQAAg0gqglQgpglg2AAImXAAIAAhNIGXAAQBVAAA/A8QBBA8AABTQAABThBA7Qg/A6hVABg");
    var mask_3_graphics_44 = new cjs.Graphics().p("ADOH6IjhmjIg7AAIAAGjIkeAAIAAnvIBMAAIAAGjICGAAIAAmjIC0AAIDiGjIBfAAIASBMgAlshlIAAhMIGXAAQA1gBAqgjQAqgmAAgzQAAg0gqglQgqglg1AAImXAAIAAhNIGXAAQBVAAA/A8QBBA8AABTQAABThBA7Qg/A6hVABg");
    var mask_3_graphics_45 = new cjs.Graphics().p("ACkH6IjgmjIg8AAIAAGjIkdAAIAAnvIBMAAIAAGjICFAAIAAmjIC1AAIDiGjIBpAAIAAABIAzAAIAnBLgAmVhlIAAhMIGWAAQA2gBApgjQAqgmAAgzQAAg0gqglQgpglg2AAImWAAIAAhNIGWAAQBVAABAA8QBAA8AABTQAABThAA7QhAA6hVABg");
    var mask_3_graphics_46 = new cjs.Graphics().p("AB4H6IjgmjIg8AAIAAGjIkdAAIAAnvIBMAAIAAGjICFAAIAAmjIC1AAIDiGjIBpAAIAAABICIAAIAqBLgAnBhlIAAhMIGXAAQA1gBApgjQAqgmAAgzQAAg0gqglQgpglg1AAImXAAIAAhNIGXAAQBUAABAA8QBAA8AABTQAABThAA7QhAA6hUABg");
    var mask_3_graphics_47 = new cjs.Graphics().p("AB4H6IjgmjIg8AAIAAGjIkdAAIAAnvIBMAAIAAGjICFAAIAAmjIC1AAIDiGjICZAAIgRgeIBMgVIBHB/gAnBhlIAAhMIGXAAQA1gBApgjQAqgmAAgzQAAg0gqglQgpglg1AAImXAAIAAhNIGXAAQBUAABAA8QBAA8AABTQAABThAA7QhAA6hUABg");
    var mask_3_graphics_48 = new cjs.Graphics().p("AB4H6IjgmjIg8AAIAAGjIkdAAIAAnvIBMAAIAAGjICFAAIAAmjIC1AAIDiGjICZAAIgthPIBFghIBqC8gAnBhlIAAhMIGXAAQA1gBApgjQAqgmAAgzQAAg0gqglQgpglg1AAImXAAIAAhNIGXAAQBUAABAA8QBAA8AABTQAABThAA7QhAA6hUABg");
    var mask_3_graphics_49 = new cjs.Graphics().p("AB4H6IjgmjIg8AAIAAGjIkdAAIAAnvIBMAAIAAGjICFAAIAAmjIC1AAIDiGjICZAAIhTiUIBDgjICSEDgAnBhlIAAhMIGXAAQA1gBApgjQAqgmAAgzQAAg0gqglQgpglg1AAImXAAIAAhNIGXAAQBUAABAA8QBAA8AABTQAABThAA7QhAA6hUABg");
    var mask_3_graphics_50 = new cjs.Graphics().p("AB4H6IjgmjIg8AAIAAGjIkdAAIAAnvIBMAAIAAGjICFAAIAAmjIC1AAIDiGjICZAAIh0jNIBHgeICvE3gAnBhlIAAhMIGXAAQA1gBApgjQAqgmAAgzQAAg0gqglQgpglg1AAImXAAIAAhNIGXAAQBUAABAA8QBAA8AABTQAABThAA7QhAA6hUABg");
    var mask_3_graphics_51 = new cjs.Graphics().p("AB4H6IjgmjIg8AAIAAGjIkdAAIAAnvIBMAAIAAGjICFAAIAAmjIC1AAIDiGjICZAAIiSkDIBDgjIDRFygAnBhlIAAhMIGXAAQA1gBApgjQAqgmAAgzQAAg0gqglQgpglg1AAImXAAIAAhNIGXAAQBUAABAA8QBAA8AABTQAABThAA7QhAA6hUABg");
    var mask_3_graphics_52 = new cjs.Graphics().p("AB4H6IjgmjIg8AAIAAGjIkdAAIAAnvIBMAAIAAGjICFAAIAAmjIC1AAIDiGjICZAAIi0k/IBJgaIDtGlgAnBhlIAAhMIGXAAQA1gBApgjQAqgmAAgzQAAg0gqglQgpglg1AAImXAAIAAhNIGXAAQBUAABAA8QBAA8AABTQAABThAA7QhAA6hUABg");
    var mask_3_graphics_53 = new cjs.Graphics().p("AB4H6IjgmjIg8AAIAAGjIkdAAIAAnvIBMAAIAAGjICFAAIAAmjIC1AAIDiGjICZAAIjRlzIBMgUIEHHTgAnBhlIAAhMIGXAAQA1gBApgjQAqgmAAgzQAAg0gqglQgpglg1AAImXAAIAAhNIGXAAQBUAABAA8QBAA8AABTQAABThAA7QhAA6hUABg");
    var mask_3_graphics_54 = new cjs.Graphics().p("AB4H6IjgmjIg8AAIAAGjIkdAAIAAnvIBMAAIAAGjICFAAIAAmjIC1AAIDiGjICZAAIjcmFIgVgkIAngRIAUgJIA/AzIgOAJIEHHTgAnBhlIAAhMIGXAAQA1gBApgjQAqgmAAgzQAAg0gqglQgpglg1AAImXAAIAAhNIGXAAQBUAABAA8QBAA8AABTQAABThAA7QhAA6hUABg");
    var mask_3_graphics_55 = new cjs.Graphics().p("AB4H6IjgmjIg8AAIAAGjIkdAAIAAnvIBMAAIAAGjICFAAIAAmjIC1AAIDiGjICZAAIjcmFIgVgkIAngRQAmgQAegXIA+AwQgcAXghATIEHHTgAnBhlIAAhMIGXAAQA1gBApgjQAqgmAAgzQAAg0gqglQgpglg1AAImXAAIAAhNIGXAAQBUAABAA8QBAA8AABTQAABThAA7QhAA6hUABg");
    var mask_3_graphics_56 = new cjs.Graphics().p("AB4H6IjgmjIg8AAIAAGjIkdAAIAAnvIBMAAIAAGjICFAAIAAmjIC1AAIDiGjICZAAIjcmFIgVgkIAngRQA8gZArguIBGAkQgsAzg8AjIEHHTgAnBhlIAAhMIGXAAQA1gBApgjQAqgmAAgzQAAg0gqglQgpglg1AAImXAAIAAhNIGXAAQBUAABAA8QBAA8AABTQAABThAA7QhAA6hUABg");
    var mask_3_graphics_57 = new cjs.Graphics().p("AB4H6IjgmjIg8AAIAAGjIkdAAIAAnvIBMAAIAAGjICFAAIAAmjIC1AAIDiGjICZAAIjcmFIgVgkIAngRQBVgkAzhMIBKAaQg1BXhYAyIEHHTgAnBhlIAAhMIGXAAQA1gBApgjQAqgmAAgzQAAg0gqglQgpglg1AAImXAAIAAhNIGXAAQBUAABAA8QBAA8AABTQAABThAA7QhAA6hUABg");
    var mask_3_graphics_58 = new cjs.Graphics().p("AB4H6IjgmjIg8AAIAAGjIkdAAIAAnvIBMAAIAAGjICFAAIAAmjIC1AAIDiGjICZAAIjcmFIgVgkIAngRQBXglA0hQQAMgSAKgUIBSABQgMAggSAfQg1BbhbAzIEHHTgAnBhlIAAhMIGXAAQA1gBApgjQAqgmAAgzQAAg0gqglQgpglg1AAImXAAIAAhNIGXAAQBUAABAA8QBAA8AABTQAABThAA7QhAA6hUABg");
    var mask_3_graphics_59 = new cjs.Graphics().p("AB4H6IjgmjIg8AAIAAGjIkdAAIAAnvIBMAAIAAGjICFAAIAAmjIC1AAIDiGjICZAAIjcmFIgVgkIAngRQBXglA0hQQAeguANg0IBQgPQgFAhgLAgQgMAmgVAkQg1BbhbAzIEHHTgAnBhlIAAhMIGXAAQA1gBApgjQAqgmAAgzQAAg0gqglQgpglg1AAImXAAIAAhNIGXAAQBUAABAA8QBAA8AABTQAABThAA7QhAA6hUABg");
    var mask_3_graphics_60 = new cjs.Graphics().p("AB4H6IjgmjIg8AAIAAGjIkdAAIAAnvIBMAAIAAGjICFAAIAAmjIC1AAIDiGjICZAAIjcmFIgVgkIAngRQBXglA0hQQAyhMABhbIBMgXIAAATQAABAgUA7QgMAmgVAkQg1BbhbAzIEHHTgAnBhlIAAhMIGXAAQA1gBApgjQAqgmAAgzQAAg0gqglQgpglg1AAImXAAIAAhNIGXAAQBUAABAA8QBAA8AABTQAABThAA7QhAA6hUABg");
    var mask_3_graphics_61 = new cjs.Graphics().p("AB4H6IjgmjIg8AAIAAGjIkdAAIAAnvIBMAAIAAGjICFAAIAAmjIC1AAIDiGjICZAAIjcmFIgVgkIAngRQBXglA0hQQA0hOgBhfQAAgegEgcIBHghQAKAtgBAwQAABAgUA7QgMAmgVAkQg1BbhbAzIEHHTgAnBhlIAAhMIGXAAQA1gBApgjQAqgmAAgzQAAg0gqglQgpglg1AAImXAAIAAhNIGXAAQBUAABAA8QBAA8AABTQAABThAA7QhAA6hUABg");
    var mask_3_graphics_62 = new cjs.Graphics().p("AB4H6IjgmjIg8AAIAAGjIkdAAIAAnvIBMAAIAAGjICFAAIAAmjIC1AAIDiGjICZAAIjcmFIgVgkIAngRQBXglA0hQQA0hOgBhfQAAgwgMgrIgHgYIA+gvQAHAQAGARIABACQAUA9gBBEQAABAgUA7QgMAmgVAkQg1BbhbAzIEHHTgAnBhlIAAhMIGXAAQA1gBApgjQAqgmAAgzQAAg0gqglQgpglg1AAImXAAIAAhNIGXAAQBUAABAA8QBAA8AABTQAABThAA7QhAA6hUABg");
    var mask_3_graphics_63 = new cjs.Graphics().p("AB4IAIjgmjIg8AAIAAGjIkdAAIAAnvIBMAAIAAGjICFAAIAAmjIC1AAIDiGjICZAAIjcmEIgVglIAngQQBXgmA0hPQA0hPgBhfQABhYgqhHIA2g4QAcApAPAuQAVA9gBBGQAABAgUA6QgMAmgVAkQg1BbhbAzIEHHTgAnBhfIAAhMIGXAAQA1AAApgkQAqgmAAgzQAAg0gqglQgpglg1AAImXAAIAAhNIGXAAQBUAABAA8QBAA8AABTQAABThAA7QhAA7hUAAg");
    var mask_3_graphics_64 = new cjs.Graphics().p("AB4IhIjgmjIg8AAIAAGjIkdAAIAAnvIBMAAIAAGjICFAAIAAmjIC1AAIDiGjICZAAIjcmFIgVgkIAngSQBXgkA0hPQA0hPgBhfQABhZgqhGIAAh6IAEAEQBCBCAbBSQAVA+gBBFQAABAgUA7QgMAmgVAkQg1BahbA0IEHHTgAnBg+IAAhMIGXAAQA1gBApgjQAqgmAAgzQAAg0gqglQgpglg1AAImXAAIAAhNIGXAAQBUAABAA8QBAA8AABTQAABThAA7QhAA6hUABg");
    var mask_3_graphics_65 = new cjs.Graphics().p("AB4I6IjgmjIg8AAIAAGiIkdAAIAAnvIBMAAIAAGkICFAAIAAmkIC1AAIDiGkICZAAIjcmFIgVglIAngRQBXglA0hPQA0hPgBhfQABiDhdhdIgIgIIAAhiQAhAXAeAeQBCBDAbBSQAVA9gBBGQAABAgUA7QgMAmgVAjQg1BahbA1IEHHTgAnBgmIAAhMIGXAAQA1AAApgkQAqglAAg0QAAgzgqgmQgpglg1AAImXAAIAAhMIGXAAQBUAABAA7QBAA8AABTQAABThAA8QhAA6hUAAg");
    var mask_3_graphics_66 = new cjs.Graphics().p("AB4JMIjgmjIg8AAIAAGjIkdAAIAAnvIBMAAIAAGjICFAAIAAmjIC1AAIDiGjICZAAIjcmFIgVgkIAngSQBXglA0hPQA0hOgBhfQABiEhdhdQgjgjgogXIAAhUQBHAeA7A8QBCBCAbBSQAVA+gBBFQAABAgUA7QgMAmgVAkQg1BahbA0IEHHTgAnBgTIAAhMIGXAAQA1gBApgjQAqgmAAgzQAAg0gqglQgpglg1AAImXAAIAAhNIGXAAQBUAABAA8QBAA8AABTQAABThAA7QhAA6hUABg");
    var mask_3_graphics_67 = new cjs.Graphics().p("AB4JXIjgmjIg8AAIAAGiIkdAAIAAnvIBMAAIAAGkICFAAIAAmkIC1AAIDiGkICZAAIjcmFIgVglIAngRQBXglA0hPQA0hPgBhfQABiDhdhdQhAhBhTgVIAAhOQBzAXBXBYQBCBDAbBSQAVA9gBBGQAABAgUA7QgMAmgVAjQg1BahbA1IEHHTgAnBgJIAAhMIGXAAQA1AAApgkQAqglAAg0QAAgzgqgmQgpglg1AAImXAAIAAhMIGXAAQBUAABAA7QBAA8AABTQAABThAA8QhAA6hUAAg");
    var mask_3_graphics_68 = new cjs.Graphics().p("AB4JaIjgmiIg8AAIAAGiIkdAAIAAnvIBMAAIAAGkICFAAIAAmkIC1AAIDiGkICZAAIjcmFIgVglIAngRQBXgmA0hOQA0hPgBhfQABiEhdhdQhahbiAgDIAAhNIAVABQCTAKBpBsQBCBDAbBSQAVA9gBBGQAABAgUA6QgMAmgVAkQg1BahbA0IEHHTgAnBgFIAAhMIGXAAQA1AAApgkQAqgmAAgzQAAgzgqgmQgpglg1AAImXAAIAAhMIGXAAQBUgBBAA8QBAA8AABTQAABThAA7QhAA7hUAAg");
    var mask_3_graphics_69 = new cjs.Graphics().p("AB4JaIjgmiIg8AAIAAGiIkdAAIAAnvIBMAAIAAGkICFAAIAAmkIC1AAIDiGkICZAAIjcmFIgVglIAngRQBXgmA0hOQA0hPgBhfQABiEhdhdQhchdiDgBIhFAAIAAhNIBFAAIAaABQCTAKBpBsQBCBDAbBSQAVA9gBBGQAABAgUA6QgMAmgVAkQg1BahbA0IEHHTgAnBgFIAAhMIGXAAQA1AAApgkQAqgmAAgzQAAgzgqgmQgpglg1AAImXAAIAAhMIGXAAQBUgBBAA8QBAA8AABTQAABThAA7QhAA7hUAAg");
    var mask_3_graphics_70 = new cjs.Graphics().p("AB4JaIjgmiIg8AAIAAGiIkdAAIAAnvIBMAAIAAGkICFAAIAAmkIC1AAIDiGkICZAAIjcmFIgVglIAngRQBXgmA0hOQA0hPgBhfQABiEhdhdQhchdiDgBIiWAAIAAhNICWAAIAaABQCTAKBpBsQBCBDAbBSQAVA9gBBGQAABAgUA6QgMAmgVAkQg1BahbA0IEHHTgAnBgFIAAhMIGXAAQA1AAApgkQAqgmAAgzQAAgzgqgmQgpglg1AAImXAAIAAhMIGXAAQBUgBBAA8QBAA8AABTQAABThAA7QhAA7hUAAg");
    var mask_3_graphics_71 = new cjs.Graphics().p("AB4JaIjgmiIg8AAIAAGiIkdAAIAAnvIBMAAIAAGkICFAAIAAmkIC1AAIDiGkICZAAIjcmFIgVglIAngRQBXgmA0hOQA0hPgBhfQABiEhdhdQhchdiDgBIjbAAIAAhNIDbAAIAaABQCTAKBpBsQBCBDAbBSQAVA9gBBGQAABAgUA6QgMAmgVAkQg1BahbA0IEHHTgAnBgFIAAhMIGXAAQA1AAApgkQAqgmAAgzQAAgzgqgmQgpglg1AAImXAAIAAhMIGXAAQBUgBBAA8QBAA8AABTQAABThAA7QhAA7hUAAg");
    var mask_3_graphics_72 = new cjs.Graphics().p("AB4JaIjgmiIg8AAIAAGiIkdAAIAAnvIBMAAIAAGkICFAAIAAmkIC1AAIDiGkICZAAIjcmFIgVglIAngRQBXgmA0hOQA0hPgBhfQABiEhdhdQhchdiDgBIkeAAIAAhNIEeAAIAaABQCTAKBpBsQBCBDAbBSQAVA9gBBGQAABAgUA6QgMAmgVAkQg1BahbA0IEHHTgAnBgFIAAhMIGXAAQA1AAApgkQAqgmAAgzQAAgzgqgmQgpglg1AAImXAAIAAhMIGXAAQBUgBBAA8QBAA8AABTQAABThAA7QhAA7hUAAg");
    var mask_3_graphics_73 = new cjs.Graphics().p("AB4JaIjgmiIg8AAIAAGiIkdAAIAAnvIBMAAIAAGkICFAAIAAmkIC1AAIDiGkICZAAIjcmFIgVglIAngRQBXgmA0hOQA0hPgBhfQABiEhdhdQhchdiDgBIltAAIAAhNIFtAAIAaABQCTAKBpBsQBCBDAbBSQAVA9gBBGQAABAgUA6QgMAmgVAkQg1BahbA0IEHHTgAnBgFIAAhMIGXAAQA1AAApgkQAqgmAAgzQAAgzgqgmQgpglg1AAImXAAIAAhMIGXAAQBUgBBAA8QBAA8AABTQAABThAA7QhAA7hUAAg");
    var mask_3_graphics_74 = new cjs.Graphics().p("AB4JaIjgmiIg8AAIAAGiIkdAAIAAnvIBMAAIAAGkICFAAIAAmkIC1AAIDiGkICZAAIjcmFIgVglIAngRQBXgmA0hOQA0hPgBhfQABiEhdhdQhchdiDgBIm6AAIAAhNIG6AAIAaABQCTAKBpBsQBCBDAbBSQAVA9gBBGQAABAgUA6QgMAmgVAkQg1BahbA0IEHHTgAnBgFIAAhMIGXAAQA1AAApgkQAqgmAAgzQAAgzgqgmQgpglg1AAImXAAIAAhMIGXAAQBUgBBAA8QBAA8AABTQAABThAA7QhAA7hUAAg");
    var mask_3_graphics_119 = new cjs.Graphics().p("AB4JaIjgmiIg8AAIAAGiIkdAAIAAnvIBMAAIAAGkICFAAIAAmkIC1AAIDiGkICZAAIjcmFIgVglIAngRQBXgmA0hOQA0hPgBhfQABiEhdhdQhchdiDgBIm6AAIAAhNIG6AAIAaABQCTAKBpBsQBCBDAbBSQAVA9gBBGQAABAgUA6QgMAmgVAkQg1BahbA0IEHHTgAnBgFIAAhMIGXAAQA1AAApgkQAqgmAAgzQAAgzgqgmQgpglg1AAImXAAIAAhMIGXAAQBUgBBAA8QBAA8AABTQAABThAA7QhAA7hUAAg");
    var mask_3_graphics_120 = new cjs.Graphics().p("AB4JaIjgmiIg8AAIAAGiIkdAAIAAnvIBMAAIAAGkICFAAIAAmkIC1AAIDiGkICZAAIjcmFIgVglIAngRQBXgmA0hOQA0hPgBhfQABiEhdhdQhchdiDgBIm6AAIAAhNIG6AAIAaABQCTAKBpBsQBCBDAbBSQAVA9gBBGQAABAgUA6QgMAmgVAkQg1BahbA0IEHHTgAnBgFIAAhMIGXAAQA1AAApgkQAqgmAAgzQAAgzgqgmQgpglg1AAImXAAIAAhMIGXAAQBUgBBAA8QBAA8AABTQAABThAA7QhAA7hUAAg");
    var mask_3_graphics_121 = new cjs.Graphics().p("AB4JaIjgmiIg8AAIAAGiIkdAAIAAnvIBMAAIAAGkICFAAIAAmkIC1AAIDiGkICZAAIjcmFIgVglIAngRQBXgmA0hOQA0hPgBhfQABiEhdhdQhchdiDgBIm6AAIAAhNIG6AAIAaABQCTAKBpBsQBCBDAbBSQAVA9gBBGQAABAgUA6QgMAmgVAkQg1BahbA0IEHHTgAnBgFIAAhMIGXAAQA1AAApgkQAqgmAAgzQAAgzgqgmQgpglg1AAImXAAIAAhMIGXAAQBUgBBAA8QBAA8AABTQAABThAA7QhAA7hUAAg");
    var mask_3_graphics_122 = new cjs.Graphics().p("AB4JaIjgmiIg8AAIAAGiIkdAAIAAnvIBMAAIAAGkICFAAIAAmkIC1AAIDiGkICZAAIjcmFIgVglIAngRQBXgmA0hOQA0hPgBhfQABiEhdhdQhchdiDgBIm6AAIAAhNIG6AAIAaABQCTAKBpBsQBCBDAbBSQAVA9gBBGQAABAgUA6QgMAmgVAkQg1BahbA0IEHHTgAnBgFIAAhMIGXAAQA1AAApgkQAqgmAAgzQAAgzgqgmQgpglg1AAImXAAIAAhMIGXAAQBUgBBAA8QBAA8AABTQAABThAA7QhAA7hUAAg");
    var mask_3_graphics_123 = new cjs.Graphics().p("AB4JaIjgmiIg8AAIAAGiIkdAAIAAnvIBMAAIAAGkICFAAIAAmkIC1AAIDiGkICZAAIjcmFIgVglIAngRQBXgmA0hOQA0hPgBhfQABiEhdhdQhchdiDgBIm6AAIAAhNIG6AAIAaABQCTAKBpBsQBCBDAbBSQAVA9gBBGQAABAgUA6QgMAmgVAkQg1BahbA0IEHHTgAnBgFIAAhMIGXAAQA1AAApgkQAqgmAAgzQAAgzgqgmQgpglg1AAImXAAIAAhMIGXAAQBUgBBAA8QBAA8AABTQAABThAA7QhAA7hUAAg");
    var mask_3_graphics_124 = new cjs.Graphics().p("AB4JaIjgmiIg8AAIAAGiIkdAAIAAnvIBMAAIAAGkICFAAIAAmkIC1AAIDiGkICZAAIjcmFIgVglIAngRQBXgmA0hOQA0hPgBhfQABiEhdhdQhchdiDgBIm6AAIAAhNIG6AAIAaABQCTAKBpBsQBCBDAbBSQAVA9gBBGQAABAgUA6QgMAmgVAkQg1BahbA0IEHHTgAnBgFIAAhMIGXAAQA1AAApgkQAqgmAAgzQAAgzgqgmQgpglg1AAImXAAIAAhMIGXAAQBUgBBAA8QBAA8AABTQAABThAA7QhAA7hUAAg");
    var mask_3_graphics_125 = new cjs.Graphics().p("AB4JaIjgmiIg8AAIAAGiIkdAAIAAnvIBMAAIAAGkICFAAIAAmkIC1AAIDiGkICZAAIjcmFIgVglIAngRQBXgmA0hOQA0hPgBhfQABiEhdhdQhchdiDgBIm6AAIAAhNIG6AAIAaABQCTAKBpBsQBCBDAbBSQAVA9gBBGQAABAgUA6QgMAmgVAkQg1BahbA0IEHHTgAnBgFIAAhMIGXAAQA1AAApgkQAqgmAAgzQAAgzgqgmQgpglg1AAImXAAIAAhMIGXAAQBUgBBAA8QBAA8AABTQAABThAA7QhAA7hUAAg");
    var mask_3_graphics_126 = new cjs.Graphics().p("AB4JaIjgmiIg8AAIAAGiIkdAAIAAnvIBMAAIAAGkICFAAIAAmkIC1AAIDiGkICZAAIjcmFIgVglIAngRQBXgmA0hOQA0hPgBhfQABiEhdhdQhchdiDgBIm6AAIAAhNIG6AAIAaABQCTAKBpBsQBCBDAbBSQAVA9gBBGQAABAgUA6QgMAmgVAkQg1BahbA0IEHHTgAnBgFIAAhMIGXAAQA1AAApgkQAqgmAAgzQAAgzgqgmQgpglg1AAImXAAIAAhMIGXAAQBUgBBAA8QBAA8AABTQAABThAA7QhAA7hUAAg");
    var mask_3_graphics_127 = new cjs.Graphics().p("AB4JaIjgmiIg8AAIAAGiIkdAAIAAnvIBMAAIAAGkICFAAIAAmkIC1AAIDiGkICZAAIjcmFIgVglIAngRQBXgmA0hOQA0hPgBhfQABiEhdhdQhchdiDgBIm6AAIAAhNIG6AAIAaABQCTAKBpBsQBCBDAbBSQAVA9gBBGQAABAgUA6QgMAmgVAkQg1BahbA0IEHHTgAnBgFIAAhMIGXAAQA1AAApgkQAqgmAAgzQAAgzgqgmQgpglg1AAImXAAIAAhMIGXAAQBUgBBAA8QBAA8AABTQAABThAA7QhAA7hUAAg");
    var mask_3_graphics_128 = new cjs.Graphics().p("AB4JaIjgmiIg8AAIAAGiIkdAAIAAnvIBMAAIAAGkICFAAIAAmkIC1AAIDiGkICZAAIjcmFIgVglIAngRQBXgmA0hOQA0hPgBhfQABiEhdhdQhchdiDgBIm6AAIAAhNIG6AAIAaABQCTAKBpBsQBCBDAbBSQAVA9gBBGQAABAgUA6QgMAmgVAkQg1BahbA0IEHHTgAnBgFIAAhMIGXAAQA1AAApgkQAqgmAAgzQAAgzgqgmQgpglg1AAImXAAIAAhMIGXAAQBUgBBAA8QBAA8AABTQAABThAA7QhAA7hUAAg");
    var mask_3_graphics_129 = new cjs.Graphics().p("AB4JaIjgmiIg8AAIAAGiIkdAAIAAnvIBMAAIAAGkICFAAIAAmkIC1AAIDiGkICZAAIjcmFIgVglIAngRQBXgmA0hOQA0hPgBhfQABiEhdhdQhchdiDgBIm6AAIAAhNIG6AAIAaABQCTAKBpBsQBCBDAbBSQAVA9gBBGQAABAgUA6QgMAmgVAkQg1BahbA0IEHHTgAnBgFIAAhMIGXAAQA1AAApgkQAqgmAAgzQAAgzgqgmQgpglg1AAImXAAIAAhMIGXAAQBUgBBAA8QBAA8AABTQAABThAA7QhAA7hUAAg");
    var mask_3_graphics_130 = new cjs.Graphics().p("AB4JaIjgmiIg8AAIAAGiIkdAAIAAnvIBMAAIAAGkICFAAIAAmkIC1AAIDiGkICZAAIjcmFIgVglIAngRQBXgmA0hOQA0hPgBhfQABiEhdhdQhchdiDgBIm6AAIAAhNIG6AAIAaABQCTAKBpBsQBCBDAbBSQAVA9gBBGQAABAgUA6QgMAmgVAkQg1BahbA0IEHHTgAnBgFIAAhMIGXAAQA1AAApgkQAqgmAAgzQAAgzgqgmQgpglg1AAImXAAIAAhMIGXAAQBUgBBAA8QBAA8AABTQAABThAA7QhAA7hUAAg");
    var mask_3_graphics_131 = new cjs.Graphics().p("AB4JaIjgmiIg8AAIAAGiIkdAAIAAnvIBMAAIAAGkICFAAIAAmkIC1AAIDiGkICZAAIjcmFIgVglIAngRQBXgmA0hOQA0hPgBhfQABiEhdhdQhchdiDgBIm6AAIAAhNIG6AAIAaABQCTAKBpBsQBCBDAbBSQAVA9gBBGQAABAgUA6QgMAmgVAkQg1BahbA0IEHHTgAnBgFIAAhMIGXAAQA1AAApgkQAqgmAAgzQAAgzgqgmQgpglg1AAImXAAIAAhMIGXAAQBUgBBAA8QBAA8AABTQAABThAA7QhAA7hUAAg");
    var mask_3_graphics_132 = new cjs.Graphics().p("AB4JaIjgmiIg8AAIAAGiIkdAAIAAnvIBMAAIAAGkICFAAIAAmkIC1AAIDiGkICZAAIjcmFIgVglIAngRQBXgmA0hOQA0hPgBhfQABiEhdhdQhchdiDgBIm6AAIAAhNIG6AAIAaABQCTAKBpBsQBCBDAbBSQAVA9gBBGQAABAgUA6QgMAmgVAkQg1BahbA0IEHHTgAnBgFIAAhMIGXAAQA1AAApgkQAqgmAAgzQAAgzgqgmQgpglg1AAImXAAIAAhMIGXAAQBUgBBAA8QBAA8AABTQAABThAA7QhAA7hUAAg");
    var mask_3_graphics_133 = new cjs.Graphics().p("AB4JaIjgmiIg8AAIAAGiIkdAAIAAnvIBMAAIAAGkICFAAIAAmkIC1AAIDiGkICZAAIjcmFIgVglIAngRQBXgmA0hOQA0hPgBhfQABiEhdhdQhchdiDgBIm6AAIAAhNIG6AAIAaABQCTAKBpBsQBCBDAbBSQAVA9gBBGQAABAgUA6QgMAmgVAkQg1BahbA0IEHHTgAnBgFIAAhMIGXAAQA1AAApgkQAqgmAAgzQAAgzgqgmQgpglg1AAImXAAIAAhMIGXAAQBUgBBAA8QBAA8AABTQAABThAA7QhAA7hUAAg");
    var mask_3_graphics_134 = new cjs.Graphics().p("AB4JaIjgmiIg8AAIAAGiIkdAAIAAnvIBMAAIAAGkICFAAIAAmkIC1AAIDiGkICZAAIjcmFIgVglIAngRQBXgmA0hOQA0hPgBhfQABiEhdhdQhchdiDgBIm6AAIAAhNIG6AAIAaABQCTAKBpBsQBCBDAbBSQAVA9gBBGQAABAgUA6QgMAmgVAkQg1BahbA0IEHHTgAnBgFIAAhMIGXAAQA1AAApgkQAqgmAAgzQAAgzgqgmQgpglg1AAImXAAIAAhMIGXAAQBUgBBAA8QBAA8AABTQAABThAA7QhAA7hUAAg");
  
    this.timeline.addTween(cjs.Tween.get(mask_3).to({graphics:null,x:0,y:0}).wait(1).to({graphics:mask_3_graphics_1,x:571.1,y:24.7}).wait(1).to({graphics:mask_3_graphics_2,x:575.4,y:24.7}).wait(1).to({graphics:mask_3_graphics_3,x:579.9,y:24.7}).wait(1).to({graphics:mask_3_graphics_4,x:584.2,y:24.7}).wait(1).to({graphics:mask_3_graphics_5,x:588.6,y:24.7}).wait(1).to({graphics:mask_3_graphics_6,x:593.7,y:25.3}).wait(1).to({graphics:mask_3_graphics_7,x:596.9,y:27.4}).wait(1).to({graphics:mask_3_graphics_8,x:598.3,y:30.2}).wait(1).to({graphics:mask_3_graphics_9,x:598.3,y:33.8}).wait(1).to({graphics:mask_3_graphics_10,x:598.3,y:37.2}).wait(1).to({graphics:mask_3_graphics_11,x:598.3,y:39.9}).wait(1).to({graphics:mask_3_graphics_12,x:598.3,y:41}).wait(1).to({graphics:mask_3_graphics_13,x:598.3,y:41}).wait(1).to({graphics:mask_3_graphics_14,x:598.3,y:41}).wait(1).to({graphics:mask_3_graphics_15,x:598.3,y:41}).wait(1).to({graphics:mask_3_graphics_16,x:598.3,y:41}).wait(1).to({graphics:mask_3_graphics_17,x:598.3,y:41}).wait(1).to({graphics:mask_3_graphics_18,x:598.3,y:41}).wait(1).to({graphics:mask_3_graphics_19,x:598.3,y:49.4}).wait(1).to({graphics:mask_3_graphics_20,x:598.3,y:52.3}).wait(1).to({graphics:mask_3_graphics_21,x:598.3,y:56.1}).wait(1).to({graphics:mask_3_graphics_22,x:598.3,y:60.5}).wait(1).to({graphics:mask_3_graphics_23,x:598.3,y:64.3}).wait(1).to({graphics:mask_3_graphics_24,x:598.3,y:67.8}).wait(1).to({graphics:mask_3_graphics_25,x:598.3,y:71.4}).wait(1).to({graphics:mask_3_graphics_26,x:598.3,y:71.4}).wait(1).to({graphics:mask_3_graphics_27,x:598.3,y:71.4}).wait(1).to({graphics:mask_3_graphics_28,x:598.3,y:71.4}).wait(1).to({graphics:mask_3_graphics_29,x:598.3,y:71.4}).wait(1).to({graphics:mask_3_graphics_30,x:598.3,y:71.4}).wait(1).to({graphics:mask_3_graphics_31,x:598.3,y:71.4}).wait(1).to({graphics:mask_3_graphics_32,x:598.3,y:71.4}).wait(1).to({graphics:mask_3_graphics_33,x:598.3,y:71.4}).wait(1).to({graphics:mask_3_graphics_34,x:598.3,y:71.4}).wait(1).to({graphics:mask_3_graphics_35,x:598.3,y:71.4}).wait(1).to({graphics:mask_3_graphics_36,x:598.3,y:71.4}).wait(1).to({graphics:mask_3_graphics_37,x:598.3,y:71.4}).wait(1).to({graphics:mask_3_graphics_38,x:598.3,y:71.4}).wait(1).to({graphics:mask_3_graphics_39,x:598.3,y:71.4}).wait(1).to({graphics:mask_3_graphics_40,x:598.3,y:71.4}).wait(1).to({graphics:mask_3_graphics_41,x:598.3,y:71.4}).wait(1).to({graphics:mask_3_graphics_42,x:598.3,y:71.4}).wait(1).to({graphics:mask_3_graphics_43,x:600.5,y:71.5}).wait(1).to({graphics:mask_3_graphics_44,x:603.8,y:71.5}).wait(1).to({graphics:mask_3_graphics_45,x:607.9,y:71.5}).wait(1).to({graphics:mask_3_graphics_46,x:612.3,y:71.5}).wait(1).to({graphics:mask_3_graphics_47,x:612.3,y:71.5}).wait(1).to({graphics:mask_3_graphics_48,x:612.3,y:71.5}).wait(1).to({graphics:mask_3_graphics_49,x:612.3,y:71.5}).wait(1).to({graphics:mask_3_graphics_50,x:612.3,y:71.5}).wait(1).to({graphics:mask_3_graphics_51,x:612.3,y:71.5}).wait(1).to({graphics:mask_3_graphics_52,x:612.3,y:71.5}).wait(1).to({graphics:mask_3_graphics_53,x:612.3,y:71.5}).wait(1).to({graphics:mask_3_graphics_54,x:612.3,y:71.5}).wait(1).to({graphics:mask_3_graphics_55,x:612.3,y:71.5}).wait(1).to({graphics:mask_3_graphics_56,x:612.3,y:71.5}).wait(1).to({graphics:mask_3_graphics_57,x:612.3,y:71.5}).wait(1).to({graphics:mask_3_graphics_58,x:612.3,y:71.5}).wait(1).to({graphics:mask_3_graphics_59,x:612.3,y:71.5}).wait(1).to({graphics:mask_3_graphics_60,x:612.3,y:71.5}).wait(1).to({graphics:mask_3_graphics_61,x:612.3,y:71.5}).wait(1).to({graphics:mask_3_graphics_62,x:612.3,y:71.5}).wait(1).to({graphics:mask_3_graphics_63,x:612.3,y:70.8}).wait(1).to({graphics:mask_3_graphics_64,x:612.3,y:67.6}).wait(1).to({graphics:mask_3_graphics_65,x:612.3,y:65.1}).wait(1).to({graphics:mask_3_graphics_66,x:612.3,y:63.3}).wait(1).to({graphics:mask_3_graphics_67,x:612.3,y:62.2}).wait(1).to({graphics:mask_3_graphics_68,x:612.3,y:61.8}).wait(1).to({graphics:mask_3_graphics_69,x:612.3,y:61.8}).wait(1).to({graphics:mask_3_graphics_70,x:612.3,y:61.8}).wait(1).to({graphics:mask_3_graphics_71,x:612.3,y:61.8}).wait(1).to({graphics:mask_3_graphics_72,x:612.3,y:61.8}).wait(1).to({graphics:mask_3_graphics_73,x:612.3,y:61.8}).wait(1).to({graphics:mask_3_graphics_74,x:612.3,y:61.8}).wait(45).to({graphics:mask_3_graphics_119,x:612.3,y:61.8}).wait(1).to({graphics:mask_3_graphics_120,x:612.3,y:61.8}).wait(1).to({graphics:mask_3_graphics_121,x:612.3,y:61.8}).wait(1).to({graphics:mask_3_graphics_122,x:612.3,y:61.8}).wait(1).to({graphics:mask_3_graphics_123,x:612.3,y:61.8}).wait(1).to({graphics:mask_3_graphics_124,x:612.3,y:61.8}).wait(1).to({graphics:mask_3_graphics_125,x:612.3,y:61.8}).wait(1).to({graphics:mask_3_graphics_126,x:612.3,y:61.8}).wait(1).to({graphics:mask_3_graphics_127,x:612.3,y:61.8}).wait(1).to({graphics:mask_3_graphics_128,x:612.3,y:61.8}).wait(1).to({graphics:mask_3_graphics_129,x:612.3,y:61.8}).wait(1).to({graphics:mask_3_graphics_130,x:612.3,y:61.8}).wait(1).to({graphics:mask_3_graphics_131,x:612.3,y:61.8}).wait(1).to({graphics:mask_3_graphics_132,x:612.3,y:61.8}).wait(1).to({graphics:mask_3_graphics_133,x:612.3,y:61.8}).wait(1).to({graphics:mask_3_graphics_134,x:612.3,y:61.8}).wait(1));
  
    // R
    this.shape_3 = new cjs.Shape();
    this.shape_3.graphics.f("#fff").s().p("AB4JaIjgmiIg8AAIAAGiIkdAAIAAnvIBMAAIAAGkICFAAIAAmkIC1AAIDiGkICZAAIjcmFIgVglIAngRQBXgmA0hOQA0hPgBhfQABiEhdhdQhchdiDgBIm6AAIAAhNIG6AAIAaABQCTAKBpBsQBCBDAbBSQAVA9gBBGQAABAgUA6QgMAmgVAkQg1BahbA0IEHHTgAnBgFIAAhMIGXAAQA1AAApgkQAqgmAAgzQAAgzgqgmQgpglg1AAImXAAIAAhMIGXAAQBUgBBAA8QBAA8AABTQAABThAA7QhAA7hUAAg");
    this.shape_3.setTransform(612.3,61.8);
  
    this.instance_6 = new lib.Tween19("synched",0);
    this.instance_6.parent = this;
    this.instance_6.setTransform(612.3,61.8);
    this.instance_6._off = true;
  
    this.instance_7 = new lib.Tween20("synched",0);
    this.instance_7.parent = this;
    this.instance_7.setTransform(612.3,61.8);
    this.instance_7.alpha = 0;
  
    var maskedShapeInstanceList = [this.shape_3,this.instance_6,this.instance_7];
  
    for(var shapedInstanceItr = 0; shapedInstanceItr < maskedShapeInstanceList.length; shapedInstanceItr++) {
      maskedShapeInstanceList[shapedInstanceItr].mask = mask_3;
    }
  
    this.timeline.addTween(cjs.Tween.get({}).to({state:[]}).to({state:[{t:this.shape_3}]},1).to({state:[{t:this.instance_6}]},118).to({state:[{t:this.instance_7}]},15).wait(1));
    this.timeline.addTween(cjs.Tween.get(this.instance_6).wait(119).to({_off:false},0).to({_off:true,alpha:0},15).wait(1));
  
    // E mask (mask)
    var mask_4 = new cjs.Shape();
    mask_4._off = true;
    var mask_4_graphics_1 = new cjs.Graphics().p("AglAcIAAg3IBLAAIAAAzIAAAAIAAAEg");
    var mask_4_graphics_2 = new cjs.Graphics().p("AglA7IAAh1IBLAAIAAA0IAAAAIAABBg");
    var mask_4_graphics_3 = new cjs.Graphics().p("AglBhIAAjBIBLAAIAAA0IAAAAIAACNg");
    var mask_4_graphics_4 = new cjs.Graphics().p("AhGBhIAAjBIBLAAIAAB1IBCAAIAABMg");
    var mask_4_graphics_5 = new cjs.Graphics().p("AhpBhIAAjBIBMAAIAAB1ICHAAIAABMg");
    var mask_4_graphics_6 = new cjs.Graphics().p("AiOBhIAAjBIBMAAIAAB1IDRAAIAABMg");
    var mask_4_graphics_7 = new cjs.Graphics().p("AiyBhIAAjBIBLAAIABB1IEaAAIAABMg");
    var mask_4_graphics_8 = new cjs.Graphics().p("AjXBhIAAjBIBMAAIABB1IFiAAIAABMg");
    var mask_4_graphics_9 = new cjs.Graphics().p("Aj0BhIAAjBIBMAAIAAB1IGdAAIAABMg");
    var mask_4_graphics_10 = new cjs.Graphics().p("AkVBhIAAjBIBMAAIABB1IHeAAIAABMg");
    var mask_4_graphics_11 = new cjs.Graphics().p("Ak5BhIAAjBIBMAAIABB1IImAAIAABMg");
    var mask_4_graphics_12 = new cjs.Graphics().p("AlbBhIAAjBIBMAAIABB1IJpAAIAABMg");
    var mask_4_graphics_13 = new cjs.Graphics().p("Al9BhIAAjBIBMAAIABB1IKuAAIAABMg");
    var mask_4_graphics_14 = new cjs.Graphics().p("AmcBhIAAjBIBMAAIABB1ILsAAIAABMg");
    var mask_4_graphics_15 = new cjs.Graphics().p("AnBBhIAAjBIBMAAIAAB1IM3AAIAABMg");
    var mask_4_graphics_16 = new cjs.Graphics().p("AnBBhIAAjBIBMAAIAAB1ILrAAIAAg/IBMAAIAACLg");
    var mask_4_graphics_17 = new cjs.Graphics().p("AnBBhIAAjBIBMAAIAAB1ILrAAIAAh1IBMAAIAADBg");
    var mask_4_graphics_18 = new cjs.Graphics().p("AnBCHIAAjAIBMAAIAAB0ILrAAIAAh0IAAAAIAAhNIBMAAIAAENg");
    var mask_4_graphics_19 = new cjs.Graphics().p("AnBCHIAAjAIBMAAIAAB0ILrAAIAAh0Ig5AAIAAhNICFAAIAAENg");
    var mask_4_graphics_20 = new cjs.Graphics().p("AnBCHIAAjAIBMAAIAAB0ILrAAIAAh0Ih+AAIAAhNIDKAAIAAENg");
    var mask_4_graphics_21 = new cjs.Graphics().p("AnBCHIAAjAIBMAAIAAB0ILrAAIAAh0IjFAAIAAhNIERAAIAAENg");
    var mask_4_graphics_22 = new cjs.Graphics().p("AnBCHIAAjAIBMAAIAAB0ILrAAIAAh0IkMAAIAAhNIFYAAIAAENg");
    var mask_4_graphics_23 = new cjs.Graphics().p("AnBCHIAAjAIBMAAIAAB0ILrAAIAAh0IlPAAIAAhNIGbAAIAAENg");
    var mask_4_graphics_24 = new cjs.Graphics().p("AnBCHIAAjAIBMAAIAAB0ILrAAIAAh0ImRAAIAAhNIHdAAIAAENg");
    var mask_4_graphics_25 = new cjs.Graphics().p("AnBCHIAAjAIBMAAIAAB0ILrAAIAAh0InXAAIAAhNIIjAAIAAENg");
    var mask_4_graphics_26 = new cjs.Graphics().p("AnBCHIAAjAIBMAAIAAB0ILrAAIAAh0IodAAIAAhNIJpAAIAAENg");
    var mask_4_graphics_27 = new cjs.Graphics().p("AnBCHIAAjAIBMAAIAAB0ILrAAIAAh0IpmAAIAAhNIKyAAIAAENg");
    var mask_4_graphics_28 = new cjs.Graphics().p("AnBCHIAAjAIBMAAIAAB0ILrAAIAAh0IqrAAIAAhNIL3AAIAAENg");
    var mask_4_graphics_29 = new cjs.Graphics().p("AnBCHIAAjAIBLAAIAAhNIM4AAIAAENgAl1A7ILrAAIAAh0IrrAAg");
    var mask_4_graphics_30 = new cjs.Graphics().p("AnBCHIAAkNIODAAIAAENgAl1A7ILrAAIAAh0IrrAAg");
    var mask_4_graphics_31 = new cjs.Graphics().p("AnBEbIAAkOIODAAIAAEOgAl1DPILrAAIAAh1IrrAAgAnBjOIAAhMIBLAAIAABMg");
    var mask_4_graphics_32 = new cjs.Graphics().p("AnBEbIAAkOIODAAIAAEOgAl1DPILrAAIAAh1IrrAAgAnBjOIAAhMICWAAIAABMg");
    var mask_4_graphics_33 = new cjs.Graphics().p("AnBEbIAAkOIODAAIAAEOgAl1DPILrAAIAAh1IrrAAgAnBjOIAAhMIDVAAIAABMg");
    var mask_4_graphics_34 = new cjs.Graphics().p("AnBEbIAAkOIODAAIAAEOgAl1DPILrAAIAAh1IrrAAgAnBjOIAAhMIEeAAIAABMg");
    var mask_4_graphics_35 = new cjs.Graphics().p("AnBEbIAAkOIODAAIAAEOgAl1DPILrAAIAAh1IrrAAgAnBjOIAAhMIFlAAIAABMg");
    var mask_4_graphics_36 = new cjs.Graphics().p("AnBEbIAAkOIODAAIAAEOgAl1DPILrAAIAAh1IrrAAgAnBjOIAAhMIGpAAIAABMg");
    var mask_4_graphics_37 = new cjs.Graphics().p("AnBEbIAAkOIODAAIAAEOgAl1DPILrAAIAAh1IrrAAgAnBjOIAAhMIHsAAIAABMg");
    var mask_4_graphics_38 = new cjs.Graphics().p("AnBEbIAAkOIODAAIAAEOgAl1DPILrAAIAAh1IrrAAgAnBjOIAAhMIIuAAIAABMg");
    var mask_4_graphics_39 = new cjs.Graphics().p("AnBEbIAAkOIODAAIAAEOgAl1DPILrAAIAAh1IrrAAgAnBjOIAAhMIJuAAIAABMg");
    var mask_4_graphics_40 = new cjs.Graphics().p("AnBEcIAAkOIODAAIAAEOgAl1DQILrAAIAAh2IrrAAgAnBjNIAAhNIKsAAIAAgBIABAAIAABOg");
    var mask_4_graphics_41 = new cjs.Graphics().p("AnBEcIAAkOIODAAIAAEOgAl1DQILrAAIAAh2IrrAAgAnBjNIAAhNIKsAAIAAgBIBMAAIAABOg");
    var mask_4_graphics_42 = new cjs.Graphics().p("AnBFXIAAkOIODAAIAAEOgAl1ELILrAAIAAh1IrrAAgAnBiSIAAhMIKsAAIAAh2IgBAAIAAgCIBNAAIAADEg");
    var mask_4_graphics_43 = new cjs.Graphics().p("AnBF8IAAkOIODAAIAAEOgAl1EwILrAAIAAh1IrrAAgAnBhtIAAhNIKsAAIAAh1IgBAAIAAhMIBNAAIAAEOg");
    var mask_4_graphics_44 = new cjs.Graphics().p("AnBF8IAAkOIODAAIAAEOgAl1EwILrAAIAAh1IrrAAgAnBhtIAAhNIKsAAIAAh1IhDAAIAAhMICPAAIAAEOg");
    var mask_4_graphics_45 = new cjs.Graphics().p("AnBF8IAAkOIODAAIAAEOgAl1EwILrAAIAAh1IrrAAgAnBhtIAAhNIKsAAIAAh1IiAAAIAAhMIDMAAIAAEOg");
    var mask_4_graphics_46 = new cjs.Graphics().p("AnBF8IAAkOIODAAIAAEOgAl1EwILrAAIAAh1IrrAAgAnBhtIAAhNIKsAAIAAh1Ii8AAIAAhMIEIAAIAAEOg");
    var mask_4_graphics_47 = new cjs.Graphics().p("AnBF8IAAkOIODAAIAAEOgAl1EwILrAAIAAh1IrrAAgAnBhtIAAhNIKsAAIAAh1Ij9AAIAAhMIFJAAIAAEOg");
    var mask_4_graphics_48 = new cjs.Graphics().p("AnBF8IAAkOIODAAIAAEOgAl1EwILrAAIAAh1IrrAAgAnBhtIAAhNIKsAAIAAh1Ik8AAIAAhMIGIAAIAAEOg");
    var mask_4_graphics_49 = new cjs.Graphics().p("AnBF8IAAkOIODAAIAAEOgAl1EwILrAAIAAh1IrrAAgAnBhtIAAhNIKsAAIAAh1IlzAAIAAhMIG/AAIAAEOg");
    var mask_4_graphics_50 = new cjs.Graphics().p("AnBF8IAAkOIODAAIAAEOgAl1EwILrAAIAAh1IrrAAgAnBhtIAAhNIKsAAIAAh1ImwAAIAAhMIH8AAIAAEOg");
    var mask_4_graphics_51 = new cjs.Graphics().p("AnBF8IAAkOIODAAIAAEOgAl1EwILrAAIAAh1IrrAAgAnBhtIAAhNIKsAAIAAh1InyAAIAAhMII+AAIAAEOg");
    var mask_4_graphics_52 = new cjs.Graphics().p("AnBF8IAAkOIODAAIAAEOgAl1EwILrAAIAAh1IrrAAgAnBhtIAAhNIKsAAIAAh1IolAAIAAhMIJxAAIAAEOg");
    var mask_4_graphics_53 = new cjs.Graphics().p("AnBF9IAAkOIODAAIAAEOgAl1EwILrAAIAAh1IrrAAgAnBhtIAAhMIKsAAIAAh1IphAAIAAhOIABAAIAAABIKsAAIAAEOg");
    var mask_4_graphics_54 = new cjs.Graphics().p("AnBF9IAAkOIODAAIAAEOgAl1EwILrAAIAAh1IrrAAgAnBhtIAAhMIKsAAIAAh1IqsAAIAAhOIBMAAIAAABIKsAAIAAEOg");
    var mask_4_graphics_55 = new cjs.Graphics().p("AnBGvIAAkOIODAAIAAEOgAl1FjILrAAIAAh2IrrAAgAnBg6IAAhNIKsAAIAAh1IqsAAIAAiyIBMAAIAABmIKsAAIAAEOg");
    var mask_4_graphics_56 = new cjs.Graphics().p("AnBHTIAAkOIODAAIAAEOgAl1GHILrAAIAAh2IrrAAgAnBgWIAAhNIKsAAIAAh1IqsAAIAAj6IBMAAIAACuIKsAAIAAEOg");
    var mask_4_graphics_57 = new cjs.Graphics().p("AnBH5IAAkOIODAAIAAEOgAl1GtILrAAIAAh1IrrAAgAnBAPIAAhLIKsAAIAAh2IqsAAIAAlGIBNAAIAABMIgBAAIAACuIKsAAIAAENg");
    var mask_4_graphics_58 = new cjs.Graphics().p("AnBH5IAAkOIODAAIAAEOgAl1GtILrAAIAAh1IrrAAgAnBAPIAAhLIKsAAIAAh2IqsAAIAAlGIB6AAIAABMIguAAIAACuIKsAAIAAENg");
    var mask_4_graphics_59 = new cjs.Graphics().p("AnBH5IAAkOIODAAIAAEOgAl1GtILrAAIAAh1IrrAAgAnBAPIAAhLIKsAAIAAh2IqsAAIAAlGIC4AAIAABMIhsAAIAACuIKsAAIAAENg");
    var mask_4_graphics_60 = new cjs.Graphics().p("AnBH5IAAkOIODAAIAAEOgAl1GtILrAAIAAh1IrrAAgAnBAPIAAhLIKsAAIAAh2IqsAAIAAlGID8AAIAABMIiwAAIAACuIKsAAIAAENg");
    var mask_4_graphics_61 = new cjs.Graphics().p("AnBH5IAAkOIODAAIAAEOgAl1GtILrAAIAAh1IrrAAgAnBAPIAAhLIKsAAIAAh2IqsAAIAAlGIFFAAIAABMIj5AAIAACuIKsAAIAAENg");
    var mask_4_graphics_62 = new cjs.Graphics().p("AnBH5IAAkOIODAAIAAEOgAl1GtILrAAIAAh1IrrAAgAnBAPIAAhLIKsAAIAAh2IqsAAIAAlGIGPAAIAABMIlDAAIAACuIKsAAIAAENg");
    var mask_4_graphics_63 = new cjs.Graphics().p("AnBH5IAAkOIODAAIAAEOgAl1GtILrAAIAAh1IrrAAgAnBAPIAAhLIKsAAIAAh2IqsAAIAAlGIHYAAIAABMImMAAIAACuIKsAAIAAENg");
    var mask_4_graphics_64 = new cjs.Graphics().p("AnBH5IAAkOIODAAIAAEOgAl1GtILrAAIAAh1IrrAAgAnBAPIAAhLIKsAAIAAh2IqsAAIAAlGIIeAAIAABMInSAAIAACuIKsAAIAAENg");
    var mask_4_graphics_65 = new cjs.Graphics().p("AnBH5IAAkOIODAAIAAEOgAl1GtILrAAIAAh1IrrAAgAnBAPIAAhLIKsAAIAAh2IqsAAIAAlGIJkAAIAABMIoYAAIAACuIKsAAIAAENg");
    var mask_4_graphics_66 = new cjs.Graphics().p("AnBH5IAAkOIODAAIAAEOgAl1GtILrAAIAAh1IrrAAgAnBAPIAAhLIKsAAIAAh2IqsAAIAAlGIKnAAIAABMIpbAAIAACuIKsAAIAAENg");
    var mask_4_graphics_67 = new cjs.Graphics().p("AnBH5IAAkOIODAAIAAEOgAl1GtILrAAIAAh1IrrAAgAnBAPIAAhLIKsAAIAAh2IqsAAIAAlGILpAAIAABMIqdAAIAACuIKsAAIAAENg");
    var mask_4_graphics_68 = new cjs.Graphics().p("AnBH6IAAkOIODAAIAAEOgAl1GuILrAAIAAh1IrrAAgAnBAQIAAhMIKsAAIAAh1IqsAAIAAlGIMpAAIAAgCIABAAIAABNIreAAIAACvIKsAAIAAENg");
    var mask_4_graphics_69 = new cjs.Graphics().p("AnBH6IAAkOIODAAIAAEOgAl1GuILrAAIAAh1IrrAAgAnBAQIAAhMIKsAAIAAh1IqsAAIAAlGIMpAAIAAgCIBLAAIAABNIsoAAIAACvIKsAAIAAENg");
    var mask_4_graphics_70 = new cjs.Graphics().p("AnBI1IAAkOIODAAIAAEOgAl1HpILrAAIAAh2IrrAAgAnBBLIAAhMIKsAAIAAh1IqsAAIAAlHIMpAAIAAhIIABAAIAAgvIBKAAIAADDIsoAAIAACvIKsAAIAAENg");
    var mask_4_graphics_71 = new cjs.Graphics().p("AnBJaIAAkOIODAAIAAEOgAl1IOILrAAIAAh1IrrAAgAnBBwIAAhMIKsAAIAAh1IqsAAIAAlGIMpAAIAAhJIABAAIAAh5IBKAAIAAEOIsoAAIAACuIKsAAIAAENg");
    var mask_4_graphics_72 = new cjs.Graphics().p("AnBJaIAAkOIODAAIAAEOgAl1IOILrAAIAAh1IrrAAgAnBBwIAAhMIKsAAIAAh1IqsAAIAAlGIMpAAIAAh2IguAAIAAhMIB5AAIAAEOIsoAAIAACuIKsAAIAAENg");
    var mask_4_graphics_73 = new cjs.Graphics().p("AnBJaIAAkOIODAAIAAEOgAl1IOILrAAIAAh1IrrAAgAnBBwIAAhMIKsAAIAAh1IqsAAIAAlGIMpAAIAAh2IhjAAIAAhMICuAAIAAEOIsoAAIAACuIKsAAIAAENg");
    var mask_4_graphics_74 = new cjs.Graphics().p("AnBJaIAAkOIODAAIAAEOgAl1IOILrAAIAAh1IrrAAgAnBBwIAAhMIKsAAIAAh1IqsAAIAAlGIMpAAIAAh2IieAAIAAhMIDpAAIAAEOIsoAAIAACuIKsAAIAAENg");
    var mask_4_graphics_75 = new cjs.Graphics().p("AnBJaIAAkOIODAAIAAEOgAl1IOILrAAIAAh1IrrAAgAnBBwIAAhMIKsAAIAAh1IqsAAIAAlGIMpAAIAAh2IjwAAIAAhMIE7AAIAAEOIsoAAIAACuIKsAAIAAENg");
    var mask_4_graphics_76 = new cjs.Graphics().p("AnBJaIAAkOIODAAIAAEOgAl1IOILrAAIAAh1IrrAAgAnBBwIAAhMIKsAAIAAh1IqsAAIAAlGIMpAAIAAh2Ik1AAIAAhMIGAAAIAAEOIsoAAIAACuIKsAAIAAENg");
    var mask_4_graphics_77 = new cjs.Graphics().p("AnBJaIAAkOIODAAIAAEOgAl1IOILrAAIAAh1IrrAAgAnBBwIAAhMIKsAAIAAh1IqsAAIAAlGIMpAAIAAh2ImBAAIAAhMIHMAAIAAEOIsoAAIAACuIKsAAIAAENg");
    var mask_4_graphics_78 = new cjs.Graphics().p("AnBJaIAAkOIODAAIAAEOgAl1IOILrAAIAAh1IrrAAgAnBBwIAAhMIKsAAIAAh1IqsAAIAAlGIMpAAIAAh2Im+AAIAAhMIIJAAIAAEOIsoAAIAACuIKsAAIAAENg");
    var mask_4_graphics_79 = new cjs.Graphics().p("AnBJaIAAkOIODAAIAAEOgAl1IOILrAAIAAh1IrrAAgAnBBwIAAhMIKsAAIAAh1IqsAAIAAlGIMpAAIAAh2IoIAAIAAhMIJTAAIAAEOIsoAAIAACuIKsAAIAAENg");
    var mask_4_graphics_80 = new cjs.Graphics().p("AnBJaIAAkOIODAAIAAEOgAl1IOILrAAIAAh1IrrAAgAnBBwIAAhMIKsAAIAAh1IqsAAIAAlGIMpAAIAAh2IpNAAIAAhMIKYAAIAAEOIsoAAIAACuIKsAAIAAENg");
    var mask_4_graphics_81 = new cjs.Graphics().p("AnBJaIAAkOIODAAIAAEOgAl1IOILrAAIAAh1IrrAAgAnBBwIAAhMIKsAAIAAh1IqsAAIAAlGIMpAAIAAh2IqVAAIAAhMILgAAIAAEOIsoAAIAACuIKsAAIAAENg");
    var mask_4_graphics_82 = new cjs.Graphics().p("AnBJaIAAkOIODAAIAAEOgAl1IOILrAAIAAh1IrrAAgAnBBwIAAhMIKsAAIAAh1IqsAAIAAlGIMpAAIAAh2IrgAAIAAhMIMrAAIAAEOIsoAAIAACuIKsAAIAAENg");
    var mask_4_graphics_83 = new cjs.Graphics().p("AnBJaIAAkOIODAAIAAEOgAl1IOILrAAIAAh1IrrAAgAnBBwIAAhMIKsAAIAAh1IqsAAIAAlGIMpAAIAAh2IspAAIAAhMIN0AAIAAEOIsoAAIAACuIKsAAIAAENg");
    var mask_4_graphics_119 = new cjs.Graphics().p("AnBJaIAAkOIODAAIAAEOgAl1IOILrAAIAAh1IrrAAgAnBBwIAAhMIKsAAIAAh1IqsAAIAAlGIMpAAIAAh2IspAAIAAhMIN0AAIAAEOIsoAAIAACuIKsAAIAAENg");
    var mask_4_graphics_120 = new cjs.Graphics().p("AnBJaIAAkOIODAAIAAEOgAl1IOILrAAIAAh1IrrAAgAnBBwIAAhMIKsAAIAAh1IqsAAIAAlGIMpAAIAAh2IspAAIAAhMIN0AAIAAEOIsoAAIAACuIKsAAIAAENg");
    var mask_4_graphics_121 = new cjs.Graphics().p("AnBJaIAAkOIODAAIAAEOgAl1IOILrAAIAAh1IrrAAgAnBBwIAAhMIKsAAIAAh1IqsAAIAAlGIMpAAIAAh2IspAAIAAhMIN0AAIAAEOIsoAAIAACuIKsAAIAAENg");
    var mask_4_graphics_122 = new cjs.Graphics().p("AnBJaIAAkOIODAAIAAEOgAl1IOILrAAIAAh1IrrAAgAnBBwIAAhMIKsAAIAAh1IqsAAIAAlGIMpAAIAAh2IspAAIAAhMIN0AAIAAEOIsoAAIAACuIKsAAIAAENg");
    var mask_4_graphics_123 = new cjs.Graphics().p("AnBJaIAAkOIODAAIAAEOgAl1IOILrAAIAAh1IrrAAgAnBBwIAAhMIKsAAIAAh1IqsAAIAAlGIMpAAIAAh2IspAAIAAhMIN0AAIAAEOIsoAAIAACuIKsAAIAAENg");
    var mask_4_graphics_124 = new cjs.Graphics().p("AnBJaIAAkOIODAAIAAEOgAl1IOILrAAIAAh1IrrAAgAnBBwIAAhMIKsAAIAAh1IqsAAIAAlGIMpAAIAAh2IspAAIAAhMIN0AAIAAEOIsoAAIAACuIKsAAIAAENg");
    var mask_4_graphics_125 = new cjs.Graphics().p("AnBJaIAAkOIODAAIAAEOgAl1IOILrAAIAAh1IrrAAgAnBBwIAAhMIKsAAIAAh1IqsAAIAAlGIMpAAIAAh2IspAAIAAhMIN0AAIAAEOIsoAAIAACuIKsAAIAAENg");
    var mask_4_graphics_126 = new cjs.Graphics().p("AnBJaIAAkOIODAAIAAEOgAl1IOILrAAIAAh1IrrAAgAnBBwIAAhMIKsAAIAAh1IqsAAIAAlGIMpAAIAAh2IspAAIAAhMIN0AAIAAEOIsoAAIAACuIKsAAIAAENg");
    var mask_4_graphics_127 = new cjs.Graphics().p("AnBJaIAAkOIODAAIAAEOgAl1IOILrAAIAAh1IrrAAgAnBBwIAAhMIKsAAIAAh1IqsAAIAAlGIMpAAIAAh2IspAAIAAhMIN0AAIAAEOIsoAAIAACuIKsAAIAAENg");
    var mask_4_graphics_128 = new cjs.Graphics().p("AnBJaIAAkOIODAAIAAEOgAl1IOILrAAIAAh1IrrAAgAnBBwIAAhMIKsAAIAAh1IqsAAIAAlGIMpAAIAAh2IspAAIAAhMIN0AAIAAEOIsoAAIAACuIKsAAIAAENg");
    var mask_4_graphics_129 = new cjs.Graphics().p("AnBJaIAAkOIODAAIAAEOgAl1IOILrAAIAAh1IrrAAgAnBBwIAAhMIKsAAIAAh1IqsAAIAAlGIMpAAIAAh2IspAAIAAhMIN0AAIAAEOIsoAAIAACuIKsAAIAAENg");
    var mask_4_graphics_130 = new cjs.Graphics().p("AnBJaIAAkOIODAAIAAEOgAl1IOILrAAIAAh1IrrAAgAnBBwIAAhMIKsAAIAAh1IqsAAIAAlGIMpAAIAAh2IspAAIAAhMIN0AAIAAEOIsoAAIAACuIKsAAIAAENg");
    var mask_4_graphics_131 = new cjs.Graphics().p("AnBJaIAAkOIODAAIAAEOgAl1IOILrAAIAAh1IrrAAgAnBBwIAAhMIKsAAIAAh1IqsAAIAAlGIMpAAIAAh2IspAAIAAhMIN0AAIAAEOIsoAAIAACuIKsAAIAAENg");
    var mask_4_graphics_132 = new cjs.Graphics().p("AnBJaIAAkOIODAAIAAEOgAl1IOILrAAIAAh1IrrAAgAnBBwIAAhMIKsAAIAAh1IqsAAIAAlGIMpAAIAAh2IspAAIAAhMIN0AAIAAEOIsoAAIAACuIKsAAIAAENg");
    var mask_4_graphics_133 = new cjs.Graphics().p("AnBJaIAAkOIODAAIAAEOgAl1IOILrAAIAAh1IrrAAgAnBBwIAAhMIKsAAIAAh1IqsAAIAAlGIMpAAIAAh2IspAAIAAhMIN0AAIAAEOIsoAAIAACuIKsAAIAAENg");
    var mask_4_graphics_134 = new cjs.Graphics().p("AnBJaIAAkOIODAAIAAEOgAl1IOILrAAIAAh1IrrAAgAnBBwIAAhMIKsAAIAAh1IqsAAIAAlGIMpAAIAAh2IspAAIAAhMIN0AAIAAEOIsoAAIAACuIKsAAIAAENg");
  
    this.timeline.addTween(cjs.Tween.get(mask_4).to({graphics:null,x:0,y:0}).wait(1).to({graphics:mask_4_graphics_1,x:462.8,y:105.4}).wait(1).to({graphics:mask_4_graphics_2,x:462.8,y:108.6}).wait(1).to({graphics:mask_4_graphics_3,x:462.8,y:112.3}).wait(1).to({graphics:mask_4_graphics_4,x:466.1,y:112.3}).wait(1).to({graphics:mask_4_graphics_5,x:469.6,y:112.3}).wait(1).to({graphics:mask_4_graphics_6,x:473.3,y:112.3}).wait(1).to({graphics:mask_4_graphics_7,x:477,y:112.3}).wait(1).to({graphics:mask_4_graphics_8,x:480.6,y:112.3}).wait(1).to({graphics:mask_4_graphics_9,x:483.5,y:112.3}).wait(1).to({graphics:mask_4_graphics_10,x:486.8,y:112.3}).wait(1).to({graphics:mask_4_graphics_11,x:490.4,y:112.3}).wait(1).to({graphics:mask_4_graphics_12,x:493.8,y:112.3}).wait(1).to({graphics:mask_4_graphics_13,x:497.2,y:112.3}).wait(1).to({graphics:mask_4_graphics_14,x:500.3,y:112.3}).wait(1).to({graphics:mask_4_graphics_15,x:504,y:112.3}).wait(1).to({graphics:mask_4_graphics_16,x:504,y:112.3}).wait(1).to({graphics:mask_4_graphics_17,x:504,y:112.3}).wait(1).to({graphics:mask_4_graphics_18,x:504,y:108.5}).wait(1).to({graphics:mask_4_graphics_19,x:504,y:108.5}).wait(1).to({graphics:mask_4_graphics_20,x:504,y:108.5}).wait(1).to({graphics:mask_4_graphics_21,x:504,y:108.5}).wait(1).to({graphics:mask_4_graphics_22,x:504,y:108.5}).wait(1).to({graphics:mask_4_graphics_23,x:504,y:108.5}).wait(1).to({graphics:mask_4_graphics_24,x:504,y:108.5}).wait(1).to({graphics:mask_4_graphics_25,x:504,y:108.5}).wait(1).to({graphics:mask_4_graphics_26,x:504,y:108.5}).wait(1).to({graphics:mask_4_graphics_27,x:504,y:108.5}).wait(1).to({graphics:mask_4_graphics_28,x:504,y:108.5}).wait(1).to({graphics:mask_4_graphics_29,x:504,y:108.5}).wait(1).to({graphics:mask_4_graphics_30,x:504,y:108.5}).wait(1).to({graphics:mask_4_graphics_31,x:504,y:93.7}).wait(1).to({graphics:mask_4_graphics_32,x:504,y:93.7}).wait(1).to({graphics:mask_4_graphics_33,x:504,y:93.7}).wait(1).to({graphics:mask_4_graphics_34,x:504,y:93.7}).wait(1).to({graphics:mask_4_graphics_35,x:504,y:93.7}).wait(1).to({graphics:mask_4_graphics_36,x:504,y:93.7}).wait(1).to({graphics:mask_4_graphics_37,x:504,y:93.7}).wait(1).to({graphics:mask_4_graphics_38,x:504,y:93.7}).wait(1).to({graphics:mask_4_graphics_39,x:504,y:93.7}).wait(1).to({graphics:mask_4_graphics_40,x:504,y:93.6}).wait(1).to({graphics:mask_4_graphics_41,x:504,y:93.6}).wait(1).to({graphics:mask_4_graphics_42,x:504,y:87.7}).wait(1).to({graphics:mask_4_graphics_43,x:504,y:84}).wait(1).to({graphics:mask_4_graphics_44,x:504,y:84}).wait(1).to({graphics:mask_4_graphics_45,x:504,y:84}).wait(1).to({graphics:mask_4_graphics_46,x:504,y:84}).wait(1).to({graphics:mask_4_graphics_47,x:504,y:84}).wait(1).to({graphics:mask_4_graphics_48,x:504,y:84}).wait(1).to({graphics:mask_4_graphics_49,x:504,y:84}).wait(1).to({graphics:mask_4_graphics_50,x:504,y:84}).wait(1).to({graphics:mask_4_graphics_51,x:504,y:84}).wait(1).to({graphics:mask_4_graphics_52,x:504,y:84}).wait(1).to({graphics:mask_4_graphics_53,x:504,y:84}).wait(1).to({graphics:mask_4_graphics_54,x:504,y:84}).wait(1).to({graphics:mask_4_graphics_55,x:504,y:78.9}).wait(1).to({graphics:mask_4_graphics_56,x:504,y:75.3}).wait(1).to({graphics:mask_4_graphics_57,x:504,y:71.5}).wait(1).to({graphics:mask_4_graphics_58,x:504,y:71.5}).wait(1).to({graphics:mask_4_graphics_59,x:504,y:71.5}).wait(1).to({graphics:mask_4_graphics_60,x:504,y:71.5}).wait(1).to({graphics:mask_4_graphics_61,x:504,y:71.5}).wait(1).to({graphics:mask_4_graphics_62,x:504,y:71.5}).wait(1).to({graphics:mask_4_graphics_63,x:504,y:71.5}).wait(1).to({graphics:mask_4_graphics_64,x:504,y:71.5}).wait(1).to({graphics:mask_4_graphics_65,x:504,y:71.5}).wait(1).to({graphics:mask_4_graphics_66,x:504,y:71.5}).wait(1).to({graphics:mask_4_graphics_67,x:504,y:71.5}).wait(1).to({graphics:mask_4_graphics_68,x:504,y:71.4}).wait(1).to({graphics:mask_4_graphics_69,x:504,y:71.4}).wait(1).to({graphics:mask_4_graphics_70,x:504,y:65.5}).wait(1).to({graphics:mask_4_graphics_71,x:504,y:61.8}).wait(1).to({graphics:mask_4_graphics_72,x:504,y:61.8}).wait(1).to({graphics:mask_4_graphics_73,x:504,y:61.8}).wait(1).to({graphics:mask_4_graphics_74,x:504,y:61.8}).wait(1).to({graphics:mask_4_graphics_75,x:504,y:61.8}).wait(1).to({graphics:mask_4_graphics_76,x:504,y:61.8}).wait(1).to({graphics:mask_4_graphics_77,x:504,y:61.8}).wait(1).to({graphics:mask_4_graphics_78,x:504,y:61.8}).wait(1).to({graphics:mask_4_graphics_79,x:504,y:61.8}).wait(1).to({graphics:mask_4_graphics_80,x:504,y:61.8}).wait(1).to({graphics:mask_4_graphics_81,x:504,y:61.8}).wait(1).to({graphics:mask_4_graphics_82,x:504,y:61.8}).wait(1).to({graphics:mask_4_graphics_83,x:504,y:61.8}).wait(36).to({graphics:mask_4_graphics_119,x:504,y:61.8}).wait(1).to({graphics:mask_4_graphics_120,x:504,y:61.8}).wait(1).to({graphics:mask_4_graphics_121,x:504,y:61.8}).wait(1).to({graphics:mask_4_graphics_122,x:504,y:61.8}).wait(1).to({graphics:mask_4_graphics_123,x:504,y:61.8}).wait(1).to({graphics:mask_4_graphics_124,x:504,y:61.8}).wait(1).to({graphics:mask_4_graphics_125,x:504,y:61.8}).wait(1).to({graphics:mask_4_graphics_126,x:504,y:61.8}).wait(1).to({graphics:mask_4_graphics_127,x:504,y:61.8}).wait(1).to({graphics:mask_4_graphics_128,x:504,y:61.8}).wait(1).to({graphics:mask_4_graphics_129,x:504,y:61.8}).wait(1).to({graphics:mask_4_graphics_130,x:504,y:61.8}).wait(1).to({graphics:mask_4_graphics_131,x:504,y:61.8}).wait(1).to({graphics:mask_4_graphics_132,x:504,y:61.8}).wait(1).to({graphics:mask_4_graphics_133,x:504,y:61.8}).wait(1).to({graphics:mask_4_graphics_134,x:504,y:61.8}).wait(1));
  
    // E
    this.shape_4 = new cjs.Shape();
    this.shape_4.graphics.f("#fff").s().p("AnBJaIAAkOIODAAIAAEOgAl1IOILrAAIAAh1IrrAAgAnBBwIAAhMIKsAAIAAh1IqsAAIAAlGIMpAAIAAh2IspAAIAAhMIN0AAIAAEOIsoAAIAACuIKsAAIAAENg");
    this.shape_4.setTransform(504,61.8);
  
    this.instance_8 = new lib.Tween23("synched",0);
    this.instance_8.parent = this;
    this.instance_8.setTransform(504,61.8);
    this.instance_8._off = true;
  
    this.instance_9 = new lib.Tween24("synched",0);
    this.instance_9.parent = this;
    this.instance_9.setTransform(504,61.8);
    this.instance_9.alpha = 0;
  
    var maskedShapeInstanceList = [this.shape_4,this.instance_8,this.instance_9];
  
    for(var shapedInstanceItr = 0; shapedInstanceItr < maskedShapeInstanceList.length; shapedInstanceItr++) {
      maskedShapeInstanceList[shapedInstanceItr].mask = mask_4;
    }
  
    this.timeline.addTween(cjs.Tween.get({}).to({state:[]}).to({state:[{t:this.shape_4}]},1).to({state:[{t:this.instance_8}]},118).to({state:[{t:this.instance_9}]},15).wait(1));
    this.timeline.addTween(cjs.Tween.get(this.instance_8).wait(119).to({_off:false},0).to({_off:true,alpha:0},15).wait(1));
  
    // D mask (mask)
    var mask_5 = new cjs.Shape();
    mask_5._off = true;
    var mask_5_graphics_1 = new cjs.Graphics().p("AgcAmIAAhLIA5AAIAABLg");
    var mask_5_graphics_2 = new cjs.Graphics().p("Ag7AmIAAhLIB3AAIgDBLg");
    var mask_5_graphics_3 = new cjs.Graphics().p("AhYAmIAAhLICxAAIgFBLg");
    var mask_5_graphics_4 = new cjs.Graphics().p("ABEAmIi+AAIAAhLIC+AAQAcAAAbADIglBIIgSAAg");
    var mask_5_graphics_5 = new cjs.Graphics().p("AAaAiIi9AAIAAhLIC9AAQBKAABAATIg4BAQgngIgrAAg");
    var mask_5_graphics_6 = new cjs.Graphics().p("ABjApQg3gQg+AAIi/AAIAAhKIC/AAQCAgBBjA3IhRAtIgdgJg");
    var mask_5_graphics_7 = new cjs.Graphics().p("ABEAaQg3gQg/AAIi+AAIAAhLIC+AAQCkAAB2BaIAJAIIhSAhQgqgagxgOg");
    var mask_5_graphics_8 = new cjs.Graphics().p("ACiBNQhihNiOAAIi+AAIAAhMIC+AAQC6AAB+B0QATASAQATg");
    var mask_5_graphics_9 = new cjs.Graphics().p("ADMBvIgKgNQgQgVgUgSQhnhdicAAIi+AAIAAhMIC+AAQC6AAB+B0QAtAqAdAyIAHANg");
    var mask_5_graphics_10 = new cjs.Graphics().p("ACSAdQhmhdidAAIi+AAIAAhMIC+AAQC7AAB9B1QBKBEAfBZIhPAHQgZg/g2gxg");
    var mask_5_graphics_11 = new cjs.Graphics().p("ADpCmQgUhfhKhEQhnhdicAAIi+AAIAAhMIC+AAQC6AAB+B1QBhBaAWB+g");
    var mask_5_graphics_12 = new cjs.Graphics().p("ADtCzQgLh6hahSQhnheicAAIi+AAIAAhMIC+AAQC6AAB+B1QB3BuAGCkg");
    var mask_5_graphics_13 = new cjs.Graphics().p("ADtDKIABgLIAAgNQAAiRhmhdQhnheicAAIi+AAIAAhMIC+AAQC6AAB+B1QB9B0AACvQAAATgBASIgCAQg");
    var mask_5_graphics_14 = new cjs.Graphics().p("ADmDeQAHgeABghIAAgOQAAiQhmhdQhnheicgBIi+AAIAAhLIC+AAQC6gBB+B2QB9B0AACuQAAATgBATQgEAqgLAmg");
    var mask_5_graphics_15 = new cjs.Graphics().p("ADOEAIAAgBQAghBAAhRQAAiQhmheQhnheicAAIi+AAIAAhMIC+AAQC6AAB+B1QB9B1AACuQAABqgvBVg");
    var mask_5_graphics_16 = new cjs.Graphics().p("ADODVQAghBAAhRQAAiQhmheQhnheicAAIi+AAIAAhMIC+AAQC6AAB+B1QB9B1AACuQAACihsBxg");
    var mask_5_graphics_17 = new cjs.Graphics().p("ACfEFQBPhYAAh/QAAiQhmheQhnheicAAIi+AAIAAhMIC+AAQC6AAB+B1QB9B1AACuQAACuh9B1QgPAOgPAMg");
    var mask_5_graphics_18 = new cjs.Graphics().p("ABlElQASgNARgPQBmheAAiQQAAiQhmheQhnheicAAIi+AAIAAhMIC+AAQC6AAB+B1QB9B1AACuQAACuh9B1QgpAmgvAag");
    var mask_5_graphics_19 = new cjs.Graphics().p("AAhE8QA5gYAugqQBmheAAiQQAAiPhmheQhnheicgBIi+AAIAAhLIC+AAQC6gBB+B2QB9B0AACuQAACvh9B1QhFBAhXAdg");
    var mask_5_graphics_20 = new cjs.Graphics().p("AgYFGQBbgVBFg/QBmhdAAiQQAAiQhmheQhnheicAAIi+AAIAAhMIC+AAQC6AAB+B1QB9B1AACuQAACuh9B1QhbBVh6AXg");
    var mask_5_graphics_21 = new cjs.Graphics().p("AhYFLIAGAAIAFgBQB+gMBXhQQBmhdAAiQQAAiQhmheQhnheicAAIi+AAIAAhMIC+AAQC6AAB+B1QB9B1AACuQAACuh9B1QhuBmicANIgFABIgGAAg");
    var mask_5_graphics_22 = new cjs.Graphics().p("AieGZIAAhNIAjAAQCcAABnheQBmheAAiQQAAiPhmheQhnheicgBIi+AAIAAhLIC+AAQC6gBB+B2QB9B0AACuQAACvh9B1Qh+B0i6ABg");
    var mask_5_graphics_23 = new cjs.Graphics().p("AjlGZIAAhNIBqAAQCcAABnheQBmheAAiQQAAiPhmheQhnheicgBIi+AAIAAhLIC+AAQC6gBB+B2QB9B0AACuQAACvh9B1Qh+B0i6ABg");
    var mask_5_graphics_24 = new cjs.Graphics().p("Ak5GZIAAhNIAEAAIAAAAIC6AAQCcAABnheQBmheAAiQQAAiPhmheQhnheicgBIi+AAIAAhLIC+AAQC6gBB+B2QB9B0AACuQAACvh9B1Qh+B0i6ABg");
    var mask_5_graphics_25 = new cjs.Graphics().p("AlgGZIAAhNIBSAAIAAAAIC6AAQCcAABmheQBnheAAiQQAAiPhnheQhmheicgBIi/AAIAAhLIC/AAQC6gBB9B2QB+B0AACuQAACvh+B1Qh9B0i6ABg");
    var mask_5_graphics_26 = new cjs.Graphics().p("AlgGZIAAh0IBMAAIAAAnIDAAAQCcAABmheQBnheAAiQQAAiPhnheQhmheicgBIi/AAIAAhLIC/AAQC6gBB9B2QB+B0AACuQAACvh+B1Qh9B0i6ABg");
    var mask_5_graphics_27 = new cjs.Graphics().p("AlgGZIAAilIBMAAIAABYIDAAAQCcAABmheQBnheAAiQQAAiPhnheQhmheicgBIi/AAIAAhLIC/AAQC6gBB9B2QB+B0AACuQAACvh+B1Qh9B0i6ABg");
    var mask_5_graphics_28 = new cjs.Graphics().p("AlgGZIAAjkIBMAAIAACXIDAAAQCcAABmheQBnheAAiQQAAiPhnheQhmheicgBIi/AAIAAhLIC/AAQC6gBB9B2QB+B0AACuQAACvh+B1Qh9B0i6ABg");
    var mask_5_graphics_29 = new cjs.Graphics().p("AlgGZIAAkiIBMAAIAADVIDAAAQCcAABmheQBnheAAiQQAAiPhnheQhmheicgBIi/AAIAAhLIC/AAQC6gBB9B2QB+B0AACuQAACvh+B1Qh9B0i6ABg");
    var mask_5_graphics_30 = new cjs.Graphics().p("AlgGZIAAldIBMAAIAAEQIDAAAQCcAABmheQBnheAAiQQAAiPhnheQhmheicgBIi/AAIAAhLIC/AAQC6gBB9B2QB+B0AACuQAACvh+B1Qh9B0i6ABg");
    var mask_5_graphics_31 = new cjs.Graphics().p("AlgGZIAAmZIBMAAIAAFMIDAAAQCcAABmheQBnheAAiQQAAiPhnheQhmheicgBIi/AAIAAhLIC/AAQC6gBB9B2QB+B0AACuQAACvh+B1Qh9B0i6ABg");
    var mask_5_graphics_32 = new cjs.Graphics().p("AlgGZIAAncIBMAAIAAGPIDAAAQCcAABmheQBnheAAiQQAAiPhnheQhmheicgBIi/AAIAAhLIC/AAQC6gBB9B2QB+B0AACuQAACvh+B1Qh9B0i6ABg");
    var mask_5_graphics_33 = new cjs.Graphics().p("AlgGZIAAogIBMAAIAAHTIDAAAQCcAABmheQBnheAAiQQAAiPhnheQhmheicgBIi/AAIAAhLIC/AAQC6gBB9B2QB+B0AACuQAACvh+B1Qh9B0i6ABg");
    var mask_5_graphics_34 = new cjs.Graphics().p("AlgGZIAApiIBMAAIAAIVIDAAAQCcAABmheQBnheAAiQQAAiPhnheQhmheicgBIi/AAIAAhLIC/AAQC6gBB9B2QB+B0AACuQAACvh+B1Qh9B0i6ABg");
    var mask_5_graphics_35 = new cjs.Graphics().p("AlgGZIAAqeIBMAAIAAJRIDAAAQCcAABmheQBnheAAiQQAAiPhnheQhmheicgBIi/AAIAAhLIC/AAQC6gBB9B2QB+B0AACuQAACvh+B1Qh9B0i6ABg");
    var mask_5_graphics_36 = new cjs.Graphics().p("AlgGZIAArkIBMAAIAAKXIDAAAQCcAABmheQBnheAAiQQAAiPhnheQhmheicgBIi/AAIAAhLIC/AAQC6gBB9B2QB+B0AACuQAACvh+B1Qh9B0i6ABg");
    var mask_5_graphics_37 = new cjs.Graphics().p("AlgGZIAAswIEMAAQC6gBB9B2QB+B0AACuQAACvh+B1Qh9B0i6ABgAkUFMIDAAAQCcAABmheQBnheAAiQQAAiPhnheQhmheicgBIjAAAg");
    var mask_5_graphics_38 = new cjs.Graphics().p("AlgIBIAAswIEMAAQC6AAB9B1QB+B1AACuQAACuh+B1Qh9B1i6AAgAkUG1IDAAAQCcAABmheQBnheAAiQQAAiQhnheQhmheicAAIjAAAgAlgm0IAAhMIA2AAIAABMg");
    var mask_5_graphics_39 = new cjs.Graphics().p("AlgIBIAAswIEMAAQC6AAB9B1QB+B1AACuQAACuh+B1Qh9B1i6AAgAkUG1IDAAAQCcAABmheQBnheAAiQQAAiQhnheQhmheicAAIjAAAgAlgm0IAAhMIB6AAIAABMg");
    var mask_5_graphics_40 = new cjs.Graphics().p("AlgIBIAAswIEMAAQC6AAB9B1QB+B1AACuQAACuh+B1Qh9B1i6AAgAkUG1IDAAAQCcAABmheQBnheAAiQQAAiQhnheQhmheicAAIjAAAgAlgm0IAAhMIDSAAIAABMg");
    var mask_5_graphics_41 = new cjs.Graphics().p("AlgIBIAAswIEMAAQC6AAB9B1QB+B1AACuQAACuh+B1Qh9B1i6AAgAkUG1IDAAAQCcAABmheQBnheAAiQQAAiQhnheQhmheicAAIjAAAgAlgm0IAAhMIEEAAIAABMg");
    var mask_5_graphics_42 = new cjs.Graphics().p("AlgIBIAAswIEMAAQC6AAB9B1QB+B1AACuQAACuh+B1Qh9B1i6AAgAkUG1IDAAAQCcAABmheQBnheAAiQQAAiQhnheQhmheicAAIjAAAgAhUm0IkMAAIAAhMIEMAAIAdAAIAABNIgdgBg");
    var mask_5_graphics_43 = new cjs.Graphics().p("AlgIBIAAswIEMAAQC6AAB9B1QB+B1AACuQAACuh+B1Qh9B1i6AAgAkUG1IDAAAQCcAABmheQBnheAAiQQAAiQhnheQhmheicAAIjAAAgAhUm0IkMAAIAAhMIEMAAQA2AAAyAHIAABNQgygIg2AAg");
    var mask_5_graphics_44 = new cjs.Graphics().p("AlgIBIAAswIEMAAQC6AAB9B1QB+B1AACuQAACuh+B1Qh9B1i6AAgAkUG1IDAAAQCcAABmheQBnheAAiQQAAiQhnheQhmheicAAIjAAAgAApmoQg8gMhBAAIkMAAIAAhMIEMAAQBGAABAAMIAABOIgJgCg");
    var mask_5_graphics_45 = new cjs.Graphics().p("AlgIBIAAswIEMAAQC6AAB9B1QB+B1AACuQAACuh+B1Qh9B1i6AAgAkUG1IDAAAQCcAABmheQBnheAAiQQAAiQhnheQhmheicAAIjAAAgAhUm0IkMAAIAAhMIEMAAQBiAABYAXIAABQQhXgbhjAAg");
    var mask_5_graphics_46 = new cjs.Graphics().p("AlgIBIAAswIEMAAQC6AAB9B1QB+B1AACuQAACuh+B1Qh9B1i6AAgAkUG1IDAAAQCcAABmheQBnheAAiQQAAiQhnheQhmheicAAIjAAAgAhUm0IkMAAIAAhMIEMAAQB9AABtAlIAABTQhrgsh/AAg");
    var mask_5_graphics_47 = new cjs.Graphics().p("AlgIBIAAswIEMAAQC6AAB9B1QB+B1AACuQAACuh+B1Qh9B1i6AAgAkUG1IDAAAQCcAABmheQBnheAAiQQAAiQhnheQhmheicAAIjAAAgAhUm0IkMAAIAAhMIEMAAQCiAACFA+IgfBHQh3g5iRAAg");
    var mask_5_graphics_48 = new cjs.Graphics().p("AlgIBIAAswIEMAAQC6AAB9B1QB+B1AACuQAACuh+B1Qh9B1i6AAgAkUG1IDAAAQCcAABmheQBnheAAiQQAAiQhnheQhmheicAAIjAAAgAhUm0IkMAAIAAhMIEMAAQDBAACYBZIAbARIhqAcQh3g6iTAAg");
    var mask_5_graphics_49 = new cjs.Graphics().p("AlgIBIAAswIEMAAQC6AAB9B1QB+B1AACuQAACuh+B1Qh9B1i6AAgAkUG1IDAAAQCcAABmheQBnheAAiQQAAiQhnheQhmheicAAIjAAAgAC8l3Qh6g9iWAAIkMAAIAAhMIEMAAQDwAACvCJg");
    var mask_5_graphics_50 = new cjs.Graphics().p("AloIBIAAswIEMAAQC6AAB9B1QB+B1AACuQAACuh+B1Qh9B1i6AAgAkcG1IDAAAQCcAABmheQBnheAAiQQAAiQhnheQhmheicAAIjAAAgADrlXQiNhdi6AAIkMAAIAAhMIEMAAQEGAAC4CjIAHAGg");
    var mask_5_graphics_51 = new cjs.Graphics().p("Al8IBIAAswIEMAAQC6AAB+B1QB9B1AACuQAACuh9B1Qh+B1i6AAgAkvG1IC/AAQCcAABnheQBmheAAiQQAAiQhmheQhnheicAAIi/AAgAENkwIgFgEQidiAjbAAIkMAAIAAhMIEMAAQEPAAC8CuIAeAeIAEAEg");
    var mask_5_graphics_52 = new cjs.Graphics().p("AmJIBIAAswIEMAAQC6AAB+B1QB9B1AACuQAACuh9B1Qh+B1i6AAgAk8G1IC/AAQCcAABnheQBmheAAiQQAAiQhmheQhnheicAAIi/AAgAEhkTIgJgIQimiZjvAAIkMAAIAAhMIEMAAQEPAAC8CuQAhAfAbAgg");
    var mask_5_graphics_53 = new cjs.Graphics().p("AmYIBIAAswIELAAQC7AAB9B1QB+B1AACuQAACuh+B1Qh9B1i7AAgAlMG1IC/AAQCdAABmheQBnheAAiQQAAiQhnheQhmheidAAIi/AAgAE4jqQgWgZgagYQiliZjwAAIkLAAIAAhMIELAAQEQAAC8CuQA0AxAmA3g");
    var mask_5_graphics_54 = new cjs.Graphics().p("AmsIBIAAswIEMAAQC6AAB+B1QB9B1AACuQAACuh9B1Qh+B1i6AAgAlfG1IC/AAQCdAABmheQBmheAAiQQAAiQhmheQhmheidAAIi/AAgAFVinQgmg+g6g2QimiZjvAAIkMAAIAAhMIEMAAQEPAAC8CuQBUBOAuBdg");
    var mask_5_graphics_55 = new cjs.Graphics().p("Am0IBIAAswIEMAAQC6AAB+B1QB+B1AACuQAACuh+B1Qh+B1i6AAgAlnG1IC/AAQCdAABmheQBmheAAiQQAAiQhmheQhmheidAAIi/AAgAFgiFQgphShKhEQimiZjvAAIkMAAIAAhMIEMAAQEPAAC8CuQBjBcAuBxg");
    var mask_5_graphics_56 = new cjs.Graphics().p("Am9IBIAAswIEMAAQC6AAB+B1QB9B1AACuQAACuh9B1Qh+B1i6AAgAlwG1IC/AAQCdAABmheQBmheAAiQQAAiQhmheQhmheidAAIi/AAgAFthTIgCgFQgphthehWQimiZjvAAIkMAAIAAhMIEMAAQEPAAC8CuQB1BtAsCLIABACIACAFg");
    var mask_5_graphics_57 = new cjs.Graphics().p("AnDIBIAAswIEMAAQC6AAB+B1QB+B1AACuQAACuh+B1Qh+B1i6AAgAl2G1IC/AAQCdAABmheQBmheAAiQQAAiQhmheQhmheidAAIi/AAgAF1gkQgjiNh0hqQimiZjvAAIkMAAIAAhMIEMAAQEPAAC8CuQCLCBAlCtg");
    var mask_5_graphics_58 = new cjs.Graphics().p("AnGIBIAAswIEMAAQC6AAB+B1QB9B1AACuQAACuh9B1Qh+B1i6AAgAl5G1IC/AAQCdAABmheQBmheAAiQQAAiQhmheQhmheidAAIi/AAgAF6ADIgCgJQgdifiAh2QimiZjvAAIkMAAIAAhMIEMAAQEPAAC8CuQCXCNAeC/IABAJg");
    var mask_5_graphics_59 = new cjs.Graphics().p("AnIIBIAAswIEMAAQC7AAB9B1QB9B1AACuQAACuh9B1Qh9B1i7AAgAl7G1IC/AAQCdAABmheQBmheAAiQQAAiQhmheQhmheidAAIi/AAgAF8AjQgSi5iRiFQimiZjvAAIkMAAIAAhMIEMAAQEPAAC8CuQCoCcASDZg");
    var mask_5_graphics_60 = new cjs.Graphics().p("AnJIBIAAswIEMAAQC7AAB9B1QB9B1AACuQAACuh9B1Qh9B1i7AAgAl8G1IC/AAQCdAABmheQBmheAAiQQAAiQhmheQhmheidAAIi/AAgAF+BIIgBgOIgBgNQgPi/iViJQimiZjvAAIkMAAIAAhMIEMAAQEPAAC8CuQCwCkALDoIABAOg");
    var mask_5_graphics_61 = new cjs.Graphics().p("AnJIBIAAswIEMAAQC7AAB9B1QB9B1AACuQAACuh9B1Qh9B1i7AAgAl8G1IC/AAQCdAABmheQBmheAAiQQAAiQhmheQhmheidAAIi/AAgAF+BiIAAgJIAAgEIgCgoQgPi/iViJQimiZjvAAIkMAAIAAhMIEMAAQEPAAC8CuQC6CtACD6IAAAEIAAAJg");
    var mask_5_graphics_62 = new cjs.Graphics().p("AnJIBIAAswIEMAAQC7AAB9B1QB9B1AACuQAACuh9B1Qh9B1i7AAgAl8G1IC/AAQCdAABmheQBmheAAiQQAAiQhmheQhmheidAAIi/AAgAF7CQQADgbAAgcQAAgWgCgWQgPi/iViJQimiZjvAAIkMAAIAAhMIEMAAQEPAAC8CuQC8CvAAD8QAAAcgCAbg");
    var mask_5_graphics_63 = new cjs.Graphics().p("AnJIBIAAswIEMAAQC7AAB9B1QB9B1AACuQAACuh9B1Qh9B1i7AAgAl8G1IC/AAQCdAABmheQBmheAAiQQAAiQhmheQhmheidAAIi/AAgAF2DKIAAgWQAIgsAAgvQAAgWgCgWQgPi/iViJQimiZjvAAIkMAAIAAhMIEMAAQEPAAC8CuQC8CvAAD8QAAA7gKA2g");
    var mask_5_graphics_64 = new cjs.Graphics().p("AnJIBIAAswIEMAAQC7AAB9B1QB9B1AACuQAACuh9B1Qh9B1i7AAgAl8G1IC/AAQCdAABmheQBmheAAiQQAAiQhmheQhmheidAAIi/AAgAF2C5IAAgFQAIgsAAgvQAAgWgCgWQgPi/iViJQimiZjvAAIkMAAIAAhMIEMAAQEPAAC8CuQC8CvAAD8QAABUgVBLg");
    var mask_5_graphics_65 = new cjs.Graphics().p("AnJIBIAAswIEMAAQC7AAB9B1QB9B1AACuQAACuh9B1Qh9B1i7AAgAl8G1IC/AAQCdAABmheQBmheAAiQQAAiQhmheQhmheidAAIi/AAgAF2DYIAAgkQAIgsAAgvQAAgWgCgWQgPi/iViJQimiZjvAAIkMAAIAAhMIEMAAQEPAAC8CuQC8CvAAD8QAABhgbBVQgJAbgMAbg");
    var mask_5_graphics_66 = new cjs.Graphics().p("AnJIBIAAswIEMAAQC7AAB9B1QB9B1AACuQAACuh9B1Qh9B1i7AAgAl8G1IC/AAQCdAABmheQBmheAAiQQAAiQhmheQhmheidAAIi/AAgAF2C0QAIgsAAgvQAAgWgCgWQgPi/iViJQimiZjvAAIkMAAIAAhMIEMAAQEPAAC8CuQC8CvAAD8QAACphUCHg");
    var mask_5_graphics_67 = new cjs.Graphics().p("AnJIBIAAswIEMAAQC7AAB9B1QB9B1AACuQAACuh9B1Qh9B1i7AAgAl8G1IC/AAQCdAABmheQBmheAAiQQAAiQhmheQhmheidAAIi/AAgAFkD8QAahMAAhXQAAgWgCgWQgPi/iViJQimiZjvAAIkMAAIAAhMIEMAAQEPAAC8CuQC8CvAAD8QAACzheCMIgIAMg");
    var mask_5_graphics_68 = new cjs.Graphics().p("AnJIBIAAswIEMAAQC7AAB9B1QB9B1AACuQAACuh9B1Qh9B1i7AAgAl8G1IC/AAQCdAABmheQBmheAAiQQAAiQhmheQhmheidAAIi/AAgAFGFCQA4hpAAiAQAAgWgCgWQgPi/iViJQimiZjvAAIkMAAIAAhMIEMAAQEPAAC8CuQC8CvAAD8QAADViECcg");
    var mask_5_graphics_69 = new cjs.Graphics().p("AnJIBIAAswIEMAAQC7AAB9B1QB9B1AACuQAACuh9B1Qh9B1i7AAgAl8G1IC/AAQCdAABmheQBmheAAiQQAAiQhmheQhmheidAAIi/AAgAE1FlIgDAAIAEgGQBIh0AAiSQAAgWgCgWQgPi/iViJQimiZjvAAIkMAAIAAhMIEMAAQEPAAC8CuQC8CvAAD8QAADeiQCiIgFAFg");
    var mask_5_graphics_70 = new cjs.Graphics().p("AEHGZIACgCQB1iKAAi5QAAgXgCgWQgPi/iViJQimiYjvAAIkMAAIAAhNIEMAAQEPAAC8CvQC8CvAAD8QAAD8i5CuIgDACIgHAGgAnJH8IAAswIEMAAQC7AAB9B1QB9B1AACuQAACuh9B1Qh9B1i7AAgAl8GwIC/AAQCdAABmhfQBmhdAAiQQAAiQhmheQhmheidAAIi/AAg");
    var mask_5_graphics_71 = new cjs.Graphics().p("ADwGpIADgEQCLiRAAjKQAAgXgCgWQgPi/iViJQimiYjvAAIkMAAIAAhNIEMAAQEPAAC8CvQC8CuAAD9QAAD5i2CtIgGAGIgeAbgAnJHyIAAswIEMAAQC7AAB9B1QB9B1AACtQAACvh9B1Qh9B1i7AAgAl8GlIC/AAQCdAABmheQBmhdAAiRQAAiPhmheQhmheidAAIi/AAg");
    var mask_5_graphics_72 = new cjs.Graphics().p("ADKG9IAOgLQCmiZAAjeQAAgWgCgWQgPi/iViJQimiYjvAAIkMAAIAAhNIEMAAQEPAAC8CvQC8CuAAD8QAAD+i8CuQggAfgkAZgAnJHkIAAswIEMAAQC7gBB9B2QB9B0AACuQAACvh9B1Qh9B0i7ABgAl8GXIC/AAQCdAABmheQBmheAAiQQAAiPhmheQhmheidgBIi/AAg");
    var mask_5_graphics_73 = new cjs.Graphics().p("ACzHJQATgPASgQQCmiZAAjeQAAgWgCgWQgPi/iViJQimiYjvAAIkMAAIAAhNIEMAAQEPAAC8CvQC8CuAAD8QAAD+i8CuQgTATgVAQQgZAUgaARgAnJHcIAAswIEMAAQC7gBB9B2QB9B0AACuQAACvh9B1Qh9B0i7ABgAl8GPIC/AAQCdAABmheQBmheAAiQQAAiPhmheQhmheidgBIi/AAg");
    var mask_5_graphics_74 = new cjs.Graphics().p("ACHHbQAqgbAngjQCmiZAAjdQAAgXgCgVQgPjAiViJQimiYjvAAIkMAAIAAhNIEMAAQEPAAC8CvQC8CuAAD9QAAD9i8CvQg9A5hGAmIgEACgAnJHPIAAswIEMAAQC7AAB9B1QB9B1AACtQAACvh9B1Qh9B1i7AAgAl8GCIC/AAQCdAABmheQBmhdAAiRQAAiPhmheQhmheidAAIi/AAg");
    var mask_5_graphics_75 = new cjs.Graphics().p("ABeHpQBBgiA5g0QCmiZAAjdQAAgXgCgVQgPjAiViJQimiYjvAAIkMAAIAAhNIEMAAQEPAAC8CvQC8CuAAD9QAAD9i8CvQhQBKhgArgAnJHFIAAswIEMAAQC7AAB9B1QB9B1AACtQAACvh9B1Qh9B1i7AAgAl8F4IC/AAQCdAABmheQBmhdAAiRQAAiPhmheQhmheidAAIi/AAg");
    var mask_5_graphics_76 = new cjs.Graphics().p("AA7HzQBVgmBIhCQCmiYAAjeQAAgVgCgWQgPjAiViJQimiZjvAAIkMAAIAAhMIEMAAQEPAAC8CuQC8CvAAD8QAAD+i8CuQhfBZh0ArgAnJG9IAAswIEMAAQC7AAB9B1QB9B1AACuQAACuh9B1Qh9B1i7AAgAl8FxIC/AAQCdAABmheQBmheAAiQQAAiQhmheQhmheidAAIi/AAg");
    var mask_5_graphics_77 = new cjs.Graphics().p("AgJIBQB9glBkhbQCmiZAAjdQAAgWgCgWQgPjAiViJQimiYjvAAIkMAAIAAhNIEMAAQEPAAC8CvQC8CuAAD9QAAD9i8CvQh6BxidAogAnJGzIAAswIEMAAQC7AAB9B1QB9B1AACtQAACvh9B1Qh9B1i7AAgAl8FmIC/AAQCdAABmheQBmhdAAiRQAAiPhmheQhmheidAAIi/AAg");
    var mask_5_graphics_78 = new cjs.Graphics().p("AhWIKQCtgaCBh3QCmiYAAjeQAAgVgCgWQgPjAiViJQimiZjvAAIkMAAIAAhMIEMAAQEPAAC8CuQC8CvAAD8QAAD+i8CuQiXCNjNAbgAnJGrIAAswIEMAAQC7AAB9B1QB9B1AACuQAACuh9B1Qh9B1i7AAgAl8FfIC/AAQCdAABmheQBmheAAiQQAAiQhmheQhmheidAAIi/AAg");
    var mask_5_graphics_79 = new cjs.Graphics().p("AigIOQDcgJCciPQCmiYAAjeQAAgVgCgWQgPjAiViJQimiZjvAAIkMAAIAAhMIEMAAQEPAAC8CuQC8CvAAD8QAAD+i8CuQiyClj8AJgAnJGoIAAswIEMAAQC7AAB9B1QB9B1AACuQAACuh9B1Qh9B1i7AAgAl8FcIC/AAQCdAABmheQBmheAAiQQAAiQhmheQhmheidAAIi/AAg");
    var mask_5_graphics_80 = new cjs.Graphics().p("AjtJbIAAhNIAwAAQDvAACmiYQCmiYAAjeQAAgWgCgWQgPjAiViJQimiYjvAAIkMAAIAAhNIEMAAQEPAAC8CvQC8CvAAD8QAAD9i8CvQi8CvkPAAgAnJGoIAAswIEMAAQC7AAB9B1QB9B1AACuQAACuh9B1Qh9B1i7AAgAl8FcIC/AAQCdAABmhfQBmhdAAiQQAAiQhmheQhmheidAAIi/AAg");
    var mask_5_graphics_81 = new cjs.Graphics().p("AkzJbIAAhNIB2AAQDvAACmiYQCmiYAAjeQAAgWgCgWQgPjAiViJQimiYjvAAIkMAAIAAhNIEMAAQEPAAC8CvQC8CvAAD8QAAD9i8CvQi8CvkPAAgAnJGoIAAswIEMAAQC7AAB9B1QB9B1AACuQAACuh9B1Qh9B1i7AAgAl8FcIC/AAQCdAABmhfQBmhdAAiQQAAiQhmheQhmheidAAIi/AAg");
    var mask_5_graphics_82 = new cjs.Graphics().p("AmAJbIAAhNIDDAAQDvAACmiYQCmiYAAjeQAAgWgCgWQgPjAiViJQimiYjvAAIkMAAIAAhNIEMAAQEPAAC8CvQC8CvAAD8QAAD9i8CvQi8CvkPAAgAnJGoIAAswIEMAAQC7AAB9B1QB9B1AACuQAACuh9B1Qh9B1i7AAgAl8FcIC/AAQCdAABmhfQBmhdAAiQQAAiQhmheQhmheidAAIi/AAg");
    var mask_5_graphics_83 = new cjs.Graphics().p("AnJJbIAAhNIEMAAQDvAACmiYQCmiYAAjeQAAgWgCgWQgPjAiViJQimiYjvAAIkMAAIAAhNIEMAAQEPAAC8CvQC8CvAAD8QAAD9i8CvQi8CvkPAAgAnJGoIAAswIEMAAQC7AAB9B1QB9B1AACuQAACuh9B1Qh9B1i7AAgAl8FcIC/AAQCdAABmhfQBmhdAAiQQAAiQhmheQhmheidAAIi/AAg");
    var mask_5_graphics_119 = new cjs.Graphics().p("AnJJbIAAhNIEMAAQDvAACmiYQCmiYAAjeQAAgWgCgWQgPjAiViJQimiYjvAAIkMAAIAAhNIEMAAQEPAAC8CvQC8CvAAD8QAAD9i8CvQi8CvkPAAgAnJGoIAAswIEMAAQC7AAB9B1QB9B1AACuQAACuh9B1Qh9B1i7AAgAl8FcIC/AAQCdAABmhfQBmhdAAiQQAAiQhmheQhmheidAAIi/AAg");
    var mask_5_graphics_120 = new cjs.Graphics().p("AnJJbIAAhNIEMAAQDvAACmiYQCmiYAAjeQAAgWgCgWQgPjAiViJQimiYjvAAIkMAAIAAhNIEMAAQEPAAC8CvQC8CvAAD8QAAD9i8CvQi8CvkPAAgAnJGoIAAswIEMAAQC7AAB9B1QB9B1AACuQAACuh9B1Qh9B1i7AAgAl8FcIC/AAQCdAABmhfQBmhdAAiQQAAiQhmheQhmheidAAIi/AAg");
    var mask_5_graphics_121 = new cjs.Graphics().p("AnJJbIAAhNIEMAAQDvAACmiYQCmiYAAjeQAAgWgCgWQgPjAiViJQimiYjvAAIkMAAIAAhNIEMAAQEPAAC8CvQC8CvAAD8QAAD9i8CvQi8CvkPAAgAnJGoIAAswIEMAAQC7AAB9B1QB9B1AACuQAACuh9B1Qh9B1i7AAgAl8FcIC/AAQCdAABmhfQBmhdAAiQQAAiQhmheQhmheidAAIi/AAg");
    var mask_5_graphics_122 = new cjs.Graphics().p("AnJJbIAAhNIEMAAQDvAACmiYQCmiYAAjeQAAgWgCgWQgPjAiViJQimiYjvAAIkMAAIAAhNIEMAAQEPAAC8CvQC8CvAAD8QAAD9i8CvQi8CvkPAAgAnJGoIAAswIEMAAQC7AAB9B1QB9B1AACuQAACuh9B1Qh9B1i7AAgAl8FcIC/AAQCdAABmhfQBmhdAAiQQAAiQhmheQhmheidAAIi/AAg");
    var mask_5_graphics_123 = new cjs.Graphics().p("AnJJbIAAhNIEMAAQDvAACmiYQCmiYAAjeQAAgWgCgWQgPjAiViJQimiYjvAAIkMAAIAAhNIEMAAQEPAAC8CvQC8CvAAD8QAAD9i8CvQi8CvkPAAgAnJGoIAAswIEMAAQC7AAB9B1QB9B1AACuQAACuh9B1Qh9B1i7AAgAl8FcIC/AAQCdAABmhfQBmhdAAiQQAAiQhmheQhmheidAAIi/AAg");
    var mask_5_graphics_124 = new cjs.Graphics().p("AnJJbIAAhNIEMAAQDvAACmiYQCmiYAAjeQAAgWgCgWQgPjAiViJQimiYjvAAIkMAAIAAhNIEMAAQEPAAC8CvQC8CvAAD8QAAD9i8CvQi8CvkPAAgAnJGoIAAswIEMAAQC7AAB9B1QB9B1AACuQAACuh9B1Qh9B1i7AAgAl8FcIC/AAQCdAABmhfQBmhdAAiQQAAiQhmheQhmheidAAIi/AAg");
    var mask_5_graphics_125 = new cjs.Graphics().p("AnJJbIAAhNIEMAAQDvAACmiYQCmiYAAjeQAAgWgCgWQgPjAiViJQimiYjvAAIkMAAIAAhNIEMAAQEPAAC8CvQC8CvAAD8QAAD9i8CvQi8CvkPAAgAnJGoIAAswIEMAAQC7AAB9B1QB9B1AACuQAACuh9B1Qh9B1i7AAgAl8FcIC/AAQCdAABmhfQBmhdAAiQQAAiQhmheQhmheidAAIi/AAg");
    var mask_5_graphics_126 = new cjs.Graphics().p("AnJJbIAAhNIEMAAQDvAACmiYQCmiYAAjeQAAgWgCgWQgPjAiViJQimiYjvAAIkMAAIAAhNIEMAAQEPAAC8CvQC8CvAAD8QAAD9i8CvQi8CvkPAAgAnJGoIAAswIEMAAQC7AAB9B1QB9B1AACuQAACuh9B1Qh9B1i7AAgAl8FcIC/AAQCdAABmhfQBmhdAAiQQAAiQhmheQhmheidAAIi/AAg");
    var mask_5_graphics_127 = new cjs.Graphics().p("AnJJbIAAhNIEMAAQDvAACmiYQCmiYAAjeQAAgWgCgWQgPjAiViJQimiYjvAAIkMAAIAAhNIEMAAQEPAAC8CvQC8CvAAD8QAAD9i8CvQi8CvkPAAgAnJGoIAAswIEMAAQC7AAB9B1QB9B1AACuQAACuh9B1Qh9B1i7AAgAl8FcIC/AAQCdAABmhfQBmhdAAiQQAAiQhmheQhmheidAAIi/AAg");
    var mask_5_graphics_128 = new cjs.Graphics().p("AnJJbIAAhNIEMAAQDvAACmiYQCmiYAAjeQAAgWgCgWQgPjAiViJQimiYjvAAIkMAAIAAhNIEMAAQEPAAC8CvQC8CvAAD8QAAD9i8CvQi8CvkPAAgAnJGoIAAswIEMAAQC7AAB9B1QB9B1AACuQAACuh9B1Qh9B1i7AAgAl8FcIC/AAQCdAABmhfQBmhdAAiQQAAiQhmheQhmheidAAIi/AAg");
    var mask_5_graphics_129 = new cjs.Graphics().p("AnJJbIAAhNIEMAAQDvAACmiYQCmiYAAjeQAAgWgCgWQgPjAiViJQimiYjvAAIkMAAIAAhNIEMAAQEPAAC8CvQC8CvAAD8QAAD9i8CvQi8CvkPAAgAnJGoIAAswIEMAAQC7AAB9B1QB9B1AACuQAACuh9B1Qh9B1i7AAgAl8FcIC/AAQCdAABmhfQBmhdAAiQQAAiQhmheQhmheidAAIi/AAg");
    var mask_5_graphics_130 = new cjs.Graphics().p("AnJJbIAAhNIEMAAQDvAACmiYQCmiYAAjeQAAgWgCgWQgPjAiViJQimiYjvAAIkMAAIAAhNIEMAAQEPAAC8CvQC8CvAAD8QAAD9i8CvQi8CvkPAAgAnJGoIAAswIEMAAQC7AAB9B1QB9B1AACuQAACuh9B1Qh9B1i7AAgAl8FcIC/AAQCdAABmhfQBmhdAAiQQAAiQhmheQhmheidAAIi/AAg");
    var mask_5_graphics_131 = new cjs.Graphics().p("AnJJbIAAhNIEMAAQDvAACmiYQCmiYAAjeQAAgWgCgWQgPjAiViJQimiYjvAAIkMAAIAAhNIEMAAQEPAAC8CvQC8CvAAD8QAAD9i8CvQi8CvkPAAgAnJGoIAAswIEMAAQC7AAB9B1QB9B1AACuQAACuh9B1Qh9B1i7AAgAl8FcIC/AAQCdAABmhfQBmhdAAiQQAAiQhmheQhmheidAAIi/AAg");
    var mask_5_graphics_132 = new cjs.Graphics().p("AnJJbIAAhNIEMAAQDvAACmiYQCmiYAAjeQAAgWgCgWQgPjAiViJQimiYjvAAIkMAAIAAhNIEMAAQEPAAC8CvQC8CvAAD8QAAD9i8CvQi8CvkPAAgAnJGoIAAswIEMAAQC7AAB9B1QB9B1AACuQAACuh9B1Qh9B1i7AAgAl8FcIC/AAQCdAABmhfQBmhdAAiQQAAiQhmheQhmheidAAIi/AAg");
    var mask_5_graphics_133 = new cjs.Graphics().p("AnJJbIAAhNIEMAAQDvAACmiYQCmiYAAjeQAAgWgCgWQgPjAiViJQimiYjvAAIkMAAIAAhNIEMAAQEPAAC8CvQC8CvAAD8QAAD9i8CvQi8CvkPAAgAnJGoIAAswIEMAAQC7AAB9B1QB9B1AACuQAACuh9B1Qh9B1i7AAgAl8FcIC/AAQCdAABmhfQBmhdAAiQQAAiQhmheQhmheidAAIi/AAg");
    var mask_5_graphics_134 = new cjs.Graphics().p("AnJJbIAAhNIEMAAQDvAACmiYQCmiYAAjeQAAgWgCgWQgPjAiViJQimiYjvAAIkMAAIAAhNIEMAAQEPAAC8CvQC8CvAAD8QAAD9i8CvQi8CvkPAAgAnJGoIAAswIEMAAQC7AAB9B1QB9B1AACuQAACuh9B1Qh9B1i7AAgAl8FcIC/AAQCdAABmhfQBmhdAAiQQAAiQhmheQhmheidAAIi/AAg");
  
    this.timeline.addTween(cjs.Tween.get(mask_5).to({graphics:null,x:0,y:0}).wait(1).to({graphics:mask_5_graphics_1,x:364.7,y:24.8}).wait(1).to({graphics:mask_5_graphics_2,x:367.8,y:24.8}).wait(1).to({graphics:mask_5_graphics_3,x:370.7,y:24.8}).wait(1).to({graphics:mask_5_graphics_4,x:374,y:24.8}).wait(1).to({graphics:mask_5_graphics_5,x:378.2,y:25.1}).wait(1).to({graphics:mask_5_graphics_6,x:382.7,y:26}).wait(1).to({graphics:mask_5_graphics_7,x:385.8,y:27.5}).wait(1).to({graphics:mask_5_graphics_8,x:388.7,y:28.6}).wait(1).to({graphics:mask_5_graphics_9,x:391,y:32.1}).wait(1).to({graphics:mask_5_graphics_10,x:392.1,y:35.1}).wait(1).to({graphics:mask_5_graphics_11,x:392.9,y:37.7}).wait(1).to({graphics:mask_5_graphics_12,x:393.2,y:40.5}).wait(1).to({graphics:mask_5_graphics_13,x:393.2,y:44}).wait(1).to({graphics:mask_5_graphics_14,x:393.2,y:47.3}).wait(1).to({graphics:mask_5_graphics_15,x:393.2,y:51}).wait(1).to({graphics:mask_5_graphics_16,x:393.2,y:55.1}).wait(1).to({graphics:mask_5_graphics_17,x:393.2,y:57.2}).wait(1).to({graphics:mask_5_graphics_18,x:393.2,y:59.1}).wait(1).to({graphics:mask_5_graphics_19,x:393.2,y:60.6}).wait(1).to({graphics:mask_5_graphics_20,x:393.2,y:61.4}).wait(1).to({graphics:mask_5_graphics_21,x:393.2,y:61.8}).wait(1).to({graphics:mask_5_graphics_22,x:393.2,y:61.8}).wait(1).to({graphics:mask_5_graphics_23,x:393.2,y:61.8}).wait(1).to({graphics:mask_5_graphics_24,x:393.2,y:61.8}).wait(1).to({graphics:mask_5_graphics_25,x:389.3,y:61.8}).wait(1).to({graphics:mask_5_graphics_26,x:389.3,y:61.8}).wait(1).to({graphics:mask_5_graphics_27,x:389.3,y:61.8}).wait(1).to({graphics:mask_5_graphics_28,x:389.3,y:61.8}).wait(1).to({graphics:mask_5_graphics_29,x:389.3,y:61.8}).wait(1).to({graphics:mask_5_graphics_30,x:389.3,y:61.8}).wait(1).to({graphics:mask_5_graphics_31,x:389.3,y:61.8}).wait(1).to({graphics:mask_5_graphics_32,x:389.3,y:61.8}).wait(1).to({graphics:mask_5_graphics_33,x:389.3,y:61.8}).wait(1).to({graphics:mask_5_graphics_34,x:389.3,y:61.8}).wait(1).to({graphics:mask_5_graphics_35,x:389.3,y:61.8}).wait(1).to({graphics:mask_5_graphics_36,x:389.3,y:61.8}).wait(1).to({graphics:mask_5_graphics_37,x:389.3,y:61.8}).wait(1).to({graphics:mask_5_graphics_38,x:389.3,y:51.3}).wait(1).to({graphics:mask_5_graphics_39,x:389.3,y:51.3}).wait(1).to({graphics:mask_5_graphics_40,x:389.3,y:51.3}).wait(1).to({graphics:mask_5_graphics_41,x:389.3,y:51.3}).wait(1).to({graphics:mask_5_graphics_42,x:389.3,y:51.3}).wait(1).to({graphics:mask_5_graphics_43,x:389.3,y:51.3}).wait(1).to({graphics:mask_5_graphics_44,x:389.3,y:51.3}).wait(1).to({graphics:mask_5_graphics_45,x:389.3,y:51.3}).wait(1).to({graphics:mask_5_graphics_46,x:389.3,y:51.3}).wait(1).to({graphics:mask_5_graphics_47,x:389.3,y:51.3}).wait(1).to({graphics:mask_5_graphics_48,x:389.3,y:51.3}).wait(1).to({graphics:mask_5_graphics_49,x:389.3,y:51.3}).wait(1).to({graphics:mask_5_graphics_50,x:390.1,y:51.3}).wait(1).to({graphics:mask_5_graphics_51,x:392.1,y:51.3}).wait(1).to({graphics:mask_5_graphics_52,x:393.4,y:51.3}).wait(1).to({graphics:mask_5_graphics_53,x:394.9,y:51.3}).wait(1).to({graphics:mask_5_graphics_54,x:396.9,y:51.3}).wait(1).to({graphics:mask_5_graphics_55,x:397.7,y:51.3}).wait(1).to({graphics:mask_5_graphics_56,x:398.6,y:51.3}).wait(1).to({graphics:mask_5_graphics_57,x:399.2,y:51.3}).wait(1).to({graphics:mask_5_graphics_58,x:399.5,y:51.3}).wait(1).to({graphics:mask_5_graphics_59,x:399.7,y:51.3}).wait(1).to({graphics:mask_5_graphics_60,x:399.8,y:51.3}).wait(1).to({graphics:mask_5_graphics_61,x:399.8,y:51.3}).wait(1).to({graphics:mask_5_graphics_62,x:399.8,y:51.3}).wait(1).to({graphics:mask_5_graphics_63,x:399.8,y:51.3}).wait(1).to({graphics:mask_5_graphics_64,x:399.8,y:51.3}).wait(1).to({graphics:mask_5_graphics_65,x:399.8,y:51.3}).wait(1).to({graphics:mask_5_graphics_66,x:399.8,y:51.3}).wait(1).to({graphics:mask_5_graphics_67,x:399.8,y:51.3}).wait(1).to({graphics:mask_5_graphics_68,x:399.8,y:51.3}).wait(1).to({graphics:mask_5_graphics_69,x:399.8,y:51.3}).wait(1).to({graphics:mask_5_graphics_70,x:399.8,y:51.9}).wait(1).to({graphics:mask_5_graphics_71,x:399.8,y:52.9}).wait(1).to({graphics:mask_5_graphics_72,x:399.8,y:54.3}).wait(1).to({graphics:mask_5_graphics_73,x:399.8,y:55.1}).wait(1).to({graphics:mask_5_graphics_74,x:399.8,y:56.4}).wait(1).to({graphics:mask_5_graphics_75,x:399.8,y:57.4}).wait(1).to({graphics:mask_5_graphics_76,x:399.8,y:58.1}).wait(1).to({graphics:mask_5_graphics_77,x:399.8,y:59.2}).wait(1).to({graphics:mask_5_graphics_78,x:399.8,y:59.9}).wait(1).to({graphics:mask_5_graphics_79,x:399.8,y:60.2}).wait(1).to({graphics:mask_5_graphics_80,x:399.8,y:60.3}).wait(1).to({graphics:mask_5_graphics_81,x:399.8,y:60.3}).wait(1).to({graphics:mask_5_graphics_82,x:399.8,y:60.3}).wait(1).to({graphics:mask_5_graphics_83,x:399.8,y:60.3}).wait(36).to({graphics:mask_5_graphics_119,x:399.8,y:60.3}).wait(1).to({graphics:mask_5_graphics_120,x:399.8,y:60.3}).wait(1).to({graphics:mask_5_graphics_121,x:399.8,y:60.3}).wait(1).to({graphics:mask_5_graphics_122,x:399.8,y:60.3}).wait(1).to({graphics:mask_5_graphics_123,x:399.8,y:60.3}).wait(1).to({graphics:mask_5_graphics_124,x:399.8,y:60.3}).wait(1).to({graphics:mask_5_graphics_125,x:399.8,y:60.3}).wait(1).to({graphics:mask_5_graphics_126,x:399.8,y:60.3}).wait(1).to({graphics:mask_5_graphics_127,x:399.8,y:60.3}).wait(1).to({graphics:mask_5_graphics_128,x:399.8,y:60.3}).wait(1).to({graphics:mask_5_graphics_129,x:399.8,y:60.3}).wait(1).to({graphics:mask_5_graphics_130,x:399.8,y:60.3}).wait(1).to({graphics:mask_5_graphics_131,x:399.8,y:60.3}).wait(1).to({graphics:mask_5_graphics_132,x:399.8,y:60.3}).wait(1).to({graphics:mask_5_graphics_133,x:399.8,y:60.3}).wait(1).to({graphics:mask_5_graphics_134,x:399.8,y:60.3}).wait(1));
  
    // D
    this.shape_5 = new cjs.Shape();
    this.shape_5.graphics.f("#fff").s().p("AnJJbIAAhNIEMAAQDvAACmiYQCmiYAAjeQAAjcimiZQimiYjvAAIkMAAIAAhNIEMAAQEPAAC8CvQC8CvAAD8QAAD9i8CvQi8CvkPAAgAnJGoIAAswIEMAAQC7AAB9B1QB9B1AACuQAACuh9B1Qh9B1i7AAgAl8FcIC/AAQCdAABmhfQBmhdAAiQQAAiQhmheQhmheidAAIi/AAg");
    this.shape_5.setTransform(399.8,60.3);
  
    this.instance_10 = new lib.Tween27("synched",0);
    this.instance_10.parent = this;
    this.instance_10.setTransform(399.8,60.3);
    this.instance_10._off = true;
  
    this.instance_11 = new lib.Tween28("synched",0);
    this.instance_11.parent = this;
    this.instance_11.setTransform(399.8,60.3);
    this.instance_11.alpha = 0;
  
    var maskedShapeInstanceList = [this.shape_5,this.instance_10,this.instance_11];
  
    for(var shapedInstanceItr = 0; shapedInstanceItr < maskedShapeInstanceList.length; shapedInstanceItr++) {
      maskedShapeInstanceList[shapedInstanceItr].mask = mask_5;
    }
  
    this.timeline.addTween(cjs.Tween.get({}).to({state:[]}).to({state:[{t:this.shape_5}]},1).to({state:[{t:this.instance_10}]},118).to({state:[{t:this.instance_11}]},15).wait(1));
    this.timeline.addTween(cjs.Tween.get(this.instance_10).wait(119).to({_off:false},0).to({_off:true,alpha:0},15).wait(1));
  
    // N mask (mask)
    var mask_6 = new cjs.Shape();
    mask_6._off = true;
    var mask_6_graphics_1 = new cjs.Graphics().p("AggAsIgBh8IAAAAIBDBXIglBLg");
    var mask_6_graphics_2 = new cjs.Graphics().p("Ag5ANIgBh8IABAAIB0CWIgoBJg");
    var mask_6_graphics_3 = new cjs.Graphics().p("AhMgJIgBh9IAAAAICcDKIgsBDg");
    var mask_6_graphics_4 = new cjs.Graphics().p("AhjglIgBh8IABgBIDIEEIguBBg");
    var mask_6_graphics_5 = new cjs.Graphics().p("Ah7hFIgBh9IABAAID4FBIgsBEg");
    var mask_6_graphics_6 = new cjs.Graphics().p("AiRhjIgBh9IAAAAIElF8IgqBFg");
    var mask_6_graphics_7 = new cjs.Graphics().p("AiqiFIgBh9IAAAAIFXG8IgmBJg");
    var mask_6_graphics_8 = new cjs.Graphics().p("AjFioIgBh9IAAAAIGOIEIgpBHg");
    var mask_6_graphics_9 = new cjs.Graphics().p("AjcjHIgBh8IAAgBIG7I+IgmBLg");
    var mask_6_graphics_10 = new cjs.Graphics().p("Aj4jqIgBh9IABAAIHyKFIgmBLg");
    var mask_6_graphics_11 = new cjs.Graphics().p("ADzGPIoHqgIgBh8IAAgBIIrLPIgjBOg");
    var mask_6_graphics_12 = new cjs.Graphics().p("ADaGPIoHqgIgBh8IABgBIIsLRIAwAAIAABMg");
    var mask_6_graphics_13 = new cjs.Graphics().p("AC1GPIoGqgIgBh8IAAgBIIsLRIB4AAIAAgDIABAAIAABPg");
    var mask_6_graphics_14 = new cjs.Graphics().p("ACQGPIoHqgIgBh8IABgBIIsLRIB4AAIAAgDIBMAAIAABPg");
    var mask_6_graphics_15 = new cjs.Graphics().p("ACQGPIoHqgIgBh8IABgBIIsLRIB4AAIAAhYIBMAAIAACkg");
    var mask_6_graphics_16 = new cjs.Graphics().p("ACQGPIoHqgIgBh8IABgBIIsLRIB4AAIAAigIBMAAIAADsg");
    var mask_6_graphics_17 = new cjs.Graphics().p("ACQGPIoHqgIgBh8IABgBIIsLRIB4AAIAAjmIBMAAIAAEyg");
    var mask_6_graphics_18 = new cjs.Graphics().p("ACQGPIoHqgIgBh8IABgBIIsLRIB4AAIAAkuIBMAAIAAF6g");
    var mask_6_graphics_19 = new cjs.Graphics().p("ACQGPIoHqgIgBh8IABgBIIsLRIB4AAIAAl1IBMAAIAAHBg");
    var mask_6_graphics_20 = new cjs.Graphics().p("ACQGPIoHqgIgBh8IABgBIIsLRIB4AAIAAm7IBMAAIAAIHg");
    var mask_6_graphics_21 = new cjs.Graphics().p("ACQGPIoHqgIgBh8IABgBIIsLRIB4AAIAAn+IBMAAIAAJKg");
    var mask_6_graphics_22 = new cjs.Graphics().p("ACQGPIoHqgIgBh8IABgBIIsLRIB4AAIAApDIBMAAIAAKPg");
    var mask_6_graphics_23 = new cjs.Graphics().p("ACQGPIoHqgIgBh8IABgBIIsLRIB4AAIAAqQIBMAAIAALcg");
    var mask_6_graphics_24 = new cjs.Graphics().p("ACQGQIoHqgIgBh9IABAAIIsLRIB4AAIAArTIBMAAIAAMfg");
    var mask_6_graphics_25 = new cjs.Graphics().p("ACQG2IoHqgIgBh9IABAAIIsLRIB4AAIAAsfIBMAAIAANrg");
    var mask_6_graphics_26 = new cjs.Graphics().p("ACQHVIoHqfIgBh9IABAAIIsLRIB4AAIAAteIBMAAIAAOpg");
    var mask_6_graphics_27 = new cjs.Graphics().p("ACQH4IoHqgIgBh9IABAAIIsLRIB4AAIAAuiIBMAAIAAPug");
    var mask_6_graphics_28 = new cjs.Graphics().p("ACQIcIoHqgIgBh9IABAAIIsLRIB4AAIAAvqIBMAAIAAQ2g");
    var mask_6_graphics_29 = new cjs.Graphics().p("ACQI5IoHqfIgBh9IABAAIIsLRIB4AAIAAwmIBMAAIAARxg");
    var mask_6_graphics_30 = new cjs.Graphics().p("ACQJaIoHqfIgBh9IABAAIIsLRIB4AAIAAxoIBMAAIAASzg");
    var mask_6_graphics_31 = new cjs.Graphics().p("ACQJaIoHqfIgBh9IABAAIIsLRIB4AAIAAxoIBMAAIAASzgABbonIAAgyIBMAAIAAAyg");
    var mask_6_graphics_32 = new cjs.Graphics().p("ACQJaIoHqfIgBh9IABAAIIsLRIB4AAIAAxoIBMAAIAASzgABbnqIAAhvIBMAAIAABvg");
    var mask_6_graphics_33 = new cjs.Graphics().p("ACQJaIoHqfIgBh9IABAAIIsLRIB4AAIAAxoIBMAAIAASzgABbmxIAAioIBMAAIAACog");
    var mask_6_graphics_34 = new cjs.Graphics().p("ACQJaIoHqfIgBh9IABAAIIsLRIB4AAIAAxoIBMAAIAASzgABbl3IAAjiIBMAAIAADig");
    var mask_6_graphics_35 = new cjs.Graphics().p("ACQJaIoHqfIgBh9IABAAIIsLRIB4AAIAAxoIBMAAIAASzgABbkxIAAkoIBMAAIAAEog");
    var mask_6_graphics_36 = new cjs.Graphics().p("ACQJaIoHqfIgBh9IABAAIIsLRIB4AAIAAxoIBMAAIAASzgABbjUIAAmFIBMAAIAAGFg");
    var mask_6_graphics_37 = new cjs.Graphics().p("ACQJaIoHqfIgBh9IABAAIIsLRIB4AAIAAxoIBMAAIAASzgABbiGIAAnTIBMAAIAAHTg");
    var mask_6_graphics_38 = new cjs.Graphics().p("ACQJaIoHqfIgBh9IABAAIIsLRIB4AAIAAxoIBMAAIAASzgABbg1IAAokIBMAAIAAIkg");
    var mask_6_graphics_39 = new cjs.Graphics().p("ACQJaIoHqfIgBh9IABAAIIsLRIB4AAIAAxoIBMAAIAASzgABbAbIAAp0IBMAAIAAJ0g");
    var mask_6_graphics_40 = new cjs.Graphics().p("ACQJaIoHqfIgBh9IABAAIIsLRIB4AAIAAxoIBMAAIAASzgABbBIIAAqhIBMAAIAALlg");
    var mask_6_graphics_41 = new cjs.Graphics().p("ACQJaIoHqfIgBh9IABAAIIsLRIB4AAIAAxoIBMAAIAASzgABbDHIAAh9IAAAAIAAqjIBMAAIAAODg");
    var mask_6_graphics_42 = new cjs.Graphics().p("ACQJaIoHqfIgBh9IABAAIIsLRIB4AAIAAxoIBMAAIAASzgAAzCTIAAh9IAoA0IAAqjIBMAAIAAODg");
    var mask_6_graphics_43 = new cjs.Graphics().p("ACQJaIoHqfIgBh9IABAAIIsLRIB4AAIAAxoIBMAAIAASzgAAGBZIAAh8IBVBtIAAqjIBMAAIAAODg");
    var mask_6_graphics_44 = new cjs.Graphics().p("ACQJaIoHqfIgBh9IABAAIIsLRIB4AAIAAxoIBMAAIAASzgAg2AKIAAh8ICRC8IAAqjIBMAAIAAODg");
    var mask_6_graphics_45 = new cjs.Graphics().p("ACQJaIoHqfIgBh9IABAAIIsLRIB4AAIAAxoIBMAAIAASzgAh0hGIAAh9IDPENIAAqjIBMAAIAAODg");
    var mask_6_graphics_46 = new cjs.Graphics().p("ACQJaIoHqfIgBh9IABAAIIsLRIB4AAIAAxoIBMAAIAASzgAi1iZIAAh9IEQFgIAAqjIBMAAIAAODg");
    var mask_6_graphics_47 = new cjs.Graphics().p("ACQJaIoHqfIgBh9IABAAIIsLRIB4AAIAAxoIBMAAIAASzgAjtjjIAAh9IFIGqIAAqjIBMAAIAAODg");
    var mask_6_graphics_48 = new cjs.Graphics().p("ACQJaIoHqfIgBh9IABAAIIsLRIB4AAIAAxoIBMAAIAASzgAktk1IAAh9IGIH8IAAqjIBMAAIAAODg");
    var mask_6_graphics_49 = new cjs.Graphics().p("ACQJaIoHqfIgBh9IABAAIIsLRIB4AAIAAxoIBMAAIAASzgAlvmLIAAh9IHKJSIAAqjIBMAAIAAODg");
    var mask_6_graphics_50 = new cjs.Graphics().p("ACsJaIoHqfIgBh9IAAAAIItLRIB3AAIAAxoIBNAAIAASzgAmTneIAAh7IABAAIIJKjIAAqjIBMAAIAAODg");
    var mask_6_graphics_51 = new cjs.Graphics().p("ADWJaIoHqfIgBh9IAAAAIItLRIB3AAIAAxoIBNAAIAASzgAmNoNIgxAAIAAhMIBWAAIIJKjIAAqjIBMAAIAAODg");
    var mask_6_graphics_52 = new cjs.Graphics().p("AD3JaIoGqfIgBh9IAAAAIIsLRIB4AAIAAxoIBMAAIAASzgAlsoNIhzAAIAAhMICYAAIIKKjIAAqjIBMAAIAAODg");
    var mask_6_graphics_53 = new cjs.Graphics().p("AEgJaIoHqfIgBh9IAAAAIItLRIB3AAIAAxoIBNAAIAASzgAlDoNIjEAAIAAhMIDpAAIIJKjIAAqjIBMAAIAAODg");
    var mask_6_graphics_54 = new cjs.Graphics().p("AEgJaIoHqfIgBh9IAAAAIItLRIB3AAIAAxoIBNAAIAASzgAlDoNIh5AAIAAA4IADAEIhOAAIAAiIIDpAAIIJKjIAAqjIBMAAIAAODg");
    var mask_6_graphics_55 = new cjs.Graphics().p("AEgJaIoHqfIgBh9IAAAAIItLRIB3AAIAAxoIBNAAIAASzgAlDoNIh5AAIAAA0IguBFIgdgnIAAieIDpAAIIJKjIAAqjIBMAAIAAODg");
    var mask_6_graphics_56 = new cjs.Graphics().p("AEgJaIoHqfIgBh9IAAAAIItLRIB3AAIAAxoIBNAAIAASzgAlDoNIh5AAIAAA4IAeAmIAAB8IhpiIIAAieIDpAAIIJKjIAAqjIBMAAIAAODg");
    var mask_6_graphics_57 = new cjs.Graphics().p("AEgJaIoHqfIgBh9IAAAAIItLRIB3AAIAAxoIBNAAIAASzgAlDoNIh5AAIAAA4IBbB2IgBB7IiljXIAAieIDpAAIIJKjIAAqjIBMAAIAAODg");
    var mask_6_graphics_58 = new cjs.Graphics().p("AEgJaIoHqfIgBh9IAAAAIItLRIB3AAIAAxoIBNAAIAASzgAlDoNIh5AAIAAA4ICNC2IgHB5IAAAAIAAgFIjRkQIAAieIDpAAIIJKjIAAqjIBMAAIAAODg");
    var mask_6_graphics_59 = new cjs.Graphics().p("AEgJaIoIqhIAAAAIgBAAIgBgCIAAACIgDAAIhJhfIAAgFIjRkQIAAieIDpAAIIJKjIAAqjIBMAAIAAODIp6s3Ih5AAIAAA4IMBPkIB3AAIAAxoIBNAAIAASzg");
    var mask_6_graphics_60 = new cjs.Graphics().p("AEgJaIoIqhIAAAAIgBAAIgBgCIAAACIhMAAIAAhkIjRkQIAAieIDpAAIIJKjIAAqjIBMAAIAAODIp6s3Ih5AAIAAA4IMBPkIB3AAIAAxoIBNAAIAASzg");
    var mask_6_graphics_61 = new cjs.Graphics().p("AEgJaIoKqjIAABSIhMAAIAAi0IjRkQIAAieIDpAAIIJKjIAAqjIBMAAIAAODIp6s3Ih5AAIAAA4IMBPkIB3AAIAAxoIBNAAIAASzg");
    var mask_6_graphics_62 = new cjs.Graphics().p("AEgJaIoKqjIAACSIhMAAIAAj0IjRkQIAAieIDpAAIIJKjIAAqjIBMAAIAAODIp6s3Ih5AAIAAA4IMBPkIB3AAIAAxoIBNAAIAASzg");
    var mask_6_graphics_63 = new cjs.Graphics().p("AEgJaIoKqjIAADUIhMAAIAAk2IjRkQIAAieIDpAAIIJKjIAAqjIBMAAIAAODIp6s3Ih5AAIAAA4IMBPkIB3AAIAAxoIBNAAIAASzg");
    var mask_6_graphics_64 = new cjs.Graphics().p("AEgJaIoKqjIAAEjIhMAAIAAmFIjRkQIAAieIDpAAIIJKjIAAqjIBMAAIAAODIp6s3Ih5AAIAAA4IMBPkIB3AAIAAxoIBNAAIAASzg");
    var mask_6_graphics_65 = new cjs.Graphics().p("AEgJaIoKqjIAAFtIhMAAIAAnPIjRkQIAAieIDpAAIIJKjIAAqjIBMAAIAAODIp6s3Ih5AAIAAA4IMBPkIB3AAIAAxoIBNAAIAASzg");
    var mask_6_graphics_66 = new cjs.Graphics().p("AEgJaIoKqjIAAG/IhMAAIAAohIjRkQIAAieIDpAAIIJKjIAAqjIBMAAIAAODIp6s3Ih5AAIAAA4IMBPkIB3AAIAAxoIBNAAIAASzg");
    var mask_6_graphics_67 = new cjs.Graphics().p("AEgJaIoKqjIAAIJIhMAAIAAprIjRkQIAAieIDpAAIIJKjIAAqjIBMAAIAAODIp6s3Ih5AAIAAA4IMBPkIB3AAIAAxoIBNAAIAASzg");
    var mask_6_graphics_68 = new cjs.Graphics().p("AEgJaIoKqjIAAJUIhMAAIAAq2IjRkQIAAieIDpAAIIJKjIAAqjIBMAAIAAODIp6s3Ih5AAIAAA4IMBPkIB3AAIAAxoIBNAAIAASzg");
    var mask_6_graphics_69 = new cjs.Graphics().p("AEgJaIoKqjIAAKjIhNAAIAAhLIABAAIAAq6IjRkQIAAieIDpAAIIJKjIAAqjIBMAAIAAODIp6s3Ih5AAIAAA4IMBPkIB3AAIAAxoIBNAAIAASzg");
    var mask_6_graphics_70 = new cjs.Graphics().p("AEgJaIoKqjIAAKjIiKAAIAAhLIA+AAIAAq6IjRkQIAAieIDpAAIIJKjIAAqjIBMAAIAAODIp6s3Ih5AAIAAA4IMBPkIB3AAIAAxoIBNAAIAASzg");
    var mask_6_graphics_71 = new cjs.Graphics().p("AEgJaIoKqjIAAKjIjPAAIAAhLICDAAIAAq6IjRkQIAAieIDpAAIIJKjIAAqjIBMAAIAAODIp6s3Ih5AAIAAA4IMBPkIB3AAIAAxoIBNAAIAASzg");
    var mask_6_graphics_72 = new cjs.Graphics().p("AEgJaIoKqjIAAKjIkdAAIAAhMIBLAAIAAABICGAAIAAq6IjRkQIAAieIDpAAIIJKjIAAqjIBMAAIAAODIp6s3Ih5AAIAAA4IMBPkIB3AAIAAxoIBNAAIAASzg");
    var mask_6_graphics_73 = new cjs.Graphics().p("AEgJaIoKqjIAAKjIkdAAIAAiRIBLAAIAABGICGAAIAAq6IjRkQIAAieIDpAAIIJKjIAAqjIBMAAIAAODIp6s3Ih5AAIAAA4IMBPkIB3AAIAAxoIBNAAIAASzg");
    var mask_6_graphics_74 = new cjs.Graphics().p("AEgJaIoKqjIAAKjIkdAAIAAjZIBLAAIAACOICGAAIAAq6IjRkQIAAieIDpAAIIJKjIAAqjIBMAAIAAODIp6s3Ih5AAIAAA4IMBPkIB3AAIAAxoIBNAAIAASzg");
    var mask_6_graphics_75 = new cjs.Graphics().p("AEgJaIoKqjIAAKjIkdAAIAAkgIBLAAIAADVICGAAIAAq6IjRkQIAAieIDpAAIIJKjIAAqjIBMAAIAAODIp6s3Ih5AAIAAA4IMBPkIB3AAIAAxoIBNAAIAASzg");
    var mask_6_graphics_76 = new cjs.Graphics().p("AEgJaIoKqjIAAKjIkdAAIAAl0IBLAAIAAEpICGAAIAAq6IjRkQIAAieIDpAAIIJKjIAAqjIBMAAIAAODIp6s3Ih5AAIAAA4IMBPkIB3AAIAAxoIBNAAIAASzg");
    var mask_6_graphics_77 = new cjs.Graphics().p("AEgJaIoKqjIAAKjIkdAAIAAnRIBLAAIAAGGICGAAIAAq6IjRkQIAAieIDpAAIIJKjIAAqjIBMAAIAAODIp6s3Ih5AAIAAA4IMBPkIB3AAIAAxoIBNAAIAASzg");
    var mask_6_graphics_78 = new cjs.Graphics().p("AEgJaIoKqjIAAKjIkdAAIAAorIBLAAIAAHgICGAAIAAq6IjRkQIAAieIDpAAIIJKjIAAqjIBMAAIAAODIp6s3Ih5AAIAAA4IMBPkIB3AAIAAxoIBNAAIAASzg");
    var mask_6_graphics_79 = new cjs.Graphics().p("AEgJaIoKqjIAAKjIkdAAIAApyIBLAAIAAInICGAAIAAq6IjRkQIAAieIDpAAIIJKjIAAqjIBMAAIAAODIp6s3Ih5AAIAAA4IMBPkIB3AAIAAxoIBNAAIAASzg");
    var mask_6_graphics_80 = new cjs.Graphics().p("AEgJaIoKqjIAAKjIkdAAIAAq/IBLAAIAAJ0ICGAAIAAq6IjRkQIAAieIDpAAIIJKjIAAqjIBMAAIAAODIp6s3Ih5AAIAAA4IMBPkIB3AAIAAxoIBNAAIAASzg");
    var mask_6_graphics_81 = new cjs.Graphics().p("AEgJaIoKqjIAAKjIkdAAIAAsGIBLAAIAAK7ICGAAIAAq6IjRkQIAAieIDpAAIIJKjIAAqjIBMAAIAAODIp6s3Ih5AAIAAA4IMBPkIB3AAIAAxoIBNAAIAASzg");
    var mask_6_graphics_82 = new cjs.Graphics().p("AEgJaIoKqjIAAKjIkdAAIAAtwIBLBjIAALCICGAAIAAq6IjRkQIAAieIDpAAIIJKjIAAqjIBMAAIAAODIp6s3Ih5AAIAAA4IMBPkIB3AAIAAxoIBNAAIAASzg");
    var mask_6_graphics_119 = new cjs.Graphics().p("AEgJaIoKqjIAAKjIkdAAIAAtwIBLBjIAALCICGAAIAAq6IjRkQIAAieIDpAAIIJKjIAAqjIBMAAIAAODIp6s3Ih5AAIAAA4IMBPkIB3AAIAAxoIBNAAIAASzg");
    var mask_6_graphics_120 = new cjs.Graphics().p("AEgJaIoKqjIAAKjIkdAAIAAtwIBLBjIAALCICGAAIAAq6IjRkQIAAieIDpAAIIJKjIAAqjIBMAAIAAODIp6s3Ih5AAIAAA4IMBPkIB3AAIAAxoIBNAAIAASzg");
    var mask_6_graphics_121 = new cjs.Graphics().p("AEgJaIoKqjIAAKjIkdAAIAAtwIBLBjIAALCICGAAIAAq6IjRkQIAAieIDpAAIIJKjIAAqjIBMAAIAAODIp6s3Ih5AAIAAA4IMBPkIB3AAIAAxoIBNAAIAASzg");
    var mask_6_graphics_122 = new cjs.Graphics().p("AEgJaIoKqjIAAKjIkdAAIAAtwIBLBjIAALCICGAAIAAq6IjRkQIAAieIDpAAIIJKjIAAqjIBMAAIAAODIp6s3Ih5AAIAAA4IMBPkIB3AAIAAxoIBNAAIAASzg");
    var mask_6_graphics_123 = new cjs.Graphics().p("AEgJaIoKqjIAAKjIkdAAIAAtwIBLBjIAALCICGAAIAAq6IjRkQIAAieIDpAAIIJKjIAAqjIBMAAIAAODIp6s3Ih5AAIAAA4IMBPkIB3AAIAAxoIBNAAIAASzg");
    var mask_6_graphics_124 = new cjs.Graphics().p("AEgJaIoKqjIAAKjIkdAAIAAtwIBLBjIAALCICGAAIAAq6IjRkQIAAieIDpAAIIJKjIAAqjIBMAAIAAODIp6s3Ih5AAIAAA4IMBPkIB3AAIAAxoIBNAAIAASzg");
    var mask_6_graphics_125 = new cjs.Graphics().p("AEgJaIoKqjIAAKjIkdAAIAAtwIBLBjIAALCICGAAIAAq6IjRkQIAAieIDpAAIIJKjIAAqjIBMAAIAAODIp6s3Ih5AAIAAA4IMBPkIB3AAIAAxoIBNAAIAASzg");
    var mask_6_graphics_126 = new cjs.Graphics().p("AEgJaIoKqjIAAKjIkdAAIAAtwIBLBjIAALCICGAAIAAq6IjRkQIAAieIDpAAIIJKjIAAqjIBMAAIAAODIp6s3Ih5AAIAAA4IMBPkIB3AAIAAxoIBNAAIAASzg");
    var mask_6_graphics_127 = new cjs.Graphics().p("AEgJaIoKqjIAAKjIkdAAIAAtwIBLBjIAALCICGAAIAAq6IjRkQIAAieIDpAAIIJKjIAAqjIBMAAIAAODIp6s3Ih5AAIAAA4IMBPkIB3AAIAAxoIBNAAIAASzg");
    var mask_6_graphics_128 = new cjs.Graphics().p("AEgJaIoKqjIAAKjIkdAAIAAtwIBLBjIAALCICGAAIAAq6IjRkQIAAieIDpAAIIJKjIAAqjIBMAAIAAODIp6s3Ih5AAIAAA4IMBPkIB3AAIAAxoIBNAAIAASzg");
    var mask_6_graphics_129 = new cjs.Graphics().p("AEgJaIoKqjIAAKjIkdAAIAAtwIBLBjIAALCICGAAIAAq6IjRkQIAAieIDpAAIIJKjIAAqjIBMAAIAAODIp6s3Ih5AAIAAA4IMBPkIB3AAIAAxoIBNAAIAASzg");
    var mask_6_graphics_130 = new cjs.Graphics().p("AEgJaIoKqjIAAKjIkdAAIAAtwIBLBjIAALCICGAAIAAq6IjRkQIAAieIDpAAIIJKjIAAqjIBMAAIAAODIp6s3Ih5AAIAAA4IMBPkIB3AAIAAxoIBNAAIAASzg");
    var mask_6_graphics_131 = new cjs.Graphics().p("AEgJaIoKqjIAAKjIkdAAIAAtwIBLBjIAALCICGAAIAAq6IjRkQIAAieIDpAAIIJKjIAAqjIBMAAIAAODIp6s3Ih5AAIAAA4IMBPkIB3AAIAAxoIBNAAIAASzg");
    var mask_6_graphics_132 = new cjs.Graphics().p("AEgJaIoKqjIAAKjIkdAAIAAtwIBLBjIAALCICGAAIAAq6IjRkQIAAieIDpAAIIJKjIAAqjIBMAAIAAODIp6s3Ih5AAIAAA4IMBPkIB3AAIAAxoIBNAAIAASzg");
    var mask_6_graphics_133 = new cjs.Graphics().p("AEgJaIoKqjIAAKjIkdAAIAAtwIBLBjIAALCICGAAIAAq6IjRkQIAAieIDpAAIIJKjIAAqjIBMAAIAAODIp6s3Ih5AAIAAA4IMBPkIB3AAIAAxoIBNAAIAASzg");
    var mask_6_graphics_134 = new cjs.Graphics().p("AEgJaIoKqjIAAKjIkdAAIAAtwIBLBjIAALCICGAAIAAq6IjRkQIAAieIDpAAIIJKjIAAqjIBMAAIAAODIp6s3Ih5AAIAAA4IMBPkIB3AAIAAxoIBNAAIAASzg");
  
    this.timeline.addTween(cjs.Tween.get(mask_6).to({graphics:null,x:0,y:0}).wait(1).to({graphics:mask_6_graphics_1,x:263.4,y:50.4}).wait(1).to({graphics:mask_6_graphics_2,x:265.8,y:53.4}).wait(1).to({graphics:mask_6_graphics_3,x:267.8,y:55.8}).wait(1).to({graphics:mask_6_graphics_4,x:270,y:58.5}).wait(1).to({graphics:mask_6_graphics_5,x:272.4,y:61.8}).wait(1).to({graphics:mask_6_graphics_6,x:274.7,y:64.8}).wait(1).to({graphics:mask_6_graphics_7,x:277.1,y:68.2}).wait(1).to({graphics:mask_6_graphics_8,x:279.9,y:71.6}).wait(1).to({graphics:mask_6_graphics_9,x:282.1,y:74.7}).wait(1).to({graphics:mask_6_graphics_10,x:284.9,y:78.3}).wait(1).to({graphics:mask_6_graphics_11,x:287.7,y:82.1}).wait(1).to({graphics:mask_6_graphics_12,x:290.2,y:82.1}).wait(1).to({graphics:mask_6_graphics_13,x:293.9,y:82.1}).wait(1).to({graphics:mask_6_graphics_14,x:297.6,y:82.1}).wait(1).to({graphics:mask_6_graphics_15,x:297.6,y:82.1}).wait(1).to({graphics:mask_6_graphics_16,x:297.6,y:82.1}).wait(1).to({graphics:mask_6_graphics_17,x:297.6,y:82.1}).wait(1).to({graphics:mask_6_graphics_18,x:297.6,y:82.1}).wait(1).to({graphics:mask_6_graphics_19,x:297.6,y:82.1}).wait(1).to({graphics:mask_6_graphics_20,x:297.6,y:82.1}).wait(1).to({graphics:mask_6_graphics_21,x:297.6,y:82.1}).wait(1).to({graphics:mask_6_graphics_22,x:297.6,y:82.1}).wait(1).to({graphics:mask_6_graphics_23,x:297.6,y:82.1}).wait(1).to({graphics:mask_6_graphics_24,x:297.6,y:82}).wait(1).to({graphics:mask_6_graphics_25,x:297.6,y:78.2}).wait(1).to({graphics:mask_6_graphics_26,x:297.6,y:75.1}).wait(1).to({graphics:mask_6_graphics_27,x:297.6,y:71.7}).wait(1).to({graphics:mask_6_graphics_28,x:297.6,y:68.1}).wait(1).to({graphics:mask_6_graphics_29,x:297.6,y:65.1}).wait(1).to({graphics:mask_6_graphics_30,x:297.6,y:61.8}).wait(1).to({graphics:mask_6_graphics_31,x:297.6,y:61.8}).wait(1).to({graphics:mask_6_graphics_32,x:297.6,y:61.8}).wait(1).to({graphics:mask_6_graphics_33,x:297.6,y:61.8}).wait(1).to({graphics:mask_6_graphics_34,x:297.6,y:61.8}).wait(1).to({graphics:mask_6_graphics_35,x:297.6,y:61.8}).wait(1).to({graphics:mask_6_graphics_36,x:297.6,y:61.8}).wait(1).to({graphics:mask_6_graphics_37,x:297.6,y:61.8}).wait(1).to({graphics:mask_6_graphics_38,x:297.6,y:61.8}).wait(1).to({graphics:mask_6_graphics_39,x:297.6,y:61.8}).wait(1).to({graphics:mask_6_graphics_40,x:297.6,y:61.8}).wait(1).to({graphics:mask_6_graphics_41,x:297.6,y:61.8}).wait(1).to({graphics:mask_6_graphics_42,x:297.6,y:61.8}).wait(1).to({graphics:mask_6_graphics_43,x:297.6,y:61.8}).wait(1).to({graphics:mask_6_graphics_44,x:297.6,y:61.8}).wait(1).to({graphics:mask_6_graphics_45,x:297.6,y:61.8}).wait(1).to({graphics:mask_6_graphics_46,x:297.6,y:61.8}).wait(1).to({graphics:mask_6_graphics_47,x:297.6,y:61.8}).wait(1).to({graphics:mask_6_graphics_48,x:297.6,y:61.8}).wait(1).to({graphics:mask_6_graphics_49,x:297.6,y:61.8}).wait(1).to({graphics:mask_6_graphics_50,x:294.9,y:61.8}).wait(1).to({graphics:mask_6_graphics_51,x:290.7,y:61.8}).wait(1).to({graphics:mask_6_graphics_52,x:287.3,y:61.8}).wait(1).to({graphics:mask_6_graphics_53,x:283.3,y:61.8}).wait(1).to({graphics:mask_6_graphics_54,x:283.3,y:61.8}).wait(1).to({graphics:mask_6_graphics_55,x:283.3,y:61.8}).wait(1).to({graphics:mask_6_graphics_56,x:283.3,y:61.8}).wait(1).to({graphics:mask_6_graphics_57,x:283.3,y:61.8}).wait(1).to({graphics:mask_6_graphics_58,x:283.3,y:61.8}).wait(1).to({graphics:mask_6_graphics_59,x:283.3,y:61.8}).wait(1).to({graphics:mask_6_graphics_60,x:283.3,y:61.8}).wait(1).to({graphics:mask_6_graphics_61,x:283.3,y:61.8}).wait(1).to({graphics:mask_6_graphics_62,x:283.3,y:61.8}).wait(1).to({graphics:mask_6_graphics_63,x:283.3,y:61.8}).wait(1).to({graphics:mask_6_graphics_64,x:283.3,y:61.8}).wait(1).to({graphics:mask_6_graphics_65,x:283.3,y:61.8}).wait(1).to({graphics:mask_6_graphics_66,x:283.3,y:61.8}).wait(1).to({graphics:mask_6_graphics_67,x:283.3,y:61.8}).wait(1).to({graphics:mask_6_graphics_68,x:283.3,y:61.8}).wait(1).to({graphics:mask_6_graphics_69,x:283.3,y:61.8}).wait(1).to({graphics:mask_6_graphics_70,x:283.3,y:61.8}).wait(1).to({graphics:mask_6_graphics_71,x:283.3,y:61.8}).wait(1).to({graphics:mask_6_graphics_72,x:283.3,y:61.8}).wait(1).to({graphics:mask_6_graphics_73,x:283.3,y:61.8}).wait(1).to({graphics:mask_6_graphics_74,x:283.3,y:61.8}).wait(1).to({graphics:mask_6_graphics_75,x:283.3,y:61.8}).wait(1).to({graphics:mask_6_graphics_76,x:283.3,y:61.8}).wait(1).to({graphics:mask_6_graphics_77,x:283.3,y:61.8}).wait(1).to({graphics:mask_6_graphics_78,x:283.3,y:61.8}).wait(1).to({graphics:mask_6_graphics_79,x:283.3,y:61.8}).wait(1).to({graphics:mask_6_graphics_80,x:283.3,y:61.8}).wait(1).to({graphics:mask_6_graphics_81,x:283.3,y:61.8}).wait(1).to({graphics:mask_6_graphics_82,x:283.3,y:61.8}).wait(37).to({graphics:mask_6_graphics_119,x:283.3,y:61.8}).wait(1).to({graphics:mask_6_graphics_120,x:283.3,y:61.8}).wait(1).to({graphics:mask_6_graphics_121,x:283.3,y:61.8}).wait(1).to({graphics:mask_6_graphics_122,x:283.3,y:61.8}).wait(1).to({graphics:mask_6_graphics_123,x:283.3,y:61.8}).wait(1).to({graphics:mask_6_graphics_124,x:283.3,y:61.8}).wait(1).to({graphics:mask_6_graphics_125,x:283.3,y:61.8}).wait(1).to({graphics:mask_6_graphics_126,x:283.3,y:61.8}).wait(1).to({graphics:mask_6_graphics_127,x:283.3,y:61.8}).wait(1).to({graphics:mask_6_graphics_128,x:283.3,y:61.8}).wait(1).to({graphics:mask_6_graphics_129,x:283.3,y:61.8}).wait(1).to({graphics:mask_6_graphics_130,x:283.3,y:61.8}).wait(1).to({graphics:mask_6_graphics_131,x:283.3,y:61.8}).wait(1).to({graphics:mask_6_graphics_132,x:283.3,y:61.8}).wait(1).to({graphics:mask_6_graphics_133,x:283.3,y:61.8}).wait(1).to({graphics:mask_6_graphics_134,x:283.3,y:61.8}).wait(1));
  
    // N
    this.shape_6 = new cjs.Shape();
    this.shape_6.graphics.f("#fff").s().p("AEgJaIoKqjIAAKjIkdAAIAAtwIBLBjIAALCICGAAIAAq6IjRkQIAAieIDpAAIIJKjIAAqjIBMAAIAAODIp6s3Ih5AAIAAA4IMBPkIB3AAIAAxoIBNAAIAASzg");
    this.shape_6.setTransform(283.3,61.8);
  
    this.instance_12 = new lib.Tween31("synched",0);
    this.instance_12.parent = this;
    this.instance_12.setTransform(283.3,61.8);
    this.instance_12._off = true;
  
    this.instance_13 = new lib.Tween32("synched",0);
    this.instance_13.parent = this;
    this.instance_13.setTransform(283.3,61.8);
    this.instance_13.alpha = 0;
  
    var maskedShapeInstanceList = [this.shape_6,this.instance_12,this.instance_13];
  
    for(var shapedInstanceItr = 0; shapedInstanceItr < maskedShapeInstanceList.length; shapedInstanceItr++) {
      maskedShapeInstanceList[shapedInstanceItr].mask = mask_6;
    }
  
    this.timeline.addTween(cjs.Tween.get({}).to({state:[]}).to({state:[{t:this.shape_6}]},1).to({state:[{t:this.instance_12}]},118).to({state:[{t:this.instance_13}]},15).wait(1));
    this.timeline.addTween(cjs.Tween.get(this.instance_12).wait(119).to({_off:false},0).to({_off:true,alpha:0},15).wait(1));
  
    // E mask (mask)
    var mask_7 = new cjs.Shape();
    mask_7._off = true;
    var mask_7_graphics_1 = new cjs.Graphics().p("AglAdIAAg5IBLAAIAAA5g");
    var mask_7_graphics_2 = new cjs.Graphics().p("AglA6IAAhzIBLAAIAABzg");
    var mask_7_graphics_3 = new cjs.Graphics().p("AgmBhIAAjBIBNAAIAABMIgCAAIAAB1g");
    var mask_7_graphics_4 = new cjs.Graphics().p("Ag/BhIAAjBIB/AAIAABMIg0AAIAAB1g");
    var mask_7_graphics_5 = new cjs.Graphics().p("AhjBhIAAjBIDHAAIAABMIh7AAIAAB1g");
    var mask_7_graphics_6 = new cjs.Graphics().p("AiIBhIAAjBIERAAIAABMIjGAAIAAB1g");
    var mask_7_graphics_7 = new cjs.Graphics().p("AitBhIAAjBIFbAAIAABMIkPAAIAAB1g");
    var mask_7_graphics_8 = new cjs.Graphics().p("AjUBhIAAjBIGpAAIAABMIldAAIAAB1g");
    var mask_7_graphics_9 = new cjs.Graphics().p("Aj2BhIAAjBIHuAAIAABMImjAAIAAB1g");
    var mask_7_graphics_10 = new cjs.Graphics().p("AkaBhIAAjBII2AAIAABMInrAAIAAB1g");
    var mask_7_graphics_11 = new cjs.Graphics().p("AlABhIAAjBIKBAAIAABMIo1AAIAAB1g");
    var mask_7_graphics_12 = new cjs.Graphics().p("AlrBhIAAjBILXAAIAABMIqLAAIAAB1g");
    var mask_7_graphics_13 = new cjs.Graphics().p("AmTBhIAAjBIMnAAIAABMIrbAAIAAB1g");
    var mask_7_graphics_14 = new cjs.Graphics().p("Am5BhIAAjBIN0AAIAABMIspAAIAAB1g");
    var mask_7_graphics_15 = new cjs.Graphics().p("Am5BhIAAjBIN0AAIAAB7IhMAAIAAgvIrdAAIAAB1g");
    var mask_7_graphics_16 = new cjs.Graphics().p("AFvBiIAAh2IrdAAIAAB1IhLAAIAAjCIN0AAIAADDg");
    var mask_7_graphics_17 = new cjs.Graphics().p("AFvCHIAAjBIrdAAIAAB1IhLAAIAAjBIN0AAIAAENg");
    var mask_7_graphics_18 = new cjs.Graphics().p("AEhCHIAAhMIBOAAIAAh1IrdAAIAAB1IhLAAIAAjBIN0AAIAAENg");
    var mask_7_graphics_19 = new cjs.Graphics().p("ADgCHIAAhMICPAAIAAh1IrdAAIAAB1IhLAAIAAjBIN0AAIAAENg");
    var mask_7_graphics_20 = new cjs.Graphics().p("ACcCHIAAhMIDTAAIAAh1IrdAAIAAB1IhLAAIAAjBIN0AAIAAENg");
    var mask_7_graphics_21 = new cjs.Graphics().p("ABOCHIAAhMIEhAAIAAh1IrdAAIAAB1IhLAAIAAjBIN0AAIAAENg");
    var mask_7_graphics_22 = new cjs.Graphics().p("AAFCHIAAhMIFqAAIAAh1IrdAAIAAB1IhLAAIAAjBIN0AAIAAENg");
    var mask_7_graphics_23 = new cjs.Graphics().p("Ag9CHIAAhMIGsAAIAAh1IrdAAIAAB1IhLAAIAAjBIN0AAIAAENg");
    var mask_7_graphics_24 = new cjs.Graphics().p("Ah+CHIAAhMIHtAAIAAh1IrdAAIAAB1IhLAAIAAjBIN0AAIAAENg");
    var mask_7_graphics_25 = new cjs.Graphics().p("AjNCHIAAhMII8AAIAAh1IrdAAIAAB1IhLAAIAAjBIN0AAIAAENg");
    var mask_7_graphics_26 = new cjs.Graphics().p("AkeCHIAAhMIKNAAIAAh1IrdAAIAAB1IhLAAIAAjBIN0AAIAAENg");
    var mask_7_graphics_27 = new cjs.Graphics().p("AlsCHIAAhMILbAAIAAh1IrdAAIAAB1IhLAAIAAjBIN0AAIAAENg");
    var mask_7_graphics_28 = new cjs.Graphics().p("Am5CHIAAkNIN0AAIAAENgAluA7ILdAAIAAh1IrdAAg");
    var mask_7_graphics_29 = new cjs.Graphics().p("Am5EEIAAhMIBPAAIAABMgAm5AKIAAkNIN0AAIAAENgAluhBILdAAIAAh2IrdAAg");
    var mask_7_graphics_30 = new cjs.Graphics().p("Am5EEIAAhMICYAAIAABMgAm5AKIAAkNIN0AAIAAENgAluhBILdAAIAAh2IrdAAg");
    var mask_7_graphics_31 = new cjs.Graphics().p("Am5EEIAAhMIDiAAIAABMgAm5AKIAAkNIN0AAIAAENgAluhBILdAAIAAh2IrdAAg");
    var mask_7_graphics_32 = new cjs.Graphics().p("Am5EEIAAhMIErAAIAABMgAm5AKIAAkNIN0AAIAAENgAluhBILdAAIAAh2IrdAAg");
    var mask_7_graphics_33 = new cjs.Graphics().p("Am5EEIAAhMIF5AAIAABMgAm5AKIAAkNIN0AAIAAENgAluhBILdAAIAAh2IrdAAg");
    var mask_7_graphics_34 = new cjs.Graphics().p("Am5EEIAAhMIHMAAIAABMgAm5AKIAAkNIN0AAIAAENgAluhBILdAAIAAh2IrdAAg");
    var mask_7_graphics_35 = new cjs.Graphics().p("Am5EEIAAhMIIbAAIAABMgAm5AKIAAkNIN0AAIAAENgAluhBILdAAIAAh2IrdAAg");
    var mask_7_graphics_36 = new cjs.Graphics().p("Am5EEIAAhMIJgAAIAABMgAm5AKIAAkNIN0AAIAAENgAluhBILdAAIAAh2IrdAAg");
    var mask_7_graphics_37 = new cjs.Graphics().p("Am5EEIAAhMIKqAAIAABMgAm5AKIAAkNIN0AAIAAENgAluhBILdAAIAAh2IrdAAg");
    var mask_7_graphics_38 = new cjs.Graphics().p("Am5EEIAAhMIL3AAIAABJIhQAAIAAADgAm5AKIAAkNIN0AAIAAENgAluhBILdAAIAAh2IrdAAg");
    var mask_7_graphics_39 = new cjs.Graphics().p("ADyEhIAAg6IqrAAIAAhMIL3AAIAACGgAm5gSIAAkOIN0AAIAAEOgAluheILdAAIAAh2IrdAAg");
    var mask_7_graphics_40 = new cjs.Graphics().p("ADyFAIAAh4IqrAAIAAhMIL3AAIAADEgAm5gxIAAkOIN0AAIAAEOgAluh9ILdAAIAAh2IrdAAg");
    var mask_7_graphics_41 = new cjs.Graphics().p("ADyFlIAAjCIqrAAIAAhMIL3AAIAAEOgAm5hWIAAkOIN0AAIAAEOgAluiiILdAAIAAh2IrdAAg");
    var mask_7_graphics_42 = new cjs.Graphics().p("ACwFlIAAhMIBCAAIAAh2IqrAAIAAhMIL3AAIAAEOgAm5hWIAAkOIN0AAIAAEOgAluiiILdAAIAAh2IrdAAg");
    var mask_7_graphics_43 = new cjs.Graphics().p("ABfFlIAAhMICTAAIAAh2IqrAAIAAhMIL3AAIAAEOgAm5hWIAAkOIN0AAIAAEOgAluiiILdAAIAAh2IrdAAg");
    var mask_7_graphics_44 = new cjs.Graphics().p("AAZFlIAAhMIDZAAIAAh2IqrAAIAAhMIL3AAIAAEOgAm5hWIAAkOIN0AAIAAEOgAluiiILdAAIAAh2IrdAAg");
    var mask_7_graphics_45 = new cjs.Graphics().p("Ag5FlIAAhMIErAAIAAh2IqrAAIAAhMIL3AAIAAEOgAm5hWIAAkOIN0AAIAAEOgAluiiILdAAIAAh2IrdAAg");
    var mask_7_graphics_46 = new cjs.Graphics().p("AiHFlIAAhMIF5AAIAAh2IqrAAIAAhMIL3AAIAAEOgAm5hWIAAkOIN0AAIAAEOgAluiiILdAAIAAh2IrdAAg");
    var mask_7_graphics_47 = new cjs.Graphics().p("AjYFlIAAhMIHKAAIAAh2IqrAAIAAhMIL3AAIAAEOgAm5hWIAAkOIN0AAIAAEOgAluiiILdAAIAAh2IrdAAg");
    var mask_7_graphics_48 = new cjs.Graphics().p("AkfFlIAAhMIIRAAIAAh2IqrAAIAAhMIL3AAIAAEOgAm5hWIAAkOIN0AAIAAEOgAluiiILdAAIAAh2IrdAAg");
    var mask_7_graphics_49 = new cjs.Graphics().p("AlqFlIAAhMIJcAAIAAh2IqrAAIAAhMIL3AAIAAEOgAm5hWIAAkOIN0AAIAAEOgAluiiILdAAIAAh2IrdAAg");
    var mask_7_graphics_50 = new cjs.Graphics().p("AlsFlIAAgDIhNAAIAAhJIKrAAIAAh2IqrAAIAAhMIL3AAIAAEOgAm5hWIAAkOIN0AAIAAEOgAluiiILdAAIAAh2IrdAAg");
    var mask_7_graphics_51 = new cjs.Graphics().p("Am5FzIAAhoIKrAAIAAh1IqrAAIAAhNIL3AAIAAEOIqsAAIAAAcgAm5hkIAAkOIN0AAIAAEOgAluiwILdAAIAAh2IrdAAg");
    var mask_7_graphics_52 = new cjs.Graphics().p("Am5GXIAAiwIKrAAIAAh1IqrAAIAAhMIL3AAIAAEOIqsAAIAABjgAm5iIIAAkOIN0AAIAAEOgAlujTILdAAIAAh2IrdAAg");
    var mask_7_graphics_53 = new cjs.Graphics().p("Am5GzIAAjoIKrAAIAAh2IqrAAIAAhMIL3AAIAAEOIqsAAIAACcgAm5ikIAAkOIN0AAIAAEOgAlujwILdAAIAAh2IrdAAg");
    var mask_7_graphics_54 = new cjs.Graphics().p("Am5HUIAAkqIKrAAIAAh2IqrAAIAAhLIL3AAIAAENIqsAAIAADcIACAAIAAACgAm5jFIAAkOIN0AAIAAEOgAlukRILdAAIAAh2IrdAAg");
    var mask_7_graphics_55 = new cjs.Graphics().p("Am5H6IAAl2IKrAAIAAh1IqrAAIAAhLIL3AAIAAENIqsAAIAADcIACAAIAABNgAm5jrIAAkOIN0AAIAAEOgAluk2ILdAAIAAh2IrdAAg");
    var mask_7_graphics_56 = new cjs.Graphics().p("Am5H6IAAl2IKrAAIAAh1IqrAAIAAhLIL3AAIAAENIqsAAIAADcIBNAAIAABNgAm5jrIAAkOIN0AAIAAEOgAluk2ILdAAIAAh2IrdAAg");
    var mask_7_graphics_57 = new cjs.Graphics().p("Am5H6IAAl2IKrAAIAAh1IqrAAIAAhLIL3AAIAAENIqsAAIAADcICWAAIAABNgAm5jrIAAkOIN0AAIAAEOgAluk2ILdAAIAAh2IrdAAg");
    var mask_7_graphics_58 = new cjs.Graphics().p("Am5H6IAAl2IKrAAIAAh1IqrAAIAAhLIL3AAIAAENIqsAAIAADcIDgAAIAABNgAm5jrIAAkOIN0AAIAAEOgAluk2ILdAAIAAh2IrdAAg");
    var mask_7_graphics_59 = new cjs.Graphics().p("Am5H6IAAl2IKrAAIAAh1IqrAAIAAhLIL3AAIAAENIqsAAIAADcIEwAAIAABNgAm5jrIAAkOIN0AAIAAEOgAluk2ILdAAIAAh2IrdAAg");
    var mask_7_graphics_60 = new cjs.Graphics().p("Am5H6IAAl2IKrAAIAAh1IqrAAIAAhLIL3AAIAAENIqsAAIAADcIF0AAIAABNgAm5jrIAAkOIN0AAIAAEOgAluk2ILdAAIAAh2IrdAAg");
    var mask_7_graphics_61 = new cjs.Graphics().p("Am5H6IAAl2IKrAAIAAh1IqrAAIAAhLIL3AAIAAENIqsAAIAADcIHBAAIAABNgAm5jrIAAkOIN0AAIAAEOgAluk2ILdAAIAAh2IrdAAg");
    var mask_7_graphics_62 = new cjs.Graphics().p("Am5H6IAAl2IKrAAIAAh1IqrAAIAAhLIL3AAIAAENIqsAAIAADcIIJAAIAABNgAm5jrIAAkOIN0AAIAAEOgAluk2ILdAAIAAh2IrdAAg");
    var mask_7_graphics_63 = new cjs.Graphics().p("Am5H6IAAl2IKrAAIAAh1IqrAAIAAhLIL3AAIAAENIqsAAIAADcIJaAAIAABNgAm5jrIAAkOIN0AAIAAEOgAluk2ILdAAIAAh2IrdAAg");
    var mask_7_graphics_64 = new cjs.Graphics().p("Am5H6IAAl2IKrAAIAAh1IqrAAIAAhLIL3AAIAAENIqsAAIAADcIKdAAIAABNgAm5jrIAAkOIN0AAIAAEOgAluk2ILdAAIAAh2IrdAAg");
    var mask_7_graphics_65 = new cjs.Graphics().p("Am5H6IAAl2IKrAAIAAh1IqrAAIAAhLIL3AAIAAENIqsAAIAADcILsAAIAABNgAm5jrIAAkOIN0AAIAAEOgAluk2ILdAAIAAh2IrdAAg");
    var mask_7_graphics_66 = new cjs.Graphics().p("AnBH6IAAl2IKrAAIAAh1IqrAAIAAhLIL3AAIAAENIqrAAIAADcIM3AAIAABNgAnBjrIAAkOIN0AAIAAEOgAl1k2ILcAAIAAh2IrcAAg");
    var mask_7_graphics_67 = new cjs.Graphics().p("AF2ITIAAgzIs3AAIAAl1IKrAAIAAh0IqrAAIAAhNIL3AAIAAENIqrAAIAADdIM3AAIAAB/gAnBkEIAAkOIN0AAIAAEOgAl1lQILcAAIAAh2IrcAAg");
    var mask_7_graphics_68 = new cjs.Graphics().p("AFyI1IAAgCIAEAAIAAh1Is3AAIAAl1IKrAAIAAh1IqrAAIAAhMIL3AAIAAENIqrAAIAADcIM3AAIAADEgAnBkmIAAkOIN0AAIAAEOgAl1lyILcAAIAAh2IrcAAg");
    var mask_7_graphics_69 = new cjs.Graphics().p("AFyJaIAAhMIAEAAIAAh1Is3AAIAAl1IKrAAIAAh1IqrAAIAAhMIL3AAIAAENIqrAAIAADcIM3AAIAAEOgAnBlLIAAkOIN0AAIAAEOgAl1mXILcAAIAAh2IrcAAg");
    var mask_7_graphics_70 = new cjs.Graphics().p("AElJaIAAhMIBRAAIAAh1Is3AAIAAl1IKrAAIAAh1IqrAAIAAhMIL3AAIAAENIqrAAIAADcIM3AAIAAEOgAnBlLIAAkOIN0AAIAAEOgAl1mXILcAAIAAh2IrcAAg");
    var mask_7_graphics_71 = new cjs.Graphics().p("ADUJaIAAhMICiAAIAAh1Is3AAIAAl1IKrAAIAAh1IqrAAIAAhMIL3AAIAAENIqrAAIAADcIM3AAIAAEOgAnBlLIAAkOIN0AAIAAEOgAl1mXILcAAIAAh2IrcAAg");
    var mask_7_graphics_72 = new cjs.Graphics().p("AB+JaIAAhMID4AAIAAh1Is3AAIAAl1IKrAAIAAh1IqrAAIAAhMIL3AAIAAENIqrAAIAADcIM3AAIAAEOgAnBlLIAAkOIN0AAIAAEOgAl1mXILcAAIAAh2IrcAAg");
    var mask_7_graphics_73 = new cjs.Graphics().p("AAkJaIAAhMIFSAAIAAh1Is3AAIAAl1IKrAAIAAh1IqrAAIAAhMIL3AAIAAENIqrAAIAADcIM3AAIAAEOgAnBlLIAAkOIN0AAIAAEOgAl1mXILcAAIAAh2IrcAAg");
    var mask_7_graphics_74 = new cjs.Graphics().p("AhDJaIAAhMIG5AAIAAh1Is3AAIAAl1IKrAAIAAh1IqrAAIAAhMIL3AAIAAENIqrAAIAADcIM3AAIAAEOgAnBlLIAAkOIN0AAIAAEOgAl1mXILcAAIAAh2IrcAAg");
    var mask_7_graphics_75 = new cjs.Graphics().p("AidJaIAAhMIITAAIAAh1Is3AAIAAl1IKrAAIAAh1IqrAAIAAhMIL3AAIAAENIqrAAIAADcIM3AAIAAEOgAnBlLIAAkOIN0AAIAAEOgAl1mXILcAAIAAh2IrcAAg");
    var mask_7_graphics_76 = new cjs.Graphics().p("AkFJaIAAhMIJ7AAIAAh1Is3AAIAAl1IKrAAIAAh1IqrAAIAAhMIL3AAIAAENIqrAAIAADcIM3AAIAAEOgAnBlLIAAkOIN0AAIAAEOgAl1mXILcAAIAAh2IrcAAg");
    var mask_7_graphics_77 = new cjs.Graphics().p("AlfJaIAAhMILVAAIAAh1Is3AAIAAl1IKrAAIAAh1IqrAAIAAhMIL3AAIAAENIqrAAIAADcIM3AAIAAEOgAnBlLIAAkOIN0AAIAAEOgAl1mXILcAAIAAh2IrcAAg");
    var mask_7_graphics_78 = new cjs.Graphics().p("AnBJaIAAhMIM3AAIAAh1Is3AAIAAl1IKrAAIAAh1IqrAAIAAhMIL3AAIAAENIqrAAIAADcIM3AAIAAEOgAnBlLIAAkOIN0AAIAAEOgAl1mXILcAAIAAh2IrcAAg");
    var mask_7_graphics_119 = new cjs.Graphics().p("AnBJaIAAhMIM3AAIAAh1Is3AAIAAl1IKrAAIAAh1IqrAAIAAhMIL3AAIAAENIqrAAIAADcIM3AAIAAEOgAnBlLIAAkOIN0AAIAAEOgAl1mXILcAAIAAh2IrcAAg");
    var mask_7_graphics_120 = new cjs.Graphics().p("AnBJaIAAhMIM3AAIAAh1Is3AAIAAl1IKrAAIAAh1IqrAAIAAhMIL3AAIAAENIqrAAIAADcIM3AAIAAEOgAnBlLIAAkOIN0AAIAAEOgAl1mXILcAAIAAh2IrcAAg");
    var mask_7_graphics_121 = new cjs.Graphics().p("AnBJaIAAhMIM3AAIAAh1Is3AAIAAl1IKrAAIAAh1IqrAAIAAhMIL3AAIAAENIqrAAIAADcIM3AAIAAEOgAnBlLIAAkOIN0AAIAAEOgAl1mXILcAAIAAh2IrcAAg");
    var mask_7_graphics_122 = new cjs.Graphics().p("AnBJaIAAhMIM3AAIAAh1Is3AAIAAl1IKrAAIAAh1IqrAAIAAhMIL3AAIAAENIqrAAIAADcIM3AAIAAEOgAnBlLIAAkOIN0AAIAAEOgAl1mXILcAAIAAh2IrcAAg");
    var mask_7_graphics_123 = new cjs.Graphics().p("AnBJaIAAhMIM3AAIAAh1Is3AAIAAl1IKrAAIAAh1IqrAAIAAhMIL3AAIAAENIqrAAIAADcIM3AAIAAEOgAnBlLIAAkOIN0AAIAAEOgAl1mXILcAAIAAh2IrcAAg");
    var mask_7_graphics_124 = new cjs.Graphics().p("AnBJaIAAhMIM3AAIAAh1Is3AAIAAl1IKrAAIAAh1IqrAAIAAhMIL3AAIAAENIqrAAIAADcIM3AAIAAEOgAnBlLIAAkOIN0AAIAAEOgAl1mXILcAAIAAh2IrcAAg");
    var mask_7_graphics_125 = new cjs.Graphics().p("AnBJaIAAhMIM3AAIAAh1Is3AAIAAl1IKrAAIAAh1IqrAAIAAhMIL3AAIAAENIqrAAIAADcIM3AAIAAEOgAnBlLIAAkOIN0AAIAAEOgAl1mXILcAAIAAh2IrcAAg");
    var mask_7_graphics_126 = new cjs.Graphics().p("AnBJaIAAhMIM3AAIAAh1Is3AAIAAl1IKrAAIAAh1IqrAAIAAhMIL3AAIAAENIqrAAIAADcIM3AAIAAEOgAnBlLIAAkOIN0AAIAAEOgAl1mXILcAAIAAh2IrcAAg");
    var mask_7_graphics_127 = new cjs.Graphics().p("AnBJaIAAhMIM3AAIAAh1Is3AAIAAl1IKrAAIAAh1IqrAAIAAhMIL3AAIAAENIqrAAIAADcIM3AAIAAEOgAnBlLIAAkOIN0AAIAAEOgAl1mXILcAAIAAh2IrcAAg");
    var mask_7_graphics_128 = new cjs.Graphics().p("AnBJaIAAhMIM3AAIAAh1Is3AAIAAl1IKrAAIAAh1IqrAAIAAhMIL3AAIAAENIqrAAIAADcIM3AAIAAEOgAnBlLIAAkOIN0AAIAAEOgAl1mXILcAAIAAh2IrcAAg");
    var mask_7_graphics_129 = new cjs.Graphics().p("AnBJaIAAhMIM3AAIAAh1Is3AAIAAl1IKrAAIAAh1IqrAAIAAhMIL3AAIAAENIqrAAIAADcIM3AAIAAEOgAnBlLIAAkOIN0AAIAAEOgAl1mXILcAAIAAh2IrcAAg");
    var mask_7_graphics_130 = new cjs.Graphics().p("AnBJaIAAhMIM3AAIAAh1Is3AAIAAl1IKrAAIAAh1IqrAAIAAhMIL3AAIAAENIqrAAIAADcIM3AAIAAEOgAnBlLIAAkOIN0AAIAAEOgAl1mXILcAAIAAh2IrcAAg");
    var mask_7_graphics_131 = new cjs.Graphics().p("AnBJaIAAhMIM3AAIAAh1Is3AAIAAl1IKrAAIAAh1IqrAAIAAhMIL3AAIAAENIqrAAIAADcIM3AAIAAEOgAnBlLIAAkOIN0AAIAAEOgAl1mXILcAAIAAh2IrcAAg");
    var mask_7_graphics_132 = new cjs.Graphics().p("AnBJaIAAhMIM3AAIAAh1Is3AAIAAl1IKrAAIAAh1IqrAAIAAhMIL3AAIAAENIqrAAIAADcIM3AAIAAEOgAnBlLIAAkOIN0AAIAAEOgAl1mXILcAAIAAh2IrcAAg");
    var mask_7_graphics_133 = new cjs.Graphics().p("AnBJaIAAhMIM3AAIAAh1Is3AAIAAl1IKrAAIAAh1IqrAAIAAhMIL3AAIAAENIqrAAIAADcIM3AAIAAEOgAnBlLIAAkOIN0AAIAAEOgAl1mXILcAAIAAh2IrcAAg");
    var mask_7_graphics_134 = new cjs.Graphics().p("AnBJaIAAhMIM3AAIAAh1Is3AAIAAl1IKrAAIAAh1IqrAAIAAhMIL3AAIAAENIqrAAIAADcIM3AAIAAEOgAnBlLIAAkOIN0AAIAAEOgAl1mXILcAAIAAh2IrcAAg");
  
    this.timeline.addTween(cjs.Tween.get(mask_7).to({graphics:null,x:0,y:0}).wait(1).to({graphics:mask_7_graphics_1,x:126.8,y:18.1}).wait(1).to({graphics:mask_7_graphics_2,x:126.8,y:15.2}).wait(1).to({graphics:mask_7_graphics_3,x:126.9,y:11.3}).wait(1).to({graphics:mask_7_graphics_4,x:129.4,y:11.3}).wait(1).to({graphics:mask_7_graphics_5,x:133,y:11.3}).wait(1).to({graphics:mask_7_graphics_6,x:136.8,y:11.3}).wait(1).to({graphics:mask_7_graphics_7,x:140.4,y:11.3}).wait(1).to({graphics:mask_7_graphics_8,x:144.3,y:11.3}).wait(1).to({graphics:mask_7_graphics_9,x:147.8,y:11.3}).wait(1).to({graphics:mask_7_graphics_10,x:151.4,y:11.3}).wait(1).to({graphics:mask_7_graphics_11,x:155.1,y:11.3}).wait(1).to({graphics:mask_7_graphics_12,x:159.4,y:11.3}).wait(1).to({graphics:mask_7_graphics_13,x:163.4,y:11.3}).wait(1).to({graphics:mask_7_graphics_14,x:167.3,y:11.3}).wait(1).to({graphics:mask_7_graphics_15,x:167.3,y:11.3}).wait(1).to({graphics:mask_7_graphics_16,x:167.3,y:11.3}).wait(1).to({graphics:mask_7_graphics_17,x:167.3,y:15.1}).wait(1).to({graphics:mask_7_graphics_18,x:167.3,y:15.1}).wait(1).to({graphics:mask_7_graphics_19,x:167.3,y:15.1}).wait(1).to({graphics:mask_7_graphics_20,x:167.3,y:15.1}).wait(1).to({graphics:mask_7_graphics_21,x:167.3,y:15.1}).wait(1).to({graphics:mask_7_graphics_22,x:167.3,y:15.1}).wait(1).to({graphics:mask_7_graphics_23,x:167.3,y:15.1}).wait(1).to({graphics:mask_7_graphics_24,x:167.3,y:15.1}).wait(1).to({graphics:mask_7_graphics_25,x:167.3,y:15.1}).wait(1).to({graphics:mask_7_graphics_26,x:167.3,y:15.1}).wait(1).to({graphics:mask_7_graphics_27,x:167.3,y:15.1}).wait(1).to({graphics:mask_7_graphics_28,x:167.3,y:15.1}).wait(1).to({graphics:mask_7_graphics_29,x:167.3,y:27.6}).wait(1).to({graphics:mask_7_graphics_30,x:167.3,y:27.6}).wait(1).to({graphics:mask_7_graphics_31,x:167.3,y:27.6}).wait(1).to({graphics:mask_7_graphics_32,x:167.3,y:27.6}).wait(1).to({graphics:mask_7_graphics_33,x:167.3,y:27.6}).wait(1).to({graphics:mask_7_graphics_34,x:167.3,y:27.6}).wait(1).to({graphics:mask_7_graphics_35,x:167.3,y:27.6}).wait(1).to({graphics:mask_7_graphics_36,x:167.3,y:27.6}).wait(1).to({graphics:mask_7_graphics_37,x:167.3,y:27.6}).wait(1).to({graphics:mask_7_graphics_38,x:167.3,y:27.6}).wait(1).to({graphics:mask_7_graphics_39,x:167.3,y:30.5}).wait(1).to({graphics:mask_7_graphics_40,x:167.3,y:33.6}).wait(1).to({graphics:mask_7_graphics_41,x:167.3,y:37.3}).wait(1).to({graphics:mask_7_graphics_42,x:167.3,y:37.3}).wait(1).to({graphics:mask_7_graphics_43,x:167.3,y:37.3}).wait(1).to({graphics:mask_7_graphics_44,x:167.3,y:37.3}).wait(1).to({graphics:mask_7_graphics_45,x:167.3,y:37.3}).wait(1).to({graphics:mask_7_graphics_46,x:167.3,y:37.3}).wait(1).to({graphics:mask_7_graphics_47,x:167.3,y:37.3}).wait(1).to({graphics:mask_7_graphics_48,x:167.3,y:37.3}).wait(1).to({graphics:mask_7_graphics_49,x:167.3,y:37.3}).wait(1).to({graphics:mask_7_graphics_50,x:167.3,y:37.3}).wait(1).to({graphics:mask_7_graphics_51,x:167.3,y:38.7}).wait(1).to({graphics:mask_7_graphics_52,x:167.3,y:42.2}).wait(1).to({graphics:mask_7_graphics_53,x:167.3,y:45.1}).wait(1).to({graphics:mask_7_graphics_54,x:167.3,y:48.4}).wait(1).to({graphics:mask_7_graphics_55,x:167.3,y:52.1}).wait(1).to({graphics:mask_7_graphics_56,x:167.3,y:52.1}).wait(1).to({graphics:mask_7_graphics_57,x:167.3,y:52.1}).wait(1).to({graphics:mask_7_graphics_58,x:167.3,y:52.1}).wait(1).to({graphics:mask_7_graphics_59,x:167.3,y:52.1}).wait(1).to({graphics:mask_7_graphics_60,x:167.3,y:52.1}).wait(1).to({graphics:mask_7_graphics_61,x:167.3,y:52.1}).wait(1).to({graphics:mask_7_graphics_62,x:167.3,y:52.1}).wait(1).to({graphics:mask_7_graphics_63,x:167.3,y:52.1}).wait(1).to({graphics:mask_7_graphics_64,x:167.3,y:52.1}).wait(1).to({graphics:mask_7_graphics_65,x:167.3,y:52.1}).wait(1).to({graphics:mask_7_graphics_66,x:168,y:52.1}).wait(1).to({graphics:mask_7_graphics_67,x:168,y:54.7}).wait(1).to({graphics:mask_7_graphics_68,x:168,y:58.1}).wait(1).to({graphics:mask_7_graphics_69,x:168,y:61.8}).wait(1).to({graphics:mask_7_graphics_70,x:168,y:61.8}).wait(1).to({graphics:mask_7_graphics_71,x:168,y:61.8}).wait(1).to({graphics:mask_7_graphics_72,x:168,y:61.8}).wait(1).to({graphics:mask_7_graphics_73,x:168,y:61.8}).wait(1).to({graphics:mask_7_graphics_74,x:168,y:61.8}).wait(1).to({graphics:mask_7_graphics_75,x:168,y:61.8}).wait(1).to({graphics:mask_7_graphics_76,x:168,y:61.8}).wait(1).to({graphics:mask_7_graphics_77,x:168,y:61.8}).wait(1).to({graphics:mask_7_graphics_78,x:168,y:61.8}).wait(41).to({graphics:mask_7_graphics_119,x:168,y:61.8}).wait(1).to({graphics:mask_7_graphics_120,x:168,y:61.8}).wait(1).to({graphics:mask_7_graphics_121,x:168,y:61.8}).wait(1).to({graphics:mask_7_graphics_122,x:168,y:61.8}).wait(1).to({graphics:mask_7_graphics_123,x:168,y:61.8}).wait(1).to({graphics:mask_7_graphics_124,x:168,y:61.8}).wait(1).to({graphics:mask_7_graphics_125,x:168,y:61.8}).wait(1).to({graphics:mask_7_graphics_126,x:168,y:61.8}).wait(1).to({graphics:mask_7_graphics_127,x:168,y:61.8}).wait(1).to({graphics:mask_7_graphics_128,x:168,y:61.8}).wait(1).to({graphics:mask_7_graphics_129,x:168,y:61.8}).wait(1).to({graphics:mask_7_graphics_130,x:168,y:61.8}).wait(1).to({graphics:mask_7_graphics_131,x:168,y:61.8}).wait(1).to({graphics:mask_7_graphics_132,x:168,y:61.8}).wait(1).to({graphics:mask_7_graphics_133,x:168,y:61.8}).wait(1).to({graphics:mask_7_graphics_134,x:168,y:61.8}).wait(1));
  
    // E
    this.shape_7 = new cjs.Shape();
    this.shape_7.graphics.f("#fff").s().p("AnBJaIAAhMIM3AAIAAh1Is3AAIAAl1IKrAAIAAh1IqrAAIAAhMIL3AAIAAENIqrAAIAADcIM3AAIAAEOgAnBlLIAAkOIN0AAIAAEOgAl1mXILcAAIAAh2IrcAAg");
    this.shape_7.setTransform(168,61.8);
  
    this.instance_14 = new lib.Tween35("synched",0);
    this.instance_14.parent = this;
    this.instance_14.setTransform(168,61.8);
    this.instance_14._off = true;
  
    this.instance_15 = new lib.Tween36("synched",0);
    this.instance_15.parent = this;
    this.instance_15.setTransform(168,61.8);
    this.instance_15.alpha = 0;
  
    var maskedShapeInstanceList = [this.shape_7,this.instance_14,this.instance_15];
  
    for(var shapedInstanceItr = 0; shapedInstanceItr < maskedShapeInstanceList.length; shapedInstanceItr++) {
      maskedShapeInstanceList[shapedInstanceItr].mask = mask_7;
    }
  
    this.timeline.addTween(cjs.Tween.get({}).to({state:[]}).to({state:[{t:this.shape_7}]},1).to({state:[{t:this.instance_14}]},118).to({state:[{t:this.instance_15}]},15).wait(1));
    this.timeline.addTween(cjs.Tween.get(this.instance_14).wait(119).to({_off:false},0).to({_off:true,alpha:0},15).wait(1));
  
    // H mask (mask)
    var mask_8 = new cjs.Shape();
    mask_8._off = true;
    var mask_8_graphics_1 = new cjs.Graphics().p("AgVAmIAAhLIArAAIAABLg");
    var mask_8_graphics_2 = new cjs.Graphics().p("AhKAmIAAhLICVAAIAABLg");
    var mask_8_graphics_3 = new cjs.Graphics().p("AhvAmIAAhLIDfAAIAABLg");
    var mask_8_graphics_4 = new cjs.Graphics().p("AitAmIAAhLIFbAAIAABLg");
    var mask_8_graphics_5 = new cjs.Graphics().p("AjMAmIAAhLIFnAAIAAAAIAyAAIAABLg");
    var mask_8_graphics_6 = new cjs.Graphics().p("AjzAmIAAhLIHnAAIAABLg");
    var mask_8_graphics_7 = new cjs.Graphics().p("AklAmIAAhLIIfAAIAAAAIAsAAIAABLg");
    var mask_8_graphics_8 = new cjs.Graphics().p("AlNAmIAAhLIKbAAIAABLg");
    var mask_8_graphics_9 = new cjs.Graphics().p("Al1AoIAAhPIK/AAIAAABIAsAAIAABLIqfAAIAAADg");
    var mask_8_graphics_10 = new cjs.Graphics().p("Al1BKIAAiTILrAAIAABLIqfAAIAABIg");
    var mask_8_graphics_11 = new cjs.Graphics().p("Al1BrIAAjVILrAAIAABMIqfAAIAACJg");
    var mask_8_graphics_12 = new cjs.Graphics().p("Al1CTIAAklILrAAIAABMIqfAAIAADZg");
    var mask_8_graphics_13 = new cjs.Graphics().p("Al1CyIAAljILrAAIAABNIqfAAIAAEWg");
    var mask_8_graphics_14 = new cjs.Graphics().p("Al1CyIAAljILrAAIAABNIqfAAIAAEWg");
    var mask_8_graphics_15 = new cjs.Graphics().p("Al1DVIAAmpILrAAIAABMIqfAAIAAFdg");
    var mask_8_graphics_16 = new cjs.Graphics().p("Al1DzIAAnlILrAAIAABNIqfAAIAAGYg");
    var mask_8_graphics_17 = new cjs.Graphics().p("Al1EbIAAo1ILrAAIAABMIqfAAIAAHpg");
    var mask_8_graphics_18 = new cjs.Graphics().p("AmQEbIAAhLIA2AAIAAnqILsAAIAABMIqgAAIAAHpg");
    var mask_8_graphics_19 = new cjs.Graphics().p("AmZEbIAAhIIggAAIAAgkIACAAIAAAhICFAAIAAnqILsAAIAABMIqfAAIAAHpg");
    var mask_8_graphics_20 = new cjs.Graphics().p("AneEbIAAgpIBKAAIAAhDIACAAIAAAhICFAAIAAnqILsAAIAABMIqfAAIAAHpg");
    var mask_8_graphics_21 = new cjs.Graphics().p("AneEbIAAhsIBMAAIAAAhICFAAIAAnqILsAAIAABMIqfAAIAAHpg");
    var mask_8_graphics_22 = new cjs.Graphics().p("AneEbIAAjRIBMAAIAACGICFAAIAAnqILsAAIAABMIqfAAIAAHpg");
    var mask_8_graphics_23 = new cjs.Graphics().p("AneEbIAAkfIBMAAIAADUICFAAIAAnqILsAAIAABMIqfAAIAAHpg");
    var mask_8_graphics_24 = new cjs.Graphics().p("AneEbIAAmaIBMAAIAAFPICFAAIAAnqILsAAIAABMIqfAAIAAHpg");
    var mask_8_graphics_25 = new cjs.Graphics().p("AneEbIAAnsIBMAAIAAGhICFAAIAAnqILsAAIAABMIqfAAIAAHpg");
    var mask_8_graphics_26 = new cjs.Graphics().p("AneE8IAAp3IBMAAIAAIsICFAAIAAnqILsAAIAABMIqfAAIAAHpg");
    var mask_8_graphics_27 = new cjs.Graphics().p("AneFhIAArBIBMAAIAAJ1ICFAAIAAnqILsAAIAABNIqfAAIAAHpg");
    var mask_8_graphics_28 = new cjs.Graphics().p("AneGMIAAsXIBMAAIAALMICFAAIAAnrILsAAIAABNIqfAAIAAHpg");
    var mask_8_graphics_29 = new cjs.Graphics().p("AneGwIAAtfIBMAAIAAMUICFAAIAAnrILsAAIAABNIqfAAIAAHpg");
    var mask_8_graphics_30 = new cjs.Graphics().p("AneHgIAAu/IBMAAIAAN0ICFAAIAAnqILsAAIAABMIqfAAIAAHpg");
    var mask_8_graphics_31 = new cjs.Graphics().p("AneIDIAAwFIBMAAIAAO6ICFAAIAAnrILsAAIAABMIqfAAIAAHqg");
    var mask_8_graphics_32 = new cjs.Graphics().p("AneIuIAAxbIBMAAIAAQQICFAAIAAnqILsAAIAABLIqfAAIAAHqg");
    var mask_8_graphics_33 = new cjs.Graphics().p("AneJaIAAyzIBAAAIAABPIAMAAIAAQZICFAAIAAnrILsAAIAABMIqfAAIAAHqg");
    var mask_8_graphics_34 = new cjs.Graphics().p("AneJaIAAyzICFAAIAABMIg5AAIAAQcICFAAIAAnrILsAAIAABMIqfAAIAAHqg");
    var mask_8_graphics_35 = new cjs.Graphics().p("AneJaIAAyzIDPAAIAABMIiDAAIAAQcICFAAIAAnrILsAAIAABMIqfAAIAAHqg");
    var mask_8_graphics_36 = new cjs.Graphics().p("AneJaIAAyzIEeAAIAABRIhNAAIAAgFIiFAAIAAQcICFAAIAAnrILsAAIAABMIqfAAIAAHqg");
    var mask_8_graphics_37 = new cjs.Graphics().p("AneJaIAAyzIEeAAIAACWIhNAAIAAhKIiFAAIAAQcICFAAIAAnrILsAAIAABMIqfAAIAAHqg");
    var mask_8_graphics_38 = new cjs.Graphics().p("AneJaIAAyzIEeAAIAAD3IhNAAIAAirIiFAAIAAQcICFAAIAAnrILsAAIAABMIqfAAIAAHqg");
    var mask_8_graphics_39 = new cjs.Graphics().p("AneJaIAAyzIEeAAIAAF6IhNAAIAAkuIiFAAIAAQcICFAAIAAnrILsAAIAABMIqfAAIAAHqg");
    var mask_8_graphics_40 = new cjs.Graphics().p("AneJaIAAyzIEeAAIAAG8IAEAAIAAABIhRAAIAAlxIiFAAIAAQcICFAAIAAnrILsAAIAABMIqfAAIAAHqg");
    var mask_8_graphics_41 = new cjs.Graphics().p("AneJaIAAyzIEeAAIAAG8IAEAAIAABMIhRAAIAAm8IiFAAIAAQcICFAAIAAnrILsAAIAABMIqfAAIAAHqg");
    var mask_8_graphics_42 = new cjs.Graphics().p("AneJaIAAyzIEeAAIAAG8IAoAAIAABMIh1AAIAAm8IiFAAIAAQcICFAAIAAnrILsAAIAABMIqfAAIAAHqg");
    var mask_8_graphics_43 = new cjs.Graphics().p("AneJaIAAyzIEeAAIAAG8IB7AAIAABMIjIAAIAAm8IiFAAIAAQcICFAAIAAnrILsAAIAABMIqfAAIAAHqg");
    var mask_8_graphics_44 = new cjs.Graphics().p("AneJaIAAyzIEeAAIAAG8IDAAAIAABMIkNAAIAAm8IiFAAIAAQcICFAAIAAnrILsAAIAABMIqfAAIAAHqg");
    var mask_8_graphics_45 = new cjs.Graphics().p("AneJaIAAyzIEeAAIAAG8IEiAAIAABMIlvAAIAAm8IiFAAIAAQcICFAAIAAnrILsAAIAABMIqfAAIAAHqg");
    var mask_8_graphics_46 = new cjs.Graphics().p("AneJaIAAyzIEeAAIAAG8IFnAAIAABMIm0AAIAAm8IiFAAIAAQcICFAAIAAnrILsAAIAABMIqfAAIAAHqg");
    var mask_8_graphics_47 = new cjs.Graphics().p("AneJaIAAyzIEeAAIAAG8IG2AAIAAABIAQAAIAABLIoTAAIAAm8IiFAAIAAQcICFAAIAAnrILsAAIAABMIqfAAIAAHqg");
    var mask_8_graphics_48 = new cjs.Graphics().p("AneJaIAAyzIEeAAIAAG8IG2AAIAAABIBiAAIAABLIplAAIAAm8IiFAAIAAQcICFAAIAAnrILsAAIAABMIqfAAIAAHqg");
    var mask_8_graphics_49 = new cjs.Graphics().p("AneJaIAAyzIEeAAIAAG8IHMAAIAAhoIBMAAIAAC0IplAAIAAm8IiFAAIAAQcICFAAIAAnrILsAAIAABMIqfAAIAAHqg");
    var mask_8_graphics_50 = new cjs.Graphics().p("AneJaIAAyzIEeAAIAAG8IHMAAIAAi7IBMAAIAAEHIplAAIAAm8IiFAAIAAQcICFAAIAAnrILsAAIAABMIqfAAIAAHqg");
    var mask_8_graphics_51 = new cjs.Graphics().p("AneJaIAAyzIEeAAIAAG8IHMAAIAAkGIBMAAIAAFSIplAAIAAm8IiFAAIAAQcICFAAIAAnrILsAAIAABMIqfAAIAAHqg");
    var mask_8_graphics_52 = new cjs.Graphics().p("AneJaIAAyzIEeAAIAAG8IHMAAIAAlRIBMAAIAAGdIplAAIAAm8IiFAAIAAQcICFAAIAAnrILsAAIAABMIqfAAIAAHqg");
    var mask_8_graphics_53 = new cjs.Graphics().p("AneJaIAAyzIEeAAIAAG8IHMAAIAAm8IBLAAIAABgIABAAIAAGoIplAAIAAm8IiFAAIAAQcICFAAIAAnrILsAAIAABMIqfAAIAAHqg");
    var mask_8_graphics_54 = new cjs.Graphics().p("AneJaIAAyzIEeAAIAAG8IHMAAIAAm8IB5AAIAABMIgtAAIAAG8IplAAIAAm8IiFAAIAAQcICFAAIAAnrILsAAIAABMIqfAAIAAHqg");
    var mask_8_graphics_55 = new cjs.Graphics().p("AneJaIAAyzIEeAAIAAG8IHMAAIAAm8IDDAAIAABMIh3AAIAAG8IplAAIAAm8IiFAAIAAQcICFAAIAAnrILsAAIAABMIqfAAIAAHqg");
    var mask_8_graphics_56 = new cjs.Graphics().p("AoDJaIAAyzIEdAAIAAG8IHMAAIAAm8IEeAAIAABMIjRAAIAAG8IplAAIAAm8IiFAAIAAQcICFAAIAAnrILrAAIAABMIqfAAIAAHqg");
    var mask_8_graphics_57 = new cjs.Graphics().p("AoDJaIAAyzIEdAAIAAG8IHMAAIAAm8IEeAAIAAClIhMAAIAAhZIiFAAIAAG8IplAAIAAm8IiFAAIAAQcICFAAIAAnrILrAAIAABMIqfAAIAAHqg");
    var mask_8_graphics_58 = new cjs.Graphics().p("AoDJaIAAyzIEdAAIAAG8IHMAAIAAm8IEeAAIAADlIhMAAIAAiZIiFAAIAAG8IplAAIAAm8IiFAAIAAQcICFAAIAAnrILrAAIAABMIqfAAIAAHqg");
    var mask_8_graphics_59 = new cjs.Graphics().p("AoDJaIAAyzIEdAAIAAG8IHMAAIAAm8IEeAAIAAE5IhMAAIAAjtIiFAAIAAG8IplAAIAAm8IiFAAIAAQcICFAAIAAnrILrAAIAABMIqfAAIAAHqg");
    var mask_8_graphics_60 = new cjs.Graphics().p("AoDJaIAAyzIEdAAIAAG8IHMAAIAAm8IEeAAIAAF1IhMAAIAAkpIiFAAIAAG8IplAAIAAm8IiFAAIAAQcICFAAIAAnrILrAAIAABMIqfAAIAAHqg");
    var mask_8_graphics_61 = new cjs.Graphics().p("AoDJaIAAyzIEdAAIAAG8IHMAAIAAm8IEeAAIAAG3IhMAAIAAlrIiFAAIAAG8IplAAIAAm8IiFAAIAAQcICFAAIAAnrILrAAIAABMIqfAAIAAHqg");
    var mask_8_graphics_62 = new cjs.Graphics().p("AoDJaIAAyzIEdAAIAAG8IHMAAIAAm8IEeAAIAAHsIhLAAIAAABIgBAAIAAmhIiFAAIAAG8IplAAIAAm8IiFAAIAAQcICFAAIAAnrILrAAIAABMIqfAAIAAHqg");
    var mask_8_graphics_63 = new cjs.Graphics().p("AoDJaIAAyzIEdAAIAAG8IHMAAIAAm8IEeAAIAAI8IhMAAIAAnwIiFAAIAAG8IplAAIAAm8IiFAAIAAQcICFAAIAAnrILnAAIAABMIqbAAIAAHqg");
    var mask_8_graphics_64 = new cjs.Graphics().p("AoDJaIAAyzIEdAAIAAG8IHMAAIAAm8IEeAAIAAJvIhMAAIAAojIiFAAIAAG8IplAAIAAm8IiFAAIAAQcICFAAIAAnrILnAAIAABMIqbAAIAAHqg");
    var mask_8_graphics_65 = new cjs.Graphics().p("AoDJaIAAyzIEdAAIAAG8IHMAAIAAm8IEeAAIAAKzIhPAAIAAAWIqbAAIAAHqgAm3IPICFAAIAAnrILqAAIAAoxIiFAAIAAG8IplAAIAAm8IiFAAg");
    var mask_8_graphics_66 = new cjs.Graphics().p("AoDJaIAAyzIEdAAIAAG8IHMAAIAAm8IEeAAIAALlIhMAAIAAgcIqeAAIAAHqgAm3IPICFAAIAAnrILqAAIAAoxIiFAAIAAG8IplAAIAAm8IiFAAg");
    var mask_8_graphics_67 = new cjs.Graphics().p("AoDJaIAAyzIEdAAIAAG8IHMAAIAAm8IEeAAIAAMYIhMAAIAAhPIqeAAIAAHqgAm3IPICFAAIAAnrILqAAIAAoxIiFAAIAAG8IplAAIAAm8IiFAAg");
    var mask_8_graphics_68 = new cjs.Graphics().p("AoDJaIAAyzIEdAAIAAG8IHMAAIAAm8IEeAAIAANWIhMAAIAAiNIqeAAIAAHqgAm3IPICFAAIAAnrILqAAIAAoxIiFAAIAAG8IplAAIAAm8IiFAAg");
    var mask_8_graphics_69 = new cjs.Graphics().p("AoDJaIAAyzIEdAAIAAG8IHMAAIAAm8IEeAAIAAOtIhMAAIAAjkIqeAAIAAHqgAm3IPICFAAIAAnrILqAAIAAoxIiFAAIAAG8IplAAIAAm8IiFAAg");
    var mask_8_graphics_70 = new cjs.Graphics().p("AoDJaIAAyzIEdAAIAAG8IHMAAIAAm8IEeAAIAAP5IhLAAIAAgfIgBAAIAAkRIqeAAIAAHqgAm3IPICFAAIAAnrILqAAIAAoxIiFAAIAAG8IplAAIAAm8IiFAAgAEyGBIAAgNIABAAIAAANg");
    var mask_8_graphics_71 = new cjs.Graphics().p("AoDJaIAAyzIEdAAIAAG8IHMAAIAAm8IEeAAIAAP5IhLAAIAAgfIgBAAIAAkRIqeAAIAAHqgAm3IPICFAAIAAnrILqAAIAAoxIiFAAIAAG8IplAAIAAm8IiFAAgAEyGBIAAgNIABAAIAAANg");
    var mask_8_graphics_72 = new cjs.Graphics().p("AoDJaIAAyzIEdAAIAAG8IHMAAIAAm8IEeAAIAAQuIhLAAIAAhUIgBAAIAAkRIqeAAIAAHqgAm3IPICFAAIAAnrILqAAIAAoxIiFAAIAAG8IplAAIAAm8IiFAAgAEyGBIAAgNIABAAIAAANg");
    var mask_8_graphics_73 = new cjs.Graphics().p("AoDJaIAAyzIEdAAIAAG8IHMAAIAAm8IEeAAIAASEIhLAAIAAiqIgBAAIAAkRIqeAAIAAHqgAm3IPICFAAIAAnrILqAAIAAoxIiFAAIAAG8IplAAIAAm8IiFAAgAEyGBIAAgNIABAAIAAANg");
    var mask_8_graphics_74 = new cjs.Graphics().p("AFwJaIAAhLIBIAAIAAmfIqeAAIAAHqIkdAAIAAyzIEdAAIAAG8IHMAAIAAm8IEeAAIAASzgAm3IPICFAAIAAnrILqAAIAAoxIiFAAIAAG8IplAAIAAm8IiFAAgAEyHTIAAhfIABAAIAABfg");
    var mask_8_graphics_75 = new cjs.Graphics().p("AEyJaIAAjmIABAAIAACbICFAAIAAmfIqeAAIAAHqIkdAAIAAyzIEdAAIAAG8IHMAAIAAm8IEeAAIAASzgAm3IPICFAAIAAnrILqAAIAAoxIiFAAIAAG8IplAAIAAm8IiFAAg");
    var mask_8_graphics_76 = new cjs.Graphics().p("ADmJaIAAhKIBYAAIAAgBIB6AAIAAmfIqeAAIAAHqIkdAAIAAyzIEdAAIAAG8IHMAAIAAm8IEeAAIAASzgAm3IPICFAAIAAnrILqAAIAAoxIiFAAIAAG8IplAAIAAm8IiFAAg");
    var mask_8_graphics_77 = new cjs.Graphics().p("ADmJaIAAiJIBNAAIAAA+ICFAAIAAmfIqeAAIAAHqIkdAAIAAyzIEdAAIAAG8IHMAAIAAm8IEeAAIAASzgAm3IPICFAAIAAnrILqAAIAAoxIiFAAIAAG8IplAAIAAm8IiFAAg");
    var mask_8_graphics_78 = new cjs.Graphics().p("ADmJaIAAjmIBNAAIAACbICFAAIAAmfIqeAAIAAHqIkdAAIAAyzIEdAAIAAG8IHMAAIAAm8IEeAAIAASzgAm3IPICFAAIAAnrILqAAIAAoxIiFAAIAAG8IplAAIAAm8IiFAAg");
    var mask_8_graphics_79 = new cjs.Graphics().p("ADmJaIAAkwIBNAAIAADlICFAAIAAmfIqeAAIAAHqIkdAAIAAyzIEdAAIAAG8IHMAAIAAm8IEeAAIAASzgAm3IPICFAAIAAnrILqAAIAAoxIiFAAIAAG8IplAAIAAm8IiFAAg");
    var mask_8_graphics_80 = new cjs.Graphics().p("ADmJaIAAmKIBNAAIAAE/ICFAAIAAmfIqeAAIAAHqIkdAAIAAyzIEdAAIAAG8IHMAAIAAm8IEeAAIAASzgAm3IPICFAAIAAnrILqAAIAAoxIiFAAIAAG8IplAAIAAm8IiFAAg");
    var mask_8_graphics_119 = new cjs.Graphics().p("ADmJaIAAmKIBNAAIAAE/ICFAAIAAmfIqeAAIAAHqIkdAAIAAyzIEdAAIAAG8IHMAAIAAm8IEeAAIAASzgAm3IPICFAAIAAnrILqAAIAAoxIiFAAIAAG8IplAAIAAm8IiFAAg");
    var mask_8_graphics_120 = new cjs.Graphics().p("ADmJaIAAmKIBNAAIAAE/ICFAAIAAmfIqeAAIAAHqIkdAAIAAyzIEdAAIAAG8IHMAAIAAm8IEeAAIAASzgAm3IPICFAAIAAnrILqAAIAAoxIiFAAIAAG8IplAAIAAm8IiFAAg");
    var mask_8_graphics_121 = new cjs.Graphics().p("ADmJaIAAmKIBNAAIAAE/ICFAAIAAmfIqeAAIAAHqIkdAAIAAyzIEdAAIAAG8IHMAAIAAm8IEeAAIAASzgAm3IPICFAAIAAnrILqAAIAAoxIiFAAIAAG8IplAAIAAm8IiFAAg");
    var mask_8_graphics_122 = new cjs.Graphics().p("ADmJaIAAmKIBNAAIAAE/ICFAAIAAmfIqeAAIAAHqIkdAAIAAyzIEdAAIAAG8IHMAAIAAm8IEeAAIAASzgAm3IPICFAAIAAnrILqAAIAAoxIiFAAIAAG8IplAAIAAm8IiFAAg");
    var mask_8_graphics_123 = new cjs.Graphics().p("ADmJaIAAmKIBNAAIAAE/ICFAAIAAmfIqeAAIAAHqIkdAAIAAyzIEdAAIAAG8IHMAAIAAm8IEeAAIAASzgAm3IPICFAAIAAnrILqAAIAAoxIiFAAIAAG8IplAAIAAm8IiFAAg");
    var mask_8_graphics_124 = new cjs.Graphics().p("ADmJaIAAmKIBNAAIAAE/ICFAAIAAmfIqeAAIAAHqIkdAAIAAyzIEdAAIAAG8IHMAAIAAm8IEeAAIAASzgAm3IPICFAAIAAnrILqAAIAAoxIiFAAIAAG8IplAAIAAm8IiFAAg");
    var mask_8_graphics_125 = new cjs.Graphics().p("ADmJaIAAmKIBNAAIAAE/ICFAAIAAmfIqeAAIAAHqIkdAAIAAyzIEdAAIAAG8IHMAAIAAm8IEeAAIAASzgAm3IPICFAAIAAnrILqAAIAAoxIiFAAIAAG8IplAAIAAm8IiFAAg");
    var mask_8_graphics_126 = new cjs.Graphics().p("ADmJaIAAmKIBNAAIAAE/ICFAAIAAmfIqeAAIAAHqIkdAAIAAyzIEdAAIAAG8IHMAAIAAm8IEeAAIAASzgAm3IPICFAAIAAnrILqAAIAAoxIiFAAIAAG8IplAAIAAm8IiFAAg");
    var mask_8_graphics_127 = new cjs.Graphics().p("ADmJaIAAmKIBNAAIAAE/ICFAAIAAmfIqeAAIAAHqIkdAAIAAyzIEdAAIAAG8IHMAAIAAm8IEeAAIAASzgAm3IPICFAAIAAnrILqAAIAAoxIiFAAIAAG8IplAAIAAm8IiFAAg");
    var mask_8_graphics_128 = new cjs.Graphics().p("ADmJaIAAmKIBNAAIAAE/ICFAAIAAmfIqeAAIAAHqIkdAAIAAyzIEdAAIAAG8IHMAAIAAm8IEeAAIAASzgAm3IPICFAAIAAnrILqAAIAAoxIiFAAIAAG8IplAAIAAm8IiFAAg");
    var mask_8_graphics_129 = new cjs.Graphics().p("ADmJaIAAmKIBNAAIAAE/ICFAAIAAmfIqeAAIAAHqIkdAAIAAyzIEdAAIAAG8IHMAAIAAm8IEeAAIAASzgAm3IPICFAAIAAnrILqAAIAAoxIiFAAIAAG8IplAAIAAm8IiFAAg");
    var mask_8_graphics_130 = new cjs.Graphics().p("ADmJaIAAmKIBNAAIAAE/ICFAAIAAmfIqeAAIAAHqIkdAAIAAyzIEdAAIAAG8IHMAAIAAm8IEeAAIAASzgAm3IPICFAAIAAnrILqAAIAAoxIiFAAIAAG8IplAAIAAm8IiFAAg");
    var mask_8_graphics_131 = new cjs.Graphics().p("ADmJaIAAmKIBNAAIAAE/ICFAAIAAmfIqeAAIAAHqIkdAAIAAyzIEdAAIAAG8IHMAAIAAm8IEeAAIAASzgAm3IPICFAAIAAnrILqAAIAAoxIiFAAIAAG8IplAAIAAm8IiFAAg");
    var mask_8_graphics_132 = new cjs.Graphics().p("ADmJaIAAmKIBNAAIAAE/ICFAAIAAmfIqeAAIAAHqIkdAAIAAyzIEdAAIAAG8IHMAAIAAm8IEeAAIAASzgAm3IPICFAAIAAnrILqAAIAAoxIiFAAIAAG8IplAAIAAm8IiFAAg");
    var mask_8_graphics_133 = new cjs.Graphics().p("ADmJaIAAmKIBNAAIAAE/ICFAAIAAmfIqeAAIAAHqIkdAAIAAyzIEdAAIAAG8IHMAAIAAm8IEeAAIAASzgAm3IPICFAAIAAnrILqAAIAAoxIiFAAIAAG8IplAAIAAm8IiFAAg");
    var mask_8_graphics_134 = new cjs.Graphics().p("ADmJaIAAmKIBNAAIAAE/ICFAAIAAmfIqeAAIAAHqIkdAAIAAyzIEdAAIAAG8IHMAAIAAm8IEeAAIAASzgAm3IPICFAAIAAnrILqAAIAAoxIiFAAIAAG8IplAAIAAm8IiFAAg");
  
    this.timeline.addTween(cjs.Tween.get(mask_8).to({graphics:null,x:0,y:0}).wait(1).to({graphics:mask_8_graphics_1,x:93.6,y:69.2}).wait(1).to({graphics:mask_8_graphics_2,x:88.2,y:69.2}).wait(1).to({graphics:mask_8_graphics_3,x:84.6,y:69.2}).wait(1).to({graphics:mask_8_graphics_4,x:78.3,y:69.2}).wait(1).to({graphics:mask_8_graphics_5,x:75.3,y:69.2}).wait(1).to({graphics:mask_8_graphics_6,x:71.4,y:69.2}).wait(1).to({graphics:mask_8_graphics_7,x:66.3,y:69.2}).wait(1).to({graphics:mask_8_graphics_8,x:62.4,y:69.2}).wait(1).to({graphics:mask_8_graphics_9,x:58.3,y:69.3}).wait(1).to({graphics:mask_8_graphics_10,x:58.3,y:72.8}).wait(1).to({graphics:mask_8_graphics_11,x:58.3,y:76.1}).wait(1).to({graphics:mask_8_graphics_12,x:58.3,y:80.1}).wait(1).to({graphics:mask_8_graphics_13,x:58.3,y:83.1}).wait(1).to({graphics:mask_8_graphics_14,x:58.3,y:83.1}).wait(1).to({graphics:mask_8_graphics_15,x:58.3,y:86.7}).wait(1).to({graphics:mask_8_graphics_16,x:58.3,y:89.6}).wait(1).to({graphics:mask_8_graphics_17,x:58.3,y:93.7}).wait(1).to({graphics:mask_8_graphics_18,x:55.6,y:93.7}).wait(1).to({graphics:mask_8_graphics_19,x:51.6,y:93.7}).wait(1).to({graphics:mask_8_graphics_20,x:47.9,y:93.7}).wait(1).to({graphics:mask_8_graphics_21,x:47.9,y:93.7}).wait(1).to({graphics:mask_8_graphics_22,x:47.9,y:93.7}).wait(1).to({graphics:mask_8_graphics_23,x:47.9,y:93.7}).wait(1).to({graphics:mask_8_graphics_24,x:47.9,y:93.7}).wait(1).to({graphics:mask_8_graphics_25,x:47.9,y:93.7}).wait(1).to({graphics:mask_8_graphics_26,x:47.9,y:90.4}).wait(1).to({graphics:mask_8_graphics_27,x:47.9,y:86.7}).wait(1).to({graphics:mask_8_graphics_28,x:47.9,y:82.4}).wait(1).to({graphics:mask_8_graphics_29,x:47.9,y:78.8}).wait(1).to({graphics:mask_8_graphics_30,x:47.9,y:74}).wait(1).to({graphics:mask_8_graphics_31,x:47.9,y:70.5}).wait(1).to({graphics:mask_8_graphics_32,x:47.9,y:66.2}).wait(1).to({graphics:mask_8_graphics_33,x:47.9,y:61.8}).wait(1).to({graphics:mask_8_graphics_34,x:47.9,y:61.8}).wait(1).to({graphics:mask_8_graphics_35,x:47.9,y:61.8}).wait(1).to({graphics:mask_8_graphics_36,x:47.9,y:61.8}).wait(1).to({graphics:mask_8_graphics_37,x:47.9,y:61.8}).wait(1).to({graphics:mask_8_graphics_38,x:47.9,y:61.8}).wait(1).to({graphics:mask_8_graphics_39,x:47.9,y:61.8}).wait(1).to({graphics:mask_8_graphics_40,x:47.9,y:61.8}).wait(1).to({graphics:mask_8_graphics_41,x:47.9,y:61.8}).wait(1).to({graphics:mask_8_graphics_42,x:47.9,y:61.8}).wait(1).to({graphics:mask_8_graphics_43,x:47.9,y:61.8}).wait(1).to({graphics:mask_8_graphics_44,x:47.9,y:61.8}).wait(1).to({graphics:mask_8_graphics_45,x:47.9,y:61.8}).wait(1).to({graphics:mask_8_graphics_46,x:47.9,y:61.8}).wait(1).to({graphics:mask_8_graphics_47,x:47.9,y:61.8}).wait(1).to({graphics:mask_8_graphics_48,x:47.9,y:61.8}).wait(1).to({graphics:mask_8_graphics_49,x:47.9,y:61.8}).wait(1).to({graphics:mask_8_graphics_50,x:47.9,y:61.8}).wait(1).to({graphics:mask_8_graphics_51,x:47.9,y:61.8}).wait(1).to({graphics:mask_8_graphics_52,x:47.9,y:61.8}).wait(1).to({graphics:mask_8_graphics_53,x:47.9,y:61.8}).wait(1).to({graphics:mask_8_graphics_54,x:47.9,y:61.8}).wait(1).to({graphics:mask_8_graphics_55,x:47.9,y:61.8}).wait(1).to({graphics:mask_8_graphics_56,x:51.6,y:61.8}).wait(1).to({graphics:mask_8_graphics_57,x:51.6,y:61.8}).wait(1).to({graphics:mask_8_graphics_58,x:51.6,y:61.8}).wait(1).to({graphics:mask_8_graphics_59,x:51.6,y:61.8}).wait(1).to({graphics:mask_8_graphics_60,x:51.6,y:61.8}).wait(1).to({graphics:mask_8_graphics_61,x:51.6,y:61.8}).wait(1).to({graphics:mask_8_graphics_62,x:51.6,y:61.8}).wait(1).to({graphics:mask_8_graphics_63,x:51.6,y:61.8}).wait(1).to({graphics:mask_8_graphics_64,x:51.6,y:61.8}).wait(1).to({graphics:mask_8_graphics_65,x:51.6,y:61.8}).wait(1).to({graphics:mask_8_graphics_66,x:51.6,y:61.8}).wait(1).to({graphics:mask_8_graphics_67,x:51.6,y:61.8}).wait(1).to({graphics:mask_8_graphics_68,x:51.6,y:61.8}).wait(1).to({graphics:mask_8_graphics_69,x:51.6,y:61.8}).wait(1).to({graphics:mask_8_graphics_70,x:51.6,y:61.8}).wait(1).to({graphics:mask_8_graphics_71,x:51.6,y:61.8}).wait(1).to({graphics:mask_8_graphics_72,x:51.6,y:61.8}).wait(1).to({graphics:mask_8_graphics_73,x:51.6,y:61.8}).wait(1).to({graphics:mask_8_graphics_74,x:51.6,y:61.8}).wait(1).to({graphics:mask_8_graphics_75,x:51.6,y:61.8}).wait(1).to({graphics:mask_8_graphics_76,x:51.6,y:61.8}).wait(1).to({graphics:mask_8_graphics_77,x:51.6,y:61.8}).wait(1).to({graphics:mask_8_graphics_78,x:51.6,y:61.8}).wait(1).to({graphics:mask_8_graphics_79,x:51.6,y:61.8}).wait(1).to({graphics:mask_8_graphics_80,x:51.6,y:61.8}).wait(39).to({graphics:mask_8_graphics_119,x:51.6,y:61.8}).wait(1).to({graphics:mask_8_graphics_120,x:51.6,y:61.8}).wait(1).to({graphics:mask_8_graphics_121,x:51.6,y:61.8}).wait(1).to({graphics:mask_8_graphics_122,x:51.6,y:61.8}).wait(1).to({graphics:mask_8_graphics_123,x:51.6,y:61.8}).wait(1).to({graphics:mask_8_graphics_124,x:51.6,y:61.8}).wait(1).to({graphics:mask_8_graphics_125,x:51.6,y:61.8}).wait(1).to({graphics:mask_8_graphics_126,x:51.6,y:61.8}).wait(1).to({graphics:mask_8_graphics_127,x:51.6,y:61.8}).wait(1).to({graphics:mask_8_graphics_128,x:51.6,y:61.8}).wait(1).to({graphics:mask_8_graphics_129,x:51.6,y:61.8}).wait(1).to({graphics:mask_8_graphics_130,x:51.6,y:61.8}).wait(1).to({graphics:mask_8_graphics_131,x:51.6,y:61.8}).wait(1).to({graphics:mask_8_graphics_132,x:51.6,y:61.8}).wait(1).to({graphics:mask_8_graphics_133,x:51.6,y:61.8}).wait(1).to({graphics:mask_8_graphics_134,x:51.6,y:61.8}).wait(1));
  
    // H
    this.shape_8 = new cjs.Shape();
    this.shape_8.graphics.f("#fff").s().p("ADmJaIAAmKIBNAAIAAE/ICFAAIAAmfIqeAAIAAHqIkdAAIAAyzIEdAAIAAG8IHMAAIAAm8IEeAAIAASzgAm3IPICFAAIAAnrILqAAIAAoxIiFAAIAAG8IplAAIAAm8IiFAAg");
    this.shape_8.setTransform(51.6,61.8);
  
    this.instance_16 = new lib.Tween39("synched",0);
    this.instance_16.parent = this;
    this.instance_16.setTransform(51.6,61.8);
    this.instance_16._off = true;
  
    this.instance_17 = new lib.Tween40("synched",0);
    this.instance_17.parent = this;
    this.instance_17.setTransform(51.6,61.8);
    this.instance_17.alpha = 0;
  
    var maskedShapeInstanceList = [this.shape_8,this.instance_16,this.instance_17];
  
    for(var shapedInstanceItr = 0; shapedInstanceItr < maskedShapeInstanceList.length; shapedInstanceItr++) {
      maskedShapeInstanceList[shapedInstanceItr].mask = mask_8;
    }
  
    this.timeline.addTween(cjs.Tween.get({}).to({state:[]}).to({state:[{t:this.shape_8}]},1).to({state:[{t:this.instance_16}]},118).to({state:[{t:this.instance_17}]},15).wait(1));
    this.timeline.addTween(cjs.Tween.get(this.instance_16).wait(119).to({_off:false},0).to({_off:true,alpha:0},15).wait(1));
  
  }).prototype = p = new cjs.MovieClip();
  p.nominalBounds = null;
  
  
  (lib.Acultureofopportunity = function(mode,startPosition,loop) {
    this.initialize(mode,startPosition,loop,{});
  
    // Y
    this.instance = new lib.Tween43("synched",0);
    this.instance.parent = this;
    this.instance.setTransform(742.4,12.5);
    this.instance.alpha = 0;
    this.instance._off = true;
  
    this.instance_1 = new lib.Tween44("synched",0);
    this.instance_1.parent = this;
    this.instance_1.setTransform(742.4,12.5);
  
    this.timeline.addTween(cjs.Tween.get({}).to({state:[]}).to({state:[{t:this.instance}]},75).to({state:[{t:this.instance_1}]},9).to({state:[]},31).wait(20));
    this.timeline.addTween(cjs.Tween.get(this.instance).wait(75).to({_off:false},0).to({_off:true,alpha:1},9).wait(51));
  
    // T
    this.instance_2 = new lib.Tween45("synched",0);
    this.instance_2.parent = this;
    this.instance_2.setTransform(710.2,12.5);
    this.instance_2.alpha = 0;
    this.instance_2._off = true;
  
    this.instance_3 = new lib.Tween46("synched",0);
    this.instance_3.parent = this;
    this.instance_3.setTransform(710.2,12.5);
  
    this.timeline.addTween(cjs.Tween.get({}).to({state:[]}).to({state:[{t:this.instance_2}]},70).to({state:[{t:this.instance_3}]},9).to({state:[]},37).wait(19));
    this.timeline.addTween(cjs.Tween.get(this.instance_2).wait(70).to({_off:false},0).to({_off:true,alpha:1},9).wait(56));
  
    // I
    this.instance_4 = new lib.Tween47("synched",0);
    this.instance_4.parent = this;
    this.instance_4.setTransform(683.7,12.5);
    this.instance_4.alpha = 0;
    this.instance_4._off = true;
  
    this.instance_5 = new lib.Tween48("synched",0);
    this.instance_5.parent = this;
    this.instance_5.setTransform(683.7,12.5);
  
    this.timeline.addTween(cjs.Tween.get({}).to({state:[]}).to({state:[{t:this.instance_4}]},67).to({state:[{t:this.instance_5}]},9).to({state:[]},41).wait(18));
    this.timeline.addTween(cjs.Tween.get(this.instance_4).wait(67).to({_off:false},0).to({_off:true,alpha:1},9).wait(59));
  
    // N
    this.instance_6 = new lib.Tween49("synched",0);
    this.instance_6.parent = this;
    this.instance_6.setTransform(654,12.5);
    this.instance_6.alpha = 0;
    this.instance_6._off = true;
  
    this.instance_7 = new lib.Tween50("synched",0);
    this.instance_7.parent = this;
    this.instance_7.setTransform(654,12.5);
  
    this.timeline.addTween(cjs.Tween.get({}).to({state:[]}).to({state:[{t:this.instance_6}]},63).to({state:[{t:this.instance_7}]},9).to({state:[]},46).wait(17));
    this.timeline.addTween(cjs.Tween.get(this.instance_6).wait(63).to({_off:false},0).to({_off:true,alpha:1},9).wait(63));
  
    // U
    this.instance_8 = new lib.Tween51("synched",0);
    this.instance_8.parent = this;
    this.instance_8.setTransform(616.7,12.8);
    this.instance_8.alpha = 0;
    this.instance_8._off = true;
  
    this.instance_9 = new lib.Tween52("synched",0);
    this.instance_9.parent = this;
    this.instance_9.setTransform(616.7,12.8);
  
    this.timeline.addTween(cjs.Tween.get({}).to({state:[]}).to({state:[{t:this.instance_8}]},59).to({state:[{t:this.instance_9}]},9).to({state:[]},51).wait(16));
    this.timeline.addTween(cjs.Tween.get(this.instance_8).wait(59).to({_off:false},0).to({_off:true,alpha:1},9).wait(67));
  
    // T
    this.instance_10 = new lib.Tween53("synched",0);
    this.instance_10.parent = this;
    this.instance_10.setTransform(582.8,12.5);
    this.instance_10.alpha = 0;
    this.instance_10._off = true;
  
    this.instance_11 = new lib.Tween54("synched",0);
    this.instance_11.parent = this;
    this.instance_11.setTransform(582.8,12.5);
  
    this.timeline.addTween(cjs.Tween.get({}).to({state:[]}).to({state:[{t:this.instance_10}]},55).to({state:[{t:this.instance_11}]},9).to({state:[]},56).wait(15));
    this.timeline.addTween(cjs.Tween.get(this.instance_10).wait(55).to({_off:false},0).to({_off:true,alpha:1},9).wait(71));
  
    // R
    this.instance_12 = new lib.Tween55("synched",0);
    this.instance_12.parent = this;
    this.instance_12.setTransform(551.6,12.5);
    this.instance_12.alpha = 0;
    this.instance_12._off = true;
  
    this.instance_13 = new lib.Tween56("synched",0);
    this.instance_13.parent = this;
    this.instance_13.setTransform(551.6,12.5);
  
    this.timeline.addTween(cjs.Tween.get({}).to({state:[]}).to({state:[{t:this.instance_12}]},51).to({state:[{t:this.instance_13}]},9).to({state:[]},61).wait(14));
    this.timeline.addTween(cjs.Tween.get(this.instance_12).wait(51).to({_off:false},0).to({_off:true,alpha:1},9).wait(75));
  
    // O
    this.instance_14 = new lib.Tween57("synched",0);
    this.instance_14.parent = this;
    this.instance_14.setTransform(514,12.5);
    this.instance_14.alpha = 0;
    this.instance_14._off = true;
  
    this.instance_15 = new lib.Tween58("synched",0);
    this.instance_15.parent = this;
    this.instance_15.setTransform(514,12.5);
  
    this.timeline.addTween(cjs.Tween.get({}).to({state:[]}).to({state:[{t:this.instance_14}]},47).to({state:[{t:this.instance_15}]},9).to({state:[]},66).wait(13));
    this.timeline.addTween(cjs.Tween.get(this.instance_14).wait(47).to({_off:false},0).to({_off:true,alpha:1},9).wait(79));
  
    // P
    this.instance_16 = new lib.Tween59("synched",0);
    this.instance_16.parent = this;
    this.instance_16.setTransform(478.3,12.5);
    this.instance_16.alpha = 0;
    this.instance_16._off = true;
  
    this.instance_17 = new lib.Tween60("synched",0);
    this.instance_17.parent = this;
    this.instance_17.setTransform(478.3,12.5);
  
    this.timeline.addTween(cjs.Tween.get({}).to({state:[]}).to({state:[{t:this.instance_16}]},43).to({state:[{t:this.instance_17}]},9).to({state:[]},71).wait(12));
    this.timeline.addTween(cjs.Tween.get(this.instance_16).wait(43).to({_off:false},0).to({_off:true,alpha:1},9).wait(83));
  
    // P
    this.instance_18 = new lib.Tween61("synched",0);
    this.instance_18.parent = this;
    this.instance_18.setTransform(444.8,12.5);
    this.instance_18.alpha = 0;
    this.instance_18._off = true;
  
    this.instance_19 = new lib.Tween62("synched",0);
    this.instance_19.parent = this;
    this.instance_19.setTransform(444.8,12.5);
  
    this.timeline.addTween(cjs.Tween.get({}).to({state:[]}).to({state:[{t:this.instance_18}]},39).to({state:[{t:this.instance_19}]},9).to({state:[]},76).wait(11));
    this.timeline.addTween(cjs.Tween.get(this.instance_18).wait(39).to({_off:false},0).to({_off:true,alpha:1},9).wait(87));
  
    // O
    this.instance_20 = new lib.Tween63("synched",0);
    this.instance_20.parent = this;
    this.instance_20.setTransform(407.7,12.5);
    this.instance_20.alpha = 0;
    this.instance_20._off = true;
  
    this.instance_21 = new lib.Tween64("synched",0);
    this.instance_21.parent = this;
    this.instance_21.setTransform(407.7,12.5);
  
    this.timeline.addTween(cjs.Tween.get({}).to({state:[]}).to({state:[{t:this.instance_20}]},35).to({state:[{t:this.instance_21}]},9).to({state:[]},81).wait(10));
    this.timeline.addTween(cjs.Tween.get(this.instance_20).wait(35).to({_off:false},0).to({_off:true,alpha:1},9).wait(91));
  
    // F
    this.instance_22 = new lib.Tween41("synched",0);
    this.instance_22.parent = this;
    this.instance_22.setTransform(350.4,12.5);
    this.instance_22.alpha = 0;
    this.instance_22._off = true;
  
    this.timeline.addTween(cjs.Tween.get(this.instance_22).wait(31).to({_off:false},0).to({alpha:1},9).to({_off:true},86).wait(9));
  
    // O
    this.instance_23 = new lib.Tween65("synched",0);
    this.instance_23.parent = this;
    this.instance_23.setTransform(319.4,12.5);
    this.instance_23.alpha = 0;
    this.instance_23._off = true;
  
    this.instance_24 = new lib.Tween66("synched",0);
    this.instance_24.parent = this;
    this.instance_24.setTransform(319.4,12.5);
  
    this.timeline.addTween(cjs.Tween.get({}).to({state:[]}).to({state:[{t:this.instance_23}]},27).to({state:[{t:this.instance_24}]},9).to({state:[]},91).wait(8));
    this.timeline.addTween(cjs.Tween.get(this.instance_23).wait(27).to({_off:false},0).to({_off:true,alpha:1},9).wait(99));
  
    // E
    this.instance_25 = new lib.Tween67("synched",0);
    this.instance_25.parent = this;
    this.instance_25.setTransform(266.9,12.5);
    this.instance_25.alpha = 0;
    this.instance_25._off = true;
  
    this.instance_26 = new lib.Tween68("synched",0);
    this.instance_26.parent = this;
    this.instance_26.setTransform(266.9,12.5);
  
    this.timeline.addTween(cjs.Tween.get({}).to({state:[]}).to({state:[{t:this.instance_25}]},24).to({state:[{t:this.instance_26}]},9).to({state:[]},95).wait(7));
    this.timeline.addTween(cjs.Tween.get(this.instance_25).wait(24).to({_off:false},0).to({_off:true,alpha:1},9).wait(102));
  
    // R
    this.instance_27 = new lib.Tween69("synched",0);
    this.instance_27.parent = this;
    this.instance_27.setTransform(233.6,12.5);
    this.instance_27.alpha = 0;
    this.instance_27._off = true;
  
    this.instance_28 = new lib.Tween70("synched",0);
    this.instance_28.parent = this;
    this.instance_28.setTransform(233.6,12.5);
  
    this.timeline.addTween(cjs.Tween.get({}).to({state:[]}).to({state:[{t:this.instance_27}]},20).to({state:[{t:this.instance_28}]},9).to({state:[]},100).wait(6));
    this.timeline.addTween(cjs.Tween.get(this.instance_27).wait(20).to({_off:false},0).to({_off:true,alpha:1},9).wait(106));
  
    // U
    this.instance_29 = new lib.Tween71("synched",0);
    this.instance_29.parent = this;
    this.instance_29.setTransform(197.1,12.8);
    this.instance_29.alpha = 0;
    this.instance_29._off = true;
  
    this.instance_30 = new lib.Tween72("synched",0);
    this.instance_30.parent = this;
    this.instance_30.setTransform(197.1,12.8);
  
    this.timeline.addTween(cjs.Tween.get({}).to({state:[]}).to({state:[{t:this.instance_29}]},16).to({state:[{t:this.instance_30}]},9).to({state:[]},105).wait(5));
    this.timeline.addTween(cjs.Tween.get(this.instance_29).wait(16).to({_off:false},0).to({_off:true,alpha:1},9).wait(110));
  
    // T
    this.instance_31 = new lib.Tween73("synched",0);
    this.instance_31.parent = this;
    this.instance_31.setTransform(163.2,12.5);
    this.instance_31.alpha = 0;
    this.instance_31._off = true;
  
    this.instance_32 = new lib.Tween74("synched",0);
    this.instance_32.parent = this;
    this.instance_32.setTransform(163.2,12.5);
  
    this.timeline.addTween(cjs.Tween.get({}).to({state:[]}).to({state:[{t:this.instance_31}]},13).to({state:[{t:this.instance_32}]},9).to({state:[]},109).wait(4));
    this.timeline.addTween(cjs.Tween.get(this.instance_31).wait(13).to({_off:false},0).to({_off:true,alpha:1},9).wait(113));
  
    // L
    this.instance_33 = new lib.Tween75("synched",0);
    this.instance_33.parent = this;
    this.instance_33.setTransform(135.6,12.5);
    this.instance_33.alpha = 0;
    this.instance_33._off = true;
  
    this.instance_34 = new lib.Tween76("synched",0);
    this.instance_34.parent = this;
    this.instance_34.setTransform(135.6,12.5);
  
    this.timeline.addTween(cjs.Tween.get({}).to({state:[]}).to({state:[{t:this.instance_33}]},9).to({state:[{t:this.instance_34}]},9).to({state:[]},114).wait(3));
    this.timeline.addTween(cjs.Tween.get(this.instance_33).wait(9).to({_off:false},0).to({_off:true,alpha:1},9).wait(117));
  
    // U
    this.instance_35 = new lib.Tween77("synched",0);
    this.instance_35.parent = this;
    this.instance_35.setTransform(101,12.8);
    this.instance_35.alpha = 0;
    this.instance_35._off = true;
  
    this.instance_36 = new lib.Tween78("synched",0);
    this.instance_36.parent = this;
    this.instance_36.setTransform(101,12.8);
  
    this.timeline.addTween(cjs.Tween.get({}).to({state:[]}).to({state:[{t:this.instance_35}]},6).to({state:[{t:this.instance_36}]},9).to({state:[]},118).wait(2));
    this.timeline.addTween(cjs.Tween.get(this.instance_35).wait(6).to({_off:false},0).to({_off:true,alpha:1},9).wait(120));
  
    // C
    this.instance_37 = new lib.Tween79("synched",0);
    this.instance_37.parent = this;
    this.instance_37.setTransform(65.3,12.5);
    this.instance_37.alpha = 0;
    this.instance_37._off = true;
  
    this.instance_38 = new lib.Tween80("synched",0);
    this.instance_38.parent = this;
    this.instance_38.setTransform(65.3,12.5);
  
    this.timeline.addTween(cjs.Tween.get({}).to({state:[]}).to({state:[{t:this.instance_37}]},3).to({state:[{t:this.instance_38}]},9).to({state:[]},122).wait(1));
    this.timeline.addTween(cjs.Tween.get(this.instance_37).wait(3).to({_off:false},0).to({_off:true,alpha:1},9).wait(123));
  
    // A
    this.instance_39 = new lib.Tween81("synched",0);
    this.instance_39.parent = this;
    this.instance_39.setTransform(12.5,12.5);
    this.instance_39.alpha = 0;
  
    this.instance_40 = new lib.Tween82("synched",0);
    this.instance_40.parent = this;
    this.instance_40.setTransform(12.5,12.5);
  
    this.timeline.addTween(cjs.Tween.get({}).to({state:[{t:this.instance_39}]}).to({state:[{t:this.instance_40}]},9).wait(126));
    this.timeline.addTween(cjs.Tween.get(this.instance_39).to({_off:true,alpha:1},9).wait(126));
  
  }).prototype = p = new cjs.MovieClip();
  p.nominalBounds = new cjs.Rectangle(0,0.7,25,23.6);
  
  
  // stage content:
  (lib.Logoanimation = function(mode,startPosition,loop) {
    this.initialize(mode,startPosition,loop,{});
  
    // HENDERSON
    this.instance = new lib.Symbol1();
    this.instance.parent = this;
  
    this.timeline.addTween(cjs.Tween.get(this.instance).wait(135));
  
    // A CULTURE OF OPPORTUNITY
    this.instance_1 = new lib.Acultureofopportunity();
    this.instance_1.parent = this;
    this.instance_1.setTransform(500,174.3,1,1,0,0,0,377,12.5);
  
    this.timeline.addTween(cjs.Tween.get(this.instance_1).wait(135));
  
  }).prototype = p = new cjs.MovieClip();
  p.nominalBounds = new cjs.Rectangle(623,255.9,25,23.6);
  // library properties:
  lib.properties = {
    id: '329A3B38FA005A488196959DEBA036E3',
    width: 1000,
    height: 187,
    fps: 24,
    color: "#FFFFFF",
    opacity: 1.00,
    manifest: [],
    preloads: []
  };
  
  
  
  // bootstrap callback support:
  
  (lib.Stage = function(canvas) {
    createjs.Stage.call(this, canvas);
  }).prototype = p = new createjs.Stage();
  
  p.setAutoPlay = function(autoPlay) {
    this.tickEnabled = autoPlay;
  }
  p.play = function() { this.tickEnabled = true; this.getChildAt(0).gotoAndPlay(this.getTimelinePosition()) }
  p.stop = function(ms) { if(ms) this.seek(ms); this.tickEnabled = false; }
  p.seek = function(ms) { this.tickEnabled = true; this.getChildAt(0).gotoAndStop(lib.properties.fps * ms / 1000); }
  p.getDuration = function() { return this.getChildAt(0).totalFrames / lib.properties.fps * 1000; }
  
  p.getTimelinePosition = function() { return this.getChildAt(0).currentFrame / lib.properties.fps * 1000; }
  
  an.bootcompsLoaded = an.bootcompsLoaded || [];
  if(!an.bootstrapListeners) {
    an.bootstrapListeners=[];
  }
  
  an.bootstrapCallback=function(fnCallback) {
    an.bootstrapListeners.push(fnCallback);
    if(an.bootcompsLoaded.length > 0) {
      for(var i=0; i<an.bootcompsLoaded.length; ++i) {
        fnCallback(an.bootcompsLoaded[i]);
      }
    }
  };
  
  an.compositions = an.compositions || {};
  an.compositions['329A3B38FA005A488196959DEBA036E3'] = {
    getStage: function() { return exportRoot.getStage(); },
    getLibrary: function() { return lib; },
    getSpriteSheet: function() { return ss; },
    getImages: function() { return img; }
  };
  
  an.compositionLoaded = function(id) {
    an.bootcompsLoaded.push(id);
    for(var j=0; j<an.bootstrapListeners.length; j++) {
      an.bootstrapListeners[j](id);
    }
  }
  
  an.getComposition = function(id) {
    return an.compositions[id];
  }



})(createjs = createjs||{}, AdobeAn = AdobeAn||{});
var createjs, AdobeAn;

$(window).on('load', init);