<?php 

require_once('phpmailer/PHPMailerAutoload.php');
require_once('recaptchalib.php');

$mail = new PHPMailer;
$mail->CharSet = 'utf-8';

$name = $_POST['name'];
$email = $_POST['email'];
$company_name = $_POST['business_name'] ? "Company name: " . $_POST['business_name'] : '';
// $company_size = $_POST['business_size'] ? "Company size: " . $_POST['business_size'] : '';
$company_title = $_POST['company_title'] ? "Company size: " . $_POST['company_title'] : '';
$comment = $_POST['user_comment'];

// recapcha
// $secret = '6LfpIXMUAAAAABt4-jHhGKowrMwYr9DlOWVJhcUv';
// $response_capcha = null;
// $reCaptcha = new ReCaptcha($secret);

// if ($_POST["g-recaptcha-response"]) {
//     $response_capcha = $reCaptcha->verifyResponse(
//         $_SERVER["REMOTE_ADDR"],
//         $_POST["g-recaptcha-response"]
//     );
// }

$result = [];

if (empty($name) || empty($email) || empty($comment)) {
    $result['status'] = 'error';
    $result['data'] = [$name, $email, $comment];
    $result['message'] = 'You must fill all required fields';
    echo json_encode($result); 
    exit;
}

if ($response_capcha != null && $response_capcha->success) {

    //$mail->SMTPDebug = 2;                               // Enable verbose debug output

    $mail->isSMTP();                                      // Set mailer to use SMTP
    $mail->Host = 'smtp.gmail.com';  					  // Specify main and backup SMTP servers
    $mail->SMTPAuth = true;                               // Enable SMTP authentication
    $mail->Username = 'emailfortests.db@gmail.com';       // Email login
    $mail->Password = 'testTEST1';                        // Email password
    $mail->SMTPSecure = 'tls';                            // Enable TLS encryption, `ssl` also accepted
    $mail->Port = 587;                                    // TCP port to connect to / этот порт может отличаться у других провайдеров

    $mail->setFrom($email);                               // From
    // $mail->addAddress('barbra.coffee@cityofhenderson.com');          // To
    $mail->addAddress('cro55er@icloud.com');          // To
    // $mail->addAddress('nilesh@madebychance.com');               // Name is optional
    //$mail->addReplyTo('info@example.com', 'Information');
    //$mail->addCC('cc@example.com');
    //$mail->addBCC('bcc@example.com');
    //$mail->addAttachment('/var/tmp/file.tar.gz');         // Add attachments
    //$mail->addAttachment('/tmp/image.jpg', 'new.jpg');    // Optional name
    $mail->isHTML(true);                                  // Set email format to HTML

    $mail->Subject = 'Contact-form Hendersonnow.com';
    // $mail->Body    = 'From: ' . $email . '<br>Name: ' . $name . '<br>' . $company_name . '<br>' . $company_size . '<br><br>' . $comment;
    $mail->Body    = 'From: ' . $email . '<br>Name: ' . $name . '<br> Company Title: ' . $company_title . '<br>' . $company_size . '<br><br>' . $comment;
    $mail->AltBody = '';


    if(!$mail->send()) {
        $result['status'] = 'error';
        $result['message'] = 'Error with sending email. Please check your email and try again';
    } else {
        $result['status'] = 'success';
    }

    echo json_encode($result); 

} else {
    $result['status'] = 'error';
    $result['message'] = 'You must fill a reCapcha';
    echo json_encode($result); 
    exit;
}


?>
