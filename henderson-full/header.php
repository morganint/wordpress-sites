<!DOCTYPE html>
<html <?php language_attributes(); ?>>

<head>

  <!-- Google Tag Manager -->
  <!-- <script>
    (function(w,d,s,l,i){w[l]=w[l]||[];w[l].push({'gtm.start':
    new Date().getTime(),event:'gtm.js'});var f=d.getElementsByTagName(s)[0],
    j=d.createElement(s),dl=l!='dataLayer'?'&l='+l:'';j.async=true;j.src=
    'https://www.googletagmanager.com/gtm.js?id='+i+dl;f.parentNode.insertBefore(j,f);
    })(window,document,'script','dataLayer','GTM-T8DDW9B');
  </script> -->
  <!-- End Google Tag Manager -->

  <meta charset="<?php bloginfo( 'charset' ); ?>">
  <meta name="viewport" content="width=device-width,initial-scale=1,maximum-scale=1,viewport-fit=cover">
  <title><?php bloginfo('name'); ?> <?php wp_title(); ?></title>
  <link rel="shortcut icon" href="<?php echo esc_url( get_template_directory_uri() ); ?>/favicon.png" type="image/png">
  <link rel="stylesheet"
  href="https://cdnjs.cloudflare.com/ajax/libs/animate.css/3.5.2/animate.min.css"
  integrity="sha384-OHBBOqpYHNsIqQy8hL1U+8OXf9hH6QRxi0+EODezv82DfnZoV7qoHAZDwMwEJvSw"
  crossorigin="anonymous">
  
	<!--[if lt IE 9]>
	<script src="<?php echo esc_url( get_template_directory_uri() ); ?>/js/html5.js"></script>
  <![endif]-->
  
  <script src='https://www.google.com/recaptcha/api.js'></script>

  <?php wp_head(); ?>
</head>

<body>
  <!-- Google Tag Manager (noscript) -->
  <!-- <noscript><iframe src="https://www.googletagmanager.com/ns.html?id=GTM-T8DDW9B"
  height="0" width="0" style="display:none;visibility:hidden"></iframe></noscript> -->
  <!-- End Google Tag Manager (noscript) -->

  <div class="overlay"></div>

  <script>document.body.classList.add('js')</script>

<div id="preloader">
  <div id="loader-h"></div>
  <div id="loader">
  </div>
</div>

  <header class="page-header">
    <div class="page-header__container">
        <a href="<?php
          if (!is_front_page()) {
             echo get_home_url(); 
          } ?>" class="logo">
          <!-- <img src="<?php // echo esc_url( get_template_directory_uri() ); ?>/img/logo.svg" alt="Henderson logo">  -->
          
          <div class="animation-container">
            <canvas id="canvas" width="900" height="187" style="max-width: 900px"></canvas>
            <div class=" animation-container__overlay " style="pointer-events:none; overflow:hidden;
              position: absolute; left: 0px; top: 0px; display: block; ">
            </div>
          </div>
        </a>   
           
      <b class="page-header__current" data-aos="flip-up">
        <?php 
          if ( !is_front_page() ) {
            if ( !is_single() && !is_search() ) {
              the_title();
            } elseif (is_search()) {
              printf( __( 'Search results: %s', 'twentyten' ), '' . get_search_query() . '' );
            }
          } 
        ?>
      </b>
      <button class="menu_btn" data-aos="flip-up">
        <div class="sandwich white">
          <div class="sw-topper"></div>
          <div class="sw-middle"></div>
          <div class="sw-footer"></div>
        </div>
      </button>
    </div>
    <nav class="menu">
      <div class="menu__lists-wrapper">
        <ul class="menu__list">
          <li class="menu__item">
            <?php if (!is_front_page()) : ?>
              <a class="menu__link" href="<?php echo get_home_url(); ?>">Home</a>
            <?php endif; ?>
          </li>
          <li class="menu__item">
            <a href="/competitive-advantage/" class="menu__link">Competitive Advantage</a>
          </li>
          <li class="menu__item">
            <a class="menu__link menu__link--toggle" href="/sites-properties/">Sites & Properties</a>
            <ul class="menu__sub-menu">
              <li class="menu__sub-item">
                <a href="/sites-properties/" data-rel="#section-1" class="menu__sub-link">Office Space Overview</a>
              </li>
              <li class="menu__sub-item">
                <a href="/sites-properties/#section-2" class="menu__sub-link">Industrial Space Overview</a>
              </li>
              <li class="menu__sub-item">
                <a href="/sites-properties/#section-3" class="menu__sub-link">Search Listings</a>
              </li>
              <li class="menu__sub-item">
                <a href="/sites-properties/#section-4" class="menu__sub-link">Maps</a>
              </li>
            </ul>
          </li>
          <li class="menu__item">
            <a class="menu__link menu__link--toggle" href="/doing-business-here/">Doing Business Here</a>
            <ul class="menu__sub-menu">
              <li class="menu__sub-item">
                <a href="/doing-business-here/" data-rel="#section-1" class="menu__sub-link">A Magnet for Magnates</a>
              </li>
              <li class="menu__sub-item">
                <a href="/doing-business-here/#section-2" class="menu__sub-link">Talking about Business in Henderson</a>
              </li>
              <li class="menu__sub-item">
                <a href="/doing-business-here/#section-3" class="menu__sub-link">Key Industries</a>
              </li>
              <li class="menu__sub-item">
                <a href="/doing-business-here/#section-4" class="menu__sub-link">Workforce</a>
              </li>
              <li class="menu__sub-item">
                <a href="/doing-business-here/#section-5" class="menu__sub-link">Demographics</a>
              </li>
              <li class="menu__sub-item">
                <a href="/doing-business-here/#section-6" class="menu__sub-link">Data Explorer</a>
              </li>
            </ul>
          </li>
          <li class="menu__item">
            <a class="menu__link menu__link--toggle" href="/taxes-incentives/">Taxes & Incentives</a>
            <ul class="menu__sub-menu">
              <li class="menu__sub-item">
                <a href="/taxes-incentives/" data-rel="#section-1" class="menu__sub-link">Taxes & Incentives</a>
              </li>
              <li class="menu__sub-item">
                <a href="/taxes-incentives/#section-2" class="menu__sub-link">Where We Axe the Tax</a>
              </li>
              <li class="menu__sub-item">
                <a href="/taxes-incentives/#section-3" class="menu__sub-link">Outrageously Advantageous</a>
              </li>
            </ul>
          </li>
          <li class="menu__item">
            <a class="menu__link menu__link--toggle" href="/living-here/">Living here</a>
            <ul class="menu__sub-menu">
              <li class="menu__sub-item">
                <a href="/living-here/" data-rel="#section-1" class="menu__sub-link">Home Advantage</a>
              </li>
              <li class="menu__sub-item">
                <a href="/living-here/#section-2" class="menu__sub-link">Water Street District</a>
              </li>
              <li class="menu__sub-item">
                <a href="/living-here/#section-3" class="menu__sub-link">Where & Getting Here</a>
              </li>
              <li class="menu__sub-item">
                <a href="/living-here/#section-4" class="menu__sub-link">Master-Planned Communities</a>
              </li>
              <li class="menu__sub-item">
                <a href="/living-here/#section-5" class="menu__sub-link">Recreation & Entertainment</a>
              </li>
              <li class="menu__sub-item">
                <a href="/living-here/#section-6" class="menu__sub-link">Innovation in Education</a>
              </li>
              <li class="menu__sub-item">
                <a href="/living-here/#section-7" class="menu__sub-link">Social Media Feed</a>
              </li>
            </ul>
          </li>
        </ul>
        <ul class="menu__list">
          <li class="menu__item">
            <a class="menu__link sub-menu_list-link" href="/news/">News</a>
          </li>
          <li class="menu__item">
            <a class="menu__link sub-menu_list-link" href="/press-room/">Press room</a>
          </li>
          <li class="menu__item">
            <a class="menu__link sub-menu_list-link" href="/contact/">Contact</a>
          </li>
          <li class="menu__item">
            <a class="menu__link sub-menu_list-link" href="/start-tank/">Start tank</a>
          </li>
          <li class="menu__item">
            <a class="menu__link sub-menu_list-link" href="/additional-resources/">Additional resources</a>
          </li>
          <li class="menu__item">
            <a class="menu__link sub-menu_list-link pop-up-link" href="#search__pop-up">Search</a>
          </li>
        </ul>
      </div>
      <div class="subscribe subscribe--menu">
          <span class="scroll-down-arrow"></span>
        <button class="btn btn--subscribe btn--white">Subscribe to our newsletter</button>

        <!-- <p class="subscribe__text">Subscribe to the Henderson Newsletter</p>

        <div class="subscribe__input-wrapper">
          <input class="subscribe__input"  id="email-menu" name="email" type="text" autocomplete="off">
          <label class="subscribe__label" for="email-menu">Email address</label>
          <span class="validation-icon" title="Clear">
            <svg class="cross" xmlns="http://www.w3.org/2000/svg" width="212.982" height="212.982" viewBox="0 0 212.982 212.982">
              <path d="M131.8,106.49l75.936-75.936A17.9,17.9,0,0,0,182.424,5.242L106.487,81.179,30.55,5.241A17.9,17.9,0,0,0,5.238,30.553l75.937,75.936L5.238,182.426A17.9,17.9,0,0,0,30.55,207.738L106.487,131.8l75.937,75.937a17.9,17.9,0,0,0,25.312-25.312L131.8,106.49Z" transform="translate(0.004 0.002)"/>
            </svg>
          </span>
          <span class="validation-icon" title="Submit">
            <svg class="check" enable-background="new 0 0 426.67 426.67" version="1.1" viewBox="0 0 426.67 426.67" xml:space="preserve" xmlns="http://www.w3.org/2000/svg">
              <path d="m153.5 366.84c-8.657 0-17.323-3.302-23.927-9.911l-119.66-119.66c-13.218-13.218-13.218-34.645 0-47.863s34.645-13.218 47.863 0l95.727 95.727 215.39-215.39c13.218-13.214 34.65-13.218 47.859 0 13.222 13.218 13.222 34.65 0 47.863l-239.32 239.32c-6.609 6.605-15.271 9.911-23.932 9.911z" />
            </svg>
          </span>
        </div> -->

        <!-- form action="https://hendersonecdev.us18.list-manage.com/subscribe/post?u=233920ccb15ea65d26754bced&amp;id=8e47e024c6"
          method="post" id="mc-embedded-subscribe-form" name="mc-embedded-subscribe-form" class="mce-form validate footer__colum__form"
          target="_blank" novalidate autocomplete="off">

          <div class="subscribe__input-wrapper">
            <input class="subscribe__input"  id="email-menu" name="EMAIL" type="text">
            <label class="subscribe__label" for="email-menu">Email address</label>
            <span class="validation-icon" title="Clear">
              <svg class="cross" xmlns="http://www.w3.org/2000/svg" width="212.982" height="212.982" viewBox="0 0 212.982 212.982">
                <path d="M131.8,106.49l75.936-75.936A17.9,17.9,0,0,0,182.424,5.242L106.487,81.179,30.55,5.241A17.9,17.9,0,0,0,5.238,30.553l75.937,75.936L5.238,182.426A17.9,17.9,0,0,0,30.55,207.738L106.487,131.8l75.937,75.937a17.9,17.9,0,0,0,25.312-25.312L131.8,106.49Z" transform="translate(0.004 0.002)"/>
              </svg>
            </span>
            <span class="validation-icon" title="Submit">
              <svg class="check" enable-background="new 0 0 426.67 426.67" version="1.1" viewBox="0 0 426.67 426.67" xml:space="preserve" xmlns="http://www.w3.org/2000/svg">
                <path d="m153.5 366.84c-8.657 0-17.323-3.302-23.927-9.911l-119.66-119.66c-13.218-13.218-13.218-34.645 0-47.863s34.645-13.218 47.863 0l95.727 95.727 215.39-215.39c13.218-13.214 34.65-13.218 47.859 0 13.222 13.218 13.222 34.65 0 47.863l-239.32 239.32c-6.609 6.605-15.271 9.911-23.932 9.911z" />
              </svg>
            </span>

            <button type="submit" id="mc-embedded-subscribe" class="subscribe__submit"></button>
          </div>

          <div id="mce-responses" class="clear">
            <div class="response" id="mce-error-response" style="display:none"></div>
            <div class="response" id="mce-success-response" style="display:none"></div>
          </div>
          <div style="position: absolute; left: -5000px;" aria-hidden="true">
            <input type="text" name="b_233920ccb15ea65d26754bced_8e47e024c6" tabindex="-1" value="">
          </div>
        </form -->
      </div>
    </nav>
  </header>
  <div id="search__pop-up" class="search__pop-up mfp-hide">
    <?php dynamic_sidebar( 'search_form' ); ?>
  </div>
  <main>