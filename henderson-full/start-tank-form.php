<?php 

require_once('phpmailer/PHPMailerAutoload.php');
$mail = new PHPMailer;
$mail->CharSet = 'utf-8';

$fullname = $_POST['fullname'];
$phone = $_POST['phone'];
$email = $_POST['email'];
$twitter = $_POST['twitter'] ? '<b>Twitter:</b> ' . $_POST['twitter'] . '<br>' : '';
$collaborators = $_POST['collaborators'] ? '<b>Collaborators:</b> ' . $_POST['collaborators'] . '<br>'  : '';
$street = $_POST['street'];
$zip = $_POST['zip'];
$city = $_POST['city'];
$state = $_POST['state'];
$youtube = $_POST['youtube'];
$state_of_dev = $_POST['state-of-dev'];
$answers = $_POST['answers'];

$result = [];

if (empty($fullname) || empty($phone) ||
    empty($email) || empty($street) ||
    empty($zip) || empty($city) ||
    empty($state_of_dev) || empty($answers) ||
    empty($state) || empty($youtube)) {
      $result['status'] = 'error';
      $result['message'] = 'You must fill all required fields';
      echo json_encode($result); 
      exit;
}

//$mail->SMTPDebug = 2;                               // Enable verbose debug output

$mail->isSMTP();                                      // Set mailer to use SMTP
$mail->Host = 'smtp.gmail.com';  					  // Specify main and backup SMTP servers
$mail->SMTPAuth = true;                               // Enable SMTP authentication
$mail->Username = 'emailfortests.db@gmail.com';       // Email login
$mail->Password = 'testTEST1';                        // Email password
$mail->SMTPSecure = 'tls';                            // Enable TLS encryption, `ssl` also accepted
$mail->Port = 587;                                    // TCP port to connect to / этот порт может отличаться у других провайдеров

$mail->setFrom($email);                               // From
$mail->addAddress('Barbra.Coffee@cityofhenderson.com');          // To
//$mail->addReplyTo('info@example.com', 'Information');
//$mail->addCC('cc@example.com');
//$mail->addBCC('bcc@example.com');
//$mail->addAttachment('/var/tmp/file.tar.gz');         // Add attachments
//$mail->addAttachment('/tmp/image.jpg', 'new.jpg');    // Optional name
$mail->isHTML(true);                                  // Set email format to HTML

$mail->Subject = 'Start Tank from  Hendersonnow.com';
$mail->Body    = '<b>From:</b> ' . $email . '<br><b>Name:</b> ' . $fullname .
                 '<br><b>Phone number:</b> ' .$phone . '<br>' . $twitter . $collaborators .
                 '<b>Street:</b> ' . $street . ' <b>Zip:</b> ' . $zip . ' <b>City:</b> ' . $city . ' <b>State:</b> ' . $state .
                 '<br><b>Youtube:</b> ' . $youtube . '<br><b>State of Development:</b> ' . $state_of_dev .
                 '<br><br><b>Answers:</b><br>' . $answers;
$mail->AltBody = '';


if(!$mail->send()) {
    $result['status'] = 'error';
    $result['message'] = 'Error with sending email. Please check your email and try again';
} else {
    $result['status'] = 'success';
}

echo json_encode($result); 

?>
