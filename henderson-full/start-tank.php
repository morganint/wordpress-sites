<?php

/**

 * Template Name: Start Tank

 * Template Post Type: page

 *

 * @package Henderson

 * @since Henderson 1.0

 */



get_header(); ?>



    <?php if( get_field( 'hero_img' ) ) : ?>


        <div class="page-img parallax-window">
            <img src="<?php the_field('hero_img');?>">
        </div>


    <?php endif; ?>



    <?php if ( have_rows('first_section') ) : 

        $content      = get_field( 'first_section' );

        $left_column  = $content['left_column'];

        $right_column = $content['right_column'];

        ?> 



        <section class="section">

            <div class="container-md">

                <h2 class="section__title tal" data-aos="flip-up"><?php echo $content['title']; ?></h2>

                <div class="grid">

                    <div class="col-1-of-2">

                        <div class="section__divider" data-aos="fade-in"></div>

                    </div>

                    <div class="col-1-of-2">

                        <div class="small-col">

                            <div class="section__divider" data-aos="fade-in"></div>

                        </div>

                    </div>

                </div>

                <div class="section__text section__text--no-center" data-aos="fade-up"><?php echo $content['content']; ?></div>

                <div class="grid">

                    <div class="col-1-of-2 col-sm">

                        <h4 class="section__title section__title--small tal" data-aos="flip-up" data-aos-delay="300"><?php echo $left_column['title']; ?></h4>

                        <div class="section__divider" data-aos="fade-in"></div>

                        <div class="column-text" data-aos="fade-up"><?php echo $left_column['content']; ?></div>

                    </div>

                    <div class="col-1-of-2 col-sm">

                        <div class="small-col">

                            <h4 class="section__title section__title--small tal" data-aos="flip-up" data-aos-delay="300"><?php echo $right_column['title']; ?></h4>

                            <div class="section__divider" data-aos="fade-in"></div>

                            <div class="column-text" data-aos="fade-up"><?php echo $right_column['content']; ?></div>

                        </div>

                    </div>

                </div>

            </div>

        </section>



    <?php endif; ?>



    <?php if ( get_field('second_section') ) : ?>



        <section class="section section--grey">

            <form action="start-tank-form.php" class="form" method="POST" id="start-tank-form">

                <div class="container-md">

                    <div class="grid">

                        <div class="col-1-of-2 col-sm left-col-anchor">

                            <h4 class="section__title section__title--small tal" data-aos="flip-up">1. Personal information</h4>

                            <div class="section__divider" data-aos="fade-in"></div>

                            <div class="form__group"  data-aos-anchor=".left-col-anchor" data-aos="fade-right" data-aos-delay="200">

                                <label for="fullname" class="form__label">Full name</label>

                                <input id="fullname" name="fullname" class="form__input" type="text" required>

                            </div>

                            <div class="form__group"  data-aos-anchor=".left-col-anchor" data-aos="fade-right" data-aos-delay="400">

                                <label for="phone" class="form__label">Phone number</label>

                                <input id="phone" name="phone" class="form__input" type="number" required>

                            </div>

                            <div class="form__group"  data-aos-anchor=".left-col-anchor" data-aos="fade-right" data-aos-delay="600">

                                <label for="email" class="form__label">Email address</label>

                                <input id="email" name="email" class="form__input" type="email" required>

                            </div>

                            <div class="form__group"  data-aos-anchor=".left-col-anchor" data-aos="fade-right" data-aos-delay="800">

                                <label for="twitter" class="form__label">Twitter handle

                                    <span class="form__muted">So we can tweet about your business and tag you! (Optional)</span>

                                </label>

                                <input id="twitter" name="twitter" class="form__input" type="text">

                            </div>

                        </div>

                        <div class="col-1-of-2 col-sm right-col-anchor">

                            <h4 class="section__title section__title--small tal" data-aos="flip-up">2. Business information</h4>

                            <div class="section__divider" data-aos="fade-in"></div>

                            <div class="form__group"  data-aos-anchor=".right-col-anchor" data-aos="fade-left" data-aos-delay="200">

                                <label for="collaborators" class="form__label">Names of Collaborators (Optional)</label>

                                <input id="collaborators" name="collaborators" class="form__input"

                                    type="text">

                            </div>

                            <div class="form__group"  data-aos-anchor=".right-col-anchor" data-aos="fade-left" data-aos-delay="400">

                                <label for="street" class="form__label">Street address</label>

                                <input id="street" name="street" class="form__input" type="text" required>

                            </div>

                            <div class="form__container"  data-aos-anchor=".right-col-anchor" data-aos="fade-left" data-aos-delay="600">

                                <div class="form__group">

                                    <label for="zip" class="form__label">Zip</label>

                                    <input id="zip" name="zip" class="form__input" type="text" required>

                                </div>

                                <div class="form__group">

                                    <label for="city" class="form__label">City</label>

                                    <input id="city" name="city" class="form__input" type="text" required>

                                </div>

                                <div class="form__group">

                                    <label for="state" class="form__label">State</label>

                                    <input id="state" name="state" class="form__input" type="text" required>

                                </div>

                            </div>

                        </div>

                    </div>

                    <h4 class="section__title section__title--small tal" data-aos="flip-up">3. Application information</h4>

                    <div class="section__divider" data-aos="fade-in"></div>

                    <div class="grid">

                        <div class="col-1-of-2 col-sm left-bottom-col-anchor">

                            <div class="form__group"  data-aos-anchor=".left-bottom-col-anchor" data-aos="fade-right" data-aos-delay="200">

                                <label for="youtube" class="form__label">Link to YouTube Pitch</label>

                                <input id="youtube" name="youtube" class="form__input" type="text"

                                    required>

                            </div>

                            <div class="form__radio-group"  data-aos-anchor=".left-bottom-col-anchor" data-aos="fade-right" data-aos-delay="400">

                                <p class="form__label">What state of development is your company in currently?</p>

                                <input id="radio1" class="form__radio"

                                    type="radio" name="state-of-dev" value="research-and-dev" required>

                                <label for="radio1" class="form__radio-label">Research & development</label>

                                <input id="radio2" class="form__radio" type="radio" name="state-of-dev"

                                    value="revenue">

                                <label for="radio2" class="form__radio-label">Revenue</label>

                                <input id="radio3" class="form__radio" type="radio" name="state-of-dev" value="product-dev">

                                <label for="radio3" class="form__radio-label">Product development</label>

                                <input id="radio4" class="form__radio" type="radio" name="state-of-dev"

                                    value="expansion">

                                <label for="radio4" class="form__radio-label">Expansion</label>

                                <input id="radio5" class="form__radio" type="radio" name="state-of-dev" value="shipping-or-live">

                                <label for="radio5" class="form__radio-label">Shipping / Live</label>

                            </div>

                        </div>

                        <div class="col-1-of-2 col-sm right-bottom-col-anchor">

                            <label  data-aos-anchor=".right-bottom-col-anchor" data-aos="fade-left" data-aos-delay="200" for="" class="form__label">In 100 words or less, answer the following questions:</label>

                            <p data-aos-anchor=".right-bottom-col-anchor" data-aos="fade-left" data-aos-delay="400" class="form__text">1. What does it do?

                                <br>2. What is your unique selling proposition?

                                <br>3. What is notable and how is it better than what else is on the market?

                                <br>4. How will you make it profitable?</p>

                            <textarea data-aos-anchor=".right-bottom-col-anchor" data-aos="fade-left" data-aos-delay="600" name="answers" id="answers" cols="30" rows="10"

                                class="form__textarea" required></textarea>

                        </div>

                    </div>

                    <div class="section__divider" data-aos="fade-in"></div>

                    <div class="grid">

                        <div class="col-1-of-2 col-xs"></div>

                        <div class="col-1-of-2 col-xs">

                            <button data-aos="fade-up" class="form__submit" type="form__submit">Submit application</button>

                        </div>

                    </div>

                </div>

            </form>

        </section>



    <?php endif; ?>



    <?php if ( get_field( 'third_section' ) ) : 

        

        $images = get_field( 'third_section' );



        ?>

        

        <section class="section">

            <div class="container-md">

                <h4 class="section__title section__title--small tal" data-aos="flip-up">Sponsors</h4>

                <div class="section__divider" data-aos="fade-in"></div>

                <div class="grid">

                    <ul class="sponsors-list equal-height">

                        

                        <?php $delay = 0; foreach( $images as $image ) :  $delay += 150; ?>

                    

                            <li class="sponsors-list__item"
                                data-aos-anchor=".sponsors-list" data-aos="fade-up"
                                data-aos-duration="400" data-aos-delay="<?php echo $delay; ?>"
                            >

                                <img src="<?php echo $image['sizes']['thumb_414']; ?>" alt="<?php echo $image['alt']; ?>" title="<?php echo $image['alt']; ?>"> 

                            </li>

                    

                        <?php endforeach; ?> 

                    

                    </ul>

                </div>

            </div>

        </section>



    <?php endif; ?>



    <div id="popup" class="popup white-popup mfp-hide">

        <h2></h2>

        <p></p>

        <button class="btn">Ok</button>

    </div>

    <script>$(document).ready(function () {



            function openPopup(title, msg) {

                $('#popup').find('h2').text(title).end().find('p').text(msg);



                $.magnificPopup.open(

                    {

                        items: {

                            src: '#popup',

                            type: 'inline',

                            preloader: false,

                            modal: true

                        }

                    }

                )



                $('#popup').find('button').on('click', function (e) {

                    e.preventDefault();

                    $.magnificPopup.close();

                });

            }



            function validateEmail(email) {

                var re = /^(([^<>()\[\]\\.,;:\s@"]+(\.[^<>()\[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;

                return re.test(String(email).toLowerCase());

            }



            $('#start-tank-form').submit(function (e) {

                e.preventDefault();



                var form = $(this);

                var data = form.serialize();



                var email = form.find('#email').val();



                if (!validateEmail(email)) {

                    openPopup('Error', 'You must enter a correct email!');

                    return

                }



                $.ajax({

                    type: form.attr('method'),

                    url: form.attr('action'),

                    data: data,

                    dataType: "json ",

                    beforeSend: function () {

                        form.find('button[type="submit "]').attr('disabled', true);

                    },

                    success: function (data) {

                        if (data.status === 'success') {

                            openPopup('Thank you for your message', 'A member of the City of Henderson Economic Development Team will be in contact shortly!');



                            form[0].reset();

                        } else if (data.status === 'error') {

                            openPopup('Error', data.message);

                        }

                    },

                    error: function (xhr, ajaxOptions, thrownError) {

                        console.log(arguments);



                        openPopup('Error', 'Something goes wrong. Please try again');

                    },

                    complete: function () {

                        form.find('button[type="submit "]').prop('disabled', false);

                    }

                });

            });

        })</script>

    

    <?php get_footer(); ?>