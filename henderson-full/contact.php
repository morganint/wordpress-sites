<?php

/**

 * Template Name: Contact Page

 * Template Post Type: page

 *

 * @package Henderson

 * @since Henderson 1.0

 */



get_header(); ?>



  <section class="section">

    <div class="container">

      <h2 class="section__title" data-aos="flip-up" data-aos-delay="200"><?php the_field( 'title' ); ?></h2>

      <div class="section__divider" data-aos="fade-in" data-aos-delay="300"></div>

      <div class="section__text" data-aos="fade-up" data-aos-delay="400"><?php the_field( 'content' ); ?></div>



      <?php if ( have_rows('team') ) : ?>

        <h4 class="section__title section__title--small" data-aos="flip-up" data-aos-delay="800">Economic Development Team</h4>

        <div class="section__divider" data-aos="fade-in"  data-aos-delay="900"></div>

        <div class="grid equal-height" data-equal-from="1200">

          <?php $delay = 1000; while ( have_rows('team') ) : the_row();  
            $delay += 150;
            $photo = get_sub_field( 'photo' ); ?>

            <div class="col-1-of-2 col-md">

              <div class="team"  data-aos="fade-up" data-aos-duration="400" data-aos-offset="200" data-aos-delay="<?php echo $delay; ?>">

                <div class="team__photo">

                  <img src="<?php echo $photo['sizes']['thumb_414'] ?>" alt="<?php the_sub_field( 'name' ); ?>">

                </div>

                <div class="team__text-block">

                  <b class="team__title"><?php the_sub_field( 'name' ); ?></b>

                  <span class="team__subtitle"><?php the_sub_field( 'position' ); ?></span>

                  <div class="team__text"><?php the_sub_field( 'bio' ); ?></div>

                  <p class="team__bottom"><?php the_sub_field( 'phone' ); ?></p>

                  <p class="team__bottom">

                    <a href="mailto:<?php the_sub_field( 'email' ); ?>"><?php the_sub_field( 'email' ); ?></a>

                  </p>

                </div>

              </div>

            </div>

          <?php endwhile; ?>



        </div>

        <?php endif; ?>

    </div>

  </section>

  <section class="section section--grey pb0">

    <div class="container">

      <h4 class="section__title section__title--small" data-aos="flip-up">Contact us for more information</h4>

      <div class="section__divider" data-aos="fade-in"></div>

      <div class="contact">

        <div class="grid">

          <div class="col-1-of-2 col-sm">

            <div class="contact__form" data-aos="fade-up" data-aos-duration="400" data-aos-delay="200">

              <form class="form" id="contact-form" action="<?php echo esc_url( get_template_directory_uri() ); ?>/contact-form.php" method="POST">

                <div class="contact__group">

                  <label class="contact__label" for="name">Name*</label>

                  <input class="contact__input" id="name" name="name" type="text" required>

                </div>

                <div class="contact__group">

                  <label class="contact__label" for="email">Email address*</label>

                  <input class="contact__input" id="email" name="email" type="email" required>

                </div>

                <div class="contact__group">

                  <label class="contact__label" for="business_name">Company name (optional)</label>

                  <input class="contact__input" id="business_name" name="business_name" type="text">

                </div>

                <!-- <div>
                  <p class="form__label">Company size (optional)</p>

                  <input id="business_size_small" class="form__radio" type="radio" name="business_size" value="small">
                  <label for="business_size_small" class="form__radio-label">1-49 (Small)</label>

                  <input id="business_size_medium" class="form__radio" type="radio" name="business_size" value="medium">
                  <label for="business_size_medium" class="form__radio-label">50-249 (Medium)</label>
                  
                  <input id="business_size_large" class="form__radio" type="radio" name="business_size" value="large">
                  <label for="business_size_large" class="form__radio-label">250+ (Large)</label>
                </div> -->

                <div class="contact__group">
                  <label class="contact__label">Title (optional)</label>

                  <div class="custom-select">
                    <button type="button" class="custom-select__dropdown-btn contact__label"></button>
                    <input type="text" name="title" placeholder="Select Title" class="disabled contact__input">

                    <ul class="custom-select__dropdown">
                      <li data-value="CEO">CEO</li>
                      <li data-value="CFO">CFO</li>
                      <li data-value="President">President</li>
                      <li data-value="Owner">Owner</li>
                      <li data-value="Founder">Founder</li>
                      <li data-value="Other">Other...</li>
                    </ul>
                  </div>

                </div>

                <div class="contact__group">

                  <label class="contact__label" for="user_comment">Comment*</label>

                  <textarea class="contact__textarea" id="user_comment" name="user_comment" required></textarea>

                </div>

                <!-- <div class="g-recaptcha" data-callback="enableBtn" data-sitekey="6LfpIXMUAAAAAMYO-HKSwMpUoosnOxsE5eCDBAfT"></div> -->

                <button class="contact__submit" type="submit" disabled>Send message</button>

              </form>

            </div>

          </div>

          <div class="col-1-of-2 col-sm">

            <div class="contact__text-block" data-aos="fade-up" data-aos-duration="400" data-aos-delay="400">

              <div class="contact__text-group">

                <b class="contact__text-title">Mailing address</b>

                <p class="contact__text"><?php the_field( 'mailing_address' ); ?></p>

              </div>

              <div class="contact__text-group">

                <b class="contact__text-title">Fax number</b>

                <p class="contact__text"><?php the_field( 'fax_number' ); ?></p>

              </div>

            </div>

          </div>

        </div>

      </div>

      <div class="section__divider section__divider--no-gutters" data-aos="fade-in"></div>

    </div>

  </section>

  <div id="popup" class="popup white-popup mfp-hide">

    <h2></h2>

    <p></p>

    <button class="btn">Ok</button>

  </div>

  <script>
    function enableBtn(e) {
      document.querySelector('.contact__submit').disabled = false;
    }

    $(document).ready(function () {

      function openPopup(title, msg) {

        $('#popup').find('h2').text(title).end().find('p').text(msg);



        $.magnificPopup.open(

          {

            items: {

              src: '#popup',

              type: 'inline',

              preloader: false,

              modal: true

            }

          }

        )



        $('#popup').find('button').on('click', function (e) {

          e.preventDefault();

          $.magnificPopup.close();

        });

      }

      

      function validateEmail(email) {

        var re = /^(([^<>()\[\]\\.,;:\s@"]+(\.[^<>()\[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;

        return re.test(String(email).toLowerCase());

      }


      $('#contact-form').submit(function (e) {

        e.preventDefault();

        var form = $(this);

        var data = form.serialize();



        var email = form.find('#email').val();



        if (!validateEmail(email)) {

          openPopup('Error', 'You must enter a correct email!');

          return

        }


        $.ajax({

          type: form.attr('method'),

          url: form.attr('action'),

          data: data,

          dataType: "json ",

          beforeSend: function () {

            form.find('button[type="submit "]').attr('disabled', true);

          },

          success: function (data) {

            if (data.status === 'success') {

              openPopup('Thank you for your message', 'A member of the City of Henderson Economic Development Team will be in contact shortly!');



              form[0].reset();

            } else if (data.status === 'error') {

              openPopup('Error', data.message);

            }

          },

          error: function (xhr, ajaxOptions, thrownError) {

            console.log(arguments);



            openPopup('Error', 'Something goes wrong. Please try again');

          },

          complete: function () {

            form.find('button[type="submit "]').prop('disabled', false);

          }

        });

      });

    })</script>

  <?php get_footer(); ?>