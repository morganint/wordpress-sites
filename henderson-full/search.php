<?php
/**
 * The home page template file
 *
 * @package Henderson
 * @since Henderson 1.0
 */

get_header(); ?>

<section class="section section--grey" id="announcements">
    <div class="container">
      <div class="search-results">

        <?php if ( have_posts() ) while ( have_posts() ) : the_post(); ?>
          <?php $image = get_the_post_thumbnail_url($post->ID, 'thumb_500'); ?>
          <?php $goLink = get_field('go_link'); ?>
          <div class="news__item-wrapper">
            <div class="news__item news__item--with-img">

              <?php if ($image) : ?>
                <div class="news__item-img" style="background-image: url(<?php echo $image; ?>)"></div>
              <?php endif; ?>

              <div class="news__text-block">
                <span class="news__date"><?php the_date(); ?></span>

                  <?php if($goLink == NULL) { ?>

                      <a href="<?php the_permalink(); ?>" class="news__link">                                        

                  <?php } else { ?>

                      <a href="<?php the_field('go_link'); ?>"  target="_blank" class="news__link">


                  <?php } ?>
                      <h3 class="news__title"><?php the_title(); ?></h3>
                  
                  <?php if (get_the_excerpt()) : ?>

                    <div class="news__text"><?php the_excerpt(); ?></div>

                    <?php if($goLink == NULL) { ?>

                        <a href="<?php the_permalink(); ?>" class="news__arrow"></a>                                   

                    <?php } else { ?>

                        <a href="<?php the_field('go_link'); ?>"  target="_blank" class="news__arrow"></a>                                             

                    <?php } ?>
                  <?php endif; ?>
              </div>
            </div>
          </div>
        <?php endwhile; ?> 
    </div>
    <div class="pagination">
      <?php 
        global $wp_query;
        $big = 999999999;
        echo paginate_links( array(
          'base' => str_replace( $big, '%#%', esc_url( get_pagenum_link( $big ) ) ),
          'format' => '?paged=%#%',
          'current' => max( 1, get_query_var('paged') ),
          'type' => 'list',
          'prev_text'    => __('« '), 
            'next_text'    => __(' »'),
          'total' => $wp_query->max_num_pages
        ) );
      ?>
    </div>  
  </div>
</section>
  
<?php get_footer(); ?>