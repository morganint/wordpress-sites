<?php

/**

 * Template Name: Living Here

 * Template Post Type: page

 *

 * @package Henderson

 * @since Henderson 1.0

 */



get_header(); ?>



  <?php if( get_field( 'hero_image' ) ) : ?>

      <div class="page-img parallax-window">
          <img src="<?php the_field('hero_image');?>">
      </div>

  <?php endif; ?>



  <?php if ( have_rows('page_menu') ) : ?>

    <div class="slick-nav-spacer">

      <nav class="slick-nav" data-aos="fade-in" data-aos-delay="200" data-aos-offset="-500">

        <ul class="slick-nav__list">

          <?php 

          $i = 1;

          while ( have_rows('page_menu') ) : the_row();

            $title = get_sub_field('menu_title');

          ?>

            <li class="slick-nav__item">

              <a href="#section-<?php echo $i; ?>" class="slick-nav__anchor" title="<?php echo $title; ?>"><?php echo $title; ?></a>

            </li>

          <?php 

            $i++;

          endwhile; ?>

        </ul>

      </nav>

    </div>

  <?php endif; ?>



  <?php if( get_field('first_section') ) : 

    $section      = get_field('first_section');

    $column_left  = $section['column_left'];

    $column_right = $section['column_right'];

    $awards       = $section['awards'];

  ?>



    <section class="section" id="section-1">

      <div class="container">

        <h2 class="section__title" data-aos="flip-up"><?php echo $section['title']; ?></h2>

        <div class="section__divider" data-aos="fade-in"></div>

        <div class="nevada-home">

          <div class="nevada-home__item">



            <?php if ( $column_left['image'] ) : ?>



              <a data-aos="fade-up" data-aos-duration="400" data-aos-delay="200" class="img-popup" href="<?php echo $column_left['image']['url']; ?>">

                <img class="img-popup__img" src="<?php echo $column_left['image']['sizes']['thumb_768']; ?>">

                

            <?php endif; ?>



            <?php if ( $column_left['text'] ) : ?>



                <div data-aos="fade-in" data-aos-duration="1000" data-aos-delay="500" class="nevada-home__text-block"><?php echo $column_left['text']; ?></div>



            <?php endif; ?>



            <?php if ( $column_left['image'] ) : ?>



              </a>



            <?php endif; ?>



          </div>

          <div class="nevada-home__item" data-aos="fade-up" data-aos-duration="400" data-aos-delay="350">



            <?php if ( $column_right['image'] ) : ?>



              <a data-aos="fade-up" data-aos-duration="400" data-aos-delay="200" class="img-popup" href="<?php echo $column_right['image']['url']; ?>">

               <img class="img-popup__img" src="<?php echo $column_right['image']['sizes']['thumb_768']; ?>">

              

            <?php endif; ?>



            <?php if ( $column_right['text'] ) : ?>



              <div data-aos="fade-in" data-aos-duration="1000" data-aos-delay="500" class="nevada-home__text-block"><?php echo $column_right['text']; ?></div>



            <?php endif; ?>



            <?php if ( $column_right['image'] ) : ?>



            </a>



            <?php endif; ?>



          </div>

        </div>



        <?php if ( $awards ) : ?>



          <h2 class="section__title" data-aos="flip-up">Awards</h2>

          <div class="section__divider" data-aos="fade-in"></div>

          <ul class="awards-list">



            <?php $delay = 0; foreach( $awards as $award ) : $delay += 150; ?>

              

              <li class="awards-list__item" data-aos-anchor=".awards-list" data-aos="fade-up" data-aos-duration="400" data-aos-delay="<?php echo $delay; ?>">

                <img src="<?php echo $award['url']; ?>" alt="<?php echo $award['alt']; ?>" title="<?php echo $award['alt']; ?>" class="awards-list__img">

                <p class="awards-list__text"><?php echo $award['alt']; ?></p>

              </li>



            <?php endforeach; ?>



          </ul>



        <?php endif; ?>



      </div>

    </section>

  <?php endif; ?>



  <?php if( get_field('second_section') ) : 

      $section = get_field('second_section');

      $slider  = $section['slider']; ?>



    <section class="section section--grey" id="section-2">

      <div class="container">

        <h2 class="section__title" data-aos="flip-up"><?php echo $section['title']; ?></h2>

        <div class="section__divider" data-aos="fade-in"></div>

        <div class="section__text" data-aos="fade-up"><?php echo $section['text']; ?></div>



        <?php if ($slider) : ?>



          <div class="facts" data-aos="fade-up" data-aos-duration="400">

            <div class="facts__slider">

              

              <?php foreach ($slider as $slide) : ?>



                <div class="facts__item-wrap">

                  <div class="facts__item">

                    <div class="facts__img" style="background-image: url(<?php echo $slide['sizes']['thumb_414']; ?>)"></div>

                    <div class="facts__text-block">

                      <span class="facts__red"><?php echo $slide['title']; ?></span>

                      <b class="facts__title"><?php echo $slide['alt']; ?></b>

                      <p class="facts__text"><?php echo $slide['caption']; ?></p>

                    </div>

                  </div>

                </div>

              

              <?php endforeach; ?>



            </div>

            <span class="slider-navs__prev"></span>

            <span class="slider-navs__next"></span>

          </div>



        <?php endif; ?>



      </div>

    </section>



  <?php endif; ?>



  <?php if( get_field('third_section') ) : 

      $section = get_field('third_section'); ?>



    <section class="section" id="section-3">

      <div class="container">

        <h2 class="section__title" data-aos="flip-up"><?php echo $section['title']; ?></h2>

        <div class="section__divider" data-aos="fade-in"></div>

        <div class="section__text" data-aos="fade-up"><?php echo $section['content']; ?></div>

        <?php if ($section['map']) : ?>

          <div class="map" data-aos="fade-up" data-aos-duration="400"></div>

        <?php endif; ?>

      </div>

    </section>



  <?php endif; ?>



    <?php if( get_field('fourth_section') ) : 

      $section = get_field('fourth_section');

      $left_column = $section['column_left'];

      $right_column = $section['column_right']; ?>



    <section class="section section--grey" id="section-4">

      <div class="container">

        <h2 class="section__title" data-aos="flip-up"><?php echo $section['title']; ?></h2>

        <div class="section__divider" data-aos="fade-in"></div>

        <div class="section__text" data-aos="fade-up"><?php echo $section['content']; ?></div>

        <div class="communities">

          <?php if ($left_column) : ?>



            <div class="communities__item" data-aos="fade-up" data-aos-duration="400" data-aos-delay="200">

              <a class="img-popup" href="<?php echo $left_column['full_image']; ?>">

                <img class="img-popup__img" src="<?php echo $left_column['thumbnail']['sizes']['thumb_768']; ?>">

                <div class="communities__text-block">

                  <b class="communities__title"><?php echo $left_column['title']; ?></b>

                  <span class="communities__text">Click to expand</span>

                </div>

              </a>

            </div>



          <?php endif; ?>



          <?php if ($right_column) : ?>



            <div class="communities__item" data-aos="fade-up" data-aos-duration="400" data-aos-delay="400">

              <a class="img-popup" href="<?php echo $right_column['full_image']; ?>">

                <img class="img-popup__img" src="<?php echo $right_column['thumbnail']['sizes']['thumb_768']; ?>">

                <div class="communities__text-block">

                  <b class="communities__title"><?php echo $right_column['title']; ?></b>

                  <span class="communities__text">Click to expand</span>

                </div>

              </a>

            </div>



          <?php endif; ?>

        </div>

      </div>

    </section>



  <?php endif; ?>

  

  <?php if( get_field('fifth_section') ) : 

    $section = get_field('fifth_section');

    $blocks = $section['content'];

  ?>



    <section class="section" id="section-5">

      <div class="container">

        <h2 class="section__title" data-aos="flip-up"><?php echo $section['title']; ?></h2>

        <div class="section__divider"  data-aos="fade-in"></div>



        <?php $delay = 0; if ($blocks) : $delay += 150; ?>



          <div id="workforce" class="workforce-anchor">

            <div class="grid equal-height ">

              <?php $delay = 0; foreach ($blocks as $block) : $delay += 100; ?>

                <div class="col-1-of-2 col-md">

                  <div class="text-with-img text-with-img--grey"
                    data-aos="fade-up"
                    data-aos-duration="400" data-aos-delay="<?php echo $delay; ?>"
                  >

                    <div class="text-with-img__img" style="background-image: url(<?php echo $block['image']; ?>)"></div>

                    <div class="text-with-img__text-block">

                      <div class="text-with-img__text"><?php echo $block['content']; ?></div>

                    </div>

                  </div>

                </div>



              <?php endforeach; ?>



            </div>

          </div>



        <?php endif; ?>



      </div>

    </section>



  <?php endif; ?>



  <section class="section section--grey" id="section-6">

    <div class="container">

      

      <?php if( get_field('sixth_section') ) : 

        $section = get_field('sixth_section');

        $blocks = $section['block'];

      ?>

        <h2 class="section__title" data-aos="flip-up">Innovation in Education</h2>

        <div class="section__divider"  data-aos="fade-in"></div>



        <?php if ($block) : ?>

          <div class="grid">

            <?php foreach($blocks as $block) : ?>

              <div class="col-1-of-2 col-md">

                <div class="full-img" data-aos="fade-up" data-aos-duration="400" data-aos-delay="200" style="background-image: url(<?php echo $block['image']; ?>)"></div>

              </div>

              <div class="col-1-of-2 col-md">

                <div class="text-block" data-aos="fade-up" data-aos-duration="400" data-aos-delay="400">

                  <div class="text-block__text"><?php echo $block['content']; ?></div>

                </div>

              </div>



            <?php endforeach; ?>



          </div>



        <?php endif; ?>



      <?php endif; ?>





      <?php if( get_field('seventh_section') ) : 

        $section = get_field('seventh_section');

        $left_column = $section['left_column'];

        $right_column = $section['right_column'];

        $left_list = $left_column['link_list'];

        $right_list = $right_column['link_list'];

        $footnote = $section['footnote'];

      ?>



      <div class="education">

        <div class="grid">

          <div class="col-1-of-2">

            <h4 data-aos="flip-up" class="section__title section__title--small section__title--left"><?php echo $left_column['title']; ?></h4>

          </div>

          <div class="col-1-of-2">

            <h4 data-aos="flip-up" class="section__title section__title--small section__title--right"><?php echo $right_column['title']; ?></h4>

          </div>

        </div>

        <div class="section__divider section__divider--no-gutters" data-aos="fade-in"></div>

        <div class="grid education-list-anchor">

          

          <?php if ( $left_list ) : ?>



            <div class="col-1-of-2 tar">

              <?php $delay = 0; foreach ( $left_list as $item ) :
                $delay += 150;
                $item_list = $item['list_links']; ?>



                <b class="education__title"
                  data-aos="fade-right" data-aos-anchor=".education-list-anchor" data-aos-duration="400" data-aos-delay="<?php echo $delay; ?>"
                ><?php echo $item['title']; ?></b>

                <ul class="education__list">



                  <?php foreach( $item_list as $list ) : $delay += 150; ?>



                    <li class="education__item" data-aos-anchor=".education-list-anchor" data-aos="fade-right" data-aos-duration="400" data-aos-delay="<?php echo $delay; ?>">

                      <a href="<?php echo $list['url']; ?>" target="_blank "><?php echo $list['link_title']; ?></a>

                    </li>



                  <?php endforeach; ?>



                </ul>

  

              <?php endforeach; ?>



            </div>



          <?php endif; ?>



          <?php if ( $left_list ) : ?>



            <div class="col-1-of-2">

              <?php $delay = 0; foreach ( $right_list as $item ) :
                $delay += 150;
                $item_list = $item['list_links']; ?>



                <b class="education__title"
                  data-aos="fade-left" data-aos-anchor=".education-list-anchor" data-aos-duration="400" data-aos-delay="<?php echo $delay; ?>"
                ><?php echo $item['title']; ?></b>

                <ul class="education__list">



                  <?php foreach( $item_list as $list ) : $delay += 150; ?>



                    <li class="education__item" data-aos-anchor=".education-list-anchor" data-aos="fade-left" data-aos-duration="400" data-aos-delay="<?php echo $delay; ?>">

                      <a href="<?php echo $list['url']; ?>" target="_blank "><?php echo $list['link_title']; ?></a>

                    </li>



                  <?php endforeach; ?>

                  

                </ul>

  

              <?php endforeach; ?>



            </div>

          <?php endif; ?>

        </div>

        <?php if ($footnote) : ?>

            <div class="education__footnote" data-aos="flip-up"><?php echo $footnote; ?></div>

        <?php endif; ?>

      </div>



      <?php endif; ?>

      

    </div>

  </section>



  <?php if( get_field('eighth_section') ) : 

    $section = get_field('eighth_section'); ?>

    <section class="section" id="section-7">

      <div class="container">

        <h2 class="section__title" data-aos="flip-up">Social Media Feed</h2>

        <div class="social-feeds">

          <ul class="social-feeds__icons-list" data-aos="flip-up">

            <?php if ($section['facebook']) : ?>



              <li class="social-feeds__item">

                <button type="button" class="social-feeds__btn active" data-social="fb">

                  <img class="social-feeds__img" data-social="fb" src="<?php echo esc_url( get_template_directory_uri() ); ?>/img/living-here/socials/facebook-1.svg" alt="Facebook">

                </button>

              </li>



            <?php endif; ?>



            <?php if ($section['twitter']) : ?>



              <li class="social-feeds__item">

                <button type="button" class="social-feeds__btn" data-social="twitter">

                  <img class="social-feeds__img" data-social="twitter" src="<?php echo esc_url( get_template_directory_uri() ); ?>/img/living-here/socials/twitter.svg" alt="Twitter">

                </button>

              </li>



            <?php endif; ?>



            <?php if ($section['linkedin']) : ?>



            <li class="social-feeds__item">

              <button type="button" class="social-feeds__btn" data-social="linkedin">

                <img class="social-feeds__img" data-social="linkedin" src="<?php echo esc_url( get_template_directory_uri() ); ?>/img/living-here/socials/linkedin.svg" alt="LinkedIn">

              </button>

            </li>



            <?php endif; ?>



          </ul>

          <div class="section__divider" data-aos="fade-in"></div>

          <div class="social-feeds__container" data-aos="fade-up">

            <div class="social-feeds__twitter" style="display: none">

              <div class="social-feeds__button-wrapper">

                <a href="https://twitter.com/HendersonEcDev?ref_src=twsrc%5Etfw" class="twitter-follow-button" data-size="small"

                  data-show-count="false">Follow @HendersonEcDev</a>

              </div>

              <a class="twitter-timeline" data-lang="en" data-width="500" data-height="700" href="https://twitter.com/HendersonEcDev?ref_src=twsrc%5Etfw">Tweets by HendersonEcDev</a>

            </div>

            <div class="social-feeds__fb">

              <div class="fb-page" data-href="https://www.facebook.com/HendersonEcDev" data-tabs="timeline" data-width="500"

                data-height="700" data-small-header="false" data-adapt-container-width="true" data-hide-cover="false" data-show-facepile="false">

                <blockquote cite="https://www.facebook.com/HendersonEcDev" class="fb-xfbml-parse-ignore">

                  <a href="https://www.facebook.com/HendersonEcDev">Henderson Economic Development</a>

                </blockquote>

              </div>

            </div>

            <div class="social-feeds__linkedin" style="display: none">

              <div class="social-feeds__button-wrapper">

                <script src="//platform.linkedin.com/in.js" type="text/javascript"></script>

                <script type="IN/CompanyProfile" data-id="4308644" data-format="inline" data-related="false"></script>

              </div>

            </div>

          </div>

        </div>

      </div>

    </section>

  <?php endif; ?>



  <div id="fb-root"></div>

  <script>(function (d, s, id) {

      var js, fjs = d.getElementsByTagName(s)[0];

      if (d.getElementById(id)) return;

      js = d.createElement(s); js.id = id;

      js.src = 'https://connect.facebook.net/en_US/sdk.js#xfbml=1&autoLogAppEvents=1&version=v3.0&appId=417453175441791';

      fjs.parentNode.insertBefore(js, fjs);

    }(document, 'script', 'facebook-jssdk'));



    window.onload = function () {

      FB.Event.subscribe('xfbml.render', function (response) {



      });

    };</script>

  <script>$(document).ready(function () {

      $('.social-feeds__btn').on('click', function (e) {

        e.preventDefault();



        $('.social-feeds__icons-list').find('.active').removeClass('active');



        $(this).addClass('active')



        var socialToShow = $(this).attr('data-social');

        var socialContainer = $('.social-feeds__container');



        socialContainer.children().hide().end().find('.social-feeds__' + socialToShow).show();



        switch (socialToShow) {

          case "twitter":

            (function () {

              var executed = false;

              if (!executed) {

                executed = true;

                // Twitter

                window.twttr = (function (d, s, id) {

                  var js, fjs = d.getElementsByTagName(s)[0],

                    t = window.twttr || {};

                  if (d.getElementById(id)) return t;

                  js = d.createElement(s);

                  js.id = id;

                  js.src = "https://platform.twitter.com/widgets.js ";

                  fjs.parentNode.insertBefore(js, fjs);



                  t._e = [];

                  t.ready = function (f) {

                    t._e.push(f);

                  };



                  return t;

                }(document, "script", "twitter-wjs "));

                // When widget is ready, apply custom styles to tweets

                twttr.ready(function (twttr) {

                  twttr.events.bind('loaded', function (event) {



                    // var src = window.location.origin + '/css/twitter.css';



                    // $('iframe.twitter-timeline').contents().find("head").append($('<link rel="stylesheet" type="text/css" href="' + src + '">'));



                    // $('iframe.twitter-timeline').height('auto');

                  });

                });

              }

            })()

            break;

        }

      })

    })



    // Initialize and add the map

    function initMap() {

      // The location of Uluru

      // 34.994656, -116.570946

      var uluru = { lat: 34.994656, lng: -116.570946 };

      // The map, centered at Uluru

      var map = new google.maps.Map(

        document.querySelector('.map'), { zoom: 8, center: uluru });



      var icons = {

        pin: '<?php echo esc_url( get_template_directory_uri() ); ?>/img/living-here/mapIcons/LocationIcon.svg',

        airplane: '<?php echo esc_url( get_template_directory_uri() ); ?>/img/living-here/mapIcons/Airplane.svg',

        semitruck: '<?php echo esc_url( get_template_directory_uri() ); ?>/img/living-here/mapIcons/SemiTruck.svg',

        ship: '<?php echo esc_url( get_template_directory_uri() ); ?>/img/living-here/mapIcons/CargoShip.svg',

        henderson: '<?php echo esc_url( get_template_directory_uri() ); ?>/img/living-here/mapIcons/Henderson_H.svg',

      }



      var locations = [

        {

          icon: {

            url: icons.airplane,

            anchor: new google.maps.Point(15, 15),

            size: new google.maps.Size(30, 30),

            scaledSize: new google.maps.Size(30, 30)

          },

          coords: { lat: 36.084459, lng: -115.153644 }

        }, // McCarran International Airport

        {

          icon: {

            url: icons.airplane,

            anchor: new google.maps.Point(15, 15),

            size: new google.maps.Size(30, 30),

            scaledSize: new google.maps.Size(30, 30)

          }, coords: { lat: 35.975291, lng: -115.138476 }

        }, // Henderson Executive Airport

        {

          icon: {

            url: icons.ship,

            anchor: new google.maps.Point(15, 15),

            size: new google.maps.Size(30, 30),

            scaledSize: new google.maps.Size(30, 30)

          },

          coords: { lat: 33.740799, lng: -118.259679 }

        }, // Los Angeles Shipping Port

        {

          icon: {

            url: icons.ship,

            anchor: new google.maps.Point(15, 15),

            size: new google.maps.Size(30, 30),

            scaledSize: new google.maps.Size(30, 30)

          },

          coords: { lat: 33.769543, lng: -118.215714 },

        }, // Long Beach Port

        {

          icon: {

            url: icons.pin,

            anchor: new google.maps.Point(15, 30),

            size: new google.maps.Size(30, 30),

            scaledSize: new google.maps.Size(30, 30)

          },

          coords: { lat: 36.324816, lng: -115.169781 }

        }, // Las Vegas

        {

          icon: {

            url: icons.semitruck,

            anchor: new google.maps.Point(15, 15),

            size: new google.maps.Size(30, 30),

            scaledSize: new google.maps.Size(30, 30)

          },

          coords: { lat: 35.383138, lng: -115.875487 }

        }, // Semi truck coords - 1

        {

          icon: {

            url: icons.semitruck,

            anchor: new google.maps.Point(15, 15),

            size: new google.maps.Size(30, 30),

            scaledSize: new google.maps.Size(30, 30)

          },

          coords: { lat: 35.668459, lng: -114.458289 }

        }, // Semi truck coords - 2

        {

          icon: {

            url: icons.pin,

            anchor: new google.maps.Point(15, 30),

            size: new google.maps.Size(30, 30),

            scaledSize: new google.maps.Size(30, 30)

          },

          coords: { lat: 34.213108, lng: -118.246156 }

        }, // Los Angeles

        {

          icon: {

            url: icons.henderson,

            anchor: new google.maps.Point(15, 15),

            size: new google.maps.Size(30, 30),

            scaledSize: new google.maps.Size(30, 30)

          },

          coords: { lat: 36.038368, lng: -114.983304 }

        }, // Los Angeles

      ];



      var markers = locations.map(function (location) {

        return new google.maps.Marker({

          position: location.coords,

          map: map,

          icon: location.icon

        });

      });

    }</script>

  <script async defer="defer" src="https://maps.googleapis.com/maps/api/js?key=AIzaSyBH_l_O_NXorzt9Mq_RZixD5AkTNcIs8aw&callback=initMap"></script>

  <?php get_footer(); ?>