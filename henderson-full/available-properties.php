<?php

/**

 * Template Name: Available Properties

 * Template Post Type: page

 *

 * @package Henderson

 * @since Henderson 1.0

 */



get_header(); ?>

  <?php if( get_field( 'hero_image' ) ) : ?>

    <!-- <div class="page-img parallax-window" data-parallax="scroll" data-image-src="<?php // the_field('hero_image'); ?>"></div> -->

    <div class="page-img parallax-window">
      <img src="<?php the_field('hero_image'); ?>">
    </div>
  
  <?php endif; ?>

  <?php if ( have_rows('page_menu') ) : ?>

    <div class="slick-nav-spacer">

      <nav class="slick-nav" data-aos="fade-in" data-aos-delay="200" data-aos-offset="-500">

        <ul class="slick-nav__list">

          <?php 

          $i = 1;

          while ( have_rows('page_menu') ) : the_row();

            $title = get_sub_field('menu_title');

          ?>

            <li class="slick-nav__item">

              <a href="#section-<?php echo $i; ?>" class="slick-nav__anchor" title="<?php echo $title; ?>"><?php echo $title; ?></a>

            </li>

          <?php 

            $i++;

          endwhile; ?>

        </ul>

      </nav>

    </div>

  <?php endif; ?>



  <?php if( get_field('first_section') ) : 

    $section = get_field('first_section');

    $block_1 = $section['columns_block']['first_column'];

    $block_2 = $section['columns_block']['second_column'];

    $block_3 = $section['columns_block']['third_column'];

  ?>

    <section class="section" id="section-1">

      <div class="container">

        <h2 class="section__title" data-aos="flip-up" data-aos-delay="200"><?php echo $section['section_title']; ?></h2>

        <div class="section__divider"></div>

        <div class="section__text" data-aos="flip-up" data-aos-delay="200"><?php echo $section['section_text']; ?></div>

        <div class="space-list">



          <?php if ($block_1) : ?>

            <div class="space-list__item" data-aos="fade-up" data-aos-duration="400" data-aos-delay="200">

              <?php if ($block_1['image']) : ?>

                <div class="space-list__img" style="background-image: url(<?php echo $block_1['image']; ?>)"></div>

              <?php endif; ?>

              <div class="space-list__text-block">

                <div class="space-list__title"><?php echo $block_1['title']; ?></div>

                <div class="space-list__text"><?php echo $block_1['text']; ?></div>

              </div>

            </div>

          <?php endif; ?>



          <?php if ($block_2) : ?>

            <div class="space-list__item" data-aos="fade-up" data-aos-duration="400" data-aos-delay="450">

              <?php if ($block_2['image']) : ?>

                <div class="space-list__img" style="background-image: url(<?php echo $block_2['image']; ?>)"></div>

              <?php endif; ?>

              <div class="space-list__text-block">

                <div class="space-list__title"><?php echo $block_2['title']; ?></div>

                <div class="space-list__text"><?php echo $block_2['text']; ?></div>

              </div>

            </div>

          <?php endif; ?>



          <?php if ($block_3) : ?>

            <div class="space-list__item" data-aos="fade-up" data-aos-duration="400" data-aos-delay="700">

              <?php if ($block_3['image']) : ?>

                <div class="space-list__img" style="background-image: url(<?php echo $block_3['image']; ?>)"></div>

              <?php endif; ?>

              <div class="space-list__text-block">

                <div class="space-list__title"><?php echo $block_3['title']; ?></div>

                <div class="space-list__text"><?php echo $block_3['text']; ?></div>

              </div>

            </div>

          <?php endif; ?>



        </div>

      </div>

    </section>

  <?php endif; ?>



  <?php if( get_field('second_section') ) : 

    $section = get_field('second_section');

    $block_1 = $section['columns_block']['first_column'];

    $block_2 = $section['columns_block']['second_column'];

  ?>

    <section class="section" id="section-2">

      <div class="container">

        <h2 class="section__title" data-aos="flip-up" data-aos-delay="200"><?php echo $section['section_title']; ?></h2>

        <div class="section__divider"></div>

        <div class="section__text" data-aos="flip-up" data-aos-delay="200"><?php echo $section['section_text']; ?></div>

        <div class="space-list">

          <div class="space-list__item" data-aos="fade-up" data-aos-duration="400" data-aos-delay="200">

            <?php if ($block_1['image']) : ?>

              <div class="space-list__img" style="background-image: url(<?php echo $block_1['image']; ?>)"></div>

            <?php endif; ?>

            <div class="space-list__text-block">

              <div class="space-list__title"><?php echo $block_1['title']; ?></div>

              <div class="space-list__text"><?php echo $block_1['text']; ?></div>

            </div>

          </div>

          <div class="space-list__item" data-aos="fade-up" data-aos-duration="400" data-aos-delay="450">

            <?php if ($block_2['image']) : ?>

              <div class="space-list__img" style="background-image: url(<?php echo $block_2['image']; ?>)"></div>

            <?php endif; ?>

            <div class="space-list__text-block">

              <div class="space-list__title"><?php echo $block_2['title']; ?></div>

              <div class="space-list__text"><?php echo $block_2['text']; ?></div>

            </div>

          </div>

        </div>

      </div>

    </section>

  <?php endif; ?>



  <?php if( get_field('third_section') ) : ?>

    <section class="section section--grey" id="section-3">

      <div class="container">

        <h2 class="section__title" data-aos="flip-up" data-aos-delay="200">Search Listings</h2>

        <div class="section__divider"></div>

        <div class="search-listings" data-aos="fade-up" data-aos-duration="400" data-aos-delay="200">

          <iframe id="RealNex" src="<?php the_field('third_section'); ?>" frameborder="0" scrolling="auto" width="100%"></iframe>

        </div>

      </div>

    </section>

  <?php endif; ?>



  <?php if ( have_rows('fourth_section') ) :

    $section = get_field('fourth_section');

    $maps    = $section['maps'];

  ?>

    <section class="section" id="section-4">

      <div class="container">

        <h2 class="section__title" data-aos="flip-up" data-aos-delay="200"><?php echo $section['section_title']; ?></h2>

        <div class="section__divider"></div>

        <div class="section__text" data-aos="flip-up" data-aos-delay="200"><?php echo $section['section_text']; ?></div>

        <div class="maps-list">

          <?php $delay = 200; foreach($maps as $map) : $delay += 50; ?>

            <a href="<?php echo $map['link']; ?>" title="<?php echo $map['title']; ?>" class="maps-list__item" data-aos="fade-up" data-aos-duration="400" data-aos-delay="<?php echo $delay; ?>">

              <img src="<?php echo $map['image']['sizes']['medium']; ?>" class="maps-list__img" alt="<?php echo $map['title']; ?>">

              <p class="maps-list__title"><?php echo $map['title']; ?></p>

            </a>

          <?php endforeach; ?>

        </div>

      </div>

    </section>

  <?php endif; ?>



  <?php get_footer(); ?>