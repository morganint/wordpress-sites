<?php
  /**
   * Template Name: Anual Report
   * Template Post Type: page
   *
   * @package Henderson
   * @since Henderson 1.0
   */
get_header(); ?>

<style>
  @import "<?php echo get_template_directory_uri(); ?>/css/anual-report.css";
</style>

<section class="page-img anual-hero">
  <img src="<?php echo get_template_directory_uri(); ?>/img/anual-page/anual-hero.jpg" alt="">
  <div class="anual-hero__text-block">
    <h1 data-aos="flip-up" data-aos-delay="250" data-aos-offset="-1000">Striking&nbsp;Gold</h1>
    <p data-aos="flip-up" data-aos-delay="600" data-aos-offset="-1000">in the silver state</p>
  </div>
  <div class="anual-hero__title-wrapper">
    <h2 data-aos="fade-in" data-aos-delay="950" data-aos-offset="-1000" class="anual-hero__title">
      <span  data-aos="flip-up" data-aos-delay="1300" data-aos-offset="-1000">
        FY2018&nbsp;Annual&nbsp;Report
      </span>
    </h2>
  </div>
</section>

<section class="opportunities">
  <h3 class="section-title" data-aos="flip-up" data-aos-delay="250">Opportunity Abounds</h3>

  <div class="opportunities__list">
    <div
      data-aos="fade-up"
      data-aos-delay="250"
      data-aos-id="capital-investment"
    >
      <div class="opportunities__item">
        <img class="opportunities__item-icon" src="<?php echo get_template_directory_uri(); ?>/img/anual-page/cash-icon.svg" alt="">
        <b class="opportunities__item-text">
          Projected 3 year <br> Capital Investment
        </b>
        <strong class="opportunities__item-value">
          $<span class="opportunities__item-number">297</span>M
        </strong>
      </div>
    </div>  

    <div
      data-aos="fade-up"
      data-aos-delay="250"
      data-aos-id="projected-employees"
    >
      <div class="opportunities__item">
        <img class="opportunities__item-icon" src="<?php echo get_template_directory_uri(); ?>/img/anual-page/people-icon.svg" alt="">
        <b class="opportunities__item-text">
          Projected new Full-Time <br> Employees within 24 months
        </b>
        <strong class="opportunities__item-value">
          <span class="opportunities__item-number">2,238</span>
        </strong>
      </div>
    </div>

    <div
      data-aos="fade-up"
      data-aos-delay="250"
      data-aos-id="projected-average"
    >
      <div class="opportunities__item">
        <img class="opportunities__item-icon" src="<?php echo get_template_directory_uri(); ?>/img/anual-page/timer-icon.svg" alt="">
        <b class="opportunities__item-text">
          Projected Average <br> Hourly Wage
        </b>
        <strong class="opportunities__item-value">
          $<span class="opportunities__item-number">20.13</span>
        </strong>
      </div>
    </div>
  </div>
</section>

<section class="industry-specs">
  <h3 class="section-title" data-aos="flip-up" data-aos-delay="250">Industry Specs</h3>

  <div
    data-aos="fade-in"
    data-aos-delay="250"
    data-aos-id="graph-slider"
  >
    <div class="graph-section">
      <div class="graph-section__item">
        <h4 class="section-title section-title--small" data-aos="flip-up" data-aos-delay="250">Capital Investment Attracted</h4>

        <div class="graph-section__graph" data-graph-max="200">

          <div class="graph-section__col" data-graph-color="#ED3741" data-graph-value="3">
            <b class="graph-section__col-title">
              Technology
            </b>
            <div class="graph-section__col-value">
              <span>$<span class="graph-section__col-number">3</span>M</span>
            </div>
          </div>

          <div class="graph-section__col" data-graph-color="#ED3741" data-graph-value="18">
            <b class="graph-section__col-title">
              Healthcare & <br>
              Life Sciences
            </b>
            <div class="graph-section__col-value">
              <span>$<span class="graph-section__col-number">18</span>M</span>
            </div>
          </div>

          <div class="graph-section__col" data-graph-color="#ED3741" data-graph-value="33">
            <b class="graph-section__col-title">
              Retail
            </b>
            <div class="graph-section__col-value">
              <span>$<span class="graph-section__col-number">33</span>M</span>
            </div>
          </div>

          <div class="graph-section__col" data-graph-color="#ED3741" data-graph-value="85">
            <b class="graph-section__col-title">
              HQ-Global <br>
              Finance
            </b>
            <div class="graph-section__col-value">
              <span>$<span class="graph-section__col-number">85</span>M</span>
            </div>
          </div>

          <div class="graph-section__col" data-graph-color="#ED3741" data-graph-value="156">
            <b class="graph-section__col-title">
              Advanced <br>
              Manufacturing <br>
              & Logistics
            </b>
            <div class="graph-section__col-value">
              <span>$<span class="graph-section__col-number">156</span>M</span>
            </div>
          </div>
        </div>
      </div>

      <div class="graph-section__item">
        <h4 class="section-title section-title--small" data-aos="flip-up" data-aos-delay="250">
          Average Hourly Wages
        </h4>

        <div class="graph-section__graph" data-graph-max="30">

          <div class="graph-section__col" data-graph-decimals="2" data-graph-color="#C8A877" data-graph-value="14.67">
            <b class="graph-section__col-title">
              Retail
            </b>
            <div class="graph-section__col-value">
              <span>$<span class="graph-section__col-number">14.67</span></span>
            </div>
          </div>

          <div class="graph-section__col" data-graph-decimals="2" data-graph-color="#C8A877" data-graph-value="19.38">
            <b class="graph-section__col-title">
              Advanced <br>
              Manufacturing <br>
              & Logistics
            </b>
            <div class="graph-section__col-value">
              <span>$<span class="graph-section__col-number">19.38</span></span>
            </div>
          </div>

          <div class="graph-section__col" data-graph-decimals="2" data-graph-color="#C8A877" data-graph-value="20.93">
            <b class="graph-section__col-title">
              Healthcare & <br>
              Life Sciences
            </b>
            <div class="graph-section__col-value">
              <span>$<span class="graph-section__col-number">20.93</span></span>
            </div>
          </div>

          <div class="graph-section__col" data-graph-decimals="2" data-graph-color="#C8A877" data-graph-value="24.68">
            <b class="graph-section__col-title">
              Technology
            </b>
            <div class="graph-section__col-value">
              <span>$<span class="graph-section__col-number">24.68</span></span>
            </div>
          </div>

          <div class="graph-section__col" data-graph-decimals="2" data-graph-color="#C8A877" data-graph-value="28.20">
            <b class="graph-section__col-title">
              HQ-Global <br>
              Finance
            </b>
            <div class="graph-section__col-value">
              <span>$<span class="graph-section__col-number">28.20</span></span>
            </div>
          </div>
        </div>
      </div>

      <div class="graph-section__item">
        <h4 class="section-title section-title--small" data-aos="flip-up" data-aos-delay="250">
          Jobs Created
        </h4>

        <div class="graph-section__graph" data-graph-max="1000">
          <div class="graph-section__col" data-graph-color="#90D1C8" data-graph-value="57">
            <b class="graph-section__col-title">
              Healthcare & <br>
              Life Sciences
            </b>
            <div class="graph-section__col-value">
              <span><span class="graph-section__col-number">57</span></span>
            </div>
          </div>

          <div class="graph-section__col" data-graph-color="#90D1C8" data-graph-value="363">
            <b class="graph-section__col-title">
              Technology
            </b>
            <div class="graph-section__col-value">
              <span><span class="graph-section__col-number">363</span></span>
            </div>
          </div>

          <div class="graph-section__col" data-graph-color="#90D1C8" data-graph-value="420">
            <b class="graph-section__col-title">
              HQ-Global <br>
              Finance
            </b>
            <div class="graph-section__col-value">
              <span><span class="graph-section__col-number">420</span></span>
            </div>
          </div>

          <div class="graph-section__col" data-graph-color="#90D1C8" data-graph-value="462">
            <b class="graph-section__col-title">
              Retail
            </b>
            <div class="graph-section__col-value">
              <span><span class="graph-section__col-number">462</span></span>
            </div>
          </div>

          <div class="graph-section__col" data-graph-color="#90D1C8" data-graph-value="936">
            <b class="graph-section__col-title">
              Advanced <br>
              Manufacturing <br>
              & Logistics
            </b>
            <div class="graph-section__col-value">
              <span><span class="graph-section__col-number">936</span></span>
            </div>
          </div>
        </div>
      </div>
    </div>
    <button class="graph-section__prev">
      <img src="<?php echo get_template_directory_uri(); ?>/img/anual-page/slider-arrow.svg" alt="">
    </button>
    <button class="graph-section__next">
      <img src="<?php echo get_template_directory_uri(); ?>/img/anual-page/slider-arrow.svg" alt="">
    </button>
  </div>
</section>

<section
  class="section-number-deals"
  data-speed="0.5" 
  data-parallax="scroll"
  data-image-src="<?php echo get_template_directory_uri(); ?>/img/anual-page/city-bg.svg"
>
  <h3 class="section-title" data-aos="flip-up" data-aos-delay="250">Number of Projects</h3>

    <div class="number-deals__wrapper">
      <div
        data-aos="fade-in"
        data-aos-delay="250"
        data-aos-id="number-deals"
      >
        <div class="opportunities__item">
          <img class="opportunities__item-icon" src="<?php echo get_template_directory_uri(); ?>/img/anual-page/deal.svg" alt="">

          <strong class="opportunities__item-value">
            <span class="opportunities__item-number">11</span>
          </strong>

          <p
            class="opportunities__item-text opportunities__item-text--red"
            data-aos="flip-up"
            data-aos-delay="2000"
            data-aos-anchor=".number-deals__wrapper"
          >
            7 Recruitments <br> 4 Expansions
          </p>
        </div>
    </div>  
  </div>
</section> 

<section class="section-marked-exp">
  <h3 class="section-title" data-aos="flip-up" data-aos-delay="250">Marketing the Experience</h3>
  <div data-aos="fade-in" data-aos-delay="250">
    <img
      src="<?php echo get_template_directory_uri(); ?>/img/anual-page/hendo-logo-light.svg"
      alt="" class="hendo-logo-light">
  </div>
  <p data-aos="fade-in" data-aos-delay="250" class="section-text">
    The City of Henderson Economic Development Department undertook a strategic focus on refining a brand and marketing strategy to enhance the visibility of Henderson as a place to do business highlighting the competitive advantages and access to quality of life.
  </p>

  <div class="plus-block">
    <div data-aos="fade-right" data-aos-delay="250">
      <div class="plus-block__item">
        <b class="plus-block__item-title">Brand Foundation</b>
        <p class="plus-block__item-text">
          The people + the businesses + local government + schooling, together create a favorable environment for businesses to thrive.
        </p>
      </div>
    </div>

    <span class="plus-block__plus-icon"
      data-aos="fade-in" data-aos-delay="500" data-aos-anchor=".plus-block"
    >
      <img src="<?php echo get_template_directory_uri(); ?>/img/anual-page/plus-icon.svg" alt="">
    </span>

    <div data-aos="fade-left" data-aos-delay="250">
      <div class="plus-block__item">
        <b class="plus-block__item-title">Marketing Goal</b>
        <p class="plus-block__item-text">
          Increase smart economic development in Henderson to give residents access to a stable and growing economy and tax base to provide an increasing quality of life.
        </p>
      </div>
    </div>
  </div>

  <h3 class="section-title" data-aos="flip-up" data-aos-delay="250">The Result</h3>
  <p data-aos="fade-in" data-aos-delay="250" class="section-text">
    As the visual touchpoint of the brand platform, the logo needed to align with the culture and vision of Henderson’s Economic Development efforts. It had to be simple and powerful, yet friendly and approachable.
  </p>

  <div class="animation-container animation-container--colored" data-aos="zoom-in" data-aos-delay="250">
    <canvas id="canvas-colored" width="900" height="187" style="max-width: 900px"></canvas>
    <div class="animation-container__overlay " style="pointer-events:none; overflow:hidden;
      position: absolute; left: 0px; top: 0px; display: block; ">
    </div>
  </div>
</section>

<section
  class="section-industry-focus"
>
  <div class="section-industry-focus-img"
    data-speed="0.3" 
    data-parallax="scroll"
    data-aos="fade-in"
    data-aos-delay="250"
    data-image-src="<?php echo get_template_directory_uri(); ?>/img/anual-page/industry-focus-bg.jpg"
  ></div>

  <div class="section-text-block"
    data-aos="fade-up"
    data-aos-delay="250"
  >
    <img
      data-aos="zoom-in"
      data-aos-delay="300"
      src="<?php echo get_template_directory_uri(); ?>/img/anual-page/loupe.svg"
      class="section-icon" alt="">
    <h3 class="section-title" data-aos="flip-up" data-aos-delay="250">Targeting Success</h3>
    <p data-aos="fade-in" data-aos-delay="250" class="section-text">
      Henderson’s culture of opportunity entices new and existing businesses to grow and expand within a friendly tax structure and supportive business environment, paired with an abundance of natural resources and unmatched quality of life for businesses, employees, and families. Throughout the year, Henderson achieved many successes in attracting companies within each industry. Local businesses and community partners also completed expansions of their business operations and many large programs.
    </p>
  </div>

  <div class="partners-block" data-aos="fade-in" data-aos-delay="250">
    <div class="partners-block__text-block">
      <b class="partners-block__text-lead" data-aos="flip-up" data-aos-delay="350">
        HEADQUARTERS & <br>
        GLOBAL FINANCE
      </b>
      <p class="partners-block__text" data-aos="fade-in" data-aos-delay="350">
        With companies like Barclays, Toyota Financial Savings Bank and Charles Schwab operating here, Henderson has the ideal workforce for the financial sector. Regional, national or international offices established in Henderson can benefit directly from the more than 6 million conventioneers coming to the region each year.
      </p>
    </div>

    <div class="partners-block__logos" data-aos="fade-in" data-aos-delay="450">
      <div class="partners-block__logo-item">
        <img src="<?php echo get_template_directory_uri(); ?>/img/anual-page/raiders-logo.png" alt="Raiders Logo">
        <div class="partners-block__partner-descr">
          The Las Vegas Raiders selected West Henderson for their new headquarters and practice facility, which will bring a $75 million capital investment and 250 full-time jobs for non-players.
          <button class="partners-block__partner-descr-close"></button>
        </div>
      </div>
      <div class="partners-block__logo-item">
        <img src="<?php echo get_template_directory_uri(); ?>/img/anual-page/toyota-logo.png" alt="Toyota Logo">
        <div class="partners-block__partner-descr">
          Toyota Financial Savings Bank, headquartered in Henderson, enhanced the community’s high-achieving K-12 schools with its program to help students become financially literate.
          <button class="partners-block__partner-descr-close"></button>
        </div>
      </div>
      <div class="partners-block__logo-item">
        <img src="<?php echo get_template_directory_uri(); ?>/img/anual-page/schwab-logo.png" alt="Charles Schwab Logo">
        <div class="partners-block__partner-descr">
          Charles Schwab consolidated its Nevada customer service operations in Henderson, creating at least 80 new jobs with capex of $2 million.
          <button class="partners-block__partner-descr-close"></button>
        </div>
      </div>
    </div>
  </div>

  <div class="partners-block" data-aos="fade-in" data-aos-delay="250">
    <div class="partners-block__logos" data-aos="fade-in" data-aos-delay="450">
      <div class="partners-block__logo-item">
        <img src="<?php echo get_template_directory_uri(); ?>/img/anual-page/xtreme-logo.png" alt="Xtreme Logo">
        <div class="partners-block__partner-descr">
          Xtreme Manufacturing, LLC invested $10.4 million in expanding its fabrication operations at its Henderson headquarters, almost doubling its workforce in order to meet current production demands.
          <button class="partners-block__partner-descr-close"></button>
        </div>
      </div>
      <div class="partners-block__logo-item">
        <img src="<?php echo get_template_directory_uri(); ?>/img/anual-page/turano-logo.png" alt="Turano Logo">
        <div class="partners-block__partner-descr">
          Turano Bakery, the nation’s leading premium bakery, began operations in Spring 2018 at its new 125,000-square-foot facility in West Henderson with more than $53 million in capital investments and 70 jobs.
          <button class="partners-block__partner-descr-close"></button>
        </div>
      </div>
      <div class="partners-block__logo-item">
        <img src="<?php echo get_template_directory_uri(); ?>/img/anual-page/smiths-logo.png" alt="Smith’s Food Logo">
        <div class="partners-block__partner-descr">
          Smith’s Food & Drug Stores brought a nearly $13.5 million dry-good distribution center to West Henderson with 270 jobs serving multiple Western states.
          <button class="partners-block__partner-descr-close"></button>
        </div>
      </div>
    </div>
    <div class="partners-block__text-block">
      <b class="partners-block__text-lead" data-aos="flip-up" data-aos-delay="350">
        ADVANCED MANUFACTURING <br> & LOGISTICS
      </b>
      <p class="partners-block__text" data-aos="fade-in" data-aos-delay="350">
        Centrally located between five major markets, Henderson is a major hub for shipping and logistics. New interstate connectivity and expansive land offerings of West Henderson, paired with an educated and capable labor force, create a fruitful business climate for manufacturing and logistics.
      </p>
    </div>
  </div>

  <div class="partners-block" data-aos="fade-in" data-aos-delay="250">
    <div class="partners-block__text-block">
      <b class="partners-block__text-lead" data-aos="flip-up" data-aos-delay="350">
        HEALTHCARE & <br> LIFE SCIENCES
      </b>
      <p class="partners-block__text" data-aos="fade-in" data-aos-delay="350">
        With the recent addition of innovative new hospitals and cutting-edge medical research and education facilities such as Spectrum Pharmaceutical, Touro University, Roseman University of Health Sciences, and the Comprehensive Cancer Centers of Nevada, Henderson is emerging as a premier medical region.
      </p>
    </div>

    <div class="partners-block__logos" data-aos="fade-in" data-aos-delay="450">
      <div class="partners-block__logo-item">
        <img src="<?php echo get_template_directory_uri(); ?>/img/anual-page/arthrex-logo.png" alt="Anthrex Logo">
        <div class="partners-block__partner-descr">
          Goode Surgical partnered with medical device company Anthrex to offer its state-of-the-art surgical training center to help surgeons perfect their surgical skills.
          <button class="partners-block__partner-descr-close"></button>
        </div>
      </div>
      <div class="partners-block__logo-item">
        <img src="<?php echo get_template_directory_uri(); ?>/img/anual-page/unlv-logo.png" alt="University of Nevada Logo">
        <div class="partners-block__partner-descr">
          University of Nevada, Las Vegas Medical School successfully completed its first year
          <button class="partners-block__partner-descr-close"></button>
        </div>
      </div>
      <div class="partners-block__logo-item">
        <img src="<?php echo get_template_directory_uri(); ?>/img/anual-page/touro-logo.png" alt="Touro University Logo">
        <div class="partners-block__partner-descr">
          Touro University, located in Henderson, continued graduating the most doctors in the region.
          <button class="partners-block__partner-descr-close"></button>
        </div>
      </div>
    </div>
  </div>

  <div class="partners-block" data-aos="fade-in" data-aos-delay="250">
    <div class="partners-block__logos" data-aos="fade-in" data-aos-delay="450">
      <div class="partners-block__logo-item">
        <img src="<?php echo get_template_directory_uri(); ?>/img/anual-page/southshore-logo.png" alt="SouthShore Logo">
        <div class="partners-block__partner-descr">
          Signaling a major move in the Lake Las Vegas comeback, SouthShore Country Club LLV replaced the former SouthShore Golf Club after a group of members purchased the course.
          <button class="partners-block__partner-descr-close"></button>
        </div>
      </div>
      <div class="partners-block__logo-item">
        <img src="<?php echo get_template_directory_uri(); ?>/img/anual-page/the-block-logo.png" alt="The Block Logo">
        <div class="partners-block__partner-descr">
          "The Block" a massive mixed-use development of residential units along with half-a-million square feet of retail and entertainment space is underway on about 110 acres in West Henderson.
          <button class="partners-block__partner-descr-close"></button>
        </div>
      </div>
      <div class="partners-block__logo-item">
        <img src="<?php echo get_template_directory_uri(); ?>/img/anual-page/costco-logo.png" alt="Costco Logo">
        <div class="partners-block__partner-descr">
          Costco built its second location in Henderson near St. Rose and Maryland parkways, adding 250 new permanent jobs.
          <button class="partners-block__partner-descr-close"></button>
        </div>
      </div>
    </div>
    <div class="partners-block__text-block">
      <b class="partners-block__text-lead" data-aos="flip-up" data-aos-delay="350">
        HOSPITALITY, <br> TOURISM & RETAIL
      </b>
      <p class="partners-block__text" data-aos="fade-in" data-aos-delay="350">
        In a region already known for its strong presence in the hospitality, tourism, and retail industries, Henderson is a magnet for convention and resort opportunities as well as corporate meeting and adventure travel.
      </p>
    </div>
  </div>

  <div class="partners-block" data-aos="fade-in" data-aos-delay="250">
    <div class="partners-block__text-block">
      <b class="partners-block__text-lead" data-aos="flip-up" data-aos-delay="350">
        TECHNOLOGY
      </b>
      <p class="partners-block__text" data-aos="fade-in" data-aos-delay="350">
        A growing number of relocating companies and entrepreneurs in Henderson have made their mark on the technology industry, including national names such as Vadatech and K2 Energy. Industry growth combined with investment from higher education has resulted in a large technologically-savvy local workforce.
      </p>
    </div>

    <div class="partners-block__logos" data-aos="fade-in" data-aos-delay="450">
      <div class="partners-block__logo-item">
        <img src="<?php echo get_template_directory_uri(); ?>/img/anual-page/virtual-guard-logo.png" alt="Virtual Guard Logo">
        <div class="partners-block__partner-descr">
          Virtual Guard, Inc., a video-monitoring company and developer of cutting-edge technology in the perimeter security sector, relocated its headquarters and operations from Los Angeles to Henderson.
          <button class="partners-block__partner-descr-close"></button>
        </div>
      </div>
      <div class="partners-block__logo-item">
        <img src="<?php echo get_template_directory_uri(); ?>/img/anual-page/stir-logo.png" alt="Startup in Residence Logo">
        <div class="partners-block__partner-descr">
          The City was selected as one of 31 agencies to participate in the Startup in Residence program to connect citizens with government agencies to solve civic challenges.
          <button class="partners-block__partner-descr-close"></button>
        </div>
      </div>
      <div class="partners-block__logo-item">
        <img src="<?php echo get_template_directory_uri(); ?>/img/anual-page/smart-cities-logo.svg" alt="Smart Cities Logo">
        <div class="partners-block__partner-descr">
          Henderson continues developing Smart Cities initiatives to strengthen the bond between entrepreneurs, city staff, and current companies.
          <button class="partners-block__partner-descr-close"></button>
        </div>
      </div>
    </div>
  </div>
</section>

<script>
  document.addEventListener('aos:in:capital-investment', function(ele) {
    $(ele.detail.detail || ele.detail).find('.opportunities__item-number').prop('number', 180).animateNumber({
      number: 297,
      easing: 'easeOutQuart',
    }, 6000)
  }, { once: true });

  document.addEventListener('aos:in:projected-employees', function(ele) {
    var comma_separator_number_step = $.animateNumber.numberStepFactories.separator(',')

    $(ele.detail.detail || ele.detail).find('.opportunities__item-number').prop('number', 1800).animateNumber({
      number: 2238,
      easing: 'easeOutQuart',
      numberStep: comma_separator_number_step
    }, 7000)
  }, { once: true });

  document.addEventListener('aos:in:projected-average', function(ele) {
    // how many decimal places allows
    var decimal_places = 2;
    var decimal_factor = decimal_places === 0 ? 1 : Math.pow(10, decimal_places);

    $(ele.detail.detail || ele.detail).find('.opportunities__item-number').prop('number', 10).animateNumber({
      number: 20.13  * decimal_factor,

      numberStep: function(now, tween) {
        var floored_number = Math.floor(now) / decimal_factor,
            target = $(tween.elem);

        if (decimal_places > 0) {
          // force decimal places even if they are 0
          floored_number = floored_number.toFixed(decimal_places);

          // replace '.' separator with ','
          // floored_number = floored_number.toString().replace('.', ',');
        }

        target.text(floored_number);
      },
      easing: 'easeOutQuart',
    }, 5000)
  }, { once: true });

  var achivedSliderSection = false,
      sliderInitialized = false;

  var graphHeight = $('.graph-section__graph ').innerHeight();

  function animateGraph() {
    var currentSlide = $('.slick-current');

    if(currentSlide.data('animated-complete')) {
      return;
    }

    $('.slick-current .graph-section__col').each(function() {
      var val = $(this).data('graph-value');
      var color = $(this).data('graph-color');
      var withDecimals = +$(this).data('graph-decimals');
      var max = +$(this).parent('.graph-section__graph').data('graph-max');

      var decimal_places = withDecimals || 0;
      var decimal_factor = decimal_places === 0 ? 1 : Math.pow(10, decimal_places);

      var colHeight = graphHeight * ( val / max );
      colHeight = colHeight > 54 ? colHeight : 54;

      $(this).find('.graph-section__col-value')
        .css('background-color', color)
        .animate({'height': colHeight + 'px' }, 7000, 'easeOutQuart')

      $(this).find('span.graph-section__col-number').animateNumber({
        number: val * decimal_factor,
        easing: 'easeOutQuart',
        numberStep: function(now, tween) {
          var floored_number = Math.floor(now) / decimal_factor,
              target = $(tween.elem);

          if (decimal_places > 0) {
            // force decimal places even if they are 0
            floored_number = floored_number.toFixed(decimal_places);

            // replace '.' separator with ','
            // floored_number = floored_number.toString().replace('.', ',');
          }

          target.text(floored_number);
        },
      }, 4000)
    })

    currentSlide.data('animated-complete', true)
  }

  document.addEventListener('aos:in:graph-slider', function(ele) {
    if (sliderInitialized) {
      animateGraph();
    } else {
      achivedSliderSection = true;
    }
  }, { once: true });

  document.addEventListener('aos:in:number-deals', function(ele) {
    $(ele.detail.detail || ele.detail).find('.opportunities__item-number').animateNumber({
      number: 11,
      easing: 'easeOutQuart',
    }, 2000)
  }, { once: true });

  $(document).on('ready', function() {
    $('.graph-section').on('init', function() {
      
      if (achivedSliderSection) {
        animateGraph();
      } else {
        sliderInitialized = true;
      }
    })

    $('.graph-section').on('afterChange', function() {
      animateGraph();
    })

    $('.graph-section').slick({
      dots: true,
      infinite: false,
      arrows: true,
      speed: 800,
      slidesToShow: 1,
      slidesToScroll: 1,
      cssEase: 'ease-in',
      dotsClass: 'graph-section__dots',
      prevArrow: '.graph-section__prev',
      nextArrow: '.graph-section__next'
    });
  })

  $(document).on('ready', function() {
    $('.partners-block__partner-descr-close').on('click', function() {
      $(this).closest('.partners-block__partner-descr').stop().fadeOut('fast');
    })

    $('.partners-block__logo-item').on('mouseleave', function() {
      $(this).find('.partners-block__partner-descr').stop().fadeOut('fast');
    })

    $('.partners-block__logo-item').on('mouseover', function() {
      $(this).find('.partners-block__partner-descr').stop().fadeIn('fast');
    })
  })

</script>
<script src="<?php echo get_template_directory_uri(); ?>/js/animated-logo-loop-colored.min.js"></script>

<?php get_footer(); ?>
