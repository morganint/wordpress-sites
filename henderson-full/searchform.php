<form role="search" method="get" id="searchform" action="<?php echo home_url( '/' ); ?>">
    <div><label class="screen-reader-text" for="s">Search for:</label>
        <input type="text" class="search-field" value="" name="s" id="s" placeholder="Search..." />
        <button type="submit" id="searchsubmit" class="searchsubmit" value=""></button>
    </div>
</form>