<?php
/**
 * Template Name: Competitive Advantage
 * Template Post Type: page
 *
 * @package Henderson
 * @since Henderson 1.0
 */

get_header(); ?>

  <!-- <div class="page-img parallax-window" data-parallax="scroll" data-image-src="img/available-properties/hero.jpg"></div>
   -->

  <?php if( get_field( 'hero_image' ) ) : ?>
    <section id="hero" data-aos="fade-in">
      <?php while( have_rows( 'hero_image' )) : the_row(); 
        if (get_sub_field('no_parallax')) : ?>
          <div class="page-img__wrapper">
            <img src="<?php the_sub_field( 'image' ); ?>">
          </div>
        <?php else : ?>
        
        <div class="page-img parallax-window">
          <img src="<?php the_field('hero_image'); ?>">
        </div>
        <?php endif; ?>
      <?php endwhile; ?>
      <div class="hero__text">
        <p class="hero__small-font" data-aos="flip-up" data-aos-delay="250" data-aos-offset="-1000">a business opportunity</p>
        <p class="hero__big-font" data-aos="flip-up" data-aos-delay="500" data-aos-offset="-1000">worth a</p>
        <p class="hero__big-font" data-aos="flip-up" data-aos-delay="750" data-aos-offset="-1000">second take</p>
      </div>  
    </section>
  <?php endif; ?>

  <section class="section" id="wow-facts">
    <div class="container">
      <h2 class="section__title" data-aos="flip-up" data-aos-delay="200" title="<?php the_field('title'); ?>" ><?php the_field('title'); ?></h2>
      <div class="section__divider"></div>

      <?php if (have_rows('block') ) : $count = 0; $delay = 0; ?>
        
        <div class="wow-facts">

          <?php while ( have_rows('block') ) : the_row();
            $count++;
            $delay += 150;
            $section = get_sub_field( 'section_number' ) ? '#section-' . get_sub_field( 'section_number' ) : '';
          ?>
          
            <a data-aos="fade-up" data-aos-duration="400" data-aos-delay=<?php echo $delay; ?> class="wow-facts__item" href="<?php echo get_sub_field( 'page_link' ) . $section; ?>">
                <div class="wow-facts__item-aside">
                  <span class="wow-facts__wow">Wow facts</span>
                  <div class="wow-facts__circle">
                    <span class="wow-facts__number"><?php echo $count; ?></span>
                  </div>
                </div>
                <div class="wow-facts__text-block">
                  <img class="wow-facts__icon" src="<?php the_sub_field( 'icon' ); ?>" alt="<?php the_sub_field( 'title' ); ?>">
                  <h3 class="wow-facts__title" title="<?php the_sub_field( 'title' ); ?>"><?php the_sub_field( 'title' ); ?></h3>
                    <div class="wow-facts__text"><?php the_sub_field( 'content' ); ?></div>
                    <span class="wow-facts__link" href="<?php echo get_sub_field( 'page_link' ) . $section; ?>"> Learn More ></span>
                </div>
            </a>

          <?php endwhile; ?>

        </div>

      <?php endif; ?>
      <div data-aos="fade-up" data-aos-delay="200">
        <a class="wow-facts__contact-btn btn btn--news" href="/contact/">Contact Us</a>
      </div>
    </div>
  </section>

  <?php get_footer(); ?>