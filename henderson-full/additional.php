

<?php

/**

 * Template Name: Additional Resources

 * Template Post Type: page

 *

 * @package Henderson

 * @since Henderson 1.0

 */



get_header(); ?>



    <?php if ( have_rows('business_resources') ) : ?>

        

        <section class="section" id="business-resources">

            <div class="container">

                <h2 class="section__title" data-aos="flip-up">Business Resources</h2>

                <div class="section__divider" data-aos="fade-in"></div>

                <ul class="resources-list resources-list-top-anchor">

                    <?php $delay = 0; while ( have_rows('business_resources') ) : the_row();  $delay += 150; ?>

                        <li class="resources-list__item"
                            data-aos-anchor=".resources-list-top-anchor" data-aos="fade-up"
                            data-aos-duration="400" data-aos-delay="<?php echo $delay; ?>"
                        >

                            <a href="<?php the_sub_field( 'link' ); ?>" class="resources-list__link" title="<?php the_sub_field( 'title' ); ?>">

                                <div class="resources-list__img" style="background-image: url(<?php the_sub_field( 'image' ); ?>)"></div>

                                <p class="resources-list__text"><?php the_sub_field( 'title' ); ?></p>

                            </a>

                        </li>

                    <?php endwhile; ?>

                </ul>

            </div>

        </section>



    <?php endif; ?>



    <?php if ( have_rows('community_resources') ) : ?>



        <section class="section" id="community-resources">

            <div class="container">

                <h2 class="section__title" data-aos="flip-up">Community Resources</h2>

                <div class="section__divider" data-aos="fade-in"></div>

                <ul class="resources-list resources-list-bottom-anchor">

                    <?php $delay = 0; while ( have_rows('community_resources') ) : the_row();  $delay += 150; ?>

                        <li class="resources-list__item"
                            data-aos-anchor=".resources-list-bottom-anchor" data-aos="fade-up"
                            data-aos-duration="400" data-aos-delay="<?php echo $delay; ?>"
                        >

                            <a href="<?php the_sub_field( 'link' ); ?>" class="resources-list__link" title="<?php the_sub_field( 'title' ); ?>">

                                <div class="resources-list__img" style="background-image: url(<?php the_sub_field( 'image' ); ?>)"></div>

                                <p class="resources-list__text"><?php the_sub_field( 'title' ); ?></p>

                            </a>

                        </li>

                    <?php endwhile; ?>

                </ul>

            </div>

        </section>

    

    <?php endif; ?>



    <?php get_footer(); ?>