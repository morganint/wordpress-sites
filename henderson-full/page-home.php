<?php
/**
 * The home page template file
 *
 * @package Henderson
 * @since Henderson 1.0
 */

get_header(); ?>
<?php
    $heroImg = get_field('hero_image');

    if ($heroImg) : ?>
      <div class="promo" data-aos="fade-in">
        <div class="promo__video-container">
          <video class="promo__video" playsinline autoplay muted loop poster="<?php echo get_template_directory_uri(); ?>/media/home-hero.jpg">
            <source src="<?php echo get_template_directory_uri(); ?>/media/home-hero.mp4" type="video/mp4">
            <source src="<?php echo get_template_directory_uri(); ?>/media/home-hero.webm" type="video/webm">
          </video>
        </div>
        <img class="promo__img" src="<?php echo $heroImg['sizes']['thumb_x2']; ?>" srcset="
        <?php echo $heroImg['sizes']['thumb_414']; ?> 414w, 
        <?php echo $heroImg['sizes']['thumb_768']; ?> 768w, 
        <?php echo $heroImg['sizes']['thumb_1200']; ?> 1200w, 
        <?php echo $heroImg['sizes']['thumb_x1']; ?> 1920w, 
        <?php echo $heroImg['sizes']['thumb_x2']; ?> 2640w"
          sizes="
            (max-width: 2640px) 100vw,
            (max-width: 1920px) 100vw,
            (max-width: 1200px) 100vw,
            (max-width: 768px) 100vw,
            (max-width: 320px) 100vw,
          100vw" alt=" <?php echo $heroImg['alt']; ?>" title="<?php echo $heroImg['alt']; ?>" src="<?php echo $heroImg['size']['thumb_1200']; ?>">
          <div class="home__hero-text">
          <div>
            <p data-aos="flip-up" data-aos-delay="250" data-aos-offset="-1000" class="white-text">welcome to</p>
            <p data-aos="flip-up" data-aos-delay="500" data-aos-offset="-1000" class="red-text hash "><span>#</span>wow</p>
            <p data-aos="flip-up" data-aos-delay="750" data-aos-offset="-1000" class="red-text ">valley</p>
            <p data-aos="flip-up" data-aos-delay="1000" data-aos-offset="-1000" class="red-text">nv</p>
            <p data-aos="fade-up" data-aos-delay="1200" data-aos-offset="-1000">
              <a class="promo-btn btn" href="/competitive-advantage/">Our Competitive Advantage</a>
            <p>
          </div>
        </div>
        <!-- <span class="promo__arrow"></span> -->
      </div>
    <?php 
    
    endif; 
    
    $first_section = get_field('first_section');

    if ( $first_section ) :
        $icon = $first_section['section_icon'];
        $title = $first_section['section_title'];
        $text = $first_section['section_text'];
    ?>
   
        <section class="section">
        <div class="section__letter section__letter--l" data-aos="fade-up" style="background-image: url(<?php echo $icon; ?>);"></div>
          <div class="container">
            <h2 class="section__title" data-aos="flip-up" data-aos-delay="200"><?php echo $title; ?></h2>
            <div class="section__divider section__divider--small"></div>
            <p class="section__text" data-aos="flip-up" data-aos-delay="200"><?php echo $text; ?></p>
          </div>
          <div class="welcome-list">
            <?php
              $firstPage = $first_section['section_direct_pages']['first_page'];
              $secondPage = $first_section['section_direct_pages']['second_page'];
              $thirdPage = $first_section['section_direct_pages']['third_page'];

              if ( $firstPage ) : ?>
                <div data-aos="fade-up" data-aos-duration="400" data-aos-delay="200">
                  <a href="<?php echo $firstPage['page_link']['url']; ?>" title="<?php echo $firstPage['page_link']['title']; ?>" target="<?php echo $firstPage['page_link']['target']; ?>" class="welcome-list__item">
                    <div class="welcome-list__img-wrap">
                      <img src="<?php echo $firstPage['page_image']['sizes']['thumb_768']; ?>" class="welcome-list__img" alt="<?php echo $firstPage['page_image']['alt'];?>">
                    </div>
                    <div class="welcome-list__text-block">
                      <h2 class="welcome-list__title"><?php echo $firstPage['page_title']; ?></h2>
                      <p class="welcome-list__text"><?php echo $firstPage['page_text']; ?></p>
                    </div>
                    <span class="welcome-list__read-more">Read more</span>
                  </a>
                </div>
              <?php endif; 

              if ( $secondPage ) : ?>
              <div data-aos="fade-up" data-aos-duration="400" data-aos-delay="500">
                <a href="<?php echo $secondPage['page_link']['url']; ?>" title="<?php echo $secondPage['page_link']['title']; ?>" target="<?php echo $secondPage['page_link']['target']; ?>" class="welcome-list__item">
                  <div class="welcome-list__img-wrap">
                    <img src="<?php echo $secondPage['page_image']['sizes']['thumb_768']; ?>" class="welcome-list__img" alt="<?php echo $secondPage['page_image']['alt'];?>">
                  </div>
                  <div class="welcome-list__text-block">
                    <h2 class="welcome-list__title"><?php echo $secondPage['page_title']; ?></h2>
                    <p class="welcome-list__text"><?php echo $secondPage['page_text']; ?></p>
                  </div>
                  <span class="welcome-list__read-more">Read more</span>
                </a>
              </div>


              <?php endif; 

              if ( $thirdPage ) : ?>
                <div data-aos="fade-up" data-aos-duration="400" data-aos-delay="800">
                  <a href="<?php echo $thirdPage['page_link']['url']; ?>" title="<?php echo $thirdPage['page_link']['title']; ?>" target="<?php echo $thirdPage['page_link']['target']; ?>" class="welcome-list__item">
                    <div class="welcome-list__img-wrap">
                      <img src="<?php echo $thirdPage['page_image']['sizes']['thumb_768']; ?>" class="welcome-list__img" alt="<?php echo $thirdPage['page_image']['alt'];?>">
                    </div>
                    <div class="welcome-list__text-block">
                      <h2 class="welcome-list__title"><?php echo $thirdPage['page_title']; ?></h2>
                      <p class="welcome-list__text"><?php echo $thirdPage['page_text']; ?></p>
                    </div>
                    <span class="welcome-list__read-more">Read more</span>
                  </a>
                </div>
               <?php endif; ?>

          </div>
        </section>

    <?php endif;
    
    $second_section = get_field('second_section');

    if ($second_section) :
      $icon = $second_section['section_icon'];
      $title = $second_section['section_title'];
      $text = $second_section['section_text'];
      $slides = $second_section['slider'];
      $learnMore = $second_section['link_learn_more'];
      ?>

      <section class="section section--grey">
      <div class="section__letter section__letter--b" data-aos="fade-up" style="background-image: url(<?php echo $icon; ?>)"></div>
        <h2 class="section__title" data-aos="flip-up" data-aos-delay="200"><?php echo $title; ?></h2>
        <div class="section__divider section__divider--small"></div>
        <div class="container">
          <p class="section__text" data-aos="flip-up" data-aos-delay="200"><?php echo $text; ?></p>
        </div>
        <?php if ( !Empty($slides) ) : ?>
          <div class="container">
            <div class="lifestyle-slider">
              <ul class="lifestyle-slider__list" data-aos="fade-up" data-aos-duration="400" data-aos-delay="200">
                <?php 
                  $delay = 0;
                  foreach ($slides as $slide) :
                  $delay += 200;
                  $image = $slide['image'];
                  $imageCropUrl = $image['sizes']['thumb_500'];
                ?>
                  <li class="lifestyle-slider__item">
                    <div class="lifestyle-slider__img-wrap">
                      <img src="<?php echo $imageCropUrl; ?>" alt="<?php echo $image['alt']; ?>">
                    </div>
                    <div class="lifestyle-slider__collapse">
                      <div class="lifestyle-slider__title"><?php echo $slide['title']; ?></div>
                      <div class="lifestyle-slider__text"><?php echo $slide['content']; ?></div>
                    </div>
                  </li>
                <?php endforeach; ?>
              </ul>
              <div data-aos="fade-in"  data-aos-offset="200">
                <span class="slider-navs__next"></span>
                <span class="slider-navs__prev"></span>
              </div>
            </div>
            <div data-aos="fade-up" data-aos-offset="200">
              <a href="<?php echo $learnMore['url']; ?>" alt="<?php echo $learnMore['title']; ?>" title="<?php echo $learnMore['title']; ?>" class="btn btn--lifestyle">Learn more</a>
            </div>
          </div>
        <?php endif; ?>

      </section>

    <?php endif; ?>

    <section class="section">
      <?php 

        $third_section = get_field('third_section');

        if ($third_section) :
          $icon = $third_section['section_icon'];
          $title = $third_section['section_title'];
          $text = $third_section['section_text'];
          $link = $third_section['banner']['button'];

      ?>
          <div class="container">
            <div class="section__letter section__letter--n"  data-aos="fade-up" style="background-image: url(<?php echo $icon; ?>)"></div>
            <h2 class="section__title" data-aos="flip-up" data-aos-delay="200"><?php echo $title; ?></h2>
            <div class="section__divider section__divider--small"></div>
            <p class="section__text" data-aos="flip-up" data-aos-delay="200"><?php echo $text; ?></p>
          </div>
          <div class="banner" data-aos="fade-in" style="background-image: url(<?php echo $third_section['banner']['image']; ?>)">
            <div class="banner__text-block">
              <h2 class="banner__text"  data-aos="flip-up" data-aos-delay="200"><?php echo $third_section['banner']['title']; ?></h2>
              <?php if ($link) : ?>
                <a href="<?php echo $link['url']; ?>" 
                  title="<?php echo $link['title']; ?>" 
                  alt="<?php echo $link['title']; ?>"
                  data-aos="fade-up" data-aos-delay="200"
                  class="banner__btn">Read more</a>
              <?php endif; ?>
            </div>
          </div>
      <?php endif; ?> 

      <div class="container">
        <div class="news">
          <div class="news__grid">
            <?php $posts = get_posts( array(
              'numberposts' => 3,
              'orderby'     => 'date',
              'order'       => 'DESC',
              'post_type'   => 'post',
              'suppress_filters' => true,
            ) );
            $delay = 0;
            foreach( $posts as $post ) :
              $delay += 250;
                setup_postdata( $post ); 
                $image = get_the_post_thumbnail_url($post->ID, 'thumb_414'); ?>

                <div class="news__item news__item--with-img" data-aos="fade-up" data-aos-duration="400" data-aos-delay="<?php echo $delay?>">
                  <div class="news__item-img" style="background-image: url(<?php echo $image; ?>)"></div>
                  <div class="news__text-block">
                    <span class="news__date"><?php the_date('m.d.Y'); ?></span>
                    <a href="<?php echo get_permalink(); ?>" class="news__link">
                      <h3 class="news__title"><?php the_title(); ?></h3>
                    </a>
                    <div class="news__text"><?php the_excerpt(); ?></div>
                    <a
                      href="<?php echo get_permalink(); ?>" class="news__arrow"></a>
                  </div>
                </div>
                
                <?php wp_reset_postdata();
            endforeach; ?>

          </div>
          <div data-aos="fade-up" data-aos-duration="400">
            <a href="/news/" class="btn btn--news">Read all news</a>
          </div>
        </div>
      </div>

    </section>
  
    <?php get_footer(); ?>