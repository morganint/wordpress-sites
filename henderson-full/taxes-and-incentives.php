

<?php

/**

 * Template Name: Taxes and Incentives

 * Template Post Type: page

 *

 * @package Henderson

 * @since Henderson 1.0

 */



get_header(); ?>



    <?php if (get_field('hero_image')): ?>

        <div class="page-img parallax-window">
            <img src="<?php the_field('hero_image');?>">
        </div>

    <?php endif;?>



    <?php if ( have_rows('page_menu') ) : ?>

        <div class="slick-nav-spacer">

            <nav class="slick-nav" data-aos="fade-in" data-aos-delay="200" data-aos-offset="-500">

                <ul class="slick-nav__list">

                <?php 

                    $i = 1;

                    while ( have_rows('page_menu') ) : the_row();

                        $title = get_sub_field('menu_title'); ?>

                        

                        <li class="slick-nav__item">

                            <a href="#section-<?php echo $i; ?>" class="slick-nav__anchor" title="<?php echo $title; ?>"><?php echo $title; ?></a>

                        </li>



                    <?php 

                    $i++;

                    endwhile; ?>

                </ul>

            </nav>

        </div>

    

    <?php endif; ?>



    <?php if ( have_rows('first_section') ) :

        

        $section = get_field('first_section'); ?>



        <section class="section" id="section-1">

            <div class="container">

                <h2 class="section__title" data-aos="flip-up" data-aos-delay="200"><?php echo $section['title']; ?></h2>

                <div class="section__divider" data-aos="fade-in"></div>

                <div class="section__text" data-aos="fade-up" data-aos-delay="200"><?php echo $section['content']; ?></div>

            </div>

        </section>

    

    <?php endif; ?>



    <?php if ( have_rows('second_section') ) :

        

        $section = get_field('second_section');

        $taxlist = $section['tax_list'];

        $leftCol = $taxlist['left_col'];

        $rightCol = $taxlist['right_col']; ?>



        <section class="section section--grey" id="section-2">

            <div class="container">

                <h4 class="section__title section__title--small" data-aos="flip-up" data-aos-delay="200"><?php echo $section['title']; ?></h4>

                <div class="section__divider" data-aos="fade-in"></div>

                <div class="section__text" data-aos="fade-up" data-aos-delay="200"><?php echo $section['content']; ?></div>

                <?php if ($taxlist) : ?>

                    <div class="tax-list">



                        <?php if ( $leftCol ) : ?>



                            <div class="tax-list__left-col">



                                <?php $delay = 0; foreach($leftCol as $item) : $delay += 150; ?>



                                    <p class="tax-list__item" data-aos-anchor=".tax-list"  data-aos="fade-right" data-aos-delay="<?php echo $delay; ?>"><?php echo $item['item_text']; ?></p>



                                <?php endforeach; ?>



                            </div>



                        <?php endif; ?>



                        <?php if ( $rightCol ) : ?>



                            <div class="tax-list__right-col">



                                <?php $delay = 0; foreach($rightCol as $item) : $delay += 150; ?>



                                    <p class="tax-list__item" data-aos-anchor=".tax-list"  data-aos="fade-left" data-aos-delay="<?php echo $delay; ?>"><?php echo $item['item_text']; ?></p>



                                <?php endforeach; ?>



                            </div>



                        <?php endif; ?>



                    </div>



                <?php endif; ?>



            </div>

        </section>



    <?php endif; ?>



    <?php if ( have_rows('third_section') ) : 

        $section = get_field( 'third_section' );

        $list    = $section['advantage_list']; ?>



        <section class="section" id="section-3">

            <div class="container">

                <h2 class="section__title" data-aos="flip-up" data-aos-delay="200"><?php echo $section['title']; ?></h2>

                <div class="section__divider" data-aos="fade-in"></div>



                <?php if ($list) : ?>



                    <div class="advantages-list">



                        <?php $delay = 0; foreach($list as $item) : $delay += 150; ?>


                            <a href="<?php echo $item['link']; ?>"
                                class="advantages-list__item" data-aos-anchor=".advantages-list"
                                data-aos="fade-up" data-aos-duration="400" data-aos-delay="<?php echo $delay; ?>"
                            >

                                <div class="advantages-list__img" style="background-image: url(<?php echo $item['icon']; ?>)">
                                    <i class="advantages-list__img-circle"></i>
                                </div>
                                
                                <div class="advantages-list__text-block">

                                    <b class="advantages-list__title"><?php echo $item['title']; ?></b>

                                    <p class="advantages-list__text"><?php echo $item['text']; ?></p>

                                </div>

                            </a>



                        <?php endforeach; ?>

                    </div>



                <?php endif; ?>



            </div>

        </section>



    <?php endif; ?>

    

    <?php get_footer(); ?>